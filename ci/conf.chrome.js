/**
 * @author: @AngularClass
 */
exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',
	specs: ['todo.spec.js'],
	useAllAngular2AppRoots: true,
	directConnect: true,
	onPrepare: function() {
		browser.ignoreSynchronization = true;
	},
	capabilities: {
		'browserName': 'chrome',
		'chromeOptions': {
			'args': ['no-sandbox']
		}
	},
};
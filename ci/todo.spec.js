describe('angularjs homepage todo list', function() {
	it('should add a todo', function() {
		var e = element(by.id('login'));
		browser.get('http://www.jubbr.com/app_test/auth/login');
		browser.sleep(500);
		var EC = protractor.ExpectedConditions;
		browser.wait(EC.visibilityOf(e), 120000);
		// browser.get('http://localhost:3000');
		//browser.manage().window().setSize(width, height);
		//headerOperations.goToPageFromNavigation([]);
		// Window width!!!
		// 960
		browser.manage().window().getSize().then((size) => console.log(size.width));
	});

	it('should make successful login', () => {
		var emailField = element(by.id('loginemail'));
		var passField = element(by.id('loginpassword'));
		var profileImage = element(by.css('span.profile-image'));
		var ok = element(by.css('button.ok'));

		emailField.sendKeys('j@jubbr.com');
		passField.sendKeys('test');
		
		ok.click();
		browser.sleep(500);

		var EC = protractor.ExpectedConditions;
		browser.wait(EC.visibilityOf(profileImage), 120000);
		// Wait for page to be fully loaded
		browser.sleep(3000);
	});
});
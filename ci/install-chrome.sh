#!/bin/bash

set -xe
apt-get update
# Install Chrome browser
apt-get -y install libxpm4 libxrender1 libgtk2.0-0 libnss3 libgconf-2-4

# Dependencies to make "headless" chrome/selenium work:
apt-get install -y gtk2-engines-pixbuf
apt-get install -y xfonts-cyrillic xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable

# Optional but nifty: For capturing screenshots of Xvfb display:
apt-get install -y imagemagick x11-apps dbus-x11
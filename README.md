# The jubbr.com web application frontend [![build status](https://repo.jubbr.com/jubbr/ng2/badges/master/build.svg)](https://repo.jubbr.com/jubbr/ng2/builds?scope=all)

This is part of the software used to run jubbr.com.
Please read the [LICENSE](LICENSE) before using and/or redistributing.

Note that this software package may contain bundled third-party
software that could only be used under a different license. The
full set of licensing information could be found under [doc/legal](doc/legal).

## Development Model 

We are using a read-only master, rebase feature branches workflow. Please add
a reference to the issue your branch addresses, e.g. ```feature/133-fix-gc```.

Please use a credential cache and https based access, e.g.

```sh
$ git config --global credential.helper cache
$ git config --global credential.helper 'cache --timeout=3600'
```
(see https://help.github.com/articles/caching-your-github-password-in-git/)

## Frontend Tooling

The build process for the frontend is performed using [webpack](https://webpack.github.io/).

## Code Formatting and Linting

This package contains configuration files for various linting tools.
Mandatory linters for typescript are
- tslint

Please use them.

## Prerequisites

You need to install current versions of

| Package Link | Check Version | Working Version |
| ------------ | ------------- | --------------- |
| [git](https://git-scm.com/)                                   | `git --version`        | 1.9.1 |
| [nodejs](https://nodejs.org/en/)                              | `nodejs --version`     | 4.4.4 (LTS) |

## Package Handling
Now that npm finally has switched to peer-dependencies it is kind of usable.
Still the whole nodejs/npm ecosystem is bloatware and 3rd party registries a
major no-no for serious software development. Having that said:
- we use npm AND ONLY npm as the package manager for third party libraries
- we check in a copy of each third party library that is needed for deployment
to be able to package and build even with 3rd party registries down/unreachable
- if a public third party library needs a fix, a patched clone hosted at the
same repo hoster like the original must be used (and checked in as well)

## Package Structure

TODO -- document this

## Testing

TODO -- document this

- the UI is tested using phantomflow
- user workflows are in folder ```flow/```
- visual results from the tests are in folder ```flow/visual/```

## Deployment 

Deployment is done using gitlab CI and git hooks.

TODO -- document this better.
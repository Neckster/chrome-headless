#!/bin/bash
# kludge to work arount gitlab 8.7 and prior limitation
# see issue #4768
# https://gitlab.com/gitlab-org/gitlab-ce/issues/4768
# taken from 

set -e

# check requirements
REQS=(curl jq)

for REQ in ${REQS[@]}
do
  which ${REQ} >/dev/null 2>&1
  if test  ! $? -eq 0
  then
    echo "requirement ${REQ} is missing" >&2
    exit 1
  fi
done

# set vars in project settings
VARS=(BASE_URL CI_AUTOBUILD_TOKEN PROJECT STAGE REF FILE)

check_vars() {
  for VAR in ${VARS[@]}
  do
    if test -z "${!VAR}"
    then
      echo "\$${VAR} is missing" >&2
      exit 1
    fi
  done
}

check_vars

urlencode() {
  # urlencode <string>
  old_lc_collate=$LC_COLLATE
  LC_COLLATE=C
  
  local length="${#1}"
  for (( i = 0; i < length; i++ )); do
    local c="${1:i:1}"
    case $c in
      [a-zA-Z0-9.~_-]) printf "$c" ;;
      *) printf '%%%02X' "'$c" ;;
    esac
  done
  
  LC_COLLATE=$old_lc_collate
}

PROJECT_UE=$(urlencode ${PROJECT})

# fetch last successful build
LAST_SUCCESSFUL_BUILD=$(\
curl -s -H "PRIVATE-TOKEN: ${CI_AUTOBUILD_TOKEN}" "${BASE_URL}/api/v3/projects/${PROJECT_UE}/builds" \
| jq -c '.[] | select(.status=="success") | select(.stage=="'${STAGE}'") | select(.ref=="'${REF}'") | {id}' \
| head -n1 \
| grep -oE '[0-9]+'\
)

echo "fetch build artifact: ${BASE_URL}/${PROJECT}/builds/${LAST_SUCCESSFUL_BUILD}/artifacts/file/${FILE}" >&2
curl -s -H "PRIVATE-TOKEN: ${CI_AUTOBUILD_TOKEN}" "${BASE_URL}/${PROJECT}/builds/${LAST_SUCCESSFUL_BUILD}/artifacts/file/${FILE}"

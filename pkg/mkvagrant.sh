#!/bin/sh
set -e

DST_DIR="${PKG}/vagrant"

test -f "${BOX_SRC_FILE}" && rm -f "${BOX_SRC_FILE}"
test -d "${DST_DIR}" || mkdir -p "${DST_DIR}"

PROJECT="${BOX_SRC_PROJECT}" \
STAGE="${BOX_SRC_STAGE}" \
REF="${BOX_SRC_VERSION}" \
FILE="pkg/vagrant/${BOX_SRC_FILE}" \
./gitlab-ci-helper.sh > "${BOX_SRC_FILE}"

vagrant destroy -f 
vagrant up
vagrant halt
vagrant package -o "${DST_DIR}/${BOX_DST_FILE}"
vagrant destroy -f 
rm "${BOX_SRC_FILE}"

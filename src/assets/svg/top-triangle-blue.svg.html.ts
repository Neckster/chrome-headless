
export const topTriangleBlue = `
  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="10px" height="10px" viewBox="0 0 24 24" xml:space="preserve">
    <polygon id="upTriangleBlue" stroke="#048ac3" stroke-width="4" fill="rgb(4, 138, 195)" points="0 0, 20 0, 20 20, 0 0" />
 </svg>
`;

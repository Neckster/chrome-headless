export const downTriangle = `
  <svg class='downTriangle' version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="10px" height="10px" viewBox="0 0 24 24" xml:space="preserve">
    <polygon id="downTriangle" stroke="#666" stroke-width="4" fill="rgb(102, 102, 102)" points="20 0, 20 20, 0 20, 20 0" />
 </svg>
`;


import {
  Component,
  ViewEncapsulation }               from '@angular/core';
import { NgRedux }                  from 'ng2-redux';
import {
  initialState,
  rootReducer }                     from './store/reducer';
import {
  TranslateService,
  TranslatePipe }                   from 'ng2-translate/ng2-translate';
import { LuaApiHttp }               from './services/shared/lua-http-api.service';
import { AuthService }              from './services/shared/auth.service';
import { RootState }                from './store/reducer';
import { AppStoreAction }           from './store/action';
import { htmlApp }                  from './app.component.html.ts';
import { ParseOrganisationService } from './services/shared/parseOrganisation.service';
import { Cache }                    from './services/shared/cache.service';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  pipes: [TranslatePipe],
  encapsulation: ViewEncapsulation.None,
  styles: [
    require('!raw!less!../less/jubbr-user-style.less')
  ],
  template: htmlApp
})
export class App {

  constructor(public translate: TranslateService, public http: LuaApiHttp,
              public auth: AuthService, private ngRedux: NgRedux<RootState>,
              public action: AppStoreAction, public cache: Cache
  ) {

    let userLang = navigator.language.split('-')[0]; // use navigator lang if available
    userLang = /(fr|en)/gi.test(userLang) ? userLang : 'en';
    translate.setDefaultLang('en');
    userLang = this.cache.get('lang') || userLang;
    ParseOrganisationService.lang = userLang;
    translate.use(userLang);

    this.ngRedux.configureStore(
      rootReducer, initialState,
      [logger]
    );
  }
  ngOnInit() {
  }
}

function logger({ getState }) {
  return (next) => (action) => {
    // console.log('will dispatch', action);

    // Call the next dispatch method in the middleware chain.
    let returnValue = next(action);

    // console.log('state after dispatch', getState());

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue;
  };
}

import { AssignmentLog, AssignmentStatus } from './Assignment';

export interface IJob {
  chef: boolean;
  companyId: any;
  curAsClass: string;
  from: string;
  info: any;
  log: any[];
  pending: boolean;
  pinfo: any;
  pname: string;
  position_id: number;
  published: boolean;
  pubmail: any;
  reminderMessageId: number;
  shiftIdx: number;
  stat: any;
  to: string;
  uname: string;
  unitId: number;
  validTo: number;
  webuser_id: number;
  weekday: number;
}

export class Job {
  private _prevJob: Job;
  private _nextJob: Job;
  constructor(

    private _chef: boolean,
    private _companyId: number,
    private _curAsClass: string,
    private _from: string,
    private _to: string,
    private _log: Array <AssignmentLog>,
    private _pinfo: string,
    private _pname: string,
    private _positionId: number,
    private _published: boolean,
    private _pending: boolean,
    private _pubmail: number,
    private _reminderMessageId: number,
    private _shiftIdx: number,
    private _stat: AssignmentStatus|any,
    private _unitId: number,
    private _validTo: number,
    private _webuserId: number,
    private _weekday: number,
    private _info
  ) {
    if (this._stat && !(this._stat instanceof AssignmentStatus)) {

      this._stat = new AssignmentStatus(this._stat.declined, this._stat.deleted);
    }
  }

  getShortCut () {
    return this.weekday + '-' + this.shift_idx;
  }

  isActive() {
    return !this.stat || !(this.stat.deleted || this.stat.declined);
  }

  get prevJob(): Job {
    return this._prevJob;
  }

  set prevJob(value: Job) {
    this._prevJob = value;
  }

  get nextJob(): Job {
    return this._nextJob;
  }

  set nextJob(value: Job) {
    this._nextJob = value;
  }

  get chef(): boolean {
    return this._chef;
  }

  set chef(value: boolean) {
    this._chef = value;
  }

  get company_id(): number {
    return this._companyId;
  }

  set company_id(value: number) {
    this._companyId = value;
  }

  get curAsClass(): string {
    return this._curAsClass;
  }

  set curAsClass(value: string) {
    this._curAsClass = value;
  }

  get from(): string {
    return this._from;
  }

  set from(value: string) {
    this._from = value;
  }

  get to(): string {
    return this._to;
  }

  set to(value: string) {
    this._to = value;
  }

  get log(): Array<AssignmentLog> {
    return this._log;
  }

  set log(value: Array<AssignmentLog>) {
    this._log = value;
  }

  get pinfo(): string {
    return this._pinfo;
  }

  set pinfo(value: string) {
    this._pinfo = value;
  }

  get pname(): string {
    return this._pname;
  }

  set pname(value: string) {
    this._pname = value;
  }

  get pending(): boolean {
    return this._pending;
  }

  set pending(value: boolean) {
    this._pending = value;
  }

  get position_id(): number {
    return this._positionId;
  }

  set position_id(value: number) {
    this._positionId = value;
  }

  get published(): boolean {
    return this._published;
  }

  set published(value: boolean) {
    this._published = value;
  }

  get pubmail(): number {
    return this._pubmail;
  }

  set pubmail(value: number) {
    this._pubmail = value;
  }

  get reminder_message_id(): number {
    return this._reminderMessageId;
  }

  set reminder_message_id(value: number) {
    this._reminderMessageId = value;
  }

  get shift_idx(): number {
    return this._shiftIdx;
  }

  set shift_idx(value: number) {
    this._shiftIdx = value;
  }

  get stat(): AssignmentStatus|any {
    return this._stat;
  }

  set stat(value: AssignmentStatus|any) {
    this._stat = value;
  }

  get unit_id(): number {
    return this._unitId;
  }

  set unit_id(value: number) {
    this._unitId = value;
  }

  get valid_to(): number {
    return this._validTo;
  }

  set valid_to(value: number) {
    this._validTo = value;
  }

  get webuser_id(): number {
    return this._webuserId;
  }

  set webuser_id(value: number) {
    this._webuserId = value;
  }

  get weekday(): number {
    return this._weekday;
  }

  set weekday(value: number) {
    this._weekday = value;
  }
  get info(): any {
    return this._info;
  }

  set info(value: any) {
    this._info = value;


  }
}

export class JobPlan {
  constructor (
    private _maxWorkTime: number,
    private _minJobCount: number,
    private _positionId: number,
    private _published: boolean,
    private _shiftIdx: number,
    private _webuserId: number,
    private _weekday: number
  ) {

  }

  get max_work_time(): number {
    return this._maxWorkTime;
  }

  set max_work_time(value: number) {
    this._maxWorkTime = value;
  }

  get min_job_count(): number {
    return this._minJobCount;
  }

  set min_job_count(value: number) {
    this._minJobCount = value;
  }

  get published(): boolean {
    return this._published;
  }

  set published(value: boolean) {
    this._published = value;
  }

  get webuser_id(): number {
    return this._webuserId;
  }

  set webuser_id(value: number) {
    this._webuserId = value;
  }

  get weekday(): number {
    return this._weekday;
  }

  set weekday(value: number) {
    this._weekday = value;
  }

  get position_id(): number {
    return this._positionId;
  }

  set position_id(value: number) {
    this._positionId = value;
  }

  get shift_idx(): number {
    return this._shiftIdx;
  }

  set shift_idx(value: number) {
    this._shiftIdx = value;
  }
}

export class JobFactory {
  public static createJob(rawJob): Job|JobPlan {
    if (rawJob.webuser_id !== 0) {
      let log = [];
      if (rawJob.log && rawJob.log.length) {
        log = rawJob.log.map(function (item: any[]) {
          return new AssignmentLog(item[0], item[1], item[2], item[3], item[4]);
        });
      }
      // console.log(rawJob.pname);
      return new Job(
        rawJob.chef,
        rawJob.company_id,
        rawJob.curAsClass,
        rawJob.from,
        rawJob.to,
        log,
        rawJob.pinfo,
        rawJob.pname,
        rawJob.position_id,
        rawJob.published,
        rawJob.pending,
        rawJob.pubmail,
        rawJob.reminder_message_id,
        rawJob.shift_idx,
        rawJob.stat,
        rawJob.unit_id,
        rawJob.valid_to,
        rawJob.webuser_id,
        rawJob.weekday,
        rawJob.info

      );
    } else {
      return new JobPlan(
        rawJob.max_work_time,
        rawJob.min_job_count,
        rawJob.position_id,
        rawJob.published,
        rawJob.shift_idx,
        rawJob.webuser_id,
        rawJob.weekday
      );
    }
  }
}

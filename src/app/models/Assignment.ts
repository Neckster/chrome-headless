export class AssignmentLog extends Array {
  public static STATUS_INDEX = 0;
  public static INFO_INDEX = 1;
  public static ID_INDEX = 2;
  public static TIME_INDEX = 3;

  constructor(...params) {
    super();
    this.push.apply(this, params);
  }
  get status(): string {
    return this[AssignmentLog.STATUS_INDEX];
  }
  set status(status: string) {
    this[AssignmentLog.STATUS_INDEX] = status;
  }
  get info(): any {
    return this[AssignmentLog.INFO_INDEX];
  }
  set info(info: any) {
    this[AssignmentLog.INFO_INDEX] = info;
  }
  get id(): number {
    return this[AssignmentLog.ID_INDEX];
  }
  set id(id: number) {
    this[AssignmentLog.ID_INDEX] = id;
  }
  get time(): number {
    return this[AssignmentLog.TIME_INDEX];
  }
  set time(time: number) {
    this[AssignmentLog.TIME_INDEX] = time;
  }
}
export class AssignmentStatus {
  constructor(
    private _declined?: any,
    private _deleted?: number
  ) {

  }

  get declined(): any {
    return this._declined;
  }

  set declined(value: any) {
    this._declined = value;
  }

  get deleted(): number {
    return this._deleted;
  }

  set deleted(value: number) {
    this._deleted = value;
  }
}


export class Assignment {
  private _prevAssign: Assignment;
  private _nextAssign: Assignment;
  private _tFrom: number;
  private _tTo: number;
  private _staff: any;
  constructor(
    private _chef: boolean,
    private _cname: string,
    private _companyId: number,
    private _from: string,
    private _to: string,
    private _log: Array <AssignmentLog>,
    private _nname: string,
    private _pinfo: string,
    private _pname: string,
    private _positionId: number,
    private _published: boolean,
    private _pubmail: number,
    private _reminderMessageId: number,
    private _shiftIdx: number,
    private _stat: AssignmentStatus|any,
    private _unitId: number,
    private _validTo: number,
    private _webuserId: number,
    private _week: number,
    private _weekday: number
  ) {
    if (this.stat && !(this.stat instanceof AssignmentStatus)) {
      this.stat = new AssignmentStatus(this.stat.declined, this.stat.deleted);
    }
  }

  getShortCut() {
    return this.weekday + '-' + this.shift_idx;
  }

  isActive() {
    return !this.stat || !(this.stat.deleted || this.stat.declined);
  }

  get prevAssign(): Assignment {
    return this._prevAssign;
  }

  set prevAssign(value: Assignment) {
    this._prevAssign = value;
  }

  get nextAssign(): Assignment {
    return this._nextAssign;
  }

  set nextAssign(value: Assignment) {
    this._nextAssign = value;
  }

  get chef(): boolean {
    return this._chef;
  }

  set chef(value: boolean) {
    this._chef = value;
  }

  get cname(): string {
    return this._cname;
  }

  set cname(value: string) {
    this._cname = value;
  }

  get company_id(): number {
    return this._companyId;
  }

  set company_id(value: number) {
    this._companyId = value;
  }

  get from(): string {
    return this._from;
  }

  set from(value: string) {
    this._from = value;
  }

  get to(): string {
    return this._to;
  }

  set to(value: string) {
    this._to = value;
  }

  get log(): Array<AssignmentLog> {
    return this._log;
  }

  set log(value: Array<AssignmentLog>) {
    this._log = value;
  }

  get nname(): string {
    return this._nname;
  }

  set nname(value: string) {
    this._nname = value;
  }

  get pinfo(): string {
    return this._pinfo;
  }

  set pinfo(value: string) {
    this._pinfo = value;
  }

  get pname(): string {
    return this._pname;
  }

  set pname(value: string) {
    this._pname = value;
  }

  get position_id(): number {
    return this._positionId;
  }

  set position_id(value: number) {
    this._positionId = value;
  }

  get published(): boolean {
    return this._published;
  }

  set published(value: boolean) {
    this._published = value;
  }

  get pubmail(): number {
    return this._pubmail;
  }

  set pubmail(value: number) {
    this._pubmail = value;
  }

  get reminder_message_id(): number {
    return this._reminderMessageId;
  }

  set reminder_message_id(value: number) {
    this._reminderMessageId = value;
  }

  get shift_idx(): number {
    return this._shiftIdx;
  }

  set shift_idx(value: number) {
    this._shiftIdx = value;
  }

  get stat(): AssignmentStatus|any {
    return this._stat;
  }

  set stat(value: AssignmentStatus|any) {
    this._stat = value;
  }

  get unit_id(): number {
    return this._unitId;
  }

  set unit_id(value: number) {
    this._unitId = value;
  }

  get valid_to(): number {
    return this._validTo;
  }

  set valid_to(value: number) {
    this._validTo = value;
  }

  get webuser_id(): number {
    return this._webuserId;
  }

  set webuser_id(value: number) {
    this._webuserId = value;
  }

  get week(): number {
    return this._week;
  }

  set week(value: number) {
    this._week = value;
  }

  get weekday(): number {
    return this._weekday;
  }

  set weekday(value: number) {
    this._weekday = value;
  }

  get tFrom(): number {
    return this._tFrom;
  }

  set tFrom(value: number) {
    this._tFrom = value;
  }

  get tTo(): number {
    return this._tTo;
  }

  get staff(): any {
    return this._staff;
  }

  set staff(value: any) {
    this._staff = value;
  }
}

export class AssignmentFactory {
  public static createAssignment(rawAssignment): Assignment {
    let log = [];
    if (rawAssignment.log && rawAssignment.log.length) {
      log = rawAssignment.log.map(function(item: any[]){
        return new AssignmentLog(item[0], item[1], item[2], item[3], item[4]);
      });
    }
    return new Assignment(
      rawAssignment.chef,
      rawAssignment.cname,
      rawAssignment.company_id,
      rawAssignment.from,
      rawAssignment.to,
      log,
      rawAssignment.nname,
      rawAssignment.pinfo,
      rawAssignment.pname,
      rawAssignment.position_id,
      rawAssignment.published,
      rawAssignment.pubmail,
      rawAssignment.reminder_message_id,
      rawAssignment.shift_idx,
      rawAssignment.stat,
      rawAssignment.unit_id,
      rawAssignment.valid_to,
      rawAssignment.webuser_id,
      rawAssignment.week,
      rawAssignment.weekday
    );
  }
}

// Variable for debug
let counter = 0;

export class Position {
  public index;
  constructor(
    private _positionId: number,
    private _companyId: number,
    private _homeStaff: Array<number>,
    private _pname: string,
    private _positionStatus: Array<string>,
    private _shift: Array<any>,
    private _staff: Array<any>,
    private _unitId: number,
    private _unpublish: number
  ) {
    this.index = ++counter;
  }

  get position_id(): number {
    return this._positionId;
  }

  set position_id(value: number) {
    this._positionId = value;
  }

  get company_id(): number {
    return this._companyId;
  }

  set company_id(value: number) {
    this._companyId = value;
  }

  get home_staff(): Array<number> {
    return this._homeStaff;
  }

  set home_staff(value: Array<number>) {
    this._homeStaff = value;
  }

  get pname(): string {
    return this._pname;
  }

  set pname(value: string) {
    this._pname = value;
  }

  get positionStatus(): Array<string> {
    return this._positionStatus;
  }

  set positionStatus(value: Array<string>) {
    this._positionStatus = value;
  }

  get shift(): Array<any> {
    return this._shift;
  }

  set shift(value: Array<any>) {
    this._shift = value;
  }

  get staff(): Array<any> {
    return this._staff;
  }

  set staff(value: Array<any>) {
    this._staff = value;
  }

  get unit_id(): number {
    return this._unitId;
  }

  set unit_id(value: number) {
    this._unitId = value;
  }

  get unpublish(): number {
    return this._unpublish;
  }

  set unpublish(value: number) {
    this._unpublish = value;
  }
}

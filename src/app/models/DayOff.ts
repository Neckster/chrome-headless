import { forEach } from 'ramda';
import * as moment from 'moment';
import { select } from 'ng2-redux';
import { Inject, Injectable, provide } from '@angular/core';

export const DAY_OFF_STATUS_APPROVE = 2;
export const DAY_OFF_STATUS_DECLINE = -1;
export const DAY_OFF_STATUS_SEND = 0;
export const DAY_OFF_STATUS_CHANGE = 1;
export const DAY_OFF_STATUS_UPDATE = 3;

export class DayOffHistory {
  static statusLabels = {'-1': 'Declined', 0: 'Sent', 1: 'Changed', 2: 'Approved', 3: 'Updated'};

  public dateMoment: moment.Moment;
  public statusLabel: string;

  constructor(
      private _status: number,
      private _date: number,
      private _webuserId: number,
      private _webuserName: string
  ) {
    this.dateMoment = moment.unix(_date);
    this.statusLabel = DayOffHistory.statusLabels[_status];
  }

  get status(): number {
    return this._status;
  }

  set status(value: number) {
    this._status = value;
  }

  get date(): number {
    return this._date;
  }

  set date(value: number) {
    this._date = value;
  }

  get webuser_id(): number {
    return this._webuserId;
  }

  set webuser_id(value: number) {
    this._webuserId = value;
  }

  get webuser_name(): string {
    return this._webuserName;
  }

  set webuser_name(value: string) {
    this._webuserName = value;
  }
}

export class DayOff {
  public approve: DayOffHistory;
  public decline: DayOffHistory;
  public update: DayOffHistory;
  public change: DayOffHistory;
  public send: DayOffHistory;


  public fromDate: moment.Moment;
  public toDate: moment.Moment;

  constructor(
    private _companyId: number,
    private _fromWeek: number,
    private _fromWeekday: number,
    private _fromShift: number,
    private _toWeek: number,
    private _toWeekday: number,
    private _toShift: number,
    private _status: number,
    private _reason: string,
    private _message: string,
    private _managerMessage: string,
    private _totalDays: number,
    private _history: DayOffHistory[],
    private _webuserId?: number,
    private _dayOffId?: number,
    private _fromWebuserId?: number
  ) {
    this.fromDate = moment(`${_fromWeek}`, 'GGWW').weekday(_fromWeekday);
    this.toDate = moment(`${_toWeek}`, 'GGWW').weekday(_toWeekday);

    // this.fromDate = moment(`${_fromWeek}${_fromWeekday}`, 'GGWWE');
    // this.toDate = moment(`${_toWeek}${_toWeekday}`, 'GGWWE');

    this.approve = this.filterForStatus(DAY_OFF_STATUS_APPROVE);
    this.decline = this.filterForStatus(DAY_OFF_STATUS_DECLINE);
    this.update = this.filterForStatus(DAY_OFF_STATUS_UPDATE);
    this.change = this.filterForStatus(DAY_OFF_STATUS_CHANGE);
    this.send = this.filterForStatus(DAY_OFF_STATUS_SEND);
  }

  get webuserId(): number {
    return this._webuserId;
  }

  set webuserId(value: number) {
    this._webuserId = value;
  }

  get companyId(): number {
    return this._companyId;
  }

  set companyId(value: number) {
    this._companyId = value;
  }

  get fromWeek(): number {
    return this._fromWeek;
  }

  set fromWeek(value: number) {
    this._fromWeek = value;
  }

  get fromWeekday(): number {
    return this._fromWeekday;
  }

  set fromWeekday(value: number) {
    this._fromWeekday = value;
  }

  get fromShift(): number {
    return this._fromShift;
  }

  set fromShift(value: number) {
    this._fromShift = value;
  }

  get toShift(): number {
    return this._toShift;
  }

  set toShift(value: number) {
    this._toShift = value;
  }

  get status(): number {
    return this._status;
  }

  set status(value: number) {
    this._status = value;
  }

  get statusLabel() {
    return [DAY_OFF_STATUS_CHANGE, DAY_OFF_STATUS_SEND].indexOf(this.status) >= 0
      ? 'Open'
      : DayOffHistory.statusLabels[this.status];
  }

  get toWeek(): number {
    return this._toWeek;
  }

  set toWeek(value: number) {
    this._toWeek = value;
  }

  get toWeekday(): number {
    return this._toWeekday;
  }

  set toWeekday(value: number) {
    this._toWeekday = value;
  }

  get reason(): string {
    return this._reason;
  }

  set reason(value: string) {
    this._reason = value;
  }

  get message(): string {
    return this._message;
  }

  set message(value: string) {
    this._message = value;
  }

  get managerMessage(): string {
    return this._managerMessage;
  }

  set managerMessage(value: string) {
    this._managerMessage = value;
  }

  get totalDays(): number {
    return this._totalDays;
  }

  set totalDays(value: number) {
    this._totalDays = value;
  }

  get history(): any { // : any
    return this._history;
  }

  set history(value: any) { // : Array
    this._history = value;
  }

  get dayOffId(): number {
    return this._dayOffId;
  }

  set dayOffId(value: number) {
    this._dayOffId = value;
  }

  get fromWebuserId(): number {
    return this._fromWebuserId;
  }

  set fromWebuserId(value: number) {
    this._fromWebuserId = value;
  }

  isOpen() {
    return [DAY_OFF_STATUS_SEND, DAY_OFF_STATUS_CHANGE].indexOf(this.status) >= 0;
  }

  /* tslint:disable:no-string-literal */
  public toJSON() {
    let jsonData = {
      webuser_id: this.webuserId,
      company_id: this.companyId,
      from_week: this.fromWeek,
      from_weekday: this.fromWeekday,
      from_shift: this.fromShift,
      to_week: this.toWeek,
      to_weekday: this.toWeekday,
      to_shift: this.toShift,
      message: this.message,
      manager_message: this.managerMessage,
      reason: this.reason,
      status: this.status,
      total_days: this.totalDays,
      from_webuser_id: this.fromWebuserId
    };
    if (this.dayOffId) {
      jsonData['day_off_id'] = this.dayOffId;
    }
    return jsonData;
  }

  /**
   *
   * @param status
   * @param firstOne
   * @returns DayOffHistory|null
   */
  private filterForStatus(status: number, firstOne?): DayOffHistory {
    const filteredHistory = this.history.filter((item) => {
      return +item.status === +status;
    });
    if (firstOne) {
      return filteredHistory[0];
    }
    return filteredHistory.slice(-1).pop();
  }

  /* tslint:disable:no-string-literal */
}

@Injectable()
export class DayOffFactory {
  @select() info$;
  private fromWebuserId;

  public constructor() {
    this.info$.subscribe((info) => {
      if (info.webuser) {
        this.fromWebuserId = info.webuser.webuser_id;
      }
    });
  }

  // static createDayOffFromForm(formData): DayOff {
  //   return new DayOff();
  // }
  createDayOff(rawData: any): any {
    let dayOffs = [];

    let history = rawData.history ? rawData.history.map(v => new DayOffHistory(
        v.status, v.date, v.webuser_id, v.webuser_name
    )) : [];
    forEach(companyId => {
      dayOffs.push(new DayOff(
          companyId as number,
          rawData.from_week as number,
          rawData.from_weekday as number,
          rawData.from_shift as number,
          rawData.to_week as number,
          rawData.to_weekday as number,
          rawData.to_shift as number,
          rawData.status as number,
          rawData.reason,
          rawData.message,
          rawData.manager_message,
          rawData.total_days,
          history,
          rawData.webuser_id,
          rawData.day_off_id,
          rawData.day_off_id ? rawData.from_webuser_id : this.fromWebuserId
      ));
    }, rawData.company_id instanceof Array ? rawData.company_id : [rawData.company_id]);

    return dayOffs.length > 1 ? dayOffs : dayOffs[0];
  }
}

export const DAY_OFF_FACTORY_PROVIDER: any[] = [
  provide(DayOffFactory,
    {
      useFactory: () => new DayOffFactory()
    })
];


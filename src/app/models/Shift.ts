
// Variable for debug
let counter = 0;

export class ShiftInfo {
  public index;
  constructor(
    private _daysInfo: Array<any>,
    private _jobberPerShift: Array<any>,
    private _publisedShPosAss: Array<any>,
    private _shiftUnpublishArr: Array<any>,
    private _shiftUnpublished: number,
    private _shiftIndex: number
  ) {
    this.index = ++counter;
  }

  get daysInfo(): Array<any> {
    return this._daysInfo;
  }

  set daysInfo(value: Array<any>) {
    this._daysInfo = value;
  }

  get jobberPerShift(): Array<any> {
    return this._jobberPerShift;
  }

  set jobberPerShift(value: Array<any>) {
    this._jobberPerShift = value;
  }

  get publisedShPosAss(): Array<any> {
    return this._publisedShPosAss;
  }

  set publisedShPosAss(value: Array<any>) {
    this._publisedShPosAss = value;
  }

  get shiftUnpublishArr(): Array<any> {
    return this._shiftUnpublishArr;
  }

  set shiftUnpublishArr(value: Array<any>) {
    this._shiftUnpublishArr = value;
  }

  get shiftUnpublished(): number {
    return this._shiftUnpublished;
  }

  set shiftUnpublished(value: number) {
    this._shiftUnpublished = value;
  }

  get shift_index(): number {
    return this._shiftIndex;
  }

  set shift_index(value: number) {
    this._shiftIndex = value;
  }
}
export class Shift {
  constructor(
    private _name: string,
    private _positions: Array<any>,
    private _from: string,
    private _to: string,
    private _active: boolean,
    private _hours: any,
    private _bubbles: any,
    private _shiftDayStatus: Array<string>,
    private _shiftUnpublish: number,
    private _shiftInfo: ShiftInfo
  ) {
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get positions(): Array<any> {
    return this._positions;
  }

  set positions(value: Array<any>) {
    this._positions = value;
  }

  get from(): string {
    return this._from;
  }

  set from(value: string) {
    this._from = value;
  }

  get to(): string {
    return this._to;
  }

  set to(value: string) {
    this._to = value;
  }

  get active(): boolean {
    return this._active;
  }

  set active(value: boolean) {
    this._active = value;
  }

  get hours(): any {
    return this._hours;
  }

  set hours(value: any) {
    this._hours = value;
  }

  get bubbles(): any {
    return this._bubbles;
  }

  set bubbles(value: any) {
    this._bubbles = value;
  }

  get shiftDayStatus(): Array<string> {
    return this._shiftDayStatus;
  }

  set shiftDayStatus(value: Array<string>) {
    this._shiftDayStatus = value;
  }

  get shiftUnpublish(): number {
    return this._shiftUnpublish;
  }

  set shiftUnpublish(value: number) {
    this._shiftUnpublish = value;
  }

  get shiftInfo(): ShiftInfo {
    return this._shiftInfo;
  }

  set shiftInfo(value: ShiftInfo) {
    this._shiftInfo = value;
  }
}

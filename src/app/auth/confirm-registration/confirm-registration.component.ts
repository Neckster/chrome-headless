import {
  Component,
  Input }                           from '@angular/core';
import { AuthService }              from '../../services/shared/auth.service';

import {
  FormBuilder,
  FormGroup,
  Validators }                      from '@angular/forms';

import { JubbrFormValidator }       from '../../lib/validators/jubbr-validator';
import { htmlRegistrationConfirm }  from './confirm-registration.html';

import { ModalApiService }          from '../../services/shared/modal';

import { LOGIN_MD }                 from '../../auth/log-in/log-in.components';
import { PASS_INFO_MD }             from '../../auth/password-info/password-info.component';
import { AuthModal }                from '../auth.modal';

export const CONFIRM_REG_MD: string = 'md_confirmReg';

@Component({
  selector: 'confirm-registration',
  template: htmlRegistrationConfirm
})
export class ConfirmRegComponent extends AuthModal {

  @Input() state;
  @Input() email;
  @Input() tokenInput;

  protected modalId = CONFIRM_REG_MD;

  private confirmRefForm: FormGroup;
  private isSending: boolean = false;
  private generalError: boolean = false;

  constructor(public auth: AuthService, private fb: FormBuilder,
              private modalApi: ModalApiService) {

    super();

    this.confirmRefForm = this.fb.group({
      password: ['', Validators.compose([
        Validators.required
      ])],
      password2: ['', Validators.compose([
        Validators.required
      ])],
      token: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ])]
    }, { validator: JubbrFormValidator.equals('password', 'password2') });

    this.confirmRefForm.valueChanges.subscribe((change) => {
      this.generalError = false;
    });

  }

  ngOnInit() {
    /* tslint:disable:no-string-literal */
    if (this.tokenInput) {
      this.confirmRefForm.patchValue({ token: this.tokenInput });
      this.confirmRefForm.controls['token'].markAsDirty();
    }
    /* tsslint:enable:no-string-literal */
  }

  onIdle() {}

  public confirm(e) {
    e.preventDefault();
    this.isSending = true;
    this.auth.updatePassword(
      this.confirmRefForm.value.token,
      this.confirmRefForm.value.password,
      this.confirmRefForm.value.password2
    ).subscribe(() => {
      this.isSending = false;
      this.modalApi.close(this.modalId);
      this.modalApi.open(PASS_INFO_MD);
    }, () => {
      this.generalError = true;
      this.isSending = false;
    });
  }

  public onLoginClick(event) {
    event.preventDefault();
    this.modalApi.close(this.modalId);
    this.modalApi.open(LOGIN_MD);
  }

}

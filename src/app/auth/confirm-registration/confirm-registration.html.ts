export const htmlRegistrationConfirm:string = `
<div id="modal" *ngIf="opened">
  <section class="uk-modal uk-open" id="confirm" tabindex="-1" role="dialog" aria-labelledby="label-confirm" aria-hidden="false" style="display: block; overflow-y: scroll;">
      <div class="uk-modal-dialog">
        <!--<a class="uk-modal-close uk-close" (click)=""  data-close="dismiss" data-dismiss="modal">-->
          <!-- icon -->
        <!--</a>-->
        
        <div class="uk-modal-header" id="label-confirm" [ngSwitch]="state">
       
        <template  [ngSwitchCase]="'confirm'">
          <h2>{{ 'Confirmation_sent' | translate }}</h2>
        </template>
        
        <template  [ngSwitchCase]="'reset'">
          <h2>
            <span>{{ 'Set_password' | translate }}</span>
          </h2>
        </template>
                  
        </div>
        
        <div class="modal-content">
          <div *ngIf="state === 'reset'">
            <div class="uk-alert uk-alert-success error ok" data-uk-alert="">
              <a class="uk-alert-close uk-close uk-width-1-1"></a>{{ 'Token_sent' | translate }}
              <span class="user-email">{{email}}.</span>          
            </div>
            <div class="passwordsent">
                <p>{{'Enter_token' | translate }}</p>
            </div>
          </div>
        
          <div *ngIf="state === 'confirm'" class="">
            <p *ngIf="email">{{ 'Confirmation_code_sent' | translate }}
              <span class="user-email">{{email}}.</span>
            </p>
            <p>{{ 'Enter_activation_code' | translate }}</p>
          </div>
          
          <div class="user_login">
            <form class="uk-form activate" (submit)="confirm($event)" [formGroup]="confirmRefForm">
              <div class="uk-form-row">
                <input class="uk-width-1-1" 
                       [ngClass]="{'uk-form-danger': confirmRefForm.controls.token.dirty && !confirmRefForm.controls.token.valid}"
                       type="text"
                       name="token"
                     
                       placeholder='{{ "placeholder_Token" | translate }}'
                       aria-describedby="errorbox-1"
                       formControlName="token">
       
                <div *ngIf="confirmRefForm.controls.token.dirty && !confirmRefForm.controls.token.valid" class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{ 'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
             
              <div class="uk-form-row">
                <input class="uk-width-1-1" 
                       [ngClass]="{'uk-form-danger': confirmRefForm.controls.password.dirty && !confirmRefForm.controls.password.valid}"
                       type="password" 
                       name="password" 
                       placeholder='{{ "placeholder_Password" | translate }}'
                       aria-describedby="errorbox-2"
                       formControlName="password">
                       
                <div *ngIf="confirmRefForm.controls.password.dirty && !confirmRefForm.controls.password.valid" class="uk-alert uk-alert-warning" id="errorbox-2">
                  <p class="ws-errormessage">{{ 'Please_fill_out_this_field' | translate}}.</p>
                 </div>
              </div>
             
              <div class="uk-form-row">
           
                <input class="uk-width-1-1" 
                       [ngClass]="{'uk-form-danger': confirmRefForm.controls.password2.dirty && !confirmRefForm.controls.password2.valid}"
                       type="password" 
                       name="password2" 
                       placeholder='{{ "placeholder_Password_confirmation" | translate }}' 
                       aria-describedby="errorbox-3"
                       formControlName="password2">
                 
                  <div *ngIf="confirmRefForm.controls.password2.dirty && !confirmRefForm.controls.password2.valid" class="uk-alert uk-alert-warning" id="errorbox-3">
                    <p class="ws-errormessage">{{ 'Please_fill_out_this_field' | translate}}.</p>
                  </div>
              </div>
             
              <div class="uk-form-row uk-width-1-1 action-buttons">
                <div class="uk-alert uk-alert-warning error missing hidden" data-uk-alert="">{{ 'msg_err_missing_password' | translate }}</div>
                <div class="uk-alert uk-alert-warning error password2" [ngClass]="{'hidden': !confirmRefForm.value.password.length || !confirmRefForm.value.password2.length || !confirmRefForm.errors?.fieldsNotEqual}" data-uk-alert="">{{'msg_err_invalid_password2' | translate}}</div>
                <div class="uk-alert uk-alert-danger error internal" [ngClass]="{'hidden': !generalError}" data-uk-alert="">{{ 'msg_err_internal' | translate }}</div>
                <button type="submit" [disabled]="!confirmRefForm.valid" class="uk-button ok uk-width-1-1">{{'Set_password' | translate}}</button>
              </div>
              
            </form>
          
          </div>
          <div *ngIf="state === 'reset'" class="uk-grid uk-margin-remove">
            <a href="#" (click)="onLoginClick($event)" class="back text-button uk-width-1-1 app-modal-login">{{ 'back_to_login' | translate}}</a>
          </div>
        </div>
      </div>
    </section>
  </div>
  `;

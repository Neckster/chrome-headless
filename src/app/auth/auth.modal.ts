import { select }   from 'ng2-redux/lib/index';

export abstract class AuthModal {

  @select(state => state.openedModals) modals$: any;

  protected modalId: string;
  private opened: boolean = false;

  constructor() {

    this.modals$.subscribe(ids => {

      this.opened = ids.includes(this.modalId);

      if (!this.opened) {
        this.onIdle();
      }

    });
  }

  public abstract onIdle(): void;

}

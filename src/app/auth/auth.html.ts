export const authTemplate = `
  <login></login>
  <signup (closeEmitter)="handleModal($event)"></signup>
  <new-password (closeEmitter)="handleModal($event)"></new-password>
  <confirm-registration 
    [state]="state" 
    [email]="newUserEmail" 
    [tokenInput]="newUserToken"></confirm-registration>
  <password-info></password-info>
`;

export const htmlLogIn:string = `
<div id="modal" *ngIf="opened">

<section class="uk-open uk-modal" id="login" tabindex="-1" role="dialog" aria-labelledby="label-login" aria-hidden="false" style="display: block; overflow-y: scroll;">
      <div class="uk-modal-dialog">
       <!--<a class="uk-modal-close uk-close" (click)="closeLoginModal($event)"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">-->
          <!-- icon -->
        <!--</a>-->
        <div class="uk-modal-header" id="label-login">
          <h2>
            <span>{{ 'Login' | translate }}</span>
          </h2>
        </div>
        <div class="modal-content">
          <div class="user-login">
            <form class="uk-form login" (submit)="login($event)" novalidate ngNoForm>
              <div class="uk-form-row">
                <input class="uk-width-1-1" 
                       type="email" 
                       id="loginemail" 
                       placeholder='{{ "placeholder_Email" | translate }}'
                       [(ngModel)]="credentials.username" 
                       (ngModelChange)="inputChange()" 
                       required="" autofocus >
                
                <div *ngIf="errors.nousername || errors.invaliduname" class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p *ngIf="errors.nousername" class="ws-errormessage">{{ 'msg_err_missing_email'|translate }}</p>
                  <p *ngIf="errors.invaliduname" class="ws-errormessage">{{'include_an_at'|translate}}</p>
                </div>
                
              </div>
              
              <div class="uk-form-row">
                <input class="uk-width-1-1 user-success" 
                       type="password" 
                       id="loginpassword"  
                       placeholder='{{ "placeholder_Password" | translate }}'
                       [(ngModel)]="credentials.password" 
                       (ngModelChange)="inputChange()" 
                       required="">
             
                <div *ngIf="errors.nopassword" class="uk-alert uk-alert-warning" id="errorbox-2">
                  <p class="ws-errormessage">{{ 'Please_fill_out_this_field' | translate }}</p>
                </div>
                
             </div>
             
              <div class="uk-form-row">
                <label class="uk-margin-top" for="loginkillsessions">
                  <input type="checkbox" id="loginkillsessions">
                  <span>{{ 'Force_session_clean' | translate }}</span>
                </label>
                <!--label for="loginbindip">
                <input type="checkbox"
                       id="loginbindip"
                       checked>
                <span>{{i18n Bind_session_IP}}</span>
              </label><br-->
              </div>
              <div class="uk-form-row uk-width-1-1 action-buttons">
                <div class="uk-alert uk-alert-warning error unknown" [ngClass]="{hidden: !errors['user_unknown']}" data-uk-alert="">{{ 'msg_err_invalid_auth' | translate }}</div>
                <div class="uk-alert uk-alert-warning error missing" [ngClass]="{hidden: !errors['field_missing']}" data-uk-alert="">{{'msg_err_missing_auth' | translate }}</div>
                <div class="uk-alert uk-alert-danger error internal" [ngClass]="{hidden: !errors['internal_error']}" data-uk-alert="">{{ 'msg_err_internal' | translate }}</div>
                <button type="submit"  [disabled]="isSending"  class="uk-width-1-1 uk-button ok">{{ 'Login' | translate }}</button>
                <div class="uk-grid uk-grid-collapse uk-width-1-1">
                  <div class="uk-width-1-1 uk-width-medium-4-10 uk-text-nowrap">
                    <a href="#passwordlink" class="text-button next app-modal-passwordlink" (click)="onForgotClick($event)">{{ 'Forgot_password' | translate }}</a>
                  </div>
                  <div class="uk-width-1-1 uk-width-medium-6-10 uk-text-nowrap">
                    <a (click)="onRegClick($event)" class="text-button next uk-align-medium-right app-modal-enroll">{{ 'Enroll_new_user' | translate }}</a>
                  </div>
                </div>
              </div></form>


          </div>
          <!-- modal-content -->
        </div>
        <!-- modal-inner -->
      </div>
    </section>
 </div>
`;

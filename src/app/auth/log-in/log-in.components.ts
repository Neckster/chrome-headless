import { Component }                from '@angular/core';
import { Router }                   from '@angular/router';
import {
  Control,
  Location }                        from '@angular/common';

import { select }                   from 'ng2-redux/lib/index';
import { AppStoreAction }           from '../../store/action';

import { AuthService }              from '../../services/shared/auth.service';
import { InfoService }              from '../../services/info.service';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { ModalApiService }          from '../../services/shared/modal';

import { ILoginError }              from '../../lib/interfaces/shared/login-error';
import { JubbrFormValidator }       from '../../lib/validators/jubbr-validator';

import { htmlLogIn }                from './log-in.html.ts';

import { SIGNUP_MD }                from '../sign-up/sign-up.component';
import { NEW_PASS_MD }              from '../new-password/new-password.component';

import { AuthModal }                from '../auth.modal';
import { UserProfile }              from '../../lib/interfaces/user/profile';

interface ILoginCredentials {
  username?: string;
  password?: string;
}

export const LOGIN_MD: string = 'md_login';

@Component({
  selector: 'login',
  template: htmlLogIn
})
export class LoginComponent extends AuthModal {

  @select() info$: any;

  public credentials: ILoginCredentials = {
    username: '',
    password: ''
  };

  public un;
  public testInfo;
  public errors: ILoginError = <ILoginError>{};
  public isSending: boolean = false;

  protected modalId: string = LOGIN_MD;

  constructor(public auth: AuthService, public info: InfoService,
              private router: Router, private modalApi: ModalApiService,
              private location: Location,
              private action: AppStoreAction
  ) {

    super();

    this.errors.valid = true;

    this.un = this.info$.subscribe(inf => {
      this.testInfo = inf;
    });

  }

  ngOnDestroy() {
    this.un.unsubscribe();
  }

  onIdle() {
    this.credentials = <ILoginCredentials>{};
    this.inputChange();
  }

  public login(e) {
    ParseOrganisationService.currUnitName = undefined;
    e.preventDefault();

    // todo rewrite this:
    if (!this.credentials.username
      || !this.credentials.password
      || JubbrFormValidator.mail(<Control>{value: this.credentials.username})) {

      if (!this.credentials.username) {
        this.errors.nousername = true;
      }
      if (!this.credentials.password) {
        this.errors.nopassword = true;
      }

      if (JubbrFormValidator.mail(<Control>{value: this.credentials.username})) {
        this.errors.invaliduname = true;
      }

      this.isSending = false;
      this.errors.valid = false;
      return;
    }
    let auth = this.auth.login(this.credentials.username, this.credentials.password);

    auth.subscribe((res: { user: UserProfile }) => {
      let a = this.testInfo;
      this.isSending = false;
      this.modalApi.close(this.modalId);
      this.auth.activateNodeSession(res.user.webuser_id)
        .subscribe((status) => {
          this.action.fetchAuthStatus();
          this.router.navigateByUrl('/news');

        });
      // this.router.navigateByUrl('/onboarding');
    }, (err) => {
      this.isSending = false;
      this.errors.valid = false;
      if (err.status === 401) {
        this.errors.user_unknown = true;
        this.credentials.password = '';
      } else {
        this.errors.internal_error = true;
      }
    });
  }

  public inputChange() {
    this.errors = <ILoginError>{};
    this.errors.valid = true;
  }

  onRegClick(event): void {
    event.preventDefault();
    this.location.go('/auth/signup');
    this.modalApi.close(this.modalId);
    this.modalApi.open(SIGNUP_MD);
  }

  onForgotClick(event): void {
    event.preventDefault();
    this.modalApi.close(this.modalId);
    this.modalApi.open(NEW_PASS_MD);
  }

}

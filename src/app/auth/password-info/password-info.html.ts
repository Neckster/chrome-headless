export const htmlPasswordInfo:string = `
<div id="modal" *ngIf="opened">
  <section class="uk-modal uk-open" id="passwordnew" tabindex="-1" role="dialog" aria-labelledby="label-passwordnew" aria-hidden="false" style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog">
    <!--<a class="modal-close uk-modal-close uk-close" (click)="closeModalPassInfo($event)" data-close="dismiss" data-dismiss="modal">-->
    <!-- icon -->
    <!--</a>-->
    <div class="uk-modal-header" id="label-passwordnew">
      <h2>
        {{ 'Password_set' | translate}}
      </h2>
      </div>
      <div class="modal-content">
        <div class="passwordnew">
          <p>{{'Password_reset' | translate}}</p>
          <div class="uk-form-row uk-width-1-1 action-buttons">
            <div class="uk-grid uk-margin-remove">
              <a href="#login" (click)="onLoginClick($event)" class="back text-button uk-width-1-1 app-modal-login">{{ 'back_to_login'| translate}}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
`;

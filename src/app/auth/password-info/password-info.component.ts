import { Component }          from '@angular/core';

import { ModalApiService }    from '../../services/shared/modal';

import { htmlPasswordInfo }   from './password-info.html';

import { LOGIN_MD }           from '../../auth/log-in/log-in.components';
import { AuthModal }          from '../auth.modal';

export const PASS_INFO_MD: string = 'md_passInfo';

@Component({
  selector: 'password-info',
  template: htmlPasswordInfo
})
export class PasswordInfoComponent extends AuthModal {

  protected modalId = PASS_INFO_MD;

  constructor(private modalApi: ModalApiService) {
    super();
  }

  onLoginClick(event) {
    event.preventDefault();
    this.modalApi.close(this.modalId);
    this.modalApi.open(LOGIN_MD);
  }

  onIdle() {}

}

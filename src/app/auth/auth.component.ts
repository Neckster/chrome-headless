// Core imports:
import { Component }              from '@angular/core';
import { ActivatedRoute }         from '@angular/router';

// Services:
import { JobService }             from '../services/shared/JobService';
import { ModalApiService }        from '../services/shared/modal';

// Template:
import { authTemplate }           from './auth.html';

interface IEvent {
  name?: any;
  payload?: any;
  email?: string;
}

@Component({
  styles: [ ],
  template: authTemplate
})
export class AuthComponent {

  private state;
  private hideScrollBar = this.jobService.hideScrollBar;
  private newUserEmail = '';
  private newUserToken = '';

  constructor(private jobService: JobService, private modalApi: ModalApiService,
              private route: ActivatedRoute) {
    console.log('CREATE');
  }

  ngOnInit() {
console.log('INIT');
    this.route.params.subscribe(({ authStrategy = 'login' } = {}) => {
      console.log('STAT', authStrategy);

      const strategyUrlMapper = {
        'login': 'md_login',
        'signup': 'md_signup',
        'confirm': 'md_confirmReg'
      };

      const modalName = strategyUrlMapper[authStrategy] || strategyUrlMapper.login;
      this.modalApi.open(modalName);

    });

    const token = this.route
      .queryParams
      .map((params: any) => params.token || undefined)
      .subscribe(tokenVal => {
        if (tokenVal) {
          this.newUserToken = tokenVal;
          this.state = 'confirm';
          this.modalApi.close();
          this.modalApi.open('md_confirmReg');
        }
      });
  }

  handleModal(event: IEvent) {
    this.newUserEmail = event.email;
    switch (event.name) {
        case 'OPEN_CONF':
          this.state = 'confirm';
          break;
        case 'OPEN_RESET':
          this.state = 'reset';
          break;
        default: // empty
     }
  }
}

import {
  Component,
  Output,
  EventEmitter }              from '@angular/core';

import { Control }            from '@angular/common';

import { AuthService }        from '../../services/shared/auth.service';
import { ModalApiService }    from '../../services/shared/modal';

import { JubbrFormValidator } from '../../lib/validators/jubbr-validator';

import { htmlNewPassword }    from './new-password.html';

import { CONFIRM_REG_MD }     from '../confirm-registration/confirm-registration.component';
import { LOGIN_MD }           from '../log-in/log-in.components';
import { AuthModal }          from '../auth.modal';

interface IError {
  'user_unknown'?: boolean;
  'email_missing'?: boolean;
  'general'?: boolean;
  'valid'?: boolean;
}

export const NEW_PASS_MD = 'md_newPassword';

@Component({
  selector: 'new-password',
  template: htmlNewPassword
})

export class NewPasswordComponent extends AuthModal {

  @Output() closeEmitter  = new EventEmitter();

  public email = '';

  public errors: IError = {
    'user_unknown': false,
    'email_missing': false,
    'general': false,
    'valid': true
  };

  public isSending: boolean = false;

  protected modalId = NEW_PASS_MD;

  constructor(public auth: AuthService, private modalApi: ModalApiService) {
    super();
  }

  onIdle() {
    this.inputChange();
    this.resetEmailModel();
  }

  sendNewToken(e) {

    e.preventDefault();
    this.isSending = true;
    this.errors = <IError>{};

    if (!this.email
      || !this.email.trim().length
      || JubbrFormValidator.mail(<Control>{value: this.email})) {

      if (!this.email) {
        this.errors.email_missing = true;
      }

      if (!this.email.trim().length) {
        this.errors.email_missing = true;
      }

      this.email = this.email.trim();

      if (JubbrFormValidator.mail(<Control>{value: this.email})) {
        this.errors.email_missing = true;
      }
      this.isSending = false;
      this.errors.valid = false;
      return false;
    }

    this.auth.reactivateEmail(this.email).subscribe((res) => {
      this.isSending = false;
      this.closeEmitter.emit({ name: 'OPEN_RESET', email: this.email });
      this.modalApi.close(this.modalId);
      this.modalApi.open(CONFIRM_REG_MD);
    }, (err) => {
      this.isSending = false;
      if (err.status === 404) {
        this.errors.user_unknown = true;
      } else {
        this.errors.general = true;
      }
    });

  }

  inputChange(event?) {
    this.errors = <IError>{};
  }

  resetEmailModel(): void {
    this.email = '';
  }

  onLoginClick(event) {
    event.preventDefault();
    this.modalApi.close(this.modalId);
    this.modalApi.open(LOGIN_MD);
  }

}

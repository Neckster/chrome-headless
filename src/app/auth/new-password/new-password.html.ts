export const htmlNewPassword:string = `
<div id="modal" *ngIf="opened">
<section class="uk-modal uk-open" id="passwordlink" tabindex="-1" role="dialog" aria-labelledby="label-passwordlink" aria-hidden="false" style="display: block; overflow-y: scroll;">
      <div class="uk-modal-dialog">
        <!--<a class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal" (click)="closeNewPassModal()">-->
          <!-- icon -->
        <!--</a>-->
        <div class="uk-modal-header" id="label-passwordlink">
          <h2>
            <span>{{ 'Forgot_password' | translate }}</span>
          </h2>
        </div>
        <div class="modal-content">
          <div class="passwordlink">
            <form class="uk-form passwordlink" (submit)="sendNewToken($event)" novalidate ngNoForm>
              <div class="uk-form-row">
                <input class="uk-width-1-1" 
                       type="email" 
                       id="passwordlinkemail" 
                       placeholder='{{ "placeholder_Email" | translate }}'
                       [(ngModel)]="email"
                       (ngModelChange)="inputChange($event)">
              </div>
              <div class="uk-form-row uk-width-1-1 action-buttons">
                <div *ngIf="errors['email_missing']" class="uk-alert uk-alert-warning error missing" data-uk-alert="">
                  {{ 'msg_err_missing_email' | translate}}.
                </div>
                
                <div *ngIf="errors['user_unknown']" class="uk-alert uk-alert-warning error unknown" data-uk-alert="">
                 {{ 'msg_err_unknown_email' | translate}}
                </div>
                
                <div class="uk-alert uk-alert-warning error token hidden" data-uk-alert="">
                  Der Code ist ungültig.
                  <br>Bitte lass dir einen neuen Code zuschicken.
                </div>
                
                <div *ngIf="errors['general']" class="uk-alert uk-alert-danger error internal" data-uk-alert="">
                 {{ 'msg_err_internal' | translate }}
                </div>
                
                <button type="submit" class="uk-button ok uk-width-1-1">{{ 'Send_password' | translate }}</button>
                <div class="uk-grid uk-margin-remove">
                  <a class="back text-button uk-width-1-1 app-modal-login" (click)="onLoginClick($event)">{{'back_to_login' | translate}}</a>
                </div>
              </div></form>


          </div>
        </div>
      </div>
    </section>
</div>
`;

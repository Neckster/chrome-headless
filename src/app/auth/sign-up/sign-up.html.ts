export const htmlSignUp:string = `
  <div id="modal" *ngIf="opened">
  <section class="uk-modal uk-open" id="enroll" tabindex="-1" role="dialog" aria-labelledby="label-enroll" aria-hidden="false" style="display: block; overflow-y: scroll;">
  <!--<div class="modal-overlay" style="" (click)="closeEmitter.emit({name: 'CLOSE_ALL'})"></div>-->
      <div class="uk-modal-dialog">
        <!--<a class="modal-close uk-modal-close uk-close" (click)="closeSignModel($event);"  data-close="dismiss" data-dismiss="modal">-->
          <!-- icon -->
        <!--</a>-->
        <div class="uk-modal-header" id="label-enroll">
          <h2>
            <span>{{ 'Enroll' | translate}}</span>
          </h2>
        </div>
        <div class="modal-content">
          <form *ngIf="!isResetting" class="uk-form enroll" [formGroup]="signupForm" novalidate (submit)="formSubmit($event)">
            <div class="user-login">
              <div class="uk-form-row">
              
                <input [ngClass]="{'uk-form-danger': signupForm.controls.firstname.dirty && !signupForm.controls.firstname.valid }" 
                       class="uk-width-1-1" 
                       type="text" 
                       id="enrollfirstname" 
                       placeholder='{{ "Firstname" | translate }}' 
                       required=""  
                       aria-describedby="errorbox-1" 
                       formControlName="firstname">
                <div *ngIf="!signupForm.controls.firstname.valid && (signupForm.controls.firstname.errors || signupForm.controls.firstname.errors?.required) && wasSubmitted" class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field'|translate}} correctly.</p>
                </div>
              </div>
                
              <div class="uk-form-row">
                <input [ngClass]="{'uk-form-danger': signupForm.controls.lastname.dirty && !signupForm.controls.lastname.valid }" 
                       class="uk-width-1-1" 
                       type="text" 
                       id="enrolllastname" 
                       placeholder='{{ "Lastname" | translate }}'
                       required="" 
                       aria-describedby="errorbox-2" 
                       formControlName="lastname">
                <div *ngIf="!signupForm.controls.lastname.valid && (signupForm.controls.lastname.errors || signupForm.controls.lastname.errors?.required) && wasSubmitted" class="uk-alert uk-alert-warning" id="errorbox-2">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field'|translate}} correctly.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <input [ngClass]="{'uk-form-danger': signupForm.controls.email.dirty && !signupForm.controls.email.valid }" 
                       class="uk-width-1-1" 
                       type="email" 
                       id="enrollemail" 
                       placeholder='{{ "placeholder_Email" | translate }}'
                       required="" 
                       aria-describedby="errorbox-3" 
                       formControlName="email">
                <div *ngIf="!signupForm.controls.email.valid && (signupForm.controls.email.errors || signupForm.controls.email.errors?.required) && wasSubmitted" class="uk-alert uk-alert-warning" id="errorbox-3">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row legalize">
                <label for="enrolllegalize">
                  <input type="checkbox" 
                         id="enrolllegalize" 
                         required="" 
                         class="uk-form-danger" 
                         aria-describedby="errorbox-4" 
                         formControlName="legalize">
                  <span>
                    <a href="datenschutz" target="_blank">{{ 'Privacy_statement' | translate }}</a> 
                    <span>and</span> 
                    <a href="agb" target="_blank">{{ 'Terms' | translate }} </a> 
                    <span>{{ 'read_and_accepted' | translate}}.</span>
                  </span>
                </label>
                
                <div *ngIf="!signupForm.controls.legalize.valid && wasSubmitted" 
                     class="uk-alert uk-alert-warning" 
                     id="errorbox-4" 
                     style="display: block;">
                  <p class="ws-errormessage">{{'msg_info_check_this_box' | translate}}</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label for="enrolltypejobber">
                  <input type="radio" 
                         id="enrolltypejobber" 
                         name="type" 
                         value="jobber" 
                         class="" 
                         formControlName="type">
                  <span>{{ 'choice_enroll_jobber' | translate }}</span>
                </label>


                <label for="enrolltypecompany">
                  <input type="radio" 
                         id="enrolltypecompany" 
                         name="type" 
                         value="company" 
                         class="" 
                         formControlName="type">
                  <span>{{ 'choice_enroll_company' | translate }}</span>
                </label>
              </div>
              <div class="uk-form-row uk-width-1-1 action-buttons">
                <div [ngClass]="{hidden: !invalidEmailError}" class="uk-alert uk-alert-warning error enrollemail invalid" data-uk-alert="">{{ 'msg_err_invalid_email'| translate}}</div>
                <div [ngClass]="{hidden: !inuseEmailError}" class="uk-alert uk-alert-warning error enrollemail inuse " data-uk-alert="">{{ 'msg_err_invalid_email_inuse'| translate}}</div>
                <div class="uk-alert uk-alert-warning error enrolllegalize hidden" data-uk-alert="">{{ 'msg_err_missing_legalize' | translate}}</div>
                <div [ngClass]="{hidden: !generalError}" class="uk-alert uk-alert-danger error internal" data-uk-alert=""> {{ 'msg_err_internal'| translate }}</div>
                <button type="submit" id="enroll_form" class="uk-button ok uk-width-1-1"> {{ 'Sign_up_now' | translate }}</button>
              </div>
            </div></form>


        </div>
        <!-- modal-content -->
      </div>
      <!-- modal-inner -->
    </section>
  </div>
  `;

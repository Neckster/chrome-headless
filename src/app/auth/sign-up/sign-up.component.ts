import {
  Component,
  Input,
  Output,
  EventEmitter }              from '@angular/core';

import {
  FormBuilder,
  FormGroup,
  Validators }                from '@angular/forms';

import { AuthService }        from '../../services/shared/auth.service';
import { InfoService }        from '../../services/info.service';
import { ModalApiService }    from '../../services/shared/modal';

import { ILoginError }        from '../../lib/interfaces/shared/login-error';
import { JubbrFormValidator } from '../../lib/validators/jubbr-validator';

import { htmlSignUp }         from './sign-up.html.ts';

import { CONFIRM_REG_MD }     from '../confirm-registration/confirm-registration.component';
import { AuthModal }          from '../auth.modal';

interface ISignupCredentials {
  firstname?: string;
  lastname?: string;
  email?: string;
  type?: string;
  legalize?: boolean;
}

export const SIGNUP_MD = 'md_signup';

@Component({
  selector: 'signup',
  template: htmlSignUp
})
export class SignupComponent extends AuthModal {

  @Input() isResetting = false;
  @Output() closeEmitter = new EventEmitter();

  signupForm: FormGroup;

  public errors: ILoginError = <ILoginError>{};
  public isSending: boolean = false;
  public wasSubmitted: boolean = false;

  protected modalId: string = SIGNUP_MD;
  private signup: ISignupCredentials = {
    firstname: undefined,
    lastname: undefined,
    email: undefined,
    type: undefined,
    legalize: undefined
  };

  private invalidEmailError = false;
  private inuseEmailError = false;
  private generalError = false;

  constructor(public auth: AuthService, public info: InfoService,
              private fb: FormBuilder, private modalApi: ModalApiService) {
    super();
  }

  ngOnInit() {
    if (!this.signupForm) {
      this._buildForm();
    }
  }

  onIdle() {
    this.signup = <ISignupCredentials>{};
    this.onReset();
  }

  onReset() {
    this._buildForm();
    this.isResetting = true;
    this.wasSubmitted = false;
    setTimeout(() => this.isResetting = false, 0);
    return false;
  }

  public formSubmit(e) {

    e.preventDefault();
    this.isSending = true;
    this.wasSubmitted = true;

    if (!this.signupForm.valid) {
      this.isSending = false;
      return false;
    }

    this.auth.enroll(this.signupForm.value).subscribe((res) => {
      this.modalApi.close(this.modalId);
      this.modalApi.open(CONFIRM_REG_MD);
      this.closeEmitter.emit({ name: 'OPEN_CONF', email: this.signupForm.value.email });
    }, (err) => {
      let errObj = JSON.parse(err._body);
      if (errObj.invalid && 'email' in errObj.invalid) {
        if (errObj.invalid.email === 'format') {
          this.invalidEmailError = true;
        } else if (errObj.invalid.email === 'duplicate') {
          this.inuseEmailError = true;
        } else {
          this.generalError = true;
        }
      } else {
        this.generalError = true;
      }
    });
  }

  private _buildForm(): void {
    if (this.fb) {
      this.signupForm = this.fb.group({

        firstname: ['',
          Validators.compose([
            Validators.required,
            Validators.minLength(4)
          ])
        ],
        lastname: ['',
          Validators.compose([
            Validators.required,
            Validators.minLength(4)
          ])
        ],
        email: ['',
          Validators.compose([
            Validators.required,
            Validators.minLength(5),
            JubbrFormValidator.mail as any
          ])
        ],
        type: ['jobber'],
        legalize: ['', JubbrFormValidator.mandatory]
      });

      this.signupForm.valueChanges.subscribe(function(a) {
        this.invalidEmailError = false;
        this.inuseEmailError = false;
        this.generalError = false;
        this.errors = <ILoginError>{};
        this.errors.valid = true;
        this.wasSubmitted = false;
      }.bind(this));
    }
  }

  private _validateForm() {
    if (this.signupForm)
      return this.signupForm.errors;
  }

}

export function Mixin(value: Function[]) {
  return function (target: Function) {
    value.forEach(v => {
      Object.getOwnPropertyNames(v.prototype).forEach(name => {
        target.prototype[name] = v.prototype[name];
      });
    });
    Reflect.defineMetadata('Mixin', value, target);
  };
}

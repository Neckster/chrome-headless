export interface ILoginError {
  'user_unknown': boolean;
  'field_missing': boolean;
  'internal_error': boolean;
  'valid': boolean;
  'nousername': boolean;
  'nopassword': boolean;
  'invaliduname': boolean;
}

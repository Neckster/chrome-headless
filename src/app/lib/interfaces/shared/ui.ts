export interface IUICaption {
  name: string;
  caption: string;
  value?: string | number;
};

export interface IUITab extends IUICaption {
  active: boolean;
};

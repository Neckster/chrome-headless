export interface UserInvite {
  webuser_id: number;
  firstname: string;
  lastname: string;
  email: string;
  home_position_id: number;
  shift_0: boolean;
  shift_1: boolean;
  shift_2: boolean;
  shift_3: boolean;
  manager_unit_id: number;
  jumper_unit_id: number;
  sendmail: boolean;
  mailmessage: string;
}

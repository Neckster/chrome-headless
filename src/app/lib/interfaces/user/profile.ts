export interface UserProfile {
  // mandatory
  // FIXME: temporary fix, will need rewrite
  webuser_id?: number;
  umail?: string;
  mailhash?: string;
  pushhash?: string;
  // optional
  firstname?: string;
  lastname?: string;
  cellphone?: string;
  street?: string;
  zip?: string;
  location?: string;
  country?: string;
  birthdate?: string;
  iban?: string;
  socialid?: string;
  skills?: any;
  ts_registry?: number;
  // FIXME: the following properties are per-user-per-company
  // TODO: create UserCompanyConnection interface with these
  // three props and company_id as key, use an array of
  // UserCompanyConnection here
  home_position_id?: number;
  jumper_unit_id?: number;
  manager_unit_id?: number;
}

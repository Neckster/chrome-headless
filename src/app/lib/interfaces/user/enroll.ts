export interface UserEnroll {
  firstname: string;
  lastname: string;
  email: string;
  type: string;
  legalize: boolean;
}

export interface IMessage {
  message: string;
  title?: string;
  read?: boolean;
  valid_from: number;
  webuser_id?: number | string;
  company_id?: number;
  unit_id?: number | string;
  reach?: string;
}

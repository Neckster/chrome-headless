import { Pipe } from '@angular/core';

@Pipe({name: 'mapToIterable'})
export class MapToIterable {
  transform(value) {

    if (!value) return;

    let result = [];
    let key;
    let val;
    if (value.entries) {
      for ([key, val] of value.entries()) {
        result.push({key, val});
      }
    } else {
      for (key in value) {
        if (value.hasOwnProperty(key)) {
          result.push({key, value: value[key]});
        }
      }
    }
    return result;
  }
}

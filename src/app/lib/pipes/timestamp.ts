import { Pipe }     from '@angular/core';
import * as moment  from 'moment';

@Pipe({name: 'timestamp'})
export class TimestampPipe {
  transform(value: any, loc = 'en', format = 'ddd: D.MM') {

    if (undefined === value) {
      return undefined;
    }

    const date = (value / (3600 * 24 * 365) > 1000
      ? moment(value)
      : moment.unix(value)).lang(loc).format(format);

    return date;

  }
}

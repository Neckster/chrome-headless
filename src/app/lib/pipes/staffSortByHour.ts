import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as moment                   from 'moment';

@Pipe({
  name: 'sortStaffByHours'
})
export class SortStaffByHoursPipe implements PipeTransform {
  transform( value: any): any {
    // let correct = value.every( el => el.value.current);
    let result = value.sort(sortShifts);

    function convertTime(t) {
      let tFromHours = t.from.split(':')[0],
          tFromMin = t.from.split(':')[1];
      return moment().hour(tFromHours).minute(tFromMin);
    }

    let sortShift = (x: any, y: any): any => {
      let X = convertTime(x),
          Y = convertTime(y);
      return X.isAfter(Y);
    };

    function sortShifts(a: any, b: any): any {
      let A, B;

      if (a.value.current) {
        let aShift = a.value.current.sort(sortShift)[0];
        A = convertTime(aShift);
      }else if (a.value.prev) {
        A = moment('00:00:00:00');
      }else if (a.value.next) {
        A = moment('23:59:59:59');
      }

      if (b.value.current) {
        let bShift = b.value.current.sort(sortShift)[0];
        B = convertTime(bShift);
      }else if (b.value.prev) {
        B = moment('00:00:00:00');
      }else if (b.value.next) {
        B = moment('23:59:59:59');
      }

      return A.isAfter(B);
    }

    return result;

  }
}

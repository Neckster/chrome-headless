import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textCut'
})
export class TextCutPipe implements PipeTransform {
  transform( value: any, args): any {
  return value && value.length > args ?
    value.slice(0, args) + '...' : value;
  }
}

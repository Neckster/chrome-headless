import { Pipe } from '@angular/core';

@Pipe({name: 'later', pure: false})
export class GreaterPipe {
  transform(value: any, args) {
    const outerArgs = Array.from(arguments);
    if (undefined === value) {
      return undefined;
    }
    return value.filter((item) => {
      return new Date(item[outerArgs[1]] * 1000) >= new Date(outerArgs[2]);
    });
  }
}

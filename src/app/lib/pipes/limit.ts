import { Pipe }     from '@angular/core';

@Pipe({name: 'limit', pure: false})
export class LimitPipe {
  transform(value: any, limit) {

    if (value === undefined) {
      return undefined;
    }

    if (!limit || limit > value.length) {
      limit = value.length;
    }

    return value.slice(0, limit);

  }
}

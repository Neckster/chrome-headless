import { Pipe } from '@angular/core';

@Pipe({name: 'filter', pure: false})
export class FilterPipe {
  transform(value: any, args) {
    const outerArgs = Array.from(arguments);
    if (undefined === value) {
      return undefined;
    }
    return value.filter((item) => {
      return item[outerArgs[1]] === outerArgs[2];
    });
  }
}

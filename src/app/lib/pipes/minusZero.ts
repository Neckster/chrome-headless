import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minusZero'
})
export class MinusZeroPipe implements PipeTransform {
  transform( value: any, args): any {
    if ( value === '-00:00' ) {
      let result = value.slice(1);
      return result;
    }else {
      return value;
    }
  }
}

import { Pipe } from '@angular/core';
@Pipe({name: 'cdate', pure: false})
export class CustomDatePipe {
  transform(value: any, args) {

    const monthMapping = {
      0: 'Jan',
      1: 'Feb',
      2: 'Mar',
      3: 'Apr',
      4: 'May',
      5: 'Jun',
      6: 'Jul',
      7: 'Aug',
      8: 'Sep',
      9: 'Oct',
      10: 'Nov',
      11: 'Dec'
    };

    if (undefined === value) {
      return undefined;
    }

    const date = new Date(value * 1000);
    let inputDate = new Date(value * 1000);
    if (inputDate.setHours(0, 0, 0, 0) === (new Date()).setHours(0, 0, 0, 0)) {
      return  `${ date.getHours() > 9 ? date.getHours() : '0' +  date.getHours() }:${
        (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes())
      }`;

    } else {
      return `${monthMapping[date.getMonth()]} ${date.getDate()}`;
    }
  }
}

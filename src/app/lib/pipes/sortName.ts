import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortName'
})
export class SortNamePipe implements PipeTransform {
  transform( value: any, args): any {

    if (!value) return;

    let arg = args.split('.');
    if ( arg.length >= 2 ) {
      return value.sort ( function(a, b) {
        if ( a[ arg[0] ][ arg[1] ] < b[ arg[0] ][ arg[1] ] ) {
          return -1;
        } else if ( a[ arg[0] ][ arg[1] ] > b[ arg[0] ][ arg[1] ] ) {
          return 1;
        }
        return 0;
      });
    }
    return value.sort ( function(a, b) {
      if (  a[ arg[0] ].toLowerCase()  <  b[ arg[0] ].toLowerCase() ) {
          return -1;
      } else if (  a[ arg[0] ].toLowerCase()  >  b[ arg[0] ].toLowerCase() ) {
        return 1;
      }
      return 0;
    });
  }
}

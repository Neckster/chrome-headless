import { Pipe } from '@angular/core';

@Pipe({name: 'snake', pure: false})
export class SnakePipe {
  transform(value: any, args) {
    if (undefined === value) {
      return undefined;
    }
    return value.replace(/\s|\-/g, '_');
  }
}

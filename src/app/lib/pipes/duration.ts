import { Pipe }     from '@angular/core';
import * as moment  from 'moment';

const DAY_HRS: number = 24;
const MONTH_HRS: number = 744;

const padLeft = (str, char, len) => {
  return str.length < len ?
    ''.concat(char.repeat( len - str.length ), str) : str;
};

@Pipe({name: 'duration', pure: false})
export class DurationPipe {
  transform(value: any) {

    if (undefined === value) {
      return undefined;
    }

    const duration = moment.duration(value);

    return `${ MONTH_HRS * duration.months() + 
      DAY_HRS * duration.days() + duration.hours()}:${padLeft('' + duration.minutes(), '0', 2)}`;
  }
}

import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'staffSort'
})
export class StaffSortPipe implements PipeTransform {
  transform(value: any, orderBy: string | number, top: boolean): any {
    if (!value) return;
      let K = top ? 1 : -1,
      check = function(a, b, k) {
        if (!a[orderBy] && b[orderBy]) {
          return 1 * k;
        }else if (!b[orderBy] && a[orderBy]) {
          return -1 * k;
        }else if (!a[orderBy] && !b[orderBy]) {
          return 0;
        }else if (typeof a[orderBy] === 'string' && typeof b[orderBy] === 'string' ) {
          return a[orderBy].toLowerCase() === b[orderBy].toLowerCase() ? 0 :
            a[orderBy].toLowerCase() > b[orderBy].toLowerCase() ?
              1 * k : -1 * k;
        }else {
          return a[orderBy] === b[orderBy] ? 0 :
            a[orderBy] > b[orderBy] ? 1 * k : -1 * k;
        }
      };
      return value.sort((a: any, b: any): any => {
        return check(a, b, K);
    });
  }
}

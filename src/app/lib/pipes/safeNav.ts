import { Pipe } from '@angular/core';
import { path } from 'ramda';

@Pipe({name: 'safe'})
export class SafeNavigationPipe {
  transform(propPath: string[], source) {
    return path(propPath, source);
  }
}

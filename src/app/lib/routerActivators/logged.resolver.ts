import { Injectable }             from '@angular/core';
import { Observable }             from 'rxjs/Rx';
import 'rxjs/add/observable/of';

import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot
}                                 from '@angular/router';

import { LuaApiHttp }             from '../../services/shared/lua-http-api.service';
import { OnBoardingService }      from '../../services/onboardin.service';
import { select }                 from 'ng2-redux';
import { AppStoreAction }         from '../../store/action';


@Injectable()
export class LoggedResolver implements CanActivateChild, CanActivate {
  @select(store => store.authState) authState$: any;
  @select(store => store.authStateChecked) authStateChecked$: any;

  private authState: any;
  private authStateChecked: boolean;

  constructor(private onboardingService: OnBoardingService,
              private router: Router, private action: AppStoreAction) {
    this.authStateChecked = false;

    this.authState$.subscribe((state) => this.authState = state);
    this.authStateChecked$.subscribe((state) => this.authStateChecked = state);
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot) {

    return this.checkIsLoggedOut();
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): any {
    return this.checkIsLoggedOut();
  }

  protected checkIsLoggedOut(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authStateChecked) {
      return this.doCheck(this.authState);
    } else {
      let self = this;
      return new Promise((resolve) => {
        let authState = this.action.fetchAuthStatus();
        authState.subscribe(resp => {
          resolve(self.doCheck(resp));
        });
      });
    }
  }

  protected doCheck(authState: any) {
    if (authState.status) {
      this.router.navigateByUrl('/' + authState.action).catch(console.error);
      return false;
    } else {
      return true;
    }
  }
}

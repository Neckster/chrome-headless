import { Injectable }           from '@angular/core';
import { Observable }           from 'rxjs/Rx';
import 'rxjs/add/observable/of';

import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot
}                                from '@angular/router';

import { LuaApiHttp }            from '../../services/shared/lua-http-api.service';
import { OnBoardingService }     from '../../services/onboardin.service';
import { select }                from 'ng2-redux';
import { AppStoreAction }        from '../../store/action';


@Injectable()
export class UnauthResolver implements CanActivateChild, CanActivate {
  @select(store => store.info.authState) authState$: any;
  @select(store => store.info.authStateChecked) authStateChecked$: any;

  private authState: any;
  private authStateChecked: boolean;

  constructor(private onboardingService: OnBoardingService,
              private router: Router,
              private action: AppStoreAction) {
    this.authStateChecked = false;

    this.authState$.subscribe((state) => this.authState = state);
    this.authStateChecked$.subscribe((state) => this.authStateChecked = state);
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot) {
    return this.checkIsLoggedIn(route);
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): any {
    return this.checkIsLoggedIn(route);
  }

  protected checkIsLoggedIn(
    route: ActivatedRouteSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authStateChecked) {
      return this.doCheck(this.authState, route);
    } else {
      let self = this;
      return new Promise((resolve) => {
        let authState = this.action.fetchAuthStatus();
        authState.subscribe(resp => {
          resolve(self.doCheck(resp, route));
        });
      });
    }
  }

  protected doCheck(authState: any,
                    route: ActivatedRouteSnapshot) {
    if (authState.status) {
      return this.checkIsOnBoardingProcess(authState, route);
    } else {
      this.router.navigateByUrl('/auth').catch(console.error);
      return false;
    }
  }

  protected checkIsOnBoardingProcess(authState: any,
                                     route: ActivatedRouteSnapshot) {
    const path = route.children.length
      && route.children[0].url.length
      && route.children[0].url[0].path ? route.children[0].url[0].path : '';
    if (authState.action === 'onboarding') { // should be changed
      if (path === 'onboarding') {
        return true;
      } else {
        this.router.navigateByUrl('/onboarding');
        return false;
      }
    } else if (authState.action === 'news') {
      if (path !== 'onboarding') {
        return true;
      } else {
        this.router.navigateByUrl('/news');
        return false;
      }
    } else {
      this.router.navigateByUrl('/auth/login');
      return false;
    }
  }
}

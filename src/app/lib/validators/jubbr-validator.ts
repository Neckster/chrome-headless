import {
  ControlGroup,
  Control,
  Validators }     from '@angular/common';

import * as moment from 'moment';

export class JubbrFormValidator {


  /*
   Checks if at least one field is truthy
   case: check if at least one checkbox of the group was checked
   */
  public static atLeastOne(...fields) {
    return (group: ControlGroup): { [key: string]: any} => {
      let satisfy = fields.some((f) => {
        return group.controls[f].value &&
          group.controls[f].value !== '0' &&
          group.controls[f].value !== 'none';
      });
      return satisfy ? undefined : { noneSelected: true };
    };
  }

  public static equals(...fields) {
    return (group: ControlGroup): { [key: string]: any} => {
      return !!fields.reduce((a, b) => {
       return group.controls[a].value === group.controls[b].value ? group.controls[a].value : NaN;
      }) ? undefined : { fieldsNotEqual: true } ;
    };
  }

  public static pristineDefault(field: Control) {

    return undefined;
    // if (field.dirty && (field.value === 'none' || field.value === '0')) {
    //   return {dirtyAndDefalut: true}
    // } else if (!field.dirty && field.value === 'none') {
    //   return undefined;
    // } else {
    //   return undefined;
    // }
    // return !field.dirty &&  field.value === 'none' ? undefined : { dirtyAndDefalut: true }
  }

  public static notDefaultOption(field: Control) {
    return field.value !== 'none' ? undefined : { defaultSelected: true };
  }

  public static phoneNumber(field: Control) {
    // matching E.164 formatted phone numbers
    if (field.value && !/^\+?\d{10,14}$/.test(field.value)) {
      return { invalidPhone: true };
    }
    return undefined;
  }

  public static birthDate(field: Control) {
    /* tslint:disable:max-line-length */
    if (field.value)
      return /^((0[1-9]|1[0-9]|2[0-9])\-02)|^((0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1]|0[3-9]|1[0-2]))\.(19[0-9][0-9]|2[0-1][0-9][0-9])$/g
        .test(field.value) ?
        undefined : field.value === '' ?
        undefined : { invalidBirthDate: true };
    return undefined;
    /* tslint:enable:max-line-length */
  }

  public static pristinePass(field: Control) {
    return field.value ? undefined : { mandatoryValue: true };
  }

  public static mandatory(field: Control) {
    return field.value ? undefined : { mandatoryValue: true };
  }

  public static ssn(field: Control) {
    const SSN_REGEXP = /^[0-9]{3}[0-9]{2}[0-9]{4}$/;

    if (field.value && field.value !== '' &&
    !SSN_REGEXP.test(field.value)) {
      return { 'incorrectSocialSecurityNumber': true };
    }

    return undefined;
  }

  public static mail(field: Control) {
    // see RFC 5322
    const EMAIL_REGEXP = new RegExp(
      // dot-atom-text, 3.2.3
      '^[a-z0-9!#$%&\'*+\/=?^_`{|}~.-]+' +
      // separator, addr-spec, 3.4.1
      '@' +
      // domain, 3.4.1
      '[a-z0-9]([a-z0-9-]*[a-z0-9])?(.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$'
    , 'i');
    if (field.value && field.value !== '' &&
      (field.value.length <= 5 || !EMAIL_REGEXP.test(field.value) ||
      field.value.indexOf('.') === -1)) {
      return { 'incorrectMailFormat': true };
    }
    return undefined;
  }

  // depracate this
  public static stringTrim(field: Control) {
    if (field.value && (!field.value.trim().length || field.value[0] === ' ')) {
      return { 'notTrimmedString': true };
    }
    return undefined;
  }

  public static trimmed(field: Control) {
    if (field.value && !field.value.trim().length ) {

      return { 'notTrimmedString': true };
    }
    return undefined;
  }

  public static validDate(field: Control) {
    // debugger;
    if (field.value && !moment(field.value, 'DD.MM.YYYY').isValid()) {
      return { 'invalidDate': true };
    }
    return undefined;
  }


  public static iBanValidator(iban: any) {
    if (iban.value.length === 0) return undefined;

    let convert = (aNumStr, aDiv) => {
      let tmp: any = '', i: any, r: any;
      for (i = 0; i < aNumStr.length; i++) {
        tmp += aNumStr.charAt(i);
        r = tmp % aDiv;
        tmp = r.toString();
      }
      return tmp / 1;
    };

    let res = (ib) => {
      let rearrange = ib.substring(4, ib.length) + ib.substring(0, 4),
          alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
          alphaMap = {},
          numbers = [];
      alphabet.forEach((value, index) => {
        alphaMap[value] = index + 10;
      });

      rearrange.split('').forEach((value, index) => {
        numbers[index] = alphaMap[value] || value;
      });
      return convert(numbers.join('').toString(), 97) === 1;
    };
    return res(iban.value) ? undefined : { 'resp.invalid': true};
  };

/*
  public static sameEmail(emailConfirm: Control) {
      let valid = true;
    if (emailConfirm.root && emailConfirm.root['controls']) {


      valid = emailConfirm.value === emailConfirm.root['controls'].umail.value
    }
    return valid ? undefined : {'notSame': true};

  }
*/

  constructor() {
  }
}

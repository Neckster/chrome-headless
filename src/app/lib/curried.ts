import * as _ from 'lodash';
export const is = (valOuter) => (val) => _.isEqual(valOuter, val);
export const or = (predicates: any[]) => (e) => predicates.some(p => p(e));
export const not = (p) => !p;
export const and = (predicates: any[]) => (e) => predicates.every(p => p(e));
export const prop = (p) => (obj: any) => obj[p];
export const present = (propArray) => (obj) => Object.keys(obj).some(s => propArray.includes(s));
export const greaterEq = (a) => (b) => a >= b;

export const pipe = (...fns) => (v) => fns.reduce((a, c) => c(a) , v);
export const map = (fn) => (list) => list.map(fn);
export const filter = (fn) => (list) => list.filter(fn);
export const sort = (fn) => (list) => list.sort(fn);

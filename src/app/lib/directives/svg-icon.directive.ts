import { Directive, ElementRef, Input }           from '@angular/core';
import { cogWheelSilhouette }
  from '../../../assets/svg/cog-wheel-silhouette.svg.html.ts';
import { addSquareButton }
  from '../../../assets/svg/add-square-button.svg.html.ts';
import { speechBubblesCommentOption }
  from '../../../assets/svg/speech-bubbles-comment-option.svg.html.ts';
import { magnifyingGlass }
  from '../../../assets/svg/magnifying-glass.svg.html';
import { plusSignInABlackCircle }
  from '../../../assets/svg/plus-sign-in-a-black-circle.svg.html';
import { removeButtonIcon }                       from '../../../assets/svg/remove-button.svg.html';
import { trash }                                  from '../../../assets/svg/trash.svg.html';
import { tickInsideCircle }
  from '../../../assets/svg/tick-inside-a-circle.svg.html';
import { tickInsideCircleNeed }
  from '../../../assets/svg/tick-inside-a-circle-need.svg.html';
import { exclamationInsideCircle }
  from '../../../assets/svg/exclamation-mark-inside-a-circle.svg.html';
import { trashCan }                               from '../../../assets/svg/trash-can.svg.html';
import { copyDoc }                                from '../../../assets/svg/copy-document.svg.html';
import { calendar }
  from '../../../assets/svg/calendar-with-spring-binder-and-date-blocks.svg.html';
import { downTriangle }       from '../../../assets/svg/down-triangle.svg.html';
import { topTriangle }        from '../../../assets/svg/top-triangle.svg.html';
import { downTriangleBlue }   from '../../../assets/svg/down-triangle-blue.svg.html';
import { topTriangleBlue }    from '../../../assets/svg/top-triangle-blue.svg.html';
import { commentIcon }        from '../../../assets/svg/comment-black-oval-bubble.svg.html';
import { printIcon }          from '../../../assets/svg/print.svg.html';
import { cloudStorageUpload } from '../../../assets/svg/cloud-storage-upload.svg.html';
import { pencilEditIcon }     from '../../../assets/svg/pencil.svg.html';
import { informationButton }  from '../../../assets/svg/information-button.svg.html';
@Directive({
  selector: '[svgTemplate]'
  // templateUrl: `../../assets/svg/ambulance.svg`

})
export class SvgTemplateDirective {
  @Input('svgTemplate') name;

  private el: HTMLElement;
  private svgTemplates = {cogWheelSilhouette, addSquareButton,
    speechBubblesCommentOption, magnifyingGlass, plusSignInABlackCircle,
    removeButtonIcon, trash, tickInsideCircle, exclamationInsideCircle, trashCan,
    copyDoc, tickInsideCircleNeed, calendar, downTriangle, topTriangle,
    downTriangleBlue, topTriangleBlue, commentIcon, cloudStorageUpload,
    printIcon, pencilEditIcon, informationButton};
  constructor(el: ElementRef) { this.el = el.nativeElement; }

  ngOnInit() {
    this.el.innerHTML = this.svgTemplates[this.name];
  }
}

import {
    Component,
    forwardRef,
    Input,
    OnInit,
    AfterViewInit,
    ViewChild,
    ElementRef, Output, EventEmitter
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ParseOrganisationService }   from '../../services/shared/parseOrganisation.service';
import { select } from 'ng2-redux';

export const RECURRENT_SELECT_CONTROLL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RecurrentSelect),
    multi: true
};

@Component({
    selector: 'recurrent-select',
    providers: [ RECURRENT_SELECT_CONTROLL_VALUE_ACCESSOR ],
    template: `
      <select #select 
        [(ngModel)]="innerValue" 
        ([change])="onChange" 
        ([click])="onClick" 
        class="select">
      </select>`
})
export class RecurrentSelect implements ControlValueAccessor, OnInit, AfterViewInit {
    @ViewChild('select') container: ElementRef;
    @Input() defaultValue;
    @Input() noLocal;
    @Input() placeholder;
    @Input() noCompany;
    @Output() change = new EventEmitter<boolean>();
    @Output() click = new EventEmitter<boolean>();
    @Output() selected = new EventEmitter();

    @select() info$: any;

    public firstOption;
    public _innerValue = 0;
    public info;
    public organizations;
    public tree;
    public viewed = false;
    public onChange = () => { return this.change; };
    public onClick = () => { return this.click; };

    constructor() {
    }

    get innerValue() {
        return this._innerValue;
    }

    set innerValue(value) {
        this._innerValue = value;
        this.selected.emit({
          action: value,
          edit: true
        });

      this.propagateChange(this._innerValue);
    }

    writeValue(obj: any): void {
        if (obj !== undefined) {
            this.innerValue = obj;

        }
    }

    // Default change propagator
    propagateChange = (_: any) => {};

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setSelect() {
      if (this.noCompany && this.tree && this.tree.child) {
        this._innerValue = this.tree.child[0].id;
        this.selected.emit({
          action: this._innerValue,
          edit: false
        });
      }
      if (this.viewed) {
        this.container.nativeElement.innerHTML =
          ParseOrganisationService.htmlOptionTree(this.tree, this.noCompany, this.noLocal);
        this.viewed = false;
      }
    }

    ngOnInit(): any {
      this.info$.subscribe(info => {
        this.info = info;
        if (this.info.manager) {
          if (!ParseOrganisationService.treeData) {
            ParseOrganisationService.treeStruct(this.info.manager.organisation);
          }

          let treeData = ParseOrganisationService.treeData[0];
          this.organizations = ParseOrganisationService
            .unitPosition(treeData.type === 'company' ? treeData.child : [treeData]);
          this.tree = ParseOrganisationService.treeData[0];

          this.firstOption = this.tree && this.tree.child && this.tree.child['0'].id;

          this.innerValue = this.firstOption;

          if (this.container) {
            this.container.nativeElement.innerHTML =
              ParseOrganisationService.htmlOptionTree(this.tree, this.noCompany, this.noLocal);
          }
        }
      });

        // this.setSelect();
        // if (this.defaultValue) {
        //     this.innerValue = this.defaultValue;
        // }
    }

    ngAfterViewInit(): void {
        if (this.tree) {
            this.container.nativeElement.innerHTML =
                ParseOrganisationService.htmlOptionTree(this.tree, this.noCompany, this.noLocal);
          this.viewed = false;
        } else {
          this.viewed = true;
        }
    }
}

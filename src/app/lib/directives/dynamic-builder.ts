import { Component } from '@angular/core';

export class DynamicBuilder {

  public createComponent(template: string, directives: any[], pipes: any[]): any {

    @Component({
      template: template,
      directives: directives,
      pipes: pipes
    })
    class CustomComponent {
      public props: any;
    }

    return CustomComponent;
  }
}

import { Injectable }           from '@angular/core';
import { Observable }           from 'rxjs/Rx';
import 'rxjs/add/observable/of';

import { FirstStepComponent }   from '../../../app/manager/firstStep/firstStepComponent';
import { OnBoardingService }    from '../../services/onboardin.service';

import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanDeactivate,
  CanActivateChild,
  RouterStateSnapshot
}          from '@angular/router';

import { LuaApiHttp }            from '../../services/shared/lua-http-api.service';
import { select }                from 'ng2-redux';
import { AppStoreAction }        from '../../store/action';


/**
 * Just check auth state and check do we can activate or deactivate onboarding
 */
@Injectable()
export class OnboardingCheck implements CanActivate,  CanDeactivate<FirstStepComponent> {
  @select(store => store.authState) authState$: any;
  @select(store => store.authStateChecked) authStateChecked$: any;

  private authState: any;
  private authStateChecked: boolean;

  constructor(private onboardingService: OnBoardingService, private router: Router,
              private action: AppStoreAction) {
    this.authStateChecked = false;

    this.authState$.subscribe((state) => this.authState = state);
    this.authStateChecked$.subscribe((state) => this.authStateChecked = state);
  }

  canDeactivate(target: FirstStepComponent): Promise<boolean>|boolean {
      return this.checkCanDeactivate(this.authState);
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): any {
      return this.checkIsOnBoardingProcess(route, this.authState);
  }

  protected checkIsOnBoardingProcess(route: ActivatedRouteSnapshot,
                                     authState: any) {
    if (authState.action === 'onboarding') { // should be changed
      if (route.routeConfig.path === 'onboarding') {
        return true;
      } else {
        this.router.navigateByUrl('/onboarding');
        return false;
      }
    } else if (authState.action === 'news') {
      if (route.routeConfig.path !== 'onboarding') {
        return true;
      } else {
        this.router.navigateByUrl('/news');
        return false;
      }
    } else {
      console.log('LOGIN');
      this.router.navigateByUrl('/auth/login');
      return false;
    }
  }

  protected checkCanDeactivate(authState: any) {
    if (authState.action === 'onboarding') {
      return false;
    } else if (authState.action === 'news') {
      // this.router.navigateByUrl('/news');
      return true;
    } else {
      // this.router.navigateByUrl('/auth/login');
      return true;
    }
  }

}

import { Injectable }             from '@angular/core';

import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanDeactivate,
  CanActivateChild,
  RouterStateSnapshot }          from '@angular/router';

import { Observable }             from 'rxjs/Observable';

import { LuaApiHttp }             from '../../services/shared/lua-http-api.service';

import { NgRedux, select }        from 'ng2-redux';
import { OnBoardingService }      from '../../services/onboardin.service';
import { AppStoreAction }         from '../../store/action';

@Injectable()
export class NewsResolver implements CanActivate {

  @select(store => store.info.authState) authState$: any;
  @select(store => store.info.authStateChecked) authStateChecked$: any;
  @select() profileSet$: any;

  private authState: any;
  private authStateChecked: boolean;
  private unsubscribe = [];
  private profileSet;


  constructor(private router: Router, private onboardingService: OnBoardingService,
            private action: AppStoreAction) {
    this.authStateChecked = false;

    this.authState$.subscribe((state) => this.authState = state);
    this.authStateChecked$.subscribe((state) => this.authStateChecked = state);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): any {
    let self = this;
    if (this.authStateChecked) {
      return this.checkIsNewsProcess(route, this.authState);
    } else {
      return new Promise((resolve) => {
        this.action.fetchAuthStatus()
          .subscribe(res => {
            resolve(self.checkIsNewsProcess(route, res));
          });
      });
    }
  }

  protected checkIsNewsProcess(
    route: ActivatedRouteSnapshot,
    authState: any
  ) {
    if (authState.action === 'onboarding') { // should be changed
      if (route.routeConfig.path === 'onboarding') {
        return true;
      } else {
        this.router.navigateByUrl('/onboarding');
        return false;
      }
    } else if (authState.action === 'news') {
      if (route.routeConfig.path !== 'news') {
        return true;
      } else {
        this.router.navigateByUrl('/news');
        return false;
      }
    } else {
      console.log('LOGIN');
      this.router.navigateByUrl('/auth/login');
      return false;
    }
  }
}

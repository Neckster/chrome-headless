export const leftPad = (v) => v > 9 ? '' + v : '0' + v;

import { Component, Input, Output,
          EventEmitter }        from '@angular/core';
import { EmitService }          from '../../services/emit.service';
import { datePickerHTML }       from './date-picker.html';
import * as moment              from 'moment';
import { range }                from 'ramda';
import { Cache }                from '../../services/shared/cache.service';

@Component({
  selector: 'my-date-picker',
  template: datePickerHTML,
  pipes: [],
  styles: [ require('./style.less') ]
})

export class DatePikerComponent {

  @Input() valueDay;
  @Input() valueDate;
  @Input() dayIn;
  @Input() minYear;
  @Output() selectedDate = new EventEmitter();

  public opened: boolean = true;
  public class: string;
  public value;
  public viewDate;
  public format;
  public el: Element;
  public date: any = moment();
  public days = [];
  public firstWeekdaySunday: boolean = false;
  public viewFormat;
  public selectedDateStore;
  public editMonth = false;
  public editYear = false;
  public lastDay: number;
  public nextTenYear = +moment().format('YYYY') + 11;

  public daysList = [ 'Mo', 'Tu', 'We', 'Th',
                      'Fr', 'Sa', 'Su' ];
  /*public monthList = [ 'January', 'February', 'March',
                      'April', 'May', 'June', 'July',
                      'August', 'September', 'October',
                      'November', 'December'];*/
  public monthList = [];
  public startYears = 2015;
  public yearList = range(this.startYears, this.nextTenYear);

  public onTouchedCallback: () => void = () => { };
  public onChangeCallback: (_: any) => void = () => { };

  constructor( public emitter: EmitService,
               public cache: Cache) {

  }

  ngOnInit() {
    if (this.dayIn && typeof this.dayIn === 'number') {
        this.valueDate = moment(this.valueDate[this.dayIn - 1]);
    }else if (this.dayIn) {
        this.valueDate = moment(this.dayIn);
    }
    this.date = this.valueDate;
    this.class = `ui-kit-calendar-container ${this.class}`;
    this.format = this.format || 'YYYY-MM-DD';
    this.viewFormat = this.viewFormat || 'D MMMM YYYY';
    this.firstWeekdaySunday = this.firstWeekdaySunday || false;
    setTimeout(() => {
      if (!this.viewDate) {
        let value = moment();
        this.onChangeCallback(value.format(this.format));
      }
      this.generateCalendar();
    });
    this.createMonthList();

    this.startYears = this.minYear ? this.minYear : this.startYears;
    this.yearList = range(this.startYears, this.nextTenYear);
  }

  createMonthList() {
    let monthCounter = range(0, 12),
        dayCounter = range(1, 8);

    this.monthList = monthCounter.map( m => {
      return moment().month(m).lang(this.cache.get('lang')).format('MMMM');
    });
  }
  generateCalendar() {
    let date =  this.date,
        month = date.month(),
        year = date.year(),
        n: number = 1,
        firstWeekDay: number = (this.firstWeekdaySunday) ?
            date.date(2).day() :
            date.date(1).day();
    if (firstWeekDay !== 1) {
      n -= (firstWeekDay + 6) % 7;
    }
    this.days = [];
    let selectedDate = this.valueDay ? this.valueDay : moment(this.value);
    for (let i = n; i <= date.endOf('month').date(); i++) {
      let currentDate = moment(`${i === 0 ? 1 : i}.${month + 1}.${year}`,
                        'DD.MM.YYYY');
      let today = (moment().isSame(currentDate, 'day') &&
                  moment().isSame(currentDate, 'month')) ?
                  true : false,
          selected = (selectedDate.isSame(currentDate, 'day')) ? true : false,
          prevMonth = currentDate.subtract(1, 'months').endOf('month').format('DD'),
          nextMonth = currentDate.subtract(1, 'months').endOf('month').format('DD');
      this.lastDay = prevMonth === 'Invalid date' ? this.lastDay : +prevMonth;
      if (i > 0) {
        this.days.push({
          day: i,
          month: month + 1,
          year: year,
          enabled: true,
          today: today,
          selected: selected,
          prev: false
        });
      } else {
        this.days.push({
          day: this.lastDay + i,
          month: month ? month  : 12 ,
          year: month ? year : year - 1,
          today: false,
          selected: selected,
          prev: true
        });
      }
    }
    if ((this.days.length % 7) !== 0) {
      let lastDay = this.days[this.days.length - 1];
      let need = 7 - ( this.days.length % 7);
      for (let nx = 1; nx <= need; nx++) {
        this.days.push({
          day: nx,
          month: lastDay.month === 12 ? 1 : lastDay.month + 1 ,
          year: lastDay.month === 12 ? lastDay.year + 1 : lastDay.year,
          today: undefined,
          selected: undefined,
          prev: true
        });
      }
    }
  }

  selectDate(e: MouseEvent, i) {
    e.preventDefault();
    let date = this.days[i.day];
    this.selectedDateStore = moment(`${i.day}.${i.month}.${i.year}`, 'DD.MM.YYYY');
    // console.log(this.selectedDateStore);
    let selectedDate = this.selectedDateStore;
    this.value = selectedDate.format(this.format);
    this.viewDate = selectedDate.format(this.viewFormat);
    let selectedMoment = moment(this.value);
    this.generateCalendar();
    this.selectedDate.emit({
      action: selectedMoment
    });
    this.close();
  }

  prevMonth() {
    this.date = this.date.subtract(1, 'month');
    this.generateCalendar();
  }

  nextMonth() {
    this.date = this.date.add(1, 'month');
    this.generateCalendar();
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  toggle() {
    this.opened = !this.opened;
  }

  open() {
    this.opened = true;
  }

  close() {
    this.opened = false;
  }

  closeWrape() {
    this.close();
    this.selectedDate.emit({
      close: true
    });
  }

  closeEdit() {
    this.editMonth = false;
    this.editYear = false;
  }

  openEdit(e, val, v?) {
    e.preventDefault();
    this.closeEdit();
    switch (val) {
      case 'month':
        this.editMonth = true;
        break;
      default:
        this.editYear = true;
        break;
    }
    e.stopPropagation();
  }

  setMonth(e, m) {
    this.date = this.date.month(m).lang(this.cache.get('lang'));
    this.generateCalendar();
    e.stopPropagation();
    this.closeEdit();
  }

  setYear(e, y) {
    this.date = this.date.year(y);
    this.generateCalendar();
    e.stopPropagation();
    this.editYear = false;
    this.closeEdit();
  }

  selectedYearCheck(y) {
    return +this.date.format('YYYY') === +y;
  }
}

export const datePickerHTML: string = `
  <div [class]="class" (click)="closeEdit()">
    <div class="ui-kit-calendar-cal-container opened" >
      <div class="ui-kit-calendar-cal-top">
         <i class='icon arrow-left' (click)='prevMonth()'></i>
            <span class='dateName' [style.color]="editMonth ? '#07a0e1':'black'"
                  (click)="openEdit($event, 'month')">
                  {{ date.format('MMMM') | translate }}
                  <div *ngIf="editMonth" class="selectDate" >
                      <div *ngFor="let m of monthList" (click)="setMonth($event, m)" >{{m}}</div>
                  </div>
            </span>
            <span *ngIf="!editYear" class='dateName' style="width: 65px"
                  (click)="openEdit($event, 'year')">
                {{ date.format('YYYY') }}
            </span>
            <span *ngIf="editYear" class='dateName' style="width: 65px">
              <select style="padding-right: 3px;padding-left: 2px" #setYears (change)="setYear($event, setYears.value)">
                 <option *ngFor="let y of yearList"  
                          [selected]="selectedYearCheck(+y)" value="{{y}}">{{y}}</option>
              </select>      
            </span>
            
         <i class='icon arrow-right'  (click)='nextMonth(this)'></i>
      </div>
      <div class="ui-kit-calendar-day-names" >
        <span *ngFor="let sd of daysList; let i = index;">{{sd | translate}}</span>
      </div>
      <div class="ui-kit-calendar-days">
        <span class='dayWrapper' *ngFor="let d of days; let i = index;">
          <span (click)="selectDate($event, d)"
                [class.todayInCalendar]="d.today && !d.prev"
                [class.dayInCalendar]="d.day && !d.prev"
                [class.prev]="d.prev"
                [class.selectedInCalendar]="d.selected && d.day && !d.today && !d.prev">
                {{ d.day }}
           </span>
        </span>
      </div>
    </div>
   </div>
  <div style="position: fixed;top:0; left:0;width: 100%;height:100%;z-index: 80;" (click)="closeWrape()">
</div>
`;

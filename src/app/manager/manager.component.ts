import { Component, HostListener }  from '@angular/core';
import { Location }                 from '@angular/common';
import {
  Router,
  NavigationEnd }                   from '@angular/router';

import { NgRedux, select }          from 'ng2-redux';
import { TranslateService }         from 'ng2-translate/ng2-translate';
import moment                       = require('moment/moment');
import { Observable }               from 'rxjs/Rx';

import { Cache }                    from '../services/shared/cache.service';
import { JobService }               from '../services/shared/JobService';
import { EventStream }              from '../services/shared/sse-client';
import { NotificationHandler }      from '../services/shared/notifications.service';
import { EmitService }              from '../services/emit.service';
import { AuthService }              from '../services/shared/auth.service';
import { ParseOrganisationService } from '../services/shared/parseOrganisation.service';

import { AppStoreAction }           from '../store/action';
import { RootState }                from '../store/reducer';

import { SvgTemplateDirective }     from '../lib/directives/svg-icon.directive';
import { IMessage }                 from '../lib/interfaces/message';

import { UnitComponent }            from './unit-select/unit.component';
import { htmlManager }              from './manager.index.htm.ts';
import { FormGroup, FormControl }   from '@angular/forms';
import * as R                       from 'ramda';

import {
  isEmpty,
  cond,
  contains,
  anyPass,
  isNil,
  T }                               from 'ramda';

import { htmlWebuser } from './staff/webuser/webuser.htm';



@Component({
  selector: 'manager',
  directives: [UnitComponent, SvgTemplateDirective],
  styles: [ require('./style.less') ],
  template: htmlManager
})

export class Manager {

  @select(state => state.jobberAlert) messageAlert$: any;
  @select(state => state.broadcastEvent) eventStream$: any;
  @select() info$: any;
  @select() companyName$: Observable<string>;
  @select() unitName$: Observable<string>;
  @select() currentUnit$: any;
  @select() availabilityAlert$: any;
  @select() prevScheduledAssignAlert$: any;
  @select() dayView$: any;
  @select() switchView$: any;
  @select() schedulePubCounter$: any;
  @select() dayUnpubStaff$: any;
  @select() weekPrintView$: any;
  @select() dayPrintView$: any;
  @select() addJobberOpen$: any;
  @select() profileSet$: any;

  public weekPrintView;
  public dayPrintView;
  public loginModalOpened = false;
  public currUnit;
  public pos: any;
  public isHaveAnotherCompanies: any = false;
  public isManager = false;
  public isInit = false;
  public unsubscribe = [];
  public unit: any;
  public currentMonthISOWeek: any;
  public day = moment().isoWeekday();
  public showControllingStat = false;

  public menuSubcatStatuses = {
    activeSetup: false,
    activeProfile: false
  };

  private momentWeek = moment().startOf('isoWeek');
  private news;
  private info;
  private uname;
  private showCreateAlert;
  private avalibleExist;
  private newsStartDate;
  private managerToggeled = true;
  private settingsToggeled = true;
  private messages = {
    read: undefined,
    unread: undefined
  };
  private messagesAvailable: any[];
  private completeProfile: boolean = true;
  private completeAvailabilities: boolean = true;
  private availableCount = false;

  private subscribedGlobal = false;
  private subscribedPersonal = false;
  private checkUrl: string;
  private dayView: boolean;
  private switchView: boolean;
  private schedulePubCounter: number;
  private dayUnpubStaff: any;
  private dayUnpubCounter: number = 0;

  private profileSet: boolean; // change
  private checkSubmit: any = false;

  private stepIndex: number = 0;
  private userHas: any;
  private addJobberOpen: any;


  private searchStr = new FormControl();

  constructor(public auth: AuthService, public emitter: EmitService,
              public action: AppStoreAction, public translate: TranslateService,
              public cache: Cache, private router: Router,
              private jobService: JobService, private ngRedux: NgRedux<RootState>,
              private eventStream: EventStream, private notification: NotificationHandler,
              private location: Location) {

    const activeSetup = { activeSetup: true, profileActive: false };
    const activeProfile = { activeSetup: false, profileActive: true };
    const noneActive = { activeSetup: false, profileActive: false };

    const setupUrlPatterns = [contains('setup-company'), contains('setup-organisation')];
    const profileUrlPatterns = [
      contains('profile'),
      contains('availability'),
      contains('assignment')
    ];

    // Called on route changes
    // matches url and returns corresponding status object
    // (string) -> (activeSetup | activeProfile | noneActive)
    const detectActiveMenu = cond([
      [anyPass(setupUrlPatterns), () => activeSetup],
      [anyPass(profileUrlPatterns), () => activeProfile],
      [T, () => noneActive]
    ]);

    this.action.fetchGetInfo();

    this.unsubscribe.push(this.info$.subscribe((info: ITest) => {
      if (info.webuser && !this.subscribedPersonal) {
        this.subscribedPersonal = true;
        // this.unsubscribe.push(this.createEventSubscription(info.webuser.pushhash));
      }
console.log(info);
      // if (info.webuser && !this.subscribedGlobal) {
      //   this.subscribedGlobal = true;
      //   this.unsubscribe.push(this.createEventSubscription());
      // }

      // todo refactor this
      if (Object.keys(info).length) {
        this.completeAvailabilities =
          !(info.available &&
          (!info.available.length ||
            info.available.every(a => a.company_id))
          || !info.available);
        this.completeProfile = (info.webuser && info.webuser.cellphone);
      }
      if (info.manager || (info.webuser && info.webuser.role === 'company')) {
        this.info = info;
        this.isManager = true;
        if (info.manager && info.manager.selected) {
          this.action.setCname(info.manager.selected.cname);
          ParseOrganisationService.nonSelect = false;
          if (!this.isInit) {
            this.action.setUname(info.manager.selected.nname);
            this.isInit = true;
          }
          this.isHaveAnotherCompanies = info.company.length > 1;
        }
      } else {
        this.isManager = false;
      }
      // this.userHas = this.isManager ? managerSteps : jobberSteps;


      if (info.webuser) {
        this.uname = info.webuser.uname;
        this.newsStartDate = new Date(info.webuser.ts_registry * 1000);
        let loginedManager = info.webuser.role !== 'jubber';
        this.action.userIsManager(loginedManager);

        (<any>window).Intercom('boot', {
          app_id: 'c0bqhjki',
          name: info.webuser.uname,
          email: info.webuser.umail,
          created_at: 1234567890
        });
      }

      if (info.news) {
        this.news = info.news;

        this.messagesAvailable = this.news
          .filter((item) => {
            return new Date(item.valid_from * 1000) >= new Date(this.newsStartDate);
          });
        this.messages.read = this.messagesAvailable
          .filter((msg: IMessage) => msg.read).length;

        this.messages.unread = this.messagesAvailable.length - this.messages.read;

        if (info.manager && !this.currUnit && info.manager.organisation.length) {
          let a = info.manager.organisation.filter(u => {
            return (+u.id.slice(1)) === info.manager.selected.unit_id;
          });
          // this.action.setUname(a[0] ? a[0].text : undefined);
          // this.currUnit = a[0] ? a[0].text : undefined;
        }
      }
      if (info && info.available && info.available.length) {
        let avalibleArr = info.available.filter(el => el.company_id === 0)[0];
        this.availableCount = avalibleArr.available.every( el => el !== 1);
        this.action.showAlertAvailability(this.availableCount);
      }else if (!info.available) {
        this.action.showAlertAvailability(true);
      }
    }));

    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.unit_id) {
        this.unit = unit;
        this.init();
      }
    }));

    // this.unsubscribe.push(this.eventStream$.subscribe(e => {
    //   if (!isEmpty(e)) {
    //     this.notification.show(e.title.default, {
    //       icon: 'assets/img/anonymous.png',
    //       body: e.message.default
    //     });
    //   }
    // }));

    this.emitter.addEvent('messageUpdate').subscribe((res) => {
      if (res.status === 'read') {
        this.messages.unread -= 1;
      }
    });

    this.messageAlert$.subscribe(el => {
      this.showCreateAlert = el;
    });

    this.dayView$.subscribe( dayView => {
      this.dayView = dayView;
    });

    this.switchView$.subscribe( switchView => {
      this.switchView = switchView;
    });

    this.schedulePubCounter$.subscribe( schedulePubCounter => {
      this.schedulePubCounter = schedulePubCounter;
    });

    this.dayUnpubStaff$.subscribe(dayUnpubStaff => {
      this.dayUnpubStaff  = dayUnpubStaff;
      if (this.dayUnpubStaff) {
        setTimeout(() => {
          this.dayUnpubCounter = Object.keys(this.dayUnpubStaff).length;
        }, 0);
      }else {
        this.dayUnpubCounter = 0;
      }
    });

    this.unsubscribe.push(this.weekPrintView$.subscribe( weekPrintView => {
      this.weekPrintView = weekPrintView;
    }));

    this.unsubscribe.push(this.dayPrintView$.subscribe( dayPrintView => {
      this.dayPrintView = dayPrintView;
    }));

    this.unsubscribe.push(this.profileSet$.subscribe( profileSet => {
      this.profileSet = profileSet;
    }));

    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.profileSet = !(e.url === '/onboarding'); // change

        this.menuSubcatStatuses = detectActiveMenu(e.url);
        this.checkUrl = e.url.slice(1);
        this.action.openWeekPrintView(false);
      }
    });

    this.addJobberOpen$.subscribe( addJobberOpen => {
      this.addJobberOpen = addJobberOpen;
    });
  }

  init() {
    if (this.unit && Array.isArray(this.unit.job) && this.unit.job.length) {
      let res = this.unit;
      let currentMonthISOWeek: any = +moment(this.momentWeek).format('WW');
      res.shift = res.shift
        .map((shift, shiftIndex) => {
          let positions = [];
          shift.positions = this.jobService.structPositions(shiftIndex,
            Object.assign({}, this.unit),
            this.day, Object.assign({}, this.info), currentMonthISOWeek);
          return this.jobService.calcShift(shift);
        });
      let bubble = res.shift
        .filter(el => {return el.bubbles && el.positions.length; })
        .map(el => {return el.bubbles; });
      if (bubble.length) {
        res.bubbles = bubble.reduce((a, b) => {
          let res2 = {};
          res2 = {
            late: (a ? a.late : 0) + (b ? b.late : 0),
            clarify: (a ? a.clarify : 0) + (b ? b.clarify : 0),
            working: (a ? a.working : 0) + (b ? b.working : 0)
          };
          return res2;
        });
      } else {
        res.bubbles = {late: 0, clarify: 0, working: 0};
      }
      this.unit = res;

    }
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe());
  }

  ngOnInit() {

    this.unitName$.subscribe(name => {
      if (name !== 'sys')
        this.currUnit = name;
    });
    this.pos = ParseOrganisationService;
    this.init();
    let path = window.location.pathname.split('/');
    if ( path[ path.length - 1 ] === 'controlling') {
      this.showControllingStat = true;
    }
    // this.switchView = this.cache.get('schedule:view');

    this.searchStr.valueChanges
      .debounceTime(1000).subscribe(str => {
      this.action.staffFilterStr(str);
    });

  }

  hideScrollBar() {
    let htmlTag = document.getElementsByTagName('html');
    htmlTag[0].classList.toggle('uk-modal-page');
  }

  changeTranslate(lang) {
    this.cache.set('lang', lang);
    this.translate.use(lang);
    this.action.localisation(lang);
    ParseOrganisationService.lang = lang;
  }

  // createEventSubscription(hash?: string) {
  //   if (this.eventStream.allow) {
  //     this.eventStream.createStream(hash).subscribe({
  //       next: (data => {
  //         const event = JSON.parse(data);
  //         this.action.broadcastEvent(event);
  //       }),
  //       error: console.error
  //     });
  //   }
  // }


  logout(e) {
    e.preventDefault();
    this.auth.logout().subscribe((res) => {
      this.auth.dropNodeSession().subscribe(() => {
        this.action.fetchAuthStatus();
        this.unsubscribe.forEach(el => {
          if (!isNil(el)) el.unsubscribe();
        });
        this.action.clearStore();
        this.cache.cleanAppCache(['lang']);
        window.location.assign('/');
      });
    }, console.error);
  }

  hideScheduledNotification() {
    this.action.hideAlertScheduledJobber();
  }

  // nav button
  toggleDayView(el) {
    this.action.dayViewAction(el);
  }

  tempVar() {
    if (this.unit) {
      return R.pluck('active', this.unit.shift).length > 1;
    }
  }

  toggleView(i) {
    this.action.switchViewAction(i);
    this.cache.setObject('schedule:view', i);
  }

  openCopy(el) {
    this.action.openCopyAnction(el);
  }

  schedulePublic(e) {
    this.action.schedulePublic(true);
    // this.hideScrollBar();
  }

  dayViewPublic(e) {
    if (this.dayUnpubStaff) {
      this.action.openPublishModal(true, this.dayUnpubStaff);
    }
  }

  addJoberModal(e) {
    this.action.addJobberOpen(true);
    this.hideScrollBar();
  }

  openWeekPrintView(e) {
    this.action.openWeekPrintView(!this.weekPrintView);
  }

  openDayPrintView(e) {
    this.action.openDayPrintView(!this.dayPrintView);
  }

  submitSteps(e, step) {
    if (this.checkSubmit) {
      this.userHas[step].submit = 'isValid';
    }else {
      this.userHas[step].submit = true;
    }
  }

  nextStep(e, step) {
    if (e.action) {
      this.userHas[step].active = false;
      if (step === 2) {
       this.profileSet = true;
        this.cache.removeKey('firstSteps');
        this.cache.removeKey('complexCompany');
      }else {
        this.stepIndex++;
        this.cache.set('firstSteps', this.stepIndex);
        this.userHas[this.stepIndex].active = true;
      }

    }
  }

  prevStep(e) {
    this.userHas[this.stepIndex].active = false;
    this.stepIndex--;
    this.cache.set('firstSteps', this.stepIndex);
    this.userHas[this.stepIndex].active = true;
    this.userHas[this.stepIndex].submit = false;
  }
}

interface ITest {
  info: any;
  company: any;
  news: any;
  unit_id: any;
  nname: any;
  manager: any;
  organisation: any;
  webuser: any;
  available: any;
}

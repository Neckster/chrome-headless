//noinspection TsLint,TsLint
export const positionHtml = `
<li 
    class="uk-nestable-item uk-nestable-nodrag uk-parent uk-nestable-nodrag jc-template-controlling-position late"
    id="controlling-shift-pos-0-131499">
                  <!--|Position Row-->
                  <div class="uk-nestable-panel">
                    <div class="uk-grid uk-grid-collapse">
                      
                      <div class="nestable-body">
                        <div class="uk-grid uk-grid-collapse" >
                          <div class="uk-width-2-10 unit-name colhead head-inner jc-pname" style="height: 4rem;white-space: nowrap; overflow:hidden">
                            <div  class="icon large  blue" [ngClass]='{"arrow-down":position.open, "arrow-right":!position.open}'(click)="toggleCollapsible()"  style='cursor:pointer; position:absolute; left:15%; white-space: nowrap' ></div>
                            {{ position.position.pname }}
                           </div>
                          <div class="uk-width-8-10 uk-text-center grid-table-inner">
                            <div class="uk-grid uk-grid-collapse">
                              <div class="uk-width-3-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-4-10">
                                    <div class="uk-grid uk-grid-collapse">
                                      <div class="uk-width-1-3 b-r">
                                        <span class="uk-badge uk-badge-notification jc-badge jc-onduty smallBubbles">
                                          {{position.bubbles.working === 0 ? '' : position.bubbles.working}}
                                         </span>
                                      </div>
                                      <div class="uk-width-1-3 b-r">
                                        <span class="uk-badge uk-badge-notification uk-badge-danger jc-badge jc-late smallBubbles">
                                          {{position.bubbles.late === 0 ? '' : position.bubbles.late}}
                                        </span>
                                      </div>
                                      <div class="uk-width-1-3 b-r">
                                        <span class="uk-badge uk-badge-notification uk-badge-warning jc-badge jc-changed smallBubbles">
                                          {{position.bubbles.clarify === 0 ? '' : position.bubbles.clarify}}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="uk-width-3-10 b-r"><span><!--IN--></span></div>
                                  <div class="uk-width-3-10 b-r"><span><!--OUT--></span></div>
                                </div>
                                <!-- uk-grid uk-grid-collapse -->
                              </div>
                              <div class="uk-width-1-10 b-r">
                                <!-- <span class="jc-idle">{{position.hours.breakh}}</span> -->
                              <!--BREACK-->
                              </div>
                              <!-- uk-width-2-10 -->

                              <div class="uk-width-2-10 b-r">&nbsp;<!--Comment--></div>
                              <!-- uk-width-2-10 -->
                              <!--Hours-->
                              <div class="uk-width-4-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-1-4 b-r"><span class="jc-planned">{{position.hours.planned}}</span><!--Planned--></div>
                                  <div class="uk-width-1-4 b-r"><span class="jc-time">{{position.hours.expected}}</span><!--Expected--></div>
                                  <div class="uk-width-1-4 b-r"><span class="jc-diff">{{position.hours.diff}}</span></div>
                                  <div class="uk-width-1-4 b-r"><span class="jc-realnet"></span>{{position.hours.actual}}</div>
                                </div>
                              </div>
                              <!-- uk-width-4-10 -->
                            </div>
                            <!-- uk-grid -->
                          </div>
                          <!-- uk-width-1-7 -->
                        </div>
                        <!-- uk-grid item 2 -->
                      </div>
                    </div>
                  </div>
                  <!-- uk-nestable-panel -->
                  
                  <!--Jobbers Row-->
                  <ul class="uk-nestable-list uk-margin-small-bottom" *ngIf="position.open">
                    <!--Jobber-->
                    <li *ngFor="let jobber of position.jobbers | sortName: 'uname'" class="uk-nestable-item uk-parent uk-nestable-nodrag jc-template-append-controlling-user">
                      <!--filled by js-->
                      <div class="needci time-current uk-nestable-panel user-table job jc-template-controlling-user">
                        <div class="uk-grid uk-grid-collapse uk-padding-remove">
                        
                          <div class="uk-width-2-10 name"   [ngClass]="{'orange-inactive': !jobber.had_login && !jobber.pending, 'red-color': jobber.pending}">
                            <span *ngIf="jobber.job && jobber.job.isWorking" 
                                  class="uk-badge uk-badge-notification jc-badge jc-onduty counterBubbles">
                                  {{jobber.job.checkIndex}}
                             </span>
                             <span class="jc-uname" >{{ jobber.uname }}</span></div>
                          
                          <div class="uk-width-8-10 uk-text-center grid-table-user b-b">
                            <div class="uk-grid uk-grid-collapse">

                              <div class="uk-width-3-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <!--Plan--> 
                                  <div class="uk-width-4-10 b-r">
                                    <span class="jc-from">{{ getJob(jobber).from }}</span>–<span class="jc-to">{{ getJob(jobber).to }}</span>
                                  </div>
                                  
                                  <!--In-->
                                  <div class="uk-width-3-10 b-r inout" (click)="openTimingModal(jobber);">
                                    <span *ngIf="jobber.lastStatus !== 'cancel'" >
                                      <span *ngIf="getJob(jobber).isLate" [style.background-color]="getJob(jobber).status === 'notEhour' ? '': ''" [style.color]="getJob(jobber).chekInC ? '#ff8f00':'black'" class="jc-checkin " [ngClass]="{'span-checkin': !getJob(jobber).checkIn }">{{ !getJob(jobber).checkIn ? getJob(jobber).hours.lateDiff : getTime(jobber.job.checkIn)}}</span>
                                      <span *ngIf='!getJob(jobber).isLate && getJob(jobber).checkIn' class="jc-checkin" [style.color]="getJob(jobber).chekInC || getJob(jobber).status === 'approve'?'black':'#ff8f00'">{{getTime(getJob(jobber).checkIn)}}</span>
                                      <span *ngIf="getJob(jobber).status === 'abandon' && getJob(jobber).isClarify && !getJob(jobber).chekInC" style="color: #ff8f00; display: inline-block;cursor: pointer;" class="icon warn abandoned">‼</span>
                                      <span *ngIf="getJob(jobber).status === 'declined' && getJob(jobber).isClarify && !getJob(jobber).chekInC" style="color: #ff8f00; top: -2px; display: inline-block;cursor: pointer;" class="icon warn abandoned">dec‼</span>
                                      <span *ngIf="getJob(jobber).isLate && !getJob(jobber).checkIn" class="icon error needci">⏰</span>
                                    </span>
                                  </div>
                                 
                                   <!--Out-->
                                  <div class="uk-width-3-10 b-r inout" (click)="openTimingModal(jobber);">
                                    <span *ngIf="jobber.lastStatus !== 'cancel'" >
                                      <span *ngIf="getJob(jobber).checkOut && !getJob(jobber).isClarify"  class="jc-checkout span-checkout">{{ getTime(jobber.job.checkOut)}}</span> <!--[style.color]="getJob(jobber).status === 'approve'?'green':'black'"-->
                                      <span *ngIf="getJob(jobber).checkOut && getJob(jobber).isClarify" [style.background-color]="getJob(jobber).status === 'notEhour' ? '': ''" [style.color]="getJob(jobber).checkOutC ? '#ff8f00': 'black'"  class="jc-checkout span-checkout">{{ getTime(jobber.job.checkOut)}}</span>
                                      <span class="icon error needco">⏰</span>
                                    </span>
                                  </div>
                                  
                                </div>
                                <!-- uk-grid uk-grid-collapse -->
                              </div>
                              
                              <!--Break-->
                              <div class="uk-width-1-10 b-r">
                                <span *ngIf="jobber.lastStatus !== 'cancel'" class="jc-idle">
                                    {{ getJob(jobber).idlemanager ? getJob(jobber).idlemanager : getJob(jobber).idleuser ? getJob(jobber).idleuser : '00:00'}}
                                </span>
                              </div>
                              
                              
                              <!-- uk-width-2-10 -->
                              <!-- TODO: show full text on click / use modal -->
                                <!--Comments-->
                              <div class="uk-width-2-10 b-r uk-position-relative uk-text-truncate">
                                <div *ngIf="getJob(jobber).info && getJob(jobber).info.message"
                                  class="uk-position-absolute uk-position-top-left uk-position-bottom-right uk-overflow-hidden jc-info jobinfo uk-text-truncate">{{getJob(jobber).info.message}}</div>
                              </div>
                              
                              
                              <!-- uk-width-3-10 -->
                              <!--Hours-->
                              <div class="uk-width-4-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-1-4 b-r">
                                    <span class="jc-planned">
                                        {{ getJob(jobber).hours.planned}}
                                    </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                      <span *ngIf="jobber.lastStatus !== 'cancel'" class="jc-time">
                                          {{getJob(jobber).hours.expected}}
                                      </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                      <span *ngIf="jobber.lastStatus !== 'cancel'" class="jc-diff">
                                          {{getJob(jobber).hours.diff}}
                                      </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                    <span *ngIf="jobber.lastStatus !== 'cancel'" class="jc-realnet">
                                        {{getJob(jobber).hours.actual}}
                                    </span>
                                  </div>
                                </div>
                              </div>
                              
                              
                              <!-- uk-width-4-10 -->
                            </div>
                            <!-- uk-grid -->
                          </div>
                          <!-- uk-width-1-7 -->
                        </div>
                        <!-- uk-grid item 3-->
                      </div>
                    </li>
                    
                  </ul>
                  <!-- uk-nestable-list -->
                </li>
`;

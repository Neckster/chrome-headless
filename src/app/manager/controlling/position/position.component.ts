import { Component, Input } from '@angular/core';
import { positionHtml } from './position.html';
import { JobService } from '../../../services/shared/JobService';
import { AppStoreAction } from '../../../store/action';
import moment = require('moment/moment');
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { Cache } from '../../../services/shared/cache.service';
import { SortNamePipe } from '../../../lib/pipes/sortName';

@Component({
  selector: 'position',
  providers: [ ],
  directives: [ ],
  pipes: [ SortNamePipe ],
  styles: [ ],
  template: positionHtml
})

export class PositionComponent {
  // Set our default values
  @Input() position: any;
  @Input() unit: any;
  @Input() shiftIndex: any;
  @Input() day: any;
  public jobbers: any;
  public jobs: any;
  public now;
  public hideScrollBar = this.jobService.hideScrollBar;
  // TypeScript public modifiers
  constructor(public jobService: JobService,
              public cache: Cache,
              public action: AppStoreAction) {
  }

  ngOnInit() {
    this.now = new Date().getTime();
    this.position.open = true;
    this.position.open = this.cache.isInArray(
      'controlling:posOpened',
      `${this.position.position.position_id}-${this.shiftIndex}`
    );
    if (this.position && this.position.jobbers) {
      this.position.jobbers = this.position.jobbers
        .sort(this.sortBubbles).map((el, i) => {

        if (el.job.isWorking) {
          el.job = Object.assign({}, el.job, {checkIndex: i + 1});
        }
        if (el.job && el.job.log && el.job.log.length) {
          let last = el.job.log[el.job.log.length - 1][0];
          el = Object.assign({}, el, {lastStatus: last});
        }
      return el;
      });
    }
  }

  sortBubbles (a, b) {
    let aTime = a.job.checkIn,
        bTime = b.job.checkIn;

    if (a.job.isWorking && b.job.isWorking) {
      return 0;
    }else if (!a.job.isWorking) {
      return 1;
    }else if (!b.job.isWorking) {
      return -1;
    }
    return aTime === bTime ? 0 : aTime < bTime ?
        -1 : 1;
  }

  openTimingModal(jobber) {
    let j = jobber;
    j.job = this.getJob(jobber, false);
    let week = this.jobService.getWeek();
    week = (+`${week[2]}${week[3]}`);
    if (this.now <= moment(j.job.from, 'HH:mm').isoWeek(week)
        .day(j.job.weekday).valueOf()) return undefined;
    let param = {
      position_id: this.position.position.position_id,
      jobber: jobber,
      week: this.jobService.getWeek()
    };
    this.action.setModalTimingData(param);
    this.hideScrollBar();
  }

  toggleCollapsible() {

    if (this.position.open) {
      this.cache.removeFromArray(
        'controlling:posOpened',
        `${this.position.position.position_id}-${this.shiftIndex}`
      );
    } else {
      this.cache.pushArray(
        'controlling:posOpened',
        `${this.position.position.position_id}-${this.shiftIndex}`
      );
    }

    this.position.open = !this.position.open;
  }

  calculateWeek() {
    let currentMonthISOWeek = moment().isoWeek();
    let currentMonthISOWeekYear = moment().isoWeekYear();
    let calendarWeek = ('' + currentMonthISOWeekYear).substring(2, 4) + currentMonthISOWeek ;
    return +calendarWeek;
  }
  // calcBubble(key) {
  //   let count = this.position.jobbers.filter(el => {return el.job[key]; }).length;
  //   return count > 0 ? count : '';
  // }
  getTime(time) {
    // let hours = `${new Date(time).getHours()}`;
    // let minutes = `${new Date(time).getMinutes()}`;
    // debugger
    // return `${hours.length === 1 ? `0${hours}` : hours}
    // :${minutes.length === 1 ? `0${minutes}` : minutes}`;
    return moment(time).format('HH:mm');
  }
  getJob(jobber, flag) {
    if (flag) return jobber;
    return jobber.job;
  }




}

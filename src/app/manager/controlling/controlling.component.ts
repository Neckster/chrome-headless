import { Component } from '@angular/core';

import { select } from 'ng2-redux/lib/index';
import moment = require('moment/moment');

import { range }                    from 'ramda';
import { JobService } from '../../services/shared/JobService';
import { Cache } from '../../services/shared/cache.service';
import { EmitService }              from '../../services/emit.service';

import { AppStoreAction } from '../../store/action';

import { htmlControlling } from './controlling.htm.ts';

import { ShiftComponent } from './shift/shift.component';
import { ModalTimingComponent } from './modal/modal.component';
import { DatePikerComponent }       from '.././date-picker/date-picker';


@Component({
  selector: 'manager-controlling',
  directives: [ ShiftComponent, ModalTimingComponent, DatePikerComponent ],
  template: htmlControlling
})
export class ManagerControlling {

  @select() info$: any;
  @select() currentUnit$: any;
  @select() modalTiming$: any;
  @select(state => state.localisation) lang$: any;

  public currentUnit;
  public unit: any;
  public acc: any;
  public currentMonthISOWeekYear: any;
  public currentMonthISOWeek: any;
  public currentMonthStart: any;
  public store: any;
  public hideScrollBar = this.jobService.hideScrollBar;
  public modalParam: any;
  public day = moment().isoWeekday();
  public openTimingModal = false;
  public week = this.calculateWeek(undefined);
  public updateInfo = false;
  public unsubscribe = [];
  public lang;
  private momentWeek = moment().startOf('isoWeek');
  private info;
  private interval;
  private unitId;

  private selDate: string = 'DD-MM-YYYY';
  private minDate: string = '01-01-2016';
  private maxDate: string = '12-31-2016';
  private disableDays: Array<number> = [];
  private toContainPrevMonth: boolean = false;
  private toContainNextMonth: boolean = false;
  private value = '';
  private datePeckerValue;
  private showCalendarVar;
  private valueDay;
  private valueDate;
  private showMonth;


  private superArr = [];
  private totalBubbles = [];

  constructor(public jobService: JobService, public cache: Cache,
              public emitter: EmitService, public action: AppStoreAction) {

    this.unsubscribe.push(this.info$.subscribe(info => {
      if (info.manager) {
        this.info = info;
        this.updateInfo = false;
        let unitId = info.manager.selected.unit_id;
        if (!isNaN(unitId) && unitId !== 0)
          this.action.fetchCurrentUnit(unitId, this.week);
      }
    }));

    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data;
      this.changeWeekValue();
    }));

    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.unit_id) {
        this.unit = unit;
        // this.action.setUname(unit.nname);
        this.changeWeekValue();
        this.calcTotalBubbles();
        this.init();
      }
    }));

    this.unsubscribe.push(this.modalTiming$.subscribe(modalData => {
      this.openTimingModal = modalData.open;
      this.modalParam = modalData.param;
      if (modalData.updated) {
        this.updateInfo = true;
        this.action.fetchGetInfo(this.week);
        // this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      }
    }));

    this.week = this.cache.get('controlling:week') || this.calculateWeek(undefined);

    if (this.cache.get('controlling:week')) {
      this.momentWeek = moment(this.cache.get('controlling:week'), 'ggWW').startOf('isoWeek');
      this.changeWeekValue();
      this.calculateWeek(
        undefined,
        this.cache.get('controlling:week').substr(2),
        this.cache.get('controlling:week').substr(0, 2)
      );
    }

    if (this.cache.get('controlling:dayIdx'))
      this.changeDay(this.cache.getObject('controlling:dayIdx'));

  }

  ngOnInit() {
    let fetch = () => {
      this.action.fetchGetInfo(this.week);
      // this.action.fetchCurrentUnit(this.unitId, this.week);
    };
    fetch = fetch.bind(this);
    this.interval = setInterval(fetch, 15000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
    this.unsubscribe.forEach(el => { el.unsubscribe(); });
  }

  calculateWeek(date, week?, year?) {
    let currentMonthISOWeek = week || moment(date).format('WW');
    let currentMonthISOWeekYear = year || moment(date).format('GG');
    let calendarCurrentWeek = currentMonthISOWeekYear +
      currentMonthISOWeek;

    return calendarCurrentWeek;
  }

  prepareShift() {
    // this.unit.activeShift = this.unit.shift.filter(el => {return el.active; });
  }

  loadWeek(weekDiff: number) {
    this.momentWeek = !weekDiff ?
      moment().startOf('isoWeek') :
      moment(this.momentWeek).add(weekDiff, 'week');
    this.week = !weekDiff ?
      this.calculateWeek(undefined) :
      this.calculateWeek(this.momentWeek);

    if (!weekDiff)
      this.changeDay(+moment().format('E'));

    this.cache.set('controlling:week', this.week);

    this.changeWeekValue();

    this.action.fetchCurrentUnit(this.unit.unit_id, this.week);
  }


  changeDay(dayIdx) {
    this.cache.set('controlling:dayIdx', dayIdx);
    this.day = dayIdx;
    this.init();
  }

  isToday(dayIdx) {
    return dayIdx === this.day;
  }

  calcTotalBubbles() {
    if (this.unit && Array.isArray(this.unit.job) && this.unit.job.length) {
      this.superArr = [];
      for (let a = 1; a <= 7; a++) {
        let res = this.unit.shift
          .map((shift, shiftIndex) => {
            let positions = [];
            shift.positions = this.jobService.structPositions(shiftIndex,
              Object.assign({}, this.unit),
              a, Object.assign({}, this.info), this.currentMonthISOWeek);
            return this.jobService.calcShift(shift);
          });
        res = res.map(res => {
          return res.bubbles ?
            res.bubbles.bubblesOnDayArr :
            this.jobService.emptyBubblesArraForWeek();
        });
        this.superArr.push(res);
      }
      this.totalBubbles = this.superArr.map((d, i) => {
        return d.map(s => s[i]).reduce( (sum, el) => {
          return {
            clarify: sum.clarify.concat(el.clarify),
            late: sum.late.concat(el.late) ,
            working:  sum.working.concat(el.working)
          };
        }, {clarify: [], late: [], working: []});
     });
    }
  }


  init() {
    if (this.unit && Array.isArray(this.unit.job) && this.unit.job.length) {
      let shiftsTest = [];
      let res = this.unit;
      res.shift = res.shift
        .map((shift, shiftIndex) => {
          let positions = [];
          shift.positions = this.jobService.structPositions(shiftIndex,
            Object.assign({}, this.unit),
            this.day, Object.assign({}, this.info), this.currentMonthISOWeek);
          return this.jobService.calcShift(shift);
        });
      let hours = res.shift
        .filter(el => { return el.hours && el.positions.length; })
        .map(el => el.hours);
      if (hours.length) {
        res.hours = this.calcHoursTotal(hours);
      } else {
        res.hours = {
          planned: '00:00', plannedU: 0,
          actual: '00:00', actualU: 0,
          diff: '00:00', diffU: 0,
          expected: '00:00', expectedU: 0
        };
      }
      let bubble = res.shift
        .filter(el => {return el.bubbles && el.positions.length; })
        .map(el => {return el.bubbles; });
      if (bubble.length) {
        res.bubbles = bubble.reduce((a, b) => {
          let res2 = {};
          let aArr = a.bubblesOnDayArr || this.jobService.emptyBubblesArraForWeek(),
            bArr = b.bubblesOnDayArr || this.jobService.emptyBubblesArraForWeek();
          let arr = aArr.map( (el, i) => {
              el.clarify = el.clarify.concat(bArr[i].clarify);
              el.late = el.late.concat(bArr[i].late);
              el.working = el.working.concat(bArr[i].working);
              return el;
            });
          res2 = {
            late: (a ? a.late : 0) + (b ? b.late : 0),
            clarify: (a ? a.clarify : 0) + (b ? b.clarify : 0),
            working: (a ? a.working : 0) + (b ? b.working : 0),
            bubblesOnDayArr: arr
          };
          return res2;
        });
      } else {
        res.bubbles = {late: 0, clarify: 0, working: 0};
      }
      this.unit = res;
    }
  }

  checkUnitValid(unit) {
    return unit.active && Array.isArray(unit.positions) && unit.positions.length;
  }

  calcHoursTotal(hoursT) {
    let minutes = undefined;
    let hours = {
      planned: undefined, plannedU: undefined,
      actual: undefined, actualU: undefined,
      diff: undefined, diffU: undefined,
      expected: undefined, expectedU: undefined,
      breakh: undefined
    };
    // breakh
    hours.breakh = this.jobService.calculateStringHours(hoursT.map(pos => pos.breakh)).stringTotal;
    // planned
    let plannedU = hoursT
      .map(h => {return h.plannedU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.planned = this.jobService.getTimeFromDiff(plannedU);
    hours.plannedU = plannedU;
    // actual
    let actualU = hoursT
      .map(h => {return h.actualU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.actual = this.jobService.getTimeFromDiff(actualU);
    hours.actualU = actualU;
    // diff
    let diffU = hoursT
      .map(h => {return h.diffU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.diff = this.jobService.getTimeFromDiff(diffU);
    hours.diffU = diffU;
    // expected
    let expectedU = hoursT
      .map(h => {return h.expectedU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.expected = this.jobService.getTimeFromDiff(expectedU);
    hours.expectedU = expectedU;
    return hours;
  }

  setDate($event) {
    if ($event.action) {
      this.valueDay = moment($event.action).lang(this.cache.get('lang'));
      this.momentWeek = moment($event.action).isoWeekday(1);
      this.week = this.calculateWeek(this.momentWeek);
      let day = $event.action.day() === 0 ? 7 : $event.action.day();
      this.changeDay(day);
      this.cache.set('controlling:week', this.week);
      this.changeWeekValue();
      this.action.fetchCurrentUnit(this.unit.unit_id, this.week);
    }
    this.showCalendarVar = false;

  }
  changeWeekValue() {
    this.jobService.setWeek(this.week);
    let currentMonthISOWeekYear = moment(this.momentWeek ).format('GGGG');
    let currentMonthISOWeek: any = +moment(this.momentWeek ).format('WW');
    let currentMonthStart = moment(this.momentWeek).lang(this.cache.get('lang')).format('MMMM');

    let endOfCurrentMonth = moment(this.momentWeek).format('WW'),
      startOfNextMonth = moment(this.momentWeek).add(1, 'month')
        .startOf('month').lang(this.cache.get('lang')).format('WW');

    if (endOfCurrentMonth === startOfNextMonth) {
      this.showMonth = currentMonthStart + ' - ' +
        moment(this.momentWeek).add(1, 'month').lang(this.cache.get('lang')).format('MMMM');
    }else {
      this.showMonth = currentMonthStart;
    }

    let accArr = [];
    for (let i = 0; i < 7; i++) {
      accArr.push(moment(this.momentWeek)
                  .add(i, 'days')
                  .format('ddd DD MMMM YYYY'));
    }
    this.valueDate = accArr; // Add array for datepicker correct work

    accArr = accArr.map(a => {
      return [
        `${a[0]}${a[1]}${a[2]}`,
        `${a[4]}${a[5]}`
      ];
    });
    // debugger;
    this.currentMonthISOWeekYear = currentMonthISOWeekYear;
    this.currentMonthISOWeek = currentMonthISOWeek;
    this.currentMonthStart = currentMonthStart;
    this.acc = accArr;
  }
  showCalendar() {
    this.showCalendarVar = true;
  }
}

interface PositionI {
  staff: any;
  position_id: any;
}

interface JobI {
  webuser_id: any;
}

export const timingModalHtml: string = `
<section class="uk-modal uk-open" id="jobreview" tabindex="-1" role="dialog"
         aria-labelledby="label-jobreview" aria-hidden="false"
         style="display: block; overflow-y: auto;">
  <div class="uk-modal-dialog"  >
    <a (click)="closeModal();" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}'
       data-close="dismiss" data-dismiss="modal">
      <!--icon-->
    </a>
    <div class="uk-modal-header">
      <div id="label-jobreview"
              style='box-shadow:none; background:rgba(0,0,0,0); min-height: 3rem; height: 3rem;'>
        <h2><a (click)="closeModal();" class="uk-modal-close"><!--icon--></a>
          <span>{{'Correct_timings' | translate}}</span>
        </h2>
      </div>
    </div>
    <div class="modal-content">
      <div>{{'The_values_in_parantheses_are' | translate}}</div>
      <form class="uk-form small uk-form-horizontal jobreview" [formGroup]="correctTimeForm">
        <div style="display: inherit"
             class="unpub hadci changedci managertime hadco changedco time-past checkin-exact checkout-auto uk-grid uk-grid-collapse uk-text-left job">
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'jobber' | translate}}</label>
              <span class="form-value jc-uname inactive">{{ param.jobber.uname }}</span>
            </div>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Info' | translate}}</label>
              
              <span class="form-value"><span class="jc-from jobber-info">{{param.jobber.job.info ? param.jobber.job.info.message : '-'}}</span>
                  </span>
              
             
            </div>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Plan' | translate}}</label>
              <span class="form-value"><span class="jc-from">{{ param.jobber.job.from }}</span> –
                  <span class="jc-to">{{ param.jobber.job.to }}</span>
                  </span>
            </div>
          </div>
          <hr>
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Check_in' | translate}}</label>
              <span class="form-value">
                    <span class="jc-checkin">{{ param.jobber.job.tciuser ? getTime(param.jobber.job.tciuser) : '-' }}</span>
                    <span class="jc-syscheckin">{{param.jobber.job.citime ? '(' + getTime(param.jobber.job.citime) + ')' : param.jobber.job.checkIn ? '-' : '-'}}</span>
                  </span>
            </div>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Check_out' | translate}}</label>
              <span class="form-value">
                    <span class="jc-checkout">
                      {{ param.jobber.job.auto ? getTime(param.jobber.job.checkOut) : param.jobber.job.tcouser ? getTime(param.jobber.job.tcouser) : '-' }}
                    </span>
                    <span class="jc-syscheckout">{{calcAutoC(param.jobber.job)}}</span>
                  </span>
            </div>
          </div> 
          <div class="uk-width-1-1" *ngIf="param.jobber.job.idleuser">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Break'}}</label>
              <span class="form-value">
                    <span class="jc-checkout">
                      {{ param.jobber.job.idleuser }}
                    </span>
                  </span>
            </div>
          </div>
          <hr>
          <div *ngIf="latestChanged" class="uk-width-1-1 managertime">
            <div class="uk-grid uk-grid-collapse">
              <div class="uk-width-4-10">
                <label class="uk-form-label">{{'Manager' | translate}}</label>
              </div>
              <div class="form-value uk-width-6-10">
                <div [ngSwitch]="latestChanged.status" class="uk-grid">
                  <template [ngSwitchCase]="'amend'">
                    <div class="uk-width-2-5">
                      <span class="jc-mancheckin">{{latestChanged.checkInTime}}</span> – <span class="jc-mancheckout">{{latestChanged.checkOutTime}}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info" style="cursor:pointer" (click)="showDetails = !showDetails">
                      <span class="uk-text-danger">{{(latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m')}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                  <template [ngSwitchCase]="'accept'">
                   <div class="uk-width-2-5">
                      <span class="jc-mancheckin">{{param.jobber.job.tciuser ? getTime(param.jobber.job.tciuser) : '–'}}</span> – <span class="jc-mancheckout">{{param.jobber.job.tcouser ? getTime(param.jobber.job.tcouser): ''}}</span>
                   </div>
                   <div class="uk-width-3-5 manager-info">
                     <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                     <span> | {{latestChanged.managerName}} </span>                    
                   </div>
                  </template>
                  <template [ngSwitchCase]="'cancel'">
                    <div class="uk-width-2-5">
                      <span class="jc-mancheckin"></span>- -<span class="jc-mancheckout"></span>
                   </div>
                   <div class="uk-width-3-5 manager-info">
                     <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                     <span> | {{latestChanged.managerName}} </span>                    
                   </div>
                  </template>
                </div>
              </div>
              <div *ngIf="showDetails" class="uk-width-1-1">
                <div class="uk-grid" style="line-height: 40px;">
                  <div class="uk-width-4-10">
                    <label class="uk-form-label uk-float-right" style="float: right!important">{{'Check_in' | translate}}</label>
                  </div>
                  <div class="uk-width-6-10 uk-grid">
                    <div class="uk-width-2-5">
                        <span class="jc-mancheckin">{{latestChanged.checkInTime}}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info" style="cursor:pointer">
                      <span class="uk-text-danger">{{(latestChanged.checkInChangedDate||latestChanged.changedDate)|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.checkInChangedName||latestChanged.managerName}} </span>
                    </div>
                  </div>
                </div>
                <div class="uk-grid" style="line-height: 40px; margin-top: 0!important;">
                  <div class="uk-width-4-10">
                    <label class="uk-form-label" style="float: right!important">{{'Check_out' | translate}}</label>
                  </div>
                  <div class="uk-width-6-10 uk-grid">
                    <div class="uk-width-2-5">
                        <span class="jc-mancheckin">{{latestChanged.checkOutTime}}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info" style="cursor:pointer">
                      <span class="uk-text-danger">{{(latestChanged.checkOutChangedDate||latestChanged.changedDate)|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.checkOutChangedName||latestChanged.managerName}} </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div *ngIf="latestChanged" class="uk-width-1-1 managertime">
             <div class="uk-grid uk-grid-collapse">
               <div class="uk-width-4-10">
                <label class="uk-form-label">{{'Break' | translate}}</label>
              </div>
              <div class="form-value uk-width-6-10">
                <div [ngSwitch]="latestChanged.status" class="uk-grid">
                  <template [ngSwitchCase]="'amend'">
                    <div class="uk-width-2-5" >
                     <span class="jc-mancheckin">{{latestChanged.idleTime}}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{(latestChanged.idleChangedDate||latestChanged.changedDate)|timestamp:this.lang:'D.M.Y, H:m' }}</span>
                      <span> | {{latestChanged.idleChangedName || latestChanged.managerName}} </span>
                    </div>
                  </template>
                  <template [ngSwitchCase]="'accept'"> 
                    <div class="uk-width-2-5">
                       <span class="jc-mancheckin">{{latestChanged.idleTime ? latestChanged.idleTime : '- -'}}</span>
                    </div>
                    <div *ngIf="latestChanged.idleTime" class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                  <template [ngSwitchCase]="'cancel'">
                    <div class="uk-width-2-5">
                        <span class="jc-mancheckin"></span>- -<span class="jc-mancheckout"></span>
                    </div>
                    <div class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                </div>
              </div>
             </div>
          </div>
          <!--<div *ngIf="latestChanged" class="uk-width-1-1 managertime">
            <div class="uk-grid uk-grid-collapse">
           
              <div class="uk-width-4-10">
                <label class="uk-form-label">{{'Manager' | translate}}</label>
              </div>
              
              
              <div class="form-value uk-width-6-10">
                <div [ngSwitch]="latestChanged.status" class="uk-grid">
                  <template [ngSwitchCase]="'amend'">
                    <template [ngIf]="latestChanged.checkOutIsSame">
                      <div class="uk-width-2-5">
                        <span class="jc-mancheckin">{{latestChanged.checkInTime}}</span> – <span class="jc-mancheckout">{{latestChanged.checkOutTime}}</span>
                      </div>
                      <div class="uk-width-3-5 manager-info">
                        <span class="uk-text-danger">{{(latestChanged.changedDate||latestChanged.checkInChangedDate)|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                        <span> | {{latestChanged.checkInChangedName || latestChanged.managerName}} </span>
                      </div>
                    </template>
                    <template [ngIf]="!latestChanged.checkOutIsSame">
                         <div class="uk-width-1-1  manager-info">
                            <span class="jc-mancheckin uk-width-2-5 uk-float-left uk-margin-left" 
                                  style="font-size: 1.6rem;">{{latestChanged.checkInTime}}</span>
                            <span class="uk-text-danger ">{{(latestChanged.changedDate||latestChanged.checkInChangedDate)|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                            <span > | {{latestChanged.checkInChangedName || latestChanged.managerName}} </span>
                         </div>     
                         <div class="uk-width-1-1  manager-info" >
                             <span class="jc-mancheckout uk-width-2-5 uk-float-left uk-margin-left" 
                                   style="font-size: 1.6rem;">{{latestChanged.checkOutTime}}</span>
                             <span class="uk-text-danger">{{latestChanged.checkOutChangedDate |timestamp:this.lang:'D.M.Y, H:m'}}</span>
                             <span > | {{latestChanged.checkOutChangedName}} </span>
                          </div>
                    </template>
                    
                  </template>
                 
                  <template [ngSwitchCase]="'accept'">
                    <div class="uk-width-1-1 ">
                      <div class="uk-width-1-1">
                         <span class="jc-checkin">{{ param.jobber.job.tciuser ? getTime(param.jobber.job.tciuser) : '-' }}</span> - GG
                         <span class="jc-syscheckin">{{param.jobber.job.citime ? ' (' + getTime(param.jobber.job.citime) + ')' : param.jobber.job.checkIn ? '-' : '-'}}</span>
                      </div>
                      <div class="uk-width-1-1 manager-info">
                        <span class="uk-width-1-1">
                            <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                            <span> | {{latestChanged.managerName}} </span>
                        </span>
                      </div>
                    </div>
                    <div class="uk-width-1-1">
                      &lt;!&ndash;<div class="uk-width-2-5">
                       <span class="jc-checkout">
                          {{ param.jobber.job.auto ? getTime(param.jobber.job.checkOut) : param.jobber.job.tcouser ? getTime(param.jobber.job.tcouser) : ' - ' }}
                        </span>
                        <span class="jc-syscheckout">{{calcAutoC(param.jobber.job)}}</span>  
                      </div>
                      <div class="uk-width-3-5 manager-info">
                       <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                          <span> | {{latestChanged.managerName}} </span>  
                      </div>&ndash;&gt;
                     
                      <div class="uk-widtg-1-1">
                        <span class="jc-checkout">
                          {{ param.jobber.job.auto ? getTime(param.jobber.job.checkOut) : param.jobber.job.tcouser ? getTime(param.jobber.job.tcouser) : ' - ' }}
                        </span>
                        <span class="jc-syscheckout">{{calcAutoC(param.jobber.job)}}</span>  
                      </div>
                      <div class="uk-width-1-1 manager-info">
                        <span>
                          <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                          <span> | {{latestChanged.managerName}} </span>           
                        </span>           
                      </div>
                    </div>
                   
                  </template>
                  
                  <template [ngSwitchCase]="'cancel'">
                    <div class="uk-width-2-5">
                        <span class="jc-mancheckin"></span>- -<span class="jc-mancheckout"></span>
                    </div>
                    <div class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                </div>   
              </div>       
            </div>
          </div>-->
          <!--<div *ngIf="latestChanged" class="uk-width-1-1 managertime">
            <div class="uk-grid uk-grid-collapse">
              <div class="uk-width-4-10">
                <label class="uk-form-label">{{'Break' | translate}}</label>
              </div>
              <div class="form-value uk-width-6-10">
                <div [ngSwitch]="latestChanged.status" class="uk-grid">
                  <template [ngSwitchCase]="'amend'">
                    <div class="uk-width-2-5" >
                     <span class="jc-mancheckin">{{latestChanged.idleTime}}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info" [style.paddingLeft]=" latestChanged.checkOutIsSame? '' : '15px'">
                      <span class="uk-text-danger">{{(latestChanged.changedDate||latestChanged.idleChangedDate)|timestamp:this.lang:'D.M.Y, H:m' }}</span>
                      <span> | {{latestChanged.managerName || latestChanged.idleChangedName}} </span>
                    </div>
                  </template>
                  <template [ngSwitchCase]="'accept'"> 
                    <div class="uk-width-2-5">
                       <span class="jc-mancheckin">{{ param.jobber.job.idlemanager }}</span>
                    </div>
                    <div class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                  <template [ngSwitchCase]="'cancel'">
                    <div class="uk-width-2-5">
                        <span class="jc-mancheckin"></span>- -<span class="jc-mancheckout"></span>
                    </div>
                    <div class="uk-width-3-5 manager-info">
                      <span class="uk-text-danger">{{latestChanged.changedDate|timestamp:this.lang:'D.M.Y, H:m'}}</span>
                      <span> | {{latestChanged.managerName}} </span>
                    </div>
                  </template>
                </div>
              </div>
            </div>
          </div>-->
          <hr>
          <div class="uk-form-row uk-width-1-1">
            <label for="jobreviewmodeaccept" [style.color]="canAccept?'black':'#c3c3c3'">
              <input   type="radio" formControlName = "status"
                      id="jobreviewmodeaccept" name="status"
                      [disabled]="!canAccept"
                      value="accept" > <!--[checked]="param.jobber.job.status === 'OK'"-->
              <span >{{'choice_jobreview_ok' | translate}}</span>
            </label>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <label for="jobreviewmodecancel">
              <input  type="radio" formControlName = "status"
                     id="jobreviewmodecancel" name="status"
                     value="cancel">  <!--[checked]="(param.jobber.job.isLate || param.jobber.job.isClarify) && !param.jobber.job.checkIn"-->
              <span>{{'choice_jobreview_cancelled' | translate}}</span>
            </label>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <label for="jobreviewmodeamend">
              <input type="radio" formControlName = "status"  
                    id="jobreviewmodeamend" name="status"
                     value="amend"> <!--[checked]="(param.jobber.job.status === 'approve' || ((param.jobber.job.isLate || param.jobber.job.isClarify) && param.jobber.job.checkIn))"-->
              <span>{{'choice_jobreview_changed' | translate}}</span>
            </label>
          </div>
          
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'In_Out' | translate}}</label>
              <div class="uk-width-small-1-1 uk-width-medium-6-10">
                <div id="jobreviewtime" class="timerangeexact uk-width-1-1">
                  <div class="timerangeexact uk-grid uk-grid-collapse">
                    <div class="timefrom">
                      <div class="timedialexact">
                        <div class="uk-grid uk-grid uk-grid-collapse">
                          <div class="hh">
                            <div class="select-wrapper uk-width-1-1">
                              <select   [disabled]="status !== 'amend'"
                                        formControlName="checkin1" class="select timehh">
                                <option *ngFor="let opt of arrTH" value="{{opt}}" >{{ opt }}</option>
                              </select>
                            </div>
                          </div>
                          <div class="dial-divider uk-text-center">:</div>
                          <div class="mm">
                            <div class="select-wrapper uk-width-1-1">
                              <select [disabled]="status !== 'amend'"
                                      formControlName="checkin2" class="select timemm">
                                <option *ngFor="let opt of arrT" value="{{opt}}">{{opt }}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-divider uk-text-center">‐</div>
                    <div class="timeto">
                      <div class="timedialexact">
                        <div class="uk-grid uk-grid uk-grid-collapse">
                          <div class="hh">
                            <div class="select-wrapper uk-width-1-1">
                              <select [disabled]=" status !== 'amend' || disabledCheckOut(param.jobber.job)" 
                                      formControlName="checkout1" 
                                      class="select timehh">
                                <option *ngFor="let opt of arrTH" value="{{opt}}">{{ opt }}</option>
                              </select>
                            </div>
                          </div>
                          <div class="dial-divider uk-text-center">:</div>
                          <div class="mm">
                            <div class="select-wrapper uk-width-1-1">
                              <select [disabled]=" status !== 'amend' || disabledCheckOut(param.jobber.job)"
                                      formControlName="checkout2" class="select timehh">
                                <option *ngFor="let opt of arrT" value="{{opt}}">{{ opt }}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{'Break' | translate}}</label>
              <div class="uk-width-small-1-1 uk-width-medium-3-10">
                <div id="jobreviewidle" class="timedialexact">
                  <div class="uk-grid uk-grid uk-grid-collapse">
                    <div class="hh">
                      <div class="select-wrapper uk-width-1-1 ws-success">
                        <select (change)="onChange($event.target.value, 'idle', 0)"
                                formControlName="idle1"
                                [disabled]="status !== 'amend'"
                                class="select timehh user-success">
                          <!-- filled by js -->
                          <option *ngFor="let opt of arrTH" value="{{opt}}">{{ opt }}</option>
                        </select>
                      </div>
                    </div>
                    <div class="dial-divider uk-text-center">:</div>
                    <div class="mm">
                      <div class="select-wrapper uk-width-1-1">
                        <select (change)="onChange($event.target.value, 'idle', 1)" 
                                formControlName="idle2"
                                [disabled]="status !== 'amend'"
                                class="select timemm">
                          <!-- filled by js -->
                            <option *ngFor="let opt of arrT" value="{{opt}}">{{ opt }}</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div *ngIf="invalidBreak" class="uk-width-small-4-1 uk-width-medium-4-1 uk-container-center uk-grid-collapse  uk-margin-top alertbox">
            <div class="uk-alert uk-alert-danger error" data-uk-alert="">
              <a (click)="invalidBreak = !invalidBreak" class="uk-alert-close uk-close"></a>
               {{'Break_is_bigger'|translate}}
            </div>
          </div>
          <div class="uk-width-1-1 uk-margin-large-top uk-margin-bottom">
            <div class="uk-grid uk-grid-small">
              <div class="uk-width-1-2 uk-margin-bottom">
                <button (click)="submit($event, param.jobber.job);" type="submit"
                        class="uk-button uk-button-large ok uk-width-1-1">OK
                </button>
              </div>
              <div class="uk-width-1-2 uk-margin-bottom">
                <button (click)="closeModal();" type="button"
                        class="uk-button uk-button-large uk-modal-close cancel uk-width-1-1">
                  {{'button_Cancel' | translate}}
                </button>
              </div>
            </div>
          </div>
        </div>
        <!--UK-GRID-->
      </form>


    </div>
  </div>
</section>`;

import { Component, Input }   from '@angular/core';
import { timingModalHtml }    from './modal.html';
import { MapToIterable }      from '../../../lib/pipes/map-to-iterable';
import * as moment            from 'moment';
import { AppStoreAction }     from '../../../store/action';
import { TargetJobService }   from '../../../services/job.service';
import { JobService }         from '../../../services/shared/JobService';
import * as R                 from 'ramda';
import { leftPad }            from '../../../lib/utils';
import { FormGroup,
        FormControl }         from '@angular/forms';

import { select }             from 'ng2-redux/lib/index';
import { DestroySubscribers } from '../../../lib/decorators/unsub.decorator';



const ZERO_FILL: string = '00';
const DEFAULT_STATUS: string = 'amend';
const AMEND_STATUS: string = 'amend';
const LAST_SHIFT_IDX: number = 3;
const HH_MM_FORMAT: string = 'HH:mm';
const ZERO: number = 0;
const HOURS: number = 24;
const MINUTES: number = 60;

@Component({
  selector: 'modal-checkin-timing',
  pipes: [MapToIterable],
  styles: [`
      hr {
        width: 100%;
      }
      .form-value {
        line-height: 40px;
      }
      .manager-info {
        padding: 0;
        font-size: 1.25rem;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
      .jobber-info {
        white-space: pre-wrap;
      }
  `],
  template: timingModalHtml
})
@DestroySubscribers()

export class ModalTimingComponent {

  @select() currentUnit$: any;

  @Input() param;

  public status: any;
  public checkin1: string = ZERO_FILL;
  public checkin2: string = ZERO_FILL;
  public checkout1: string = ZERO_FILL;
  public checkout2: string = ZERO_FILL;
  public idle1: string = ZERO_FILL;
  public idle2: string = ZERO_FILL;
  public arrT: string[] = [];
  public arrTH: string[] = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public correcting: any;

  public subscribers: any = {};

  private staff: any;
  private invalidBreak: boolean;
  private diffString;
  private correctionStatus: 'accept' | 'amend' | 'cancel';
  private correctionTs: number = 0;
  private correctionManager: { idle: string; tcimanager: number; tcomanager: number};
  private correctionExist: boolean = false;
  private correctionManagerName: string;
  private canAccept: boolean;
  private latestChanged: any;
  private lastLog: any;

  private correctTimeForm = new FormGroup({
    status: new  FormControl(),
    checkin1: new  FormControl(ZERO_FILL),
    checkin2: new  FormControl(ZERO_FILL),
    checkout1: new  FormControl(ZERO_FILL),
    checkout2: new  FormControl(ZERO_FILL),
    idle1: new  FormControl(ZERO_FILL),
    idle2: new  FormControl(ZERO_FILL)
  });
  constructor(public action: AppStoreAction, public targetJob: TargetJobService,
              public jobService: JobService) {

    const minutes = R.range(ZERO, MINUTES);
    const hours = R.range(ZERO, HOURS);

    this.arrT = minutes.map(leftPad);
    this.arrTH = hours.map(leftPad);

  }

  ngOnInit() {
    this.subscribers = {
      [Symbol('unit')]: this.currentUnit$.subscribe(unit => {
        this.staff = unit.staff;
      })
    };

    this.correctTimeForm.valueChanges.subscribe(el => {
      this.status = el.status;
    });

    this.correctionExist = this.param.jobber.job.log && this.param.jobber.job.log.length > 0;
    if (this.correctionExist) {
      this.latestChanged = this.whatManagersChanged(this.param.jobber.job.log);
      /*if (latestChanged) {
        this.param.jobber.job = Object.assign({}, this.param.jobber.job,
            { latestChanged : latestChanged });
      }*/

      let findChanges = R.pipe(
        R.reverse,
        R.take(2),
        R.reverse,
        x => R.difference.apply(null, x)
      );

      let changes = findChanges(this.param.jobber.job.log);
      // console.log(changes);
      this.correctionStatus = this.param.jobber.job.log[this.param.jobber.job.log.length - 1][0];
      this.canAccept = this.param.jobber.job.checkIn && this.param.jobber.job.tciuser;
      if (this.correctionStatus === 'accept'
        || this.correctionStatus ===  'amend'
        || this.correctionStatus === 'cancel') {
        this.correctTimeForm.patchValue({status : this.correctionStatus});
      } else if (this.correctionStatus === 'declined') {
        this.correctTimeForm.patchValue({status : 'cancel'});
      }else {
        this.correctTimeForm.patchValue({status : 'amend'});
      }
      this.correctionManager = this.param.jobber.job.log[this.param.jobber.job.log.length - 1][1];
      this.correctionTs = this.param.jobber.job.log[this.param.jobber.job.log.length - 1][3];
      this.correctionManagerName = this.staff[this.param.jobber.job.log[this.param.jobber.job.log.length - 1][4]]; // tslint:disable

    } else {
      this.correctTimeForm.patchValue({status : !this.canAccept ? 'cancel' : 'amend'});
    }

    if (this.param) {

      if (this.param.jobber.job.checkIn) {
        this.correctTimeForm.patchValue({
          checkin1: moment(this.param.jobber.job.checkIn).format('HH'),
          checkin2: moment(this.param.jobber.job.checkIn).format('mm')
        });
      } else {
        let [hh, mm] = this.param.jobber.job.from.split(':');
        this.correctTimeForm.patchValue({
          checkin1: hh,
          checkin2: mm
        });
      }

      if (this.param.jobber.job.tciuser) {
        this.correctTimeForm.patchValue({status : 'accept'});
      }

      if (this.param.jobber.job.checkOut) {
        this.correctTimeForm.patchValue({
          checkout1: moment(this.param.jobber.job.checkOut).format('HH'),
          checkout2:  moment(this.param.jobber.job.checkOut).format('mm')
        });
      } else {
        let [hh, mm] = this.param.jobber.job.to.split(':');
        this.correctTimeForm.patchValue({
          checkout1: hh,
          checkout2: mm
        });
      }

      if (this.param.jobber.job.idle) {
        let [hh, mm] = this.param.jobber.job.idle.split(':');
        this.correctTimeForm.patchValue({
          idle1: hh,
          idle2: mm
        });
      }

      this.correcting = this.param.jobber.job.log ?
        this.param.jobber.job.log
          .filter(el => el[0] === AMEND_STATUS)
          .sort((a, b) => {
            return a > b ? 1 : -1;
          }) : [];
    }

  }

  whatManagersChanged(log) {
    let last = log[log.length - 1],
        penultimate = log.length > 1 ? log[log.length - 2] : '',
        res;

    if (!this.isManagerChanged(last)) return res;

    if (log.length === 1 || (penultimate && !this.isManagerChanged(penultimate))) {
      if (last[0] === 'amend') {
        return res = Object.assign({},
          { status: last[0] ? last[0] : '',
            checkInTime :
              last[1] && last[1].tcimanager ? this.getTime(last[1].tcimanager) : '',
            checkOutTime :
              last[1] && last[1].tcomanager ? this.getTime(last[1].tcomanager) : '',
            idleTime:
              last[1] && last[1].idle ? last[1].idle : '',
            changedDate: last[3] ? last[3] : '',
            managerId: last[4] ? last[4] : '',
            managerName: last[5] ? last[5] : '',
            checkOutIsSame: true});
      }else {
        return res = this.acceptOrCancelStat(last);
      }
    }else if (last[0] === 'amend') {
      return res = this.checkDifferentChanges(log);
    }else {
      return res = this.acceptOrCancelStat(last);
    }
  }

  isManagerChanged(log: any) {
    return log[0] === 'accept' ||
      log[0] === 'accepted' ||
      log[0] === 'amend' ||
      log[0] === 'cancel' ?
      true : false
  }

  amendStatus(log) {

  }

  acceptOrCancelStat(log) {
   let a = Object.assign({},
      { status : log[0] ? log[0] : '',
        changedDate: log[3] ? log[3] : '',
        checkInTime: log[1] && log[1].tcimanager? this.getTime(log[1].tcimanager) : '',
        checkOutTime:  log[1] && log[1].tcomanager? this.getTime(log[1].tcomanager) : '',
        idleTime: log[1] && log[1].idle ? log[1].idle : '',
        managerId: log[4] ? log[4] : '',
        managerName : log[5] ? log[5] : ''});
    return a;
  }

  checkDifferentChanges(log) {
    let startFrom: any,
        cIn, cInInfo,
        cOut, cOutInfo,
        cIdle, cIdleInfo,
        res;
    let filtered =
      log.filter(el => this.isManagerChanged(el))
         .forEach((el, index) => {
           if (el[0] === 'amend') {
              if (cIn !== el[1].tcimanager) {
                cIn = el[1].tcimanager;
                cInInfo = el;
              }
             if (cOut !== el[1].tcomanager) {
               cOut = el[1].tcomanager;
               cOutInfo = el;
             }
             if (cIdle !== el[1].idle) {
               cIdle = el[1].idle;
               cIdleInfo = el;
             }
           }else {
             cIn = cOut = cIdle = '';
             cInInfo = cOutInfo = cIdleInfo = '';
           }
        });
    res = {
      status : log[log.length - 1][0],
      checkInTime : this.getTime(cIn),
      checkInChangedName: cInInfo[5],
      checkInChangedId: cInInfo[4],
      checkInChangedDate: cInInfo[3],
      checkOutTime : this.getTime(cOut),
      checkOutChangedName: cOutInfo[5],
      checkOutChangedId: cOutInfo[4],
      checkOutChangedDate:  cOutInfo[3],
      idleTime: cIdle,
      idleChangedName:  cIdleInfo[5],
      idleChangedId: cIdleInfo[4],
      idleChangedDate: cIdleInfo[3]
    };
    console.log(res);
    let checkInFirst = res.checkOutChangedDate < res.checkInChangedDate,
        id = checkInFirst ? cInInfo[4] : cOutInfo[4],
        mName = checkInFirst ? cInInfo[5] : cOutInfo[5],
        date = checkInFirst ? cInInfo[3] : cOutInfo[3];
    res = Object.assign({}, res, {
      managerId: id,
      managerName: mName,
      changedDate: date
    });
  return res;
  }

  getTime(time) {
    return moment(time).format(HH_MM_FORMAT);
  }

  calcAutoC(job) {
    if (job.auto) return '(auto)';
    if (job.cotime) return `(${this.getTime(job.cotime)})`;
    return '-';
  }


  submit(e, job) {
    e.preventDefault();
    let formVal = this.correctTimeForm.value;
    this.invalidBreak = false;
    let miliIn = moment(`${formVal.checkin1}:${formVal.checkin2}`, HH_MM_FORMAT),
        miliOut = this.getMiliOut(this.param.jobber.job.shift_idx, miliIn);

    let params = {
      checkin     : miliIn.format(HH_MM_FORMAT),
      checkout    : miliOut.format(HH_MM_FORMAT),
      idle        : `${formVal.idle1}:${formVal.idle2}`,
      mode        :  formVal.status,
      position_id : `${this.param.position_id}`,
      shift_idx   : `${this.param.jobber.job.shift_idx}`,
      webuser_id  : `${this.param.jobber.job.webuser_id}`,
      week        : `${this.param.week}`,
      weekday     : `${this.param.jobber.job.weekday}`
    };

    let diff = miliOut.valueOf() - miliIn.valueOf();
    this.diffString = this.jobService.getTimeFromDiff(diff);

   /* if (formVal.status === 'amend' && this.disabledCheckOut(job)) {
      delete params.checkout
    }*/

    if (this.diffString <= params.idle ) {
      this.invalidBreak = true;
    } else {
      // TODO fetch request to adjust
      console.log(params);
      this.targetJob.adjust(params).subscribe(res => {
        this.closeModal(true);
      });
    }

  }

  getMiliOut(shift, checkIn) {
    let formVal = this.correctTimeForm.value;
    let checkOut = moment(`${formVal.checkout1}:${formVal.checkout2}`, HH_MM_FORMAT);
    if (shift === LAST_SHIFT_IDX || checkOut.valueOf() < checkIn.valueOf()) {
      checkOut.add(1, 'day');
    }
    return checkOut;
  }

  closeModal(updated) {
    if (updated) {
      this.action.closeModalTiming(true);
    }
    this.action.closeModalTiming(false);
  }

  onChange(value, key, part) {
    if (part === 0) {
      this[`${key}1`] = value;
    } else {
      this[`${key}2`] = value;
    }
  }

  disabledCheckOut(job) {
    return job.tciuser && !job.tcouser
  }

  ngOnDestroy() {
    this.hideScrollBar();
  }

}

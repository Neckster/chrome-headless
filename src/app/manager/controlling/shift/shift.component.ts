import { Component, Input } from '@angular/core';
import { shiftHtml } from './shift.html';
import { PositionComponent } from '../position/position.component';
import { MapToIterable } from '../../../lib/pipes/map-to-iterable';
import { SortNamePipe } from '../../../lib/pipes/sortName';
import { MinusZeroPipe } from '../../../lib/pipes/minusZero';
import { JobService } from '../../../services/shared/JobService';
import { Cache } from '../../../services/shared/cache.service';

@Component({
  selector: 'shift',
  providers: [ ],
  directives: [PositionComponent],
  pipes: [ MapToIterable, SortNamePipe, MinusZeroPipe ],
  styles: [ ],
  template: shiftHtml
})

export class ShiftComponent {
  // Set our default values
  @Input() shift: any;
  @Input() unit: any;
  @Input() shiftIndex: any;
  @Input() day: any;
  // TypeScript public modifiers
  constructor(public jobService: JobService,
              public cache: Cache) {
  }

  ngOnInit() {
    this.shift.open = this.cache.isInArray(
      'controlling:shiftOpened',
      `${this.shiftIndex}-${this.unit.nname}`
    );
  }

  toggleCollapsible() {
    if (this.shift.open) {
      this.cache.removeFromArray(
        'controlling:shiftOpened',
        `${this.shiftIndex}-${this.unit.nname}`
      );
    } else {
      this.cache.pushArray(
        'controlling:shiftOpened',
        `${this.shiftIndex}-${this.unit.nname}`
      );
    }

    this.shift.open = !this.shift.open;
  }

  valid(pos, i) {
    return pos.jobbers.filter(el => {return el.job.shift_idx !== i; }).length > 0;
  }

  calcClarify() {
    let count = this.shift.positions.map(pos => {
        return pos.jobbers.filter(el => {return el.job.isClarify; }).length;
    }).reduce((a, b) => {return a + b; }, 0);
    return count > 0 ? count : '';
  }

}

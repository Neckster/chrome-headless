//noinspection TsLint
export const shiftHtml = `

              <!--Shift values-->
              <div class="uk-nestable-panel">
                <div class="uk-grid uk-grid-collapse">
                  <div class="nestable-body">
                    <div class="uk-grid uk-grid-collapse">
                    <div  class="icon large  blue" [ngClass]='{"arrow-down":shift.open, "arrow-right":!shift.open}'(click)="toggleCollapsible()"  style='cursor:pointer; position:absolute; left:5px'></div>
                      <div class="uk-width-2-10 jc-sname colhead">
                        {{ shift.name | translate }}
                       </div>
                      <div class="uk-width-8-10 uk-text-center grid-table">
                        <div class="uk-grid uk-grid-collapse">
                        
                          <div class="uk-width-3-10">
                            <div class="uk-grid uk-grid-collapse">
                              
                              <!--Plan-->
                              <div class="uk-width-4-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-1-3 b-r">
                                    <span class="uk-badge uk-badge-notification jc-badge jc-onduty">{{shift.bubbles?.working === 0 ? '' : shift.bubbles?.working}}</span>
                                  </div>
                                  <div class="uk-width-1-3 b-r">
                                    <span
                                      class="uk-badge uk-badge-notification uk-badge-danger jc-badge jc-late">{{shift.bubbles?.late === 0 ? '' : shift.bubbles?.late}}</span>
                                  </div>
                                  <div class="uk-width-1-3 b-r">
                                    <span class="uk-badge uk-badge-notification uk-badge-warning jc-badge jc-changed">{{shift.bubbles?.clarify === 0 ? '' : shift.bubbles?.clarify}}</span>
                                  </div>
                                </div>
                              </div>
                              
                              <div class="uk-width-3-10 b-r"><span><!--IN--></span></div>
                              <div class="uk-width-3-10 b-r"><span><!--OUT--></span></div>
                            </div>
                            <!-- uk-grid uk-grid-collapse -->
                          </div>
                          
                          <!--Breack-->
                          <div class="uk-width-1-10 b-r">
                            <!--<span class="jc-idle">{{shift.hours.breakh}}</span>-->
                          </div>
                          <!-- uk-width-2-10 -->
                          
                          <!--Comment-->
                          <div class="uk-width-2-10 b-r">&nbsp;</div>
                          <!-- uk-width-2-10 -->
                          <!--Hours-->
                          <div class="uk-width-4-10">
                            <div class="uk-grid uk-grid-collapse">
                              <div class="uk-width-1-4 b-r"><span class="jc-planned">{{shift.hours?.planned}}</span></div>
                              <div class="uk-width-1-4 b-r"><span class="jc-time">{{shift.hours?.expected}}</span></div>
                              <div class="uk-width-1-4 b-r"><span class="jc-diff">{{shift.hours?.diff | minusZero}}</span></div>
                              <div class="uk-width-1-4 b-r"><span class="jc-realnet">{{shift.hours?.actual }}</span></div>
                            </div>
                          </div>
                          <!-- uk-width-4-10 -->
                        </div>
                        <!-- uk-grid -->
                      </div>
                      <!-- uk-width-1-7 -->
                    </div>
                    <!-- uk-grid item 2 -->
                  </div>
                </div>
              </div>
              <!-- uk-nestable-panel -->
              
              <!--Position list-->
              <ul class="uk-nestable-list jc-template-append-controlling-position" *ngIf="shift.open">
                <!--Position-->
                <position *ngFor="let position of shift.positions | sortName:'position.pname'"  [position]="position" [unit]="unit" [day]="day" [shiftIndex]="shiftIndex"></position>
                <!--Position End-->
              </ul>
              <!-- uk-nestable-list -->
              
`;

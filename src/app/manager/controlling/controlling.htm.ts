//noinspection TsLint
export const htmlControlling = `

<div *ngIf="modalParam">
<modal-checkin-timing [param]='modalParam'></modal-checkin-timing>
</div>
<div *ngIf="day != undefined && unit" class="controlling" style='padding: 0 1rem'>
  <!--HEAD-->
  <div class="uk-width-1-1 uk-container-center">
    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-bottom ">
      <div class="uk-width-small-1-2 uk-width-large-2-10 uk-width-xlarge-1-6">
        <h1 class="uk-text-left">{{'Check_in' | translate}}</h1>
      </div>
    </div>
    <!-- nested uk-grid -->
  </div>
 
  <div class="uk-width-1-1 uk-margin-top uk-container-center">
    <div id="controlling-grid" class="uk-grid uk-grid-collapse">
        <div data-uk-sticky="{top:120}"
          class="uk-width-small-6-10 uk-width-medium-4-10 uk-width-large-3-10 dayhead nosel gridleft"
          style="margin: 0px;height: 5rem;">
          <div class="kw-info uk-flex uk-flex-space-between uk-flex-middle" style="height: 5rem;">
            <div class="kw-action uk-width-small-5-10 uk-width-large-4-10">
              <div class="uk-grid uk-grid-collapse">
                <span (click)="loadWeek(-1)"
                      class="icon arrow-left uk-width-2-10 uk-visible-large jc-week-prev"><span
                  class="label">{{'last_week' | translate}}</span></span>
                <span (click)="loadWeek(-1)" class="icon arrow-left uk-width-2-10 uk-hidden-large jc-day-prev"><span
                  class="label">{{'yesterday' | translate}}</span></span>
                <span (click)="loadWeek(0)" class="today uk-width-6-10"><span
                  class="text jc-today">{{'today' | translate}}</span>
                <span class="date">Mo, 16. May 2016</span>
                </span>
                <span (click)="loadWeek(1)"
                      class="icon arrow-right uk-width-2-10 uk-visible-large jc-week-next"><span
                  class="label">{{'next_week' | translate}}</span></span>
                <span (click)="loadWeek(1)" class="icon arrow-right uk-width-2-10 uk-hidden-large jc-day-next"><span
                  class="label">{{'tomorrow' | translate}}</span></span>
              </div>
            </div>
            <div class="uk-width-small-2-10 uk-width-large-2-10 align-center" >
                <div style="position:relative;padding-left: 50%; max-width:30px">
                  <i   (click)="showCalendar()" [svgTemplate]="'calendar'" style="cursor: pointer; "></i>
                  <div *ngIf="showCalendarVar">
                    <my-date-picker (selectedDate)='setDate($event)' 
                                    [valueDate]="valueDate"
                                    [valueDay]="valueDay"  
                                    [dayIn]="day"></my-date-picker>
                  </div>
                </div>
              </div>
            <div class="kw-date uk-width-small-5-10 uk-width-large-5-10">
              <h2>{{showMonth}}</h2>
              <h3>{{'KW' | translate}} {{currentMonthISOWeek}}/{{currentMonthISOWeekYear}}</h3>
            </div>
          </div>
        </div>
        <div data-uk-sticky="{top:120}"
          class="uk-width-small-4-10 uk-width-medium-6-10 uk-width-large-7-10 dayhead nosel gridright"
          style="margin: 0px;">
          <div class="kw-days jc-template-append-calendar-head-row">
            <div class="uk-width-1-1">
              <div class="uk-grid uk-grid-collapse sevendays">
                <div class="uk-width-1-1" >
                  <!--Day Slecet Values-->
                  <div *ngFor="let a of acc;let i = index" (click)="changeDay(i+1)"
                       [ngClass]="{today: isToday(i+1)}" class="calday day-{{i}} caldayWrap" >
                    <span class="wday">{{a[0]}}</span>
                    <span class="mday">{{a[1]}}</span>
                    <div *ngIf="unit.bubbles && unit.bubbles.bubblesOnDayArr"
                          [style.visibility]="totalBubbles[i].clarify.length?'':'hidden'"
                          class="clarifyBubbleOnDay">
                      <span>{{totalBubbles[i].clarify.length}}</span>
                    </div>
                  </div>

                </div>
              </div>
              <!-- sevendays uk-grid  -->
            </div>
          </div>
        </div>
        <!--
      <div class="uk-sticky-placeholder" style="height: 50px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 51px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 50px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 51px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 50px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 51px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 50px; margin: 0px;"></div>
      <div class="uk-sticky-placeholder" style="height: 51px; margin: 0px;"></div>
      -->
      <div class="uk-container uk-container-center uk-width-1-1">
        <span
          class="uk-width-1-1 tip uk-hidden-large">{{'swipe_left_right' | translate}}</span>
      </div>
      <div
        class="uk-container uk-container-center uk-width-1-1 uk-padding-remove uk-position-relative">
        <div class="uk-overflow-container ">
          <ul id="controlling-list" class="uk-nestable">

            <!--Unit and Total-->
            <li class="uk-nestable-item uk-parent uk-nestable-nodrag jc-template-controlling-head">
              <!--MAIN heading uk-nestable-panel -->
              <div class="uk-nestable-panel">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-1-1 main-heading">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="nestable-body">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-2-10 colhead"><span class="unit-head selected nname">{{ unit.nname  }}</span>
                          </div>
                          <div class="uk-width-8-10 uk-text-center grid-table">
                            <div class="uk-grid uk-grid-collapse">

                              <div class="uk-width-3-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-4-10 b-r">{{'plan' | translate}}</div>
                                  <div class="uk-width-3-10 b-r">{{'In' | translate}}</div>
                                  <div class="uk-width-3-10 b-r">{{'Out' | translate}}</div>
                                </div>
                                <!-- uk-grid uk-grid-collapse -->
                              </div>
                              <div class="uk-width-1-10 b-r">{{'Break' | translate}}</div>
                              <!-- uk-width-2-10 -->

                              <div class="uk-width-2-10 b-r">{{'Comment' | translate}}</div>
                              <!-- uk-width-2-10 -->

                              <div class="uk-width-4-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-1-4 b-r double-line">
                                    <span>{{'abbr_Hours_fullstop' | translate}}</span><br>
                                    <span>{{'planned' | translate}}</span>
                                  </div>
                                  <div class="uk-width-1-4 b-r double-line">
                                    <span>{{'abbr_Hours_fullstop' | translate}}</span><br>
                                    <span>{{'expected' | translate}}</span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">+/-</div>
                                  <div class="uk-width-1-4 b-r">{{'Actual_abbr_hours' | translate}}</div>
                                </div>
                              </div>
                              <!-- uk-width-4-10 -->
                            </div>
                            <!-- uk-grid -->
                          </div>
                          <!-- uk-width-1-7 -->
                        </div>
                        <!-- uk-grid item 2 -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!--MAIN heading uk-nestable-panel -->
              <div class="uk-nestable-panel">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-1-1 main-heading">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="nestable-body">
                        <div class="uk-grid uk-grid-collapse checkInWrap">
                          <div class="uk-width-2-10 colhead" style="height: 5rem">
                            <span class="uk-text-bold">{{'Total' | translate}}</span>
                          </div>
                          <div class="uk-width-8-10 uk-text-center grid-table">
                            <div class="uk-grid uk-grid-collapse jc-controlling-day">
                              <div class="uk-width-3-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-4-10">
                                    <div class="uk-grid uk-grid-collapse">
                                      <div class="uk-width-1-3 b-r"><span
                                        class="jc-onduty">&nbsp;</span></div>
                                      <div class="uk-width-1-3 b-r"><span
                                        class="jc-changed">&nbsp;</span></div>
                                      <div class="uk-width-1-3 b-r"><span
                                        class="jc-late">&nbsp;</span></div>
                                    </div>
                                  </div>
                                  <div class="uk-width-3-10 b-r">&nbsp;</div>
                                  <div class="uk-width-3-10 b-r">&nbsp;</div>
                                </div>
                                <!-- uk-grid uk-grid-collapse -->
                              </div>
                              <div class="uk-width-1-10 b-r">
                                <!-- <span class="jc-idle">{{unit.hours ? unit.hours.breakh : ''}}</span> -->
                              </div>
                              <!-- uk-width-2-10 -->

                              <div class="uk-width-2-10 b-r">&nbsp;</div>
                              <!-- uk-width-2-10 -->
                              <div class="uk-width-4-10">
                                <div class="uk-grid uk-grid-collapse">
                                  <div class="uk-width-1-4 b-r">
                                    <span class="jc-planned uk-text-bold">
                                      {{ unit.hours ? unit.hours.planned : '00:00' }}
                                    </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                    <span class="jc-time uk-text-bold">
                                      {{ unit.hours ? unit.hours.expected : '00:00'}}
                                    </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                     <span class="jc-diff uk-text-bold">
                                       {{ unit.hours ? unit.hours.diff : '00:00'}}
                                     </span>
                                  </div>
                                  <div class="uk-width-1-4 b-r">
                                      <span class="jc-realnet uk-text-bold">
                                        {{ unit.hours ? unit.hours.actual : '00:00'}}
                                      </span>
                                  </div>
                                </div>
                              </div>
                              <!-- uk-width-4-10 -->
                            </div>
                            <!-- uk-grid -->
                          </div>
                          <!-- uk-width-1-7 -->
                        </div>
                        <!--               uk-grid item 2 -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!--heading uk-nestable-panel -->
            </li>
            <!--Unit and Total-->

            <!--Shifts-->

            <li *ngFor="let unitShift of unit.shift; let i = index"
                class="uk-nestable-item uk-nestable-nodrag uk-parent uk-nestable-nodrag nesting jc-template-controlling-shift late"
                id="controlling-shift-{{i}}">
              <shift *ngIf="checkUnitValid(unitShift, i)" [shiftIndex]="i" [shift]="unitShift"
                     [day]="day" [unit]="unit"></shift>
            </li>

          </ul>
        </div>
      </div>
      <!-- uk-overflow-container -->
    </div>
    <!-- controlling grid -->
  </div>
</div>
`;

// noinspection TsLint
export const htmlManager: string = `
<select-unit *ngIf="isManager" [opened]="loginModalOpened" (closeEmitter)="loginModalOpened = false"></select-unit>
<div class="user-jobber user-manager">
  <div class="uk-sticky-placeholder">
    <header id="mainheader" class="uk-contrast uk-sticky-init uk-active" data-uk-sticky="">
      <div class="uk-container uk-container-center uk-padding-remove">
        <nav class="uk-navbar">
          <div *ngIf='isManager' class="uk-navbar-content manager">
            <a class="manager-nav uk-navbar-toggle uk-hidden-xlarge"
               data-uk-offcanvas="{target:'#offcanvas'}"></a>
          </div>
          <a class="uk-navbar-brand brand">jubbr
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5000 2109">jubbr
              <path
                d="M4541 1629h108v-608c0-37 5-73 9-106 30-176 138-307 289-307 21 0 37 0 53 5V507c-13-2-29-4-43-4-151 0-266 110-314 248h-4l-4-222h-101c5 106 9 220 9 330v771zm-1177 0l7-200h5c87 154 218 225 395 225 241 0 491-195 491-587 0-333-192-564-470-564-195 0-330 87-404 230h-4V0h-108v1372c0 83-4 179-9 257h99zm18-645c0-39 7-69 14-96 53-183 207-296 368-296 250 0 388 225 388 477 0 287-144 493-397 493-172 0-314-110-362-278-5-27-9-55-9-80V984zm-1285 645l7-200h5c87 154 218 225 395 225 241 0 491-195 491-587 0-333-193-564-470-564-195 0-330 87-403 230h-5V0h-108v1372c0 83-5 179-9 257h99zm18-645c0-39 7-69 14-96 53-183 207-296 367-296 250 0 388 225 388 477 0 287-145 493-397 493-172 0-314-110-363-278-5-27-9-55-9-80V984zm-479-456h-108v686c0 44-11 87-23 122-46 110-163 223-323 223-218 0-294-172-294-410V528H780v640c0 418 234 486 374 486 197 0 324-119 379-220h5l7 196h101c-5-85-9-172-9-268V528zM370 117c-33 0-64 20-79 49-21 37-15 86 14 116 17 19 43 28 68 26 28-1 55-18 69-42 24-42 15-101-24-131-13-10-30-16-47-16zM14 2109c135-6 278-67 342-192 76-143 62-308 64-465V528H312c-1 351 2 702-1 1052-6 113-3 238-71 335-55 74-151 101-240 105l14 89z"></path>
            </svg>
          </a>
          <div *ngIf="profileSet && isManager" class="manager-nav uk-navbar-content uk-visible-xlarge uk-clearfix">
            <ul class="uk-navbar-nav">
              <li class="header-link">
                <a routerLink="/schedule" (click)="showControllingStat = false" routerLinkActive="router-link-active"> {{ 'Weekly_rota' | translate }} </a>
              </li>
              <li class="header-link">
                <a routerLink="/controlling" (click)="showControllingStat = true" routerLinkActive="router-link-active"> {{ 'Controlling' | translate }} </a>
              </li>
              <li class="header-link">
                <a routerLink="/staff" (click)="showControllingStat = false" routerLinkActive="router-link-active"> {{ 'Staff' | translate }} </a>
              </li>
              <li class="header-link">
                <a routerLink="/reports" (click)="showControllingStat = false" routerLinkActive="router-link-active"> {{ 'Reports' | translate }} </a>
              </li>
              <!-- TODO
              <li class="header-link">
                <a href="#manager-hiring">{{i18n Hiring}}</a>
              </li>
              -->
              <!-- TODO
              <li class="header-link">
                <a href="#manager-report">{{i18n Reports}}</a>
              </li>
              -->
              <li class="manager-nav uk-visible-large uk-parent dropdown"
                  data-uk-dropdown="{mode:'hover', pos:'bottom-right', remaintime: 100}"
                  aria-haspopup="true"
                  aria-expanded="false">
                <a class="uk-padding-remove dropdown-toggle " [ngClass]="{'router-link-active': menuSubcatStatuses.activeSetup }">
                  <i [svgTemplate]="'cogWheelSilhouette'"></i>
                </a>
                <!-- organisation dropdown -->
                <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-bottom"
                     style="top: 71px; left: -165px; display: none;" (click)="showControllingStat = false">
                  <ul class="uk-nav">
                    <li>
                      <a routerLink="/setup-company" class="uk-dropdown-close activeDropLink" >{{'My_Company' | translate}} </a>
                    </li>
                    <li>
                      <a routerLink="/setup-organisation" class="uk-dropdown-close activeDropLink" >{{'Organisation_and_Places' | translate}}</a>
                    </li>
                    <!-- TODO
                    <li>
                      <a href="#setup-plan">{{i18n Plans}}</a>
                    </li>
                    -->
                    <!-- no need to translate the following links -->
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <div  class="uk-navbar-content uk-navbar-flip uk-clearfix">
            <ul class="uk-navbar-nav">
              <li *ngIf='profileSet' class="header-link" (click)="showControllingStat = false">
                <a routerLink="/news" class="" routerLinkActive="router-link-active">
                  <i [svgTemplate]="'speechBubblesCommentOption'"></i>
                  <span id="jc-newsbadge" class="newsbadge" [ngClass]="{'hidden': !messages.unread}">
                    <span class="uk-badge uk-badge-notification uk-badge-danger jc-newscount">
                      {{messages.unread}}
                    </span>
                  </span>
                </a>
              </li>
              <li class="uk-parent dropdown"
                  data-uk-dropdown="{mode:'hover', pos:'bottom-right', remaintime: 100}"
                  aria-haspopup="true" aria-expanded="false">
                <a class="dropdown-toggle"
                   [ngClass]="{'router-link-active': menuSubcatStatuses.profileActive }"
                   [class.menuItem-active]="false">
                <span class="profile-image">
                  <img *ngIf='!info || !info.webuser || !info.webuser.mailhash' src="https://www.gravatar.com/avatar/e9893a9e9c1a2d761c2f43985f9a59d7?d=404"
                       class="jc-my-image"
                       alt="Profile">
                  <div *ngIf='info && info.webuser'>  
                   <img  src="{{ 'https://www.gravatar.com/avatar/'+ info.webuser.mailhash }}"
                       class="jc-my-image"
                       alt="Profile">
                  </div>
                </span>
                </a>
                <!-- user dropdown -->
                <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-bottom "
                    (click)="showControllingStat = false"
                     style="top: 71px; left: -137px;">
                  <ul  class="uk-nav" >
                    <li class="jobber">
                      <p class="head-user-name uk-text-truncate">
                        <span class="jc-my-uname"> {{ uname }} </span>
                      </p>
                    </li>
                    <li *ngIf='profileSet' class="jobber " >
                      <a routerLink="/profile" class=" uk-dropdown-close activeDropLink">{{'Profile' | translate}}</a>
                    </li>
                    <li *ngIf='profileSet' class="jobber">
                      <a routerLink="/availability" class="uk-dropdown-close activeDropLink" >{{'Availability' |
                        translate}}</a>
                    </li>
                    <li *ngIf='profileSet' class="jobber">
                      <a routerLink="/assignment" class="uk-dropdown-close activeDropLink" >{{'Assignment_plural_other' |
                        translate}}</a>
                    </li>
                    <li *ngIf="translate.currentLang === 'de'"  class="jobber lang en">
                      <a (click)="$event.preventDefault(); changeTranslate('en')" class="uk-dropdown-close activeDropLink" >English Version</a>
                    </li>
                    <li *ngIf="translate.currentLang === 'en'" class="jobber lang de">
                      <a (click)="$event.preventDefault(); changeTranslate('de')" class="uk-dropdown-close activeDropLink" >Deutsche Version</a>
                    </li>
                    <!-- TODO li class="jobber">
                      <a href="#jobber-jobs">{{i18n Jobs}}</a>
                    </li-->
                    <li>
                      <a class="logout uk-dropdown-close" href="#" (click)="logout($event)">{{'Log_out' | translate}}</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- uk-container uk-container-center -->
    </header>
  </div>
  <div class="uk-sticky-placeholder" *ngIf='profileSet && isManager' >
    <nav id="mainnav" class="uk-navbar uk-width-1-1" data-uk-sticky="{top:70}" style="overflow: hidden;">
      <div class="uk-container uk-container-center uk-padding-remove uk-clearfix uk-grid uk-grid-collapse uk-flex uk-flex-middle" style="height: 65px; overflow-x: scroll; overflow-y: hidden">
        <div class="uk-width-1-1 subHeader"  
            [ngClass]="{'subHeaderNav': !dayView && checkUrl == 'schedule', 
                        'dayViewSubHeader': dayView && checkUrl == 'schedule',
                         'controllingViewSubHeader': checkUrl === 'controlling',
                         'staffViewSubHeader':  checkUrl === 'staff' }">
        
          <div class=' uk-display-inline' style="float: left">
            <button *ngIf="isManager" [disabled]="!isHaveAnotherCompanies && pos.isEditCompany" (click)="loginModalOpened = !loginModalOpened; hideScrollBar()" type="button"
                    class="manager organisation uk-button uk-float-left uk-button-primary uk-text-truncate uk-display-inline-block"
                    aria-hidden="false" style="margin-right: 35px">
              <span *ngIf="!currUnit && !pos.isEditCompany" class="nounit">Choose unit</span>
              <span *ngIf="currUnit && !pos.isEditCompany" class="jc-nname nname uk-text-truncate">{{currUnit}}</span>
              <span *ngIf="pos.isEditCompany" class="jc-nname nname uk-text-truncate">{{companyName$ | async}}</span>
            </button>
          </div>
          
          <div *ngIf="checkUrl === 'schedule' && !weekPrintView && !dayPrintView" class="u uk-display-inline" style="float: right; text-align: right;">
          
            <div *ngIf="!dayView" style="height: 25px" class="uk-display-inline-block  uk-float-right"> 
                <div *ngIf="schedulePubCounter === 0" class=" uk-text-right uk-display-inline-block">
                  <button style="min-width: 200px" type="button"  class="uk-button uk-width-1-1 newSize uk-text-truncate" disabled="">
                    <span class="disabled">{{'changes_plural_zero' | translate}}</span>
                  </button>
                </div>
                <div *ngIf="schedulePubCounter" class=" uk-text-right uk-display-inline-block">
                  <button style="min-width: 200px" (click)="schedulePublic($event)" type="button" class="uk-button uk-width-1-1 newSize uk-text-truncate">
                    <span class="enabled">{{'Publish_changes' | translate}}  <span> {{schedulePubCounter}} </span> </span>
                  </button>
                </div>
            </div>
            <div *ngIf="dayView" style="height: 25px" class=" uk-display-inline-block  uk-float-right"> 
                <button style="min-width: 200px" (click)="dayViewPublic($event)" type="button" [disabled]="!dayUnpubCounter" class="uk-button uk-width-1-1 newSize uk-text-truncate">
                  <span *ngIf='dayUnpubCounter' class="enabled">{{'Publish_changes' | translate}}  <span>{{dayUnpubStaff ? dayUnpubCounter : 0}}</span> </span> 
                  <span *ngIf='!dayUnpubCounter' class="disabled">{{'changes_plural_zero' | translate}}</span>
                </button>
            </div>
            <div class="uk-margin-left uk-margin-small-right uk-display-inline-block uk-float-right" >
              <div *ngIf='!dayView' class="uk-width-1-1 uk-display-inline-block" style="height: 25px">
                <div>
                   <button class="copyButton newSize uk-align-center" [disabled]="false" type="button">
                     <i [svgTemplate]="'printIcon'" (click)="openWeekPrintView($event)"></i>
                   </button>
                </div>
              </div>
              <div *ngIf='dayView' class="uk-width-1-1 uk-display-inline-block" style="height: 25px">
                <div>
                   <button  class="copyButton newSize uk-align-center" [disabled]="false" type="button">
                     <i  [svgTemplate]="'printIcon'" (click)="openDayPrintView($event)"></i>
                   </button>
                </div>
              </div>
            </div>          
            <div class="uk-margin-left  uk-display-inline-block uk-float-right" style="height: 25px" [ngClass]="{'uk-hidden-medium uk-hidden-small' : dayView }">
              <div class="uk-width-1-1">
                 <button *ngIf="!dayView" class="copyButton newSize uk-align-center"  type="button" (click)="openCopy(true) ">
                   <i  [svgTemplate]="'copyDoc'" ></i>
                 </button>
              </div>
            </div>
            <div class="uk-margin-left uk-display-inline-block uk-float-right" style="height: 25px">
              <div *ngIf="tempVar()" class="uk-button-group">
                  <button class="uk-button dayView newSize" [ngClass]="{'activeView': dayView }" (click)="toggleDayView(true)">{{'Day' | translate}}</button>
                  <button class="uk-button dayView newSize" [ngClass]="{'activeView': !dayView }" (click)="toggleDayView(false)">{{'Week' | translate}}</button>
              </div>
              <div *ngIf="!tempVar()" class="uk-button-group">
                  <button class="uk-button dayView newSize" >{{'Day' | translate}}</button>
                  <button class="uk-button dayView newSize" >{{'Week' | translate}}</button>
              </div>
            </div>
            <div class="uk-display-inline-block uk-float-right" style="height: 25px" [ngClass]="{'uk-hidden-medium uk-hidden-small' : dayView }" >
              <div class="uk-button-group" *ngIf="!dayView && tempVar()">
                  <button class="uk-button  dayView newSize" [ngClass]="{'activeView': switchView }" (click)="toggleView(true)">{{'Shift' | translate}}</button>
                  <button class="uk-button dayView newSize" [ngClass]="{'activeView': !switchView }" (click)="toggleView(false)">{{'Position' | translate}}</button>
              </div>
              <div class="uk-button-group" *ngIf="!dayView && !tempVar()">
                  <button class="uk-button dayView newSize" >{{'Shift' | translate}}</button>
                  <button class="uk-button dayView newSize" >{{'Position' | translate}}</button>
              </div>
            </div>
            
          </div>
          
          <div *ngIf="checkUrl === 'controlling'" class=" uk-display-inline" style="float: left; text-align: left">
             <div class="uk-display-inline">
              {{'currently_working' | translate}}
              <span class="uk-badge uk-badge-notification jc-badge jc-onduty">
                  {{unit?.bubbles?.working || 0}}
              </span>
            </div>
            <div class="uk-display-inline">
                {{'late' | translate}}
                 <span class="uk-badge uk-badge-notification uk-badge-danger jc-badge jc-late">
                    {{unit?.bubbles?.late || 0}}
                 </span>
            </div>
            <div class="uk-display-inline">
              {{'review_needed' | translate}}
              <span class="uk-badge uk-badge-notification uk-badge-warning jc-badge jc-changed">
                {{unit?.bubbles?.clarify || 0}}
              </span>
            </div>
          </div>
          
          <div *ngIf="checkUrl === 'staff'" class=" uk-display-inline" style="float: right; text-align: right;">
             <div class=" uk-margin-bottom uk-display-inline-block" style="height:15px;position: relative; top: 4px;">
                <button type="button" class="uk-button  uk-width-1-1 jc-click-userinvite newSize"
                        (click)="addJoberModal($event); " [disabled]="!currUnit">{{'plussign_jobber' | translate}}
                </button>
             </div>
             <div class="uk-margin-left  uk-display-inline-block">
                <form class="uk-form uk-autocomplete uk-width-1-1 jc-search-staff" ngNoForm>
                  <div class="search-box uk-form-icon-flip uk-width-1-1">
                    <input class="uk-search-field uk-width-1-1 jc-search-staff" type="search"
                           autocomplete="off" 
                            id="staffsearchtext"
                            [formControl]="searchStr"
                           placeholder="{{'placeholder_Search_staff' | translate}}"/>
                    <i class="search-icon" [svgTemplate]="'magnifyingGlass'"></i>
                  </div>
                </form>
              </div>
          </div>
          
        </div>
      </div>
    </nav>
  </div>
  

  <!--content go here-->
  <div  class="content uk-padding-remove uk-container uk-container-center" [ngClass]="{'newStyles': weekPrintView || dayPrintView}">
      <div *ngIf='profileSet' id="global-message" style="padding-top: 15px">

        <div *ngIf="!completeProfile" class="uk-alert uk-alert-warning  error warn-incomplete-profile" data-uk-alert="">
          <span>{{'msg_warn_availability_part1' | translate }} <a routerLink="/profile" class="">{{'your_profile' | translate}}</a></span>
        </div>

        <div *ngIf="showCreateAlert && showCreateAlert.new && !showCreateAlert.pending" class="uk-alert uk-alert-success error invite-mail-new " data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close"></a>
          <span>{{ 'msg_info_invite_mail_new_part1' | translate}} <span class="jc-uname">{{ showCreateAlert.uname }}</span> {{ 'msg_info_invite_mail_new_part2'  | translate }}</span>
        </div>

        <div *ngIf="showCreateAlert && !showCreateAlert.new && !showCreateAlert.pending"  class="uk-alert uk-alert-warning error invite-mail-internal" data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close"></a>
          <span>{{'msg_info_jobber_was_invited_to_connect' | translate }}</span>
        </div>

        <div *ngIf="showCreateAlert && showCreateAlert.new && showCreateAlert.pending" class="uk-alert uk-alert-success error invite-new " data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close"></a>
          <span>{{'msg_info_invite_new_part1' | translate }}  <span class="jc-uname">{{ showCreateAlert.uname }}</span>{{ 'msg_info_invite_new_part2'| translate }} </span>
        </div>

        <div *ngIf="showCreateAlert && !showCreateAlert.new && showCreateAlert.pending" class="uk-alert uk-alert-warning error invite-internal" data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close"></a>
          <span>{{'msg_info_jobber_was_add_to_connect' | translate }}</span>
        </div>
        <div class="uk-alert uk-alert-wa  Googlerning " *ngIf=" availabilityAlert$ | async"  data-uk-alert="">
          {{ 'msg_warn_availability_part1' | translate }}
          <a  routerLink="/availability" >{{ 'msg_warn_availability_part2' | translate }}</a>

        </div>
        <div class="uk-alert uk-alert-warning " *ngIf="prevScheduledAssignAlert$ | async"  data-uk-alert="">
          <a (click)="hideScheduledNotification()" class="uk-alert-close uk-close"></a>
          {{ 'msg_jobber_has_been_scheduled' | translate }}
        </div>

      </div>
       <div *ngIf="!profileSet && showCreateAlert && showCreateAlert.new && showCreateAlert.pending" 
            class="uk-alert uk-alert-success error invite-new uk-margin-large-top" data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close"></a>
          <span>{{'msg_info_invite_new_part1' | translate }}  <span class="jc-uname">{{ showCreateAlert.uname }}</span>{{ 'msg_info_invite_new_part2'| translate }} </span>
       </div>
       <div *ngIf="!profileSet && showCreateAlert && !showCreateAlert.new && showCreateAlert.pending" 
            class="uk-alert uk-alert-warning error invite-internal uk-margin-large-top" data-uk-alert="">
          <a (click)="action.jobberAlert(false)" class="uk-alert-close uk-close "></a>
          <span>{{'msg_info_jobber_was_add_to_connect' | translate }}</span>
       </div>
       
      <router-outlet></router-outlet>

  </div>
  <footer
    class="uk-container uk-container-center uk-padding-remove uk-margin-large-top uk-width-1-1">
    <div class="narrow-footer">
      <ul class="uk-float-right">
        <li class="copyright">© Jubbr AG 2016</li>
      </ul>
    </div>
  </footer>
</div>

<!-- OFFCANVAS -->
<div *ngIf='profileSet' id="offcanvas" class="uk-offcanvas manager">
    <div class="uk-offcanvas-bar">
      <a class="uk-text-right close-big uk-offcanvas-close">×</a>
      <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
        <li class="uk-parent manager" [attr.aria-expanded]="managerToggeled" [ngClass]="{'uk-open': managerToggeled}">
          <a href="#" (click)="$event.preventDefault();managerToggeled = !managerToggeled;" class=""><span>Manager</span></a>
          <ul class="uk-nav-sub" [ngClass]="{'uk-hidden': !managerToggeled}">
            <li  class="">
              <a routerLink="/schedule" (click)="showControllingStat = false" class="uk-offcanvas-close">Schedule</a>
            </li>
            <li class="">
              <a routerLink="/controlling" (click)="showControllingStat = true" class="uk-offcanvas-close"> {{ 'Controlling' | translate }} </a>
            </li>
            <li class="">
              <a routerLink="/staff" (click)="showControllingStat = false" class="uk-offcanvas-close"> {{ 'Staff' | translate }} </a>
            </li>
            <li class="header-link">
                <a routerLink="/reports" (click)="showControllingStat = false" class="uk-offcanvas-close" 
                routerLinkActive="router-link-active"> {{ 'Reports' | translate }} </a>
              </li>
            <!-- TODO
            <li class="manager">
              <a href="#manager-hiring">{{i18n Hiring}}</a>
            </li>

            <li class="manager">
              <a href="#manager-report" class="">Reports</a>
            </li>
            -->
          </ul>
        </li>
        
        <li class="uk-parent uk-open manager" [attr.aria-expanded]="settingsToggeled" [ngClass]="{'uk-open': settingsToggeled}">
          <a href="#" (click)="$event.preventDefault();settingsToggeled = !settingsToggeled;" class=""><span>Settings</span></a>
          <ul class="uk-nav-sub" [ngClass]="{'uk-hidden': !settingsToggeled}">
            <li>
              <a routerLink="/setup-company" class="uk-dropdown-close uk-offcanvas-close">{{'My_Company' | translate}} </a>
            </li>
            <li>
              <a routerLink="/setup-organisation" class="uk-dropdown-close uk-offcanvas-close">{{'Organisation_and_Places' | translate}}</a>
            </li>
            <!-- TODO
            <li>
              <a href="#setup-plan">{{i18n Plans}}</a>
            </li>
            -->
          </ul>
        </li>
      </ul>
    </div>
  </div>
<!-- OFFCANVAS END-->
`;

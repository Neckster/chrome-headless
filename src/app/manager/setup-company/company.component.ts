import { Component }                from '@angular/core';
import { select }                   from 'ng2-redux/lib/index';

import {
  FORM_DIRECTIVES,
  REACTIVE_FORM_DIRECTIVES }        from '@angular/forms';

import {
  FormBuilder,
  FormGroup,
  Validators }                      from '@angular/forms';

import { JubbrFormValidator }       from '../../lib/validators/jubbr-validator';
import { CompanyService }           from '../../services/company.service';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { AppStoreAction }           from '../../store/action';
import { htmlCompany }              from './company.htm.ts';
import { Observable }               from 'rxjs/Rx';
import * as R                       from 'ramda';

@Component({
  selector: 'company',
  directives: [FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES],
  template: htmlCompany
})
export class SetupCompany {

  @select('info') info$;

  public currentOrganisation: any;
  public company: any;
  public info: any;
  public store: any;
  public updateSuccess: boolean = false;
  public updateError: boolean = false;

  private wasSubmitted: boolean = false;
  private subscriptions = [];
  private updateCompanyForm: FormGroup;

  constructor(public action: AppStoreAction, public fb: FormBuilder,
              public companyService: CompanyService) {

    ParseOrganisationService.isEditCompany = true;

    this.updateCompanyForm = this.fb.group({
      cname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      cellphone: ['', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32),
        JubbrFormValidator.phoneNumber as any,
        JubbrFormValidator.trimmed as any
      ])],
      country: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(128),
        JubbrFormValidator.mail as any,
        JubbrFormValidator.stringTrim as any
      ])],
      firstname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      lastname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      location: ['', Validators.compose([
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      phone: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.phoneNumber as any,
        JubbrFormValidator.stringTrim as any
      ])],
      region: ['', Validators.compose([
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      salutation: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      sector: [''],
      street: ['', Validators.compose([
        Validators.minLength(2),
        Validators.maxLength(150),
        JubbrFormValidator.stringTrim as any
      ])],
      website: ['', Validators.compose([
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.stringTrim as any
      ])],
      zip: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(6),
        JubbrFormValidator.stringTrim as any
      ])]
    });

    this.updateCompanyForm.valueChanges.subscribe((_) => {
      this.wasSubmitted = false;
      this.updateError = false;
    });

    this.subscriptions = [
      this.info$.subscribe(info => {
        this.info = info;
        if (info.manager && !this.company) {

          const processCompany = R.pipe(
            R.filter(R.propEq('company_id', this.info.manager.selected.company_id)) as any,
            R.head
          );

          this.company = processCompany(info.company);
          this.populateFields(this.company).subscribe(
            R.always(undefined),
            R.always(undefined),
            R.always(undefined));
        }
      })
    ];

  }

  populateFields(data: any): Observable<any> {
    return Observable.create((observer) => {
      Object.keys(data).forEach(key => {
        if (this.updateCompanyForm.controls.hasOwnProperty(key)) {
          this.updateCompanyForm.controls[key].patchValue(data[key]);
          observer.next({[key]: data[key]});
        }
      });
      observer.complete();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    ParseOrganisationService.isEditCompany = false;
    this.subscriptions.forEach(el => el.unsubscribe());
  }

  formSubmit(event) {

    event.preventDefault();

    this.wasSubmitted = true;
    this.updateError = false;
    this.updateSuccess = false;

    if (!this.updateCompanyForm.valid) {

      this.updateError = true;
      return;
    }

    let p = { company_id: this.info.manager.selected.company_id.toString() };
    let params = this.updateCompanyForm.value;
    Object.keys(params).forEach(key => {
      if (params[key] !== '') p[key] = params[key];
      if (typeof params[key] === 'string' &&
          params[key].length !== params[key].trim().length)
        p[key] = params[key].trim();
    });
    this.companyService.putCompany(p).subscribe(res => {
      this.updateSuccess = true;
      // this.company = undefined; // no need to hide form after update
      this.populateFields(res);
      this.action.fetchGetInfo();
    }, error => {
      this.updateError = true;
    });

  }
}

//noinspection TsLint
export const htmlCompany = `
<div class="page company plannerview uk-clearfix">
  <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    <div
      class="uk-width-small-1-1 uk-width-medium-9-10 uk-width-xlarge-7-10 uk-container-center uk-grid-collapse">
      <h1>{{'Contact_data' | translate}}</h1>
      <div id="company-profile-form" class="uk-margin">
        <!-- company  profile starts here-->
        <div class="uk-width-small-1-1 uk-width-large-8-10 uk-margin-large-top">
        
          <form class="uk-form uk-form-horizontal company" id="companyedit"
                [formGroup]="updateCompanyForm" novalidate (submit)="formSubmit($event)">
                
            <input type="hidden" id="companycompany_id" >
            <!--Company Name-->
            <div class="uk-form-row ws-success">
              <label class="required uk-form-label" for="cname">{{'Company' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.cname.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10  cname user-success" 
                     type="text"
                     id="cname" 
                     placeholder="{{'placeholder_Company' | translate}}"
                     formControlName="cname">
              <div *ngIf="updateCompanyForm.controls.cname.dirty && 
                          !updateCompanyForm.controls.cname.valid && 
                          updateCompanyForm.controls.cname.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Salutation-->
            <div class="uk-form-row ws-success">
              <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="salutation">{{'Salutation' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.salutation.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 salutation user-success"
                     type="text"
                     id="salutation" 
                     placeholder="{{'placeholder_Salutation' | translate}}"
                     formControlName="salutation">
              <div *ngIf="updateCompanyForm.controls.salutation.dirty && 
                          !updateCompanyForm.controls.salutation.valid && 
                          updateCompanyForm.controls.salutation.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--First Name-->
            <div class="uk-form-row ws-success">
              <label class="required uk-form-label" for="profilefirstname">{{'Firstname' |
                translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.firstname.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10  profilefirstname user-success"
                     type="text"
                     id="profilefirstname" 
                     placeholder="{{'placeholder_Firstname' | translate}}"
                     formControlName="firstname">
              <div *ngIf="updateCompanyForm.controls.firstname.dirty && 
                          !updateCompanyForm.controls.firstname.valid && 
                          updateCompanyForm.controls.firstname.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Last Name-->
            <div class="uk-form-row ws-success">
              <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="profilelastname">{{'Lastname' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.lastname.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 profilelastname user-success"
                     type="text"
                     id="profilelastname" 
                     placeholder="{{'placeholder_Lastname' | translate}}"
                     formControlName="lastname">
              <div *ngIf="updateCompanyForm.controls.lastname.dirty && 
                          !updateCompanyForm.controls.lastname.valid && 
                          updateCompanyForm.controls.lastname.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Street-->
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="street">{{'Street'
                | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.street.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 profilestreet" type="text"
                     id="profilestreet"
                     placeholder="{{'placeholder_Street' | translate}}" 
                     formControlName="street">
              <div *ngIf="updateCompanyForm.controls.street.dirty && 
                          !updateCompanyForm.controls.street.valid && 
                          updateCompanyForm.controls.street.errors?.required"
                   class="uk-alert uk-alert-warning"
                   id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Zip-->
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilezip">{{'ZIP'
                | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.zip.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 zip profilestreet" type="text"
                     id="profilezip"
                     placeholder="{{'placeholder_ZIP' | translate}}" 
                     formControlName="zip">
              <div *ngIf="updateCompanyForm.controls.zip.dirty && 
                          !updateCompanyForm.controls.zip.valid && 
                          updateCompanyForm.controls.zip.errors?.required"
                   class="uk-alert uk-alert-warning"
                   id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Place-->
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="profilelocation">{{'Place' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.location.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation" 
                     type="text"
                     id="profilelocation" 
                     placeholder="{{'placeholder_Location' | translate}}"
                     formControlName="location">
              <div *ngIf="updateCompanyForm.controls.location.dirty && 
                          !updateCompanyForm.controls.location.valid && 
                          updateCompanyForm.controls.location.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Region-->
            <div class="uk-form-row ">
              <label class=" uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="region">{{'Region'
                | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.region.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 region user-success" 
                     type="text"
                     id="region" placeholder="{{'placeholder_Region' | translate}}"
                     formControlName="region">
              <div *ngIf="updateCompanyForm.controls.region.dirty && 
                          !updateCompanyForm.controls.region.valid && 
                          updateCompanyForm.controls.region.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Country-->
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="profilecountry">{{'Country' | translate}}</label>
              <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <select [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.country.valid }"
                        id="profilecountry"
                        class="select profilecountry" 
                        formControlName="country">
                  <option selected="" value="">{{'please_select' | translate}}</option>
                  <option value="CH">{{'country_CH' | translate}}</option>
                  <option value="DE">{{'country_DE' | translate}}</option>
                  <option value="AT">{{'country_AT' | translate}}</option>
                </select>
              </div>
            </div>

            <!--Phone-->
            <div class="uk-form-row">
              <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="phone">{{'Phone' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.phone.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 phone" type="tel"
                     title="+99(99)99999999" id="phone"
                     placeholder="{{'placeholder_Phone' | translate}}" 
                     formControlName="phone">
              <div *ngIf="updateCompanyForm.controls.phone.dirty && 
                          !updateCompanyForm.controls.phone.valid && 
                          updateCompanyForm.controls.phone.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Mobile-->
            <div class="uk-form-row">
              <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="profilecellphone">{{'Cellphone' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.cellphone.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 profilecellphone" type="tel"
                     title="+99(99)99999999" id="profilecellphone"
                     placeholder="{{'placeholder_Cellphone' | translate}}" 
                     formControlName="cellphone">
              <div *ngIf="updateCompanyForm.controls.cellphone.dirty && 
                          !updateCompanyForm.controls.cellphone.valid && 
                          updateCompanyForm.controls.cellphone.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--Industry-->
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="sector">{{'Industry' | translate}}</label>
              <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <select [ngClass]="{'uk-form-danger':wasSubmitted && !updateCompanyForm.controls.sector.valid }" 
                        id="sector"
                        class="select sector" 
                        formControlName="sector">
                  <option>{{'please_select' | translate}}</option>
                  <option value="entertainment">{{'Events' | translate}}</option>
                  <option value="gastro">{{'Gastronomy' | translate}}</option>
                  <option value="hotel">{{'Hotels' | translate}}</option>
                  <option value="tourism">{{'Tourism' | translate}}</option>
                  <option value="admin">{{'Administration' | translate}}</option>
                </select>
              </div>
            </div>

            <!--Email-->
            <div class="uk-form-row">
              <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                     for="email">{{'Email' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.email.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 email user-success" 
                     type="email"
                     id="email" 
                     placeholder="{{'placeholder_Email' | translate}}" 
                     formControlName="email">
              <div *ngIf="updateCompanyForm.controls.email.dirty && 
                          !updateCompanyForm.controls.email.valid && 
                          updateCompanyForm.controls.email.errors?.required"
                   class="uk-alert uk-alert-warning"
                   id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!--WebSite-->
            <div class="uk-form-row">
              <label class=" uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="website">{{'Website'
                | translate}}</label>
              <input [ngClass]="{'uk-form-danger': wasSubmitted && !updateCompanyForm.controls.website.valid }"
                     class="uk-width-small-1-1 uk-width-medium-6-10 website user-success"
                     type="text"
                     id="website" placeholder="{{'placeholder_Website' | translate}}"
                     formControlName="website">
              <div *ngIf="updateCompanyForm.controls.website.dirty && 
                          !updateCompanyForm.controls.website.valid && 
                          updateCompanyForm.controls.website.errors?.required"
                   class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
              </div>
            </div>

            <!-- TODO
            <div class="uk-form-row">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="companytext">{{i18n Info_Company}}</label>
              <textarea class="uk-width-small-1-1 uk-width-medium-6-10 companytext" id="companytext"
                placeholder="{{i18n placeholder Info_Company}}"></textarea>
            </div>
            -->
            <div [ngClass]="{'hidden': !updateSuccess }"
                 class=" uk-width-1-1 uk-alert uk-alert-success error ok"
                 data-uk-alert="">
              <a class="uk-alert-close uk-close"></a>{{'msg_info_changes_saved' | translate}}.
            </div>
            
            <div [ngClass]="{'hidden': !updateError }"
                 class="uk-width-1-1 uk-alert uk-alert-danger error internal"
                 data-uk-alert="">
              <a class="uk-alert-close uk-close"></a>{{'msg_err_internal_store' | translate}}.
            </div>
           
            <div class="uk-width-1-1 uk-container-center uk-grid-collapse">
              <div class="action-buttons uk-margin-top">
                <button type="submit" type="submit"
                        class="uk-width-1-1 ok uk-button uk-button-large uk-margin-bottom">
                  {{'Save_changes' | translate}}
                </button>
              </div>
            </div>
          </form>


        </div>
        <!-- company  profile tab ends here -->
      </div>
    </div>
    <!-- column -->
  </div>
  <!-- grid -->
</div>
`;

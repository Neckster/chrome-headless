export const htmlNews: string = `
<send-message 
  [opened]="sendMessageOpened" 
  (modelUpdate)="updateStatuses()"
  (closeEmitter)="closedModal($event, err)"></send-message>

<!--
<div *ngIf="firstTime" class="uk-sticky-placeholder" style="height: 50px;">
  <nav id="mainnav" class="uk-navbar uk-width-1-1" data-uk-sticky="{top:70}" style="margin: 0px;background: #fff; box-shadow:none!important;">
    <div class="uk-container uk-container-center uk-padding-remove uk-clearfix uk-grid uk-grid-collapse uk-flex uk-flex-middle">
      <div class="uk-width-1-2">
        
      </div>
      <div class="uk-width-1-2">
        TODO
        <button type="button" href="#setup-plan" class="uk-button uk-button-primary uk-float-right uk-text-truncate">
          <span class="trial-count">{{i18n Days_left_on_trial}}</span>
        </button>
     
      </div>     
    </div>
  </nav>
</div>
-->
<div class="page news jobberview uk-clearfix">
	<div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
		<div class=
		"uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-9-10 uk-width-xlarge-7-10 uk-container-center uk-grid-collapse">
		<div id="alertbox-news">
				<div *ngIf="successStatus" class="uk-width-1-1 uk-alert uk-alert-success error ok"
				data-uk-alert="">
					<a class="uk-alert-close uk-close uk-width-1-1"></a>{{'msg_info_message_sent' | translate}}
				</div>
				
				<div *ngIf="errorStatus" class="uk-width-1-1 uk-alert uk-alert-danger error internal"
				data-uk-alert="">
					<a class="uk-alert-close uk-close uk-width-1-1"></a>{{'msg_err_internal_send' | translate}} {{'Info' | translate}}: {{messageSendingInfo}}
				</div>
			</div>
			
			<h1>{{'News' | translate}}</h1>
        <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle uk-clearfix">
          
          <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-margin-small-top uk-margin-small-bottom">
            <a *ngIf="isManager" class="uk-align-left manager-link jc-click-newspublish" (click)="sendMessageOpened = !sendMessageOpened;hideScrollBar()">
            {{'plussign_Add_new_message' | translate}}    
            </a>
          </div>
          
          <div class="switch uk-width-small-1-1 uk-width-medium-1-2 uk-margin-small-top uk-margin-small-bottom">
          <input class="switch" 
                 id="show-deleted-news" 
                 type="checkbox" 
                 [attr.disabled]="!newslist?.length ? null : true"
                 (ngModelChange)="toggleRemovedStatus($event)" 
                 [(ngModel)]="removedStatus">
            <label class="uk-align-medium-right" for="show-deleted-news"><span><span class="jc-deleted-news-count">
            <!--12--></span>{{'show_deleted' | translate}}</span></label>
          </div>
        </div>
			
			<section *ngIf="newsList?.length">
        <ul class="uk-nestable" id="newslist">
          <li *ngFor="let news of newsList | later:'valid_from':registrationTime | filter:'deleted':removedStatus | paginate: { id: 'server', itemsPerPage: 15, currentPage: selectedPage }; let i = index"
              class="uk-nestable-item uk-nestable-nodrag uk-parent uk-nestable-list-item  jc-template-newsentry uk-position-relative"
              [ngClass]="{'uk-collapsed': !itemOpened[news.message_id], 'unread': !news.read, 'read': news.read}">
              <news-item [news]="news" (out)="handleMessageAction($event)"></news-item>
          </li>
        </ul>
      </section>
      
			<pagination-controls class="uk-container-center uk-pagination" (pageChange)="pageChange($event)" id="server" autoHide="true" directionLinks="false">
      </pagination-controls>
			
		</div><!-- column -->
	</div><!-- uk-grid -->
</div>
`;

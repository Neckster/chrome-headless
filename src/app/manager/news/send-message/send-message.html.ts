export const sendMessageHtml: string = `

<div id="modal" *ngIf="opened">
  <section aria-hidden="false" aria-labelledby="label-newspublish" class="uk-modal uk-open" id="newspublish" role="dialog" style="display: block; overflow-y: auto;" tabindex="-1">
    <div class="modal-overlay" style="" (click)="closeEmitter.emit(null)"></div>
    <div class="uk-modal-dialog">
      <a class="uk-modal-close uk-close" data-close="dismiss" data-dismiss="modal"  title ='{{ "dismiss" | translate }}' (click)="closeEmitter.emit(null);hideScrollBar()">
      <!-- icon --></a>
      <div class="uk-modal-header">
        <div id="label-newspublish">
          <h2><a class="uk-modal-close" (click)="closeEmitter.emit(null);hideScrollBar()" style='margin-right:5px; padding-top:5px'><!-- mobile-back-button --></a>{{'Send_message_plural_other' | translate}}</h2>
        </div>
      </div>
      <div class="modal-content">
          
      
        <form class="uk-form uk-form-horizontal newspublish" 
              (submit)="sendMessage($event)" 
              [formGroup]="messageGroup" 
              #sendMessageForm="ngForm">
         
          <div class="uk-grid uk-grid-collapse uk-text-left">
        
            <!-- Global row -->
            <div class="uk-form-row">       
              <p>{{'Reach' | translate}}</p>
              <label *ngIf="superUser" class="uk-form-label" for="newspublishreachglobal">
                <input id="newspublishreachglobal" 
                       name="reach"  
                       type="radio" 
                       formControlName="reach"
                       value="global"> 
                <span>{{'global' | translate}}</span>
              </label>
            </div>
            
            <!-- Local row -->
            <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="newspublishreachlocal">
                  <input id="newspublishreachlocal" 
                         name="reach" 
                         type="radio"
                         formControlName="reach"
                         value="local"> 
                  <span>{{'local' | translate}}</span>
                </label>
                <div class="select-wrapper disabled uk-width-small-1-1 uk-width-medium-6-10">
                 
                 
                  <!-- <select [(ngModel)]=homePositionId
                              (click)="focusSelect(homePositionId, 'home')"
                              (ngModelChange)="userSubmitSkill(userData.webuser_id, homePositionId, 'home_position_id')"
                              class="select">
                        <option value="0">{{'None_female'|translate}}</option>
                        <optgroup *ngFor="let unit of unitOrganisation " label="{{unit.text}}">
                          <option *ngFor="let position of unit.child" [ngValue]="position.id">{{position.text}}</option>
                        </optgroup>
                      </select>   -->
                 
                 
                   <!--<select name="unit" -->
                          <!--id="newspublishunit"-->
                          <!--class="select">-->
                    <!--<option value="false" disabled>-->
                      <!--{{'Choose_unit' | translate}}-->
                    <!--</option>-->
                    <!--<option *ngFor="let unit of unitOrganisation" value="{{unit.id}}">-->
                      <!--{{unit.text}}-->
                    <!--</option>-->
                  <!--</select>-->
                  
                   <recurrent-select 
                           [defaultValue]="localId"
                           [noLocal]="true"
                           formControlName="localId">
                   </recurrent-select>
                  
                </div>
                <div [ngClass]="{'uk-hidden': !errors['unitIdErr'] }" class="uk-alert uk-alert-warning">
                    <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                   </div>
              </div>
            </div>
            
            <!-- User row -->
            <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="newspublishreachuser">
                  <input id="newspublishreachuser" 
                         name="reach" 
                         type="radio" 
                         formControlName="reach"
                         value="user"> 
                  <span>{{'private' | translate}}</span>
                </label>
                <div class="select-wrapper disabled uk-width-small-1-1 uk-width-medium-6-10">
                  <select class="select" 
                          [disabled]="messageGroup.controls.reach.value !== 'user'" 
                          formControlName="user"
                          [ngClass]="{'uk-form-danger': wasSubmitted && messageGroup.errors?.notProperUserSelected }"
                          id="newspublishwebuser_id">
                    <option value="none" disabled>
                      {{'Choose_Jobber' | translate}}
                    </option>
                    <option *ngFor="let person of staffList" value="{{person.webuser_id}}">
                      {{person.uname}}
                    </option>
                  </select>
                   
                </div>
                
                <div [ngClass]="{'uk-hidden': !(wasSubmitted && messageGroup.errors?.notProperUserSelected) }" class="uk-alert uk-alert-warning">
                      <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                   </div>
              </div>
            </div>
            
            <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <label class="required uk-form-label uk-width-1-1" for="newspublishtitle">{{'Title' | translate}}</label>
              <input class="uk-width-1-1"
                     id="newspublishtitle"
                     minlength="1"
                     maxlength="64"
                     placeholder="{{'placeholder_Title' | translate}}"
                     type="text"
                     [ngClass]="{'uk-form-danger': wasSubmitted && !messageGroup.controls.title?.valid}"
                     formControlName="title">
                     
              <div [ngClass]="{'uk-hidden': !(wasSubmitted && messageGroup.controls.title.errors) }" class="uk-alert uk-alert-warning">
                <p *ngIf="messageGroup.controls.title.errors?.required || messageGroup.controls.title.errors?.notTrimmedString" class="ws-errormessage">{{'msg_err_missing_title' | translate}}</p>
                <!--<p *ngIf="messageGroup.controls.title.errors?.maxlength?.actualLength > messageGroup.controls.title.errors?.maxlength?.requiredLength" class="ws-errormessage">Please enter shorter message title.</p>-->
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <label class="required uk-form-label uk-width-1-1" for="newspublishmessage">{{ 'Message' | translate }}</label>
              <textarea class="uk-width-1-1"
                        id="newspublishmessage"
                        placeholder="{{'Message' | translate}}"
                        rows="4"
                        [ngClass]="{'uk-form-danger': wasSubmitted && !messageGroup.controls.message?.valid}"
                        formControlName="message"></textarea>
            </div>
            <div [ngClass]="{'uk-hidden' : !(wasSubmitted && messageGroup.controls.message.errors)}" class="uk-alert uk-alert-warning" id="errorbox-1">
              <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
            </div>
            <div class="uk-width-1-1 uk-container-center uk-grid-collapse">
              <div class="action-buttons uk-margin-top">
                <button class="ok uk-button uk-margin-bottom uk-width-1-1" type="submit">
                  {{'Send_message' | translate}}
                </button>
              </div>
            </div>
          </div>
          
        </form>
      </div>
    </div>
  </section>
</div>
`;
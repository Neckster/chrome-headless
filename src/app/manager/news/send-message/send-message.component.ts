import {
  Component,
  Input,
  Output,
  EventEmitter,
  SimpleChange }                    from '@angular/core';
import { sendMessageHtml }          from './send-message.html';
import { MessageService }           from '../../../services/message.service';
import { StaffService }             from '../../../services/staff.service';
import { Observable }               from 'rxjs/Observable';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { NgRedux, select }          from 'ng2-redux/lib/index';
import { JobService }               from '../../../services/shared/JobService';
import {
  FormBuilder,
  FormGroup }                       from '@angular/forms';
import { Validators }               from '@angular/common';
import { JubbrFormValidator }       from '../../../lib/validators/jubbr-validator';
import { TranslateService }         from 'ng2-translate/ng2-translate';
import * as R                       from 'ramda';
const L                             = require('ramda-lens');


const isUser = (user) => () => R.equals('user')(user);
const isGlobal = (val) => () => R.equals('global')(val);
const isNone = (val) => () => R.equals('none')(val);

const reachLens = R.lensProp('reach');
const valLens = R.lensProp('value');
const userLens = R.lensProp('user');

const noUserError = { notProperUserSelected: true};

interface IMessageError {
  titleErr?: boolean;
  titleExceeded?: boolean;
  messageErr?: boolean;
  unitIdErr?: boolean;
  userIdErr?: boolean;
}

interface IPage {
  staff?: any;
  pagination?: any;
}

@Component({
  selector: 'send-message',
  styles: [`
    .modal-overlay {
      position: absolute;
      height: 100%;
      width: inherit;
    }
    .uk-modal-header h2 .uk-modal-close::before {
      top: 0;
    }
    .uk-modal-dialog > .uk-close:first-child:after, 
    .uk-modal-dialog > .uk-close:first-child:before {
      top: 60%;
    }
  `],
  template: sendMessageHtml
})
export class SendMessage {
  @Input() noLocal;
  @Input() opened: Observable<any>;
  @Output() closeEmitter = new EventEmitter();
  @Output() modelUpdate = new EventEmitter();

  @select() info$: any;
  @select() currentUnit$: any;
  @select() currentCompanyStaff$: any;

  public info;
  public unsubscibe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  private localWithoutNo: boolean = true;
  private staffList;
  private unitStore;
  private unitOrganisation;
  private errors: IMessageError = {};
  private staffCache;
  private messageGroup: FormGroup;
  private wasSubmitted = false;
  private superUser: boolean;
  private deafutltId: any;
  // Constructor
  constructor(private formBuilder: FormBuilder, private message: MessageService,
              private ts: TranslateService,
              private staffService: StaffService, private jobService: JobService) {

    this.staffCache = {};
    this.staffList = [];
    this.unsubscibe.push(this.currentCompanyStaff$.subscribe(staff => {
      this.staffList = staff;
    }));
    this.unsubscibe.push(this.info$.subscribe(info => {
      this.info = info;
      /* tslint:disable:no-string-literal */
      if (this.info.manager) {
        this.unitOrganisation = this.parseSettingsStaff(this.info.manager.organisation);
        if (info.manager.selected && this.messageGroup &&
            this.messageGroup.controls && this.messageGroup.controls['localId'] ) {
          this.messageGroup.patchValue({localId: info.manager.selected.unit_id});
        }
      }
      /* tsslint:enable:no-string-literal */

      if (info && info.webuser && info.webuser.is_super_user) {
        this.superUser = info.webuser.is_super_user;
      }

      this.currentUnit$.subscribe(unit => {
        this.unitStore = unit;
      });
    }));
  }

  /*
   * | Lifecycle hooks section
   * -------------------------
   */

  ngOnInit() {

    this.messageGroup = this.formBuilder.group({
      reach: ['local'],
      user: ['none', Validators.compose([
      ])],
      title: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(64),
        JubbrFormValidator.trimmed
      ])],
      message: ['',  Validators.compose([
        Validators.required,
        Validators.minLength(1)
      ])],
      localId: [this.deafutltId]
    }, {
      validator: (formGroup) => {

        const reachLevel = L.view(R.compose(reachLens, valLens), formGroup.controls);
        const selUser = L.view(R.compose(userLens, valLens), formGroup.controls);

        const validateInput = R.ifElse(
          R.allPass([isUser(reachLevel), isNone(selUser)]),
          () => noUserError,
          () => undefined
        );

        return validateInput(formGroup);
      }
    });

    this.messageGroup.valueChanges.subscribe(val => {
      // debugger;
      this.inputChange();
    });

  }

  ngOnDestroy() {
    this.unsubscibe.forEach(el => el.unsubscribe());
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
  }

  /*
   * | End of Lifecycle hooks section
   * --------------------------------
   */

  // UI handlers

  radioChange(e) {
    this.inputChange();
  }

  sendMessage(e) {

    e.preventDefault();
    this.wasSubmitted = true;
    this.modelUpdate.emit(undefined);

    if (!this.messageGroup.valid) {
      return;
    }
    const prepareMessage = (message) => {
      const decorateGlobalMessage = (msg) =>
        Object.assign({}, msg,
          {  unit: 'Choose unit', selectlang: this.ts.currentLang });
      const decoratePersonalMessage = (msg) =>
        Object.assign({}, msg,
            { unit: 'Chose unit', webuser_id: msg.user, selectlang: this.ts.currentLang });

      const decorateMessage = R.ifElse(isGlobal(L.view(reachLens, message)),
        decorateGlobalMessage,
        decoratePersonalMessage
      );

      const composeMessage = R.compose(decorateMessage, R.map(R.trim));

      return composeMessage(message);
    };
    this.message.create(prepareMessage(this.messageGroup.value)).subscribe((res) => {
      this.closeEmitter.emit({ status: 'success', message: res });
    }, (err) => {
      this.closeEmitter.emit({ status: 'error', message: err.json().message });
    });
    this.hideScrollBar();

  }

  inputChange() {
    this.errors = <IMessageError>{};
    this.wasSubmitted = false;
  }

  // Helpers

  resetForm(form) {
    form.reset();
    form.patchValue({reach: 'global', user: 'none'});
  }

  handleSuccessSending(successResponse: any): void {
    this.closeEmitter.emit({ status: 'success', message: successResponse });
  }

  handleErrorSending(errorResponse: any): void {
    this.closeEmitter.emit({ status: 'error', message: errorResponse.json().message });
  }

  getStaffService(amount): any {
    return this.staffService.get(1, amount, 'uname', 'DESC').subscribe((res) => {
      if (res.pagination.page <= res.pagination.pagerMax) {
        let reqs = [];
        for (let i = res.pagination.page; i <= Math.ceil(res.pagination.total / amount); i++) {
          reqs.push(this.staffService.get(i, amount, 'uname', 'DESC'));
        }
        return Observable.concat(...reqs)
          .reduce((acc: Array<Observable<any>>, x: Observable<any>) => {
            acc.push(x);
            return acc;
          }, []).subscribe((resInner: any[]) => {
            resInner.forEach((page: IPage) => {
              page.staff.forEach((person) => {
                this.staffList.push(person);
              });
            });
            this.staffList = this.staffList
              .filter((el, index, arr) => {
                  return arr.findIndex((t) => { return t.umail === el.umail; }) === index;
              });
          });
      }
    });
  }

  parseSettingsStaff(organisation) {
    ParseOrganisationService.treeStruct(organisation);

    let unitOrganisation = ParseOrganisationService.unitPosition(

      ParseOrganisationService.treeData[0].child
    );
    return unitOrganisation;
  }

}

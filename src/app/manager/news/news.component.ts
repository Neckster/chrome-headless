import {
  Component,
  NgZone }                      from '@angular/core';
import { ActivatedRoute }       from '@angular/router';

import { select }               from 'ng2-redux/lib/index';
import { InfiniteScroll }       from 'angular2-infinite-scroll';

import { Maybe, Just }          from 'monet';

import { Cache }                from '../../services/shared/cache.service';
import { JobService }           from '../../services/shared/JobService';

import { FilterPipe }           from '../../lib/pipes/filter';
import { GreaterPipe }          from '../../lib/pipes/later';
import { UserProfile }          from '../../lib/interfaces/user/profile';

import { NewsItem }             from './news-item/news-item.component';
import { SendMessage }          from './send-message/send-message.component';

import { AppStoreAction }       from '../../store/action';

import { htmlNews }             from './news.html.ts';

import { isEmpty }              from 'ramda';

interface INews {
  compiled: string;
  deleted: boolean;
  message_id: number;
  nname: string;
  read: boolean;
  title: string;
  uname: string;
  valid_from: number;
}

type MessageCollection = INews[];

@Component({
  selector: 'jobber-news',
  directives: [NewsItem, SendMessage, InfiniteScroll ],
  pipes: [ FilterPipe, GreaterPipe ],
  styles: [ require('!raw!less!./styles.less') ],
  template: htmlNews
})
export class JobberNews {

  @select(store => store.info.news) news$: any;
  @select(store => store.info.webuser) user$: any;
  @select(store => store.info.manager) manager$: any;
  @select(state => state.broadcastEvent) eventStream$: any;
  @select('info') info$: any;

  public hideScrollBar = this.jobService.hideScrollBar;

  private subscriptions: any[] = [];
  private subscribeNew: any[] = [];
  private itemOpened = {};
  private removedStatus;
  private sendMessageOpened = false;
  private messageSendingError = false;
  private messageSendingSuccess = false;
  private messageSendingInfo = '';

  private isManager: boolean;
  private registrationTime: number;

  private selectedPage = 1;

  private newsList: MessageCollection;

  constructor(private action: AppStoreAction, private cache: Cache,
              private jobService: JobService, private zone: NgZone,
              private route: ActivatedRoute) {

    this.subscriptions = [
      this.user$.subscribe(u =>
        Maybe.fromNull(u).bind((ub: UserProfile) => {
          this.registrationTime = ub.ts_registry * 1000;
          return Just(ub.ts_registry);
        })
      ),
      this.manager$.subscribe(m =>
        Maybe.fromNull(m).bind(mm => Just(this.isManager = !!mm))
      ),
      this.news$.subscribe((news: MessageCollection) => {
        // had to add this instead of the 'async' pipe
        // as changes from the event stream won't render
        this.zone.run(() => this.newsList = news);
      }),
      this.eventStream$.subscribe(e => {
        if (!isEmpty(e))
          this.action.fetchGetInfo();
      })
    ];

  }

  ngOnInit() {

    this.removedStatus = this.cache.getObject('news:statusToggle') || false;
    const messageStates = this.cache.getObject(
      'news:messageOpened'
    ) || [];

    messageStates.forEach(s => this.itemOpened[s] = true);

    this.selectedPage = this.cache.getObject('news:page') || 1;

    // this.ref.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscribeNew.forEach(s => s.unsubscribe());
  }

  handleMessageAction($event) {
    switch ($event.action) {
      case 'toggle':
        if (!this.itemOpened[$event.id]) {
          this.cache.pushArray(
            'news:messageOpened', `${$event.id}`
          );
        } else {
          this.cache.removeFromArray(
            'news:messageOpened', `${$event.id}`
          );
        }
        this.itemOpened[$event.id] = !this.itemOpened[$event.id];
        this.subscribeNew.push(
          this.news$.subscribe(n => {
            if (n) n.find((el: INews) => el.message_id === $event.id).read = true;
          })
        );
        break;
      case 'remove':
        this.action.fetchGetInfo();
        break;
      default: // nothing
    }
  }

  closedModal(event) {
    if (event) {
      switch (event.status) {
        case 'error':
          this.messageSendingError = true;
          this.messageSendingInfo = event.message;
          break;
        case 'success':
          this.action.fetchGetInfo();
          this.messageSendingSuccess = true;
          break;
        default: // nothing
      }
    }
    this.sendMessageOpened = false;
  }

  updateStatuses() {
    this.messageSendingError = false;
    this.messageSendingSuccess = false;
  }

  get successStatus() {
    return this.messageSendingSuccess;
  }

  get errorStatus() {
    return this.messageSendingError;
  }

  toggleRemovedStatus() {
    setTimeout(() => {
      // todo: new rb
      this.cache.set('news:statusToggle', this.removedStatus);
    }, 0);
  }

  pageChange($event) {
    this.selectedPage = $event;
    this.cache.set('news:page', $event);
  }

}

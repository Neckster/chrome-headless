import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ComponentFactory,
  ComponentResolver
} from '@angular/core';

import { DynamicBuilder } from '../../../lib/directives/dynamic-builder';

import { htmlNewsItem } from './news-item.html.ts';
import { MessageService } from '../../../services/message.service';
import { CustomDatePipe } from '../../../lib/pipes/date';
import { EmitService } from '../../../services/emit.service';
import { Observable } from 'rxjs/Rx';
import { EmptyObservable } from 'rxjs/observable/EmptyObservable';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';


@Component({
  selector: 'news-item',
  providers: [ DynamicBuilder ],
  directives: [ SvgTemplateDirective ],
  pipes: [ CustomDatePipe ],
  styles: [ ],
  template: htmlNewsItem
})

export class NewsItem {

  @Input() news;
  @Output() out = new EventEmitter();
  @ViewChild('dynamicChild', {read: ViewContainerRef})

  private target: ViewContainerRef;
  private messageRead = false;
  private props: any;

  constructor(private message: MessageService, private emitter: EmitService,
    private builder: DynamicBuilder, private componentResolver: ComponentResolver) {
  }

  ngOnInit() {
    if (this.news.compiled.indexOf('href=') !== -1) {
      this.news.compiled = this.news.compiled.replace(
        /href="#{0,1}\D+current"/g,
        'routerLink="/assignment/current"');
      this.news.compiled = this.news.compiled.replace(
        /href="#{0,1}\D+assignment"/g,
        'routerLink="/assignment/future"');
    }
    this.props = {
      str: this.news.compiled
    };

    let template = this.props.str;

    let component = this.builder.createComponent(template, [], []);
    this.componentResolver.resolveComponent(component)
      .then( (factory: ComponentFactory<any>) => {
        let dynComponent = this.target.createComponent(factory, 0);
        let instance: any = dynComponent.instance;
        instance.props = this.props;
      });

  }

  removeMessage(isRead: boolean, id: number): void {

    let read = this.invokeReadMessageService(isRead, id);
    let remove = this.message.remove(id);

    Observable.concat(read, remove).reduce((acc: Array<Observable<any>>, x: Observable<any>) => {
      acc.push(x);
      return acc;
    }, []).subscribe((res) => {
      this.out.emit({
        action: 'remove', id
      });
    }, (err) => {
      console.error(err);
    });
  }

  readMessage(read: boolean, id: number): void {
    this.invokeReadMessageService(read, id).subscribe((res) => {
      this.messageRead = true;
      this.emitter.emit('messageUpdate', {status: 'read', amount: 1, id});
    }, (err) => {
      console.error(err);
      this.messageRead = false;
    });
  }

  test() {
  }

  invokeReadMessageService(read: boolean, id: number): Observable<any> {
    return this.messageRead || read ?
      EmptyObservable.create() : this.message.read(id);
  }

}

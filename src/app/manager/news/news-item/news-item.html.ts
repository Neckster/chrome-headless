export const htmlNewsItem = `
<div class="uk-nestable-panel">
  <div class="uk-nestable-toggle" data-nestable-action="toggle" (click)="readMessage(news.read, news.message_id);out.emit({action: 'toggle', id: news.message_id})"></div>
  <div class="uk-grid uk-grid-collapse uk-width-1-1">
    <div class="uk-width-7-10 uk-text-truncate jc-newsheading">
      {{news.title}}
    </div>
    <div class="uk-width-2-10 uk-clearfix">
      <span class="jc-newsdate uk-float-right" 
            style=" padding-right: 8px">{{news.valid_from | cdate }}</span>
    </div>
    <div *ngIf="!news.deleted" class="uk-width-1-10 uk-clearfix uk-position-relative" >
      <div class="delete">
      <i (click)="removeMessage(news.read, news.message_id)" [svgTemplate]="'trashCan'"></i>
      </div>
    </div>
  </div>
</div>
<ul class="uk-nestable-list">
  <li>
    <div class="uk-width-1-1 uk-text-bold">
      <span class="jc-uname">
        {{news.uname}}
      </span> 
      <span class="jc-nname"></span>
    </div>
    <div class="uk-width-1-1">
      <div class="jc-newstext newstext" #dynamicChild></div>
    </div>
  </li>
</ul>
`;

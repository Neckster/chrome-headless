import { Component, Input, Output, EventEmitter } from '@angular/core';
import { select } from 'ng2-redux';
import { unitHtml } from './unit.html';
import { OrganizationService } from '../../services/organisation.service';
import { TreeViewComponent } from '../tree-view/tree-view.component';
import { AppStoreAction } from '../../store/action';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { JobService } from '../../services/shared/JobService';

import { Directory } from '../organisation-tree/tree-view/directory';

@Component({
  selector: 'select-unit',
  providers: [],
  directives: [TreeViewComponent],
  pipes: [],
  styles: [],
  template: unitHtml
})

export class UnitComponent {

  @Input() opened;
  @Output() closeEmitter = new EventEmitter();
  @select() companyName$;
  @select() info$;
  @select() currentUnit$;

  public info: any;
  public companies: any[];
  public company: any;
  // public currCompanyName = ParseOrganisationService.currCompany;
  public currentCompany: any;
  public close: any;
  public pos: any = ParseOrganisationService;
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public action: AppStoreAction,
              public organisationService: OrganizationService,
              public jobService: JobService) {

    this.unsubscribe.push(this.info$.subscribe(info => {
      if (info.manager) {
        this.info = info;
        this.companies = this.info.company
          .filter(el => { return el.company_id !== info.manager.selected.company_id; });
        if (this.info.manager.selected.company_id && !this.currentCompany) {
          this.organisationService.getOrganisation(this.info.manager.selected.company_id)
            .subscribe(res => {
            ParseOrganisationService.treeStruct(res.manager.organisation);
            let treeData = ParseOrganisationService.treeData[0];
            let org = res.company.filter(comp => {
              return comp.company_id === res.manager.selected.company_id; })[0];
            this.action.setCurrentOrganisation(org);
            this.currentCompany = [new Directory(
              treeData.id,
              treeData.text,
              ParseOrganisationService.getDir(treeData.child),
              ParseOrganisationService.getFiles(treeData.child),
              treeData.type, treeData.data)];
          });
        }
      }
    }));

    this.pos = ParseOrganisationService;
    this.close = this.closeEmitter;
    this.closeEmitter.subscribe(em => {  this.opened = false; });
  }
  ngOnChanges() {
  }

  updateStaff() {
    this.closeEmitter.emit(undefined);
  }
  selectCompany(company: any) {
    if ( this.pos.isEditCompany ) this.hideScrollBar();
    this.organisationService.getOrganisation(company.company_id).subscribe(res => {
      let obj = {};
      res.manager.organisation.forEach(org => {obj[org.id] = false; });
      ParseOrganisationService.treeViewOpen = Object.assign({}, obj);
      ParseOrganisationService.treeStruct(res.manager.organisation);
      this.companies = this.info.company
        .filter(el => { return el.company_id !== company.company_id; });
      this.action.setUname('Choose Unit');
      this.action.setCname(res.manager.selected.cname);
      let treeData = ParseOrganisationService.treeData[0];
      let org = res.company.filter(comp => {
        return comp.company_id === res.manager.selected.company_id; })[0];
      this.action.setCurrentOrganisation(org);
      if (ParseOrganisationService.isEditCompany) {
        this.action.fetchGetInfo();
        this.currentCompany = [new Directory(
          treeData.id,
          treeData.text,
          ParseOrganisationService.getDir(treeData.child ? treeData.child : []),
          ParseOrganisationService.getFiles(treeData.child ? treeData.child : []),
          treeData.type, treeData.data)];
        this.closeEmitter.emit(undefined);
        return undefined;
      } else {
        this.currentCompany = [new Directory(
          treeData.id,
          treeData.text,
          ParseOrganisationService.getDir(treeData.child ? treeData.child : []),
          ParseOrganisationService.getFiles(treeData.child ? treeData.child : []),
          treeData.type, treeData.data)];
        let top = document.getElementById('top');
        top.scrollIntoView(false);
      }

    });
  }
  ngOnInit() {
    this.pos = ParseOrganisationService;
    this.close = this.closeEmitter;
  }
  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe());
  }
  closeUnitModal( e ) {
    e.preventDefault();
    this.updateStaff();
    this.hideScrollBar();
  }


}

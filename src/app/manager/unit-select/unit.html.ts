export const unitHtml = `
<div *ngIf="opened">

  <section class="uk-modal uk-open" id="unit" tabindex="-1" role="dialog" aria-labelledby="label-unit" aria-hidden="false" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <a class="uk-modal-close uk-close" (click)="closeUnitModal($event);" title ='{{ "dismiss" | translate }}'  data-close="dismiss" data-dismiss="modal">
      <!--icon-->
      </a>
      <div  id="top" class="uk-modal-header">
          <h2><a (click)="closeUnitModal($event);"
                  data-close="dismiss" data-dismiss="modal" 
                  class="uk-modal-close"><!--icon--></a>
          <span *ngIf="!pos.isEditCompany">{{'Choose_Unit' | translate}}<span class="selected cname">{{companyName$ | async}}</span>)</span>
          <span *ngIf="pos.isEditCompany" >{{'Company' | translate}}</span>
          </h2>
      </div>
      <div  class="modal-content">
        <ul class="uk-nav uk-nav-side uk-nav-parent-icon" id="unitsel">
        <!--<li class="uk-parent jc-template-nav-group uk-open" aria-expanded="true">-->
          <!--FIXME: href -->
        <!--<a href="#" class="jc-name">Test Firma</a>-->
        <!--<div style="overflow: hidden; position: relative;" class=""><ul class="uk-nav-sub uk-nav-parent-icon jc-children"><li class="uk-parent jc-template-nav-group">-->
          <!--&lt;!&ndash;FIXME: href &ndash;&gt;-->
        <!--<a href="#" class="jc-name">Group</a>-->
        <!--<ul class="uk-nav-sub uk-nav-parent-icon jc-children"></ul>-->
        <!--</li><li class="jc-template-nav-item nav-item">-->
        <!--<a class="jc-name jc-click-item jc-unit_id-150324">Unit</a>-->
        <!--</li></ul></div>-->
        <!---->
        <!--</li>-->
        <div *ngIf="currentCompany != undefined">
        <tree-view *ngIf="!pos.isEditCompany"  [close]="close" [company]="currentCompany" [child]="false" ></tree-view>
        </div>
        <li *ngIf="!pos.isEditCompany && companies.length > 0" class="uk-nav-header jc-template-nav-header-company">{{'Change_company' | translate}}</li>
        
        <template [ngIf]="companies.length > 0">
          <li class="jc-template-nav-item nav-item" *ngFor="let company of companies">
           <a target="#top" class="jc-name jc-click-item jc-company_id-150062" (click)="selectCompany(company)">{{company ? company.cname : ''}}</a>
          </li>
        </template>
       
        </ul>
      </div>
    </div>
  </section>
</div>`


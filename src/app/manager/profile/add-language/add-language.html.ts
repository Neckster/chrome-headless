export const addLangModal: string = `
<div id="modal" *ngIf="opened">
  <section id="addlanguage" class="uk-modal uk-open" tabindex="-1" role="dialog" aria-labelledby="label-lang-select" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="modal-overlay" style="" (click)="hideScrollBar(); out.emit({name: 'close'})"></div>
      <div class="uk-modal-dialog">
        <a class="modal-close uk-modal-close uk-close" (click)="hideScrollBar(); out.emit({name: 'close'})"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header" id="label-lang-select">
          <h2><a (click)="out.emit({name: 'close'}); hideScrollBar()"  class="uk-modal-close" aria-hidden="false"></a>
            <span>{{'Select_language' | translate}}</span>
          </h2>
        </div>
        <div class="modal-content">
        
       
          <a *ngFor="let lang of langsList | filter:'active':true" 
             (click)="$event.preventDefault();out.emit({ name: 'add', lang: lang.code });hideScrollBar()" 
             class="">
           <div class="uk-grid uk-margin">
              <div class="uk-width-5-6">{{lang.name | translate}}</div>
              <div class="uk-width-1-6 uk-text-right"><span class="icon plus circle inverse"></span></div>
            </div>
          </a>
          
        </div>
      </div>
    </section>
</div>
`;

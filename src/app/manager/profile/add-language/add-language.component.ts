import { Component, Input, Output, EventEmitter } from '@angular/core';
import { addLangModal } from './add-language.html.ts';
import { Observable } from 'rxjs/Rx';
import { FilterPipe } from '../../../lib/pipes/filter';
import { EmitService } from '../../../services/emit.service';
import { JobService } from '../../../services/shared/JobService';
@Component({
  selector: 'add-language',
  providers: [ ],
  directives: [ ],
  pipes: [ FilterPipe ],
  styles: [
    `
    .modal-overlay {
      position: absolute;
      height: 100%;
      width: inherit;
    }
  `
  ],
  template: addLangModal
})

export class AddLanguage {

  @Input() opened;
  @Input() langs;
  @Output() out = new EventEmitter();

  public hideScrollBar = this.jobService.hideScrollBar;
  private langsList = [
    {
      code: 'de',
      name: 'lang_German',
      active: false
    },
    {
      code: 'en',
      name: 'lang_English',
      active: false
    },
    {
      code: 'fr',
      name: 'lang_French',
      active: true
    },
    {
      code: 'es',
      name: 'lang_Spanish',
      active: true
    },
    {
      code: 'it',
      name: 'lang_Italian',
      active: true
    },
    {
      code: 'pt',
      name: 'lang_Portuguese',
      active: true
    },
    {
      code: 'ru',
      name: 'lang_Russian',
      active: true
    },
    {
      code: 'hi',
      name: 'lang_Hindi',
      active: true
    },
    {
      code: 'ar',
      name: 'lang_Arabic',
      active: true
    },
    {
      code: 'zh',
      name: 'lang_Chinese',
      active: true
    },
    {
      code: 'ja',
      name: 'lang_Japanese',
      active: true
    },
    {
      code: 'ko',
      name: 'lang_Korean',
      active: true
    },
    {
      code: 'bn',
      name: 'lang_Bengali',
      active: true
    },
    {
      code: 'th',
      name: 'lang_Tai_Kadai',
      active: true
    }
  ];

  constructor(private emitter: EmitService, private jobService: JobService) {
    this.emitter.addEvent('langChange').subscribe((change) => {
      if (change.status === 'available') {
        this.langsList.find((el) => el.code === change.code).active = true;
      } else {
        this.langsList.find((el) => el.code === change.code).active = false;
      }
    });
  }

  ngOnInit() {
  }


}

export const skillsValues = {
  'entertain': {
    'cashier': { skill: 'none', jobInterest: false },
    'cloakroom':  { skill: 'none', jobInterest: false },
    'host':  { skill: 'none', jobInterest: false },
    'security':  { skill: 'none', jobInterest: false },
    'steward':  { skill: 'none', jobInterest: false },
    'usher':  { skill: 'none', jobInterest: false },
    'driver':  { skill: 'none', jobInterest: false },
    'flagman':  { skill: 'none', jobInterest: false },
    'logistics':  { skill: 'none', jobInterest: false },
    'roadie':  { skill: 'none', jobInterest: false },
    'stagehand':  { skill: 'none', jobInterest: false },
    'rigger':  { skill: 'none', jobInterest: false },
    'event-technician':  { skill: 'none', jobInterest: false },
    'forklift':  { skill: 'none', jobInterest: false}
  },
  'gastro': {
    'principal': { skill: 'none', jobInterest: false },
    'service': { skill: 'none', jobInterest: false },
    'sommelier':  { skill: 'none', jobInterest: false },
    'buffet':  { skill: 'none', jobInterest: false },
    'bar-principal':  { skill: 'none', jobInterest: false },
    'bar':  { skill: 'none', jobInterest: false },
    'runner':  { skill: 'none', jobInterest: false },
    'head-chef':  { skill: 'none', jobInterest: false },
    'cook':  { skill: 'none', jobInterest: false },
    'kitchen-help':  { skill: 'none', jobInterest: false },
    'scullery':  { skill: 'none', jobInterest: false },
    'reception':  { skill: 'none', jobInterest: false },
    'allround':  { skill: 'none', jobInterest: false },
    'restaurant-manager':  { skill: 'none', jobInterest: false}
  },
  'hotel': {
    'reception':  { skill: 'none', jobInterest: false },
    'head-porter':  { skill: 'none', jobInterest: false },
    'concierge':  { skill: 'none', jobInterest: false },
    'porter':  { skill: 'none', jobInterest: false },
    'driver':  { skill: 'none', jobInterest: false },
    'housekeeping-principal':  { skill: 'none', jobInterest: false },
    'housekeeping':  { skill: 'none', jobInterest: false },
    'room-service':  { skill: 'none', jobInterest: false },
    'fitness':  { skill: 'none', jobInterest: false },
    'shop':  { skill: 'none', jobInterest: false },
    'f-b-manager':  { skill: 'none', jobInterest: false },
    'division-manager':  { skill: 'none', jobInterest: false },
    'c-b-manager':  { skill: 'none', jobInterest: false}
  },
  'tourism': {
    'tour-guide':  { skill: 'none', jobInterest: false },
    'cashier':  { skill: 'none', jobInterest: false },
    'technician':  { skill: 'none', jobInterest: false },
    'letting':  { skill: 'none', jobInterest: false },
    'instructor-skiing':  { skill: 'none', jobInterest: false },
    'instructor-water':  { skill: 'none', jobInterest: false },
    'bus-driver':  { skill: 'none', jobInterest: false}
  },
  'admin': {
    'allround':  { skill: 'none', jobInterest: false },
    'secretary':  { skill: 'none', jobInterest: false },
    'finance':  { skill: 'none', jobInterest: false },
    'marketing':  { skill: 'none', jobInterest: false },
    'hr':  { skill: 'none', jobInterest: false },
    'event':  { skill: 'none', jobInterest: false },
    'promotion':  { skill: 'none', jobInterest: false },
    'production':  { skill: 'none', jobInterest: false },
    'sales':  { skill: 'none', jobInterest: false },
    'buying':  { skill: 'none', jobInterest: false },
    'warehouse':  { skill: 'none', jobInterest: false },
    'cleaning':  { skill: 'none', jobInterest: false },
    'building-services':  { skill: 'none', jobInterest: false },
    'it':  { skill: 'none', jobInterest: false}
  },
  'lang': {
    'de': { skill: 'none', active: true, persist: true },
    'en': { skill: 'none', active: true, persist: true },
    'fr': { skill: 'none', active: false },
    'es': { skill: 'none', active: false },
    'it': { skill: 'none', active: false },
    'pt': { skill: 'none', active: false },
    'ru': { skill: 'none', active: false },
    'hi': { skill: 'none', active: false },
    'ar': { skill: 'none', active: false },
    'zh': { skill: 'none', active: false },
    'ja': { skill: 'none', active: false },
    'ko': { skill: 'none', active: false },
    'bn': { skill: 'none', active: false },
    'th': { skill: 'none', active: false }
  }
};

//noinspection TsLint
export const avatarChangeHTML:string = `
    <label class="uploader" ondragover="return false;"
          [class.loaded]="loaded" 
          [style.outlineColor]="dragging ? activeColor : baseColor"
          (dragenter)="handleDragEnter()"
          (dragleave)="handleDragLeave()"
          (drop)="handleDrop($event)">
          
          <i class="infoCloud" [@animate]="'fadeIn'" *ngIf="!imageLoaded" [svgTemplate]="'cloudStorageUpload'"></i>
          <i class="infoCloud imageWasLoaded" [@animate]="'fadeIn'" *ngIf="imageLoaded" [svgTemplate]="'cloudStorageUpload'"></i>
          
          <img *ngIf="imageSrc"
              [src]="imageSrc" 
              (load)="handleImageLoad()" 
              [@animate]="'zoomIn'"
              [class.loaded]="imageLoaded"/>
          
          <input type="file" name="file" accept="image/*"
              (change)="handleInputChange($event)">
      </label>
      <div class="uk-button-group avatarChangeButton">
          <button (click)="sendImg($event)" class="uk-button">Save</button>
          <button  (click)='clearAvatar()' class="uk-button">Clear</button>
      </div>
`;

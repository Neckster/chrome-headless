import { Component }          from '@angular/core';
import { avatarChangeHTML }    from './avatar-change.html';
import { NgRedux, select }    from 'ng2-redux';
import { AppStoreAction }     from '../../../store/action';
import { EmitService }        from '../../../services/emit.service';
import { Cache }              from '../../../services/shared/cache.service';
import { animateFactory }     from 'ng2-animate';
import * as R                 from 'ramda';
import { Observable }         from 'rxjs/Rx';
import { RootState }          from '../../../store/reducer';


@Component({
  selector: 'avatar-change',
  directives: [],
  pipes: [],
  styles: [require('./avatar-change-style.less')],
  template: avatarChangeHTML,
  animations: [animateFactory(400, 100, 'ease-in')]
})
export class AvatarChangeComponent {

  public borderColor: any;
  public iconColor: any;

  public activeColor: string = '#07a0e1';
  public baseColor: string = '#ccc';
  public overlayColor: string = 'rgba(255,255,255,0.5)';

  public dragging: boolean = false;
  public loaded: boolean = false;
  public imageLoaded: boolean = false;
  public imageSrc: string = '';

  constructor(private action: AppStoreAction,
              private emitter: EmitService,
              private cache: Cache,
              private ngRedux: NgRedux<RootState>) {}

  handleDragEnter() {

    this.dragging = true;
  }

  ngOnInit() {
    this.imageSrc = this.cache.get('changeAvatar');
  }

  handleDragLeave() {
    this.dragging = false;
  }

  handleDrop(e) {

    e.preventDefault();
    this.dragging = false;
    this.handleInputChange(e);
  }

  handleImageLoad() {

    this.imageLoaded = true;
    this.iconColor = this.overlayColor;
  }

  handleInputChange(e) {

    let file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    let pattern = /image-*/;
    let reader = new FileReader();

    if (!file) return;

    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }

    this.loaded = false;

    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    this.cache.set('changeAvatar', this.imageSrc);
    this.loaded = true;
  }
  sendImg(e) {
    e.preventDefault();
    if (this.imageSrc) {
      // api must be here!!!!
    }
  }

  _setActive() {

    this.borderColor = this.activeColor;
    if (this.imageSrc.length === 0) {
      this.iconColor = this.activeColor;
    }
  }

  _setInactive() {
    this.borderColor = this.baseColor;
    if (this.imageSrc.length === 0) {
      this.iconColor = this.baseColor;
    }
  }

  clearAvatar() {
    this.imageSrc = '';
    this.cache.set('changeAvatar', this.imageSrc);
    this.loaded = false;
    this.imageLoaded = false;
  }

}

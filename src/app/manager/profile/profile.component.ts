///<reference path="../../../../typings/globals/moment-range/index.d.ts"/>
import { Component,
        Input, Output }       from '@angular/core';
import {
  FORM_DIRECTIVES,
  REACTIVE_FORM_DIRECTIVES,
  FormControl }               from '@angular/forms';
import {
  FormBuilder,
  FormGroup,
  Validators }                from '@angular/forms';
import { profileTemplate }    from './profile.htm.ts';
import { UserProfile }        from '../../lib/interfaces/user/profile';
import { UserService }        from '../../services/user.service';
import { JubbrFormValidator } from '../../lib/validators/jubbr-validator';
import { NgRedux, select }    from 'ng2-redux';
import { AppStoreAction }     from '../../store/action';
import { AddLanguage }        from './add-language/add-language.component';
import { EmitService }        from '../../services/emit.service';
import { MapToIterable }      from '../../lib/pipes/map-to-iterable';
import { SnakePipe }          from '../../lib/pipes/snake';
import { Cache }              from '../../services/shared/cache.service';
import { JobService }         from '../../services/shared/JobService';
import { animateFactory }     from 'ng2-animate';
import { AvatarChangeComponent } from './avatar-change/avatar-change-component';
import * as R                 from 'ramda';
import { Observable }         from 'rxjs/Rx';
import { RootState }          from '../../store/reducer';
import * as moment            from 'moment';
import { skillsValues }       from './config/skills';
import { EventEmitter }       from '@angular/router-deprecated/src/facade/async';

const noop = R.always(undefined);


@Component({
  selector: 'profile',
  directives: [AddLanguage, FORM_DIRECTIVES,
          REACTIVE_FORM_DIRECTIVES, AvatarChangeComponent],
  pipes: [ MapToIterable, SnakePipe ],
  styles: [`
        label.intrest-job-chck span:before{
          font-size: 25px;
          top:3px;
        }
        .cd-tabs .scroll-right:after{
          /*display: none;*/
        }
        .firstStepMargin{
          position: relative;
          left: 100px;
        }
        
         @media(max-width: 768px) {
         .firstStepMargin{
            left: 0;
           }
         }
         .thirdStep .cd-tabs-navigation{
          text-align: center;
         }
         .thirdStep ul.cd-tabs-navigation {
            width: 100%;
            overflow-x: scroll;
            min-width: 625px;
         }
        .thirdStep .cd-tabs-navigation li{
          display: inline-block!important;
        }
     `],
  template: profileTemplate,
  animations: [animateFactory(400, 100, 'ease-in')]
})
export class JobberProfile {

  @select() info$;

  @Input() firstStep;
  @Input() thirdStep;
  @Input() submitForm;
  @Output() correctSubmit = new EventEmitter(true);

  public unsubscribe = [];
  public showLangTitle = false;
  public hideScrollBar = this.jobService.hideScrollBar;
  private personalDataState: boolean = true;

  private skillsMap = {
    entertain: true,
    gastro: false,
    hotel: false,
    tourism: false,
    admin: false,
    lang: false
  };

  // TODO: user separate config file
  private skillsValues = skillsValues;

  // TODO: use enum
  private scaleMapping = {
    '0': 'none',
    '1': 'basics',
    '2': 'good',
    '3': 'expert',
    'none': 0,
    'basics': 1,
    'good': 2,
    'expert': 3
  };

  private initialUmail = '';

  // TODO: error object
  private updateSuccess: boolean = false;
  private updateError: boolean = false;
  private emailInuseError: boolean = false;
  private ibanError: boolean = false;
  private emailChanged: boolean = false;
  private formSynced: boolean = false;
  private info;

  private updateUserForm: FormGroup;
  private needsUpdate = true;
  private showLangAlert = false;
  private addLanguageModalOpened = false;
  private isSending: boolean = false;
  private wasSubmitted: boolean = false;
  private birthdateTrace;
  private activeSubmit: boolean = true;
  private birthdayDate = moment();
  private birthdateDefault: any = '';
  private calendarDateOpen: boolean;

  constructor (private user: UserService,
              private jobService: JobService,
              private fb: FormBuilder,
              private action: AppStoreAction,
              private emitter: EmitService,
              private cache: Cache,
              private ngRedux: NgRedux<RootState>) {

    /* tslint:disable:no-string-literal */

    this.updateUserForm = this.fb.group({
      firstname: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32),
          JubbrFormValidator.trimmed as any
        ])
      ],
      lastname: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32)
        ])
      ],
      umail: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(64),
          JubbrFormValidator.mail as any,
          JubbrFormValidator.trimmed as any
        ])
      ],
      umailConfirm: [
        '',
        Validators.compose([
          Validators.minLength(2),
          Validators.maxLength(64),
          JubbrFormValidator.mail as any,
          JubbrFormValidator.trimmed as any
        ])
      ],
      cellphone: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(32),
          JubbrFormValidator.phoneNumber as any,
          JubbrFormValidator.trimmed as any
        ])
      ],
      street: [
        '',
        Validators.compose([
          Validators.minLength(4),
          Validators.maxLength(32),
          Validators.pattern('^([A-ZÄÖÜ][a-zäöüß]+(([,] )|( )|([-])))+[1-9][0-9]{0,3}[a-z]?$'),
          JubbrFormValidator.trimmed as any
        ])
      ],
      zip: [
        '',
        Validators.compose([
          Validators.minLength(4),
          Validators.maxLength(5),
          JubbrFormValidator.trimmed as any
        ])
      ],
      location: [
        '',
        Validators.compose([
          Validators.maxLength(64),
          JubbrFormValidator.trimmed as any
        ])
      ],
      country: [
        '',
        Validators.compose([
          // JubbrFormValidator.notDefaultOption as any
        ])
      ],
      birthdate: [
        '',
        Validators.compose([
          JubbrFormValidator.trimmed as any
        ])
      ],
      iban: [
        '',
          Validators.compose([
          Validators.minLength(0),
          Validators.maxLength(40),
          JubbrFormValidator.iBanValidator as any,
          JubbrFormValidator.trimmed as any
        ])
      ],
      socialid: [
        '',
        Validators.compose([
          Validators.maxLength(64),
          JubbrFormValidator.ssn as any,
          JubbrFormValidator.trimmed as any
        ])
      ],
      maritalStatus: [
        'none',
        Validators.compose([
          // JubbrFormValidator.notDefaultOption
        ])
      ],
      childrenAmount: [
        '',
        Validators.compose([
          Validators.pattern('^[0-9]{1,2}$')
        ])
      ],
      confession: [
        '', Validators.compose([
          Validators.minLength(2),
          Validators.maxLength(64),
          JubbrFormValidator.trimmed as any
        ])
      ]
    }, {
      validator: (formGroup) => {

        const checkConfirmation = R.compose(R.equals(formGroup.controls.umail.value), R.trim);
        const mailConfirmError = { confirmationInvalid: true };

        // if the umail confirmation was touched
        const matchUmails = R.ifElse(
          R.pathEq(['controls', 'umailConfirm', 'valid'], true),
          // check if it matches the umail field
          (f) => {
            return checkConfirmation(f.controls.umailConfirm.value) || !this.emailChanged ?
                    undefined : mailConfirmError;
          },
          () => undefined
        );
        return matchUmails(formGroup);
      }
    });

    this.birthdateTrace = [];

    // todo: upd this
    this.updateUserForm.controls['birthdate'].valueChanges.subscribe((...args) => {
      if (this.updateUserForm.controls['birthdate'].value) {
        this.birthdateTrace.push(this.updateUserForm.controls['birthdate'].value.length);
        const tail = this.birthdateTrace.slice(Math.max(this.birthdateTrace.length - 2, 1));
        const len = this.updateUserForm.controls['birthdate'].value.length;
        const [f, s] = tail;
        const desc = f > s;
        if (!desc && (len === 2 || len === 5))
          this.updateUserForm.controls['birthdate'].patchValue(
            this.updateUserForm.controls['birthdate'].value + '.'
          );
      }
    });

    this.updateUserForm.controls['umail'].valueChanges.subscribe(value => {
      this.umailChange(value);
    });

    this.updateUserForm.valueChanges.subscribe(val => this.wasSubmitted = false);

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      if (this.info.webuser) {

        this.populateFields(this.info.webuser).subscribe(
          () => {
            this.initialUmail = this.info.webuser.umail;
          },
          () => {},
          () => {
            this.updateUserForm.patchValue({
              'birthdate': R.ifElse((x) => !R.isNil(x),
                (b) => R.pipe(R.split('-'), R.reverse,
                        R.join('-'), R.replace('-', '.'), R.replace('-', '.'))(b),
                () => R.identity
              )(this.updateUserForm.value.birthdate)
            });
          }
        );
        this.birthdayDate = this.birthdateDefault = this.updateUserForm.value.birthdate ?
          moment(`${this.updateUserForm.value.birthdate}`, 'DD.MM.YYYY') : moment();
        this.birthdateDefault = this.updateUserForm.value.birthdate ?
                          moment(`${this.updateUserForm.value.birthdate}`, 'DD.MM.YYYY') : moment();

        // todo: refactor this
        for (let skillGroup in this.info.webuser.skills) {
          if (this.info.webuser.skills.hasOwnProperty(skillGroup)) {
            for (let skill in this.info.webuser.skills[skillGroup]) {
              if (this.info.webuser.skills[skillGroup].hasOwnProperty(skill)) {
                if (this.skillsValues[skillGroup][skill]) {

                  if (skillGroup === 'lang' && !this.skillsValues[skillGroup][skill].active) {
                    this.skillsValues[skillGroup][skill].active = true;
                  }

                  this.skillsValues[skillGroup][skill].skill = this.scaleMapping[
                    this.info.webuser.skills[skillGroup][skill].skill
                  ];

                  if (this.info.webuser.skills[skillGroup][skill].jobInterest) {
                    this.skillsValues[skillGroup][skill].jobInterest =
                      this.info.webuser.skills[skillGroup][skill].jobInterest;
                  }
                }
              }
            }
          }
        }

        this.showLangAlert = !this.checkLangsValidity();

        // todo: refactor this
        Object.keys(this.skillsValues['lang'])
          .filter((lang) => {
            return this.skillsValues['lang'][lang].active;
          })
          .forEach((lang) => {
            this.emitter.emit('langChange', {
              code: lang,
              status: 'unavailable'
            });
          });

        setTimeout(() => {
          if (this.cache.get('profile:state')) {
            this.setState(this.cache.get('profile:state'));
          }
        }, 0);

      }
    }));
    /* tslint:enable:no-string-literal */
  }

  ngOnInit() {
    this.info = this.ngRedux.getState().info;
    // this.avatarHash = 0;
  }
  ngOnChanges(change) {
    if (this.submitForm) {
      this.formSubmit(event, undefined, true);
    }
  }

  ngOnDestroy() {
    this.action.fetchGetInfo();
    this.unsubscribe.forEach(el => el.unsubscribe());
  }

  formSubmit(e, skillsObj, inside?) {
    this.wasSubmitted = true;
    this.updateSuccess = false;
    this.updateError = false;
    this.ibanError = false;
    this.emailInuseError = false;

    if (!this.updateUserForm.valid) {
      this.correctSubmit.emit({
        action: false
      });
      return;
    }

    const checkBadConf = R.allPass([
      R.pathEq(['umail', 'dirty'], true),
      R.pathEq(['umailConfirm', 'valid'], false)
    ]);


    if (checkBadConf(this.updateUserForm.controls)) {
      return;
    }

    if (e)
      e.preventDefault();

    if (skillsObj)
      this.needsUpdate = false;

    this.isSending = true;

    const filterTruthy = R.pickBy(R.allPass([
      x => x
      // x => R.is(String, x),
      // x => x.length
    ]));

    const trim = R.map(R.trim);
    const decorateUser = R.merge({ 'webuser_id': this.info.webuser.webuser_id });

    const decorateSkills = R.ifElse(
      () => !R.isNil(skillsObj),
      R.merge({ 'skills': skillsObj }),
      R.merge({ 'skills': this.info.webuser.skills })
    );

    const decorateBirthDate = R.ifElse(
      R.path(['birthdate']),
      (a) => R.merge(a)({'birthdate': R.pipe(R.split('.'), R.reverse, R.join('-'))(a.birthdate)}),
      R.identity
    );


    const decorateUmail2 = R.ifElse(
      () => this.emailChanged,
      R.merge({ 'umail2': this.updateUserForm.value.umailConfirm }),
      R.identity
    );

    const processProfileData = R.compose(
      decorateUmail2,
      decorateSkills,
      decorateUser,
      trim,
      decorateBirthDate,
      filterTruthy
    ); //


    this.activeSubmit = false;
    let childAmount = this.updateUserForm.value.childrenAmount;
    this.updateUserForm.patchValue({'childrenAmount': childAmount ? childAmount.toString() : ''});
    this.user.update(processProfileData(this.updateUserForm.value))
      .subscribe(
        (res: any) => {
        this.activeSubmit = true;
        this.populateFields(res);
        if (inside) {
          this.correctSubmit.emit({
            action: true
          });
        }
        this.updateSuccess = true;
        this.formSynced = true;

        if (this.needsUpdate) {
          this.populateFields(res);
          this.action.fetchGetInfo();
        }

      this.birthdateTrace.length = 0;
    }, (err) => {
      try {
        const resp = JSON.parse(err._body);
        if ('invalid' in resp) {
          Object.keys(resp.invalid).forEach(el => {
            this.updateUserForm.controls[el].setErrors({
              [resp.invalid[el]]: true
            });
          });
        }

      } catch (e) {
        this.updateError = true;
      }
      this.updateError = true;
    });
  }

  populateFields(data: UserProfile): Observable<any> {
    return Observable.create((observer) => {
      Object.keys(data).forEach(key => {
        if (this.updateUserForm.controls.hasOwnProperty(key)) {
          this.updateUserForm.controls[key].patchValue(data[key]);
          observer.next({[key]: data[key]});
        }
      });
      observer.complete();
    });
  }

  radioChange(skillName: string) {

    let componentRef = this;
    let requestPayloadSkills = {};
    let payload;
    const [skillGroup, skill, prop] = skillName.split('-');

    setTimeout(() => {

      if (skillGroup === 'lang') {
        this.showLangAlert = !this.checkLangsValidity();
      }

      // TODO: refactor this thing
      /* tslint:disable:no-shadowed-variable */
      Object.keys(this.skillsValues).forEach((skillGroup) => {
        Object.keys(this.skillsValues[skillGroup]).forEach((skill) => {
          if (this.skillsValues[skillGroup][skill].skill !== 'none' ||
            this.skillsValues[skillGroup][skill].jobInterest) {
            requestPayloadSkills[skillGroup] = requestPayloadSkills[skillGroup] || {};
            requestPayloadSkills[skillGroup][skill] = {};
            requestPayloadSkills[skillGroup][skill].jobInterest =
              this.skillsValues[skillGroup][skill].jobInterest;
            requestPayloadSkills[skillGroup][skill].skill =
              this.scaleMapping[this.skillsValues[skillGroup][skill].skill];
            if (skillGroup === 'lang')
              requestPayloadSkills[skillGroup][skill].active = true;

          }
        });
      });
      /* tslint:enable:no-shadowed-variable */

      this.user.updateSkills(this.info.webuser.webuser_id, requestPayloadSkills).subscribe(
        noop, noop, noop
      );

    }, 0);
  }

  checkLangsValidity() {
    /* tslint:disable:no-string-literal */
    return Object.keys(this.skillsValues['lang']).some((lang) => {
      return this.skillsValues['lang'][lang].skill !== 'none';
    });
    /* tslint:enable:no-string-literal */
  }

  umailChange(val): void {
    this.emailChanged = val !== this.initialUmail;
    if (!this.emailChanged) this.updateUserForm.patchValue({umailConfirm: ''});
  }

  switchState(): void {
    this.personalDataState = !this.personalDataState;
  }

  setState(state: string): void {
    switch (state) {
      case 'P_DATA':
        this.cache.set('profile:state', 'P_DATA');
        this.personalDataState = true;
        break;
      case 'SKILLS':
        this.cache.set('profile:state', 'SKILLS');
        this.personalDataState = false;

        const skillSelected = this.cache.get('profile:skill');
        if (skillSelected)
          this.setSkillState(skillSelected);

        break;
      default: // empty
    }
  }

  setSkillState(skill: string): void {
    this.cache.set('profile:skill', skill);
    for (let p in this.skillsMap) {
      if (this.skillsMap.hasOwnProperty(p)) {
        this.skillsMap[p] = skill === p;
      }
    }
    this.showLangTitle = false;
    if ( skill === 'lang' )  this.showLangTitle = true;
  }

  add(event) {
    this.addLanguageModalOpened = true;
  }

  removeLang(lang: string) {
    /* tslint:disable:no-string-literal */
    this.skillsValues['lang'][lang].active = false;
    this.skillsValues['lang'][lang].skill = 'none';
    this.radioChange(`lang-${lang}`);
    this.emitter.emit('langChange', {
      'status': 'available',
      'code': lang
    });
    /* tslint:enable:no-string-literal */
  }

  handleModal($event) {
    /* tslint:disable:no-string-literal */
    switch ($event.name) {
      case 'close':
        this.addLanguageModalOpened = false;
        break;
      case 'add':
        this.addLanguageModalOpened = false;
        this.skillsValues['lang'][$event.lang]['active'] = true;
        this.emitter.emit('langChange', {
          'status': 'unavailable',
          code: $event.lang
        });
        break;
      default: // empty
    }
    /* tslint:enable:no-string-literal */
  }

  scrollingArrow( e ) {
    let el = e.target,
        sum =  el.scrollWidth - el.clientWidth - el.scrollLeft ,
        arrScroll = document.getElementById('ArrowScroll');
    if ( el.scrollLeft > 0 && sum > 8  ) {
        arrScroll.classList.add('scroll-left');
        arrScroll.classList.add('scroll-right');
    } else if ( sum < 8  ) {
      arrScroll.classList.remove('scroll-right');
    } else if ( el.scrollLeft === 0 ) {
      arrScroll.classList.remove('scroll-left');
      arrScroll.classList.add('scroll-right');
    }
  }

  _isUmailValid(): boolean {
    return this.updateUserForm.value.umail ===
      (this.updateUserForm.value.umailConfirm || this.updateUserForm.value.umail2);
  }

  setBirthdayDate(e) {
    if (e.action) {
      this.birthdateDefault = e.action;
      this.updateUserForm.patchValue({birthdate: e.action.format('DD.MM.YYYY')});
    }
    this.calendarDateOpen = false;
  }
}

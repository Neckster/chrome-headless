//noinspection TsLint
export const profileTemplate:string = `

<add-language [opened]="addLanguageModalOpened" (out)="handleModal($event)" [langs]="activeLanguages"></add-language>
<div class="page profile jobberview uk-clearfix">
  <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    <div
      class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-9-10 uk-width-xlarge-7-10 uk-container-center uk-grid-collapse">
      <h1 *ngIf="!firstStep && !thirdStep">{{'profile' | translate}}</h1>
       
      <ul *ngIf='!firstStep && !thirdStep' id="profile-subnav" class="uk-tab uk-tab-responsive jc-profile-subnav">
       
        <li id="profile-subnav-personal" aria-expanded="personalDataState"
            [ngClass]="{'uk-active': personalDataState}">
          <a [ngClass]="{'current': personalDataState}"
             (click)="$event.preventDefault();setState('P_DATA')">
             {{'Personal_data' | translate}}
          </a>
        </li>
        
        <li id="profile-subnav-skill" aria-expanded="!personalDataState"
            [ngClass]="{'uk-active': !personalDataState}">
          <a [ngClass]="{ 'current': !personalDataState }"
             (click)="$event.preventDefault();setState('SKILLS')">{{'Skills' | translate}}</a>
        </li>
        <!-- TODO li id="profile-subnav-rating"><a href="#jobber-profile">Bewertungen</a></li-->
        <!-- TODO li id="profile-subnav-contract"><a href="#jobber-profile">Verträge</a></li-->
        <li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true"
            aria-expanded="false">
            <a>{{'Personal_data' | translate}}</a>
          <div class="uk-dropdown uk-dropdown-small">
            <ul class="uk-nav uk-nav-dropdown"></ul>
            <div></div>
          </div>
        </li>
      </ul>
      <ul id="profile-subtab" class="uk-switcher uk-margin">
        <!-- jobber profile starts here-->
        <li *ngIf="firstStep ? true : thirdStep ? false : personalDataState"  
            [ngClass]="{'uk-active':!thirdStep && ( firstStep || personalDataState), 'firstStepMargin': firstStep}">
          <div class="uk-width-small-1-1 uk-width-large-8-10 uk-margin-large-top">
            <form class="uk-form uk-form-horizontal profile jc-template-replace-user-profile"
                  id="jc-profile-personal"
                  [formGroup]="updateUserForm" novalidate (submit)="formSubmit($event, undefined)">
                  
             
               <div *ngIf='!firstStep' class="avatarChangeWrap">
                  <avatar-change></avatar-change>
               </div>
              
              <div class="uk-form-row">
                <label class="required uk-form-label" for="profilefirstname">
                  {{'Firstname' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': updateUserForm.controls.firstname.dirty && !updateUserForm.controls.firstname.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10  profilefirstname"
                       type="text" 
                       formControlName="firstname"
                       id="profilefirstname" 
                       placeholder="{{'placeholder_Firstname' | translate}}" > 
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.firstname.valid && updateUserForm.controls.firstname.errors"
                     class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilelastname">
                  {{'Lastname' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': updateUserForm.controls.lastname.dirty && !updateUserForm.controls.lastname.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 profilelastname"
                       type="text"
                       id="profilelastname" 
                       placeholder="{{'placeholder_Lastname' | translate}}" 
                       formControlName="lastname">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.lastname.valid && updateUserForm.controls.lastname.errors?.required"
                     class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profileumail">
                  {{'Email' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': updateUserForm.controls.umail.dirty && !updateUserForm.controls.umail.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 profileumail user-success"
                       type="email" 
                       id="profileumail" placeholder="{{'placeholder_Email' | translate}}"
                       formControlName="umail">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.umail.valid && updateUserForm.controls.umail.errors?.required"
                     class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>

              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profileumail2">
                  <span class="cram">{{'repeat_when_changed' | translate}}</span>
                </label>
                
                <input [disabled]="!emailChanged"
                       [ngClass]="{'uk-form-danger': emailChanged && updateUserForm.controls.umailConfirm.dirty && !updateUserForm.controls.umailConfirm.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 profileumail2" 
                       type="email"
                       id="profileumail2" 
                       maxlength="128" 
                       placeholder="{{'placeholder_Email_confirmation' | translate}}" 
                       formControlName="umailConfirm" #umc>
                       
                <!--<div *ngIf="wasSubmitted && updateUserForm.errors?.confirmationInvalid"
                     class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'wrong_email_retype_pleas' | translate}}</p>
                </div>-->
              </div>

              <div class="uk-form-row">
              
                <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilecellphone">
                  {{'Cellphone' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': updateUserForm.controls.cellphone.dirty && !updateUserForm.controls.cellphone.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 profilecellphone" 
                       type="tel"
                       title="+99(99)99999999" 
                       id="profilecellphone" 
                       placeholder="{{'placeholder_Cellphone' | translate}}"
                       formControlName="cellphone">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.cellphone.valid && updateUserForm.controls.cellphone.errors" 
                     class="uk-alert uk-alert-warning" 
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
                
              </div>
              
              <div class="uk-form-row">
              
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilestreet">
                  {{'Street'| translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.street.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 profilestreet" 
                       type="text"
                       id="profilestreet"
                       placeholder="{{'placeholder_Street' | translate}}" 
                       formControlName="street">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.street.valid && updateUserForm.controls.street.errors?.required"
                     class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
                
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilezip">
                  {{'ZIP' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.zip.valid}"
                       class="uk-width-small-1-1 uk-width-medium-6-10 zip profilestreet" 
                       type="text"
                       id="profilezip"
                       placeholder="{{'placeholder_ZIP' | translate}}" 
                       formControlName="zip">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.zip.valid && updateUserForm.controls.zip.errors.required"
                     class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilelocation">
                  {{'Place' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.location.valid }"
                       class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                       type="text"
                       id="profilelocation" 
                       placeholder="{{'placeholder_Location' | translate}}" 
                       formControlName="location">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.location.valid && updateUserForm.controls.location.errors.required" 
                     class="uk-alert uk-alert-warning" 
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
              
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilecountry">
                  {{'Country' | translate}}
                </label>
                
                <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <!--<select [(ngModel)]="country.iso">-->
                <!--<option *ngFor="#country of countryArray" [value]="iso.alpha-2">{{title.Text}}</option>-->
                <!--</select>-->
                  <select [ngClass]="{'uk-form-danger': !updateUserForm.controls.country.valid }" 
                          id="profilecountry"
                          class="select profilecountry" 
                          formControlName="country">
                    <option selected="" value="none" disabled>{{'please_select' | translate}}</option>
                    <option value="CH">{{'country_CH' | translate}}</option>
                    <option value="DE">{{'country_DE' | translate}}</option>
                    <option value="AT">{{'country_AT' | translate}}</option>
                  </select>
                </div>
              </div>

              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilebirthdate">
                  {{'Date_of_birth' | translate}}
                </label>
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.birthdate.valid }" 
                       class="uk-width-small-1-1 uk-width-medium-6-10 date profilebirthdate user-success"
                       id="profilebirthdate" 
                       type="text"
                       [readonly]="true"
                       formControlName="birthdate" 
                       maxlength="10"
                       (click)="calendarDateOpen = true"
                       placeholder="{{'birthday_placeholder' | translate }}">
                <div *ngIf="calendarDateOpen" class="uk-float-right">
                  <my-date-picker (selectedDate)="setBirthdayDate($event)"
                      [minYear] = "1940"
                      [valueDate]="birthdayDate" 
                      [valueDay]="birthdateDefault">
                  </my-date-picker>   
                </div>       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.birthdate.valid && updateUserForm.controls.birthdate.errors"
                     class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}</p>
                </div>
                
              </div>
              <div class="uk-form-row">
              
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profileiban">
                  {{'IBAN' | translate}}
                </label>
               
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.iban.valid || updateUserForm.controls.iban.errors}"
                       class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                       type="text" 
                       id="profileiban" 
                       placeholder="IBAN (format CH00 0000 0000 0000 0000 0)"
                       formControlName="iban">
                <!--<div *ngIf="wasSubmitted && !updateUserForm.controls.iban.valid && updateUserForm.controls.iban.errors"
                     class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'msg_err_invalid_IBAN' | translate}}</p>
                </div>-->
                
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilesocialid">
                  {{'Social_Security_Number' | translate}}
                </label>
                
                <input [ngClass]="{'uk-form-danger': !updateUserForm.controls.socialid.valid}"
                       class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                       type="text" 
                       id="profilesocialid" 
                       placeholder="{{'placeholder_Social_Security_Number' | translate}}"
                       formControlName="socialid">
                       
                <div *ngIf="wasSubmitted && !updateUserForm.controls.socialid.valid && updateUserForm.controls.socialid.errors"
                     class="uk-alert uk-alert-warning" id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilemarital">
                  {{'Marital_status' | translate}}
                </label>
                
                <select [ngClass]="{'uk-form-danger': !updateUserForm.controls.maritalStatus.valid }" 
                        class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                        formControlName="maritalStatus">
                  <option selected="" value="none">{{'please_select' | translate}}</option>
                  <option value="unmarried">{{ 'unmarried'| translate }}</option>
                  <option value="married">{{ 'married'| translate }}</option>
                  <option value="divorced">{{ 'divorced'| translate }}</option>
                </select>
                
                <div *ngIf="wasSubmitted && !updateUserForm.controls.maritalStatus.valid && updateUserForm.controls.maritalStatus.errors"
                     class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilechildren">
                  {{'Number_of_children' | translate}}
                </label>
                
                <input class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                       type="number"
                       id="profilechildren" 
                       min="0" 
                       [ngClass]="{'uk-form-danger': !updateUserForm.controls.childrenAmount.valid }" 
                       formControlName="childrenAmount">
                       
                <div *ngIf="false" class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
                
              </div>
              
              <div class="uk-form-row">
                <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profileconfession">
                  {{'Confession' | translate}}
                </label>
                
                <input class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                       type="text"
                       formControlName="confession"
                        [ngClass]="{'uk-form-danger': !updateUserForm.controls.confession.valid }" 
                       id="profileconfession">
                      
                <div *ngIf="false" class="uk-alert uk-alert-warning"
                     id="errorbox-1">
                  <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}.</p>
                </div>
                
              </div>
              
              <div *ngIf="!firstStep && !thirdStep && updateSuccess && updateUserForm.controls.iban.valid"
                   class=" uk-width-1-1 uk-alert uk-alert-success error ok"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_info_changes_saved' | translate}}
              </div>

              <div *ngIf="wasSubmitted && !updateUserForm.controls.umailConfirm.value" 
                   class="uk-width-1-1 uk-alert uk-alert-warning error umail2 missing"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_err_repeat_email' | translate }}
              </div>
              
              <div *ngIf="wasSubmitted && updateUserForm.errors?.confirmationInvalid 
                            && updateUserForm.controls.umailConfirm.value"
                   class="uk-width-1-1 uk-alert uk-alert-warning error umail2 mismatch"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_err_repeat_email_mismatch' | translate}}
              </div>
              
              <div *ngIf="wasSubmitted && emailInuseError"
                   class="uk-width-1-1 uk-alert uk-alert-warning error umail"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_err_repeat_email_inuse' | translate}}
              </div>
              
              <!--<div *ngIf="wasSubmitted && ibanError"
                   class="uk-width-1-1 uk-alert uk-alert-warning error iban malformed"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_err_invalid_IBAN' | translate}}
              </div>-->
              
              <div *ngIf="!updateUserForm.controls.iban.valid"
                   class="uk-width-1-1 uk-alert uk-alert-danger error internal"
                   data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>{{'msg_err_invalid_IBAN' | translate}}
              </div>

              <div *ngIf="!firstStep" class="uk-width-1-1 uk-container-center uk-grid-collapse">
                <div class="action-buttons uk-margin-top">
                  <button type="submit"
                          [disabled]="!activeSubmit || !updateUserForm.valid"
                          class="uk-width-1-1 ok uk-button uk-button-large uk-margin-bottom">
                    {{'Save_changes' | translate}}
                  </button>
                </div>
              </div>
            </form>

          </div>
          <!--usrdata-->
        </li>
        <!-- jobber profile tab ends here -->
        <!-- jobber capabilities starts here-->

        <li *ngIf="thirdStep ? true : firstStep ? false : !personalDataState" id="profile-subview-skill" 
        
            [ngClass]="{'uk-active':!firstStep && ( thirdStep || !personalDataState)}">
          <div class="jc-template-replace-user-capabilities">
            <form class="uk-form uk-form-horizontal capabilities" ngNoForm>
              <div class="cd-tabs uk-grid uk-grid-collapse uk-grid-divider " [ngClass]="{'thirdStep' : thirdStep}" >
                <div class="uk-width-1-1  scroll-right" id='ArrowScroll' [ngClass]="{'uk-width-large-3-10' : !thirdStep, 'uk-width-1-1': thirdStep}">
                  <nav id='tabsNavigation'  (scroll)='scrollingArrow( $event )'>
                    <ul class="cd-tabs-navigation" >
                      <li><a data-content="skills-entertain"
                             [ngClass]="{selected: skillsMap.entertain}"
                             (click)="setSkillState('entertain')">Events</a></li>
                      <li><a data-content="skills-gastro" [ngClass]="{selected: skillsMap.gastro}"
                             (click)="setSkillState('gastro')">{{'Gastronomy' | translate}}</a></li>
                      <li><a data-content="skills-hotels" [ngClass]="{selected: skillsMap.hotel}"
                             (click)="setSkillState('hotel')">{{'Hotels' | translate}}</a></li>
                      <li><a data-content="skills-tourism" [ngClass]="{selected: skillsMap.tourism}"
                             (click)="setSkillState('tourism')">{{'Tourism' | translate}}</a></li>
                      <li><a data-content="skills-admin" [ngClass]="{selected: skillsMap.admin}"
                             (click)="setSkillState('admin')">{{'Administration' | translate}}</a></li>
                      <li><a data-content="skills-lang" [ngClass]="{selected: skillsMap.lang}"
                             (click)="setSkillState('lang')">{{'Languages' | translate}}</a></li>
                    </ul>
                    <!-- cd-tabs-navigation -->
                  </nav>
                </div>
                <div class="uk-width-1-1 thirdStep" [ngClass]="{'uk-width-large-7-10' : !thirdStep, 'uk-width-1-1': thirdStep}">
                  <ul class="cd-tabs-content" style="height: auto;">
                    <li *ngFor="let skillTop of skillsValues | mapToIterable"
                      [ngClass]="{selected: skillsMap[skillTop.key]}">      
                      <div id="lang-alert"
                           [ngClass]="{'hidden': !(showLangAlert && skillTop.key === 'lang') }"
                           class="uk-alert uk-alert-warning uk-width-1-1 error missing"
                           data-uk-alert="">{{'msg_warn_missing_language_skill' | translate}}
                      </div>   
                       <ul class="uk-list uk-list-striped">
                        <li>
                          <div class="uk-grid uk-grid-collapse">
                            <div class="uk-width-small-8-10">
                              <div class="uk-grid uk-grid-collapse">
                                <div *ngIf="!showLangTitle" class="uk-width-small-1-1 uk-width-medium-1-2">{{'Function' | translate}}</div>
                                 <div *ngIf="showLangTitle" class="uk-width-small-1-1 uk-width-medium-1-2">{{'Languages' | translate}}</div>
                                <div class="uk-width-small-1-1 uk-width-medium-1-2">
                                  {{'Skills_self_assessment' | translate}}
                                </div>
                              </div>
                            </div>
                            <div class="uk-width-2-10" *ngIf="skillTop.key !== 'lang'">{{'Looking_for_job' | translate}}</div>
                          </div>
                          <div class="uk-grid uk-grid-collapse">
                            <div
                              class="uk-width-small-2-10 uk-width-medium-4-10 uk-width-large-4-10 uk-width-xlarge-4-10"></div>
                            <div
                              class="uk-width-small-6-10 uk-width-medium-4-10 uk-width-large-4-10 uk-width-xlarge-4-10">
                              <div class="uk-grid uk-grid-collapse uk-text-center">
                                <span class="uk-width-1-4">{{'skill_level_none' | translate}}</span>
                                <span class="uk-width-1-4">{{'skill_level_Basics' | translate}}</span>
                                <span class="uk-width-1-4">{{'skill_level_good' | translate}}</span>
                                <span class="uk-width-1-4">{{'skill_level_Expert' | translate}}</span>
                              </div>
                            </div>
                          </div>
                        </li>
                        
                        <template ngFor let-skill [ngForOf]="skillTop.value | mapToIterable">
                          <li *ngIf="skillTop.key !== 'lang' || skillTop.key === 'lang' && skillsValues[skillTop.key][skill.key]['active']" >
                            <div id="{{skillTop.key}}-{{skill.key}}" 
                                class="uk-grid uk-grid-collapse uk-clearfix">
                              <div class="uk-width-small-1-1 uk-width-medium-4-10">
                                <p>{{'skill_'+skillTop.key+'_' + skill.key | snake | translate}}</p>
                              </div>
                              <div class="uk-width-small-6-10 uk-width-medium-4-10 skill-block">
                                <div class="uk-grid uk-grid-collapse">
                                  <label class="uk-width-1-4">
                                    <input type="radio" name="{{skillTop.key}}-{{skill.key}}"
                                           (ngModelChange)="radioChange(skillTop.key+'-skill')"
                                           [(ngModel)]="skillsValues[skillTop.key][skill.key].skill"
                                           value="none"><span></span></label>
                                  <label class="uk-width-1-4">
                                    <input type="radio" name="{{skillTop.key}}-{{skill.key}}"
                                           (ngModelChange)="radioChange(skillTop.key+'-skill')"
                                           [(ngModel)]="skillsValues[skillTop.key][skill.key].skill"
                                           value="basics"><span></span></label>
                                  <label class="uk-width-1-4">
                                    <input type="radio" name="{{skillTop.key}}-{{skill.key}}"
                                           (ngModelChange)="radioChange(skillTop.key+'-skill')"
                                           [(ngModel)]="skillsValues[skillTop.key][skill.key].skill"
                                           value="good"><span></span></label>
                                  <label class="uk-width-1-4">
                                    <input type="radio" name="{{skillTop.key}}-{{skill.key}}"
                                           (ngModelChange)="radioChange(skillTop.key+'-skill')"
                                           [(ngModel)]="skillsValues[skillTop.key][skill.key].skill"
                                           value="expert"><span></span></label>
                                </div>
                              </div>
                              
                              <div  *ngIf="skillTop.key !== 'lang'" class="uk-width-small-2-10">
                                <label class="intrest-job-chck">
                                  <input type="checkbox"
                                         name="entertain-cashier-chk"
                                         (ngModelChange)="radioChange('jobInterest')"
                                         [(ngModel)]="skillsValues[skillTop.key][skill.key].jobInterest">
                                         <span></span></label>
                              </div>
                              
                              <div *ngIf="skillTop.key === 'lang' && !skillsValues[skillTop.key][skill.key].persist" 
                                    class="uk-width-2-10">
                                <a class="uk-link current" (click)="removeLang(skill.key)">{{'button_Delete' | translate}}</a>
                              </div>
                            </div>
                          </li>
                        </template>
                       </ul>
                       
                       <div *ngIf="skillsMap.lang" id="add-lang" class="uk-margin">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-1-1 uk-clearfix">
                            <a (click)="add($event);hideScrollBar()" class="uk-link uk-float-left uk-clearfix jc-click-addlanguage">
                              {{'plussign_add_language' | translate}}
                            </a>
                          </div>
                        </div>
                      </div>
                       
                    </li>
                  
                 
                  </ul>
                </div>
                <!-- cd-tabs-content -->
              </div>
              <!-- cd-tabs --></form>


          </div>
        </li>
        <!-- jobber profile tab ends here -->
        <li id="profile-subview-rating" class="jobber-feedback" aria-hidden="true">
          <div class="uk-text-left feedback-section jc-template-replace-feedback-section">
            <form class="uk-form uk-form-horizontal feedback-form uk-margin-bottom">
              <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                <div class="averagerating uk-width-1-1">
                  <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle uk-flex-wrap-reverse">
                    <h2>{{'Total_rating' | translate}}</h2>
                    <div class="uk-flex uk-flex-middle uk-flex-item-auto">
                      <select
                        class="uk-width-small-1-1 uk-width-1-1 jc-template-replace-select-rating"
                        disabled=""
                        id="ratingaverage" style="display: none;">
                        <option value="4" data-rating="full">{{'Fully_satisfied' | translate}}</option>
                        <option value="3.5" data-rating="half">{{'assessment_answer_Very_pleased' | translate}}</option>
                        <option value="3" data-rating="full">{{'OK_alright' | translate}}</option>
                        <option value="2.5" data-rating="half">{{'Could_be_better' | translate}}</option>
                        <option value="2" data-rating="full">{{'assessment_answer_Rather_unhappy' | translate}}</option>
                        <option value="1.5" data-rating="half">{{'assessment_answer_satisfaction_1_not_satisfied_at_all' | translate}}</option>
                        <option value="1" data-rating="full">{{'assessment_answer_Very_unhappy' | translate}}</option>
                        <option value="0.5" data-rating="half">{{'Not_at_all_satisfied' | translate}}</option>
                        <option value="0" selected="">{{'no_evaluation' | translate}}</option>
                      </select>
                      <span class="rateit" id="ratingaveragerateit"
                            data-rateit-backingfld="#ratingaverage"><button
                        id="rateit-reset-2" data-role="none" class="rateit-reset"
                        aria-label="reset rating"
                        aria-controls="rateit-range-2" style="display: none;"></button><span
                        id="rateit-range-2"
                        class="rateit-range"
                        tabindex="0" role="slider"
                        aria-label="rating"
                        aria-owns="rateit-reset-2"
                        aria-valuemin="4"
                        aria-valuemax="0"
                        aria-valuenow="null"
                        aria-readonly="true"
                        style="width: 0px; height: 16px;"><span
                        class="rateit-selected" style="height:16px"></span><span
                        class="rateit-hover"
                        style="height:16px"></span></span></span>
                      <span class="jc-template-replace-count-rating">(<span
                        class="jc-count">0</span>)</span>
                    </div>
                    <a id="manager-rate-btn"
                       class="uk-button uk-button-primary feedback-btn jc-click-managerrate">
                      <span>assess</span>
                      <span class="jc-hidden">{{'the_earliest' | translate}}</span>
                      <span class="jc-hidden jc-feedbackdate">12.12.15</span>
                    </a>
                    <!--  FIXME/TODO change ID's/classes in  spans -->
                    <a id="jobber-rate-btn"
                       class="uk-button uk-button-primary feedback-btn jc-click-requestfeedback"><span>{{'demand_assessment' | translate}}</span>
                    </a>
                  </div>
                </div>
                <!-- #averagerating FLEX based layout-->
                <div class="uk-width-1-1">
                  <div class="uk-grid uk-grid-collapse uk-flex">
                    <div class="uk-flex uk-flex-top  uk-flex-item-auto">
                      <h4>{{'Company_assessment' | translate}}</h4>
                      <div class="uk-flex uk-flex-middle">
                        <div id="dprofileratingmanager" class="jc-template-replace-select-rating">
                          {{'None_so_far' | translate}}
                        </div>
                        <span class="rateit" id="dprofileratingmanagerrateit"
                              data-rateit-backingfld="#dprofileratingmanager"></span>
                        <span id="dprofileratingmanagercount"
                              class="jc-template-replace-count-rating"></span>
                      </div>
                    </div>
                    <div class="uk-flex uk-flex-top">
                      <h4>{{'Colleague_assessment' | translate}}</h4>
                      <div class="uk-flex uk-flex-middle">
                        <div id="dprofileratingcolleague" class="jc-template-replace-select-rating">
                          {{'None_so_far' | translate}}
                        </div>
                        <span class="rateit" id="dprofileratingcolleaguerateit"
                              data-rateit-backingfld="#dprofileratingcolleague"></span>
                        <span id="dprofileratingcolleaguecount"
                              class="jc-template-replace-count-rating"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>


            <div class="uk-width-1-1">
              <ul id="jobber-feedback-list" class="uk-nestable">
                <li class="uk-width-1-1 jc-template-replace-jobber-feedback">
                  <div class="uk-width-1-1">
                    <ul class="uk-nestable uk-padding-remove">
                      <li class="uk-nestable-item uk-nestable-nodrag uk-parent uk-collapsed">
                        <div class="uk-nestable-panel">
                          <div data-nestable-action="toggle" class="uk-nestable-toggle"></div>
                          <div class="panel-body uk-grid uk-grid-collapse uk-flex uk-flex-wrap-top">
                            <div class="uk-width-small-1-1 uk-width-medium-1-3">
                              <div class="uk-float-left uk-width-small-1-2 uk-width-medium-1-1">
                                <p class="uk-text-bold uk-text-left uk-margin-remove">{{'Date' | translate}}</p>
                              </div>
                              <div class="uk-float-left uk-width-small-1-2 uk-width-medium-1-1">
                                <p class="uk-text-left uk-margin-remove"><span
                                  class="jc-feedback-date">12.01.2016</span></p>
                              </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-3">
                              <div class="uk-float-left uk-width-small-1-2 uk-width-medium-1-1">
                                <p class="uk-text-bold uk-text-left uk-margin-remove">{{'Company' | translate}}</p>
                              </div>
                              <div class="uk-float-left uk-width-small-1-2 uk-width-medium-1-1">
                                <p class=" uk-text-left uk-margin-remove"><span
                                  class="jc-feedback-company"><!--Pizza Gruppe--></span>
                                </p>
                              </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-3">
                              <div class="uk-float-left  uk-width-small-1-2 uk-width-medium-1-1">
                                <p class="uk-text-bold uk-text-left uk-margin-remove">{{'Manager' | translate}}</p>
                              </div>
                              <div class="uk-float-left uk-width-small-1-2 uk-width-medium-1-1">
                                <p class="uk-text-left uk-margin-remove"><span
                                  class="jc-feedback-manager"><!--Peter Planer--></span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <ul class="uk-nestable-list">
                          <li>
                            <!-- <div class="uk-width-1-1 jc-template-replace-jobber-feedback-details"></div> -->
                            <div class="jc-template-feedback-details">
                              <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                                <div class="uk-width-1-1">
                                  <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-small-1-1 uk-width-large-1-2">
                                      <div class="uk-grid uk-grid-collapse uk-text-left">
                                        <div class="uk-width-small-1-2 uk-text-bold">{{'Assessment' | translate}}
                                        </div>
                                        <div class="uk-width-small-1-2">
                                          <p class="uk-width-1-1 uk-margin-remove"><span
                                            class="jc-feedback-average"><!--3.5--></span>
                                          </p>
                                        </div>

                                      </div>
                                    </div>
                                    <div class="uk-width-small-1-1 uk-width-large-1-2">
                                      <div class="uk-grid uk-grid-collapse uk-text-left">
                                        <div class="uk-width-small-1-2 uk-text-bold">{{'Assignment' | translate}}
                                        </div>
                                        <div class="uk-width-small-1-2">
                                          <p class="uk-width-1-1 uk-margin-remove"><span
                                            class="jc-feedback-applications">{{'Fully_satisfied' | translate}}</span>
                                          </p>
                                        </div>
                                        <div class="uk-width-small-1-2 uk-text-bold">{{'Team' | translate}}</div>
                                        <div class="uk-width-small-1-2">
                                          <p class="uk-width-1-1 uk-margin-remove"><span
                                            class="jc-feedback-team">{{'OK_alright' | translate}}</span>
                                          </p>
                                        </div>
                                        <div class="uk-width-small-1-2 uk-text-bold">{{'Expertise' | translate}}</div>
                                        <div class="uk-width-small-1-2">
                                          <p class="uk-width-1-1 uk-margin-remove"><span
                                            class="jc-feedback-expertise">{{'assessment_answer_Very_pleased' | translate}}</span>
                                          </p>
                                        </div>
                                        <div class="uk-width-small-1-2 uk-text-bold">{{'Appearance' | translate}}
                                        </div>
                                        <div class="uk-width-small-1-2">
                                          <p class="uk-width-1-1 uk-margin-remove"><span
                                            class="jc-feedback-appearance">{{'Could_be_better' | translate}}</span>
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </li>
        <!-- Contracts (Vertrage) block -->
        <li id="profile-subview-contracts" aria-hidden="true">
          <div class="uk-width-1-1">
            <h4>{{'Start_of_work_company' | translate}}</h4>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-width-1-1 uk-margin-top">
              <h4 class="uk-margin-remove">{{'Active_contracts' | translate}}</h4>
            </div>
          </div>
          <!-- Active Contracts List -->
          <div class="uk-width-1-1">
            <ul id="contracts-active-list" class="uk-nestable">
              <li
                class="uk-nestable-item uk-nestable-nodrag uk-parent uk-collapsed jc-template-jobber-contract-item"
                id="contract-1">
                <div class="uk-nestable-panel uk-clearfix">
                  <div data-nestable-action="toggle" class="uk-nestable-toggle"></div>
                  <div
                    class="panel-body uk-grid uk-grid-collapse uk-flex uk-flex-middle uk-width-1-1">
                    <div class="uk-width-4-10">
                      <div class="uk-padding-remove uk-grid-width-1-1 uk-text-left">
                        <div class="uk-text-bold">{{'Start_of_work' | translate}}</div>
                        <div><span class="jc-contracts-start-date">01.06.2016</span></div>
                      </div>
                    </div>
                    <div class="uk-width-6-10">
                      <div class="uk-padding-remove uk-grid-width-1-1 uk-text-left">
                        <div class="uk-text-bold">{{'Company' | translate}}</div>
                        <div><span class="jc-contracts-company">Pizza Gruppe</span></div>
                      </div>
                    </div>
                  </div>
                </div>
                <ul class="uk-nestable-list">
                  <li>
                    <div class="jc-template-replace-jobber-contract-detail">
                      <form class="uk-form uk-form-horizontal small">
                        <div class="uk-form-row">
                          <div
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10">
                            Unit
                          </div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-unitname"><!--Unit name--></span>
                        </div>
                        <div class="uk-form-row">
                          <div
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10">
                            Position
                          </div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-positionname"><!--Position name--></span>
                        </div>
                      </form>


                    </div>
                    <div class="jc-template-replace-staff-contract-form">
                      <form class="uk-form uk-form-horizontal small avail-contracts">
                        <div class="uk-form-row">
                          <label
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10"
                            for="jc-contracts-personnelnumber">{{'Personnel_number' | translate}}</label>
                          <input class="uk-width-small-1-1 uk-width-medium-6-10" type="text"
                                 id="jc-contracts-personnelnumber" maxlength="32"
                                 placeholder="Personnel number"
                                 required="">
                        </div>
                        <div class="uk-form-row">
                          <div class="uk-form-label">{{'Unit' | translate}}</div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-unit-name"><!-- Unit Name --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-jobtitle">
                            {{'Job_description' | translate}}
                          </label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-jobtitle"><!-- Service --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-start">
                            {{'Begin_of_contract' | translate}}
                          </label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-start"><!-- Contract start --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-period">
                            {{'Notice_period' | translate}}
                           </label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-period"><span><!--1--></span>Month</span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-earn">{{'Gross_salary' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-earn"><!--25.00 / {{i18n abbr_Hours_fullstop}}--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-expenses">{{'Expenses' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-expenses"><!--inkl.--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-bonus">{{'Year_end_salary' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-bonus"><!--inkl.--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-holidays">{{'Holiday_entitlement' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-holidays"><!--12--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-activity">{{'Level_of_employment' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-activity"><!--50%--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-month-hours">{{'Hours_per_month' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-month-hours"><!--176.5--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-comment">{{'Comment_plural_other' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-comment"><!--Hinweis: foo --></span>
                        </div>
                        <div id="upload-drop" class="uk-placeholder uk-text-center">
                          <i
                            class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right">
                            <!--icon--></i>
                          <span class="uk-form-file">choose file<input id="avail-contractsupload"
                                                                       type="file"></span>.
                        </div>
                        <div id="files-progressbar"
                             class="uk-progress uk-progress-striped uk-active uk-hidden">
                          <div class="uk-progress-bar" style="width: 0%;">...</div>
                        </div>
                        <div class="files-upload-box uk-margin-bottom">
                          <h2 class="uk-text-center"><span class="uk-hidden">{{'Documents' | translate}}</span><span>{{'Upload' | translate}}</span>
                          </h2>
                          <div class="uk-width-1-1">
                            <ul
                              class="uk-grid uk-grid-collapse uk-grid-width-small-1-3 uk-grid-width-large-1-6 uk-text-center uk-padding-remove">
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                            </ul>
                          </div>
                        </div>
                      </form>


                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-width-1-1 uk-margin-top">
              <h4 class="uk-margin-remove">{{'Inactive_contracts' | translate}}</h4>
              <p class="uk-margin-remove">{{'time_from_to' | translate}}</p>
            </div>
          </div>
          <!-- Inactive Contracts List -->
          <div class="uk-width-1-1">
            <ul id="contracts-inactive-list" class="uk-nestable">
              <li
                class="uk-nestable-item uk-nestable-nodrag uk-parent uk-collapsed jc-template-jobber-contract-item"
                id="contract-2">
                <div class="uk-nestable-panel uk-clearfix">
                  <div data-nestable-action="toggle" class="uk-nestable-toggle"></div>
                  <div
                    class="panel-body uk-grid uk-grid-collapse uk-flex uk-flex-middle uk-width-1-1">
                    <div class="uk-width-4-10">
                      <div class="uk-padding-remove uk-grid-width-1-1 uk-text-left">
                        <div class="uk-text-bold"> {{'Start_of_work' | translate}}</div>
                        <div><span class="jc-contracts-start-date">01.06.2016</span></div>
                      </div>
                    </div>
                    <div class="uk-width-6-10">
                      <div class="uk-padding-remove uk-grid-width-1-1 uk-text-left">
                        <div class="uk-text-bold">{{'Company' | translate}}</div>
                        <div><span class="jc-contracts-company">Pizza Gruppe</span></div>
                      </div>
                    </div>
                  </div>
                </div>
                <ul class="uk-nestable-list">
                  <li>
                    <div class="jc-template-replace-jobber-contract-detail">
                      <form class="uk-form uk-form-horizontal small">
                        <div class="uk-form-row">
                          <div
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10">
                            Unit
                          </div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-unitname"><!--Unit name--></span>
                        </div>
                        <div class="uk-form-row">
                          <div
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10">
                            Position
                          </div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-positionname"><!--Position name--></span>
                        </div>
                      </form>


                    </div>
                    <div class="jc-template-replace-staff-contract-form">
                      <form class="uk-form uk-form-horizontal small avail-contracts">
                        <div class="uk-form-row">
                          <label
                            class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-4-10"
                            for="jc-contracts-personnelnumber"> {{'Personnel_number' | translate}}</label>
                          <input class="uk-width-small-1-1 uk-width-medium-6-10" type="text"
                                 id="jc-contracts-personnelnumber" maxlength="32"
                                 placeholder="Personnel number"
                                 required="">
                        </div>
                        <div class="uk-form-row">
                          <div class="uk-form-label">{{'Unit' | translate}}</div>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-unit-name"><!-- Unit Name --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-jobtitle">{{'Job_description' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-jobtitle"><!-- Service --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-start">{{'Begin_of_contract' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-start"><!-- Contract start --></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-period">{{'Notice_period' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-period"><span><!--1--></span>Month</span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-earn">{{'Gross_salary' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-earn"><!--25.00 / {{i18n abbr_Hours_fullstop}}--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-expenses">{{'Expenses' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-expenses"><!--inkl.--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-bonus"> {{'Year_end_salary' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-bonus"><!--inkl.--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-holidays"> {{'Holiday_entitlement' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-holidays"><!--12--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-activity"> {{'Level_of_employment' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-activity"><!--50%--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-month-hours">{{'Hours_per_month' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-month-hours"><!--176.5--></span>
                        </div>
                        <div class="uk-form-row">
                          <label class="uk-form-label" for="jc-contracts-comment">{{'Comment_plural_other' | translate}}</label>
                          <span
                            class="form-value uk-width-small-1-1 uk-width-medium-6-10 jc-contracts-comment"><!--Hinweis: foo --></span>
                        </div>
                        <div id="upload-drop" class="uk-placeholder uk-text-center">
                          <i
                            class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right">
                            <!--icon--></i>
                          <span class="uk-form-file">choose file<input id="avail-contractsupload"
                                                                       type="file"></span>.
                        </div>
                        <div id="files-progressbar"
                             class="uk-progress uk-progress-striped uk-active uk-hidden">
                          <div class="uk-progress-bar" style="width: 0%;">...</div>
                        </div>
                        <div class="files-upload-box uk-margin-bottom">
                          <h2 class="uk-text-center"><span class="uk-hidden"> {{'Documents' | translate}}</span><span>{{'Upload' | translate}}</span>
                          </h2>
                          <div class="uk-width-1-1">
                            <ul
                              class="uk-grid uk-grid-collapse uk-grid-width-small-1-3 uk-grid-width-large-1-6 uk-text-center uk-padding-remove">
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                              <li><a href="TODO"><i
                                class="uk-icon-file-text-o uk-icon-large"></i></a></li>
                            </ul>
                          </div>
                        </div>
                      </form>


                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- MAIN uk-grid -->
        </li>
      </ul>
    </div>
    <!-- column -->
  </div>
  <!-- grid -->
</div> 
`;

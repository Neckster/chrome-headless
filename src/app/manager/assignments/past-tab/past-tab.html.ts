export const pastHtml = `   
<!--<div *ngIf="!assigns.length"-->
       <!--class="uk-width-1-1 uk-container-center uk-margin-small  padded">-->
   <!--<p class="text-center uk-margin-remove">{{'No_past_assignments_this_month' | translate}}</p>-->
<!--</div> -->

<div>
<div [formGroup]="assigmentControllers" class="uk-width-1-1 uk-form uk-margin-large-top" >

      <div class="uk-width-1-1 assigmentsFilters past-selector"  [formGroup]="assigmentControllers">
          <div class="uk-width-2-10 uk-float-left">
            <select formControlName="monthControl">
              <option *ngFor="let month of monthList" value="{{month}}">{{month | translate}}</option>
            </select>
          </div>
          <div class="uk-width-1-10 uk-float-left">
            <select class="assignment-year" formControlName="yearControl">
              <option *ngFor="let year of yearsList" value="{{year}}">{{year}}</option>
            </select>
        </div>
      </div>
      
  <ul class="uk-nestable uk-width-1-1 pastTabMain">
    <li *ngFor="let ass of assigns | paginate: { id: 'server', itemsPerPage: 20, currentPage: selectedPage }; let i = index" 
    class="hadci changedci managertime hadco changedco time-current checkin-early checkout-early  assignment uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item uk-collapsed">
        <div class="uk-nestable-panel uk-clearfix assignment-head" [style.background]="ass.open ? '#eee':'#fff' " (click)="toggleCollapsible(ass,  i + ((selectedPage - 1) * 20))" style='cursor:pointer; '>
         <div class="icon large  blue uk-margin-right uk-margin-small-left" [ngClass]='{"arrow-down": ass.open, "arrow-right":!ass.open}'></div>
          <div class="uk-grid uk-grid-collapse uk-width-1-1 uk-float-right panel-body uk-flex uk-flex-middle">
            <div class="uk-flex uk-flex-wrap uk-width-1-1">
              <div class="uk-width-medium-2-10 uk-width-small-2-3 jobdate">
                <span> {{ ass.tFrom | timestamp:this.lang }} </span>
                <br>
                <span>{{ass.from}}</span>–<span>{{ass.to}}</span>
              </div>
              <div class="uk-width-medium-2-10 uk-width-small-1-3 
              uk-flex-order-last-medium 
              jobloc job-lst">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-1-1">
                    <div class="uk-float-right">
                     <span *ngIf="ass.stat && !ass.stat.declined && ass.hours.actualU" >
                        <span>{{'Actual_colon' | translate}}</span>
                        <span>{{ass.hours.actual}}</span>
                        <span>{{ 'abbr_Hours'| translate}} </span>
                      <!--<span class="isign">ⓘ</span>-->
                      </span>
                      <span *ngIf="ass.stat && ass.stat.declined">{{'Refused' | translate}}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="uk-width-medium-6-10 uk-width-small-1-1 jobloc">
                <div class="uk-clearfix">
                  <div class="uk-float-left">
                    <span>{{ass.cname}}</span>
                    <br>
                    <span>{{ass.nname}}</span>&nbsp;–<span>{{ass.pname}}</span>
                  </div>
                  <div *ngIf="(ass.info || ass.chef) && !ass.open" class="uk-float-right uk-margin-remove">
                    <span class="isign" style="display:inline-block;margin-top:10px;">ⓘ</span>
                  </div>
                </div>
              </div>
            </div>
            <!-- uk-flex-wrap -->
          </div>
          <!-- uk-grid  -->
        </div>
        
        <ul *ngIf="ass.open" style='box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 3px 0 rgba(0, 0, 0, 0.12);'>
          <li style=' list-style: none '>
            <div class="uk-panel">
              <div *ngIf="ass.info || ass.chef" class="uk-width-1-1 jobdetail" class="uk-margin-top">
                <div class="uk-width-1-1">
                  <div *ngIf="ass.chef === 't'" class="jobinfo_head jc-have-info"> 
                    <span>{{ 'Note_colon' | translate }}</span> 
                    <p class="uk-margin-top-remove">{{ 'Shift_Leader_Statement' | translate }}</p>
                  </div>
                  <div *ngIf="ass.info?.compiled || ass.info?.message" class="jobinfo">
                    <span>{{'Message' | translate}}:</span>
                    <div class="info-wrapper" [innerHTML]="ass.info?.compiled || ass.info?.message"></div>
                  </div>
                </div>
              </div>
            
              <div *ngIf="!ass.stat?.declined">
               <div class="uk-width-1-1 uk-margin-top">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3 uk-margin-bottom">
                        <span>{{'Planned_assignment' | translate}}</span>
                        <span><span>{{ass.from}}</span>–<span>{{ass.to}}</span></span>
                      </div>
                      <div class="uk-width-1-10 uk-hidden-small"></div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-4-10"></div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{'Checked_in_colon' | translate}}</div>
                          <div class="uk-width-4-10 uk-text-center">
                            <span class="span-checkin">{{ ass.tciuser ? getTime(ass.tciuser) : '-' }}
                            </span>
                          </div>
                         <span *ngIf="compareCheckIn(ass)" class="icon checkmark circle"></span>
                          <div class="uk-width-2-10">
                            <span class="span-syscheckin">{{ass.citime ? '(' + getTime(ass.citime) + ')' : ass.checkIn ? '-' : '-'}}</span>
                          </div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-4-10 uk-text-left">
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3 top-block">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{'Correction_colon' | translate}}</div>
                          <div class="uk-width-4-10 uk-text-center">
                            {{ass.latestChanges?.tcimanager ? ass.latestChanges.tcimanager : '-'}}
                          </div>
                          <div class="uk-width-2-10 corretionInfo">
                            <!--<span class="isign">ⓘ</span>-->
                            {{ass.latestChanges?.timeChanged ? '! '+  ass.latestChanges.timeChanged : ''}}
                              {{ass.latestChanges?.mName ? '|' + ass.latestChanges.mName : ''}}
                            </div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-3-10 jobaction uk-clearfix uk-text-left">
                    <div class="uk-grid uk-grid-collapse hidden">
                      <!-- TODO: we do not have this info in job yet -->
                      <p><span>Manager</span>:&nbsp;
                        <span class="jc-uname"></span>&nbsp;(<span><!--05.06.2015--></span>)</p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- uk-width-1-1 -->
              <div class="uk-width-1-1 uk-margin">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{ 'Break' | translate }}</div>
                          <div class="uk-width-4-10 uk-text-center"><span>{{ass.idleuser ? ass.idleuser : '-' }}</span></div>
                          <div class="uk-width-2-10"></div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-width-large-4-10 jobaction uk-clearfix uk-text-left">
                    <!--  empty accordingly to designs -->
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3 top-block">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{'Correction_colon' | translate}}</div>
                          <div class="uk-width-4-10 uk-text-center">
                            {{ass.latestChanges?.idle ? ass.latestChanges.idle : '-'}}
                          </div>
                          <div class="uk-width-2-10 corretionInfo">
                            <!--<span class="isign">ⓘ</span>-->
                              {{ass.latestChanges?.timeChanged ? '! '+  ass.latestChanges.timeChanged : ''}}
                              {{ass.latestChanges?.mName ? '|' + ass.latestChanges.mName : ''}}
                          </div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-width-large-4-10 jobaction uk-clearfix uk-text-left">
                    <div class="uk-grid uk-grid-collapse">
                      <!--  empty accordingly to designs -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- uk-width-1-1 -->
              <div class="uk-width-1-1 uk-margin">
                <div class="uk-grid uk-grid-collapse">
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uk-width-small-1-1 uk-width-1-1 uk-width-large-2-3">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{'Checked_out' | translate}}</div>
                          <div class="uk-width-4-10 uk-text-center">
                            <span *ngIf="ass.checkOut">
                              {{ !ass.cotime ? '-' : ass.tcouser ? getTime(ass.tcouser) : '' }}
                            </span>
                           <span *ngIf="compareCheckOut(ass)" class="icon checkmark circle"></span>
                            <span *ngIf="!ass.checkOut">-</span>
                          </div>
                          <div class="uk-width-2-10"><span>{{calcAutoC(ass)}}</span>
                          </div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-width-large-3-10 jobaction uk-clearfix uk-text-left checkout-early checkout-late">
                    <!--<p>-->
                      <!--<span>&lt;!&ndash;00:15&ndash;&gt;</span>-->
                      <!--<span class="checkout-early">checked-out too early</span>-->
                      <!--<span class="checkout-late">checked-out too late</span>-->
                    <!--</p>-->
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-5-10 uk-width-large-6-10">
                    <div class="uk-grid uk-grid-collapse">
                      <div class="uk-width-1-3 uk-hidden-small"></div>
                      <div class="uuk-width-small-1-1 uk-width-1-1 uk-width-large-2-3 top-block">
                        <div class="uk-grid uk-grid-collapse">
                          <div class="uk-width-4-10">{{'Correction_colon' | translate}}</div>
                          <div class="uk-width-4-10 uk-text-center">
                            {{ass.latestChanges?.tcomanager ? ass.latestChanges.tcomanager : '-'}}
                          </div>
                          <div class="uk-width-2-10 corretionInfo"><span class="isign uk-hidden">ⓘ</span>
                            {{ass.latestChanges?.timeChanged ? '! '+ ass.latestChanges.timeChanged : ''}}
                              {{ass.latestChanges?.mName ? '|' + ass.latestChanges.mName : ''}}
                          </div>
                        </div>
                        <div class="uk-width-1-10 uk-hidden-small"></div>
                      </div>
                    </div>
                  </div>
                  <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-width-large-3-10 jobaction uk-clearfix uk-text-left">
                    <div class="uk-grid uk-grid-collapse">
                      <!--  empty accordingly to designs -->
                    </div>
                  </div>
                </div>
              </div>
             
            </div>
            
              
              <!-- uk-width-1-1 -->
              <div class="uk-width-1-1">
                <div class="hidden">
                  <div class="jobcolleaguehead">
                    <span class="jobcolleaguetext">Staff</span>
                  </div>
                </div>
                <ul class="uk-nestable-list uk-width-1-1 uk-padding-remove"></ul>
              </div>
              <!-- uk-width-1-1 -->
              
             <div class="uk-width-1-1 uk-text-left uk-margin-top uk-margin-bottom">
                        <div class="uk-form-label">{{'Staff' | translate}}</div>
                        <ul *ngIf="ass.staff && ass.staff.length"
                          class="uk-list uk-width-9-10">
                          <li class="uk-nestable-item uk-nestable-nodrag uk-parent jobcolleaguepos section">
                            <div *ngIf="ass.staff && ass.staff.length"
                                 class="uk-nestable-panel uk-clearfix"  [style.background]="ass.openIn ? '#fafafa':'#fff' " style='box-shadow:none!important; cursor:pointer' (click)="toggleStaffItem(ass, i + ((selectedPage - 1) * 20))">
                              <div class="icon large arrow-down blue uk-margin-right uk-margin-small-left" *ngIf="ass.openIn"></div>
                              <div class="icon large arrow-right blue uk-margin-right uk-margin-small-left" *ngIf="!ass.openIn"></div>
                              <div class="uk-grid uk-grid-collapse">
                                <div>{{ ass.pname }}</div>
                              </div>
                            </div>
                            <ul  *ngIf="ass.openIn && ass.staff && ass.staff.length"
                                class="uk-list" style='padding-left:30px'>
                              <li *ngFor="let jobber of ass.staff"
                                  class="uk-item uk-nestable-nodrag uid-131988">
                                <assess-modal *ngIf="jobber.open" (closeEmitter)="jobber.open = false" [jobber]='jobber' [unitName]='jobber.uname'></assess-modal>
                                <div class="uk-panel">
                                  <div class="jobstaffposname uk-clearfix">
                                    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                                      <div class="uk-width-7-10">
                                        <span>{{ jobber.uname }}</span>&nbsp;&nbsp;
                                        <span>{{ jobber.from }}</span>–<span>{{ jobber.to }}</span>
                                      </div>
                                      <div class="uk-width-3-10">
                                        <button class="uk-button-link app-modal-userrate assessButton" 
                                                [style.color]=" jobber.rating ? 'black' : '#07a0e1'"
                                                (click)="jobber.open = true;" type='button'>
                                             {{'assess' | translate}}
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
            </div>
          </li>
        </ul>
      </li>
  </ul>
  </div>
  </div>
  <template [ngIf]="assigns.length">
    <pagination-controls class="uk-container-center uk-pagination" (pageChange)="pageChange($event)" id="server" autoHide="true" directionLinks="false"></pagination-controls>
  </template>

`;

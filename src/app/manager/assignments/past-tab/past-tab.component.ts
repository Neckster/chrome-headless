import { Component }                from '@angular/core';
import { FormGroup, FormControl }   from '@angular/forms';
import { select }                   from 'ng2-redux/lib/index';
import { range }                    from 'ramda';
import {
  PaginationControlsCmp,
  PaginatePipe,
  PaginationService }               from 'ng2-pagination/index';
import moment                       = require('moment/moment');
import { TargetJobService }         from '../../../services/job.service';
import { JobService }               from '../../../services/shared/JobService';
import { InfoService }              from '../../../services/info.service';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { Cache }                    from '../../../services/shared/cache.service';
import * as _                       from 'lodash';
import { pastHtml }                 from './past-tab.html.ts';

import { TimestampPipe } from '../../../lib/pipes/timestamp';

import { AppStoreAction }           from '../../../store/action';

import { AssessModalComponent }     from '../assess-modal/assess-modal';

import {
  is, or, not, and, prop as get,
  present, pipe,
  map, filter, sort }               from '../../../lib/curried';

import * as R from 'ramda';


// const pipe = (...fns) => (v) => fns.reduce((a, c) => c(a) , v);
// const map = (fn) => (list) => list.map(fn);
// const filter = (fn) => (list) => list.filter(fn);
// const sort = (fn) => (list) => list.sort(fn);

@Component({
  selector: 'past-ass',
  providers: [ PaginationService ],
  directives: [ AssessModalComponent, PaginationControlsCmp ],
  pipes: [ PaginatePipe, TimestampPipe ],
  styles: [`
    .jobinfo_head { clear: right; }
    .refuse-btn { margin: 2px 15px; }
    .past-selector { display: inline-block;}
    .assignment-year { padding: 1rem; }
    .corretionInfo { 
      width: 100%;
      color: red;
      text-align: right;
    }
  `],
  template: pastHtml
})

export class PastAssignmentsComponent {

  @select() currentUnit$;
  @select(state => state.localisation) lang$: any;

  public info: any;
  public currentUnit;
  public assessAss: any;
  public openAssess = false;
  public assigns = [];
  public unsubscribe = [];
  public lang;
  public hideScrollBar = this.jobService.hideScrollBar;

  private selectedPage = 1;
  private defaultMonth = moment().format('MMMM');
  private defaultYear = moment().format('YYYY');
  private thisYear: number = moment().add(1, 'year').year();
  private lastYear: number = moment().subtract(10, 'year').year();
  private yearsList = _.rangeRight(this.lastYear, this.thisYear);
  private monthCounter = _.rangeRight(0, 12);
  private monthList = this.monthCounter.map(m => {
    return moment().month(m).format('MMMM');
  });
  private assignsNew: any;
  private assigmentControllers = new FormGroup({
    monthControl: new FormControl(this.defaultMonth),
    yearControl: new FormControl(this.defaultYear)
  });
  constructor(public jobService: JobService,
              public infoService: InfoService,
              public targetjobService: TargetJobService,
              public cache: Cache,
              public action: AppStoreAction) {

    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
    }));
  }

  ngOnInit() {
    this.init();
    this.selectedPage = this.cache.getObject('assignments:past:page') || 1;
    this.assigmentControllers.valueChanges.subscribe(el => {
      this.submit(el.yearControl, el.monthControl);
    });
    // this.submit();
    // console.log(this);
  }


  submit(year?, month?) {
    year = year || this.assigmentControllers.value.yearControl;
    month = month || this.assigmentControllers.value.monthControl;
    this.targetjobService
        .getPastAssignments(+year, moment(month, 'MMMM').month())
        .subscribe(info => {
          info.map((e) => {
            return _.merge(e, e.jinfo);
          });
          // let MergeAssign = _.merge(info, this.assigns);
          // this.assigns = MergeAssign;
          // console.log(this.assigns[0]);
          // this.assigns = info;
          this.assigns = _.merge(this.assigns, info);
          // console.log(this.assigns[0]);
        });
  }



  init() {

    const weekDepth = 7;
    let week = +ParseOrganisationService.calculateWeek() - weekDepth;

    // console.log(week);

    this.infoService.get(`${week}`)
      .subscribe((res: any) => {

        let test: Response = Object.assign({}, res);

        let calcedAss = this.jobService.calcAssignments(Array.isArray(test.assignment) ?
          test.assignment : [], test.meta.time, true);

        const userId = test.webuser.webuser_id;

        const statusPastAndNotCanceled = [
          (a) => is(userId)(get('webuser_id')(a)),
          (a) => is('past')(get('status')(a)),
          (a) => get('published')(a),
          (a) => not(is({ deleted: 1, accepted: 1 })(get('stat')(a))),
          (a) => get('stat')(a) ? not(is(1)(get('deleted')(get('stat')(a)))) : true
        ];

        const statusPastOtherValidStatus =
          statusPastAndNotCanceled.concat(
            (a) => get('stat')(a) && present(['cancel', 'declined'])(get('stat')(a))
          );

        /*
         * end of rules
         */

        // Main 'past' pipe transforming function
        // filtering, mapping and sorting

        const prepareAssignments = pipe(
          // filtering
          filter(or([
            (a) => and(statusPastAndNotCanceled)(a),
            (a) => and(statusPastOtherValidStatus)(a)
          ])),

          // mapping
          map(el => Object.assign(
            {},
            el,
            this.jobService.calcHours(el, test.meta.time),
            this.calculdateDate(el.tFrom),
            { staff: this.jobService.getStaff(el, res) }
          )),

          map(el => Object.assign(
            {},
            el,
            { open: this.cache.isInArray(
              'assignments:past:po',
              `${el.company_id}-${el.position_id}-${el.tFrom}`
            ) },
            { openIn: this.cache.isInArray(
              'assignments:past:po_st',
              `${el.company_id}-${el.position_id}-${el.tFrom}`
            ) }
          )),

          // sorting
          sort((a, b) => (this.compare(a, b) ? -1 : (this.compare(b, a) ? 1 : 0)))
        );

        this.assigns = prepareAssignments(calcedAss);
        this.assigns = this.setManagerChanged(this.assigns);
       /* if (this.assigns && this.assigns.length) {
          this.assigns.forEach(el => {
            if (el.latestChanges) console.log(el.latestChanges);
          });
        }*/
      });
  }


  calculdateDate(time) {
    const jobTiming = moment(time);
    const timings = {
      dayoffweek: jobTiming.format('ddd'),
      dmDate: jobTiming.format('D.MM')
    };
    return timings;
    /*
    let data = moment(job.tFrom).lang(this.lang).format('ddd:D.MM').split(':');
    return Object.assign({}, job, {dayoffweek: data[0], dmDate: data[1]});
    */
  }

  compare(a, b) {
    let result = false;
    if (a.week > b.week) {
      return true;
    } else if (a.week === b.week) {
      if (a.weekday > b.weekday) {
        return true;
      } else if (a.weekday === b.weekday) {
        if (a.shift_idx > b.shift_idx) {
          return true;
        } else if (a.shift_idx === b.shift_idx) {
          return a.tFrom < b.tFrom;
        }
      }
    }
    return result;
  }

  getTime(t) {
      return  t ? moment(t).lang(this.lang).format('HH:mm') : '-';
  }

  compareCheckIn(ass) {
    let ci = this.getTime(ass.citime);
    let tci = this.getTime(ass.tciuser);
    return (ci === '-' && tci === '-') ? false : tci === ci;
  }

  compareCheckOut(ass) {
    let co = this.getTime(ass.cotime);
    let tco = this.getTime(ass.tcouser);
    return (co === '-' && tco === '-') ? false : co === tco;
  }

  calcAutoC(job) {
    if (job.autoCheckOut)
      return '(auto)';
    if (job.checkOut && job.cotime)
      return `(${this.getTime(job.cotime)})`;
    return '-';
  }

  openAssessM(ass) {
    this.assessAss = ass;
    this.openAssess = true;
  }

  toggleCollapsible(ass, index) {
    if (this.assigns[index].open) {
      this.cache.removeFromArray(
        'assignments:past:po',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:past:po',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    }
    this.assigns[index].open = !this.assigns[index].open;
  }

  toggleStaffItem(ass, index) {
    if (this.assigns[index].openIn) {
      this.cache.removeFromArray(
        'assignments:past:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:past:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    }
    this.assigns[index].openIn = !this.assigns[index].openIn;
  }

  pageChange($event) {
    this.selectedPage = $event;
    this.cache.set('assignments:past:page', $event);
  }

  setManagerChanged(assigns) {
    if (assigns && assigns.length) {
      return assigns.map(el => {
        if (!el.log) return el;

        let stat = el.log[el.log.length - 1][0],
            last = el.log[el.log.length - 1];
        if (stat !== 'checkedin' &&
            stat !== 'checkedout' &&
            stat !== 'declined' &&
          typeof last[5] === 'string') {
            let latestChanges = {
                status: last[0] ? last[0] : '',
                timeChanged: last[3] ? moment(last[3])
                    .format('DD.MM.YYYY') : '',
                mId: last[4] ? last[4] : '',
                mName: last[5] ? last[5] : ''
            };
          latestChanges = Object.assign({}, latestChanges,
                          this.addChangedTime(last[1]));
            return Object.assign({},
              el, {latestChanges : latestChanges});
        }
        return el;
      });
    }
  }

  addChangedTime (obj) {
    let res;
    if (!obj || typeof obj !== 'object') {
      res =  { info: obj ? obj : ''};
    }else {
      res = {
        tcimanager: obj.tcimanager ?
            this.getTime(obj.tcimanager) : '',
        tcomanager: obj.tcomanager ?
            this.getTime(obj.tcomanager) : '',
        idle : obj.idle ? obj.idle : ''
      };
    }
    return res;
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe() );
  }

}

interface Response {
  assignment: any;
  webuser: { webuser_id: any};
  meta: { time: any };
}

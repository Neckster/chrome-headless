import { Component, Input } from '@angular/core';

import moment = require('moment/moment');
import { select } from 'ng2-redux/lib/index';
import { Maybe } from 'monet';

import { JobService } from '../../../services/shared/JobService';
import { TargetJobService } from '../../../services/job.service';
import { Cache } from '../../../services/shared/cache.service';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';

import { AppStoreAction } from '../../../store/action';

import * as c from '../../../lib/curried';
import { TimestampPipe } from '../../../lib/pipes/timestamp';

import { tentativeHtml } from './tentative.html';

import { RefuseModalComponent } from '../refuse-modal/refuse-modal';
import { AssessModalComponent } from '../assess-modal/assess-modal';

@Component({
  selector: 'tentative',
  directives: [ RefuseModalComponent, AssessModalComponent ],
  pipes: [ TimestampPipe ],
  styles: [`
    .jobinfo_head { clear: right; }
    .refuse-btn { margin: 2px 15px; }
  `],
  template: tentativeHtml
})

export class TentativeComponent {

  @select() info$;
  @select() currentUnit$;
  public refuseAss: any;
  @select(state => state.localisation) lang$: any;
  public assessAss: any;
  public info: any;
  public currentUnit;
  public tentative;
  public openAssess = false;
  public openRefuse = false;
  public unsubscribe = [];
  public lang;
  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public jobService: JobService,
              public targetJob: TargetJobService,
              public cache: Cache,
              public action: AppStoreAction) {

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      if (!info.manager) {
        this.init();
      }
    }));

    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
    }));

    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname)
        this.currentUnit = unit;
      this.init();
    }));

  }

  init() {
    Maybe.fromNull(this.info)
      .filter(info => c.and([
        (i) => c.prop('assignment')(i),
        (i) => c.greaterEq(c.prop('length')(c.prop('assignment')(i)))(1)
      ])(info))
      .bind(info => {

        const userId = this.info.webuser.webuser_id;
        const isoWeekday = moment(this.info.meta.time, ['x']).isoWeekday();

        // processTentative (...fns) -> (assignments)
        const processTentative = c.pipe(
          c.filter(c.and([
            (a) => c.is(userId)(c.prop('webuser_id')(a)),
            (a) => c.not(c.prop('published')(a))
          ])),
          c.map(a => Object.assign(
            {},
            a,
            { tFrom: this.jobService.getRealTime(this.info.meta.time, a.from,
              ('' + a.week).slice(-2), a.weekday, parseInt('20' + ('' + a.week).substr(0, 2), 10))
            },
            { staff:  this.jobService.getStaff(a, this.info) }
          )),
          c.map(a => Object.assign(
            {},
            a,
            { open: this.cache.isInArray(
                'assignments:tentative:po',
                `${a.company_id}-${a.position_id}-${a.tFrom}`
            ) },
            {
              openIn: this.cache.isInArray(
                'assignments:tentative:po_st',
                `${a.company_id}-${a.position_id}-${a.tFrom}`
              )
            }
          )),
          c.sort((a, b) => ((a.tFrom > b.tFrom) ? 1 : (b.tFrom > a.tFrom) ? -1 : 0))
        );

        return this.tentative = processTentative(this.info.assignment);
      });
  }

  getD(ass) {
    return moment(ass.tFrom, ['x']).lang(this.lang).format('ddd:D.MM').split(':');
  }

  toggleCollapsible(a, index) {
    if (this.tentative[index].open) {
      this.cache.removeFromArray(
        'assignments:tentative:po',
        `${a.company_id}-${a.position_id}-${a.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:tentative:po',
        `${a.company_id}-${a.position_id}-${a.tFrom}`
      );
    }
    this.tentative[index].open = !this.tentative[index].open;
  }

  toggleStaffItem(ass, index) {
    if (this.tentative[index].openIn) {
      this.cache.removeFromArray(
        'assignments:tentative:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:tentative:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    }
    this.tentative[index].openIn = !this.tentative[index].openIn;
  }

  openRefuseM(assignment) {
    this.refuseAss = assignment;
    this.openRefuse = true;
  }

  openAssessM(assignment) {
    this.assessAss = assignment;
    this.openAssess = true;
  }

  avaible(assignment) {
    let week = ParseOrganisationService.calculateWeek();
    this.targetJob.avaible(1, assignment, assignment.week).subscribe(res => {
      this.action.fetchGetInfo(week);
    });
  }

  getCheckInTime( time ) {
    // TODO: fix this, upcoming.component same
    if (!time) {
      return '-:-';
    }

    let t = time.split(':'),
      hours = +t[0],
      min = +t[1];
    if ( min < 15 ) {
      min = ( min - 15 ) + 60;
      hours--;
    } else {
      min -= 15;
    }
    return hours + ':' + min;
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe());
  }

}

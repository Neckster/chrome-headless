export const assignmentsHtml = `
<div class="page assignment jobberview uk-clearfix">

  <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    
    <checkin-modal 
      *ngIf="currentAssignments?.length" 
      (closeEmitter)="openModal = false; onModalClose()"
      [param]="currentAssign" 
      [open]="openModal">
    </checkin-modal>
    
    <checkout-modal 
      *ngIf="currentAssignments?.length" 
      (closeEmitter)="checkOutModal = false; onModalClose()"
      [param]="currentAssign" 
      [open]="checkOutModal">
    </checkout-modal>
    
    <refuse-modal 
      *ngIf="currentAssignments?.length" 
      (closeEmitter)="refuseModal = false; onModalClose()"
      [param]="currentAssign" 
      [open]="refuseModal">
    </refuse-modal>

    <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-9-10 uk-width-xlarge-7-10 uk-container-center uk-grid-collapse">
      <h1>{{'Assignment_plural_other' | translate}}</h1>
      <div class="uk-width-1-1">
        <ul id="assignment-subnav" class="uk-tab uk-tab-responsive hideThisTab" data-uk-tab>
          <li id="assignment-subnav-current"
              aria-expanded="true"
              [ngClass]="{ 'uk-active': checkTabStatus('current') }">
            <a (click)="changeTab('current')" href="">
              {{'current' | translate}}
            </a>
          </li>
          <li id="assignment-subnav-future" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('future') }">
            <a (click)="changeTab('future')" href="" class="current">
              {{'upcoming' | translate}}
            </a>
          </li>
          <li id="assignment-subnav-unpublished" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('tentative') }">
            <a (click)="changeTab('tentative')" href="" class="current">
            {{'tentative_plural_other' | translate}}
            </a>
          </li>
          <li id="assignment-subnav-past" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('past') }">
            <a (click)="changeTab('past')" href="" class="current">
              {{'past' | translate}}
            </a>
          </li>
          <!-- TODO li id="assignment-subnav-payout"><a href="#jobber-assignment">{{i18n Payout}}</a></li-->

        </ul>
        <nav class="uk-navbar showThisTab" style='background:white;box-shadow:none;'>
            <ul class="uk-navbar-nav">
                <li class="uk-parent " data-uk-dropdown>
                    <a class='mainTab'>{{ checkTitleTab()}}</a>
                    <div class="uk-dropdown ">
                        <ul class="uk-nav uk-nav-navbar">
                             <li id="assignment-subnav-current"
                                  aria-expanded="true"
                                  [ngClass]="{ 'uk-active': checkTabStatus('current') }">
                                <a (click)="changeTab('current')" class="uk-dropdown-close">
                                  {{'current' | translate}}
                                </a>
                              </li>
                              <li id="assignment-subnav-future" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('future') }">
                                <a (click)="changeTab('future')"  class="current uk-dropdown-close">
                                  {{'upcoming' | translate}}
                                </a>
                              </li>
                              <li id="assignment-subnav-unpublished" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('tentative') }">
                                <a (click)="changeTab('tentative')"  class="current uk-dropdown-close">
                                {{'tentative_plural_other' | translate}}
                                </a>
                              </li>
                              <li id="assignment-subnav-past" aria-expanded="false" [ngClass]="{ 'uk-active': checkTabStatus('past') }">
                                <a (click)="changeTab('past')" class="curren uk-dropdown-close">
                                  {{'past' | translate}}
                                </a>
                              </li>

                        </ul>
                    </div>

                </li>
            </ul>
        </nav>

        <ul id="assignment-subtab" class="uk-switcher uk-margin">
          <!-- assignments today -->
          <li *ngIf="tabs[0].active" id="jobber-assignment-today" class="uk-active">
            <current
              [assigns]="currentAssignments" 
              (out)="onCurrentEvent($event)">
            </current>  
          </li>
           
          <!-- future assignments -->
          <li *ngIf="tabs[1].active" id="jobber-assignment-future" aria-hidden class="uk-active">
            <upcoming></upcoming>
          </li>
          
          <!-- unpublished assignments -->
          <li *ngIf="tabs[2].active" id="jobber-assignment-unpublished" aria-hidden class="uk-active">
            <p>{{'info_tentative_assignments' | translate}}</p>
            <tentative></tentative>
          </li>
          
          <!-- past assignmnets -->
          <li *ngIf="tabs[3].active" aria-hidden class="uk-active">
            <past-ass></past-ass>
          </li>
          
          <!-- payoff -->
          <li aria-hidden="true" >
            <div class="uk-width-1-1 uk-grid uk-grid-collapse">
              <div class="uk-width-1-1 uk-text-left uk-text-bold uk-margin">
                <div class="uk-grid uk-grid-small">
                  <div class="uk-width-small-4-10 uk-width-large-1-2">
                    <h5>{{'Company' | translate}}</h5>
                    <button type="button"
                            class="organisation uk-button uk-button-large uk-margin-remove uk-text-truncate">
                      <span class="nounit hidden">{{'Choose_Unit' | translate}}</span>
                      <span
                        class="jc-nname nname uk-text-truncate">restaurant "little shangri-la"</span>
                    </button>
                  </div>
                  <div class="uk-width-small-3-10 uk-width-large-1-4">
                    <h5>{{'date_Year' | translate}}</h5>
                    <form class="uk-form">
                      <input class="uk-width-1-1" type="text" placeholder="{{'placeholder_date_Year' | translate}}"
                             data-uk-datepicker="{format:'YYYY'}"></form>


                  </div>
                  <div class="uk-width-small-3-10 uk-width-large-1-4">
                    <h5>{{'date_Month' | translate}}</h5>
                    <form class="uk-form">
                      <input class="uk-width-1-1" type="text" placeholder="{{'placeholder_date_Month' | translate}}"
                             data-uk-datepicker="{format:'MM'}"></form>


                  </div>
                </div>
              </div>
              <!-- summary -->
              <div class="uk-width-1-1 uk-grid uk-grid-collapse uk-flex uk-flex-middle panel-body">
                <div class="uk-width-1-1 uk-text-primary">
                  <div class="uk-grid uk-grid-collapse">
                    <div class="uk-width-1-1 uk-text-left">
                      <div class="uk-grid uk-grid-collapse">
                        <div class="uk-width-small-3-10 uk-width-medium-4-10">{{'Total' | translate}}</div>
                        <div class="uk-width-small-7-10 uk-width-medium-6-10 uk-text-left">
                          <div class="uk-grid uk-grid-collapse">
                            <div class="uk-width-9-10 uk-text-center">
                              <div class="uk-grid uk-grid-collapse">
                                <div class="uk-width-3-10">{{'Actual' | translate}}</div>
                                <div class="uk-width-4-10">{{'Correction' | translate}}</div>
                                <div class="uk-width-3-10">{{'Net' | translate}}</div>
                              </div>
                            </div>
                            <div class="uk-width-1-10 uk-text-center"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="uk-width-1-1">
                  <div class="uk-grid uk-grid-collapse">
                    <div class="uk-width-1-1 uk-text-left">
                      <div class="uk-grid uk-grid-collapse">
                        <div
                          class="uk-width-small-3-10 uk-width-medium-4-10 uk-width-medium-4-10 uk-text-bold">
                          <span class="jobdate_month"><!--September--></span>
                          <span class="jobdate_year"><!--2016--></span>
                        </div>
                        <div class="uk-width-small-7-10 uk-width-medium-6-10 uk-text-left">
                          <div class="uk-grid uk-grid-collapse">
                            <div class="uk-width-9-10 uk-text-center uk-text-bold">
                              <div class="uk-grid uk-grid-collapse">
                                <div class="uk-width-3-10">
                                  <span class="jc-time-total"><!--18:40--></span>h
                                </div>
                                <div class="uk-width-4-10">
                                  <span class="jc-diff-total"><!--00:25--></span>h
                                </div>
                                <div class="uk-width-3-10">
                                  <span class="jc-diff-total"><!--18:15--></span>h
                                </div>
                              </div>
                            </div>
                            <div class="uk-width-1-10 uk-text-center">
                              <div class="jobinfo_wrap uk-margin-remove jc-have-info">
                                <span class="isign uk-text-danger">ⓘ</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          
        </ul>
      </div>
    </div>
    <!-- column -->
  </div>
  <!-- uk-grid -->
</div>`;

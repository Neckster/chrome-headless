export const currentTabTemplate: string = `
<ul class="uk-nestable uk-width-1-1 jc-template-assignment-list jc-assignment-list jc-view-assignment">
  <li class="time-future jc-template-assignment-entry-today jc-job uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item">
    <div class="uk-grid uk-grid-collapse">
    
      <!-- no job today -->
      <div *ngIf="!assigns?.length"
           class="uk-width-1-1 uk-container-center uk-margin-small top-block padded jc-job-today jc-job-today-none">
        <div class="uk-grid uk-grid-collapse uk-text-large uk-text-bold">
          <div class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center uk-margin-small-top">
            <div class="uk-clearfix">
              <p class="text-center uk-margin-remove">{{'No_assignment_today' | translate}}</p>
            </div>
          </div>
        </div>
      </div>
          
      <div class="uk-width-small-1-1 uk-container-center uk-text-center uk-width-medium-6-10 uk-width-large-5-10">
        <div *ngIf="assigns?.length" class="uk-grid uk-grid-collapse">
          <!-- next job today: time info -->
          <div *ngIf="assigns[0].status" class="uk-width-1-1 uk-container-center uk-text-left jc-job-today jc-job-today-next">
            <div class="uk-grid uk-grid-collapse uk-text-bold">
              <div class="uk-width-1-2">
                <p>{{'Planned_assignment' | translate}}</p>
              </div>
              <div class="uk-width-1-2">
                <p><span class="jc-from">{{ assigns[0].from }}</span>–<span
                  class="jc-to">{{ assigns[0].to }}</span></p>
              </div>
            </div>
          </div>
          <div *ngIf="assigns[0].status" class="uk-width-1-1 uk-text-left jobloc">
            <div class="uk-grid uk-grid-collapse">
              <div class="uk-width-1-2"><span
                class="jc-cname">{{ assigns[0].cname }}</span></div>
              <div class="uk-width-1-2" style='word-wrap:break-word'>
                <span class="jc-nname">{{assigns[0].nname}}</span> – <span
                class="jc-pname">{{assigns[0].pname}}</span>
              </div>
            </div>
          </div>
          <!-- active (current) job  -->
          <div *ngIf="isInWork(assigns[0])"
               class="uk-width-1-1 uk-container-center uk-margin-small top-block padded">
            <div class="uk-grid uk-grid-collapse uk-text-large uk-text-bold">
              <div class="uk-width-1-2">
                <p><span>{{'Check_in' | translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span class="jc-checkin"></span>&nbsp;{{ showCorrectCheckIn(assigns[0])
                  }} <span> {{'time_suffix_o_clock' | translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span>{{'on_duty'|translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span class="jc-onjob"></span>&nbsp; {{ getDuty(assigns[0]) }}<span> {{ 'abbr_Hours'| translate}}</span>
                </p>
              </div>
              <!-- checkout button -->
              <div
                class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center uk-margin-top">
                <div class="uk-clearfix">
                  <div class="uk-width-1-1 uk-container-center">
                    <button (click)="emit({ event: 'checkout'}, {assign: assigns[0]}); hideScrollBarFn()" type="button"
                            class="uk-width-1-1 uk-button big-btn jc-click-checkout"
                            [ngClass]="{'redCheckIn': checkCheckColor('out', assigns[0].status, assigns[0].to )}">
                                <span class="uk-grid uk-grid-collapse">
                                  <span class="uk-width-1-2 uk-text-large uk-text-bold">
                                    <span>{{'Check_out' | translate}}: </span>
                                  </span>
                                  <span class="uk-width-1-2 uk-text-large uk-text-bold">
                                    <span class="jc-to">{{ assigns[0].to}}</span>
                                    <span>&nbsp;<span>{{'time_suffix_o_clock' | translate}}</span></span>
                                  </span>
                                </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- future: no check-in yet -->
          <div *ngIf="assigns[0].status === 'cantCheckIn'"
               class="uk-width-1-1 uk-container-center uk-margin-small">
            <div class="uk-grid uk-grid-collapse top-block padded uk-width-1-1 uk-container-center uk-margin-small">
              <div class="uk-grid uk-grid-collapse uk-width-1-1">
                <div class="uk-width-1-2">
                  <p><span>{{'Check_in_from' | translate}}</span></p>
                </div>
                <div class="uk-width-1-2">
                  <p><span class="jc-checkin"></span>&nbsp;{{ getCheckInTime(assigns[0])
                    }}&nbsp;<span>{{'time_suffix_o_clock' | translate}}</span></p>
                </div>
              </div>
              <div class="uk-grid uk-grid-collapse padded uk-width-1-1">
                <div class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center">
                  <div class="uk-clearfix">
                    <div class="uk-width-1-1 uk-container-center">
                      <button (click)="emit({ event: 'checkin'}, {assign: assigns[0]}); hideScrollBarFn()" type="button"
                              class="uk-width-1-1 uk-button big-btn uk-text-large uk-text-bold "
                              [disabled]="assigns[0].status !== 'canCheckIn' || assigns[0].status !== 'canCheckIn'"
                              [ngClass]="{'redCheckIn': checkCheckColor( 'in', assigns[0].status, assigns[0].from )}">
                        {{'Check_in' | translate}}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button (click)="emit({event: 'refuse'}, {assign: assigns[0] }); hideScrollBarFn()" type="button"
                    class="uk-float-right uk-button-link decline jc-click-jobdeclineModal">
              {{'Refuse' | translate}}
            </button>
          </div>
          <!-- job.mayCheckin || job.needCheckin -->
          <div
            *ngIf="assigns[0].status === 'canCheckIn' || assigns[0].status === 'needToCheckIn'"
            class="uk-width-1-1 uk-container-center uk-margin-small jc-job-today jc-job-today-check-in">
              <div class="uk-grid uk-grid-collapse top-block padded uk-width-1-1 uk-container-center uk-margin-small">
                <div class="uk-grid uk-grid-collapse uk-width-1-1">
                  <div class="uk-width-1-2">
                    <p><span>{{'Check_in_from' | translate}}</span></p>
                  </div>
                  <div class="uk-width-1-2">
                    <p><span class="jc-checkin"></span>&nbsp;{{ getCheckInTime(assigns[0])
                      }} &nbsp;<span>{{'time_suffix_o_clock' | translate}}</span></p>
                  </div>
                </div>
                <div class="uk-grid uk-grid-collapse padded uk-width-1-1">
                  <div class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center">
                    <div class="uk-clearfix">
                      <div class="uk-width-1-1 uk-container-center">
                    <button (click)="emit({event: 'checkin'}, {assign: assigns[0]}); hideScrollBarFn()" type="button"
                            class="uk-width-1-1 uk-button big-btn uk-text-large uk-text-bold"
                            [ngClass]="{'redCheckIn': checkCheckColor( 'in', assigns[0].status, assigns[0].from )}">
                      {{'Check_in' | translate}}
                    </button>
                  </div>
                </div>
              </div>
            </div>
              </div>
            <button (click)="emit({event: 'refuse'}, {assign: assigns[0]}); hideScrollBarFn()" type="button"
                    class="uk-float-right uk-button-link">
              <span class="time-near time-current">{{'Refuse' | translate}}</span>
              <span class="time-recent" style="display: none">{{'did_not_take_place' | translate}}</span>
            </button>
          </div>
  
          <!-- recent: job.needCheckout -->
          <div *ngIf="assigns[0].status === 'needToCheckOut!'"
               class="uk-width-1-1 uk-margin-small top-block padded jc-job-today jc-job-today-check-out">
            <div class="uk-grid uk-grid-collapse">
              <div class="uk-width-1-2">
                <p><span>{{'Check_in_from' | translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span class="jc-checkin"></span>&nbsp;{{ getCheckInTime(assigns[0])
                  }}<span>{{'time_suffix_o_clock' | translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span>{{'on_duty' | translate}}</span></p>
              </div>
              <div class="uk-width-1-2">
                <p><span class="jc-onjob"></span>&nbsp;{{ getDuty(assigns[0]) }}<span> {{ 'abbr_Hours'| translate}}</span>
                </p>
              </div>
              <div
                class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center uk-margin-top">
                <div class="uk-clearfix">
                  <div class="uk-width-1-1 uk-container-center">
                    <button
                      class="uk-width-1-1 uk-button big-btn uk-button-danger uk-text-large uk-text-bold">
                      {{'Check_out' | translate}}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- job.info block -->
          <div *ngIf="assigns[0] && assigns[0].info" class="uk-width-1-1 uk-text-left jobdetail">
            <div class="uk-width-1-1">
              <div class="uk-form-label">{{'Note' | translate}}</div>
              <p>{{assigns[0].info.message}}</p>
              <div class="jobinfo jc-jobinfo"></div>
            </div>
          </div>
  
          <!-- job.info block END -->
          <!-- job.colleague block -->
          <div *ngIf="assigns[0].staff && assigns[0].staff.length"
               class="uk-width-1-1 uk-text-left jc-job-colleague">
            <div class="uk-form-label">{{'Staff' | translate}}</div>
            <ul
              class="uk-nestable-list uk-width-1-1 jc-template-append-assignment-position uk-padding-remove">
              <li
                class="uk-nestable-item uk-nestable-nodrag uk-parent jobcolleaguepos section"
                id="jc-assignment-colleague-job_id-1627-1-2-131499-position_id-131499">
                <div *ngIf="assigns[0].staff && assigns[0].staff.length"
                     class="uk-nestable-panel uk-clearfix" [style.background]="assigns[0].openIn ? '#fafafa':'#fff' " (click)=" toggleStaffItem()" style='cursor:pointer'>
                  <div  class="icon large arrow-down blue uk-margin-right uk-margin-small-left" *ngIf="assigns[0].openIn" style='cursor:pointer' ></div>
                  <div class="icon large arrow-right blue uk-margin-right uk-margin-small-left" *ngIf="!assigns[0].openIn" style='cursor:pointer'></div>
                  <div class="uk-grid uk-grid-collapse">
                    <div class="jc-pname">{{ assigns[0].pname }}</div>
                  </div>
                </div>
                <ul *ngIf="assigns[0].openIn && assigns[0].staff && assigns[0].staff.length"
                    class="uk-nestable-list jc-template-append-assignment-colleague">
                  <li *ngFor="let jobber of assigns[0].staff"
                      class="uk-nestable-item uk-nestable-nodrag uid-131988">
                    <assess-modal *ngIf="jobber.open" (closeEmitter)="jobber.open = false" [jobber]='jobber' [unitName]='jobber.uname'></assess-modal>
                    <div class="uk-panel">
                      <div class="jobstaffposname uk-clearfix">
                        <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                          <div class="uk-width-7-10">
                            <span class="jc-uname">{{ jobber.uname }}</span>&nbsp;&nbsp;
                            <span class="jc-from">{{ jobber.from }}</span>–<span
                            class="jc-to">{{ jobber.to }}</span>
                          </div>
                          <div class="uk-width-3-10">
                            <button class="uk-button-link app-modal-userrate assessButton" [style.color]=" jobber.rating ? 'black' : '#07a0e1'" [disabled]='blocked'  (click)="jobber.open = true;" type='button'>{{'assess' | translate}}</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div *ngIf="assigns[1]" class="uk-width-1-1" style="margin-top: 10px">
            <div class="uk-width-1-1 uk-container-center uk-text-left jc-job-today jc-job-today-next" style="padding-left: 0;">
              <div class="uk-grid uk-grid-collapse uk-text-bold">
                <div class="uk-width-1-2">{{'Planned_assignment' | translate}}</div>
                <div class="uk-width-1-2">
                  <p>
                    <span class="jc-from">{{ assigns[1].from }}</span>–<span class="jc-to">{{ assigns[1].to }}</span>
                  </p>
                </div>
              </div>
              <div class="uk-grid uk-grid-collapse">
                <div class="uk-width-1-2">{{ assigns[1].cname }}</div>
                <div class="uk-width-1-2">
                  <span class="jc-nname">{{assigns[1].nname}}</span> –
                  <span class="jc-pname">{{assigns[1].pname}}</span>
                </div>
              </div>
            </div>
            <div class="uk-width-1-1 uk-container-center uk-margin-small jc-job-today jc-job-today-check-in">
              <div class="uk-grid uk-grid-collapse top-block padded uk-width-1-1 uk-container-center uk-margin-small">
              <div class="uk-grid uk-grid-collapse uk-width-1-1">
                <div class="uk-width-1-2">
                  <p><span>{{'Check_in_from' | translate}}</span></p>
                </div>
                <div class="uk-width-1-2">
                  <p>
                    <span class="jc-checkin"></span>&nbsp;{{ getCheckNextAssignmentInTime(assigns[1])}} &nbsp;<span>{{'time_suffix_o_clock' | translate}}</span>
                  </p>
                </div>
              </div>
              <div class="uk-grid uk-grid-collapse padded uk-width-1-1">
                <div class="uk-width-small-7-10 uk-width-xlarge-2-3 uk-container-center">
                  <div class="uk-clearfix">
                    <div class="uk-width-1-1 uk-container-center">
                      <button (click)="emit({event: 'checkin'}, {assign: assigns[1]}); hideScrollBarFn()" type="button"
                              class="uk-width-1-1 uk-button big-btn uk-text-large uk-text-bold "
                              [disabled]="assigns[1].status !== 'canCheckIn' || assigns[1].status !== 'canCheckIn'"
                              [ngClass]="{'redCheckIn': checkCheckColor( 'in', assigns[1].status, assigns[1].from )}">
                        {{'Check_in' | translate}}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
                </div>
              <button (click)="emit({event: 'refuse'}, {assign: assigns[1]}); hideScrollBarFn()" type="button"
                      class="uk-float-right uk-button-link">
                <span class="time-near time-current">{{'Refuse' | translate}}</span>
                <span class="time-recent" style="display: none">{{'did_not_take_place' | translate}}</span>
              </button>
            </div>
            <div *ngIf="assigns[1].staff && assigns[1].staff.length"
                 class="uk-width-1-1 uk-text-left jc-job-colleague" style="padding-left: 0; margin-top: 25px;">
              <div class="uk-form-label">{{'Staff' | translate}}</div>
              <ul class="uk-nestable-list uk-width-1-1 jc-template-append-assignment-position uk-padding-remove">
                <li class="uk-nestable-item uk-nestable-nodrag uk-parent jobcolleaguepos section" id="next-assignment-staff">
                  <div *ngIf="assigns[1].staff && assigns[1].staff.length"
                       class="uk-nestable-panel uk-clearfix"
                       [style.background]="assigns[1].openIn ? '#fafafa':'#fff' "
                       (click)=" toggleNextStaffItem(assigns[1])" style='cursor:pointer'>
                    <div class="icon large arrow-down blue uk-margin-right uk-margin-small-left"
                         *ngIf="assigns[1].openIn" style='cursor:pointer'></div>
                    <div class="icon large arrow-right blue uk-margin-right uk-margin-small-left"
                         *ngIf="!assigns[1].openIn" style='cursor:pointer'></div>
                    <div class="uk-grid uk-grid-collapse">
                      <div class="jc-pname">{{ assigns[1].pname }}</div>
                    </div>
                  </div>
                  <ul *ngIf=" assigns[1].openIn && assigns[1].staff && assigns[1].staff.length"
                      class="uk-nestable-list jc-template-append-assignment-colleague">
                    <li *ngFor="let jobber of assigns[1].staff"
                        class="uk-nestable-item uk-nestable-nodrag uid-131988">
                      <assess-modal *ngIf="jobber.open" (closeEmitter)="jobber.open = false"
                                    [jobber]='jobber' [unitName]='jobber.uname'></assess-modal>
                      <div class="uk-panel">
                        <div class="jobstaffposname uk-clearfix">
                          <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                            <div class="uk-width-7-10">
                              <span class="jc-uname">{{ jobber.uname }}</span>&nbsp;&nbsp;
                              <span class="jc-from">{{ jobber.from }}</span>–<span
                                    class="jc-to">{{ jobber.to }}</span>
                            </div>
  
                            <div class="uk-width-3-10">
                              <button class="uk-button-link app-modal-userrate assessButton"
                                      [disabled]='blocked' [style.color]=" jobber.rating ? '#black' : '#07a0e1'" 
                                      (click)="jobber.open = true;" type='button'>
                                {{'assess' | translate}}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </li>
</ul>
`;

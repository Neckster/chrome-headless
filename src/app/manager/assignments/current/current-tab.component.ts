import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChange } from '@angular/core';
import { currentTabTemplate }       from './current-tab.html';
import { JobService } from '../../../services/shared/JobService';
import { curryN, merge } from 'ramda';

import * as moment from 'moment';

const emitter = curryN(3, (emitterInterface, payload, event) => {
  return emitterInterface.emit(merge(payload, event));
});

@Component({
  selector: 'current',
  template: currentTabTemplate
})
export class CurrentTabComponent implements OnInit {

  @Input() assigns;
  @Output() out = new EventEmitter();

  private emit: (event, payload) => void;
  private hideScrollBarFn; // : (any) => any;

  constructor(private jobService: JobService) {
    this.hideScrollBarFn = this.jobService.hideScrollBar;
    this.emit = emitter(this.out);
  }

  ngOnInit() {}

  ngOnChanges(change: SimpleChange) {}

  isInWork(ass) {
    return ass.status === 'needToCheckOut'
      || ass.status === 'canCheckOut'
      || ass.status === 'inWork';
  }

  isNextAssignment(current, nextAssign) {
    return this.jobService.isNextAssignment(current, nextAssign);
  }

  checkCheckColor( type, status, time ) {
    if ( status === undefined || time === undefined ) return;
    let currentTime = new Date(),
      current = currentTime.getHours() * 60 + currentTime.getMinutes();
    let timeToCheck = time.split(':'),
      check = +timeToCheck[0] * 60 + +timeToCheck[1];
    let needCheck = current > check;
    if ( type === 'in') {
      return  ( status === 'canCheckIn' && needCheck );
    }
    if ( type === 'out' ) {
      return  ( status === 'needToCheckOut' && needCheck );
    }
  }

  getCheckInTime(ass) {
    return ass.checkIn < ass.tFrom ? moment(ass.tFrom).format('HH:mm')
      : (
      ass.checkIn
        ? moment(ass.checkIn).format('HH:mm')
        : moment(ass.tFrom).add(-15, 'm').format('HH:mm'));
  }

  toggleStaffItem() {
    let assign = this.assigns[0];
/*
    if (assign.openIn) {
      this.cache.removeFromArray(
        'assignments:current:po_st',
        `${assign.company_id}-${assign.position_id}` +
        `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    } else {
      this.cache.pushArray(
        'assignments:current:po_st',
        `${assign.company_id}-${assign.position_id}` +
        `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    }
    */
    assign.openIn = !assign.openIn;
  }


  getCheckNextAssignmentInTime(ass) {
    return (!ass.checkIn || ass.checkIn > ass.tFro)
      ? moment(ass.tFrom).add(-15, 'm').format('HH:mm')
      : moment(ass.tFrom).format('HH:mm');
  }



}

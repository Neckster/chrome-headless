import { Component, Input }         from '@angular/core';
import { ActivatedRoute }           from '@angular/router';

import { select }                   from 'ng2-redux/lib/index';
import * as moment                  from 'moment';

import {
  identity as Identity,
  head } from 'ramda';

import { JobService }               from '../../services/shared/JobService';
import { Cache }                    from '../../services/shared/cache.service';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';

import { assignmentsHtml }          from './assignments.html.ts';
import { AppStoreAction }           from '../../store/action';

import { Assignment }               from '../../models/Assignment';

import { CheckinModalComponent }    from './checkio/checkin-modal/checkin.modal.component';
import { CheckoutModalComponent }   from './checkio/checkout-modal/checkout.modal.component';
import { PastAssignmentsComponent } from './past-tab/past-tab.component';
import { UpcomingAssComponent }     from './upcoming/upcoming.component';
import { RefuseModalComponent }     from './refuse-modal/refuse-modal';
import { AssessModalComponent }     from './assess-modal/assess-modal';
import { TentativeComponent }       from './tentative/tentative.component';
import { CurrentTabComponent }      from './current/current-tab.component';
import { AssignmentsService }       from '../../services/assignments.service';
import { InfoResponse }             from '../../services/company.service';


const ifelse = (condition, t, f) => {
  return condition ? t : f;
};

const padLeft = (str, char, len) => {
  if (str.length < len) {
    return ''.concat(char.repeat( len - str.length ), str);
  }
};

const REFRESH_INTERVAL: number = 15000;

@Component({
  selector: 'assignments',
  directives: [ CheckinModalComponent, CheckoutModalComponent,
    PastAssignmentsComponent, UpcomingAssComponent, RefuseModalComponent,
    AssessModalComponent, TentativeComponent, CurrentTabComponent ],
  styles: [ require('!raw!less!./style.less') ],
  template: assignmentsHtml
})

export class AssignmentsComponent {

  @select() info$;
  @select() currentUnit$;

  public pastAss;
  public currentUnit;
  public info;
  public checkOutModal = false;
  public currentAssign: any;
  public refuseModal = false;
  public assessModal = false;
  public openModal = false;
  public assignments: any;
  public currentAssignments: any;
  public unsubscribe = [];
  public keys = [
    'checkIn',
    'waitForCheckIn',
    'needToCheckOut',
    'inWork',
    'canCheckOut'
  ];
  public hideScrollBar = this.jobService.hideScrollBar;
  public tabs = [
    { name: 'current', active: true },
    { name: 'future', active: false },
    { name: 'tentative', active: false },
    { name: 'past', active: false }
  ];
  public titleTab;

  private interval;

  constructor(public jobService: JobService, public cache: Cache,
              public action: AppStoreAction, private route: ActivatedRoute,
              private assignmentsService: AssignmentsService) {
  }

  ngOnInit() {
    const fetch = () => {
      this.action.fetchGetInfo(ParseOrganisationService.calculateWeek());
    };
    fetch();
    // this.interval = setInterval(fetch, REFRESH_INTERVAL);


    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname) {
        this.currentUnit = unit;
        this.currentAssignments = this.initAssignments(this.info);
        this.currentAssign = head(this.currentAssignments);
      }
    }));

    this.unsubscribe.push(this.info$.subscribe(info => {
      if (info.manager) {
        this.info = info;
        let unitId = info.manager.selected.unit_id;
        if (unitId !== 0 && !isNaN(unitId))
          this.action.fetchCurrentUnit(info.manager.selected.unit_id);
      } else if (info.assignment) {
        this.info = info;
        this.currentAssignments = this.initAssignments(this.info);
        this.currentAssign = head(this.currentAssignments);
      }
    }));

    if (this.cache.get('assignments:tab'))
      this.tabs = this.tabs.map((tab) =>
        ({ name: tab.name, active: this.cache.get('assignments:tab') === tab.name }));


    this.route.params.subscribe(({ tab = 'current' } = {}) => {
      this.tabs = this.tabs.map(t => Object.assign({}, t, { active: (t.name === tab ) }));
      window.scrollTo(0, 0);
    });

  }

  public modalOpen() {
    this.openModal = true;
  }

  public onCurrentEvent(e) {
    if (e.event === 'checkin') {
      this.currentAssign = e.assign;
      this.modalOpen();
    }
    if (e.event === 'refuse') {
      this.currentAssign = e.assign;
      this.refuseModal = true;
    }
    if (e.event === 'checkout') {
      this.currentAssign = e.assign;
      this.checkOutModal = true;
    }
  }

  initAssignments(info: InfoResponse) {

    if (!info || !info.assignment.length) return;

    const userId = info.webuser.webuser_id;
    const week =  moment(info.meta.time).isoWeekday();
    const timeGGWW = +moment(info.meta.time).format('GGWW');

    let assigns = this.assignmentsService.getCurrentAssignments(
      {webuser_id: userId, week, timeGGWW},
      info.assignment,
      info
    );

    assigns.forEach((a: any, i) => {
      if ((this.cache.isInArray(
          'assignments:current:po_st',
          `${a.company_id}-${a.position_id}` +
          `-${a.dayoffweek}-${a.shift_idx}`
        ) && i === 0) || (i === 1 && this.isNextAssignment(assigns[0], a) && this.cache.isInArray(
              'assignments:next:po_st',
              `${a.company_id}-${a.position_id}` +
              `-${a.dayoffweek}-${a.shift_idx}`
          )))
        a.openIn = true;
    });

    return assigns;

  }

  getDuty(ass) {
    let diff = moment.duration(moment(this.info.meta.time).diff(moment(ass.checkIn))).asHours();
    let hour = `${Math.floor(diff)}`;
    let minutes = `${Math.floor(((diff % 1) * 60))}`;
    if (diff <= (1 / 60) || (hour === '0' && minutes === '0')) {
      return ``;
    }
    const hrs = ifelse((hour.length === 1), padLeft('' + hour, '0', 2), hour);
    const mins = ifelse((minutes.length === 1), padLeft('' + minutes, '0', 2), minutes);

    return`${hrs}:${mins}`;

  }

  getCheckInTime(ass) {
    return ass.checkIn < ass.tFrom ? moment(ass.tFrom).format('HH:mm')
      : (
        ass.checkIn
        ? moment(ass.checkIn).format('HH:mm')
        : moment(ass.tFrom).add(-15, 'm').format('HH:mm'));
  }

  showCorrectCheckIn(inform) {
    return inform.checkIn ?
      moment(inform.checkIn).format('HH:mm') :
      moment(inform.tFrom).format('HH:mm');
  }

  getCheckNextAssignmentInTime(ass) {
    return (!ass.checkIn || ass.checkIn > ass.tFro)
        ? moment(ass.tFrom).add(-15, 'm').format('HH:mm')
        : moment(ass.tFrom).format('HH:mm');
  }

  calcAssignments(assigns) {
    return this.jobService.calcAssignments(assigns, this.info.meta.time);
  }

  changeTab(key) {
    this.cache.set('assignments:tab', key);
    this.tabs = this.tabs
      .map(tab => { return { name: tab.name, active: tab.name === key ? true : false }; });
  }

  checkTitleTab() {
    let tab = this.tabs.filter( t => {
      return t.active === true;
    })[0].name;

    if ( tab === 'future' ) {
      return this.titleTab = 'upcoming';
    } else {
      this.titleTab = tab;
    }
    return this.titleTab;
  }

  toggleStaffItem() {
    let assign = this.currentAssignments[0];

    if (assign.openIn) {
      this.cache.removeFromArray(
        'assignments:current:po_st',
        `${assign.company_id}-${assign.position_id}` +
        `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    } else {
      this.cache.pushArray(
        'assignments:current:po_st',
        `${assign.company_id}-${assign.position_id}` +
        `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    }
    assign.openIn = !assign.openIn;
  }

  toggleNextStaffItem() {
    let assign = this.currentAssignments[1];

    if (assign.openIn) {
      this.cache.removeFromArray(
          'assignments:next:po_st',
          `${assign.company_id}-${assign.position_id}` +
          `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    } else {
      this.cache.pushArray(
          'assignments:next:po_st',
          `${assign.company_id}-${assign.position_id}` +
          `-${assign.dayoffweek}-${assign.shift_idx}`
      );
    }
    assign.openIn = !assign.openIn;
  }

  checkTabStatus(tab: string) {
    return this.tabs.find((t) => t.name === tab).active;
  }


  openCO() {
    this.checkOutModal = true;
  }

  openAssess() {
    this.assessModal = true;
  }

  isInWork(ass) {
    return ass.status === 'needToCheckOut'
      || ass.status === 'canCheckOut'
      || ass.status === 'inWork';
  }

  checkInBefore(from) {
    return moment(from, 'HH:mm').subtract(15, 'minutes').format('HH:mm');
  }

  isNextAssignment(current, nextAssign) {
    if (!current) {
      current = this.currentAssignments[0];
    }
    if (!nextAssign && this.currentAssignments.length > 1) {
      nextAssign = this.currentAssignments[1];
    }
    return this.jobService.isNextAssignment(current, nextAssign);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
    this.unsubscribe.forEach(el => el.unsubscribe());
  }

  onModalClose(event) {
    this.action.fetchGetInfo();
  }
}

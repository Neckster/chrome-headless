import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { AppStoreAction } from '../../../store/action';
import { TargetJobService } from '../../../services/job.service';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { refuseHtml } from './refuse-modal.html';
import { JobService } from '../../../services/shared/JobService';

@Component({
  selector: 'refuse-modal',
  providers: [ ],
  directives: [],
  pipes: [ ],
  styles: [`
      header#label-jobdecline{
        height: 1.2em;
        min-height: 1.2em;
        position: static;
      }
      .uk-modal-dialog > .uk-close:first-child:after,
      .uk-modal-dialog > .uk-close:first-child:before{
        top:60%;
      }
      .uk-modal-header h2 .uk-modal-close::before{
        top:0
      }
      
    
      `],
  template: refuseHtml
})

export class RefuseModalComponent {
  @Input() open;
  @Input() param;
  @Output() closeEmitter = new EventEmitter();
  public reason;
  public comment;
  public errorMessage = false;
  public hideScrollBar = this.jobService.hideScrollBar;
  // TypeScript public modifiers
  constructor(public action: AppStoreAction,
              public targetJob: TargetJobService,
              public jobService: JobService) {
  }
  handleReason(event) {
    this.reason = event.target.value;
    if ( this.reason === undefined ) {
      this.errorMessage = true;
    }
  }
  save(e) {
    e.preventDefault();
    if ( this.reason === undefined ) {
      this.errorMessage = true;
      return;
    }
    let value = {info: this.comment, reason: this.reason};
    let week = ParseOrganisationService.calculateWeek();
    this.targetJob.jobRefuse(value, this.param, this.param.week).subscribe(res => {
      this.action.fetchGetInfo(week);
      this.closeEmitter.emit(undefined);
    });
    this.hideScrollBar();
    this.errorMessage = false;
    this.comment = '';
  }
  closeRefuseModal( e ) {
    e.preventDefault();
    this.closeEmitter.emit(undefined);
    this.hideScrollBar();
  }
}

export const refuseHtml = `
<section *ngIf="open" class="uk-modal uk-open RefuesModel" id="jobdecline" tabindex="-1" role="dialog" aria-labelledby="label-jobdecline" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog">
        <a style="cursor:pointer" class="uk-modal-close uk-close" (click)="closeRefuseModal( $event );"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header">
          <header id="label-jobdecline"  style="box-shadow:none!important;">
            <h2>
            <a  class="uk-modal-close" (click)="closeRefuseModal( $event );">
              <!-- mobile-back-button -->
            </a>{{'Refuse_job' | translate}}</h2>
          </header>
        </div>
        <div class="modal-content">
          <form class="uk-form jobdecline" ngNoForm>
            <div class="uk-grid uk-grid-collapse uk-text-left">
              <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
                <label class="uk-form-label" for="jobdeclineinfo">{{'Comment' | translate}}</label>
              </div>
              <div class="uk-width-small-1-1 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
                <textarea [(ngModel)]="comment" class="uk-width-1-1" id="jobdeclineinfo"></textarea>
              </div>
              <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
                <label class="uk-form-label" for="jobdeclinereason">{{'Reason' | translate}}</label>
              </div>
              <div class="uk-width-small-1-1 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
                <div class="select-wrapper uk-width-1-1 ">
                  <select (change)="handleReason($event)" id="jobdeclinereason" class="select" >
                    <option value="please_select" selected disabled>{{'please_select' | translate}}</option>
                    <option value="sick" >{{'Sickness' | translate}}</option>
                    <option value="unavailable">{{'unavailable' | translate}}</option>
                  </select>
                </div>
              </div>
              <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
                <div class="uk-alert uk-alert-warning error reason missing " [ngClass]='{"hidden" : !errorMessage}'>{{'msg_err_missing_reason' | translate}}</div>
              </div>
              <div class="uk-width-1-1">
                <div class="uk-grid uk-grid-small">
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button style="cursor:pointer" (click)="save($event)" type="submit" class="uk-button uk-button-large uk-button-danger uk-width-1-1">{{'Refuse' | translate}}</button>
                  </div>
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button style="cursor:pointer" (click)="closeRefuseModal( $event );" class="uk-button uk-modal-close uk-button-large cancel uk-width-1-1">{{'button_Cancel' | translate}}</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- UK-GRID --></form>
          

        </div>
      </div>
    </section>
    `;

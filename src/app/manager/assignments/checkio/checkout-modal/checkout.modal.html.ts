//noinspection TsLint
export const checkoutModalHtml = `
<section *ngIf="open" class="uk-modal checkout uk-open" id="checkinout" tabindex="-1" role="dialog"
         aria-labelledby="label-checkinout" aria-hidden="false"
         style="display: block; overflow-y: scroll;">
  <div class="uk-modal-dialog">
    <a (click)="closeEmitter.emit(); hideScrollBar()" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}'
       data-close="dismiss" data-dismiss="modal">
      <!-- icon -->
    </a>
    <div class="uk-modal-header">
        <h2>
          <a (click)="closeEmitter.emit(); hideScrollBar()" class="uk-modal-close">
            <!-- mobile-back-button -->
          </a>
          <span class="checkin">{{'Check_in' | translate}}</span><span class="checkout">{{'Check_out' | translate}}</span></h2>
    </div>
    <div class="modal-content">
      <form class="uk-form checkinout" ngNoForm>
        <div class="uk-grid uk-grid-collapse uk-text-left">
          <div
            class="uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <input type="hidden" id="checkinoutplanned" value="16:00" name="planned">
            <div class="info">
              <div id="checkinoutinfo"></div>
                  <span class="checkout">
                    <span *ngIf="!isPast" class="early">{{'Assignment_finish_future' | translate}} <span
                      class="plannedtime">{{param.to}}</span></span>
                    <span *ngIf="isPast" class="late">{{'Assignment_finish_past' | translate}} <span
                      class="plannedtime">{{param.to}}</span></span>
                  </span>
            </div>
          </div>
          <div
            class="checkout uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom ws-success">
            <label class="uk-form-label" for="checkintypeplanned">
              <input type="radio" id="checkintypeplanned" name="type" (change)="handleRadio(true)" value="planned" [checked]="isPast"
                     class="user-success">
              <span class="early hidden">{{'Confirm_planned_finish_past' | translate}} <span class="plannedtime">{{param.to}}</span></span>
              <span class="late"> {{'Confirm_planned_finish_past' | translate}} <span
                class="plannedtime">{{param.to}}</span></span>
            </label>
          </div>
          <div
            class="checkout uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <span class="early hidden"> {{'Adjust_finish' | translate}}:</span>
            <label class="uk-form-label late" for="checkouttypejobber">
              <input type="radio" (change)="handleRadio(false)" id="checkouttypejobber" name="type"
                     value="jobber" [checked]="!isPast" class="user-success">
              <span>{{'Adjust_finish' | translate}}:</span>
            </label>
          </div>
          <div
            class="uk-width-small-1-3 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            Time
          </div>
          <div
            class="uk-width-small-2-3 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <div id="checkinouttime" class="timedialexact">
              <div class="uk-grid uk-grid uk-grid-collapse">
                <div class="hh">
                  <div class="select-wrapper uk-width-1-1">
                    <select (change)='handleHours($event)' class="select timehh" [disabled]="dataFromAss">
                      <!-- filled by js -->
                      <option *ngFor="let h of hours" [selected]="selectedHour(h)" value="{{h}}">{{h}}</option>
                    </select>
                  </div>
                </div>
                <div class="dial-divider uk-text-center">:</div>
                <div class="mm">
                  <div class="select-wrapper uk-width-1-1">
                    <select (change)="handleMinutes($event)" class="select timemm" [disabled]="dataFromAss">
                      <!-- filled by js -->
                      <option *ngFor="let m of minutes" [selected]="selectedMinutes(m)" value="{{m}}">{{m}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="uk-width-small-1-3 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <label class="uk-form-label" for="checkinoutconsultation">{{'Agreement' | translate}}</label>
          </div>
          <div
            class="uk-width-small-2-3 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <div class="select-wrapper uk-width-1-1 ws-success">
              <select id="checkinoutconsultation" class="select user-success" name="consultation">
                <option>{{'No_agreement' | translate}}</option>
              </select>
            </div>
          </div>
          <div
            class="checkout uk-width-small-1-3 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <label class="uk-form-label">
              <span>{{'Break' | translate}}</span><br><span>{{'remark_Total_duration' | translate}}</span>
            </label>
          </div>
          <div
            class="checkout uk-width-small-2-3 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <div class="timedialexact" id="checkinoutidle">
              <div class="uk-grid uk-grid uk-grid-collapse">
                <div class="hh">
                  <div class="select-wrapper uk-width-1-1">
                    <select [(ngModel)]="idlHour" class="select timehh">
                      <option [ngValue]="'00'">00</option>
                      <option [ngValue]="'01'">01</option>
                      <option [ngValue]="'02'">02</option>
                    </select>
                  </div>
                </div>
                <div class="dial-divider uk-text-center">:</div>
                <div class="mm">
                  <div class="select-wrapper uk-width-1-1">
                    <select [(ngModel)]="idleMinutes" class="select timemm">
                      <!-- filled by js -->
                      <option *ngFor="let m of minutes" [ngValue]="m">{{m}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div *ngIf="error" class="uk-width-small-4-1 uk-width-medium-4-1 uk-container-center uk-grid-collapse  alertbox">
              <div id="error-organisation-position-active" class="uk-alert uk-alert-danger error" data-uk-alert="">
                <a (click)="error = !error" class="uk-alert-close uk-close"></a>{{"msg_err_checkout_before_checkin"| translate}}
              </div>
          </div>
          <div *ngIf="invalidBreak" class="uk-width-small-4-1 uk-width-medium-4-1 
          uk-container-center 
          uk-grid-collapse  alertbox">
              <div id="error-organisation-position-active" class="uk-alert uk-alert-danger error" data-uk-alert="">
                <a (click)="invalidBreak = !invalidBreak" class="uk-alert-close uk-close"></a>
                {{'Break_is_bigger'|translate}}
              </div>
          </div>
          <div
            class="uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <span class="uk-alert uk-alert-warning error future regular hidden">{{'msg_err_time_is_future' | translate}}</span>
            <span class="uk-alert uk-alert-warning error max regular hidden">{{'msg_err_time_is_after_finish' | translate}}</span>
            <span class="uk-alert uk-alert-warning error min regular hidden">{{'msg_err_time_is_before' | translate}}<span
              class="gracetime"><!--00:00--></span></span>
            <span class="uk-alert uk-alert-warning error clash regular hidden">{{'msg_err_checkout_before_checkin' | translate}}</span>
          </div>
          <div class="uk-width-1-1">
            <div class="uk-grid uk-grid-small">
              <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                <button (click)="submit($event);" type="submit"
                        class="uk-button uk-button-large ok uk-width-1-1">OK<span class="checkin">{{'comma_check_in' | translate}}</span>
                  <span class="checkout">{{'comma_check_out' | translate}}</span>
                </button>
              </div>
              <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                <button (click)="$event.preventDefault();closeEmitter.emit(); hideScrollBar()"
                        class="uk-button uk-modal-close uk-button-large cancel uk-width-1-1"> {{'button_Cancel' | translate}}
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>


    </div>
  </div>
  <!--.checkinout-->
</section>
`;

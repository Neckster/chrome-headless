import {
  Component,
  Input,
  Output,
  EventEmitter }                    from '@angular/core';
import { checkoutModalHtml }        from './checkout.modal.html.ts';
import { MapToIterable }            from '../../../../lib/pipes/map-to-iterable';
import * as moment                  from 'moment';
import { AppStoreAction }           from '../../../../store/action';
import { TargetJobService }         from '../../../../services/job.service';
import { ParseOrganisationService } from '../../../../services/shared/parseOrganisation.service';
import { JobService }               from '../../../../services/shared/JobService';
import { CheckIOGeneric }           from '../checkio.generic';
import { range }                    from 'ramda';

@Component({
  selector: 'checkout-modal',
  pipes: [ MapToIterable ],
  template: checkoutModalHtml
})

export class CheckoutModalComponent extends CheckIOGeneric {

  @Input() open;
  @Input() param;
  @Output() closeEmitter = new EventEmitter();

  get currentHour(): number {
    return parseInt(this.time.substring(0, 2), 10);
  }

  get currentMinute(): number {
    return parseInt(this.time.substring(3, 5), 10);
  }


  public hour: any;
  public error = false;
  public idlHour = '00';
  public idleMinutes = '00';
  public minute: any;
  public time = '00:00';
  public radio = false;
  public isPast = false;
  public dataFromAss = false;

  private invalidBreak: boolean;
  private diffString: string;

  constructor(public action: AppStoreAction, public targetJob: TargetJobService,
              public jobService: JobService) {
    super(jobService);
  }

  ngOnInit() {
    if (this.param) {
      this.isPast = moment().valueOf() > moment(this.param.to, 'HH:mm').valueOf();
      let hours = range(0, 24);
      let minutes = range(0, 60);
      let now = moment().format('HH:mm');

      this.hours = this.leftPad(hours);
      this.minutes = this.leftPad(minutes);
      this.hour = moment().format('HH');
      this.minute = moment().format('mm');
      this.time = now;
    }
    if (this.isPast) this.handleRadio(true);
  }

  handleHours(event) {

    let now = moment().format('HH:mm');
    this.hour = event.target.value;

    if ((+this.hour) === (+this.hours[0])) {

      let mm = [];

      for (let i = 0; i < 60; i++) {
        if (i >= (+`${now[3]}${now[4]}`)) {
          mm.push(i);
        }
      }

      this.minutes = this.leftPad(mm);

    } else {
      let mm = range(0, 61);
      this.minutes = this.leftPad(mm);
    }
  }

  handleMinutes(event) {
    this.minute = event.target.value;
  }

  submit(e) {
    e.preventDefault();
    this.error = false;
    this.invalidBreak = false;
    let wasAssignHm = this.param.from,
    now = moment().add(1, 'm').format('HH:mm'),
    selectedTime =  this.hour + ':' + this.minute;
    if (selectedTime < now) {
      let
        brakeH =  this.idlHour + ':' + this.idleMinutes,
        miliOut = moment().set('hour', +this.hour).set('minute', +this.minute),
        miliIn = this.param.checkIn;
      if (selectedTime > wasAssignHm) {
        miliOut = moment().add(1, 'day').set('hour', +this.hour).set('minute', +this.minute);
      }
      let diff = +miliOut - miliIn;
      this.diffString = this.jobService.getTimeFromDiff(diff);

      let week = ParseOrganisationService.calculateWeek();
      let t;
      this.dataFromAss ?
        t = moment(this.param.to, 'HH:mm').valueOf() :
        t = moment().hour(+this.hour).minute(+this.minute).valueOf();
      if (this.diffString <= brakeH) {
        this.invalidBreak = true;
      } else {
        this.targetJob.jobStatus(this.param, 'checkedin', t, week, 0, brakeH)
          .subscribe(() => {
            this.action.fetchGetInfo();
            this.closeEmitter.emit(undefined);
            this.hideScrollBar();
          });
      }

    } else {
      this.error = true;
    }
  }

  handleRadio(value) {
    this.dataFromAss = value;
  }

}

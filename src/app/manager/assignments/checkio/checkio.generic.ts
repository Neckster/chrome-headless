import { JobService } from '../../../services/shared/JobService';
import { map } from 'ramda';

export abstract class CheckIOGeneric {

  public time: string;
  public hours: string[];
  public minutes: string[];
  public disabled: boolean;
  public currentHour: number;
  public currentMinute: number;

  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public jobService: JobService) {
    this.disabled = false;
  }

  selectedMinutes(m: string) {
    return (parseInt(m, 10) === this.currentMinute);
  }

  selectedHour(h: string) {
    return (parseInt(h, 10) === this.currentHour);
  }

  leftPad(arr: number[]) {
    return map((v) => v > 9 ? '' + v : '0' + v, arr);
  }

  abstract submit(event: any): void;

}

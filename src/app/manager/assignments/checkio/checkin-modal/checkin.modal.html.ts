//noinspection TsLint
export const checkinModalHtml = `
<section *ngIf="open" class="uk-modal checkin uk-open" id="checkinout" tabindex="-1" role="dialog"
         aria-labelledby="label-checkinout" aria-hidden="false"
         style="display: block; overflow-y: scroll;">
  <div class="uk-modal-dialog">
    <a class="uk-modal-close uk-close" (click)="closeCheckInModal($event); "  title ='{{ "dismiss" | translate }}'
       data-close="dismiss" data-dismiss="modal">
      <!-- icon -->
    </a>
    <div class="uk-modal-header">
        <h2>
          <a  class="uk-modal-close" (click)="closeCheckInModal($event); " >
            <!-- mobile-back-button -->
          </a>
          <span class="checkin">{{'Check_in' | translate}}</span><span class="checkout">{{'Check_out' | translate}}</span></h2>
    </div>
    <div class="modal-content">
      <form class="uk-form checkinout" ngNoForm>
        <div class="uk-grid uk-grid-collapse uk-text-left">
          <div
            class="uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <input type="hidden" id="checkinoutplanned" value="09:00">
            <div class="info">
              <div id="checkinoutinfo"></div>
                  <span class="checkin">
                    <span class="early hidden">{{'Assignment_will_start' | translate}} <span class="plannedtime">{{param.from}}</span></span>
                    <span class="late">{{'Assignment_has_started' | translate}} <span
                      class="plannedtime">{{param.from}}</span></span>
                  </span>
            </div>
          </div>
          <div
            class="checkin uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <label class="uk-form-label" for="checkintypeplanned">
              <input (click)="radioHandle(true)" type="radio" id="checkintypeplanned" name="type"
                     value="planned">
              <span *ngIf="!isFifteenMinutesBeforeStart()" class="late">{{'Confirm_planned_start_past' | translate}} <span class="plannedtime">{{param.from}}</span></span>
              <span *ngIf="isFifteenMinutesBeforeStart()" class="late">{{'Confirm_planned_start_future' | translate}} <span class="plannedtime">{{param.from}}</span></span>
            </label>
          </div>
          <div
            class="checkin uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <label class="uk-form-label" for="checkintypejobber">
              <input (click)="radioHandle(false)" type="radio" id="checkintypejobber" name="type"
                     value="jobber" checked>
              <span *ngIf="!isFifteenMinutesBeforeStart()" class="late">{{'Adjust_start_late' | translate}}</span>
              <span *ngIf="isFifteenMinutesBeforeStart()" class="early">{{'Adjust_start_early' | translate}}</span>
            </label>
          </div>
          <div class="uk-width-small-1-3 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            {{ 'Time' | translate }}
          </div>
          <div
            class="uk-width-small-2-3 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <div id="checkinouttime" class="timedialexact">
              <div class="uk-grid uk-grid uk-grid-collapse">
                <div class="hh">
                  <div class="select-wrapper uk-width-1-1">
                    <select class="select timehh" (change)="timeHandler($event, true)" [disabled]="disabled">
                      <option *ngFor="let h of hours;let i = index" [selected]="selectedHour(h)">{{h}}</option>
                    </select>
                  </div>
                </div>
                <div class="dial-divider uk-text-center">:</div>
                <div class="mm">
                  <div class="select-wrapper uk-width-1-1">
                    <select class="select timemm" (change)="timeHandler($event, false)" [disabled]="disabled">
                      <option *ngFor="let m of minutes" [selected]="selectedMinutes(m)">{{m}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="uk-width-small-1-3 uk-width-medium-4-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <label class="uk-form-label" for="checkinoutconsultation">{{'Agreement' | translate}}</label>
          </div>
          <div class="uk-width-small-2-3 uk-width-medium-6-10 uk-container-center uk-grid-collapse uk-margin-bottom">
            <div class="select-wrapper uk-width-1-1">
              <select id="checkinoutconsultation" class="select" [disabled]="disabled">
                <option>{{'No_agreement' | translate}}</option>
              </select>
            </div>
          </div>
          <div class="timedialexact" id="checkinoutidle">

          </div>
        </div>
        <div class="uk-width-1-1">
          <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
              <button (click)="submit($event)" type="submit"
                      class="uk-button uk-button-large ok uk-width-1-1">OK<span class="checkin">{{'comma_check_in' | translate}}</span>
                <span class="checkout">{{'comma_check_out' | translate}}</span>
              </button>
            </div>
            <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
              <button (click)="closeCheckInModal($event)"
                      class="uk-button uk-modal-close uk-button-large cancel uk-width-1-1">{{'button_Cancel' | translate}}
              </button>
            </div>
          </div>
        </div>
      </form>


    </div>
  </div>
  <!--.checkinout-->
</section>`;

import {
  Component,
  Input,
  Output,
  EventEmitter }                    from '@angular/core';
import { checkinModalHtml }         from './checkin.modal.html.ts';
import { MapToIterable }            from '../../../../lib/pipes/map-to-iterable';
import * as moment                  from 'moment';
import { AppStoreAction }           from '../../../../store/action';
import { TargetJobService }         from '../../../../services/job.service';
import { ParseOrganisationService } from '../../../../services/shared/parseOrganisation.service';
import { JobService }               from '../../../../services/shared/JobService';
import { CheckIOGeneric }           from '../checkio.generic';
import { range }                    from 'ramda';

@Component({
  selector: 'checkin-modal',
  pipes: [ MapToIterable ],
  template: checkinModalHtml
})

export class CheckinModalComponent extends CheckIOGeneric {

  @Input() open;
  @Input() param;
  @Output() closeEmitter = new EventEmitter();

  get currentHour(): number {
    return parseInt(this.time.substring(0, 2), 10);
  }

  get currentMinute(): number {
    return parseInt(this.time.substring(3, 5), 10);
  }

  constructor(public action: AppStoreAction, public jobService: JobService,
              public targetJob: TargetJobService) {
    super(jobService);
    this.initTime();
  }

  ngOnInit() {
    if (this.param) {
      let tHours = range(0, 24);
      let tMin = range(0, 60);
      this.hours = this.leftPad(tHours);
      this.minutes = this.leftPad(tMin);
    }
  }

  initTime() {
    this.time = moment().format('HH:mm');
  }

  submit(event) {
    event.preventDefault();

    let week = ParseOrganisationService.calculateWeek();

    let currentTimestamp = moment()
      .hour(this.currentHour)
      .minute(this.currentMinute)
      .valueOf();

    this.targetJob.jobStatus(this.param, 'checkedin', currentTimestamp, week, 1)
      .subscribe(res => {
        this.action.fetchGetInfo();
        this.closeEmitter.emit(undefined);
        this.hideScrollBar();
      });
  }

  closeModal(updated) {
    if (updated) {
      this.action.closeModalTiming(true);
    }
    this.action.closeModalTiming(false);
  }

  timeHandler(event, hourChanged: boolean) {

    let val = event.target.value;

    this.time = hourChanged ?
      `${val}:${this.time[3]}${this.time[4]}` :
      `${this.time[0]}${this.time[1]}:${val}`;

  }

  radioHandle(val) {
    if (val) {
      this.time = this.param.from;
      this.disabled = true;
    } else {
      this.initTime();
      this.disabled = false;
    }
  }

  isFifteenMinutesBeforeStart() {
    let diff = moment(this.param.from, 'HH:mm').diff(moment(), 'minutes');
    return diff >= 0 && diff < 15;
  }

  closeCheckInModal(e) {
    e.preventDefault();
    this.closeEmitter.emit();
    this.hideScrollBar();
  }

}

export const assessHtml = `
<section class="uk-modal uk-open assessModel" id="jobdecline" tabindex="-1" role="dialog" aria-labelledby="label-jobdecline" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog">
       
        <a style="cursor:pointer; z-index: 50; margin-top:5px" class="uk-modal-close uk-close"  (click)="closeAssessModal( $event ) "  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header">
          <header id="label-jobdecline" style="box-shadow:none">
            <h2>
                <a  class="arrow uk-modal-close " (click)="closeAssessModal( $event )"></a>
                {{'Assess_jobber' | translate}}
             </h2>
          </header>
        </div>
        <div class="modal-content">
          <form class="uk-form jobdecline">
            <div class="assessInform">
              <p> {{ 'assessment_question_teamwork'| translate}}<span class="jc-uname"> {{unitName}}</span>  {{'assessment_question_teamwork_ending'| translate}}</p>
              <p *ngIf='correctTime'>{{ 'Last_evaluation' | translate }} <span class="jc-lastevaluation">{{correctTime}}</span></p>
            </div>
            <div class="buttonBlock">
                   <button class="uk-button userrate opt rateuserval-4 uk-width-1-1 uk-margin-bottom" [ngClass]="{'hadrating': jobber.rating == 4 }" (click)="sendRating($event, 4);"  data-close="dismiss" data-dismiss="modal">
                    {{'assessment_answer_teamwork_4_With_pleasure' | translate}}
                   </button>
                   <button class="uk-button userrate opt rateuserval-4 uk-width-1-1 uk-margin-bottom" [ngClass]="{'hadrating': jobber.rating == 3 }" (click)="sendRating($event, 3); "  data-close="dismiss" data-dismiss="modal">
                    {{'assessment_answer_teamwork_3_Why_not' | translate}}
                   </button>
                   <button class="uk-button userrate opt rateuserval-4 uk-width-1-1 uk-margin-bottom" [ngClass]="{'hadrating': jobber.rating == 2 }" (click)="sendRating($event, 2); "  data-close="dismiss" data-dismiss="modal">
                    {{'assessment_answer_teamwork_2_Rather_not' | translate}}
                   </button>
                   <button class="uk-button userrate opt rateuserval-4 uk-width-1-1 uk-margin-bottom" [ngClass]="{'hadrating': jobber.rating == 1 }" (click)="sendRating($event, 1); "  data-close="dismiss" data-dismiss="modal">
                    {{'assessment_answer_teamwork_1_No_way' | translate}}
                    </button>
                   <button class="uk-button cancel uk-width-1-1 uk-margin-bottom uk-modal-close" (click)="closeAssessModal( $event )"  data-close="dismiss" data-dismiss="modal">
                    {{'assess_later' | translate}}
                   </button>
            </div>
          </form>
        </div>
        
      
      </div>
    </section>
    `;


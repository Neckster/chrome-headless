import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppStoreAction } from '../../../store/action';
import { TargetJobService } from '../../../services/job.service';
import { assessHtml } from './assess-modal.html';
import { select } from 'ng2-redux/lib/index';
import { JobService } from '../../../services/shared/JobService';

@Component({
  // The selector is what angular internally  uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'assess-modal',  // <controlling></controlling>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [ ],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [  ],
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [`
          header a.arrow:before{
            font-family: 'jclight', 'HelveticaNeue-Light', sans-serif;
            content: "\\2329";
            position: relative;
            top: 1px;
          }
        `],
  // Every Angular template is first compiled by the browser before
  // Angular runs it's compiler
  template: assessHtml
})

export class AssessModalComponent {
  @Input() open;
  @Input() unitName;
  @Input() jobber;
  @select() info$;
  @Output() closeEmitter = new EventEmitter();
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  private isHadrating = false;
  private correctTime;

  // TypeScript public modifiers
  constructor(public action: AppStoreAction,
              public targetJob: TargetJobService,
              public jobService: JobService) {
  }
  ngOnInit() {
    let ratingTime;
    if ( this.jobber.rating !== undefined ) {
      ratingTime = this.jobber.rating_time;
      let time = new Date( ratingTime * 1000 ),
          currDate = time.getDate(),
          currMonth = time.getMonth() + 1,
          currYear = time.getFullYear();
      this.correctTime = currMonth + '/' + currDate + '/' + currYear;
    }
    this.hideScrollBar();
  }
  sendRating(event, rating) {
    event.preventDefault();
    this.targetJob.jobAssess(this.jobber.webuser_id, rating).subscribe(res => {
    });
    this.closeEmitter.emit(undefined);
    this.action.fetchGetInfo();
  }
  closeAssessModal( e ) {
    e.preventDefault();
    this.closeEmitter.emit(undefined);
  }
  ngOnDestroy() {
    this.hideScrollBar();
  }
}

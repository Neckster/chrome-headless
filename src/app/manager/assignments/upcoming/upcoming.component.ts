import { Component }                from '@angular/core';

import * as moment                  from 'moment';
import { select }                   from 'ng2-redux/lib/index';
import { Maybe }                    from 'monet';
import * as R                       from 'ramda';

import { JobService }               from '../../../services/shared/JobService';
import { TargetJobService }         from '../../../services/job.service';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { Cache }                    from '../../../services/shared/cache.service';

import { TimestampPipe } from '../../../lib/pipes/timestamp';

import { upcomingHtml }             from './upcoming.html.ts';

import { RefuseModalComponent }     from '../refuse-modal/refuse-modal';
import { AssessModalComponent }     from '../assess-modal/assess-modal';


import { AppStoreAction }           from '../../../store/action';

import {
  is, not, and,
  prop, present, greaterEq }        from '../../../lib/curried';
import * as c                       from '../../../lib/curried';


@Component({
  selector: 'upcoming',
  directives: [ RefuseModalComponent, AssessModalComponent ],
  pipes: [ TimestampPipe ],
  styles: [`
    .jobinfo_head { clear: right; }
    .refuse-btn { margin: 2px 15px; }
  `],
  template: upcomingHtml
})

export class UpcomingAssComponent {

  @select() info$;
  @select() currentUnit$;
  @select(state => state.localisation) lang$: any;

  public refuseAss: any;
  public assessAss: any;
  public info: any;
  public currentUnit;
  public upcoming;
  public openRefuse = false;
  public openAssess = false;
  public unsubscribe = [];
  public lang;
  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public jobService: JobService, public cache: Cache,
              public targetJob: TargetJobService, public action: AppStoreAction) {

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      if (!info.manager)
        this.init();

    }));

    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
    }));

    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname)
        this.currentUnit = unit;
      this.init();
    }));
  }

  init() {

    Maybe.fromNull(this.info)
      .filter(info => c.and([
        (i) => c.prop('assignment')(i),
        (i) => c.greaterEq(c.prop('length')(c.prop('assignment')(i)))(1)
      ])(info))
      .bind(info => {

        const currentDate = moment(this.info.meta.time, 'x');

        const userId = this.info.webuser.webuser_id;

        const isoWeekday = currentDate.isoWeekday();
        const isoWeekTop = currentDate.isoWeek();
        const year = currentDate.format('YY');

        const jubbrWeek = ''.concat('' + year, '' + isoWeekTop, '' + isoWeekday);
        const isCurrentUser = is(userId);

        const processUpcoming = c.pipe(
          c.filter(and([
            (a) => isCurrentUser(prop('webuser_id')(a)),
            (a) => greaterEq(''.concat(prop('week')(a), prop('weekday')(a)))(jubbrWeek),
            (a) => prop('stat')(a) ? not(is(1)(prop('deleted')(prop('stat')(a)))) : true,
            (a) => prop('published')(a)
          ])),
          c.map(ass => {

            ass.staff = this.jobService.getStaff(ass, this.info);

            ass.tFrom = this.jobService.getRealTime(
              this.info.meta.time,
              ass.from,
              ('' + ass.week).slice(-2),
              ass.weekday,
              parseInt('20' + ('' + ass.week).substr(0, 2), 10));

            return ass;
          }),
          c.filter(ass => {
            return ass.weekday === moment(this.info.meta.time, ['x']).isoWeekday() ?
              (ass.tFrom - 15 * 60 * 1000 > this.info.meta.time ? true : false) : true;
          }),
          c.sort((a, b) => ((a.tFrom > b.tFrom) ? 1 : (b.tFrom > a.tFrom) ? -1 : 0))
        );

        this.upcoming = processUpcoming(this.info.assignment);

        let current = this.info.assignment
          .filter(and([
            (a) => isCurrentUser(prop('webuser_id')(a)),
            (a) => is(isoWeekday)(prop('weekday')(a)),
            (a) => is(+`16${moment(this.info.meta.time, ['x']).isoWeek()}`)(prop('week')(a))
          ]))
          .filter(ass => { return !ass.stat || !ass.stat.declined; });

        let calcedAssigns = this.jobService.calcAssignments(current, this.info.meta.time)
          .filter(ass => {
            return !ass.stat ||
              !ass.stat.hasOwnProperty('deleted') ||
              !ass.stat.hasOwnProperty('declined');
          });

        current = Object.assign([], calcedAssigns)
          .filter(ass => {
            return ass.status !== 'past' && ass.webuser_id !== undefined && ass.published === true;
          });

        let nextJob = this.jobService.isNextAssignment(current[0], current[1]);

        Array.prototype.unshift.apply(
            this.upcoming,
            current.slice(nextJob ? 2 : 1)
        );

        this.removeSameAssigns(this.upcoming, current, nextJob);
        this.upcoming = this.upcoming
          .filter((val, i, a) => a.indexOf(val) === i)
          .filter(ass => {
            return !nextJob || current[1].tFrom !== ass.tFrom;
          });

        this.upcoming.forEach((a, i) => {
          if (this.cache.isInArray(
              'assignments:upcoming:po',
              `${a.company_id}-${a.position_id}-${a.tFrom}`
            ))
            a.open = true;

          if (this.cache.isInArray(
              'assignments:upcoming:po_st',
              `${a.company_id}-${a.position_id}-${a.tFrom}`
            ))
            a.openIn = true;
        });
        return this.upcoming;
      });
  }

  /**
   * Find and remove same assigns
   * @param ass
   */
  removeSameAssigns(ass, currentAssigns, isNextJob) {
    let duplicates = [];
    for (let i of ass.slice(0)) {
      let count = 0;
      for (let key in ass.slice(0)) {
        if (ass[key]) {
          let j = ass[key];
          if (this.isTheSameAssign(j, i)) {
            count++;
            if (count > 1 && duplicates.indexOf(key) < 0) {
              duplicates.push(key);
            }
          }
        }
      }
    }

    for (let i = 0; i < (isNextJob ? 2 : 1); ++i) {
      let item = currentAssigns[i];
      if (item) {
        for (let key in ass.slice(0)) {
          if (ass[key]) {
            if (this.isTheSameAssign(item, ass[key]) && duplicates.indexOf(key) < 0) {
              duplicates.push(key);
            }
          }
        }
      }
    }

    for (let i = duplicates.length - 1; i >= 0; --i) {
      ass.splice(duplicates[i], 1);
    }
  }

  isTheSameAssign(j: any, i: any) {
    const pickAttrs = R.pick(['week', 'weekday', 'shift_idx', 'position_id']);
    return R.equals(pickAttrs(i), pickAttrs(j));
  }

  toggleCollapsible(ass, index) {
    if (this.upcoming[index].open) {
      this.cache.removeFromArray(
        'assignments:upcoming:po',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:upcoming:po',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    }
    this.upcoming[index].open = !this.upcoming[index].open;
  }

  toggleStaffItem(ass, index) {
    if (this.upcoming[index].openIn) {
      this.cache.removeFromArray(
        'assignments:upcoming:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    } else {
      this.cache.pushArray(
        'assignments:upcoming:po_st',
        `${ass.company_id}-${ass.position_id}-${ass.tFrom}`
      );
    }
    this.upcoming[index].openIn = !this.upcoming[index].openIn;
  }

  getD(ass) {
    return moment(ass.tFrom).lang(this.lang).format('ddd:D.MM').split(':');
  }

  getYear(ass) {
    // debugger
    const now = moment().year();
    const mom = moment(ass.tFrom).year();

    return now === mom ? '' : mom;
  }

  openRefuseM(ass) {
    this.refuseAss = ass;
    this.openRefuse = true;
  }

  openAssessM(ass) {
    this.assessAss = ass;
    this.openRefuse = true;
  }

  avaible(ass) {
    let week = ParseOrganisationService.calculateWeek();
    this.targetJob.avaible(1, ass, week).subscribe(res => {
      this.action.fetchGetInfo(week);
    });
  };

  getCheckInTime( time ) {
    if (!time) {
      return '-:-';
    }

    let t = time.split(':'),
        hours = +t[0],
        min = +t[1];
    if ( min < 15 ) {
        min = ( min - 15 ) + 60;
        hours--;
    } else {
      min -= 15;
    }
    return  ( hours > 9 ? hours : '0' + hours ) + ':' + ( min > 9 ? min : '0' + min );

  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe() );
  };

}

//noinspection TsLint
export const upcomingHtml = `
<refuse-modal (closeEmitter)="openRefuse = false" [param]="refuseAss" [open]="openRefuse"></refuse-modal>
<div *ngIf="!upcoming || !upcoming.length" class="uk-width-1-1 uk-container-center uk-margin-small  padded jc-job-today jc-job-today-none">
	<p class="text-center uk-margin-remove">{{'No_assignments_planned' | translate}}</p>
</div>
<template [ngIf]="upcoming">
  <li *ngFor="let ass of upcoming; let i = index" class="time-future jc-template-assignment-entry-future assignment jc-job uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item" id="jc-assignment-job_id-1628-4-3-131534">
	<div *ngIf="ass" class="uk-nestable-panel uk-clearfix assignment-head" [style.background]="ass.open ? '#eee':'#fff' " (click)="toggleCollapsible(ass, i)" style='cursor:pointer;'>
		<div class="icon large  blue uk-margin-right uk-margin-small-left" [ngClass]='{"arrow-down":ass.open, "arrow-right":!ass.open}'></div>
		<div class="uk-width-1-1 uk-float-right panel-body uk-grid uk-grid-collapse uk-flex uk-flex-middle ">
			<div class="uk-flex uk-flex-wrap uk-width-1-1 jobdateloc">
				<div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-3 jobdate">
					<span class=""> {{ ass.tFrom | timestamp:this.lang }} </span>
					<span class="jobdate_time"><br>
            <span class="jc-from">{{ass.from}}</span>–<span class="jc-to">{{ass.to}}</span>
					</span>
				</div>
				<div class="uk-width-large-3-10 uk-width-medium-2-10 uk-width-small-2-3 uk-flex-order-last-medium jobaction">
					<div class="uk-grid uk-grid-collapse">
						<div class="uk-width-1-1 uk-clearfix">
							<div class="declined uk-float-right uk-text-right">{{'refused' | translate}}</div>
							<div class="check-hint">
								<div class="checkinhint uk-float-right uk-text-right"><span *ngIf="!ass.stat || !ass.stat.declined">{{'Check_in' | translate}}</span> <span *ngIf="!ass.stat || !ass.stat.declined">{{'as_of' | translate}}</span>&nbsp;
                  <span *ngIf="!ass.stat || !ass.stat.declined" class="jc-checkinmin span-checkinmin">{{ getCheckInTime(ass.from) }}</span>
									<!--<a *ngIf="!ass.stat || !ass.stat.declined" style="display: block"
                     (click)="openRefuseM(ass);hideScrollBar()">{{'Refuse' | translate}}</a>-->
									<span *ngIf="ass.stat?.declined" style="color: red">
                  {{'refused' |  translate}}
                  </span>
									<button *ngIf="ass.stat && ass.stat.declined" class="uk-button uk-button-primary" style=" display: inherit; float: none;" (click)="avaible(ass)">
										{{'yet_available' | translate}}
									</button>
								</div>
								<div class="hadci"><span class="uk-visible-small">{{'in' | translate}}:</span><span class="uk-hidden-small">{{'Check_in' | translate}}:</span>
									<span class="jc-checkin span-checkin"></span>
								</div>
								<div class="needco">
									<div class="checkouthint uk-float-right"><span>{{'Check_out_until' | translate}}</span>&nbsp;<span class="jc-checkoutmax">08:00</span>
									</div>
								</div>
								<div class="hadco">
									<div><span class="uk-visible-small">{{'out' | translate}}:</span><span class="uk-hidden-small">{{'Check_out' | translate}}:</span>
										<span class="jc-checkout"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-1-1 uk-clearfix">
							<button type="button" class="uk-float-right uk-button uk-button-primary accept jc-click-jobaccept">
								{{'yet_available' | translate}}
							</button>
						</div>
					</div>
				</div>
				<div class="uk-width-large-5-10 uk-width-medium-5-10 uk-width-small-1-1 jobloc">
					<div class="uk-clearfix">
						<div class="uk-float-left">
							<span class="jc-cname">{{ass.cname}}</span>
							<br>
							<span class="jc-nname">{{ass.nname}}</span>&nbsp;– <span class="jc-pname">{{ass.pname}}</span>
						</div>
						<div *ngIf="(ass.info || ass.chef) && !ass.open" class="uk-float-right uk-margin-remove">
							<span class="isign" style="display:inline-block;margin-top:10px;">ⓘ</span>
						</div>
					</div>
				</div>
			</div>
			<!-- uk-flex-wrap -->
		</div>
		<!-- uk-grid  -->
	</div>

	<div *ngIf="ass"> </div>

	<ul *ngIf="ass && ass.open" class="uk-nestable-list jc-template-append-assignment-colleague" style='padding-bottom:25px'>

	  <!-- Refuse button -->
		<a *ngIf="ass && (!ass.stat || !ass.stat.declined)" style="" class="uk-align-right refuse-btn" (click)="openRefuseM(ass);hideScrollBar()">{{'Refuse' | translate}}</a>

    <div *ngIf="ass.info || ass.chef" class="uk-width-1-1 jobdetail">
      <div class="uk-width-1-1">
        <div *ngIf="ass.chef === 't'" class="jobinfo_head jc-have-info">
          <span>{{ 'Note_colon' | translate }}</span>
          <p class="uk-margin-top-remove">{{ 'Shift_Leader_Statement' | translate }}</p>
        </div>
        <div *ngIf="ass.info?.compiled || ass.info?.message" class="jobinfo">
          <span>{{'Message' | translate}}:</span>
          <div class="info-wrapper" [innerHTML]="ass.info?.compiled || ass.info?.message"></div>
        </div>
      </div>
    </div>

		<ul class="uk-nestable-item uk-nestable-nodrag uk-padding-remove">
			<ul *ngIf="ass && ass.staff && ass.staff.length" class="uk-width-1-1 uk-text-left uk-padding-remove">
				<div class="uk-form-label">{{'Staff' | translate}}</div>
				<ul class="uk-nestable-list uk-width-1-1 jc-template-append-assignment-position uk-padding-remove">

					<li class="uk-nestable-item uk-nestable-nodrag uk-parent jobcolleaguepos section" id="jc-assignment-colleague-job_id-1627-1-2-131499-position_id-131499">

            <!-- Position name -->
						<div *ngIf="ass.staff && ass.staff.length" class="uk-nestable-panel uk-clearfix" (click)="toggleStaffItem(ass, i)" [style.background]="ass.openIn ? '#fafafa':'#fff' " style='cursor:pointer'>
							<div class="icon large arrow-down blue uk-margin-right uk-margin-small-left" *ngIf="ass.openIn" style='cursor:pointer'></div>
							<div class="icon large arrow-right blue uk-margin-right uk-margin-small-left" *ngIf="!ass.openIn" style='cursor:pointer'></div>
							<div class="uk-grid uk-grid-collapse">
								<div class="jc-pname">{{ ass.pname }}</div>
							</div>
						</div>

            <!-- Staff collapsible -->
						<ul *ngIf="ass.openIn && ass.staff && ass.staff.length" class="uk-nestable-list jc-template-append-assignment-colleague">
							<li *ngFor="let jobber of ass.staff" class="uk-nestable-item uk-nestable-nodrag uid-131988">
								<assess-modal *ngIf="jobber.open" (closeEmitter)="jobber.open = false" [jobber]='jobber' [unitName]='jobber.uname'></assess-modal>
								<div class="uk-panel">
									<div class="jobstaffposname uk-clearfix">
										<div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
											<div class="uk-width-7-10">
												<span class="jc-uname">{{ jobber.uname }}</span>&nbsp;&nbsp;
												<span class="jc-from">{{ jobber.from }}</span>–<span class="jc-to">{{ jobber.to }}</span>
											</div>
											<div class="uk-width-3-10">
												<!--<button class="uk-button-link app-modal-userrate assessButton" (click)="jobber.open = true;hideScrollBar()" type='button'>{{'assess' | translate}}</button>-->
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>

					</li>
				</ul>
			</ul>
		</ul>
	</ul>
</li>
</template>

`;

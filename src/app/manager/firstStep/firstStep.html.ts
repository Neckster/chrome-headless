export const firstStepHTML = `
      <div  class="fistSteps">
           <div class="templateForJobbers">
              <div class="uk-width-1-1 bubblesWrap">
                 <div *ngFor="let b of [0,1,2]; let i = index" 
                      class="bubblesShow"
                      [ngClass]="{'active': b <= stepIndex}">
                 </div>        
              </div>
              
              <div class="stepsTitle" 
                    [ngClass]="{'title-position-one-two': stepIndex === 1 || stepIndex === 2, 
                        'title-position-three': stepIndex === 3}">
                   {{userHas[stepIndex].title | translate}}
              </div>
              
              <div *ngIf="userHas  && !isManager">
                <div *ngIf='stepIndex === 0' class="uk-width-1-1 stepsBody">
                   <profile [firstStep]="true" [submitForm]="userHas[0].submit" (correctSubmit)="nextStep($event, 0)"></profile>     
                </div>
                <div *ngIf='stepIndex === 1' class="uk-width-1-1 stepsBody">
                    <availability-controlling [secondStep]="true" [submitForm]="userHas[1].submit" (correctSubmit)="nextStep($event, 1)"></availability-controlling>            
                </div>
                <div *ngIf='stepIndex === 2' class="uk-width-1-1 stepsBody">
                   <profile  [thirdStep]="true" [submitForm]="userHas[2].submit" (correctSubmit)="nextStep($event, 2)"></profile>     
                </div>
              </div>
              
              <div *ngIf="userHas && isManager">
                <div *ngIf='stepIndex === 0' class="uk-width-1-1 stepsBody">
                   <add-complex-tree [firstStep]="true" [submitForm]="userHas[0].submit" (correctSubmit)="nextStep($event, 0)"></add-complex-tree>     
                </div>
                
                <div *ngIf='stepIndex === 1' class="uk-width-1-1 stepsBody">
                   <setup-organisation [secondStep]="true" [submitForm]="userHas[1].submit" (correctSubmit)="nextStep($event, 1)"></setup-organisation>     
                </div>
                
                <div *ngIf='stepIndex === 2' class="uk-width-large-6-10 uk-width-media-6-10 uk-width-small-1-1 stepsBody">
                   <create-jobber [opened]="true" [thirdStep]="true" [submitForm]="userHas[2].submit" [organisationStaff]="organisationStaff" (correctSubmit)="nextStep($event, 2)">>
                    </create-jobber>
                </div>
              </div>
              
              
              <div class="uk-width-1-1 buttonsWrapper">
                 <div class="uk-width-1-1 uk-margin-large-top stepsButtons">
                   <button *ngIf="stepIndex !== 0" 
                           (click)="prevStep($event)" 
                           class="stepsButton prev">
                      {{'Previous'| translate}}
                   </button>
                   <button (click)="submitSteps($event, stepIndex)" 
                            class="stepsButton next">
                      {{'Next'| translate}}
                   </button>
                 </div>       
              </div> 
              
           </div>
           <div *ngIf="false" class="templateForJobbers">
              
           </div>
      </div>
`;

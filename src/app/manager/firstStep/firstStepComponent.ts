import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ComponentFactory,
  ComponentResolver }                 from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot }                 from '@angular/router';

import { firstStepHTML }              from './firstStep.html';
import { UserService }                from '../../services/user.service';
import { JubbrFormValidator }         from '../../lib/validators/jubbr-validator';
import { AppStoreAction }             from '../../store/action';
import { select }                     from 'ng2-redux/lib/index';
import { ParseOrganisationService }   from '../../services/shared/parseOrganisation.service';
import { PositionService }            from '../../services/position.service';
import { JobService }                 from '../../services/shared/JobService';
import { Cache }                      from '../../services/shared/cache.service';
import { OnBoardingService }          from '../../services/onboardin.service';

interface IOnboardingStep {
  name: string;
  title: string;
  active: boolean;
  submit: boolean;
}


const jobberSteps: IOnboardingStep[] = [
  {
    name: 'profile',
    title: 'onboarding_enter_profile',
    active: true,
    submit: false
  },
  {
    name: 'availability',
    title: 'onboarding_enter_assignments',
    active: false,
    submit: false
  },
  {
    name: 'skills',
    title: 'onboarding_enter_capabilities',
    active: false,
    submit: false
  }
];
const managerSteps: IOnboardingStep[] = [
  {
    name: 'company',
    title: 'Enter_company',
    active: true,
    submit: false
  },
  {
    name: 'tree',
    title: 'onboarding_shift',
    active: false,
    submit: false
  },
  {
    name: 'addJobber',
    title: 'onboarding_invite',
    active: false,
    submit: false
  }
];

@Component({
  selector: 'frist-step',
  template: firstStepHTML,
  styles: [ require('./styles.less')],
  providers: [ OnBoardingService ]
})

export class FirstStepComponent {

  @select() info$: any;
  @select(state => state.showAddModal) modal$: any;
  @select() currentUnit$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  @select() userIsManager$;
  @select() profileSet$;

  public info;
  public week;
  public profileSet: boolean;
  private unsubscribe = [];
  private stepIndex: number = 0;
  private userHas: any;
  private isManager: boolean;
  private checkSubmit: any = false;

  constructor(public user: UserService,
              public router: Router,
              public action: AppStoreAction,
              public jobService: JobService,
              public cache: Cache,
              public positionService: PositionService,
              public onBoardingService: OnBoardingService
  ) {


    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      if (info && info.manager &&
          info.manager.organisation) {
        let id;
        info.manager.organisation.forEach(el => {
          if (el.type === 'company') {
            id = el.id.slice(1);
          }
        });
        this.action.fetchCurrentUnit(id);
      }
      if (info && info.webuser &&
          info.webuser.onboarding_process && !this.stepIndex) {
        this.stepIndex = info.webuser.onboarding_process; // if we have set onboarding before
      }
    }));


    this.unsubscribe.push(this.userIsManager$.subscribe(userIsManager => {
      this.userHas = userIsManager ? managerSteps : jobberSteps;
      this.isManager = userIsManager;
      if (this.cache.get('firstSteps') && this.userHas && this.userHas.length) {
        this.stepIndex = +this.cache.get('firstSteps');
        this.userHas = this.userHas.map( (el, index) => {
          el.active = index === this.stepIndex;
          return el;
        });
      }
    }));



    this.unsubscribe.push(this.profileSet$.subscribe( profileSet => {
      this.profileSet = profileSet;
    }));
  }

  ngOnInit() {
    this.action.fetchGetInfo();
    // check on correct work other components
  }

  ngOnChanges() {

  }

  submitSteps(e, step) {
    if (this.checkSubmit) {
      this.userHas[step].submit = 'isValid';
    }else {
      this.userHas[step].submit = true;
    }
  }

  nextStep(e, step) {
    if (e.action) {
      this.userHas[step].active = false;
      console.log('STEP');
      if (step === 2) {
        this.cache.removeKey('firstSteps');
        this.cache.removeKey('complexCompany');
        this.onBoardingService.changeOnBoardingStep(false, this.info.webuser.webuser_id)
          .subscribe(() => {
          this.action.fetchAuthStatus().subscribe(() => {
            this.action.profileSet(true);
            this.router.navigateByUrl('/news');
          });
        });
      } else {
        this.stepIndex++;
        this.cache.set('firstSteps', this.stepIndex);
        this.userHas[this.stepIndex].active = true;
        console.log(this.stepIndex);
        this.onBoardingService.changeOnBoardingStep(this.stepIndex, this.info.webuser.webuser_id)
          .subscribe((res: any) => {
           console.log('set ' + this.stepIndex + ' done');
          });
        this.action.fetchGetInfo();
      }
    }else {
      this.userHas[step].submit = 'isValid';
    }
  }

  prevStep(e) {
    this.userHas[this.stepIndex].active = false;
    this.stepIndex--;
    this.cache.set('firstSteps', this.stepIndex);
    this.userHas[this.stepIndex].active = true;
    this.userHas[this.stepIndex].submit = false;
    this.onBoardingService.changeOnBoardingStep(this.stepIndex, this.info.webuser.webuser_id)
      .subscribe((res: any) => {
        console.log('previous step: ' + this.stepIndex);
      });
  }

}

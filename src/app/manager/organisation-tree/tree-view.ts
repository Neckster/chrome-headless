import { Component, Input, ElementRef } from '@angular/core';
import { DirectoryView } from './directory-view/directory-view';
import { Directory } from './tree-view/directory';
import { orgStruct } from './organisation-struct.html';
import { DelModalComponent } from './delModal/del-modal.component';
import { UnitService } from '../../services/unit.service';
import { AppStoreAction } from '../../store/action';
import {
    DirectoryEventEmitter,
    DIRECTORY_ITEM_DROP,
    DIRECTORY_ITEM_DRAG,
    DIRECTORY_ITEM_MOVED,
    DIRECTORY_ITEM_ON_DRAG,
    DIRECTORY_ITEM_ON_MOVING
} from './directory-view/directory-event-emitter';
import { PositionService } from '../../services/position.service';
let dragula = require('dragula');

@Component({
  selector: 'my-tree-view',
  template: orgStruct,
  directives: [DirectoryView, DelModalComponent],
  viewProviders: [  ],

  styles: []
})
export class TreeViewBase {
  @Input() company: Array<Directory>;
  @Input() options: any;
  @Input() parentShift: any;
  @Input() info: any;
  @Input() secondStep;

  public directoryEmitter: DirectoryEventEmitter;
  public lvl = 0;
  directories: Array<Directory>;
  protected drag;
  private isChangeCompany;

  /**
   * Condition is moved unit and position
   *
   * @param targetDir
   * @param item
   * @returns {boolean}
   */
  public static isPositionMoving(targetDir: Directory, item: Directory): boolean {
    return (targetDir.type === 'unit' && item.type === 'position');
  }

  /**
   * Condition is moved unit/group/company
   *
   * @param targetDir
   * @param item
   * @returns {boolean}
   */
  public static isUnitMoving(targetDir: Directory, item: Directory): boolean {
    return (targetDir.type === 'group' || targetDir.type === 'company')
        && (item.type === 'unit' || item.type === 'group');
  }

  constructor(
      public unitService: UnitService,
      public action: AppStoreAction,
      private el: ElementRef,
      private positionService: PositionService
  ) {
    this.directoryEmitter = new DirectoryEventEmitter();

  }
  delUnit(id) {
    this.unitService.del(id).subscribe(delRes => {
      this.action.openDelModal(false, {});
      this.action.fetchGetInfo();
    });
  }
  ngOnChanges(changes) {
    if (changes.company) {
      this.isChangeCompany = true;
    } else {
      this.isChangeCompany = false;
    }
  }
  ngAfterViewChecked() {
    if (this.isChangeCompany) {
      if (this.drag) {
        this.drag.destroy();
      }
      let self = this;
      this.drag = dragula(this.getTreeItems(), {
        isContainer: function (el: Element) {
          return el.classList.contains('list-item') || el.classList.contains('drop-handler');
        },
        accepts: (el: Element, target: Element, source: Element, sibling: Element): boolean => {
          if (!el.contains(target) && !el.classList.contains('root-item')) {
            if (el.getAttribute('data-id')
                && target.getAttribute('data-directory-id')
                && el.getAttribute('data-id') !== target.getAttribute('data-directory-id')) {
              let item = this.getItemOnDirectoryList(el.getAttribute('data-id'), this.company);

              let targetDir = this.getItemOnDirectoryList(
                  target.getAttribute('data-directory-id'),
                  this.company
              );

              if ((TreeViewBase.isPositionMoving(targetDir as Directory, item as Directory)
                  || TreeViewBase.isUnitMoving(targetDir as Directory, item as Directory))
                  && !this.isInsideDirectory(item as Directory, targetDir as Directory)) {
                el.classList.add('accepted');
                el.classList.remove('disaccepted');

                this.directoryEmitter.broadcast(
                    DIRECTORY_ITEM_ON_DRAG,
                    targetDir as Directory,
                    item
                );
                this.directoryEmitter.broadcast(DIRECTORY_ITEM_ON_MOVING, item as Directory, {
                  target: targetDir,
                  lvl: target.getAttribute('lvl')
                });

                return true;
              }
            }
          }
          el.classList.remove('accepted');
          el.classList.add('disaccepted');
          return false;
        },
        moves: (el: Element, container: Element, handle: Element): boolean => {
          return handle.classList.contains('drag-handle');
        },
        invalid: function (el, handle) {
          return false; // don't prevent any drags from initiating by default
        },
        direction: 'vertical',
        copy: true,                       // elements are moved by default, not copied
        copySortSource: false,
        mirrorContainer: document.getElementById('organization-tree-drag-mirror'),
        revertOnSpill: true,
        removeOnSpill: true,
        ignoreInputTextSelection: false
      }).on('drag', (el: Element) => {
        el.classList.remove('ex-moved');
        this.getDropHandlers().forEach(function(dropHandler: Element){
          dropHandler.classList.remove('hidden');
        });
        this.getItemsInfo().forEach(function(infoContainer: Element){
          infoContainer.classList.add('on-out');
        });
      }).on('drop', (el: Element, target: Element, source: Element, sibling: Element) => {
        el.classList.add('ex-moved');
        el.classList.remove('accepted');

        if (el.getAttribute('data-id')
            && target.getAttribute('data-directory-id')
            && source.getAttribute('data-directory-id')) {
          let item = this.getItemOnDirectoryList(
              el.getAttribute('data-id'),
              this.company
          ) as Directory;

          let targetDir = this.getItemOnDirectoryList(
              target.getAttribute('data-directory-id'),
              this.company
          ) as Directory;
          let sourceDir = this.getItemOnDirectoryList(
              source.getAttribute('data-directory-id'),
              this.company
          ) as Directory;

          this.directoryEmitter.broadcast(DIRECTORY_ITEM_DROP, targetDir as Directory, item);
          this.directoryEmitter.broadcast(DIRECTORY_ITEM_DRAG, sourceDir as Directory, item);
          this.directoryEmitter.broadcast(DIRECTORY_ITEM_MOVED, item as Directory, {
            source: sourceDir,
            target: targetDir,
            lvl: target.getAttribute('lvl')
          });

          if (TreeViewBase.isPositionMoving(targetDir as Directory, item as Directory)) {
            this.positionService.putTreePosition(item.id.slice(1),
                targetDir.id.slice(1)).subscribe(data => {
              this.action.fetchGetInfo();
            });
          } else if (TreeViewBase.isUnitMoving(targetDir as Directory, item as Directory)) {
            this.positionService.putTreeUnit(targetDir.id.slice(1),
                item.id.slice(1)).subscribe(data => {
              this.action.fetchGetInfo();
            });
          }
        }
      }).on('dragend', function () {
        self.getDropHandlers().forEach(function(dropHandler: Element){
          dropHandler.classList.add('hidden');
        });
        self.getItemsInfo().forEach(function(infoContainer: Element){
          infoContainer.classList.add('on-out');
        });
      }).on('cancel', (el: Element, container: Element, source: Element) => {
        el.classList.remove('ex-moved');
        self.getDropHandlers().forEach(function(dropHandler: Element){
          dropHandler.classList.add('hidden');
        });
        self.getItemsInfo().forEach(function(infoContainer: Element){
          infoContainer.classList.add('on-out');
        });
      }).on('over', (el: HTMLElement, container: HTMLElement, source: HTMLElement) => {
        el.classList.add('ex-over');

        // Set correct level from current element
        let draggedItems = Array.from(el.getElementsByClassName('titleName'));
        let diff = parseInt(container.getAttribute('lvl'), 10)
          - parseInt(draggedItems[0].getAttribute('lvl'), 10);

        draggedItems.forEach((target: HTMLElement) => {
          let lvl = parseInt(target.getAttribute('lvl'), 10);
          target.style.paddingLeft =
            ((diff + lvl + 1) * 15) + 'px';
        });
      }).on('out', (el: Element, container: Element, source: Element) => {
        el.classList.remove('ex-over');
      });
      this.getTreeItems().forEach(function(dir: HTMLElement){
        let children: HTMLElement[] =
            Array.from(dir.getElementsByClassName('titleName')) as HTMLElement[];
        for (let el of children) {
          el.style.paddingLeft = ((parseInt(dir.getAttribute('lvl'), 10) + 1) * 15) + 'px';
        }
      });
      this.isChangeCompany = false;
    }
  }

  /**
   * Search function on directories list
   *
   * @param wanted
   * @param directories
   * @returns {any}
   */
  private getItemOnDirectoryList(wanted: string, directories: Array<Directory>): Directory|boolean {
    for (let dir of directories) {
      if (dir.id === wanted) {
        return dir;
      }
    }
    // Recursive search on subdirs
    for (let dir of directories) {
      let result = this.getItemOnDirectory(wanted, dir);
      if (result) {
        return result;
      }
    }
    return false;
  }

  /**
   * Recursive search function
   *
   * @param wanted
   * @param directory
   * @returns {any}
   */
  private getItemOnDirectory(wanted: string, directory: Directory): Directory|boolean {
    // Find In files
    if (directory.files) {
      for (let file of directory.files) {
        if (file.id === wanted) {
          return file;
        }
      }
    }
    // Find in directories
    if (directory.directories) {
      for (let dir of directory.directories) {
        if (dir.id === wanted) {
          return dir;
        }
      }
      // Find inside directories
      for (let dir of directory.directories) {
        let result = this.getItemOnDirectory(wanted, dir);
        if (result) {
          return result;
        }
      }
    }
    return false;
  }

  private isInsideDirectory(directory: Directory, current: Directory): boolean {
    return !!this.getItemOnDirectory(current.id, directory);
  }

  private getTreeItems(): HTMLElement[] {
    return this.getChildren('list-item') as HTMLElement[];
  }

  private getItemsInfo(): HTMLElement[] {
    return this.getChildren('item-info') as HTMLElement[];
  }

  private getDropHandlers(): HTMLElement[] {
    return this.getChildren('drop-handler') as HTMLElement[];
  }

  private getChildren(className): any[] {
    return Array.from(this.el.nativeElement.getElementsByClassName(className));
  }
}

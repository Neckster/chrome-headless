import {
  Component,
  Input,
  Output,
  EventEmitter }                    from '@angular/core';
import { UnitService }              from '../../../services/unit.service';
import { AppStoreAction }           from '../../../store/action';
import { shiftsModalHtml }          from './shifts-modal.html';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import moment                       = require('moment/moment');
import { JobService }               from '../../../services/shared/JobService';

@Component({
  selector: 'shifts-modal',
  template: shiftsModalHtml
})

export class ShiftsModalComponent {

  @Input() opened;
  @Output() closeEmitter = new EventEmitter();

  public state;
  public shift;
  public hours = [];
  public minutes = [];
  public minutesF = [];
  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public action: AppStoreAction, public jobService: JobService,
              public unitService: UnitService) {

      this.state = {from: {}, to: {}};
  }
  ngOnInit() {
  }
  changeHandle(event, flag) {
    if (flag) {
      this.state.from.h = event.target.value;
    } else {
      this.state.to.h = event.target.value;
    }
    let sysShift = ParseOrganisationService.sys.shift[ParseOrganisationService.unitId.shiftIndex];
    this.setRules(sysShift);
  }
  changeHandleM(event, flag) {
    if (flag) {
      this.state.from.m = event.target.value;
    } else {
      this.state.to.m = event.target.value;
    }
    let sysShift = ParseOrganisationService.sys.shift[ParseOrganisationService.unitId.shiftIndex];
    this.setRules(sysShift);
  }
  ngOnChanges() {
    if (ParseOrganisationService.unitId) {
        this.shift = ParseOrganisationService.unitId.shift;
      this.state = {
        from: {h: this.shift.from.split(':')[0], m: this.shift.from.split(':')[1]},
        to: {h: this.shift.to.split(':')[0], m: this.shift.to.split(':')[1]},
        parent: ParseOrganisationService.unitId.parent
      };
      let sysShift = ParseOrganisationService.sys.shift[ParseOrganisationService.unitId.shiftIndex];
      this.setRules(sysShift);
    } else {
      this.state = {from: {}, to: {}};
    }
  }
  calcMinutes(type) {
    let sysShift = ParseOrganisationService.sys.shift[ParseOrganisationService.unitId.shiftIndex];
    if (this.state.from.h === this.shift.hoursF[0]) {
      let sysM = +sysShift.from;
      sysM = +`${sysM[0]}${sysM[1]}`;
      let min = [];
      while (sysM < 60) {
        min.push(sysM);
        sysM++;
      }
      this.minutesF = min;
    } else if (this.state.from.h === this.shift.hoursF[6]) {
    } else {
      let start = 0;
      let mF = [];
      while (start < 60) {
        mF.push(start);
        start++;
      }
      this.minutesF = mF;
    }
  }
  setRules(sysShift) {
      // From
      let start = moment(sysShift.from, 'HH:mm').subtract(3, 'hours');
      let end = moment(sysShift.from, 'HH:mm').add(3, 'hours');
      let sysMF = moment(sysShift.from, 'HH:mm').format('mm');
      let sysMT = moment(sysShift.to, 'HH:mm').format('mm');
      let hoursF = [];
      let minutesF = [];
      while (start.format('HH') !== end.format('HH')) {
        hoursF.push(start.format('HH'));
        start.add(1, 'hours');
      }
     hoursF.push(end.format('HH'));
     if ((+this.state.from.h) === (+hoursF[6])) {
       let i = 0;
       minutesF.push(0);
     } else {
       let i = 0;
       while (i < 60) {
         minutesF.push(i);
         i++;
       }
     }
    // TO
    start = moment(sysShift.to, 'HH:mm').subtract(3, 'hours');
    end = moment(sysShift.to, 'HH:mm').add(3, 'hours');
    let minutesT = [];
    let hoursT = [];
    while (start.format('HH') !== end.format('HH')) {
      hoursT.push(start.format('HH'));
      start.add(1, 'hours');
    }
    hoursT.push(end.format('HH'));
    if ((+this.state.to.h) !== (+hoursT[6])) {
      let i = 0;
      while (i <= 59) {
        minutesT.push(i);
        i++;
      }
    } else {
        minutesT.push(0);
    }
    // Init
    this.shift = Object.assign({}, this.shift,
      {
        hoursF : hoursF,
        minutesF: this.niceLook(minutesF),
        hoursT: hoursT,
        minutesT: this.niceLook(minutesT)
      });
  }
  save(e) {
    let a = ParseOrganisationService.unitId;
    e.preventDefault();
    this.closeEmitter.emit({state: this.state, unitId: ParseOrganisationService.unitId});
    this.hideScrollBar();
  }
  closeShiftModal(e) {
    e.preventDefault();
    this.closeEmitter.emit();
    this.hideScrollBar();
  }
  niceLook(arr) {
    return arr.map(el => {
      return `${el}`.length === 1 ? `0${el}` : el;
    });
  }
  selectedM(m, flag) {
    return flag ?
      +(this.state.from.m) === (+m) :
      +(this.state.to.m) === (+m);
  }
  selectedH(h, flag) {
    return flag ?
      +(this.state.from.h) === (+h) :
      +(this.state.to.h) === (+h);
  }
}

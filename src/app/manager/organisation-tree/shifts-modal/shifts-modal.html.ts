//noinspection TsLint
export const shiftsModalHtml = `
<div *ngIf="opened">
  <section class="uk-modal uk-open" id="timepicker" tabindex="-1" role="dialog" aria-labelledby="label-timepicker" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog">
        <a (click)="closeShiftModal($event)" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header">
          <!--<header id="label-timepicker">-->
            <h2>
            <a (click)="closeShiftModal($event)" class="uk-modal-close"><!-- mobile-back-button --></a>{{'Choose_time_for' | translate}} {{state.parent.text || state.parent.name}}<span class="jc-timepickername"></span></h2>
          <!--</header>-->
        </div>
        <div class="modal-content">
          <form class="uk-form timepicker" ngNoForm>
            <div class="uk-grid uk-grid-collapse uk-text-left">
              <div id="timepickertime" class="timerangeexact uk-width-1-1">
                <!-- filled by js -->
              <div class="timerangeexact uk-grid uk-grid-collapse">
      <div class="timefrom"><div class="timedialexact">
      <div class="uk-grid uk-grid uk-grid-collapse">
        <div class="hh">
          <div class="select-wrapper uk-width-1-1 ws-success">
            <select [(ngModel)]="state.from.h" class="select timehh user-success" (change)="changeHandle($event, true)">
            <option *ngFor="let h of shift.hoursF;let i = index" [selected]="selectedH(h, true)">{{h}}</option>
            </select>
          </div>
        </div>
        <div class="dial-divider uk-text-center">:</div>
        <div class="mm">
          <div class="select-wrapper uk-width-1-1 ws-success">
            <select [(ngModel)]="state.from.m" class="select timemm user-success" (change)="changeHandleM($event, true)">
              <!-- filled by js -->
              <option *ngFor="let m of shift.minutesF;let i = index" [selected]="selectedM(m, true)">{{m}}</option>
            </select>
          </div>
        </div>
      </div>
    </div></div>
      <div class="time-divider uk-text-center">‐</div>
      <div class="timeto"><div class="timedialexact">
      <div class="uk-grid uk-grid uk-grid-collapse">
        <div class="hh">
          <div class="select-wrapper uk-width-1-1 ws-success">
            <select [(ngModel)]="state.to.h" class="select timehh user-success" (change)="changeHandle($event, false)">
              <option *ngFor="let h of shift.hoursT;let i = index" [selected]="selectedH(h, false)">{{h}}</option>
            </select>
          </div>
        </div>
        <div class="dial-divider uk-text-center">:</div>
        <div class="mm">
          <div class="select-wrapper uk-width-1-1 ws-success">
            <select [(ngModel)]="state.to.m" class="select timemm user-success" (change)="changeHandleM($event, false)">
              <!-- filled by js -->
              <option *ngFor="let m of shift.minutesT;let i = index" [selected]="selectedM(m, false)">{{m}}</option>
            </select>
          </div>
        </div>
      </div>
    </div></div>
    </div></div>
              <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-grid uk-grid-small">
                  <div class="uk-width-1-2 uk-margin-bottom">
                    <button (click)="save($event)" type="submit" class="uk-button uk-button-large ok uk-width-1-1">{{'button_OK' |translate}}</button>
                  </div>
                  <div class="uk-width-1-2 uk-margin-bottom">
                    <button (click)="closeShiftModal($event); " type="button" class="uk-button uk-button-large uk-modal-close cancel uk-width-1-1">{{'button_Cancel' | translate}}</button>
                  </div>
                </div>
              </div>
              <!-- uk-width-1-1 uk-margin-large-bottom -->
            </div></form>
          

        </div>
      </div>
  </section>
</div>
`;

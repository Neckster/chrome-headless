//noinspection TsLint
export const delModalHtml = `
<section *ngIf="data && data.show" class="uk-modal uk-open" id="unitdel" tabindex="-1" role="dialog" aria-labelledby="label-unitdel" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog">
        <a (click)="close()" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!--icon-->
        </a>
        <div class="uk-modal-header">
            <h2><a (click)="close()" class="uk-modal-close"><!--icon--></a>
              <span *ngIf='data.data.type === "unit" ' class="edit type unit">{{'modal_header_Deactivate_Unit' | translate}} </span>
              <span *ngIf='data.data.type === "group" ' class="edit type unit">{{'modal_header_Deactivate_Group' | translate}} </span>
              <span *ngIf='data.data.type === "position" ' class="edit type unit">{{'Deactivate_position' | translate}} </span>
            </h2>
        </div>
        <div class="modal-content">
          <form class="uk-form unitdel">
            <div class="uk-grid uk-grid-small uk-text-left">
              <div class="uk-width-1-1 uk-margin">
                <p class="jc-confirm">
                  <span class="edit type unit">{{data.data.type}}</span>
                  <span class="edit nname">{{data.data.text || data.data.name}}</span> <span>{{'deactivate'|translate}}.</span></p>
              </div>
              <div class="uk-form-row uk-width-1-1">
                <input class="uk-width-1-1" type="hidden" id="unitdelunit_id" value="150676" name="unit_id">
              </div>
              <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-grid uk-grid-small">
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button (click)="delete();$event.preventDefault(true);" type="submit" class="uk-button delete uk-button-large  uk-width-1-1">{{'deactivate'|translate}}</button>
                  </div>
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button (click)="close();$event.preventDefault(true);" class="uk-button uk-modal-close uk-button-large cancel uk-width-1-1">{{'button_Cancel'|translate}}</button>
                  </div>
                </div>
              </div>
              <!--uk-width-1-1 uk-margin-large-bottom-->
            </div>
            <!--uk-grid --></form>
          

        </div>
      </div>
    </section>
    `;

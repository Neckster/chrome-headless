import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgRedux, select } from 'ng2-redux';
import { delModalHtml } from './del-modal.html';
import moment = require('moment/moment');
import * as _ from 'lodash';
import { AppStoreAction } from '../../../store/action';
import { JobService } from '../../../services/shared/JobService';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'del-modal',  // <select-unit></select-unit>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [],
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: delModalHtml
})

export class DelModalComponent {
  // Set our default values
  @Output() closeEmitter = new EventEmitter();
  @select(state => state.delData) delData$;

  public data: any;
  public hideScrollBar = this.jobService.hideScrollBar;

  // TypeScript public modifiers
  constructor(public action: AppStoreAction, public jobService: JobService) {
    this.delData$.subscribe(data => {
      this.data = data;
    });
  }
  close() {
    this.action.openDelModal(false, {});
    this.hideScrollBar();
  }
  delete() {
    this.closeEmitter.emit(this.data.data.id.slice(1));
    this.hideScrollBar();
  }
  ngOnChanges() {
  }
}

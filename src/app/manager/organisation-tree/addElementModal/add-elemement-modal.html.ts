//noinspection TsLint
export const addModalHtml = `
<section class="uk-modal uk-open" id="unitadd" tabindex="-1" role="dialog" aria-labelledby="label-unitadd" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal" (click)="closeEmitter.emit();hideScrollBar()">
          <!--icon-->
        </a>
        <div class="uk-modal-header">
          <!--<header id="label-unitadd">-->
            <h2><a (click)="closeEmitter.emit();hideScrollBar()" class="uk-modal-close"><!--icon--></a>
              <span *ngIf="state.parent.type === 'unit'"> {{'Add_Position' | translate}} {{state.parent.dir.name || state.parent.dir.text}}</span>
              <span *ngIf="state.parent.type !== 'unit'"> {{'Add_Unit' | translate}} {{state.parent.dir.name || state.parent.dir.text}}</span>
            </h2>
          <!--</header>-->
        </div>
        <div class="modal-content">
          <form class="uk-form uk-form-horizontal unitadd" ngNoForm>
            <div class="uk-grid uk-grid-collapse uk-text-left">
              
              <div *ngIf="state.parent.type !== 'unit'"  class="uk-form-row uk-width-1-1 uk-margin">
                <div class="uk-grid uk-grid-collapse">
                  <label class="uk-form-label" for="unitaddnname">{{'Name' | translate}}</label>
                  <input [(ngModel)]="state.name" type="text" class="uk-width-small-1-1 uk-width-medium-6-10" id="unitaddnname" maxlength="32" required="">
                </div>
              </div>
              
              <div *ngIf="state.parent.type === 'unit'"  class="uk-form-row uk-width-2-1 uk-margin">
                <div class="uk-grid uk-grid-collapse">
                  <input style="width: 100%" [(ngModel)]="state.name" type="text" placeholder="{{'Position' | translate}}" class="uk-width-small-4-4 uk-width-medium-6-10" id="unitaddnname" maxlength="32" required="">
                </div>
              </div>
              
              <div *ngIf="state.parent.type !== 'unit'" class="uk-form-row uk-width-1-1">
                <div class="uk-grid uk-grid-collapse">
                  <label class="uk-form-label" for="unitaddunittypeunit">
                    <input (click)="state.type = 'unit'" [checked]="'unit' == state.type" type="radio" id="unitaddunittypeunit" name="type" value="unit" checked>
                    <span>{{'Unit' | translate}}</span>
                  </label>
                  <p class="uk-width-small-1-1 uk-width-medium-6-10">{{'remark_unit_contains_positions' | translate}}</p>
                </div>
              </div>
              <div *ngIf="state.parent.type !== 'unit'" class="uk-form-row uk-width-1-1 uk-margin-top">
                <div class="uk-grid uk-grid-collapse">
                  <label class="uk-form-label" for="unitaddunittypegroup">
                    <input (click)="state.type = 'group'" [checked]="'group' == state.type" type="radio" id="unitaddunittypegroup" name="type" value="group">
                    <span>{{'Group' | translate}}</span>
                  </label>
                  <p class="uk-width-small-1-1 uk-width-medium-6-10">{{'remark_group_is_shallow' | translate}}</p>
                </div>
              </div>
              <div class="hidden uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
                <input type="hidden" id="unitaddparent" value="150170">
                <input type="hidden" id="unitaddcompany_id" value="150039">
              </div>
              <div class="uk-width-1-1 uk-margin-large-top">
                <div class="uk-grid uk-grid-small">
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button type="submit" class="uk-button ok uk-button-large uk-width-1-1" (click)="save();$event.preventDefault(true);hideScrollBar()">{{'add' | translate}}</button>
                  </div>
                  <div class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                    <button class="uk-button uk-modal-close uk-button-large cancel uk-width-1-1" (click)="closeEmitter.emit();$event.preventDefault(true);hideScrollBar()">{{'button_Cancel' | translate}}</button>
                  </div>
                </div>
              </div>
              <!--uk-width-1-1 uk-margin-large-bottom-->
            </div>
            
            <!--UK-GRID--></form>
          

        </div>
        <!--unitadd-->
      </div>
      <!--uk-modal-dialog-->
    </section>
`;

import {
  Component,
  Input,
  Output,
  EventEmitter }                    from '@angular/core';
import { UnitService }              from '../../../services/unit.service';
import { AppStoreAction }           from '../../../store/action';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { JobService }               from '../../../services/shared/JobService';
import { addModalHtml }             from './add-elemement-modal.html';

@Component({
  selector: 'add-element-modal',
  template: addModalHtml
})

export class AddElementModalComponent {

  @Input() opened;
  @Output() closeEmitter = new EventEmitter();

  public state;
  public  shift;
  public hideScrollBar = this.jobService.hideScrollBar;

  constructor(public action: AppStoreAction, public unitService: UnitService,
              public jobService: JobService) {

    this.state = {type: 'unit'};
    let data = ParseOrganisationService.addModalData;
    this.state.parent = data;
  }
  ngOnInit() {
    let data = ParseOrganisationService.addModalData;
    this.state.parent = data;
  }
  save() {
    this.closeEmitter.emit(this.state);
  }
}

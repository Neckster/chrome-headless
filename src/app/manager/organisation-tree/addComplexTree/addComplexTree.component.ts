import { Component,
  Input, Output }                   from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators }                from '@angular/forms';
import { JubbrFormValidator }       from '../../../lib/validators/jubbr-validator';
import { NgRedux, select }          from 'ng2-redux';
import { AppStoreAction }           from '../../../store/action';
import { EmitService }              from '../../../services/emit.service';
import { Cache }                    from '../../../services/shared/cache.service';
import { JobService }               from '../../../services/shared/JobService';
import * as R                       from 'ramda';
import { Observable }               from 'rxjs/Rx';
import { RootState }                from '../../../store/reducer';
import { AddComplexTreeHTML }       from './addComplexTree.html';
import { EventEmitter }             from '@angular/router-deprecated/src/facade/async';
import { OnBoardingService }        from '../../../services/onboardin.service';

const noop = R.always(undefined);


@Component({
  selector: 'add-complex-tree',
  directives: [],
  pipes: [ ],
  styles: [require('./style.less')],
  template: AddComplexTreeHTML
})
export class AddComplexTree {

  @select() info$;

  @Input() firstStep;
  @Input() thirdStep;
  @Input() submitForm;
  @Output() correctSubmit = new EventEmitter(true);


  public unsubscribe = [];
  public showNewPosition: boolean = false;
  private addComplexCompanyControllers: FormGroup;
  private companyDanger: boolean = false;
  private unitDanger: boolean = false;
  private positionDanger: boolean = false;
  private positionSecondDanger: boolean = false;
  private info;
  private id;
  private defaultData;

  constructor(private action: AppStoreAction, private emitter: EmitService,
              private fb: FormBuilder,
              private cache: Cache, public jobService: JobService,
              private ngRedux: NgRedux<RootState>,
              public onBoardingService: OnBoardingService) {
    this.defaultData = {
      company: undefined,
      unit: undefined,
      position: []
    };
    this.addComplexCompanyControllers = this.fb.group({
      company: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32),
          JubbrFormValidator.trimmed as any
        ])
      ],
      unit: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32),
          JubbrFormValidator.trimmed as any
        ])
      ],
      position: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32),
          JubbrFormValidator.trimmed as any
        ])
      ],
      positionSecond: [
        '',
        Validators.compose([
          Validators.minLength(2),
          Validators.maxLength(32),
          JubbrFormValidator.trimmed as any
        ])
      ]
    });
    this.unsubscribe.push(this.info$.subscribe(info => {
      if (info) {
        this.info = info;
        let firstPositionSet = false;
        if (info.manager &&
            info.manager.organisation &&
            info.manager.organisation.length &&
            !this.cache.get('firstSteps:complexCompany')) {
          info.manager.organisation.forEach(el => {
            switch (el.type) {
              case 'company':
                this.id = el.data.company_id;
                this.addComplexCompanyControllers.patchValue({company: el.text});
                break;
              case 'unit':
                this.addComplexCompanyControllers.patchValue({unit: el.text});
                break;
              default:
                if (!firstPositionSet) {
                  firstPositionSet = true;
                  this.addComplexCompanyControllers.patchValue({position: el.text});
                } else {
                  this.addComplexCompanyControllers.patchValue({positionSecond: el.text});
                }
            }
          });
        }
        this.initDefaultValue(info, firstPositionSet);
      }
    }));
  }

  private initDefaultValue(info, firstPositionSet: boolean) {
    if (info.manager &&
      info.manager.organisation &&
      info.manager.organisation.length) {
      info.manager.organisation.forEach(el => {
        switch (el.type) {
          case 'company':
            this.id = el.data.company_id;
            this.defaultData.company = el.data.company_id;
            break;
          case 'unit':
            this.defaultData.unit = el.id;
            break;
          case 'position':
            if (!firstPositionSet) {
              firstPositionSet = true;
              this.defaultData.position = [el.id];
            } else {
              this.defaultData.position.push(el.id);
              this.showNewPosition = true;
            }
            break;
          default: ;
        }
      });
    }
    return firstPositionSet;
  }
  /* tslint:disable */
  ngOnInit() {

    if (this.cache.get('firstSteps:complexCompany')) {
      let val = JSON.parse(this.cache.get('firstSteps:complexCompany'));
      this.addComplexCompanyControllers.patchValue(val);
    }


    this.addComplexCompanyControllers.controls['company'].valueChanges.subscribe(el => {
      this.companyDanger = el.length > 2 && el.length < 32 ?
            false : true;
    });

    this.addComplexCompanyControllers.controls['unit'].valueChanges.subscribe(el => {
      this.unitDanger = el.length > 2 && el.length < 32 ?
            false : true;
    });

    this.addComplexCompanyControllers.controls['position'].valueChanges.subscribe(el => {
      this.positionDanger = el.length > 2 && el.length < 32 ?
              false : true;
    });

    this.addComplexCompanyControllers.controls['positionSecond'].valueChanges.subscribe(el => {
      this.positionSecondDanger = el.length > 2 && el.length < 32 ?
              false : true;
    });
  }
  /* tslint:disable */
  ngOnChanges(change) {
    if (this.submitForm) {
      this.sumbitForm();
    }
  }


  addPosition(e) {
    e.preventDefault();
    this.showNewPosition = true;
  }


  sumbitForm() {
   if(this.addComplexCompanyControllers.valid) {
     let sendObj = this.addComplexCompanyControllers.value;
     this.cache.set('firstSteps:complexCompany', JSON.stringify(sendObj));

     var positions = [{
       position_id: this.defaultData.position[0] ? +this.defaultData.position[0].substring(1) : undefined,
       position_name: sendObj.position
     }];
     if (sendObj.positionSecond) {
       positions.push({
         position_id: this.defaultData.position[1] ? +this.defaultData.position[1].substring(1) : undefined,
         position_name: sendObj.positionSecond
       });
     }
     this.onBoardingService.changeDefaultCompanyData({
       company_id: this.defaultData.company ? this.defaultData.company : undefined,
       company_name: sendObj.company
     }, {
       unit_id: this.defaultData.unit ? +this.defaultData.unit.substring(1) : undefined,
       unit_name: sendObj.unit
     }, positions).subscribe(data => {
       console.log("Data", data);
       this.correctSubmit.emit({ // should be delete after service will be ready
         action: true
       });
     });

     // this.SERVICE?.sendComplexCompanyTree(Object.assign({}, sendObj, {company_id:id })).subscribe(() => {
     //   this.correctSubmit.emit({
     //     action: true
     //   })
     // });



   }else {
      let form = this.addComplexCompanyControllers.controls;

     if (!form['company'].valid)  this.companyDanger = true;

     if (!form['unit'].valid) this.unitDanger = true;

     if (!form['position'].valid) this.positionDanger = true;

     if (this.showNewPosition && !form['positionSecond'].valid) this.positionSecondDanger = true;

     this.correctSubmit.emit({
       action: false
     })
   }
  }
  /* tslint:disable */
  ngOnDestroy() {

  }

  turnOffValidation() {
    this.companyDanger = false;
    this.unitDanger = false;
    this.positionDanger = false;
    this.positionSecondDanger = false;
  }


}

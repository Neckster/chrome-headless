export const AddComplexTreeHTML = `
      <div *ngIf="true" class="uk-grid" [formGroup]="addComplexCompanyControllers" >
          <div class="uk-form addComplexCompanyBody uk-width-large-4-10 uk-width-medium-6-10 uk-width-small-1-10">
            <div class="company-block">
              <i class="fourBubbles uk-width-small-1-10"></i>
              <input  name="company"
                      type="text"
                      formControlName="company"
                      [ngClass]="{'uk-form-danger' : (addComplexCompanyControllers.controls.company.dirty &&
                                          !addComplexCompanyControllers.controls.company.valid) || companyDanger}"
                      class="uk-width-9-10"
                      placeholder="{{'Enter_company' | translate}} (AGG, GmbH)"
                      id="company_structure">
            </div>
            <p>{{'onboarding_set_unit'| translate}}</p>
            <div class="unit-block uk-margin-bottom">
              <i class="twoBubbles uk-width-small-1-10"></i>
              <input  name="unit"
                      type="text"
                      formControlName="unit"
                      [ngClass]="{'uk-form-danger' : (addComplexCompanyControllers.controls.unit.dirty &&
                                 !addComplexCompanyControllers.controls.unit.valid ) || unitDanger}"
                      class="uk-width-9-10"
                      placeholder="{{'placeholder_Unit_name' | translate}}"
                      id="unit_structure">
            </div>
            <div class="position-block uk-margin-bottom">
              <i class="oneBubbles uk-width-small-1-10" ></i>
              <input  name="position"
                       class="uk-width-9-10"
                       [ngClass]="{'uk-form-danger' : (addComplexCompanyControllers.controls.position.dirty &&
                                   !addComplexCompanyControllers.controls.position.valid) || positionDanger}"
                      type="text"
                      formControlName="position"
                      placeholder="{{'placeholder_Position_name' | translate}}"
                      id="nextWeek">
               <a *ngIf='!showNewPosition'
                  class='uk-width-1-1 uk-float-left uk-margin-large-left uk-margin-top uk-margin-bottom' 
                  (click)="addPosition($event)">
                  + {{'Position'|translate}}
               </a>
            </div>
            <div *ngIf='showNewPosition'
                 class="position-block uk-margin-bottom" >
              <i class="oneBubbles uk-width-small-1-10" ></i>
              <input  name="positionSecond"
                      type="text"
                      [ngClass]="{'uk-form-danger' : (addComplexCompanyControllers.controls.positionSecond.dirty &&
                                  !addComplexCompanyControllers.controls.positionSecond.valid) || positionSecondDanger}"
                      formControlName="positionSecond"
                      class="uk-width-9-10"
                      placeholder="{{'placeholder_Position_name' | translate}}"
                      id="position-structure-second">
            </div>
          </div>
      </div>
`;

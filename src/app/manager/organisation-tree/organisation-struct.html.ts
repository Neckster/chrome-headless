export const orgStruct =  `
 <del-modal (closeEmitter)="delUnit($event)"></del-modal>
  <!--<treeview [lvl]="lvl" [info]="info" [child]="false" [directories]="company" [options]="options" [parentShift]="parentShift"></treeview>-->
<div class="uk-text-left">
  <div id="organization-tree-drag-mirror" style="display: none;"></div>
    <ul class="treeViewMain uk-nav" *ngIf="parentShift" data-directory-id="">
        <li *ngFor="let item of company" [attr.data-id]="item.id">
            <directory-view [secondStep]="secondStep" [lvl]="lvl" 
                [info]="info" [child]="false" [options]="options" 
                [parentShift]="parentShift" [parentEmitter]="directoryEmitter" 
                [directory]="item"></directory-view>
        </li>
    </ul>
</div>
  `;

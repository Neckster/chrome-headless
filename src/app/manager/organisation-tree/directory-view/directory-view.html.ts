// noinspection TsLint
export const directoryViewHtml =`
<div class="direcory-view">
    <add-element-modal *ngIf="addModalOpened" [opened]="addModalOpened"
                       (closeEmitter)="addEl($event);addModalOpened = false;"></add-element-modal>
    <shifts-modal [opened]="loginModalOpened"
                  (closeEmitter)="loginModalOpened = false;saveShift($event)"></shifts-modal>
    <div style=" width: 100%; position:relative;" role="presentation" class='hoverList uk-clearfix'
         [ngClass]="{'activeClass':  serv.activeId == directory.id}"
         (click)=' addActive(directory.id)'>
        <div class="item-info">
            <div class="shiftrow uk-grid uk-grid-collapse uk-text-left uk-padding-remove ">
                <!-- Buttons -->
                <div class="action-buttons action-buttons-relative uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse"
                     (dblclick)="directory.editable = canBeEditable(directory)">
                    <div *ngIf="!secondStep && directory.type !== 'company' && serv.activeId == directory.id"
                         (click)="delEl(directory)"
                         class="action trash" style="display: inline-block;"><i
                            [svgTemplate]="'trashCan'"></i></div>
                    <div class="action icon plus" *ngIf="!secondStep &&  serv.activeId == directory.id"
                         style="display: inline-block;"
                         (click)="addModalOpen(directory)"></div>
                    <!--<div (click)="directory.editable = true" class="action icon plus2"  style="display: inline-block;margin-right: 1%" ></div>-->
                    <!--<div (click)="directory.editable = true" class="action icon remove" style="display: inline-block;"></div>-->
                </div>

                <!-- Shift start -->
                <div class="shifts uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-6-10 uk-grid-collapse marginTopMobile">
                    <div class="uk-grid uk-grid-collapse uk-text-left uk-flex uk-flex-middle uk-padding-remove">
                        <div class='uk-width-1-5'>
                            <toggle-btn [id]="directory.id" (click)="setInherit(directory, $event)"
                                        [isUnit]="true"
                                        [checked]="directory.data.inherit"></toggle-btn>
                        </div>
                        <!-- Shiftse -->
                        <div class="uk-width-1-5 uk-container-center uk-grid-collapse shift-item"
                             *ngFor="let shift of directory.data.shift; let i = index"
                             style='font-size: 1.6rem; line-height: 2.4rem'>
                            <!--&lt;!&ndash; Shifts Schedule &ndash;&gt;-->
                            <span (click)="modalOpen(directory.id, i, shift, directory)"
                                  [ngClass]="{'inherit-shift': directory.data.inherit}"
                                  class='spanQuery'>
                      <span class="jc-shift-0-from from" id="company-150048-shift-0-from">{{ directory.data.inherit && directory.pShift ? directory.pShift[i].from  : shift.from}}</span>–<span
                                    class="jc-shift-0-to to" id="company-150048-shift-0-to">{{ directory.data.inherit && directory.pShift ? directory.pShift[i].to :  shift.to}}</span>
                    </span>

                            <!--&lt;!&ndash; Shifts checkbox &ndash;&gt;-->
                            <label *ngIf="directory.data.inherit">
                                <input disabled="disabled"
                                       [(ngModel)]=" shift.active "
                                       type="checkbox"><span
                                    class='checkBoxSpan '>&nbsp;</span>
                            </label>

                            <label *ngIf="!directory.data.inherit">
                                <input [(ngModel)]="shift.active" type="checkbox"
                                       (change)="checkShift(directory, directory.data.shift[i], i)"><span
                                    class='checkBoxSpan'>&nbsp;</span>
                            </label>

                            <!--&lt;!&ndash;<label *ngIf="directory.data.inherit && directory.type === 'company'">&ndash;&gt;-->
                            <!--&lt;!&ndash;<input  [(ngModel)]="parentShift.active" disabled="disabled" type="checkbox" (change)="checkShift(dir, shift, i)"><span class='checkBoxSpan'>&nbsp;</span>&ndash;&gt;-->
                            <!--&lt;!&ndash;</label>&ndash;&gt;-->

                        </div>
                        <!-- Shifts ENd -->
                    </div>
                </div>
            </div>
            <div  class='titleName jstree-anchor'
                    style=" margin-bottom: 1.5%; position: absolute; top:0px;"
                    [attr.lvl]="1 + lvl"
                    (dblclick)="directory.editable = canBeEditable(directory)"
                    (touchstart)="touchRenameDetected( (directory.name || directory.text) , $event) ? directory.editable = true : ''">
                <i *ngIf="dirShow[directory.id]" class='navIconClose drag-handle'
                   (click)="dirShow[directory.id] = !dirShow[directory.id];changeOpen(directory.id, dirShow[directory.id])"></i>
                <i *ngIf="!dirShow[directory.id]" class='navIconOpen drag-handle'
                   (click)="dirShow[directory.id] = !dirShow[directory.id];changeOpen(directory.id, dirShow[directory.id])"></i>
                <a class="drag-handle">
                    <i [ngClass]="{'fourBubbles': directory.type == 'company', 'threeBubbles': directory.type == 'group', 'twoBubbles': directory.type == 'unit', 'oneBubbles': directory.type == 'position'}"
                       class="drag-handle"></i>
                    {{ directory.editable !== true?(directory.name||directory.text):'' }}
                    <input size (onKeyDown)="this.size=this.value.length" maxlength='33'
                           class='InputChangeName'
                           [focus]="directory.editable" (click)="stopEvent($event)"
                           [value]="directory.name"
                           (keyup.enter)="rename(directory, $event)"
                           (blur)="rename(directory, $event)" type="text"
                           *ngIf="directory.editable">
                </a>
            </div>
        </div>
        <div class="drop-handler hidden" [attr.data-directory-id]="directory.id"
             [attr.lvl]="lvl"></div>
    </div>
    <ul class="directories list-item" [attr.data-directory-id]="directory.id" [attr.lvl]="lvl"
        style="" [ngClass]="{'hidden': !dirShow[directory.id]}">
        <li *ngFor="let dir of directory.directories | sortName : 'type'" [attr.data-id]="dir.id">
            <directory-view [directory]="dir" [info]="info" [lvl]="lvl + 1" [child]="true"
                            [parent]="directory" [parentEmitter]="parentEmitter"
                            [options]="options"
                            [secondStep]="secondStep"
                            [parentShift]="directory.data.inherit && directory.pShift ? directory.pShift : directory.data.shift"></directory-view>
        </li>
        <li *ngFor="let file of directory.files| sortName: 'text'; let fileIndex = index"
            style=" position:relative "
            [id]="file.id" class='paddingPlus uk-clearfix'
            [ngClass]="{'activeClass':   serv.activeId == file.id, 'hoverList': file.type !== 'position'}"
            [attr.data-id]="file.id"
            (click)=' addActive(file.id)'>
            <div class="item-info">
                <div unselectable="on" style="position: absolute; width: 100%" role="presentation">
                    <div class="shiftrow uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
                        <div class="action-buttons action-buttons-relative uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse"
                             (dblclick)="file.editable = true">
                            <div (click)="delEl(file)" *ngIf="!secondStep && serv.activeId == file.id"
                                 class="action  trash"
                                 style="display: inline-block;"><i [svgTemplate]="'trashCan'"></i>
                            </div>
                            <div *ngIf="!secondStep && file.type !== 'position'  &&  serv.activeId == file.id"
                                 (click)="addModalOpen(file, iP, file.type)"
                                 class="action icon plus"
                                 style="display: inline-block;"></div>
                            <!--<div (click)="file.editable = true" class="action icon plus2"  style="display: inline-block;margin-right: 1%" ></div>-->
                        </div>
                        <!---->
                        <!--<div  class="action-buttons uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse">-->
                        <!--</div>-->
                        <div class="shifts uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-6-10 uk-grid-collapse marginTopMobile">
                            <div class="uk-grid uk-grid-collapse uk-text-left uk-flex uk-flex-middle uk-padding-remove">

                                <!-- Toggle for units -->
                                <div class='uk-width-1-5'>
                                    <toggle-btn *ngIf="file.type !== 'position'" [id]="file.id"
                                                [isUnit]="true"
                                                (click)="setInherit2(file, $event, iP)"
                                                [checked]="file.data.inherit"></toggle-btn>
                                </div>
                                <!-- Shiftse -->
                                <div class="uk-width-1-5 uk-container-center uk-grid-collapse shift-item"
                                     *ngFor="let shift of file.data.shift; let i = index">
                                    <!--&lt;!&ndash; Shifts Schedule &ndash;&gt;-->
                                    <span (click)="modalOpenFile(iP, fileIndex, i, shift, file)"
                                          [ngClass]="{'inherit-shift': file.data.inherit}"
                                          class="fixedTimeString ">
                  <span class="jc-shift-0-from from"
                        style=' line-height: 2.4rem;'
                        id="company-150048-shift-0-from">{{file.data.inherit && directory.data.shift ? directory.data.shift[i].from : shift.from}}</span>–<span
                                            class="jc-shift-0-to to"
                                            style=' line-height: 2.4rem;'>{{file.data.inherit && directory.data.shift ? directory.data.shift[i].to : shift.to}}</span>
                </span>

                                    <!--&lt;!&ndash; Shifts checkbox &ndash;&gt;-->

                                    <label *ngIf="file.data.inherit">
                                        <input style="overflow: hidden" disabled="disabled"
                                               [(ngModel)]="shift.active"
                                               type="checkbox"><span
                                            class='checkBoxSpan'>&nbsp;</span>
                                    </label>

                                    <label *ngIf="!file.data.inherit">
                                        <input style="overflow: hidden" [(ngModel)]="shift.active"
                                               type="checkbox"
                                               (change)="checkShift(file, shift, i, true)"><span
                                            class='checkBoxSpan'>&nbsp;</span>
                                    </label>

                                </div>
                                <!-- Shifts ENd -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jstree-anchor  titleName unitStyle"
                     (dblclick)="file.editable = true"
                     [attr.lvl]="1 + lvl"
                     (touchstart)="touchRenameDetected( (file.name || file.text) , $event) ? file.editable = true : ''"
                     style=" position:relative;  top:0;">
                    <a class='drag-handle'>
                        <!--<i class="oneBubbles"></i>-->
                        <i [ngClass]="{'oneBubbles': file.type == 'position', 'twoBubbles': file.type == 'unit', 'threeBubbles': file.type == 'group' }"
                           class="drag-handle"></i>
                        {{file.editable !== true ? (file.name || file.text) : ''}}
                        <input class='InputChangeName' maxlength='33' [focus]="file.editable"
                               style=" "
                               (click)="stopEvent($event)"
                               [value]="file.text" (keyup.enter)="rename(file, $event)"
                               (blur)="rename(file, $event)"
                               type="text" *ngIf="file.editable">
                    </a>
                </div>
            </div>
            <div class="drop-handler hidden" *ngIf="file.type !== 'position'"
                 [attr.data-directory-id]="file.id" [attr.lvl]="lvl"></div>
        </li>
    </ul>
</div>
`;

import { Component, Input, Directive, ElementRef, Inject } from '@angular/core';
import { NgRedux, select } from 'ng2-redux';
import { Directory } from '../tree-view/directory';
import { UnitService } from '../../../services/unit.service';
import { ShiftsModalComponent } from '../shifts-modal/shifts-modal';
import { MdSlideToggle } from '@angular2-material/slide-toggle';
import { ToggleComponent } from '../toggle-btn/toggle.btn';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { AddElementModalComponent } from '../addElementModal/add-element-modal';
import { AppStoreAction } from '../../../store/action';
import { CompanyService } from '../../../services/company.service';
import { directoryViewHtml } from './directory-view.html';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';
import { DelModalComponent } from '../delModal/del-modal.component';
import { JobService } from '../../../services/shared/JobService';
import { SortNamePipe } from '../../../lib/pipes/sortName';
import { PositionService } from '../../../services/position.service';
import { OrganizationService } from '../../../services/organisation.service';
import {
    DirectoryEventEmitter,
    DIRECTORY_SHIFT_EVENT_KEY,
    DIRECTORY_ITEM_DRAG,
    DIRECTORY_ITEM_DROP, DIRECTORY_ITEM_MOVED, DIRECTORY_ITEM_ON_MOVING
} from './directory-event-emitter';

import { equals } from 'ramda';

@Directive({
  selector: '[focus]'
})
class FocusDirective {
  @Input()
  focus: boolean;
  constructor(@Inject(ElementRef) private element: ElementRef) {}
  protected ngOnChanges() {
    this.element.nativeElement.focus();
  }
}

@Component({
  selector: 'directory-view',
  template: directoryViewHtml,
  pipes: [ SortNamePipe ],
  directives: [ SvgTemplateDirective,
    DirectoryView,
    ShiftsModalComponent,
    MdSlideToggle,
    ToggleComponent,
    AddElementModalComponent,
    FocusDirective ],
  providers: [ DirectoryEventEmitter ]
})

export class DirectoryView {
  @Input() directory: Directory;
  @Input() child:  any;
  @Input() options: any;
  @Input() parentShift: any;
  @Input() lvl: any;
  @Input() info: any;
  @Input() parent: Directory;
  @Input() parentEmitter: DirectoryEventEmitter;
  @Input() secondStep;

  public serv = ParseOrganisationService;
  public listOWN =  [
    { name: 'Jilles', age: 21 },
    { name: 'Todd', age: 24 },
    { name: 'Lisa', age: 18 }
  ];
  public dirShow;
  public loginModalOpened = false;
  public addModalOpened = false;
  public flag = false;
  public pShift: any;
  public shift;
  public width1;
  public width2;
  public hideScrollBar = this.jobService.hideScrollBar;
  public lastReName: string;
  public hasClass(el: any, name: string) {
    return new RegExp('(?:^|\s+)' + name + '(?:\s+|$)').test(el.className);
  }

  public addClass(el: any, name: string) {
    if (!this.hasClass(el, name)) {
      el.className = el.className ? [el.className, name].join(' ') : name;
    }
  }

  public removeClass(el: any, name: string) {
    if (this.hasClass(el, name)) {
      el.className = el.className.replace(new RegExp('(?:^|\s+)' + name + '(?:\s+|$)', 'g'), '');
    }
  }

  constructor(public unitService: UnitService,
              public action: AppStoreAction,
              public companyService: CompanyService,
              public positionService: PositionService,
              public jobService: JobService,
              private organizationService: OrganizationService) {
  }

  ngOnInit() {

    this.setUpDirectories();
    if (this.lvl > 0) {
      this.width1 = `${40 - (this.lvl * 2)}%`;
      this.width2 = `${60 + (this.lvl * 2)}%`;
    }
    this.dirShow = this.serv.treeViewOpen;

    if (this.parent) {

      this.parentEmitter.on(DIRECTORY_SHIFT_EVENT_KEY, this.parent.id).subscribe(event => {
        this.getCheckParentShifts();
        // if (this.directory.data.inherit) {
        //   if (!this.directory.data.shift && event.data.totalShift) {
        //     this.directory.data.shift = event.data.totalShift.map(el => Object.assign({}, el));
        //   } else if (this.directory.data.shift) {
        //     this.directory.data.shift[event.data.index] = Object.assign({}, event.data.shift);
        //   }
        //
        //   // Broadcast changes for the children
        //   this.resetSubdirectoriesShift(event.data.index, event.data.shift);
        // }
      });

      this.parentEmitter.on(DIRECTORY_ITEM_MOVED, this.directory.id).subscribe(event => {
        this.lvl = event.data.lvl;
      });

      this.parentEmitter.on(DIRECTORY_ITEM_ON_MOVING, this.directory.id).subscribe(event => {
        this.lvl = event.data.lvl;
      });

      this.parentEmitter.on(DIRECTORY_ITEM_DROP, this.directory.id).subscribe(event => {
      });
    }
  }

  changeOpen(id, value) {

    ParseOrganisationService.treeViewOpen[id] = value;
  }
  touchRenameDetected( name, event ) {
    if (!event || event.target.tagName === 'I' ) return;

    if ( this.lastReName !== name ) {
      this.lastReName = name;
      return false;
    }else if ( this.lastReName === name ) {
      this.lastReName = '';
      return true;
    }
  }
  ngOnChanges(changes) {
    if (this.lvl > 0) {
      this.width1 = `${40 - (this.lvl * 2)}%`;
      this.width2 = `${60 + (this.lvl * 2)}%`;

    }
    let a = this.parentShift;

    this.setUpDirectories();
  }
  setUpDirectories() {
    let dirShift = {};
    this.directory.data.inherit ?
      dirShift = this.parentShift : dirShift = this.directory.data.shift;
    return Object.assign({}, this.directory, {
      files: this.directory.files.map(el => {
        return el.data.inherit ? Object.assign({}, el, {pShift: dirShift})
          : Object.assign({}, el, {pShift: el.data.shift });
      }),
      pShift: dirShift});
  }
  setInherit(dir, event) {
    event.preventDefault();

    let params = {
      unit_id: +dir.id.slice(1),
      inherit: !dir.data.inherit
    };
    this.unitService.put(params).subscribe(el => {
      dir.data.inherit = dir.data.inherit ? false : true;
      dir.data.inherit ?
          dir.pShift = Object.assign([], this.parentShift) :
          dir.pShift = Object.assign([], dir.data.shift);
      this.getCheckParentShifts();
    });
  }
  setInherit2(file, event) {
    let params = {
      unit_id: +file.id.slice(1),
      inherit: !file.data.inherit
    };
    this.unitService.put(params).subscribe(el => {
      file.data.inherit = !file.data.inherit;
      file.pShift = Object.assign([], this.directory.data.inherit ?
          this.parentShift :
          file.data.shift);
      let parentShift = this.directory.data.shift;
      if (file.data.inherit) {
        parentShift.forEach((item, index) => {
          if (file.data.shift[index].from !== item.from
            || file.data.shift[index].to !== item.to
            || file.data.shift[index].active !== item.active) {
            if (file.data.shift[index]) {
              delete file.data.shift[index];
            }
            file.data.shift[index] = Object.assign({}, item);
          }
        });
      }
    });
    event.stopPropagation();
    event.preventDefault();
  }
  checkShift(unit, shift, index, isFile) {
    shift.active = !shift.active;
    let params = {unit_id: +unit.id.slice(1), shift: unit.data.shift};
    this.unitService.put(params).subscribe(el => {
      unit.data.shift[index].active = shift.active;
    });
    if (!isFile) {
      this.resetSubdirectoriesShift(index, shift);
    }
  }
  resetSubdirectoriesShift(index, shift) {
    // TODO: Ask Ingo
    // Because some time server send inherit groups without shifts
    // this.getCheckParentShifts();
    let totalShift = this.directory.data.shift.map(el => Object.assign({}, el));

    for (let file of this.directory.files) {
      if (file.data.shift && file.data.inherit && file.data.shift[index]) {
        // delete file.data.shift[index];
        Object.assign(file.data.shift[index], shift);
      }
    }

    this.parentEmitter
      .broadcastShiftChanges(index, shift, this.directory, totalShift as Array<any>);
  }
  saveShift(data) {
    if (!data) return;
    let shift = {
      from: [data.state.from.h, data.state.from.m].join(':'),
      to: [data.state.to.h, data.state.to.m].join(':')
    };
    let params;
    if (data.unitId.hasOwnProperty('fileIndex')) {
      this.directory
          .files[data.unitId.fileIndex].data.shift[data.unitId.shiftIndex].from = shift.from;
      this.directory
          .files[data.unitId.fileIndex].data.shift[data.unitId.shiftIndex].to = shift.to;
      params = {
        shift: this.directory
            .files[data.unitId.fileIndex].data.shift,
        unit_id: this.directory
            .files[data.unitId.fileIndex].id.slice(1)
      };
    } else {
      this.directory.data.shift[data.unitId.shiftIndex].from = shift.from;
      this.directory.data.shift[data.unitId.shiftIndex].to = shift.to;

      for (let file of this.directory.files) {
        if (file.data.shift && file.data.inherit) {
          delete file.data.shift[data.unitId.shiftIndex];
          file.data.shift[data.unitId.shiftIndex] = Object.assign({}, shift);
        }
        // file.data.shift[data.unitId.shiftIndex].to = shift.to;
      }

      params = {
        shift: this.directory.data.shift,
        unit_id: this.directory.id.slice(1)
      };
      this.resetSubdirectoriesShift(data.unitId.shiftIndex, shift);
    }
    this.unitService.put(params).subscribe(el => {
    });
  }

  addEl(state) {
    if (state) {
      if (state.parent.type === 'unit') {
        this.positionService.putPoition(state.name,
            state.parent.dir.data.company_id,
            state.parent.dir.id)
            .subscribe(res => {
              this.action.fetchGetInfo();
            });
      } else {
        let params = {
          company_id: `${state.parent.dir.data.company_id}`,
          nname: state.name,
          parent: state.parent.dir.id.slice(1),
          type: state.type
        };
        this.unitService.put(params).subscribe(res => {
          this.action.fetchGetInfo();
        });
      }
    }
  }

  delEl(file) {
    if (file.type === 'position') {
      this.unitService.getUnit(file.parent.slice(1)).subscribe((res: UnitI) => {
        let staffArr = res.positions[file.id.slice(1)].staff;
        if (!staffArr) {
          this.positionService.delPosition(file.id.slice(1)).subscribe(delRes => {
            this.action.fetchGetInfo();
          });
        } else if (!staffArr.some(el => el instanceof Array)) {
          this.positionService.delPosition(file.id.slice(1)).subscribe(delRes => {
            this.action.fetchGetInfo();
          });
        } else {
          this.action.treeViewError(true);
        }
      });
    } else {
      if ((!file.directories || !file.directories.length) && (!file.files || !file.files.length)) {
        this.action.openDelModal(true, file);
        this.hideScrollBar();
      } else {
        this.action.treeViewErrorU(true);
      }
    }

  }
  modalOpen(index, shiftIndex, shift, parent) {
    ParseOrganisationService.unitId = {};
    ParseOrganisationService.unitId = {
      index: index,
      shiftIndex: shiftIndex, shift: shift, parent: parent
    };
    this.loginModalOpened = true;
    this.hideScrollBar();
  }

  modalOpenFile(index, fileIndex, shiftIndex, shift, parent) {
    ParseOrganisationService.unitId = {};
    ParseOrganisationService.unitId = {
      index: index,
      fileIndex: fileIndex,
      shiftIndex: shiftIndex,
      shift: shift,
      parent: parent
    };
    this.loginModalOpened = true;
    this.hideScrollBar();
  }

  addModalOpen(dir) {
    ParseOrganisationService.addModalData = {
      index: dir.id,
      dir: dir,
      type: dir.type
    };
    this.addModalOpened = true;
    this.hideScrollBar();
  }

  stopEvent(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  addActive(id) {
    ParseOrganisationService.activeId = id;
  }

  rename(el, event ) {
    if (el.type === 'company') {
      let params = {
        cname: el.name,
        company_id: +el.data.company_id
      };
      this.companyService.putCompany(params).subscribe(res => {
        el.name = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    } else if (el.type === 'position') {
      let params = {
        pname: event.target.value,
        position_id: +el.id.slice(1)
      };
      this.positionService.rename(params).subscribe(res => {
        el.text = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    } else {
      let params = {
        nname: event.target.value,
        type: el.type,
        unit_id: el.id.slice(1)
      };
      this.unitService.put(params).subscribe(res => {
        el.name = event.target.value;
        el.text = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    }
  }
  canBeEditable(directory) {
    return directory.type !== 'company';
  }

  private getCheckParentShifts() {
    let parentShift = !this.parent ? this.directory.pShift : this.parent.data.shift;
    if (parentShift && this.directory.data.inherit) {
      parentShift.forEach((item, index) => {

        if (!equals(this.directory.data.shift[index], item)) {
          // if (this.directory.data.shift[index]) {
          //   delete this.directory.data.shift[index];
          // }
          Object.assign(this.directory.data.shift[index], item);
        }
        this.resetSubdirectoriesShift(index, item);

      });
    }
  }
}


interface UnitI {
  positions: any;
}

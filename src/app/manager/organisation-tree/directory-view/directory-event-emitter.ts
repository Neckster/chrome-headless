import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Directory } from '../tree-view/directory';


export interface DirectoryEvent {
    key: string;
    directory: Directory;
    data: any;
}

export const DIRECTORY_SHIFT_EVENT_KEY = 'directory_shift_event';
export const DIRECTORY_ITEM_DRAG = 'directory_item_drag';
export const DIRECTORY_ITEM_ON_DRAG = 'directory_item_on_drag';
export const DIRECTORY_ITEM_ON_MOVING = 'directory_item_on_moving';
export const DIRECTORY_ITEM_DROP = 'directory_item_drop';
export const DIRECTORY_ITEM_MOVED = 'directory_item_moved';

export class DirectoryEventEmitter {
    private directoryEmitter: Subject<DirectoryEvent>;


    constructor() {
        this.directoryEmitter = new Subject<DirectoryEvent>();
    }

    broadcastShiftChanges (index: number,
                           shift: any,
                           directory: Directory,
                           totalShift?: Array<any>) {
        this.emit(
            DIRECTORY_SHIFT_EVENT_KEY + '.' + directory.id,
            directory,
            {index: index, shift: shift, totalShift}
            );
    }
    broadcast (event: string, directory: Directory, data: any) {
        event = event + '.' + directory.id;
        this.emit(
            event,
            directory,
            data
        );
    }

    emit(key: string, directory: Directory, data: any) {
        this.directoryEmitter.next({key: key, directory: directory, data: data});
    }

    onAny(key: string) {
        return this.directoryEmitter.asObservable()
            .filter(event => event.key === key);
    }

    on(key: string, currentId: number) {
        key = key + '.' + currentId;
        return this.directoryEmitter.asObservable()
            .filter(event => {
                return event.key === key;
            });
    }
}

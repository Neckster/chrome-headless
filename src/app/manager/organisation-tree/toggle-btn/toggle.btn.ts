import { Component, Input, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'toggle-btn',  // <select-unit></select-unit>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [],
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: `<div  class="toggle uk-width-1-5 uk-container-center uk-grid-collapse" #container
    (click)="clickCheck($event)"></div>`
})

export class ToggleComponent implements OnInit, AfterViewInit {
  // Set our default values
  @Input() id;
  @Input() checked;
  @Input() isUnit;
  // @Input() setInherit;
  // @Output() closeEmitter = new EventEmitter();
  public toggle;
  public checkedC;
  @ViewChild('container') container: ElementRef;
  // TypeScript public modifiers
  constructor() {
  }


  clickCheck() {
    // console.log(checkedC);
    if (this.checkedC === undefined) this.checkedC = this.checked;
    if (!this.isUnit) return;
    this.checkedC = !this.checkedC;
    this.container.nativeElement.innerHTML =
      `<input class="switch" type="checkbox" ${this.checkedC ? 'checked' : ''} id="${this.id}">
            <label for="${this.id}" >
              <!-- empty -->
            </label>`;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
      this.container.nativeElement.innerHTML =
          `<input class="switch" type="checkbox" ${this.checked ? 'checked' : ''} id="${this.id}">
            <label for="${this.id}" >
              <!-- empty -->
            </label>`;
  }
}

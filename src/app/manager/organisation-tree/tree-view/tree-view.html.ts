// noinspection TsLint
export const treeViewHtml =`
        <add-element-modal *ngIf="addModalOpened" [opened]="addModalOpened"
                           (closeEmitter)="addEl($event);addModalOpened = false;"></add-element-modal>
        <shifts-modal [opened]="loginModalOpened" (closeEmitter)="loginModalOpened = false;saveShift($event)"></shifts-modal>

        <div class="direcory-view">
          <div class="directory-caption handle">
          <i *ngIf="dirShow[iP]" class='navIconClose' (click)="dirShow[iP] = !dirShow[iP]; changeOpen(directories.id, dirShow[iP])"></i>
          <i *ngIf="!dirShow[iP]" class='navIconOpen' (click)="dirShow[iP] = !dirShow[iP]; changeOpen(directories.id, dirShow[iP])"></i>
              {{directories.name}}
          </div>
          <ul class="directories list-item">
              <li *ngFor="let dir of directories.directories">
                  <directory-view [directory]="dir"></directory-view>
              </li>
              <li *ngFor="let file of directories.files" class="file">
                  {{ file.text }}
              </li>
          </ul>
      </div>





<!--<ul [ngClass]="{ 'uk-nav': !child}" data-uk-nav="{multiple:true}" *ngIf="parentShift" class='treeViewMain' >
    <li [id]='dir.id' *ngFor="let dir of directories; let iP = index" style="position: relative;">


        <div style=" width: 100%; position:relative;" role="presentation" class='hoverList'
             [ngClass]="{'activeClass':  serv.activeId == dir.id}" (click)=' addActive(dir.id)'>
            <div class="shiftrow uk-grid uk-grid-collapse uk-text-left uk-padding-remove ">
                &lt;!&ndash; Buttons &ndash;&gt;
                <div class="action-buttons uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse"
                     (dblclick)="dir.editable = true;">
                    <div *ngIf="dir.type !== 'company' && serv.activeId == dir.id " (click)="delEl(dir)"
                         class="action trash" style="display: inline-block;"><i [svgTemplate]="'trashCan'"></i></div>
                    <div class="action icon plus" *ngIf=" serv.activeId == dir.id " style="display: inline-block;"
                         (click)="addModalOpen(dir, iP, dir.type)"></div>
                    &lt;!&ndash;<div (click)="dir.editable = true" class="action icon plus2"  style="display: inline-block;margin-right: 1%" ></div>&ndash;&gt;
                    &lt;!&ndash;<div (click)="dir.editable = true" class="action icon remove" style="display: inline-block;"></div>&ndash;&gt;
                </div>

                &lt;!&ndash; Shift start &ndash;&gt;
                <div class="shifts uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-6-10 uk-grid-collapse marginTopMobile">
                    <div class="uk-grid uk-grid-collapse uk-text-left uk-flex uk-flex-middle uk-padding-remove">
                        <div class='uk-width-1-5'>
                            <toggle-btn [id]="dir.id" (click)="setInherit(dir, $event, Ip)" [isUnit]="true"
                                        [checked]="dir.data.inherit"></toggle-btn>
                        </div>
                        &lt;!&ndash; Shiftse &ndash;&gt;
                        <div class="uk-width-1-5 uk-container-center uk-grid-collapse"
                             *ngFor="let shift of dir.data.shift; let i = index"
                             style='font-size: 1.6rem; line-height: 2.4rem'>
                            &lt;!&ndash; Shifts Schedule &ndash;&gt;
                            <span (click)="modalOpen(iP, i, shift, dir)" [ngClass]="{'inherit-shift': dir.data.inherit}"
                                  class='spanQuery'>
                      <span class="jc-shift-0-from from" id="company-150048-shift-0-from">{{ dir.data.inherit ? parentShift[i].from : shift.from}}</span>–<span
                                    class="jc-shift-0-to to" id="company-150048-shift-0-to">{{dir.data.inherit ? parentShift[i].to: shift.to}}</span>
                    </span>

                            &lt;!&ndash; Shifts checkbox &ndash;&gt;
                            <label *ngIf="dir.data.inherit">
                                <input disabled="disabled" [(ngModel)]="parentShift[i].active" type="checkbox"><span
                                    class='checkBoxSpan '>&nbsp;</span>
                            </label>

                            <label *ngIf="!dir.data.inherit">
                                <input [(ngModel)]="shift.active" type="checkbox"
                                       (change)="checkShift(dir, dir.pShift[i], i)"><span
                                    class='checkBoxSpan'>&nbsp;</span>
                            </label>

                            &lt;!&ndash;<label *ngIf="dir.data.inherit && dir.type === 'company'">&ndash;&gt;
                            &lt;!&ndash;<input  [(ngModel)]="parentShift.active" disabled="disabled" type="checkbox" (change)="checkShift(dir, shift, i)"><span class='checkBoxSpan'>&nbsp;</span>&ndash;&gt;
                            &lt;!&ndash;</label>&ndash;&gt;

                        </div>
                        &lt;!&ndash; Shifts ENd &ndash;&gt;
                    </div>
                </div>
            </div>
            <div [style.padding-left]="lvl >= 0 ? ( lvl == 0 ? '10px' :  lvl == 1 ? '20px' : (lvl * 10 * 1.6) + 'px') : '0px'"
                 class='titleName jstree-anchor' style=" margin-bottom: 1.5%; position: absolute; top:0px; "
                 (dblclick)="dir.editable = true;"
                 (touchstart)=" touchRenameDetected( (dir.name||dir.text) , $event) ? dir.editable = true : '' ">
                <i *ngIf="dirShow[iP]" class='navIconClose'
                   (click)="dirShow[iP] = !dirShow[iP];changeOpen(dir.id, dirShow[iP])"></i>
                <i *ngIf="!dirShow[iP]" class='navIconOpen'
                   (click)="dirShow[iP] = !dirShow[iP];changeOpen(dir.id, dirShow[iP])"></i>
                <a>
                    <i [ngClass]="{'fourBubbles': dir.type == 'company', 'threeBubbles': dir.type == 'group', 'twoBubbles': dir.type == 'unit', 'oneBubbles': dir.type == 'position'}"></i>
                    {{ dir.editable !== true?(dir.name||dir.text):'' }}
                    <input size (onKeyDown)="this.size=this.value.length" maxlength='33' class='InputChangeName'
                           [focus]="dir.editable" (click)="stopEvent($event)" [value]="dir.name"
                           (keyup.enter)="rename(dir, $event)" (blur)="rename(dir, $event)" type="text"
                           *ngIf="dir.editable">
                </a>
            </div>
        </div>
        <ul *ngIf="dirShow[iP]">
            <li class="uk-parent list-item" >
                <treeview [info]="info" [lvl]="lvl + 1" [child]="true" [directories]="dir.directories"
                          [options]="options" [parentShift]="dir.pShift"></treeview>
            </li>

            <li *ngFor="let file of dir.files; let fileIndex = index" style=" position:relative "
                [id]="file.id" class='hoverList paddingPlus' [ngClass]="{'activeClass':   serv.activeId == file.id}"
                (click)=' addActive(file.id)'>

                <div unselectable="on" style="position: absolute; width: 100%" role="presentation">
                    <div class="shiftrow uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
                        <div class="action-buttons uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse"
                             (dblclick)="file.editable = true;">
                            <div (click)="delEl(file)" *ngIf=" serv.activeId == file.id" class="action  trash"
                                 style="display: inline-block;"><i [svgTemplate]="'trashCan'"></i></div>
                            <div *ngIf="file.type !== 'position'  &&  serv.activeId == file.id"
                                 (click)="addModalOpen(file, iP, file.type)" class="action icon plus"
                                 style="display: inline-block;"></div>
                            &lt;!&ndash;<div (click)="file.editable = true" class="action icon plus2"  style="display: inline-block;margin-right: 1%" ></div>&ndash;&gt;
                        </div>
                        &lt;!&ndash;&ndash;&gt;
                        &lt;!&ndash;<div  class="action-buttons uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-grid-collapse">&ndash;&gt;
                        &lt;!&ndash;</div>&ndash;&gt;
                        <div class="shifts uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-6-10 uk-grid-collapse marginTopMobile">
                            <div class="uk-grid uk-grid-collapse uk-text-left uk-flex uk-flex-middle uk-padding-remove">

                                &lt;!&ndash; Toggle for units &ndash;&gt;
                                <div class='uk-width-1-5'>
                                    <toggle-btn *ngIf="file.type !== 'position'" [id]="file.id" [isUnit]="true"
                                                (click)="setInherit2(file, $event, iP)"
                                                [checked]="file.data.inherit"></toggle-btn>
                                </div>
                                &lt;!&ndash; Shiftse &ndash;&gt;
                                <div class="uk-width-1-5 uk-container-center uk-grid-collapse"
                                     *ngFor="let shift of file.data.shift; let i = index">
                                    &lt;!&ndash; Shifts Schedule &ndash;&gt;
                                    <span (click)="modalOpenFile(iP, fileIndex, i, shift, file)"
                                          [ngClass]="{'inherit-shift': file.data.inherit}" class="fixedTimeString ">
                  <span class="jc-shift-0-from from"
                        style=' line-height: 2.4rem;'
                        id="company-150048-shift-0-from">{{file.data.inherit ? dir.pShift[i].from : shift.from}}</span>–<span
                                            class="jc-shift-0-to to"
                                            style=' line-height: 2.4rem;' id="company-150048-shift-0-to">{{file.data.inherit ? dir.pShift[i].to : shift.to}}</span>
                </span>

                                    &lt;!&ndash; Shifts checkbox &ndash;&gt;

                                    <label *ngIf="file.data.inherit">
                                        <input style="overflow: hidden" disabled="disabled"
                                               [(ngModel)]="dir.pShift[i].active" type="checkbox"><span
                                            class='checkBoxSpan'>&nbsp;</span>
                                    </label>

                                    <label *ngIf="!file.data.inherit">
                                        <input style="overflow: hidden" [(ngModel)]="shift.active" type="checkbox"
                                               (change)="checkShift(file, shift, i, true)"><span class='checkBoxSpan'>&nbsp;</span>
                                    </label>

                                </div>
                                &lt;!&ndash; Shifts ENd &ndash;&gt;
                            </div>
                        </div>
                    </div>
                </div>
                <a class='jstree-anchor  titleName unitStyle' (dblclick)="file.editable = true; "
                   (touchstart)=" touchRenameDetected(file.name || file.text) ? file.editable = true : '' "
                   style=" position:relative;  top:0 "
                   [style.padding-left]="lvl >= 0 ? ( lvl == 0 ? '25px' :  lvl >= 4 ?  (lvl*20) + 'px' : (lvl*30) + 'px') : '10px'"
                   [style.line-height]="lvl > 5 ? '2em' : 'none'">
                    &lt;!&ndash;<i class="oneBubbles"></i>&ndash;&gt;
                    <i [ngClass]="{'oneBubbles': file.type == 'position', 'twoBubbles': file.type == 'unit', 'threeBubbles': file.type == 'group' }"></i>
                    {{file.editable !== true ? (file.name || file.text) : ''}}
                    <input class='InputChangeName' maxlength='33' [focus]="file.editable"
                           style="height: 30px; margin-top:7px; padding:5px; " (click)="stopEvent($event)"
                           [value]="file.text" (keyup.enter)="rename(file, $event)" (blur)="rename(file, $event)"
                           type="text" *ngIf="file.editable">
                </a>
            </li>
        </ul>
    </li>
</ul>-->
`;

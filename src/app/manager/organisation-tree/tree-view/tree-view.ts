import { Component, Input, Directive, ElementRef, Inject } from '@angular/core';
import { select } from 'ng2-redux';
import { Directory } from './directory';
import { UnitService } from '../../../services/unit.service';
import { ShiftsModalComponent } from '../shifts-modal/shifts-modal';
import { MdSlideToggle } from '@angular2-material/slide-toggle';
import { ToggleComponent } from '../toggle-btn/toggle.btn';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { AddElementModalComponent } from '../addElementModal/add-element-modal';
import { AppStoreAction } from '../../../store/action';
import { CompanyService } from '../../../services/company.service';
import { treeViewHtml } from './tree-view.html';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';
import { DelModalComponent } from '../delModal/del-modal.component';
import { JobService } from '../../../services/shared/JobService';
import { SortNamePipe } from '../../../lib/pipes/sortName';
import { PositionService } from '../../../services/position.service';
import { OrganizationService } from '../../../services/organisation.service';

@Directive({
  selector: '[focus]'
})
class FocusDirective {
  @Input()
  focus: boolean;
  constructor(@Inject(ElementRef) private element: ElementRef) {}
  protected ngOnChanges() {
    this.element.nativeElement.focus();
  }
}

@Component({
  selector: 'treeview',
  template: treeViewHtml,
  pipes: [ SortNamePipe ],
  directives: [
    SvgTemplateDirective,
    TreeView,
    ShiftsModalComponent,
    MdSlideToggle,
    ToggleComponent,
    AddElementModalComponent,
    FocusDirective],
  styles: [`
      .inherit-shift {
       pointer-events: none;
       cursor: default;
       color: grey
      }
      .zoom-glyph {zoom: 123%;}
     .checkbox-comp {
      width: 10%;
      height: 20px;
      position: inherit; 
      margin-top: 8%;
      pointer-events: all;
      margin-right: 12%
      
     }
     .marginLeftName{
      padding-left: 20px;
     }
     .marginLeftLastName{
        padding-left: 40px;
     }
     .trash{
      cursor: pointer;
      margin-right: 8px;
     }
    
      .navIconOpen,.navIconClose{
        margin-right:10px; 
      }
     .navIconOpen:before, .navIconClose:before{
        background-position: -4px -4px;
        font-family: 'jclight', 'HelveticaNeue-Light', 'Helvetica Neue Light', sans-serif;
        font-size: 120%;
        color: #07a0e1;
        
     }
     .navIconClose:before{
      content: "\\2304";
     }
     .navIconOpen:before{
      content: "\\232a";
     }
     
     .uk-notouch{
      color:rgba(0,0,0,0)!important;
     }
     .icond {content: "\\15B"}
     .titleName{
       line-height: 40px;
       height: 40px;
     }
     
     a, a:hover{
        color:black;
     }
     input.InputChangeName{
        height: 30px;
        min-width: 200px!important;
        margin-top:7px;
        padding:5px;
        position:absolute;
        top:0;
        
     }
     .uk-nav ul{
         padding-left: 0;
     }
     i, i:before{
      cursor:pointer;
     }
     .oneBubbles:before, .twoBubbles:before, .threeBubbles:before, .fourBubbles:before{
         font-family: 'jclight', 'HelveticaNeue-Light', 'Helvetica Neue Light', sans-serif;
         line-height: 4rem;
         height: 4rem;
         font-size: 1.8rem;
         margin-right:5px;
     }
     .oneBubbles:before{
        content: '\\2582' ;
     }
     .twoBubbles:before{
        content: '\\2584' ;
     }
     .threeBubbles:before{
        content: '\\2586' ;
     }
     .fourBubbles:before{
        content: "\\2588" ;
     }
     span.checkBoxSpan:before{
        font-family: 'jclight', 'HelveticaNeue-Light',  sans-serif;
        font-size: 30px;
        margin-top: 10px;
        position: relative;
        top:2px;
        left:2px;
     }
     .activeClass, .activeClass:hover{
        background: #beebff;
        background: -moz-linear-gradient(top, #beebff 0%, #a8e4ff 100%);
        background: -webkit-linear-gradient(top, #beebff 0%, #a8e4ff 100%);
        background: -o-linear-gradient(top, #beebff 0%, #a8e4ff 100%);
        background: -ms-linear-gradient(top, #beebff 0%, #a8e4ff 100%);
        background: linear-gradient(to bottom, #beebff 0%, #a8e4ff 100%);
     }
     .no-touch ul{
        z-index: 99;
        display: block;
      }
    .fixedTimeString{
      font-size: 1.6rem;
    }
     
     
     @media (max-width: 1100px){
         span.checkBoxSpan{
          margin-left:20px;
         }
     }
     @media (max-width: 960px){
         span.checkBoxSpan{
          margin-left:0;
          text-align: center;
         }
     }
     @media (max-width: 640px){
         span.checkBoxSpan{
          margin-left:30px;
         }
         .spanQuery{
            font-size: 1.4rem;
          }
     }
     @media (max-width: 480px){
      .marginTopMobile{
        margin-top:40px;
      }
      .spanQuery{
        font-weight: 100;
        font-size: 1.2rem;
        margin-left: -2px;
      }
       span.checkBoxSpan:before{
        font-size: 20px;
        margin:0;
       }
      .jstree-anchor{
          margin-bottom: 4.2rem!important;
          float: none!important;
       }
       .fixedTimeString{
          white-space: nowrap;
          word-wrap: normal;
          width: 20px;
          margin-left: -2px;
          font-size: 1.2rem!important;

       }
       .fixedTimeString .to{
        margin-right: 2px;
       }
     
       li.hoverList.paddingPlus{
        padding-bottom: 15px;
       }
       
     }
     
     
     `]
})

export class TreeView {
  @Input() directories: Array<Directory>;
  @Input() child:  any;
  @Input() options: any;
  @Input() parentShift: any;
  @Input() lvl: any;
  @Input() info: any;


  public serv = ParseOrganisationService;
  public listOWN =  [
    { name: 'Jilles', age: 21 },
    { name: 'Todd', age: 24 },
    { name: 'Lisa', age: 18 }
  ];
  public dirShow;
  public loginModalOpened = false;
  public addModalOpened = false;
  public flag = false;
  public pShift: any;
  public shift;
  public width1;
  public width2;
  public hideScrollBar = this.jobService.hideScrollBar;
  public lastReName: string;
  public hasClass(el: any, name: string) {
    return new RegExp('(?:^|\s+)' + name + '(?:\s+|$)').test(el.className);
  }

  public addClass(el: any, name: string) {
    if (!this.hasClass(el, name)) {
      el.className = el.className ? [el.className, name].join(' ') : name;
    }
  }

  public removeClass(el: any, name: string) {
    if (this.hasClass(el, name)) {
      el.className = el.className.replace(new RegExp('(?:^|\s+)' + name + '(?:\s+|$)', 'g'), '');
    }
  }

  public onDrag(args) {
    let [e, el] = args;
    this.removeClass(e, 'ex-moved');
  }

  public onDrop(args) {
    let [e, el] = args;
    this.addClass(e, 'ex-moved');
  }

  public onOver(args) {
    let [e, el, container] = args;
    this.addClass(el, 'ex-over');
  }

  public onOut(args) {
    let [e, el, container] = args;
    this.removeClass(el, 'ex-over');
  }
  constructor(public unitService: UnitService,
              public action: AppStoreAction,
              public companyService: CompanyService,
              public positionService: PositionService,
              public jobService: JobService,
              private organizationService: OrganizationService) {
    this.setUpDirectories();


  }
/* tslint:disable:no-string-literal */

  ngOnInit() {
    if (this.lvl > 0) {
      this.width1 = `${40 - (this.lvl * 2)}%`;
      this.width2 = `${60 + (this.lvl * 2)}%`;
    }
    let id = this.directories['id'];
    this.dirShow = this.serv.treeViewOpen[id];
  }
/* tsslint:enable:no-string-literal */

  changeOpen(id, value, event) {

    ParseOrganisationService.treeViewOpen[id] = value;

  }

  touchRenameDetected( name, event ) {
    if ( event.target.tagName === 'I' ) return;

    if ( this.lastReName !== name ) {
        this.lastReName = name;
        return false;
    }else if ( this.lastReName === name ) {
      this.lastReName = '';
      return true;
    }
  }

  ngOnChanges() {
    if (this.lvl > 0) {
      this.width1 = `${40 - (this.lvl * 2)}%`;
      this.width2 = `${60 + (this.lvl * 2)}%`;

    }
    this.setUpDirectories();
  }

  setUpDirectories() {
    if (this.directories) {
      this.directories = this.directories.map(dir => {
        let dirShift = {};
        dir.data.inherit ? dirShift = this.parentShift : dirShift = dir.data.shift;
        return Object.assign({}, dir, {
          files: dir.files.map(el => {
          return el.data.inherit ? Object.assign({}, el, {pShift: dirShift})
            : Object.assign({}, el, {pShift: el.data.shift });
        }),
          pShift: dirShift});
      });
    }
  }

  setInherit(dir, event, index) {

    let params = {
      unit_id: +dir.id.slice(1),
      inherit: !dir.data.inherit
    };
    // this.flag = true;
    this.unitService.put(params).subscribe(el => {
      dir.data.inherit = dir.data.inherit ? false : true;
        dir.data.inherit ?
          dir.pShift = Object.assign({}, this.parentShift) :
          dir.pShift = Object.assign({}, dir.data.shift);
    });

  }

  setInherit2(file, event, index) {
    let params = {
      unit_id: +file.id.slice(1),
      inherit: !file.data.inherit
    };
    this.unitService.put(params).subscribe(el => {
      file.data.inherit = !file.data.inherit;
      file.pShift = Object.assign([], this.directories[index].data.inherit ?
        this.parentShift :
        file.data.shift);
    });
    event.stopPropagation();
    event.preventDefault();
  }

  checkShift(unit, shift, index, isFile) {
    shift.active = !shift.active;
    let params = {unit_id: +unit.id.slice(1), shift: unit.data.shift};
    this.unitService.put(params).subscribe(el => {
      unit.pShift[index].active = shift.active;
    });
  }

  saveShift(data) {
    if (!data) return;
    let shift = {
      from: [data.state.from.h, data.state.from.m].join(':'),
      to: [data.state.to.h, data.state.to.m].join(':')
    };
    let params;
    if (data.unitId.hasOwnProperty('fileIndex')) {
      this.directories[data.unitId.index]
        .files[data.unitId.fileIndex].data.shift[data.unitId.shiftIndex].from = shift.from;
      this.directories[data.unitId.index]
        .files[data.unitId.fileIndex].data.shift[data.unitId.shiftIndex].to = shift.to;
      params = {
        shift: this.directories[data.unitId.index]
          .files[data.unitId.fileIndex].data.shift,
        unit_id: this.directories[data.unitId.index]
          .files[data.unitId.fileIndex].id.slice(1)
      };
    } else {
      this.directories[data.unitId.index].data.shift[data.unitId.shiftIndex].from = shift.from;
      this.directories[data.unitId.index].data.shift[data.unitId.shiftIndex].to = shift.to;
      params = {
        shift: this.directories[data.unitId.index].data.shift,
        unit_id: this.directories[data.unitId.index].id.slice(1)
      };
    }
    this.unitService.put(params).subscribe(el => {
    });
  }


  addEl(state) {
    if (state) {
      if (state.parent.type === 'unit') {
        this.positionService.putPoition(state.name,
          state.parent.dir.data.company_id,
          state.parent.dir.id)
          .subscribe(res => {
            this.action.fetchGetInfo();
          });
      } else {
        let params = {
          company_id: `${state.parent.dir.data.company_id}`,
          nname: state.name,
          parent: state.parent.dir.id.slice(1),
          type: state.type
        };
        this.unitService.put(params).subscribe(res => {
          this.action.fetchGetInfo();
        });
      }
    }
  }

  delEl(file) {
    if (file.type === 'position') {
      this.unitService.getUnit(file.parent.slice(1)).subscribe((res: UnitI) => {
        let staffArr = res.positions[file.id.slice(1)].staff;
        if (!staffArr) {
          this.positionService.delPosition(file.id.slice(1)).subscribe(delRes => {
            this.action.fetchGetInfo();
          });
        } else if (!staffArr.some(el => el instanceof Array)) {
          this.positionService.delPosition(file.id.slice(1)).subscribe(delRes => {
            this.action.fetchGetInfo();
          });
        } else {
          this.action.treeViewError(true);
        }
      });
    } else {
      if ((!file.directories || !file.directories.length) && (!file.files || !file.files.length)) {
          this.action.openDelModal(true, file);
          this.hideScrollBar();
      } else {
        this.action.treeViewErrorU(true);
      }
    }

  }
  modalOpen(index, shiftIndex, shift, parent) {
    ParseOrganisationService.unitId = {};
    ParseOrganisationService.unitId = {
      index: index,
      shiftIndex: shiftIndex, shift: shift, parent: parent
    };
    this.loginModalOpened = true;
    this.hideScrollBar();
  }

  modalOpenFile(index, fileIndex, shiftIndex, shift, parent) {
    ParseOrganisationService.unitId = {};
    ParseOrganisationService.unitId = {
      index: index,
      fileIndex: fileIndex,
      shiftIndex: shiftIndex,
      shift: shift,
      parent: parent
    };
    this.loginModalOpened = true;
    this.hideScrollBar();
  }

  addModalOpen(dir, index, type) {
    ParseOrganisationService.addModalData = {
      index: index,
      dir: dir,
      type: type
    };
    this.addModalOpened = true;
    this.hideScrollBar();
  }

  stopEvent(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  addActive(id) {
    ParseOrganisationService.activeId = id;
  }

  rename(el, event ) {
    if (el.type === 'company') {
      let params = {
        cname: el.name,
        company_id: +el.data.company_id
      };
      this.companyService.putCompany(params).subscribe(res => {
        el.name = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    } else if (el.type === 'position') {
      let params = {
        pname: event.target.value,
        position_id: +el.id.slice(1)
      };
      this.positionService.rename(params).subscribe(res => {
        el.text = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    } else {
      let params = {
        nname: event.target.value,
        type: el.type,
        unit_id: el.id.slice(1)
      };
      this.unitService.put(params).subscribe(res => {
        el.name = event.target.value;
        el.text = event.target.value;
        el.editable = false;
        this.action.fetchGetInfo();
      });
    }
  }
}

interface UnitI {
  positions: any;
}

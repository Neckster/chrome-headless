export class Directory {
  id: any;
  name: string;
  directories: Array<any>;
  files: Array<any>;
  expanded: boolean;
  checked: boolean;
  type: any;
  data: any;
  pShift: any;
  constructor(id, name, directories, files, type, data?) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.files = files;
    this.directories = directories;
    this.expanded = false;
    this.checked = false;
    if (data) this.data = data;
  }
  toggle() {
    this.expanded = !this.expanded;
  }
  check() {
    let newState = !this.checked;
    this.checked = newState;
    this.checkRecursive(newState);
  }
  checkRecursive(state) {
    this.directories.forEach(d => {
      d.checked = state;
      d.checkRecursive(state);
    });
  }
}

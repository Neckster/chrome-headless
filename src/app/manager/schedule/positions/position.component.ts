import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy }     from '@angular/core';

import { select }               from 'ng2-redux';
import * as moment              from 'moment/moment';
import * as R                   from 'ramda';

import { Cache }                from '../../../services/shared/cache.service';
import { EmitService }          from '../../../services/emit.service';
import { ScheduleService }      from '../../../services/shared/SchedulService';

import { MapToIterable }        from '../../../lib/pipes/map-to-iterable';
import { DurationPipe }         from '../../../lib/pipes/duration';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';
import { IconState }            from '../../../lib/enums/IconState';

import { htmlPosition }         from './position-template.htm.ts';
import { StaffGreedController } from '../staff-grid/staff-grid.component';

const getDayTimes = R.pipe(
  R.map(R.path(['shiftInfo', 'daysInfo'])),
  R.map(R.pluck('totalDayTime'))
) as any;

const timePerShift = R.pipe(
  getDayTimes,
  R.map(R.reduce((a, c) => a += moment.duration(`${c}:00`) as any, 0))
);

const positionsPerDayTransducer = R.pipe(
  getDayTimes,
  R.transpose,
  R.map(R.reduce((a, c) => a += moment.duration(`${c}:00`) as any, 0))
);

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'position-controlling',
  directives: [StaffGreedController, SvgTemplateDirective],
  pipes: [MapToIterable, DurationPipe],
  template: htmlPosition,
  styles: [`
    .positionNotification {
          position:absolute;
          top:15px;
          right: -1.5rem;
    }
    .positionTotalHours{
      margin-right: -10px;
    }
    @media (max-width: 1000px) {

      .positionTotalHours{
        margin-right: 0;
      }
    }
    @media (max-width: 750px) {
       .positionNotification {
            right: 3.5rem;
      }
    }
    @media (max-width: 479px) {
      .positionTotalHours {  
        margin-right: 20px !important;
        
      }
      .positionNotification{
         right: 26%;
      }

    }
   `]
})
export class PositionSchedule {

  @select(state => state.saveScheduleWeek) week$: any;

  @Input() currentPosition;
  @Input() currentUnit;
  @Input() scheduled;

  @Output() changedPosition = new EventEmitter();
  @Output() recalculatedTotals = new EventEmitter(true);

  public week;
  public curPos;

  private showList: boolean = false;
  private arrayIteration = new Array(7);
  private positionStatus;

  private timePerShift;
  private positionTotal;
  private unpubArray = [0, 0, 0, 0, 0, 0, 0];

  constructor(public cache: Cache, public scheduleService: ScheduleService,
              public emitter: EmitService) {

    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    });
  }

  ngOnInit() {
    this.emitter.addEvent(this.getUpdateEventName()).subscribe(el => {
      this.totalPositionEmitters(el);
    });
    this.showList = this.cache.isInArray(
      'schedule:positionOpened',
      `pos_${this.currentPosition.position_id}`
    );
    if (this.currentPosition && this.currentPosition.unpubArray) {
      this.unpubArray = this.currentPosition.unpubArray;
    }

    this.updatePositionsStatus(this.currentPosition);
  }


  ngOnChanges() {
    this.timePerShift = timePerShift(this.currentPosition.shift);
    this.positionTotal = R.sum(this.timePerShift);
    this.recalculatedTotals.emit(positionsPerDayTransducer(this.currentPosition.shift));
  }

  toggleShiftCollapsible() {
    if (this.showList) {
      this.cache.removeFromArray(
        'schedule:positionOpened',
        `pos_${this.currentPosition.position_id}`
      );
    } else {
      this.cache.pushArray(
        'schedule:positionOpened',
        `pos_${this.currentPosition.position_id}`
      );
    }
    this.showList = !this.showList;
  }

  totalPositionEmitters(obj) {
    let stArray = [];
    let positionUnpublished = 0;
    this.unpubArray = [0, 0, 0, 0, 0, 0, 0];
    for (let i = 0; i < 4; i++) {
      if (!this.currentUnit.shift[i].active) continue;

      let dayInfo = this.currentPosition.shift[i].shiftInfo.daysInfo[obj.weekday - 1];
      if (dayInfo.targetInfo &&
        dayInfo.dayAssigns < dayInfo.targetInfo.min_job_count) {
        stArray.push(IconState.Bad);
      } else if (dayInfo.dayAssigns === 0) {
        stArray.push(IconState.Empty);
      } else {
        // let stat = dayInfo.targetStatus ? 'Bad' : 'Good';
        let stat = dayInfo.targetStatus ? 'Bad' :
          dayInfo.targetInfo ? 'Good' : 'Need';

        if (!dayInfo.targetStatus  && dayInfo.targetInfo &&
          dayInfo.targetInfo.min_job_count === 0 &&
          dayInfo.targetInfo.max_work_time === 0 ) { stat = 'Need'; }

          stArray.push(IconState[stat]);
      }
      positionUnpublished += obj.position.shift[i].shiftInfo.shiftUnpublished;
      if (obj.position.shift && obj.position.shift[i].shiftInfo) {
        obj.position.shift[i].shiftInfo.daysInfo.forEach( (el, i) => {
          this.unpubArray[i] += el.unpublishedDay;
        });
      }
    }
    // debugger;
    let positionStatus = this.scheduleService.assignIconClass(stArray, (obj.weekday - 1));
    obj.position.unpublish = positionUnpublished;
    obj.position.positionStatus[obj.weekday - 1] = positionStatus;
    this.emitter.emit('changeTree', 'position');
  }

  updatePositionsStatus(pos) {
    let shift = pos.shift.filter((el, i) => this.currentUnit.shift[i].active),
      shiftsInfo = [],
      status = [],
      weekRange = R.range(0, 7);
    shift.map((sEl, index) => {
      let daysInfo = sEl.shiftInfo.daysInfo,
        shiftStatus = [];
      weekRange.forEach(day => {
        if (daysInfo[day].targetInfo &&
          daysInfo[day].dayAssigns < daysInfo[day].targetInfo.min_job_count) {
          shiftStatus.push(IconState.Bad);
        } else if (daysInfo[day].dayAssigns === 0) {
          shiftStatus.push(IconState.Empty);
        } else {
          let stat = daysInfo[day].targetStatus ? 'Bad' :
            daysInfo[day].targetInfo ? 'Good' : 'Need';

          if (!daysInfo[day].targetStatus  && daysInfo[day].targetInfo &&
              daysInfo[day].targetInfo.min_job_count === 0 &&
              daysInfo[day].targetInfo.max_work_time === 0) { stat = 'Need'; }

          shiftStatus.push(IconState[stat]);
        }
      });
      // debugger;
      shiftsInfo.push(shiftStatus);
    });
    weekRange.forEach(key => {
      let stArray = [];
      shiftsInfo.map(info => {
        stArray.push(info[key]);
      });
      let time = 'time_future';
      if (this.currentPosition.positionStatus[key]) {
        time = ' ' + this.currentPosition.positionStatus[key].split(' ')[1];
      }
      status.push(this.scheduleService.assignIconClass(stArray) + time);
    });

    this.currentPosition.positionStatus = status;
  }

  getUpdateEventName() {
    return 'updatePositionEmitter[' + this.currentPosition.position_id + ']';
  }
}

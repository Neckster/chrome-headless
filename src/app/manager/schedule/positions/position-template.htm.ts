export const htmlPosition:string = `
<div [ngClass]="{'collapsed': !showList}" class="pos section">
  <div class="uk-clearfix schedule-row group collapsed">
    <div class="uk-width-1-1 uk-clearfix">
      <div
           class="uk-width-small-10-10 uk-width-medium-3-10 uk-float-left gridleft">
        <div class="uk-grid uk-grid-collapse uk-position-relative callefthead">
          <div class="uk-width-small-1-10 uk-width-large-1-10 secshowhide" >
            <div class="secshow">
              <span class="icon large arrow-right grey" style='cursor:pointer' (click)="toggleShiftCollapsible()">&nbsp;</span>
            </div>
            <div class="sechide">
              <span class="icon large arrow-down blue"  style='cursor:pointer' (click)="toggleShiftCollapsible()">&nbsp;</span>
            </div>
          </div>
          <div  class="text uk-width-small-6-10 uk-width-large-5-10" style="white-space: nowrap">
              {{currentPosition.pname}}
           </div>
           <div class="uk-width-small-1-10 uk-width-large-1-10">
             <span class="positionNotification">
                <span *ngIf="currentPosition.unpublish" class="uk-badge uk-badge-notification uk-float-left " >{{currentPosition.unpublish}}</span>
             </span>
          </div>
           <div class="uk-width-small-2-10 uk-width-large-2-10">
              <span class="uk-float-right positionTotalHours" style="white-space: nowrap">T: {{positionTotal | duration}}{{ 'abbr_hour_letter' | translate }}</span>
           </div>


        </div>
      </div>
      <!--  gridleft -->
      <div class="uk-hidden-small uk-width-medium-7-10 uk-float-left gridright">
        <div class="uk-width-1-1 rowhead">
          <div class="uk-grid uk-grid-collapse sevendays">
            <div class="uk-width-1-1">
              <div *ngFor="let item of arrayIteration; let i = index" class="calday">
                <div class="{{currentPosition.positionStatus[i]}} calstat nosel">
                  <div class="calstat_icons cstat_good_mark">
                    <i [svgTemplate]="'tickInsideCircle'"></i>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_need_mark">
                    <i [svgTemplate]="'tickInsideCircleNeed'"></i>
                    <span  *ngIf='unpubArray[i]'class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_bad_mark">
                    <i [svgTemplate]="'exclamationInsideCircle'"></i>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_empty_mark">
                    <div class="icon plus circle"></div>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>
                  
                </div>
              </div>
            </div>

          </div>
          <!-- sevendays uk-grid  -->
        </div>
        <!-- rowhead -->
      </div>
      <!-- gridright -->
    </div>
  </div>
  <div *ngIf="showList">
    <div *ngFor="let shift of currentPosition.shift; let i = index" class="secbody">
    <div *ngIf="currentUnit.shift[i].active" > <!-- changed from shift.active -->
      <staff-grid (updatePositionEmitter)="totalPositionEmitter($event)"
                         [overallTiming]="timePerShift[i]"
                         [currentUnit]="currentUnit"
                         [currentPosition]="currentPosition"
                         [shiftIndex]="i"
                         [scheduled]="scheduled"
                         [shift]="shift"
                         [shiftPosition]="false">
      </staff-grid>
    </div>
    </div>
  </div>

</div>
`;

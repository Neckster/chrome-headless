export const printViewHTML = `
  <div class="printView uk-width-1-1" (click)="closeTagsList($event)">
    <div class="filterBlock uk-display-inline-block uk-float-left">
      <div class="uk-width-1-1">
        <div class="buttonWrap uk-display-inline-block ">
          <a class="buttonPrint" [href]="urlPrint">{{'print_PDF' | translate}}</a>
        </div> 
        <!--<div class="buttonWrap uk-display-inline-block ">
          <button class="buttonSave">{{'save_XLS' | translate}}</button>
        </div>-->
      </div> 
      <div class="uk-width-1-1 filterBody uk-grid" [formGroup]="positionsControllers">
        <h2>{{'Print_options' | translate}}</h2> 
        <div class="filterContent filterTitle uk-width-1-1 uk-margin-small-top">
              <div class="uk-width-4-6 uk-float-left">{{'OPTION'|translate}}</div>  
              <div class="uk-width-1-6 uk-float-left">{{'ON'|translate}}</div>  
              <div class="uk-width-1-6 uk-float-left">{{'OFF'|translate}}</div>  
        </div>
        <div class="filterContent uk-width-1-1"  >
          <div class="uk-width-4-6 uk-float-left">{{'Unpublished'|translate}}</div>  
          <div class="uk-width-1-6 uk-float-left">
            <label class="unpulished">
              <input type="radio" id="unpulishedTrue" value="true" formControlName="unpublished" name="unpublished">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6  uk-float-left">
            <label class="unpulished" >
              <input type="radio" value="false" id="unpulishedFalse" formControlName="unpublished" name="unpublished">
              <span></span>
            </label>
          </div>  
        </div>
        <div class="filterContent uk-width-1-1"  >
          <div class="uk-width-4-6 uk-float-left">{{'Days_off'|translate}}</div>  
          <div class="uk-width-1-6 uk-float-left">
            <label class="daysOff">
              <input value = "true" type="radio" id="daysOffTrue" formControlName="days_off"  name="days_off">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6  uk-float-left">
            <label class="daysOff" >
              <input type="radio" id="daysOffFalse" value="false" formControlName="days_off" name="days_off" >
              <span></span>
            </label>
          </div>  
        </div>
        <div class="filterContent uk-width-1-1">
          <div class="uk-width-4-6 uk-display-inline-block uk-float-left">{{'Position'|translate}}</div>  
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="postions">
              <input type="radio" id="positionTrue" value="true" formControlName="positions" name="positions">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="postions" >
              <input type="radio" id="positionFalse" value="false" formControlName="positions" name="positions">
              <span></span>
            </label>
          </div>  
        </div>
        <div class="filterContent uk-width-1-1">
          <div class="uk-width-4-6 uk-display-inline-block uk-float-left">{{'Shift Notes'|translate}}</div>  
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="shift">
              <input  value="true" type="radio" id="shiftTrue" formControlName="shift_notes" name="shift_notes">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="shift" >
              <input type="radio" value="false" id="shiftFalse" formControlName="shift_notes" name="shift_notes">
              <span></span>
            </label>
          </div>  
        </div>
        <div class="filterContent uk-width-1-1">
          <div class="uk-width-4-6 uk-display-inline-block uk-float-left">{{'Day Notes'|translate}}</div>  
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="dayNotes">
              <input type="radio" value="true" id="dayNotesTrue" formControlName="day_notes" name="day_notes">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="shift" >
              <input type="radio" value="false" id="dayNotesFalse" formControlName="day_notes" name="day_notes">
              <span></span>
            </label>
          </div>  
        </div>
        <div class="filterContent uk-width-1-1">
          <div class="uk-width-4-6 uk-display-inline-block uk-float-left">{{'Leadership'|translate}}</div>  
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="leader">
              <input  type="radio" value='true' id="leaderTrue" name="leadership" formControlName="leadership">
              <span></span>
            </label>
          </div>
          <div class="uk-width-1-6 uk-display-inline-block uk-float-left">
            <label class="leader" >
              <input type="radio" value="false" id="leaderFalse" name="leadership" formControlName="leadership" >
              <span></span>
            </label>
           
          </div>  
        </div>
      </div>
      <div class="uk-width-1-1 filterFooter uk-grid">
        <h2>{{'Print By' | translate}}</h2> 
        <div class="filterContent allPositions uk-width-1-1 uk-margin-small-top">
            <div class="tagInput" (click)="openTagsList($event)">
              <div *ngFor="let t of selectedTags" 
                    class="uk-display-inline-block positionTag"
                    (click)="openTagsList($event, selectedTags)">
                <span class="selectedTagName">{{tagsList[t]}}</span><span class="cross" (click)="deletePosition($event, t)"></span>
             </div>
            </div>
           <ul *ngIf='availableTags.length' 
                  class="tagDropDown uk-list"
                  [ngClass] ="{'activeScrolling': availableTags.length > 4}"
                  [style.display]="showTagDropDown ?  'block' : 'none' ">
              <li  [ngClass] ="{'animationHeight': showTagDropDown}" (click)='selectedAll($event)'>{{'All position' | translate}}</li>
              <li *ngFor="let p of availableTags" (click)="selectTag($event, p)">
                <span>{{tagsList[p]}}</span>
              </li>
            </ul>
        </div>
      </div>
    </div>
      
    <div class="contentBlock uk-display-inline-block " style="clear: both">
         <div class="uk-width-1-1 contentTitle">
            <h1>{{'Print Preview' | translate}}</h1>
            <div class="close">
              <span (click)="closePrintView()">{{'CLOSE'}}</span>
            </div>
         </div>
         
         <div style="width: 100%; margin: 0 auto;" [style.height]=" iframeHeight ? iframeHeight+ 'px': '100%'">
          <iframe [src]="urlPreview" id="iframeBody"
                  frameborder="0" scrolling="no"
                  #iframeVar
                  (load)="resizeIframe(iframeVar)" 
                  style="width: 100%; height: 100%;" ></iframe>
         </div>
        
       

         <!--<a style="margin: 1rem; color:#07a0e1; background: #fff;" class="uk-button uk-display-inline-block" [href]="urlPrint">
            <i [svgTemplate]="'printIcon'"></i>
            Print
         </a>-->
         
    </div>
  </div>
`;

import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener }                     from '@angular/core';
import {
  DomSanitizationService,
  SafeResourceUrl }                  from '@angular/platform-browser';
import * as moment                   from 'moment';
require('moment-range');
import { select }                    from 'ng2-redux/lib/index';
import * as R                        from 'ramda';
import { AppStoreAction }            from '../../../store/action';
import { JobService }                from '../../../services/shared/JobService';
import { Cache }                     from '../../../services/shared/cache.service';
import { MapToIterable }             from '../../../lib/pipes/map-to-iterable';
import { SortNamePipe }              from '../../../lib/pipes/sortName';
import { printViewHTML }             from './print-view.html';
import { DatePikerComponent }        from '../../date-picker/date-picker';
import { FormGroup, FormControl }    from '@angular/forms';
import { NodeApiHttp }               from '../../../services/shared/node-http-api.service';
import { URLSearchParams }           from '@angular/http';
import { DestroySubscribers }        from '../../../lib/decorators/unsub.decorator';
import { Subscriber }                from 'rxjs';

@Component({
  selector: 'print-view',
  directives: [ DatePikerComponent ],
  pipes: [MapToIterable, SortNamePipe],
  styles: [require('./style.less')],
  template: printViewHTML
})
@DestroySubscribers()
export class PrintViewComponent {

  @select() info$;
  @select() currentUnit$;
  @select(state => state.localisation) lang$: any;
  @Input() positionsIn;
  @Input() week;
  @Input() weekDay;
  @Input() unitId;

  public subscribers: any = {};

  public currentUnit;
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public timeTemplate = [];
  private lang;
  private info;
  private webuserId;
  private tagsList: any;
  private selectedTags = [];
  private availableTags = [];
  private now: any;
  private showTagDropDown: boolean = false;
  private positionsControllers = new FormGroup({
    unpublished: new FormControl('false'),
    days_off: new FormControl('false'),
    positions: new FormControl('true'),
    shift_notes: new FormControl('true'),
    day_notes: new FormControl('true'),
    leadership: new FormControl('true'),
    position_list: new FormControl([])
  });

  private urlPreview: SafeResourceUrl;
  private urlPrint: SafeResourceUrl;

  private buildPreviewUrl;
  private buildPrintUrl;
  private iframeTag;
  private iframeHeight = 150;

  constructor(public action: AppStoreAction, public cache: Cache,
              public jobService: JobService, private node: NodeApiHttp,
              private sanitizer: DomSanitizationService) {


    this.subscribers = {
      [Symbol('lang')]: this.lang$.subscribe(data => {
        this.lang = data;
      }),
      [Symbol('info')]: this.info$.subscribe(info => {
        this.info = info;
        if ( this.info && this.info.webuser ) {
          this.webuserId = this.info.webuser.webuser_id;
        }
      })
    };

    if (this.cache.get('dayView:dateOnDayView')) {
      let getCache = this.cache.get('dayView:dateOnDayView');
      this.now = moment(getCache);
    }

    this.buildPreviewUrl = this.buildUrl('preview');
    this.buildPrintUrl = this.buildUrl('print');
  }

  ngOnInit() {
    if (this.positionsIn && this.positionsIn.length) {
      let res = this.positionsIn.map(p => p.position_id);
      this.positionsControllers.patchValue({position_list: res});
      this.selectedTags = this.positionsControllers.value.position_list;
      this.tagsList = this.createTagList(this.positionsIn);
      this.availableTags = this.findDiffElement(this.tagsList, this.selectedTags);
      this.updateDocumentUrl(this.positionsControllers.value);
    }

    this.positionsControllers.valueChanges.subscribe(val => {
      this.selectedTags = val.position_list;
      this.availableTags = this.findDiffElement(this.tagsList, val.position_list);
      this.updateDocumentUrl(val);
      this.resizeIframe();
    });
  }

  updateDocumentUrl(params: {[key: string]: any}) {

    this.urlPreview = this.buildPreviewUrl(Object.assign({}, params, {
      unit_id: this.unitId,
      webuser_id: this.webuserId,
      lang: this.lang,
      week: this.week
    }));

    this.urlPrint = this.buildPrintUrl(Object.assign({}, params, {
      unit_id: this.unitId,
      webuser_id: this.webuserId,
      lang: this.lang,
      week: this.week
    }));

  }

  buildUrl(type: 'preview' | 'print'): any {
    return function (qs: any): SafeResourceUrl {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        encodeURI(`${this.node.baseUrl}/${type}/schedule/week${this.node.jsonToQueryString(qs)}`)
      );
    };
  }

  findDiffElement(all, ins) {
    let obj = Object.assign({}, all);
    ins.forEach(el => delete obj[el]);
    return Object.keys(obj).map(el => +el);
  }

  createTagList(arr) {
    let resObj = {};
    arr.map(p => {
      resObj[p.position_id] = p.pname;
      return resObj;
    });
    return resObj;
  }

  closePrintView() {
    this.action.openWeekPrintView(false);
    this.action.openDayPrintView(false);
  }

  openTagsList(e, arr) {
    e.preventDefault();
    e.stopPropagation();
    this.showTagDropDown = true;
  }
  closeTagsList(e) {
    this.showTagDropDown = false;
  }

  deletePosition(e, id) {
    e.preventDefault();
    e.stopPropagation();
    if (this.positionsControllers
            .value
            .position_list.length <= 1) return;

    this.positionsControllers.patchValue({
        position_list: this.selectedTags.filter(el => el !== +id)
      });
    this.closeTagsList(event);
  }


  selectTag(e, id) {
    this.selectedTags.push(+id);
    this.positionsControllers.patchValue({
      position_list: this.selectedTags
    });
    this.closeTagsList(event);
  }

  selectedAll(e) {
    let sumArr = this.selectedTags.concat(this.availableTags);
    this.positionsControllers.patchValue({
      position_list:  sumArr
    });
    this.closeTagsList(event);
  }

  resizeIframe(obj?) {
    if (obj) this.iframeTag = obj;

    this.iframeHeight = this.iframeTag ?
          this.iframeTag.contentWindow.document.body.scrollHeight : 0;
  }

}

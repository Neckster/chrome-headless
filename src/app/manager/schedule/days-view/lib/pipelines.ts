import * as R                        from 'ramda';
import { THRESHOLD_MAX_HOUR }        from './constants';

export const groupByPositionByUser = R.pipe(
  R.groupBy(R.prop('position_id')) as any,
  R.map(R.groupBy(R.prop('webuser_id'))) as any
);

export const getOverallSum = R.pipe(
  R.filter((v: any) => R.not(R.isEmpty(v))),
  R.map(R.zipWith((a, b) => { return { hour: a, val: b }; }, R.range(0, THRESHOLD_MAX_HOUR))),
  R.values,
  R.flatten,
  R.groupBy(R.prop('hour')) as any,
  R.map(R.pluck('val')),
  R.map(R.sum)
);

// extract ranges from list
export const getRanges = R.pipe(
  R.map(R.map(R.pluck('range'))),
  R.map(R.values),
  R.map(R.flatten)
) as any;

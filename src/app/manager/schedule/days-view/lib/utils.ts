import * as R       from 'ramda';
import * as moment  from 'moment';
require('moment-range');

import {
  LAST_FULL_HOUR,
  NEG_ONE_CORRECTION,
  NO_CORRECTION,
  ZERO,
  DAY_RANGE,
  POS_ONE_CORRECTION,
  POS_TWO_CORRECTION } from './constants';


interface ITimeRange {
  from: number;
  to: number;
}

const MONDAY_IDX: number = 1; // 1
const SUNDAY_IDX: number = 0;  //
const SATURDAY_IDX: number = 6; // 7

// getIndices : number -> object
export const getIndices = (dayIdx: number) => {

  /* tslint:disable:no-null-keyword */
  let indices = {
    prev: null, current: null, next: null
  };
  /* tslint:enable:no-null-keyword */

  if (dayIdx === SUNDAY_IDX) {
    indices.prev = 6;
    indices.current = 7;
  } else if (dayIdx === SATURDAY_IDX) {
    indices.prev = 5;
    indices.current = 6;
    indices.next = 7;
  } else if (dayIdx === MONDAY_IDX) {
    indices.current = 1;
    indices.next = 2;
  } else {
    indices.prev = dayIdx - 1;
    indices.current = dayIdx;
    indices.next = dayIdx + 1;
  }

  return indices;

};

// pairFactory :: a -> b -> [a, b]
const pairFactory = (unit) => (n) => [n, unit];

// partially applying `pairFactory` to create custom factories (hours, minutes)
export const hourPairFactory = pairFactory('h');
export const minutePairFactory = pairFactory('m');
export const dayPairFactory = pairFactory('d');

export const oneHour = hourPairFactory(1); // [1, 'h']
export const oneDay = dayPairFactory(1); // [1, 'd']


// Object.assign -> extend (as native assign implicitly evaluates getters)
export const extend = (target, ...sources) => {
  for (let source of sources) {

    if (Object.keys(source).length) {
      for (let key of Object.keys(source)) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      }
    }

    if (Object.keys(Object.getPrototypeOf(source)).length) {
      for (let key of Object.keys(Object.getPrototypeOf(source))) {
        Object.defineProperty(target, key,
          Object.getOwnPropertyDescriptor(Object.getPrototypeOf(source), key));
      }
    }

  }
  return target;
};

// leftPad :: number[] -> string[]
// prepends each elem with the ':00' (formatting hours for UI)
export const leftPad = (arr: number[]) => R.map(v => v + ':00', arr);

// create one-hour ranges for the whole day range (day lasts to the 4.59am of the next day)
// @param day - current day moment obj
// @param rotating - flag indicates if shift rotates
// getFullDayRanges :: moment -> moment.range[]
export const getFullDayRanges = (day?: moment.Moment, rotating: boolean = false): any[] => {

  // by def. day timeline starts at 5am
  let dayStart = day.clone()
    .hours(DAY_RANGE.START.HOURS)
    .minutes(DAY_RANGE.START.MINUTES)
    .seconds(ZERO)
    .milliseconds(ZERO);


  // and ends at 4.59 next day
  let dayEnd = (<any>day).clone().add(...oneDay)
    .hours(DAY_RANGE.END.HOURS)
    .minutes(DAY_RANGE.END.MINUTES)
    .seconds(59)
    .millisecond(59);

  // init timeline range
  let dayTimeRange = (<any>moment).range(dayStart, dayEnd);

  let accum = [];

  // split timeline range into 1-hour ranges
  dayTimeRange.by('hours', (m) => {
    // init range limits
    let rangeStart = m;
    let rangeEnd = m.clone().add(...oneHour);
    // sub 1 min if it's last hour in a day
    // //m.hours() === LAST_FULL_HOUR && !rotating ? NEG_ONE_CORRECTION : NO_CORRECTION;
    let correction = NO_CORRECTION;
    rangeEnd = rangeEnd.add(...minutePairFactory(correction));
    // init range
    let oneHourRange = (<any>moment).range(rangeStart, rangeEnd);
    accum = accum.concat(oneHourRange);
  });

  return accum;

};

// calcDiff :: (moment, dayIndex, assignment) -> { tsfrom, tsto }
// calculates actual starting end ending points for the assignments
export const calcDiff = (
  moment: moment.Moment,
  dayIndex: number,
  { weekday, from, to }): { tsfrom: any, tsto: any, isRotating: boolean } => {

  let diff: number = weekday - dayIndex;

  if (diff === 6) diff = -1;
  if (diff === 7) diff = 0;

  // todo use getIndices
  // console.log(getIndices(dayIndex));

  const fromInt: number = parseInt(from, 10);
  const toInt: number = parseInt(to, 10);

  const [ fromHours , fromMinutes ] = from.split(':');
  const [ toHours , toMinutes ] = to.split(':');

  let f = parseInt(fromHours, 10) * 60 + parseInt(fromMinutes, 10);
  let t = parseInt(toHours, 10) * 60 + parseInt(toMinutes, 10);

  // cross-assignment spans for two days
  const isXAssignment: boolean = f > t;

  let correction: ITimeRange = {
    from: 0,
    to: 0
  };

  let correctionTransformation = {};

  switch (diff) {
    case -1: // prev day
      if (isXAssignment) {
        correctionTransformation = {
          from: R.add(NEG_ONE_CORRECTION)
        };
      } else {
        correctionTransformation = {
          from: R.add(NEG_ONE_CORRECTION),
          to: R.add(NEG_ONE_CORRECTION)
        };
      }
      break;
    case 0: // current
      if (isXAssignment) {
        correctionTransformation = {
          to: R.add(POS_ONE_CORRECTION)
        };
      }
      break;
    case 1: // next
      if (isXAssignment) {
        correctionTransformation = {
          from: R.add(POS_ONE_CORRECTION),
          to: R.add(POS_TWO_CORRECTION)
        };
      } else {
        correctionTransformation = {
          from: R.add(POS_ONE_CORRECTION),
          to: R.add(POS_ONE_CORRECTION)
        };
      }
      break;
    default: // empty
  }

  let corrected: any = R.evolve(correctionTransformation as any, correction);

  return {
    tsfrom: (<any>moment).clone()
      .add(...dayPairFactory(corrected.from))
      .hours(fromInt)
      .minute(fromMinutes)
      .seconds(ZERO)
      .milliseconds(ZERO),
    tsto: (<any>moment).clone()
      .add(...dayPairFactory(corrected.to))
      .hours(toInt)
      .minute(toMinutes)
      .seconds(ZERO)
      .milliseconds(ZERO),
    isRotating: isXAssignment
  };

};

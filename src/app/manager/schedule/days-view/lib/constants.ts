export const LAST_FULL_HOUR: number = 4;

export const NO_CORRECTION: number = 0;
export const NEG_ONE_CORRECTION: number = -1;
export const POS_ONE_CORRECTION: number = 1;
export const POS_TWO_CORRECTION: number = 2;

export const THRESHOLD_MAX_HOUR: number = 24;
export const THRESHOLD_MIN_HOUR: number = 5;

export const ZERO: number = 0;

export const DAY_RANGE = {
  START: {
    HOURS: 5,
    MINUTES: 0
  },
  END: {
    HOURS: 4,
    MINUTES: 59
  }
};

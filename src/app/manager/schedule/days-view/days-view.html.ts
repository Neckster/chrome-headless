export const daysViewHtml: string = `
  <div *ngIf='unit' class="page schedule plannerview uk-clearfix" data-view="schedule" style=" position: relative; top:0" >
  <!--<div *ngIf="openPublishModal$ | async" >
    <publish-modal [unit]="unit" [showDay]="showDay"></publish-modal>
  </div>-->
  <div class="uk-width-1-1  dayViewLeft dayViewTitle">
            <h2 class="uk-width-1-1 titleDate" style="margin:0!important;">{{showDay.format('dd  D.MM.YYYY')}}</h2>
            <div class="uk-width-1-1" 
              [ngClass]="{'titleNote': weeklyNotes[dayIdx > 0 ? dayIdx - 1 : 6]?.body.trim().length}">
              {{ weeklyNotes[dayIdx > 0 ? dayIdx - 1 : 6]?.body }}
            </div>
          </div>
     <!--<div class="uk-width-small-6-10 uk-width-medium-3-10 uk-width-large-3-10 uk-text-right uk-margin-bottom publishButtonStyle" >
        <button [disabled]="!unpubStaff.length" (click)="opebPublishModal($event)" type="button" class="uk-button uk-width-1-1 uk-text-truncate">
          <span class="enabled">{{'Publish_changes' | translate}}  <span> {{unpubStaff.length}} </span> </span>
          <span class="disabled">{{'changes_plural_zero' | translate}}</span>
        </button>
      </div>-->
  <div  class="dayViewScroll uk-grid uk-grid-collapse uk-text-left uk-padding-remove" style="width: 103%; min-height: 400px; height: 100%; " #scrollElement (scroll)="onScroll($event, dayViewContentVal)">
    <div class="uk-width-1-1 uk-container-center uk-margin-bottom " id="schedule-grid"  >
      <!-- 100% width -->
			<div id='dayViewStickyContainerId' [style.left]="moveHeaderleft" style="min-width: 1080px" class="uk-width-1-1 uk-grid uk-grid-collapse "  >
			  <div   class='uk-width-1-1' style="width: 1080px;">
					<div  class=" dayhead nosel dayViewLeft dateBlock" >
						<div class="kw-info uk-flex uk-flex-space-between uk-flex-middle" >
							<div class="kw-action uk-width-7-10">
								<div class="uk-grid uk-grid-collapse">
									<span (click)="loadWeek(-1)"  class="icon arrow-left uk-width-2-10 uk-visible-large"><span
                    class="label">{{'last_week' | translate}}</span></span>
									<span (click)="loadWeek(-1)"  class="icon arrow-left uk-width-2-10 uk-hidden-large"><span
                    class="label">{{'yesterday' | translate}}</span></span>
									<span class="today uk-width-6-10">
                    <span (click)="loadWeek(0)" class="text">{{'today' | translate}}</span>
									<span class="date">Fr, 13. May 2016</span>
									</span>
									<span (click)="loadWeek(1)" class="icon arrow-right uk-width-2-10 uk-visible-large">
									  <span class="label">{{'next_week' | translate}}</span>
									</span>
									<span (click)="loadWeek(1)" class="icon arrow-right uk-width-2-10 uk-hidden-large"><span
                    class="label">{{'tomorrow' | translate}}</span></span>
								</div>
							</div>
							<div class="uk-width-large-3-10 align-center">
                <div style="max-width:30px; padding-left: 25%">
                   <i (click)="showCalendar()" [svgTemplate]="'calendar'" style="cursor: pointer;"></i> 
                  <div *ngIf="showCalendarVar">
                    <my-date-picker (selectedDate)="setDate($event)"
                                    [valueDate]="valueDate"
                                    [valueDay]="showDay"
                                    [dayIn]="dayIn"></my-date-picker>
                  </div>
                </div>
              </div>
						</div>
					</div>
					<div class="dayhead nosel dayViewRight" style="margin: 0; float: left;">
						<div class="kw-days">
							<div class="uk-width-1-1  kw-action">
								<div class="uk-grid uk-grid-collapse ">
									<div class="uk-width-1-1" style="background: #fafafa;">
										<div *ngFor="let a of timeTemplate;let i = index"
										        class="dayTemp day" 
										        [ngClass]="{'dayTempLeft' : i === 23}">
                        <span class="mday">{{timeTemplate[i]}}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
      </div>
      <div id="dayViewContent" #dayViewContentVal style="width: 1080px">
        <div class="uk-clearfix schedule-row heading">
        <div class="uk-width-1-1 dayViewLeft overflowBlock">
          <div class="uk-width-9-10 uk-float-left dayViewUniName overflowBlock">{{unit.nname}}</div>
          <span class="uk-width-1-10 uk-float-left  dayViewTotalHour">T:{{unitTime | duration}}{{'abbr_hour_letter' | translate }}</span>
        </div>
        <div class="dayViewRight" >
          <div *ngFor="let a of dayRange;let i = index"  class="dayTemp day" 
                [ngClass]="{'dayTempLeft' : i === 23, 'activeHourUnit': overallTimigs[a] }">
              <span *ngIf="overallTimigs[a]">{{overallTimigs[a] | duration}}</span>
              <span *ngIf="!overallTimigs[a]">0</span>
          </div>
        </div>
      </div>
        <div *ngIf="unitInfo" style="width: 1080px">
          <div *ngFor="let position of unitInfo|mapToIterable|sortName:'key'; let p = index">
             <positions-day
                (sendUnpubPositions) = 'getPositionData($event)'
                [positionName]="positions[position.key]?.pname"
                [staff]="position.value"
                [dayIdx]="dayIdx"
                [showDay]="showDay"
                [totalHours]="reducedPositionRanges[position.key]">
             </positions-day>
          </div>
      </div>
      </div>
     
    </div>
  </div>
</div>
`

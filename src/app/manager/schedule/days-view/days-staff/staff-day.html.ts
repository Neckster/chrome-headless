export const staffDayHtml = `
  <div *ngIf="true" class="uk-width-1-1 uk-clearfix schedule-row group collapsed staffDay" > 
      <div class="dayViewLeft dayStaff" > 
         <div class='uk-width-2-10' style="padding-left:5px; float: left;">
           <img  src="https://www.gravatar.com/avatar/e9893a9e9c1a2d761c2f43985f9a59d7?d=404"
                 class="jc-my-image"
                 alt=""
                 style="width:45px; border-radius: 50%;">
          <div *ngIf='false'>
            <img  style="width:45px; border-radius: 50%; "
                  src="{{ 'https://www.gravatar.com/avatar/'+ info.webuser.mailhash }}"
                  class="jc-my-image"
                   alt="Profile">
          </div>
        </div>
        <div class='uk-width-8-10 dayStaffInfo' style="float:left;">
          <span class="dayStaffName" > {{ uname }} </span>
          <span class="dayStaffDetail">D: {{totalDayHours | duration }}h</span> 
         </div>
         <span *ngIf='publishCounter' class="custom-notification">{{publishCounter}}</span>          
        <!-- <div class='dayStaffBubbles'>
           <span   class=" uk-float-right"></span>
         </div>-->
      </div>
      <div class="dayViewRight" >
         <div *ngFor="let a of timeTemplate;let i = index"  class="dayTemp day" [ngClass]="{'dayTempLeft' : i === 23}">
            <span [style.visibility]="true ? 'hidden':'' ">{{0}}</span>
         </div>
         
         <div *ngFor="let timeLine of timeLineArray; let s = index">
           <div class="timeLineBlock "
                (click) = '!timeLine.past  && editAssignment(s, timeLine.statusTimeBox)'
                [ngClass]="{'published' : timeLine.published, 
                            'timeBLockInherit' : timeLine.width < 36*3,
                            'pastBox' : timeLine.published && timeLine.past,
                            'nexTimeBox': timeLine.statusTimeBox === 'nextBox'}"
                [style.width]=" timeLine.width ? timeLine.width + 'px' : '0' "
                [style.left]=" calcLeft(timeLine) ">
                <span>{{timeLine.from}}  - </span><span *ngIf="!timeLine.cut">{{timeLine.to}}</span> <span *ngIf="timeLine.chef">(C)</span>
                
                <div  class="timeLineToolTipWrap" [style.display]=" timeLine.width < 36*3 ? 'block': 'none'" >
                   <span class="timeLineToolTip"> 
                      <span>{{timeLine.from}}  - </span><span *ngIf="!timeLine.cut">{{timeLine.to}}</span> <span *ngIf="timeLine.chef">(C)</span>
                   </span>
                </div>
                <i *ngIf="timeLine.info" class="infoIcon" [svgTemplate]="'commentIcon'">
                </i>
                <div *ngIf='timeLine.info' class="datViewInfoToolTip">
                        <span>{{timeLine.info | textCut: 18}}</span>
                 </div>
                
           </div>
           <div
              class="backgroundBlock"
              [style.width]=" timeLine.width ? timeLine.width + 'px' : '0' "
              [style.left]=" calcLeft(timeLine) "
           ></div>
         </div>
         
      </div>
  </div>
  
  
  
`;

import {
  Component,
  Input,
  Output,
  EventEmitter }                      from '@angular/core';
import { AppStoreAction }             from '../../../../store/action';
import { JobService }                 from '../../../../services/shared/JobService';
import { select }                     from 'ng2-redux/lib/index';
import { staffDayHtml }               from './staff-day.html';
import { range, map, path }           from 'ramda';
import * as moment                    from 'moment';
import { animateFactory }             from 'ng2-animate';
import { TextCutPipe }                from '../../../../lib/pipes/textCut';
import { DurationPipe }               from '../../../../lib/pipes/duration';

@Component({
  selector: 'staff-day',
  styles: [ require('../dayViewStyle.less') ],
  template: staffDayHtml,
  pipes: [TextCutPipe, DurationPipe],
  animations: [animateFactory(300, 100, 'ease-in')]
})

export class StaffDayViewComponent {

  @select() info$;
  @select() currentUnit$;
  @Input() jobber;
  @Input() name;
  @Input() showDay;

  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public timeTemplate = [];
  // private left = 0;
  private timeLineArray = [];
  private maxPrevObj: any;
  private minNextObj: any;
  private nextBlockObj: any;
  private k: number = 36; // 1 hour === 1 column === 36px
  private timeBlockObj: any = {};
  private publishCounter: number = 0;
  private totalDayHours: number = 0;
  private staff = {};
  private uname;

  constructor(private action: AppStoreAction, private jobService: JobService) {}

  ngOnInit() {

    // TODO: extract this operation to the upper level!
    this.currentUnit$.subscribe(unit => {
      this.staff = unit.staff;
    });

    let tHours = range(0, 24);
    this.timeTemplate = this.leftPad(tHours);
    if (this.jobber.current) {
      this.jobber.current.map(jobber => {
        this.createTimeLine(jobber);
      });
    }
    if (this.jobber.prev) {
      this.calcPrevShift(this.jobber.prev);
    }
    if (this.jobber.next) {
      this.calcNextShift(this.jobber.next);
    }

    this.uname = (this.staff[this.name] && this.staff[this.name].uname) || '';
  }

  leftPad(arr: number[]) {
    return map (v => (v > 9 ? '' + v : '0' + v) + ':00', arr);
  }

  calcPrevShift(shifts) {
    let maxVal = 0, maxObj;
    let shift = shifts.map( sh => {
      let timeFrom = +sh.from.split(':')[0] + (+sh.from.split(':')[1]) / 60,
        timeTo = +sh.to.split(':')[0] + (+sh.to.split(':')[1]) / 60;
      if (timeFrom > timeTo && timeTo >= 5 && maxVal < timeTo ) {
        maxVal = timeTo; maxObj = sh;
      }
    });
    if (maxObj) {
      this.createTimeLine(maxObj, 'prevBox');
      this.maxPrevObj = maxObj;
    }
  }

  calcNextShift(shifts) {
    let minValue, nextDayObj;
    let shift = shifts.map(sh => {
      let from = +sh.from.split(':')[0] + (+sh.from.split(':')[1]) / 60,
        to =  +sh.to.split(':')[0] + (+sh.to.split(':')[1]) / 60;
      if (from < 5 ) {
        minValue = from; nextDayObj = sh;
      }
    });
    if (nextDayObj) {
      this.createTimeLine(nextDayObj, 'nextBox');
      this.nextBlockObj = nextDayObj;
    }
  }

  createTimeLine(j, stat?) {
    if (!j.webuser_id) return;

    this.checkTotalHours(j);

    let needTime: any,
        checkTime: any = j.to + ' ' + this.showDay.format('DD MMMM YYYY');

    // if stat === undefined calc obj with current time line
    if (!stat) {
      let dis = 5 * this.k,
          fromH = +j.from.split(':')[0],
          fromM = +j.from.split(':')[1],
          toH = +j.to.split(':')[0],
          toM = +j.to.split(':')[1];
      // percentage minute
      let fromMinutes = fromM ? fromM / 60 : 0,
          toMinutes = toM ? toM / 60 : 0;

      // time interval === width of time line block
      if (toH + toMinutes >= fromH + fromMinutes) {

        if (fromH < 5) {
          fromH = 5;
          fromMinutes = 0; }

        if ( toH < 5 ) {
          toH = 5;
          toMinutes = 0; }

        this.timeBlockObj.width = ((toH + toMinutes) - (fromH + fromMinutes)) * this.k;


      } else if (toH + toMinutes < fromH + fromMinutes)  {

          if (fromH < 5) {
              fromH = 5;
              fromMinutes = 0; }

          if (toH >= 5) {
             toH = 5;
             toMinutes = 0; }

          this.timeBlockObj.width = ( 24 - (fromH + fromMinutes)  +  (toH + toMinutes) ) * this.k;

      }
      //  block position on time line
      this.timeBlockObj.left = (fromH + fromMinutes) * this.k - dis;
      // check job status: past or future
      if (j.from > j.to) {
        needTime = moment(checkTime).add(1, 'day');
      }else {
        needTime = moment(checkTime);
      }
      this.timeBlockObj.statusTimeBox = 'currentBox';
      if (!j.published) this.publishCounter++;

    }else if (stat === 'prevBox') {   // if stat === prevBox calc obj with next day

      let toPrev = +j.to.split(':')[0] + (+j.to.split(':')[1]) / 60;
      this.timeBlockObj.width = ( toPrev - 5) * this.k;
      needTime = moment(checkTime);
      this.timeBlockObj.statusTimeBox = 'prevBox';

    }else if (stat === 'nextBox') {  // if stat === nextBox calc obj with next day

      let fromRes = +j.from.split(':')[0] + (+j.from.split(':')[1]) / 60,
          toRes = +j.to.split(':')[0] + (+j.to.split(':')[1]) / 60;
      // debugger;
      toRes = toRes > 5 ? 5 : toRes;
      this.timeBlockObj.width = (toRes - fromRes) * this.k;
      this.timeBlockObj.right = ( 19 + fromRes ) * this.k;
      needTime = moment(checkTime).add(1, 'day');
      this.timeBlockObj.statusTimeBox = 'nextBox';

    }
    this.timeBlockObj.past = needTime.isAfter(moment()) > 0 ? false : true;
    this.timeBlockObj.shift_idx = j.shift_idx;
    this.timeBlockObj.from = j.from;
    this.timeBlockObj.to = j.to;
    this.timeBlockObj.chef = j.chef ? true : false;
    this.timeBlockObj.info = j.info && j.info.message ? j.info.message : false;
    this.timeBlockObj.published = j.published;
    this.timeBlockObj.pname = j.pname;



    this.timeLineArray.push(this.timeBlockObj);
    this.timeBlockObj = {};
  }

  editAssignment(index, statusTimeBox) {
    let job;
    switch (statusTimeBox) {
      case 'prevBox':
        job = this.maxPrevObj;
        break;
      case 'nextBox':
        job = this.nextBlockObj;
        break;
      default:
        job = this.jobber.current[index];
    }
    this.action.openModalAssign(job);
  }

  calcLeft(timeLine) {
    let res = timeLine.left ?
      timeLine.left  : timeLine.right ?
      timeLine.right : 0;
    return res + 'px';
  }

  checkTotalHours(job) {
    let from = moment(`${job.from}`, 'HH:mm'),
        to = moment(`${job.to}`, 'HH:mm'),
        tHours = 0;

    if (from.isBefore(to)) {
      tHours = to.diff(from);
    }else {
      to = to.add(1, 'day');
      tHours =  to.diff(from);
    }
    this.totalDayHours += +tHours;
  }

  ngOnDestroy() {

  }

}

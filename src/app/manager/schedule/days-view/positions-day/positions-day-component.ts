import { Component, Input, Output,
  EventEmitter, HostListener }                        from '@angular/core';
import { AppStoreAction }                             from '../../../../store/action';
import { TargetJobService }                           from '../../../../services/job.service';
import { JobService }                                 from '../../../../services/shared/JobService';
import { select }                                     from 'ng2-redux/lib/index';
import { positionsDayHtml }                           from './positions-day.html';
import { range }                                      from 'ramda';
import { map }                                        from 'ramda';
import * as R                                         from 'ramda';
import { StaffDayViewComponent }                      from '../days-staff/staff-day-component';
import { MapToIterable } from '../../../../lib/pipes/map-to-iterable';
import { SortNamePipe } from '../../../../lib/pipes/sortName';
import { SortStaffByHoursPipe } from '../../../../lib/pipes/staffSortByHour';
import { Cache }        from '../../../../services/shared/cache.service';
import { animateFactory }     from 'ng2-animate';

import { getIndices } from '../lib/utils';

@Component({
  selector: 'positions-day',
  pipes: [MapToIterable, SortNamePipe, SortStaffByHoursPipe],
  styles: [ require('../dayViewStyle.less') ],
  directives: [StaffDayViewComponent],
  animations: [animateFactory(300, 100, 'ease-in')],
  template: positionsDayHtml
})

export class PositionsDayViewComponent {

  @select() info$;

  @Input()
  set positionName(pname: string) {
    this._pname = pname || '';
  }


  @Input() staff;
  @Input() dayIdx;
  @Input() showDay;
  @Input() totalHours;
  @Output() sendUnpubPositions = new EventEmitter();


  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public timeTemplate = [];
  private left = 0;
  private showToolTip: boolean = false;
  private showList: boolean = false;

  private groupedStaff = {};
  private currentPositionId;
  private unpubCounter: number = 0;
  private overallSum: number;
  private _pname: string;
  private dayRange;

  constructor(public action: AppStoreAction,
              public cache: Cache,
              public jobService: JobService) {

  }

  ngOnInit() {
    this.showList = this.cache.isInArray(
      'dayView:positionOpened',
      `pos_${this.currentPositionId}`
    );
  }

  ngOnChanges(change) {
    if (change.staff) this.groupingActions();
  }

  get positionName(): string {
    return this._pname;
  }

  groupingActions() {
    const filterStaff = R.pipe(
      R.map(R.groupBy((v: any) => {
        return v.weekday === getIndices(this.dayIdx).next ? 'next' :
          v.weekday === getIndices(this.dayIdx).prev ? 'prev' : 'current';
      }))
    );

    this.groupedStaff = filterStaff(this.staff);

    this.checkUnpublishedJob(this.groupedStaff);

    let tHours = range(5, 24).concat(range(0, 5));
    this.timeTemplate = this.leftPad(tHours);
    this.dayRange = range(5, 24).concat(range(0, 5));
    this.showToolTip = this.positionName.length > 22 ? true : false;

    this.overallSum = R.sum(this.totalHours);

    if (this.staff && Object.keys(this.staff).length) {
      let objKey = Object.keys(this.staff),
        firstKey = objKey[0];
      if (firstKey) {
        this.currentPositionId = this.staff[firstKey][0].position_id;
      }
    }
  }

  leftPad(arr: number[]) {
    return map (v => (v > 9 ? '' + v : '0' + v) + ':00', arr);
  }

  toggleShiftCollapsible() {
    if (this.showList) {
      this.cache.removeFromArray(
        'dayView:positionOpened',
        `pos_${this.currentPositionId}`
      );
    } else {
      this.cache.pushArray(
        'dayView:positionOpened',
        `pos_${this.currentPositionId}`
      );
    }
    this.showList = !this.showList;
  }

  checkUnpublishedJob( group ) {
    for (let key in group) {
      if (group.hasOwnProperty(key)) {
        if (!group[key].current) continue;
        let unpubJob = group[key].current.map( j => {
          if ( j.published === false ) {
            this.unpubCounter++;
            return j;
          }
          return false;
        }).filter( el => el !== false );
        if (unpubJob.length ) {
          this.sendUnpubPositions.emit({
            action: unpubJob
          });
        }
      }
    }
  }
  ngOnDestroy() {

  }

}

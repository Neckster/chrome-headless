export const positionsDayHtml = `
  <div>
    <div class="uk-clearfix schedule-row group collapsed">
      <div class="dayViewLeft">
        <div class="uk-width-1-10 secshowhide" style="float: left;">
          <div *ngIf='!showList' class="secshow">
            <span class="icon large arrow-right grey" style='cursor:pointer' (click)="toggleShiftCollapsible()">&nbsp;</span>
          </div>
          <div *ngIf='showList' class="sechide">
            <span class="icon large arrow-down blue"  style='cursor:pointer' (click)="toggleShiftCollapsible()"></span> 
          </div>
        </div>
          <div class="uk-width-8-10 dayPositionsName" >
            <span class="dayStaffToolTip "  [ngClass]="{'showToolTip' : showToolTip}">{{ positionName }}</span>
            {{ positionName }}
            <div class="uk-width-1-10 dayBubbles">
               <span *ngIf="unpubCounter"  class="uk-badge uk-badge-notification uk-float-right positionsBubbles">{{unpubCounter}}</span>
            </div>
              
          </div>
          <div class="uk-width-1-10 uk-float-right dayViewTotalHour">
            <span class="">T:{{ overallSum | duration }}{{'abbr_hour_letter' | translate }}</span>  
          </div>
      </div>
      
      <div class="dayViewRight">
        <div *ngFor="let a of dayRange;let i = index" class="dayTemp day " [ngClass]="{'dayTempLeft' : i === 23, 'activeHourPosition': totalHours[a] }">
          <span *ngIf="totalHours[a]">{{totalHours[a] | duration }}</span>
          <span *ngIf="!totalHours[a]">0</span>
        </div>
      </div>
    </div>
    <div *ngIf="showList" >
    
       <div *ngFor="let jobber of groupedStaff|mapToIterable|sortStaffByHours; let sI = index" > <!--|sortStaffByHours -->
          <staff-day  [name]="jobber.key" [jobber]="jobber.value" [showDay]="showDay"></staff-day>  
      </div>
      
    </div>
    
    
   </div>
   
`;

import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener }                     from '@angular/core';
import * as moment                   from 'moment';
require('moment-range');
import { select }                    from 'ng2-redux/lib/index';
import * as R                        from 'ramda';
import { AppStoreAction }            from '../../../store/action';
import { JobService }                from '../../../services/shared/JobService';
import { Cache }                     from '../../../services/shared/cache.service';
import { MapToIterable }             from '../../../lib/pipes/map-to-iterable';
import { SortNamePipe }              from '../../../lib/pipes/sortName';
import { daysViewHtml }              from './days-view.html';
import { PositionsDayViewComponent } from './positions-day/positions-day-component';
import { DatePikerComponent }        from '../../date-picker/date-picker';
import { AssignModalController }     from '../modal-assign/assign.component';
import { PublishModalComponent }     from '../modal-publish/modal-publish-component';
import { ScheduledService }          from '../../../services/scheduled.service';
import * as pipeline                 from './lib/pipelines';

import {
  LAST_FULL_HOUR,
  NO_CORRECTION,
  NEG_ONE_CORRECTION,
  THRESHOLD_MAX_HOUR,
  THRESHOLD_MIN_HOUR } from './lib/constants';

import { hourPairFactory, minutePairFactory, oneHour } from './lib/utils';

import { extend, leftPad } from './lib/utils';

import { getFullDayRanges, calcDiff, getIndices } from './lib/utils';
import { IJob } from '../../../models/Job';

interface IJobComposite extends IJob {
  isInPast?: boolean;
  isRotating?: boolean;
  range?: any;
  tsfrom?: moment.Moment;
  tsto?: moment.Moment;
}

@Component({
  selector: 'days-controlling',
  directives: [PositionsDayViewComponent,
    PublishModalComponent, DatePikerComponent,
    AssignModalController],
  pipes: [MapToIterable, SortNamePipe],
  styles: [require('./dayViewStyle.less')],
  template: daysViewHtml
})

export class ManagerDaysView {

  @select() info$;
  @select() currentUnit$;
  @select(state => state.localisation) lang$: any;
  @select() openPublishModal$;
  // @select() publishModalData$;

  @Input() unit;
  @Input() timeDayview;
  @Input() valueDate;
  @Input() weeklyNotes;

  @Output() changeWeekEvent = new EventEmitter();

  public currentUnit;
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public timeTemplate = [];
  public acc;
  public lang;
  public showDay;
  public userId;
  public showCalendarVar;
  public dayIn;
  public info;
  private unitInfo = {};
  private reducedPositionRanges;
  private overallTimigs = {};
  private dayRange;
  private unitTime: number;

  private dayIdx: number;
  private selectedAssign: any = false;
  private updateInfoDayView: boolean = true;
  private setCache: boolean = false;
  private publishData: any;
  private unpubStaff = [];
  private moveHeaderleft: any;
  private positions;

  constructor(public action: AppStoreAction,
              public scheduledService: ScheduledService,
              public cache: Cache,
              public jobService: JobService) {

    this.unsubscribe.push(this.currentUnit$.subscribe(data => {
      this.currentUnit = data;
      this.positions = data.positions;
    }));

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      let self = this;
      if (this.info.manager) {
        this.userId = info.manager.selected.unit_id;
      }
    }));
    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data;
    }));
    this.unsubscribe.push(this.openPublishModal$.subscribe(open => {
      if (open)  this.sendPulish();
    }));


  }

  @HostListener('window:scroll', ['$event']) track(event) {
    let w = window;
    let fixEl = document.getElementById('dayViewStickyContainerId');
    if (fixEl) {
      if (w.scrollY > 120) {
        fixEl.classList.add('st');
      } else if (w.scrollY < 120) {
        fixEl.classList.remove('st');
      }
    }
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    let el = document.getElementById('dayViewContent').getBoundingClientRect();
    this.moveHeaderleft = el.left + 'px';
  }

  onScroll(e, content) {
    let dis = content.getBoundingClientRect();
    this.moveHeaderleft =  dis.left + 'px';
  }

  ngOnInit() {
    let tHours = R.range(THRESHOLD_MIN_HOUR, THRESHOLD_MAX_HOUR)
      .concat(R.range(0, THRESHOLD_MIN_HOUR));

    this.dayRange = R.range(THRESHOLD_MIN_HOUR, THRESHOLD_MAX_HOUR)
      .concat(R.range(0, THRESHOLD_MIN_HOUR));

    this.timeTemplate = leftPad(tHours);

    if (this.cache.get('dayView:dateOnDayView')) {
      let getCache = this.cache.get('dayView:dateOnDayView');
      let now = moment(getCache);
      this.loadDate(now);
    } else {
      this.loadDate();
    }

  }

  loadDate(now?) {
    let d = this.timeDayview;
    let currentDate = moment().isoWeek(+d.currentMonthISOWeek)
            .isoWeekYear(+d.currentMonthISOWeekYear).lang(this.lang);

    let isSame =  moment().isSame(currentDate, 'isoWeek');

    if (now) {
      this.showDay = now.isSame(currentDate, 'isoWeek') ? now : '';
    }

    this.showDay =  this.showDay ?
                      this.showDay === 'prev' ?
                        currentDate.endOf('isoWeek') :
                          this.showDay === 'next' ?
                          currentDate.startOf('isoWeek') :
                        this.showDay
                    : isSame ? moment() : currentDate.startOf('isoWeek');
    this.dayIdx = this.showDay.day();

    this.changeData(this.unit);
    this.unpubStaff = [];
  }

 ngOnChanges(change) {
   this.loadDate();
   if (this.setCache) {
     this.cache.set('dayView:dateOnDayView', this.showDay.format('DD MMMM YYYY'));
     this.setCache = false;
   }
  }

  changeData(unit) {

    if (unit && unit.job) {

      // filter-out deleted and declined
      const negPreds = [
        R.propEq('deleted', 1),
        R.propEq('declined', 1),
        R.pathEq(['declined', 'reason'], 'sick'),
        R.pathEq(['declined', 'reason'], 'unavailable')
      ];

      // filter jobs list
      let jobsList = R.filter(R.allPass([
        R.propSatisfies(v =>
        v === getIndices(this.dayIdx).current ||
        v === getIndices(this.dayIdx).prev ||
        v === getIndices(this.dayIdx).next, 'weekday'),
        R.propSatisfies(v => v !== 0, 'webuser_id'),
        R.propSatisfies(v => R.isNil(v) || !R.anyPass(negPreds)(v), 'stat')
      ]), unit.job);


      // console.log(jobsList);

      // get positions list
      let positionsList = R.values(unit.positions);

      // get users list
      let usersList = R.values(unit.staff);

      // join jobs with positions, users, and time data
      const decorateJobsList = R.pipe(
        // R.map(x => {
        //   console.log(x.webuser_id);
        //   console.log(usersList.find(y => y.webuser_id === x.webuser_id));
        //   return x;
        // }),
        R.map((j: IJobComposite) => extend({}, j,
          {
            uname: R.find(R.propEq('webuser_id', j.webuser_id))(usersList) ?
            R.find(R.propEq('webuser_id', j.webuser_id))(usersList).uname : ''
          },
          {
            pname: R.find(R.propEq('position_id', j.position_id))(positionsList).pname
          },
          calcDiff(this.showDay, this.dayIdx, j)
        )),
        R.map((v: IJobComposite) => extend({}, v, {
          range: (<any>moment).range(v.tsfrom, v.tsto)
        })),
        R.map((v: IJobComposite) => extend({}, v, {
          isInPast: moment().isAfter(v.range.end, 'minutes')
        })),
        R.filter(R.anyPass([
            R.allPass([
              R.propEq('published', false),
              R.propEq('isInPast', false)
            ]),
          R.propEq('published', true)
        ]))
      );

      let res = decorateJobsList(jobsList);

      // apply grouping to the res list
      this.unitInfo = R.isEmpty(res) ? {} : pipeline.groupByPositionByUser(res);
      let workingTimeRanges = {};
      this.overallTimigs = {};
      this.unitTime = 0;
      this.reducedPositionRanges = {};

      if (R.isEmpty(this.unitInfo as any)) return;

      // console.log(this.unitInfo);

      const filterEmptyOut = R.pipe(
        R.map(R.filter((v: any) => v.some((x: IJobComposite) => {
          return R.propEq('weekday', getIndices(this.dayIdx).current)(x) ||
            (R.propEq('weekday', getIndices(this.dayIdx).prev)(x) &&
              x.isRotating && parseInt(x.to, 10) > 5) ||
            (R.propEq('weekday', getIndices(this.dayIdx).next)(x) && parseInt(x.from, 10) < 5);
        }))),
        R.filter((v: any) => R.not(R.isEmpty(v)))
      );

      // once again filter out users, which do not have assignments of a current day
      this.unitInfo = filterEmptyOut(this.unitInfo as any);
      // console.log(this.unitInfo);
      if (R.isEmpty(this.unitInfo as any)) { return; }

      // todo: check if it's not redundant
      workingTimeRanges = pipeline.getRanges(
        (this.unitInfo as any)
      );

      if (R.isEmpty(workingTimeRanges as any)) return;

      // console.log(this.dayIdx)'
      // console.log(this.unitInfo);
      // console.log(workingTimeRanges);

      const rangesNewPipe = R.pipe(
        R.map(R.map(R.map(R.pick(['range', 'isRotating'])))),
        R.map(R.values),
        R.map(R.flatten)
      )(this.unitInfo as any);

      // console.log(rangesNewPipe);
      // console.log(workingTimeRanges);

      const reduceRanges = R.pipe(
        R.map((v: any) => {
          return getFullDayRanges(this.showDay).reduce((acc, c) => {
            let res = R.reduce((a, cur: any) => {
              // let [start] = c.toDate();
              let outerDayIdx = `${moment(c.toDate()[0]).hours()}`;
              a[outerDayIdx] = a[outerDayIdx] || [];
              a[outerDayIdx].push(c.intersect(cur.range));
              return a;
            }, {}, v);
            return Object.assign({}, acc, res);
          }, {});
        }),
        R.map(R.map(R.filter(R.identity))),
        R.map(R.map(R.map(v => v.valueOf()))),
        R.map(R.map(R.reduce(R.add, 0 as any))),
        R.map(R.values)
      );

      this.reducedPositionRanges = reduceRanges(rangesNewPipe as any);
      this.overallTimigs = pipeline.getOverallSum(this.reducedPositionRanges);
      this.unitTime = R.sum(R.values(this.overallTimigs) as number[]);

    }
  }

  loadWeek(weekDiff: number) {
    this.action.dayViewUnpubStaff([] );
    switch (weekDiff) {
      case 1 :
        if (this.showDay.day() === 0) {
          this.changeWeekEvent.emit({
            action: weekDiff
          });
          this.showDay = 'next';
        } else {
          this.showDay.add(1, 'day');
        }
        break;
      case -1 :
        if (this.showDay.day() === 1) {
          this.changeWeekEvent.emit({
            action: weekDiff
          });
          this.showDay = 'prev';
        }else { this.showDay.add(-1, 'day'); }
        break;
      default:
        this.changeWeekEvent.emit({
          action: weekDiff
        });
        this.showDay = moment();
    }

    this.loadDate();

    this.cache.set('dayView:dateOnDayView', this.showDay.format('DD MMMM YYYY'));
  }

  showCalendar() {
    this.dayIn = this.showDay.format('dd DD MMMM YYYY');
    this.showCalendarVar = true;
  }

  setDate($event) {
    if ($event.action) {
      this.showDay = $event.action;
      this.changeWeekEvent.emit({
        action: $event.action
      });
    }
    this.setCache = true;
    this.showCalendarVar = false;
  }

  opebPublishModal(e) {
    this.action.openPublishModal(true, this.unpubStaff);
  }

  getPositionData(e) {
    if (e.action && e.action.length) {
      this.unpubStaff = this.unpubStaff.concat(e.action);
      this.action.dayViewUnpubStaff(this.unpubStaff );
    }
  }

  sendPulish() {
    let dayInfo = this.showDay.format('GGWWE');
    let weekInfo = this.showDay.format('GGWW');

    this.scheduledService.publishJobs(
      this.unit.unit_id,
      this.showDay.format('GGWW'),
      this.showDay.format('E'))
      .subscribe(() => {
        this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, weekInfo);
        this.action.openPublishModal(false, []);
      });
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => {
      el.unsubscribe();
    });
    this.cache.set('dayView:dateOnDayView', this.showDay.format('DD MMMM YYYY'));
  }

}

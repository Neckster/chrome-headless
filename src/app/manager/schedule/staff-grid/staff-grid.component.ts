import {
  Component,
  Input,
  Output,
  ElementRef,
  EventEmitter,
  NgZone, ViewChild }             from '@angular/core';

import { select }                 from 'ng2-redux/lib/index';
// import moment                     = require('moment/moment');
import {
  allPass,
  not,
  pathEq,
  forEach,
  anyPass }                       from 'ramda';

import { PositionService }        from '../../../services/position.service';
import { JobService }             from '../../../services/shared/JobService';
import { Cache }                  from '../../../services/shared/cache.service';
import { ScheduleService }        from '../../../services/shared/SchedulService';
import { WebuserService }         from '../../../services/webuser.service';
import { TargetJobService }       from '../../../services/job.service';
import { EmitService }            from '../../../services/emit.service';

import { AppStoreAction }         from '../../../store/action';

import { DurationPipe }           from '../../../lib/pipes/duration';
import { TextCutPipe }           from '../../../lib/pipes/textCut';

import { JobFactory }             from '../../../models/Job';

import { htmlShiftPosition }      from './staff-grid-template.htm';

import { AssignModalController }  from '../modal-assign/assign.component';


import { DayOff, DayOffFactory }                 from '../../../models/DayOff';
import * as moment from 'moment';
import * as R from 'ramda';
import { extend } from '../days-view/lib/utils';
import { SafeNavigationPipe } from '../../../lib/pipes/safeNav';

const reasonsAbbrMapping = {
  'holidays': 'HOL',
  'education': 'EDU',
  'services': 'SOC',
  'other': 'ABS',
  'sickness': 'SICK',
  'overtime': 'OVER'
};

@Component({
  selector: 'staff-grid',
  pipes: [DurationPipe, TextCutPipe, SafeNavigationPipe],
  providers: [DayOffFactory],
  styles: [require('./styles.less')],
  template: htmlShiftPosition
})

export class StaffGreedController {

  @Input() currentPosition;
  @Input() currentUnit;
  @Input() shift;
  @Input() shiftIndex;
  @Input() shiftPosition;
  @Input() overallTiming;
  @Input() scheduled;

  @select() info$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  @select() deleteAssign$: any;
  @select() scheduled$: any;
  @Output() updatePositionEmitter = new EventEmitter();

  @ViewChild('dayOffVar') dayOffVar: ElementRef;

  public info;
  public week;
  public openConfirm;
  public hideScrollBar = this.jobService.hideScrollBar;
  public isDayOffShift = [];
  private shiftParse;
  private weekDay;

  private selectedJobber;
  private showList: boolean;
  private arrayIteration = new Array(7);
  private jobIsArr;
  private removeJobber = false;
  private publisedShPosAss;
  private blockedAssign;
  private shiftUnpublished = 0;
  private jobberAssignment = [];
  private unsubscribe = [];
  private totalHours: number = 0;

  private emptyBoxPrev: any = new Object({});
  private emptyBoxNext: any = new Object({});
  private addPrev: boolean = false;
  private addNext: boolean = false;

  private scheduledJobberNotification: any = false;

  private dayOffsData = {};
  private groupedVacations = {};
  private reasonsAbbrMapping = reasonsAbbrMapping;
  private addVacationOpened = false;
  private unpubArray = [];
  private selectedVacationData = {
    userId: undefined,
    vacationData: undefined
  };
  private daysOffStyle: any = {};

  constructor(public targetService: TargetJobService, public action: AppStoreAction,
              public cache: Cache, private emitter: EmitService,
              public positionService: PositionService,
              public jobService: JobService, public elRef: ElementRef,
              public scheduleService: ScheduleService,
              private webuserService: WebuserService, private zone: NgZone,
              private dayOffFactory: DayOffFactory) {



    this.shiftParse = webuserService.shiftParse;
    this.weekDay = webuserService.weekDay;

    this.isDayOffShift = this.arrayIteration.map(i => false);

    this.info$.subscribe(info => {

      this.info = info;

      let v = this.info.manager && this.info.manager.day_offs
          ? this.info.manager.day_offs
          : undefined;

      if (v) {
        Object.keys(v).forEach(cid => {
          Object.keys(v[cid]).forEach(val => {
            this.dayOffsData[val] = v[cid][val];
          });
        });
      } else {
        this.dayOffsData = {};
      }

      if (this.dayOffsData) {
        this.dayOffsData = R.map(R.map((val: any) =>
          this.dayOffFactory.createDayOff(val)))(this.dayOffsData as any);
      } else {
        this.dayOffsData = {};
      }

      this.calculateVacations(this.dayOffsData, this.week);
    });

    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
      this.calculateVacations(this.dayOffsData, this.week);

    });

  }

  changeParent(el) {
    el.className = el.className + ' daysOffInside ';
    return;
  }


  calculateVacations(vacations: any, week: number) {

    if (!vacations || !week) {
      return;
    }

    vacations = R.map(R.filter((v: DayOff) => {
      return this.info.manager && this.info.manager.selected
        ? this.info.manager.selected.company_id === v.companyId
        : true;
    }))(vacations);
    const transformVacations = R.pipe(
      R.map(R.filter((v: any) => {
        return v.toWeek >= week && v.fromWeek <= week && v.status !== -1;
      })),
      R.map(R.map((v: any) => {

        let rangeStart = v.fromWeek < week ? 1 : v.fromWeekday;
        let rangeEnd = v.toWeek > week ? 8 : v.toWeekday + 1;

        let dayRange = R.range(rangeStart, rangeEnd);

        return dayRange.map(day => {
          return extend({}, v, { day }, { inPast: moment().isAfter(v.toDate) });
        });

      })),
      R.map(R.map(R.groupBy(R.prop('day')))),
      R.map((v: any) => {
        return v.reduce((acc, val) => {
          return extend(acc, val);
        }, {});
      }),
      R.map(R.map(R.map((v: any) => {

          let crossWeek = true;
          let shiftRange = [];

          let shiftRangeStart;
          let shiftRangeEnd;

          if (v.toWeek === +week || v.fromWeek === +week)
            crossWeek = false;


         if (crossWeek) {
           shiftRange = R.range(0, 4);
         } else {
           shiftRangeStart = v.fromWeekday === (<any>v).day ? v.fromShift : 0;
           shiftRangeEnd = v.toWeekday === (<any>v).day ? v.toShift + 1 : 3 + 1;
           shiftRange = R.range(shiftRangeStart, shiftRangeEnd);
         }

         return shiftRange.map(shift => {
           return extend({}, v, { shiftIdx: shift });
         });

      }))),
      R.map(R.map(R.map(R.groupBy(R.prop('shiftIdx'))))),
      R.map(R.map((v: any) => {
        return v.reduce((acc, val) => {
          return extend(acc, val);
        }, {});
      })),
      R.map(R.map(R.map(R.head)))
    );

    this.groupedVacations = transformVacations(vacations);
  }

  ngOnInit() {
    if (this.shiftPosition)
      this.shift = this.currentPosition.shift[this.shiftIndex];
    if (this.currentUnit)
      this.jobIsArr = Array.isArray(this.currentUnit.job);
    this.scheduleService.setWeek(this.week);
    this.showList = this.cache.isInArray(
      'schedule:shiftOpened',
      `${this.currentPosition.position_id}-${this.shiftIndex}`
    );
    this.publisedShPosAss = this.shift.shiftInfo.publisedShPosAss;

    // this.shiftUnpublished = this.shift.shiftInfo.shiftUnpublished;
    let unpubService = this.scheduleService.checkUnpublished(this.shift.shiftInfo.daysInfo);
    this.shiftUnpublished = unpubService.counter;
    this.unpubArray  =  unpubService.info;

    // console.log(this.shift.shiftInfo);

    if (this.shift && this.shift.shiftInfo) {
     let daysInfo =  this.shift.shiftInfo.daysInfo,
         jobbers = this.shift.shiftInfo.jobberPerShift;



    this.shift.shiftInfo.jobberPerShift = jobbers.map(j =>

      Object.assign({}, j, {
        totalHours: daysInfo.reduce((sum, current) => {
          let job = current.currentDayAssign[j.webuser_id];
          let dayOff = this.dayOffsData[j.webuser_id]  && job ?
            this.dayOffsData[j.webuser_id].reduce((arr, v) => {
            let dayOffFrom = moment(`${v.fromWeek}${v.fromWeekday}`, 'GGWWE'),
                    dayOffTo = moment(`${v.toWeek}${v.toWeekday}`, 'GGWWE'),
                    curentDay = moment(`${this.week}${job.weekday}`, 'GGWWE');
            if (this.week >= v.fromWeek  && this.week <= v.toWeekday) {
              if ((curentDay.isAfter(dayOffFrom) && curentDay.isBefore(dayOffTo)) ||
                curentDay.isSame(dayOffFrom) || curentDay.isSame(dayOffTo)) {
                arr.push(v);
              }
            }
            return arr;
            }, []) : false;
          if (job && !dayOff.length) {
            let from = moment(`${job.from}`, 'HH:mm'),
                to = moment(`${job.to}`, 'HH:mm'),
                time;
            if (from.isBefore(to)) {
              time = to.diff(from);
            }else {
              to = to.add(1, 'day');
              time =  to.diff(from);
            }
            return sum + time;
          }
          return sum;
       }, 0)
      }));
    }
  }

  // events
  toggleShiftCollapsible() {
    if (this.showList) {
      this.cache.removeFromArray(
        'schedule:shiftOpened',
        `${this.currentPosition.position_id}-${this.shiftIndex}`
      );
    } else {
      this.cache.pushArray(
        'schedule:shiftOpened',
        `${this.currentPosition.position_id}-${this.shiftIndex}`
      );
    }
    this.showList = !this.showList;
  }

  assign(weekday, shiftIdx, positionId, webuserId, event, job, daysInfo) {
    this.blockedAssign = {
      weekday, shiftIdx, positionId,
      webuserId, pending: daysInfo.pending, job
    };
    let assignedPosition = this.shift.shiftInfo.daysInfo[weekday - 1];

    let previousAssignment = this.getPreviousAssignment(webuserId, weekday - 1);
    if (previousAssignment && !assignedPosition.currentDayAssign[webuserId]) {
      if (daysInfo.assign[weekday - 1].daysInfo !== 'time-past') {
        // If we have assignment for this shift
        // if (previousAssignment.shift_idx === shiftIdx) {
          this.scheduledJobberNotification = {
            assign: previousAssignment,
            staffInfo: this.shift.shiftInfo.jobberPerShift
                .filter(item => item.webuser_id === webuserId)[0]
          };
          this.hideScrollBar();
        // } else {
        //   this.updateJobAssign();
        // }
      }
      // this.action.showAlertScheduledJobber(
      //   this.getPrevScheduledAssignment(daysInfo, weekday - 1)
      // );
    } else if (daysInfo.assign[weekday - 1].daysInfo !== 'time-past') {
      if (!job) {
        if (event.target.parentNode.classList.contains('ostatus_off')) {
          this.openConfirm = true;
        } else {
          this.updateJobAssign();
        }
      } else {
        this.action.openModalAssign(job);
        let eName = 'updteAssign' + positionId + shiftIdx;
        this.unsubscribe.push(eName);
        this.emitter.addEvent(eName).subscribe(el => {
          this.deleteAssign(el);
          this.emitter.emit(this.getUpdateEventName(), {
            shift: this.shift,
            shiftIdx: this.shiftIndex,
            act: this.shiftUnpublished,
            weekday: el.weekday,
            position: this.currentPosition
          });
          this.emitter.emit(this.getUpdateEventName(true), {
            shift: this.shift,
            shiftIdx: this.shiftIndex,
            act: this.shiftUnpublished,
            weekday: el.weekday,
            position: this.currentPosition
          });
        });
        // this.hideScrollBar();
      }
    }
  }

  replaceName(msg) {
    return msg.replace('{name}', this.scheduledJobberNotification.staffInfo.uname);
  }

  getPrevScheduledAssignment(daysInfo, weekday) {
    if (daysInfo.assign[weekday].dayAssignments
      && daysInfo.assign[weekday].dayAssignments[this.shiftIndex]) {
      return daysInfo.assign[weekday].dayAssignments[this.shiftIndex];
    }
    return false;
  }

  ngOnDestroy() {
    delete this.shift;
    this.unsubscribe.forEach(name => this.emitter.unsubscribeAll(name) );
    this.emitter.unsubscribeAll(this.getUpdateEventName());
  }

  deleteAssign(el, all?) {
    let oldAssignment = this.publisedShPosAss;
    if (this.publisedShPosAss) {

      this.publisedShPosAss.forEach((assign, idx) => {
        if (assign.webuser_id === el.webuser_id && assign.weekday === el.weekday) {
          oldAssignment.splice(idx, 1);
        }
      });
    }
    if (el.status === 'update' && !all) {
      el.status = 'updated';
      this.publisedShPosAss.push(el);
    }
    this.shift.shiftInfo.publisedShPosAss = oldAssignment;
    this.shift.shiftInfo.daysInfo[el.weekday - 1].currentDayAssign = {};
    this.shift.shiftInfo.daysInfo[el.weekday - 1] =
      this.scheduleService.init(el.weekday - 1, oldAssignment, false, this.shift);

    let unpubService = this.scheduleService.checkUnpublished(this.shift.shiftInfo.daysInfo);
    this.shiftUnpublished = unpubService.counter;
    this.unpubArray  =  unpubService.info;

    this.shift.shiftInfo.shiftUnpublished = this.shiftUnpublished;
    // this.action.fetchGetInfo();
  }

  showAddModal(shiftIdx, positionId) {
    this.action.showAddModal({shiftIdx, positionId});
    this.hideScrollBar();
  }

  targetJobber(weekday, positionId, shiftIndex, target) {
    if (target.timeStatus !== 'time-past') {
      let jobTarget;
      if (target.targetInfo)
        jobTarget = target.targetInfo;
      if (!jobTarget) jobTarget = {weekday: weekday + 1, positionId, shiftIndex};
      this.action.showModal(jobTarget);
      this.hideScrollBar();
    }
  }
  checkdeletedRefuse(obj?) {
    if (obj && this.info.assignment.length) {
      let assing = this.info.assignment.filter( el => {
        return  el.webuser_id === obj.webuserId &&
                el.position_id === obj.positionId &&
                el.weekday === obj.weekday && el.stat
                && el.week === this.scheduleService.week
                && el.stat.declined;
      });
      return assing ? assing[0] : false;
    }
  };

  updateJobAssign() {
    let declined = this.checkdeletedRefuse( this.blockedAssign );
    let job;
    let toString = this.currentUnit.shift[this.blockedAssign.shiftIdx].to;
    let fromString = this.currentUnit.shift[this.blockedAssign.shiftIdx].from;
    let tto = +moment(this.week, 'GGWWE')
      .add(this.blockedAssign.weekday - 1, 'd')
      .add(+toString.slice(0, 2), 'h')
      .add(+toString.slice(-2), 'm');
    let tfrom = +moment(this.week, 'GGWWE')
      .add(this.blockedAssign.weekday - 1, 'd')
      .add(+fromString.slice(0, 2), 'h');
    if (declined) {
      job = JobFactory.createJob({
        position_id: declined.position_id,
        shift_idx: declined.shift_idx,
        weekday: declined.weekday,
        webuser_id: declined.webuser_id,
        to: toString,
        from: fromString,
        curAsClass: 'declined',
        pending: declined.pending,
        published: false,
        stat: declined.stat
      });
    } else if (!declined) {
      job = JobFactory.createJob({
        position_id: this.blockedAssign.positionId,
        shift_idx: this.blockedAssign.shiftIdx,
        weekday: this.blockedAssign.weekday,
        webuser_id: this.blockedAssign.webuserId,
        to: toString,
        from: fromString,
        curAsClass: 'unpub needci',
        pending: this.blockedAssign.pending,
        published: false
      });
    }
    this.publisedShPosAss.push(job);
    // debugger;
    this.shift.shiftInfo.daysInfo[this.blockedAssign.weekday - 1] =
      this.scheduleService.init(this.blockedAssign.weekday - 1,
        this.publisedShPosAss, false, this.shift);

    this.shift.shiftInfo.publisedShPosAss = this.publisedShPosAss;

    let unpubService = this.scheduleService.checkUnpublished(this.shift.shiftInfo.daysInfo);
    this.shiftUnpublished = unpubService.counter;
    this.unpubArray  =  unpubService.info;

    this.shift.shiftInfo.shiftUnpublished = this.shiftUnpublished;
    this.targetService
      .putAssign(this.blockedAssign.weekday, this.blockedAssign.shiftIdx,
        this.blockedAssign.positionId, this.blockedAssign.webuserId,
        this.week, toString, fromString, tto, tfrom)
      .subscribe(el => {
       console.log(el);
        // this.action.getCurrentUnit(newUnit);
      });
    this.emitter.emit(this.getUpdateEventName(), {
      shift: this.shift,
      shiftIdx: this.shiftIndex,
      act: this.shiftUnpublished,
      weekday: this.blockedAssign.weekday,
      position: this.currentPosition
    });
    this.emitter.emit(this.getUpdateEventName(true), {
      shift: this.shift,
      shiftIdx: this.shiftIndex,
      act: this.shiftUnpublished,
      weekday: this.blockedAssign.weekday,
      position: this.currentPosition
    });
    this.action.fetchGetInfo();
  }

  selectJobber(currentPosition, shiftIndex, staffId) {
    this.selectedJobber = {currentPosition, shiftIndex, staffId};
    this.jobberAssignment = this.shift.shiftInfo.daysInfo.map(elDay => {
      if (elDay.timeStatus === 'time-future') {
        let assignment = elDay.currentDayAssign[staffId];
        return assignment;
      }
    }).filter(el => el);
    this.hideScrollBar();

  }

  deleteJobber() {
    let currentPosition = this.selectedJobber.currentPosition,
      shiftIndex = this.selectedJobber.shiftIndex,
      staffId = this.selectedJobber.staffId;

    let staffIdxPosition = currentPosition.staff[shiftIndex];
    if (staffIdxPosition instanceof Array && staffIdxPosition.indexOf(staffId) !== -1) {
      staffIdxPosition.splice(staffIdxPosition.indexOf(staffId), 1);
      if (!staffIdxPosition.length)
        currentPosition.staff[shiftIndex] = {};
    }
    let job = this.shift.shiftInfo.jobberPerShift;
    // delete assignment for current week
    if (this.jobberAssignment) {
      this.jobberAssignment.forEach(el => {
        this.deleteAssign(el, true);
        this.targetService.deleteAssign(el.weekday, el.shift_idx, el.position_id,
          el.webuser_id, this.week, 1).subscribe();
      });
    }
    let checkAssign = this.shift.shiftInfo.daysInfo.filter(el => {
      return el.currentDayAssign[staffId];
    }).length;
    if (!checkAssign) {
      this.shift.shiftInfo.jobberPerShift = job.filter(el => {
        return el.webuser_id !== staffId;
      });
    }
    this.positionService
      .putPoition(currentPosition.pname, currentPosition.company_id, currentPosition.unit_id,
        currentPosition.staff)
      .subscribe(() => {
        // this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      });
    this.selectedJobber = {};
    this.hideScrollBar();
  }

  assignmentModalClose() {
    this.openConfirm = false;
  }

  removeModalClose() {
    this.removeJobber = false;
    this.hideScrollBar();
  }

  hasAssignmentInfo(assign) {
    let className: string;
    if (assign && assign.info) {
      className = 'hasinfo ';
    }
    if (assign && assign.curAsClass.indexOf('declined') === -1
          && assign.curAsClass.indexOf('unpub') === -1) {
      className += ' jobIsPublished';
    }
    return className;
  }

  // UI checks

  assertShiftClear(shiftInfo: IShiftInfo) {
    const assertPipe = allPass([
      (s) => not(s.targetInfo && (s.targetInfo.max_work_time || s.targetInfo.min_job_count)),
      (s) => not(s.dayAssigns),
      (s) => not(s.pendingCount)
    ]);
    return assertPipe(shiftInfo);
  }

  checkNeed(shift, i) {
    let info = shift.shiftInfo.daysInfo[i],
        status = info.targetStatus,
        targetInfo = info.targetInfo,
        time = info.targetInfo ? info.targetInfo.max_work_time : 0,
        job = info.targetInfo ? info.targetInfo.min_job_count : 0;

    return status ? false :
        !targetInfo ? true :
          (time === 0 && job === 0 ) ?
            true : false;
  }

  /**
   * Check class for position, also check do we have another assignments for this shift and position
   * @param i
   * @param staffInfo
   * @returns {string}
   */
  getPositionClass(i, staffInfo, currentAssign) {
    let classAssignment = this.getPreviousAssignment(staffInfo.webuser_id, i)
      ? ScheduleService.unavailablePositionClass + ' by-assignment'
      : staffInfo.assign[i].classAssignment;
    let currentClass = currentAssign
      ? this.shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].chef ?
        this.shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].curAsClass
        + ' chef ' :
        this.shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].curAsClass
      : classAssignment;
    return currentClass;
  }

  checkToolTipe(shift, i, webId) {

    this.addNext = false;
    this.addPrev = false;

    let prev  = this.shiftIndex ? this.shiftIndex - 1 : -1;
    let next = this.shiftIndex === 3 ? -1 : this.shiftIndex + 1;
    this.checkShiftAssignment(next, i + 1, webId, 'emptyBoxNext', 'addNext');
    this.checkShiftAssignment(prev, i + 1, webId, 'emptyBoxPrev', 'addPrev');
    return  this.addNext || this.addPrev ? true : false;
  }

  getPositionShiftClass(i, staffInfo) {
    let positionClass = this.getPositionClass(
        i,
        staffInfo,
        this.shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]
    );

    let assugnmentClass = this.hasAssignmentInfo(
        this.shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]
    );

    return `${positionClass} ${assugnmentClass} ${staffInfo.assign[i].daysInfo}`;
  }

  private checkShiftAssignment(shift: number, i, webId, dataField: string, flagField: string) {
    if (shift !== -1) {
      if (this.scheduled[shift] && this.scheduled[shift][i] && this.scheduled[shift][i][webId]) {
        let dayAssign = this.scheduled[shift][i][webId];
        const index = Object.keys(dayAssign)[0];

        if (!dayAssign[this.currentPosition.position_id]) {
          this[dataField].label = ' | ' + dayAssign[index].pname;

          if (dayAssign[index].unit_id !== this.currentPosition.unit_id) {
            this[dataField].label += ' | ' + dayAssign[index].nname;
          }
        } else {
          this[dataField].label = '';
        }
        this[dataField].from = dayAssign[index].from;
        this[dataField].to = dayAssign[index].to;
        this[dataField].cl = dayAssign[index].published;

        this[flagField] = true;
      }
    }
  }


  private getUpdateEventName(isPositionEvent?) {
    if (!isPositionEvent) {
      return 'updatePositionEmitter' + this.shiftPosition + '[' + this.shiftIndex + ']';
    } else {
      return 'updatePositionEmitter' + '[' + this.currentPosition.position_id + ']';
    }
  }

  private getPreviousAssignment(webuserId, weekday) {
    weekday = weekday + 1;
    let prevDay = weekday - 1;
    let foundScheduledJob = false;

    const from = moment(this.shift.from, 'HH:mm');
    const to = moment(this.shift.to, 'HH:mm');

    // Check if job on any of shifts aren't on this shift
    if (this.scheduled) {
      for (let i in this.scheduled) {
        if (this.scheduled.hasOwnProperty(i)) {
          if (this.scheduled[i][weekday] && this.scheduled[i][weekday][webuserId]) {
            if (this.shiftIndex !== i
              || (i === this.shiftIndex
                && !this.scheduled[i][weekday][webuserId][this.currentPosition.position_id])) {
              for (let positionId in this.scheduled[i][weekday][webuserId]) {
                if (this.scheduled[i][weekday][webuserId].hasOwnProperty(positionId)) {
                  const pFrom =
                    moment(this.scheduled[i][weekday][webuserId][positionId].from, 'HH:mm');
                  const pTo =
                    moment(this.scheduled[i][weekday][webuserId][positionId].to, 'HH:mm');

                  if (this.checkPositionOnShift(pFrom, from, pTo, to)) {
                    return this.scheduled[i][weekday][webuserId][positionId];
                  }
                }
              }
            }
          }
          if (this.scheduled[i][prevDay] && this.scheduled[i][prevDay ][webuserId]) {
            for (let positionId in this.scheduled[i][prevDay][webuserId]) {
              if (this.scheduled[i][prevDay][webuserId].hasOwnProperty(positionId)) {
                const pFrom =
                  moment(this.scheduled[i][prevDay][webuserId][positionId].from, 'HH:mm');
                const pTo =
                  moment(this.scheduled[i][prevDay][webuserId][positionId].to, 'HH:mm');

                if (pFrom.unix() > pTo.unix() && from.unix() < pTo.unix() ) {
                  return this.scheduled[i][prevDay][webuserId][positionId];
                }
              }
            }
          }
        }
      }
    }
    return foundScheduledJob;
  }

  private checkPositionOnShift(pFrom: moment.Moment,
                               from: moment.Moment,
                               pTo: moment.Moment,
                               to: moment.Moment) {
    return (pFrom.unix() === from.unix() && pTo.unix() === to.unix())
      || (pFrom.unix() < from.unix() && pTo.unix() > from.unix())
      || (pTo.unix() > to.unix() && pFrom.unix() < to.unix());
  }

  private closeScheduledJobberNotification(e) {
    e.preventDefault();
    this.scheduledJobberNotification = false;
    this.hideScrollBar();
  }

/*
  private isInVacationRange(userId: number, week, dayIdx:number): boolean {
    let a = this.getVacations(this.dayOffsData, userId)
       .bind(v => {
         const conditions = R.allPass([
           (x) => x.to_week >= parseInt(week, 10),
           (x) => x.from_week <= parseInt(week, 10),
           (x) => x.from_week === parseInt(week, 10) ? dayIdx >= x.from_weekday : true,
           (x) => x.to_week === parseInt(week, 10) ?  x.to_weekday > dayIdx  : true//
         ]);

         let rs = R.filter(conditions)(v);

         return Just({
             accepted: rs.length ? R.head(rs).status === 2 : 0,
             amount: rs.length > 0
         });
       });

    return (<any>a).isValue && (<any>a).val.amount;
  }

  private getVacations(vacations, userId: number): Maybe<DayOff[]> {
    return Maybe.of(vacations)
      .bind(v => Maybe.fromNull(R.prop(`${userId}`)(v)));
  }
*/

  private onVacationCellClick(event, userId, vacationData) {
    if (vacationData.inPast ||  vacationData.approve ||
         vacationData.decline || vacationData.update ) return;

    this.addVacationOpened = true;
    this.selectedVacationData.userId = userId;
    this.selectedVacationData.vacationData = vacationData;
  }

  private onVacationModalOutput(event) {
    this.addVacationOpened = false;
    this.action.fetchGetInfo(this.week);
  }

}

interface IShiftInfo {
  targetInfo: any;
  dayAssigns: any;
  pendingCount: any;
}

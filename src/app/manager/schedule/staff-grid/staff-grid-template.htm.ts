//noinspection TsLint
export const htmlShiftPosition: string = `

<div *ngIf="addVacationOpened">
   <add-vacation 
      [opened] = "addVacationOpened"
      [editByManager] = true
      [userId] = "selectedVacationData.userId"
      [userName] ="selectedVacationData.userId"
      [vacation]="selectedVacationData.vacationData"
      (closeEmitter)="onVacationModalOutput($event)">    
    </add-vacation>
</div>


<div [ngClass]="{'collapsed': !showList}"
     class="uk-clearfix schedule-row shiftpos-0-131499 section">
  <div class="uk-width-1-1 uk-clearfix ">
    <div class="uk-width-small-10-10 uk-width-medium-3-10 uk-float-left gridleft">
      <div class="uk-grid uk-grid-collapse uk-position-relative callefthead ">
        <div class="uk-width-small-1-10 uk-width-large-1-10 secshowhide" style="width:10%!important;">
          <div class="secshow moreHight">
            <span class="icon large arrow-right grey" style='cursor:pointer' (click)="toggleShiftCollapsible(); ">&nbsp;</span>
          </div>
          <div class="sechide moreHight">
            <span class="icon large arrow-down blue" style='cursor:pointer' (click)="toggleShiftCollapsible(); ">&nbsp;</span>
          </div>
        </div>

        <!--ShiftName-->
        <div *ngIf="shiftPosition" class="text uk-width-small-9-10 uk-width-large-9-10 moreHight" style="position:relative; top:0">
         <div class="nameStyle" style="float: left">
             <div class="staffParentName">{{currentPosition.pname}}</div>
             <div class="relatedInfo">{{shiftParse[shiftIndex] | translate}}&nbsp;&nbsp;&nbsp;{{shift.from  + '-' + shift.to}}h</div>
         </div>
         <div class="notificationOnStaff totalHourWrap" style="position: absolute; right:0; white-space: nowrap">
            <span  class="totalHour">T: {{overallTiming | duration}}{{ 'abbr_hour_letter' | translate }}</span>
          </div>
          <div class="notificationOnStaff notificationBubbles" style="position: absolute; right:-1.5rem">
             <span *ngIf="shift.shiftInfo.shiftUnpublished" class="custom-bubbles">{{shift.shiftInfo.shiftUnpublished}}</span>
          </div>

        </div>
        <!--ShiftNameEnd-->

        <!--PositionsName-->
        <div *ngIf="!shiftPosition" class="text uk-width-small-9-10 uk-width-large-9-10 moreHight" style="position:relative; top:0">
          <div class="nameStyle" style="float: left" >
             <div class="staffParentName">{{shiftParse[shiftIndex] | translate}}</div>
             <div class="relatedInfo">{{currentPosition.pname}}&nbsp;&nbsp;&nbsp;{{shift.from  + '-' + shift.to}}{{ 'abbr_hour_letter' | translate }}</div>
          </div>
          <div class="notificationOnStaff totalHourWrap" style="position: absolute; right:0; white-space: nowrap">
            <span class="totalHour">T: {{overallTiming | duration}}{{ 'abbr_hour_letter' | translate }}</span>
          </div>
          <div class="notificationOnStaff notificationBubbles" style="position: absolute; right:-1.5rem">
             <span *ngIf="shift.shiftInfo.shiftUnpublished" class="uk-badge uk-badge-notification ">{{shift.shiftInfo.shiftUnpublished}}</span>
          </div>
        </div>
        <!--PositionsNameEnd-->
      </div>
    </div>
    <!--  gridleft -->
    <div class="uk-hidden-small uk-width-medium-7-10 uk-float-left gridright">
      <div class="uk-width-1-1 rowhead">
        <div class="uk-grid uk-grid-collapse sevendays moreHight" >
          <div class="uk-width-1-1">

            <div *ngFor="let item of arrayIteration; let i = index"
                 class="calday sstatus_future moreHight">
              <div (click)="targetJobber(i, currentPosition.position_id, shiftIndex, shift.shiftInfo.daysInfo[i]); "
                   class=" calstat cstat_empty nosel {{shift.shiftInfo.daysInfo[i].timeStatus}}" style="padding-top: 5px"> <!-- deleted class uk-vertical-align-middle -->

                <div [ngSwitch]="jobIsArr">

                  <template [ngSwitchCase]="false">
                     <div class="icon plus circle"></div>

                  </template>

                  <template [ngSwitchCase]="true">

                    <div *ngIf="assertShiftClear(shift.shiftInfo.daysInfo[i])" class="calstat_icons cstat_empty_mark">
                      <div class="icon plus circle"></div>
                    </div>

                    <div *ngIf="shift.shiftInfo.daysInfo[i].targetInfo"
                         class="{{shift.shiftInfo.daysInfo[i].timeStatus === 'time-past' ? 'time-past' : shift.shiftInfo.daysInfo[i].pendingCount ?
                         'cstat_bad' : shift.shiftInfo.daysInfo[i].targetStatus ||  shift.shiftInfo.daysInfo[i].dayAssigns < shift.shiftInfo.daysInfo[i].targetInfo?.min_job_count ? 'cstat_bad' : ''}}">

                      <div *ngIf="shift.shiftInfo.daysInfo[i].targetInfo.min_job_count || shift.shiftInfo.daysInfo[i].dayAssigns" class="calstat_jobs">

                        <div *ngIf="shift.shiftInfo.daysInfo[i].targetInfo.min_job_count" class="calstat_hours">
                          <span class="calstat_plan">{{shift.shiftInfo.daysInfo[i].dayAssigns}}</span>/
                          <span class="calstat_need">{{shift.shiftInfo.daysInfo[i].targetInfo.min_job_count}}</span>
                          <span *ngIf="shift.shiftInfo.daysInfo[i].pendingCount">({{shift.shiftInfo.daysInfo[i].pendingCount}})</span>
                        </div>

                        <div *ngIf="!shift.shiftInfo.daysInfo[i].targetInfo.min_job_count" class="calstat_hours">
                          <span class="calstat_plan">{{shift.shiftInfo.daysInfo[i].dayAssigns}}</span>
                          <span *ngIf="shift.shiftInfo.daysInfo[i].pendingCount">({{shift.shiftInfo.daysInfo[i].pendingCount}})</span>
                          <br>
                           
                          <!--<span [style.visibility]="checkNeed(shift, i)?'visible':'hidden'" class="needAtr">{{'need' | translate}}</span>-->
                        </div>

                      </div>

                      <!--display planed time config-->

                      <div *ngIf="shift.shiftInfo.daysInfo[i].totalDayTime || shift.shiftInfo.daysInfo[i].targetInfo.max_work_time">
                        <div *ngIf="shift.shiftInfo.daysInfo[i].targetInfo.max_work_time" class="calstat_hours">
                          <span class="calstat_plan">({{shift.shiftInfo.daysInfo[i].totalDayTime}}</span>
                          <span class="calstat_need">/{{shift.shiftInfo.daysInfo[i].targetInfo.max_work_time}}:00h)</span>
                          <span [style.visibility]="unpubArray[i]?'visible':'hidden'" class="draftInfo">{{'Draft'}}:{{unpubArray[i]}}</span>
                        </div>
                        <div *ngIf="!shift.shiftInfo.daysInfo[i].targetInfo.max_work_time && shift.shiftInfo.daysInfo[i].totalDayTime !== '0:00' " class="calstat_hours">
                          <span class="calstat_plan">({{shift.shiftInfo.daysInfo[i].totalDayTime}})</span>
                          <br>
                          <span [style.left]="!unpubArray[i] ? '15px' : '0px'"
                                [style.display]="checkNeed(shift, i)?'':'none'" 
                                class="needAtr">{{'need' | translate}}</span>
                          <span [style.visibility]="unpubArray[i]?'visible':'hidden'" class="draftInfo">{{'Draft'}}:{{unpubArray[i]}}</span>
                        </div>
                      </div>

                    </div>

                    <div *ngIf="!shift.shiftInfo.daysInfo[i].targetInfo && shift.shiftInfo.daysInfo[i].dayAssigns"
                      class="{{shift.shiftInfo.daysInfo[i].timeStatus === 'time-past' ? 'time-past' : shift.shiftInfo.daysInfo[i].pendingCount ?
                         'cstat_bad' : shift.shiftInfo.daysInfo[i].targetStatus ? 'cstat_bad' : ''}}"
                         [style.marginBottom]=" shift.shiftInfo.daysInfo[i].targetStatus ? '' : '2rem'">
                      <div class="calstat_jobs">
                        <div class="calstat_hours">
                          <span class="calstat_plan">{{shift.shiftInfo.daysInfo[i].dayAssigns}}</span>
                          <span *ngIf="shift.shiftInfo.daysInfo[i].pendingCount">({{shift.shiftInfo.daysInfo[i].pendingCount}})</span>
                        </div>
                      </div>
                      <div class="calstat_hours">
                        <span class="calstat_plan">({{shift.shiftInfo.daysInfo[i].totalDayTime}})</span><br/>
                        <span [style.left]="!unpubArray[i] ? '15px' : '0px'"
                              [style.display]="checkNeed(shift, i)?'':'none'" 
                                class="needAtr">{{'need' | translate}}</span>
                        <span [style.visibility]="unpubArray[i]?'visible':'hidden'" class="draftInfo">{{'Draft'}}:{{unpubArray[i]}}</span>
                      </div>
                    </div>
                    <div *ngIf="shift.shiftInfo.daysInfo[i].pendingCount &&
                               !shift.shiftInfo.daysInfo[i].targetInfo &&
                               !shift.shiftInfo.daysInfo[i].dayAssigns"
                               class="cstat_bad calstat_hours">
                      <span>0 ({{shift.shiftInfo.daysInfo[i].pendingCount}})</span> <br/>
                      <span [style.visibility]="unpubArray[i]?'visible':'hidden'" class="draftInfo">{{'Draft'}}:{{unpubArray[i]}}</span>
                    </div>
                    
                   </template>
                </div>
                
              </div>
            </div>

          </div>
        </div>
        <!-- sevendays uk-grid  -->
      </div>
      <!-- rowhead -->
    </div>
    <!-- gridright -->
  </div>
  <div class="uk-width-1-1 grid-table">
    <div class="table-container uk-grid uk-grid-collapse" >
      <div class="uk-width-1-1">
        <div *ngIf="shift.shiftInfo.jobberPerShift.length">

          <div *ngFor="let staffInfo of shift.shiftInfo.jobberPerShift;"
               class="uk-width-1-1">
            <div *ngIf="staffInfo.uname" class="uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <div (click)="selectJobber(currentPosition, shiftIndex, staffInfo.webuser_id); removeJobber = true;"
                  [ngClass]="{'inactive': !staffInfo.had_login && !staffInfo.pending, 'red-color': staffInfo.pending}"
                  class="table-heading" style="text-align: left; white-space: nowrap;">
                    <div style="margin-left:5%; ">
                        <img  src="https://www.gravatar.com/avatar/e9893a9e9c1a2d761c2f43985f9a59d7?d=404"
                             class="jc-my-image"
                             alt="{{staffInfo.uname}}"
                             style="width:40px; border-radius: 50%; margin-right:2%;float:left">

                      <div *ngIf='false'>
                        <img  style="width:20px; border-radius: 50%; margin-right: 5px; margin-right:2%; float:left"
                              src="{{ 'https://www.gravatar.com/avatar/'+ info.webuser.mailhash }}"
                              class="jc-my-image"
                               alt="Profile">
                      </div>
                      <div class="jobberInfo">
                        <span class="jobberName">{{staffInfo.uname}}</span>
                        <span class="joberTotalHours"> W: {{staffInfo.totalHours | duration}}</span>
                     </div>
                  </div>
                </div>
                
                <!-- Top level day -->
                <div *ngFor="let item of arrayIteration; let i = index"
                     style="position:relative;top:0;" #daysOffSet
                     class="{{!(([staffInfo.webuser_id, i+1, shiftIndex, 'status'] | safe:groupedVacations) >= 2) ? getPositionShiftClass(i, staffInfo) : shift.shiftInfo.daysInfo[i].timeStatus}} uk-width-1-10 tablecell job">
                  <!-- Case this is a day-off -->
                  <template 
                    [ngIf]="!!([staffInfo.webuser_id, i+1, shiftIndex] | safe:groupedVacations) 
                            && (shift.shiftInfo.daysInfo[i].timeStatus === 'time-future' 
                                || ([staffInfo.webuser_id, i+1, shiftIndex, 'status'] | safe:groupedVacations) >= 2)">
                     
                    <div (click)="onVacationCellClick($event, staffInfo.webuser_id, groupedVacations[staffInfo.webuser_id][i+1][shiftIndex])"
                         class="day-off-position-shift"
                         [ngClass]="{ 'approved' : ([staffInfo.webuser_id, i+1, shiftIndex, 'status'] | safe:groupedVacations) >= 2, 'past': shift.shiftInfo.daysInfo[i].timeStatus === 'time-past', '': changeParent(daysOffSet)}">
                      <!-- Approved day-off reason -->
                      <span>
                        {{reasonsAbbrMapping[([staffInfo.webuser_id, i+1, shiftIndex, 'reason'] | safe:groupedVacations)] | translate}}
                      </span>
                    </div>
                    
                  </template>
                  
                  <!-- Case this is a regular day -->
                  
                  <template 
                    [ngIf]="!([staffInfo.webuser_id, i+1, shiftIndex] | safe:groupedVacations) 
                            || (shift.shiftInfo.daysInfo[i].timeStatus === 'time-past' 
                            && ([staffInfo.webuser_id, i+1, shiftIndex, 'status'] | safe:groupedVacations) !== 2)">
                            
                    <div (click)="assign(i+1, shiftIndex, currentPosition.position_id, staffInfo.webuser_id, $event, 
                         publisedShPosAss.length ? shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id] : false, staffInfo);" 
                         class="work-position-shift">
                        <div class="cell-info">
                          
                          <div *ngIf="publisedShPosAss.length && shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id] &&
                              shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].curAsClass !== 'declined'" >
                            <!--<i *ngIf="shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].info" class='commnetBubbles'  ></i>-->
                            <span>
                              {{ shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]?.from}}
                            </span>-
                            <span>
                              {{shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]?.to}}
                            </span>
                            <span class="chef">(C)</span>
                          </div>

                      <div class="hidden-while-not-assign" 
                        *ngIf="getPreviousAssignment(staffInfo.webuser_id, i) && !(publisedShPosAss.length && shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id] &&
                              shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id].curAsClass !== 'declined')">
                              {{'assign' | translate}}
                      </div>
                         
                           <span *ngIf="!publisedShPosAss.length || !shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]"
                                class="plussign">
                            +
                            </span>
                          
                            <div class="jobber"></div>
                          
                            <div class="declined"> {{'refused' | translate}}</div>
                          
                        </div>
                        
                        <div class="infomark info-alarm" [svgTemplate]="'commentIcon'"></div>
                        
                        <div *ngIf='shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]?.info'
                             class="infoMarkToolTip">
                          <span>
                            {{shift.shiftInfo.daysInfo[i].currentDayAssign[staffInfo.webuser_id]?.info?.message | textCut:10}}
                          </span>
                        </div>
                        
                      </div>
                      
                  </template>
                  
                  <div class="tooltips" *ngIf="checkToolTipe(shift,i, staffInfo.webuser_id)" >
                    <div *ngIf="addPrev" class="prev-assignment position-arrow arrow-top-right {{ !emptyBoxPrev.cl ? 'planned' : 'published' }}" >
                      <div class="inner"  [svgTemplate]="!emptyBoxPrev.cl ? 'topTriangleBlue' : 'topTriangle'"></div>
                      <div class="tooltip">
                        <span>{{ emptyBoxPrev.from}}</span>-<span>{{emptyBoxPrev.to}}</span><span>{{emptyBoxPrev.label}}</span>
                      </div>
                    </div>
                    <div *ngIf="addNext" class="next-assignment position-arrow arrow-bottom-right {{ !emptyBoxNext.cl ?  'planned' : 'published' }}">
                      <div class="inner" [svgTemplate]="!emptyBoxNext.cl ? 'downTriangleBlue' : 'downTriangle'"></div>
                      <div class="tooltip">
                        <span>{{ emptyBoxNext.from}}</span>-<span>{{emptyBoxNext.to}}</span><span>{{emptyBoxNext.label}}</span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="uk-width-1-1 uk-clearfix">
          <div class="uk-grid uk-grid-collapse">
            <div class="table-add" style="text-align: left!important; margin-left: 6rem">
              <a (click)="showAddModal(shiftIndex, currentPosition.position_id);"
                 class="table-link">{{'plussign_add_staff' |
                translate}}</a>
            </div>
          </div>
        </div>
      </div>
      <!-- table-container -->
    </div>
  </div>
</div>

<!--modal-->

<div *ngIf="openConfirm">
  <unavailable-modal
      (updateJobAssign)="updateJobAssign()"
      (close)="openConfirm = $event.action">
  </unavailable-modal>
</div>

<div *ngIf="scheduledJobberNotification">
  <section class="uk-modal uk-open" id="job-scheduled" tabindex="-1" role="dialog"
           aria-labelledby="label-jobclash" aria-hidden="false"
           style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog">
      <a (click)="closeScheduledJobberNotification($event);" class="uk-modal-close uk-close">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-jobclash">
          <h2><a (click)="closeScheduledJobberNotification($event);" class="uk-modal-close"><!--icon--></a>
            <span>{{'Add_assignment' | translate}}</span></h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form uk-form-horizontal jobclash">
          <div class="uk-grid uk-grid-small uk-text-left">
            <div class="uk-width-1-1 uk-container-center uk-grid-collapse uk-margin-bottom anotherAssignmentContent">
              <div class="uk-width-1-1">{{ replaceName('msg_jobber_has_been_scheduled' | translate)}}:</div>
                <ul>
                   <li class='uk-margin-large-left' >
                    <div class="uk-width-1-1">
                      {{weekDay[scheduledJobberNotification.assign.weekday - 1] | translate}}
                      {{shiftParse[scheduledJobberNotification.assign.shift_idx] | translate}}
                      {{scheduledJobberNotification.assign.from}}-{{scheduledJobberNotification.assign.to}},
                      {{scheduledJobberNotification.assign.pname}} - {{ scheduledJobberNotification.assign.nname }}
                    </div>
                  </li>
                </ul>
            </div>
          </div>
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-width-1-1 uk-margin-large-top uk-margin-bottom">
              <div class="uk-grid uk-grid-small">
                <div class="uk-width-1-2">
                  <button (click)="closeScheduledJobberNotification($event); updateJobAssign();"
                          type="button"
                          class="uk-button uk-button-large uk-modal-close uk-width-1-1">
                    {{'assignment_Schedule' | translate}}
                  </button>
                </div>
                <div class="uk-width-1-2">
                  <button (click)="closeScheduledJobberNotification($event);"
                          type="button"
                          class="uk-button uk-button-large uk-modal-close cancel uk-width-1-1">
                    {{'assignment_Cancel' | translate}}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>

<div *ngIf="removeJobber">
  <section class="uk-modal uk-open" id="useredit" tabindex="-1" role="dialog"
           aria-labelledby="label-useredit" aria-hidden="false"
           style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <a (click)="removeModalClose();" class="uk-modal-close uk-close" title="dismiss"
         data-close="dismiss" data-dismiss="modal">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-useredit">
          <h2>
            <span *ngIf="!jobberAssignment.length">{{'Edit_jobber' | translate}}</span>
            <span *ngIf="jobberAssignment.length"> {{'This_jobber_has' | translate}} {{jobberAssignment.length}} {{'open_assignments' | translate}}</span>
          </h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form useredit">
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div *ngIf="jobberAssignment.length" class="uk-width-1-1">
              <div class="uk-width-medium-1-1 uk-margin-bottom modalContentText">
                <!--<p>The next ones are:</p>-->
                <!--<div class="uk-text-center">{{currentPosition.pname}}: {{currentUnit.staff[futureAssignment[0].webuser_id].uname}}</div>-->
                <ul>
                  <li  *ngFor="let assignment of jobberAssignment">
                    {{weekDay[assignment.weekday - 1] | translate}}
                    {{shiftParse[assignment.shift_idx] | translate}}
                    {{assignment.from}}-{{assignment.to}},
                    {{currentPosition.pname}}
                  </li>
                </ul>
                <p>{{'would_like' | translate}}
                  <b>{{currentUnit.staff[jobberAssignment[0].webuser_id].uname}}</b>
                  {{'from_this_shift' | translate}}
                </p>
              </div>

            </div>
            <div id="useredit_ok" class="uk-width-1-1 uk-margin-bottom uk-grid uk-grid-collapse">
              <div class="uk-width-1-2" style="padding-right: 10px">
                <button (click)="deleteJobber(); removeJobber = false;" type="button"
                        class="uk-width-1-1 uk-button uk-button-large uk-modal-close remove delete uk-margin-medium-right">
                  {{'Remove' | translate}}
                </button>
              </div>
              <div class="uk-width-1-2" style="padding-left: 10px">
                <button type="button" (click)="removeModalClose(); removeJobber = false;"
                        class="uk-width-1-1 uk-button uk-button-large uk-modal-close cancel uk-margin-medium-left">
                  {{'Cancel' | translate}}
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!--useredit-->
  </section>
</div>
`;

import { Component, Input, Output, EventEmitter }   from '@angular/core';
import { unavailableModalHTML }                     from './unavailable-modal.html';
import { JobService }                               from '../../../../services/shared/JobService';

@Component({
  selector: 'unavailable-modal',
  template: unavailableModalHTML
})

export class UnavailableModalComponent {

  @Output() close = new EventEmitter();
  @Output() updateJobAssign = new EventEmitter();
  private hideScrollBar = this.jobService.hideScrollBar;

  constructor(private jobService: JobService) {}

    ngOnInit() {
      this.hideScrollBar();
    }

    updateJob() {
      this.updateJobAssign.emit({
        action: true
      });
      this.closeModal();
    }

    closeModal() {
      this.close.emit({
        action: false
      });
    }

    ngOnDestroy() {
      this.hideScrollBar();
    }
}

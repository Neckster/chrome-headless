export const unavailableModalHTML = `
<section  class="uk-modal uk-open" id="jobclash" tabindex="-1" role="dialog"
           aria-labelledby="label-jobclash" aria-hidden="false"
           style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog" >
      <a (click)="closeModal()" class="uk-modal-close uk-close">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-jobclash">
          <h2><a (click)="closeModal()" class="uk-modal-close"><!--icon--></a>
            <span>{{'Add_assignment' | translate}}</span></h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form uk-form-horizontal jobclash">
          <div class="uk-grid uk-grid-small uk-text-left ">
            <div class="uk-width-1-1 uk-container-center uk-grid-collapse uk-margin-bottom modalContentText">
              <p >{{'msg_warn_jobber_unavailable' | translate}}</p>
            </div>
          </div>
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-width-1-1 uk-margin-large-top uk-margin-bottom">
              <div class="uk-grid uk-grid-small">
                <div class="uk-width-1-2">
                  <button (click)="updateJob()"
                          type="button" class="uk-button uk-button-large ok uk-width-1-1">
                    {{'button_Schedule' | translate}}
                  </button>
                </div>
                <div class="uk-width-1-2">
                  <button (click)="closeModal()"
                          type="button"
                          class="uk-button uk-button-large uk-modal-close cancel uk-width-1-1">
                    {{'button_Cancel' | translate}}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
 </section>
`;

import {
  Component,
  Input,
  Output,
  EventEmitter }                from '@angular/core';

import * as moment              from 'moment/moment';
import { select }               from 'ng2-redux/lib/index';
import * as R                   from 'ramda';

import { EmitService }          from '../../../services/emit.service';
import { Cache }                from '../../../services/shared/cache.service';
import { ScheduleService } from '../../../services/shared/SchedulService';


import { MapToIterable }        from '../../../lib/pipes/map-to-iterable';
import { SortNamePipe }         from '../../../lib/pipes/sortName';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';
import { IconState }                from '../../../lib/enums/IconState';


import { htmlShiftPosition }    from './shift-template.htm.ts';

import { StaffGreedController } from '../staff-grid/staff-grid.component';

const getDayTime = R.pipe(
  R.path(['positions']),
  // R.sortBy(R.prop('pname')),
  R.pluck('shift'),
  R.map(R.pluck('shiftInfo')),
  R.map(R.pluck('daysInfo')),
  R.map(R.map(R.pluck('totalDayTime')))
) as any;

const processShift = R.pipe(
  getDayTime,
  R.map(R.map(R.reduce((a, x) => a += moment.duration(`${x}:00`) as any, 0)))
);

const totalProcessor = R.pipe(
  getDayTime,
  R.map(R.transpose),
  R.map(R.zipWith((x, y) => { return { day: x, val: y }; }, R.range(0, 7))),
  R.flatten,
  R.groupBy(R.prop('day')) as any,
  R.map(R.pluck('val')),
  R.map(R.map(R.reduce((a, x) => a += moment.duration(`${x}:00`) as any, 0))),
  R.map(R.sum)
  // R.values
) as any;


@Component({
  selector: 'shift-controlling',
  directives: [ StaffGreedController, SvgTemplateDirective ],
  pipes: [ MapToIterable, SortNamePipe ],
  template: htmlShiftPosition,
  styles: [`
      .positionNotification {
          position:absolute;
          top:15px;
          right: -1.5rem;
    }
    .positionTotalHours{
      margin-right: -10px;
    }
    @media (max-width: 1000px) {
       
      .positionTotalHours{
        margin-right: 0;
      }
    }
    @media (max-width: 750px) {
       .positionNotification {
            right: 3.5rem;
      }
    }
    @media (max-width: 479px) {
      .positionTotalHours {  
        margin-right: 20px !important;
        
      }
      .positionNotification{
         right: 26%;
      }

    }
    `]
})

export class ShiftController {

  @select(state => state.saveScheduleWeek) week$: any;

  @Input() currentUnit;
  @Input() shift;
  @Input() shiftIndex;
  @Input() scheduled;
  @Output() changedShift = new EventEmitter();

  @Output() recalculatedTotals = new EventEmitter(true);

  public week;
  private showList: boolean;
  private arrayIteration = new Array(7);
  private shiftParse = ['shift_0', 'shift_1', 'shift_2', 'shift_3'];
  private subscription;

  private posTimings = [];
  private shiftTimings = [];
  private unpubArray = [0, 0, 0, 0, 0, 0, 0];

  constructor(public cache: Cache,
              public scheduleService: ScheduleService,
              public emitter: EmitService) {

    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    });
  }

  ngOnInit() {
    this.showList = this.cache.isInArray(
      'schedule:shift2Opened',
      `shift_${this.shiftIndex}`
    );
  if (this.shift && this.shift.unpubArray) {
    this.unpubArray = this.shift.unpubArray;
  }
    this.subscription = this.emitter.addEvent(this.getUpdateEventName()).subscribe(el => {
      this.totalShiftEmitters(el);
    });
  }

  ngOnChanges(change) {
    this.recalculatedTotals.emit(R.values(totalProcessor(this.shift)));
    this.posTimings = processShift(this.shift);
    this.shiftTimings = R.pipe(R.transpose, R.map(R.sum))(this.posTimings);
  }

  toggleShiftCollapsible() {
    if (this.showList) {
      this.cache.removeFromArray(
        'schedule:shift2Opened',
        `shift_${this.shiftIndex}`
      );
    } else {
      this.cache.pushArray(
        'schedule:shift2Opened',
        `shift_${this.shiftIndex}`
      );
    }
    this.showList = !this.showList;
  }

  isEmpty(arrEmpty) {
    return Object.keys(arrEmpty).every(e => arrEmpty[e] === '0:00');
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.emitter.unsubscribeAll(this.getUpdateEventName());
    delete this.subscription;
    delete this.shift;
  }

  totalShiftEmitters(obj) {
    let badMarck = [],
        shiftUnpublished = 0,
        arrEmpty = {};

    this.shift.positions.map((pEl, index ) => {

      let shift = pEl.shift[obj.shiftIdx].shiftInfo;
      if (shift.publisedShPosAss.length) {
        shiftUnpublished += shift.shiftUnpublished;

        badMarck.push(shift.daysInfo[obj.weekday - 1].targetStatus);
      }
      arrEmpty[index] =  pEl.shift[obj.shiftIdx].shiftInfo.daysInfo[obj.weekday - 1].totalDayTime;
      pEl.shift[obj.shiftIdx].shiftInfo.daysInfo.forEach((day, i) => {
        this.unpubArray[i] += day.unpublishedDay;
      });

    });

    let empty = this.isEmpty(arrEmpty);

    let positionStatus =
      badMarck.includes(true) ?
        'cstat_bad time-future' : empty ?
        'cstat_empty time-future' : 'cstat_good time-future';
    this.shift.shiftUnpublish = shiftUnpublished;
    this.shift.shiftDayStatus[obj.weekday - 1] = positionStatus ;
    // this.emitter.emit('changeTree', 'shift');

    this.emitter.emit('changeTree', Object.assign({}, obj));
  }

  private getUpdateEventName() {
    return 'updatePositionEmittertrue[' + this.shiftIndex + ']';
  }
}

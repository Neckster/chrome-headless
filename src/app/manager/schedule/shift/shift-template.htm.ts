export const htmlShiftPosition:string = `
<div *ngIf="shift.active" [ngClass]="{'collapsed': !showList}"
     class="shift section" id="calshift-1">
  <div
    class="uk-clearfix schedule-row shift-1 group collapsed"
    id="shift-1">
    <div class="uk-width-1-1 uk-clearfix">
      <div  class="uk-width-small-10-10 uk-width-medium-3-10 uk-float-left ">
        <div class="uk-grid uk-grid-collapse uk-position-relative callefthead">
          <div class="uk-width-small-1-10 uk-width-large-1-10 secshowhide">
            <div class="secshow">
              <span class="icon large arrow-right grey" style="cursor: pointer" (click)="toggleShiftCollapsible()">&nbsp;</span>
            </div>
            <div class="sechide">
              <span class="icon large arrow-down blue" style="cursor: pointer" (click)="toggleShiftCollapsible()">&nbsp;</span>
            </div>
          </div>
          <div class="text uk-width-small-6-10 uk-width-large-5-10" style="white-space: nowrap">{{shiftParse[shiftIndex] | translate}}</div>


          <div class="uk-width-small-1-10 uk-width-large-1-10" >
              <span class="positionNotification">
                <span *ngIf="shift.shiftUnpublish" class="uk-badge uk-badge-notification uk-float-left " >{{shift.shiftUnpublish}}</span>
             </span>
          </div>
           <div class="uk-width-small-2-10 uk-width-large-2-10">
              <span class="uk-float-right positionTotalHours" style="white-space: nowrap">T: {{ shiftTimings[shiftIndex] | duration }}{{ 'abbr_hour_letter' | translate }}</span>
           </div>
        </div>
      </div>

      <!--  gridleft -->
      <div class="uk-hidden-small uk-width-medium-7-10 uk-float-left gridright">
        <div class="uk-width-1-1 rowhead">
          <div class="uk-grid uk-grid-collapse sevendays">
            <div  class="uk-width-1-1">

              <div *ngFor="let item of arrayIteration; let i = index" class="calday">

                <div class="{{shift.shiftDayStatus[i]}} calstat nosel">

                  <div class="calstat_icons cstat_good_mark">
                    <i [svgTemplate]="'tickInsideCircle'"></i>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_need_mark">
                    <i [svgTemplate]="'tickInsideCircleNeed'"></i>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_bad_mark">
                    <i [svgTemplate]="'exclamationInsideCircle'"></i>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                  <div class="calstat_icons cstat_empty_mark">
                    <div class="icon plus circle"></div>
                    <span *ngIf='unpubArray[i]' class="unpublishSpot"></span>
                  </div>

                </div>

              </div>

            </div>
          </div>
          <!-- sevendays uk-grid  -->
        </div>
        <!-- rowhead -->
      </div>
      <!-- gridright -->
    </div>
  </div>
  <div *ngIf="showList && shift.active" class="generic_secbody secbody">
    <div *ngFor="let position of shift.positions | sortName:'pname'; let i = index">
      <staff-grid
        (updatePositionEmitter)="totalShiftEmitter($event)"
        [overallTiming]="posTimings[i][shiftIndex]"
        [shiftIndex]="shiftIndex"
        [currentUnit]="currentUnit"
        [currentPosition]="position"
        [scheduled]="scheduled"
        [shiftPosition]="true">
      </staff-grid>
    </div>
  </div>
</div>
`;

import { Component, Input, Output, EventEmitter }  from '@angular/core';
import { AppStoreAction }                          from '../../../store/action';
import { copyHtml }                                from './modal-copy.html';
import { select }                                  from 'ng2-redux/lib/index';
import { JobService }                              from '../../../services/shared/JobService';
import { MapToIterable }                           from '../../../lib/pipes/map-to-iterable';
import { FormArray, FormBuilder, FormGroup }       from '@angular/forms';
import { Validators }                              from '@angular/common';
import { map, forEach }                            from 'ramda';
import { SortNamePipe }                            from '../../../lib/pipes/sortName';
import { ScheduledService }                        from '../../../services/scheduled.service';
import { JobPlan }                                 from '../../../models/Job';
import { Cache }                                   from '../../../services/shared/cache.service';
import * as moment                                 from 'moment';


@Component({

  // The selector is what angular internally  uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'copy-modal',
  providers: [ScheduledService],
  directives: [],
  pipes: [MapToIterable, SortNamePipe],
  styles: [`
            .buttonOk, .buttonCancel{
              width: 48%;
              cursor:pointer;
            }
            .buttonOk{
              float: left;
            }
            .buttonCancel{
              float: right;
            }
            h2, span.time{
              padding-left: 10px
            }
            span.time{
              margin-top: 15px;
              color: rgba(0, 0, 0, 0.54);
            }
            .uk-modal-header{
              margin-bottom: 0!important;
            }
          `],
  template: copyHtml
})

export class CopyModalComponent {

  @select() info$;
  @select() week$;
  @select() lang$;
  @Input() currentUnit;
  @Input() week;
  @Output() closeEmitter = new EventEmitter();

  public unsubscribe = [];
  public emptyAlert = false;
  public hideScrollBar = this.jobService.hideScrollBar;
  private copyForm;
  private unitSelected: boolean =  false;
  private tempInfo: any = {};
  private currentWeek: any;
  private lang: string;
  private currentMonth: string;

  // TypeScript public modifiers
  constructor(public action: AppStoreAction,
              public fb: FormBuilder,
              public jobService: JobService,
              public cache: Cache,
              private scheduledService: ScheduledService) {

    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
    }));
    this.unsubscribe.push(this.week$.subscribe(data => {
      let currentData = data || this.cache.get('schedule:week') || moment().format('GGWW'),
          setMoment = moment(`${currentData}`, 'GGWW');
      this.currentMonth = setMoment.lang(this.lang).format('MMMM');
      console.log(this.currentMonth);
      let currentMonthISOWeek = setMoment.format('WW'),
          currentMonthISOWeekYear = setMoment.format('GGGG');
          this.currentWeek = currentMonthISOWeek + '/' + currentMonthISOWeekYear;
    }));

  }

  ngOnInit() {
    this.hideScrollBar();
    const group = map(v => {
      return [false];
    }, this.currentUnit.positions);
    this.copyForm = this.buildForm(this.fb, group);

    this.copyForm.valueChanges.subscribe(val => {
      if (val.unit) {
      }
    });
  }

  buildForm(fb, props): FormGroup {
    let def = {
      unit: [false],
      need: [false],
      shift_0: [true],
      shift_1: [true],
      shift_2: [true],
      shift_3: [true],
      assignments: [false],
      nextWeek: ['1']
    };
    let group = Object.assign({}, def, props);
    return fb.group(group, {
      validator: (formGroup) => {
        let value = formGroup.value,
          groupOne = [],
          groupTwo = [],
          groupThree = [];
        for (let key in value) {
          if (value.hasOwnProperty(key)) {
            switch (key) {
              case 'need':
              case 'assignments':
                groupOne.push(value[key]);
                break;
              case 'shift_0':
              case 'shift_1':
              case 'shift_2':
              case 'shift_3':
                groupTwo.push(value[key]);
                break;
              case 'unit':
                groupThree.push(value[key]);
                break;
              default:
                groupThree.push(value[key]);
                break;
            }
          }
        }
        let result = groupOne.some(this.isTrue) &&
                     groupTwo.some(this.isTrue) &&
                     groupThree.some(this.isTrue);
        return result ? undefined : formGroup;
      }
    });
  }

  isTrue(b) {
    return b === true;
  }

  sendCopyFilter(e) {
    e.preventDefault();
    let form = this.copyForm.value;
    let positions = [];
    let jobs = 0;
    if (form.unit) {
      for (let i in this.currentUnit.positions) {
        if (this.currentUnit.positions.hasOwnProperty(i)) {
          positions.push(this.currentUnit.positions[i].position_id);
        }
      }
    } else {
      for (let i in form) {
        if (i && form.hasOwnProperty(i)) {
          if (parseInt(i, 10) && form[i]) {
            positions.push(typeof(i) === 'string' ? parseInt(i, 10) : i);
          }
        }
      }
    }

    let needOnly = form.need && form.assignments ? -1 : form.assignments ? 0 : 1;
    let count = typeof(form.nextWeek) === 'string' ? parseInt(form.nextWeek, 10) : form.nextWeek;
    let week = typeof(this.week) === 'string' ? parseInt(this.week, 10) : this.week;
    let self = this;

    if (this.currentUnit.job.filter) {
      jobs += this.currentUnit.job.filter((job) => {
        return positions.indexOf(job.position_id) >= 0
          && (
            (needOnly !== 1 && job.published)
            || (needOnly && job instanceof JobPlan)
          );
      }).length;
    }

    if (!jobs) {
      this.emptyAlert = true;
      return;
    }
    this.emptyAlert = false;
    this.scheduledService
      .checkForFutureAssignments(week, positions, count).subscribe((res) => {
      if (!res.empty) {
        this.closeAssessModal(e);
        this.action.openAlertCopyModal(true, {
          week: week,
          positions: positions,
          count: count,
          needOnly: needOnly
          // shifts: []
        });
      } else {
        this.scheduledService.copyWeekFor(week, positions, count, needOnly).subscribe(() => {
          self.closeAssessModal(e);
        });
      }
    });
  }

  changeUnitState(isChecked) {
    if (isChecked) {
      this.unitSelected = true;
      let posValue = {};
      this.tempInfo = {};
      for (let key in this.currentUnit.positions) {
        if (this.copyForm.controls.hasOwnProperty(key)) {
          this.tempInfo[key] = this.copyForm.value[key];
          posValue[key] = true;
        }
      }
      this.copyForm.patchValue(posValue);
    }else {
      this.unitSelected = false;
      this.copyForm.patchValue(this.tempInfo);
    }
  }

  closeAssessModal(e) {
    e.preventDefault();
    this.closeEmitter.emit(undefined);
    this.action.openCopyAnction(false);
  }

  ngOnDestroy() {
    this.hideScrollBar();
  }

  disabledPosition(s) {
    if (s) {
      let status = s.map(shift => {
        return shift && Object.keys(shift).length ? true : false;
      });
      return status.some(i => i === true) ? false : true;
    }
    return true;
  }

}

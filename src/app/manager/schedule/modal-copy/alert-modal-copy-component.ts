import { Component, Input, Output, EventEmitter }  from '@angular/core';
import { AppStoreAction }                          from '../../../store/action';
import { alertCopyHtml }                           from './alert-modal-copy.html';
import { select }                                  from 'ng2-redux/lib/index';
import { JobService }                              from '../../../services/shared/JobService';
import { ScheduledService }                        from '../../../services/scheduled.service';

@Component({
  // The selector is what angular internally  uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'alert-copy-modal',
  styles: [`
        .copyAlertMessage{
          font-size: 1.6rem;
          color: black;
        }
          `],
  template: alertCopyHtml,
  providers: [ ScheduledService ]
})

export class AlertCopyModalComponent {
  @select() info$;
  @select() copyModalData$;
  @Input() currentUnit;
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  private copyModalData;

  // TypeScript public modifiers
  constructor(public action: AppStoreAction,
              public jobService: JobService,
              public scheduledService: ScheduledService) {
  }
  ngOnInit() {
    this.hideScrollBar();
    this.copyModalData$.subscribe((data) => {
      this.copyModalData = data;
    });
  }
  send(e) {
    let self = this;

    this.scheduledService.copyWeekFor(
      this.copyModalData.week,
      this.copyModalData.positions,
      this.copyModalData.count,
      this.copyModalData.needOnly).subscribe(() => {
      self.closeAssessModal(e);
    });
  }
  closeAssessModal(e) {

    e.preventDefault();
    this.action.openAlertCopyModal(false, undefined);
  }
  ngOnDestroy() {
    this.hideScrollBar();
  }
}

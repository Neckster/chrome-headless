export const alertCopyHtml = `
<section class="uk-modal uk-open assessModel" id="jobdecline" tabindex="-1" role="dialog" aria-labelledby="label-jobdecline" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog" style="padding-bottom: 60px">
      
        <a style="cursor:pointer; z-index: 50; margin-top:5px" class="uk-modal-close uk-close"  (click)="closeAssessModal($event) "  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header uk-width-1-1" style="padding: 0!important;">
          <header id="label-jobdecline" style="box-shadow:none">
            <h2 style="padding-left: 10px">
                <a  class="arrow uk-modal-close " (click)="closeAssessModal($event)"></a>
                {{'copy' | translate}}
             </h2>
          </header>
        </div>
        <div class="modal-content uk-width-1-1" style="padding:0!important;">
          <p class="uk-width-1-1 uk-margin copyAlertMessage" style="text-align: center; line-height: 3rem; word-spacing: 3px">
            {{'copyAlert' | translate}}
          </p>
          <div class="uk-width-1-1 uk-text-center" style="margin-bottom: 30px">
            <div class="uk-width-1-1" style="padding-right: 20px">
              <button style="cursor:pointer; float: left; "
                      (click)="send($event)"
                      class="uk-button uk-button-large uk-button-primary uk-width-1-2">
                 {{'OK'}}
              </button>
            </div>
            <div class="uk-width-1-1" style="padding-left: 20px">
              <button style="cursor:pointer; float: right;"
                      (click)="closeAssessModal($event)"
                      class="uk-button uk-button-large uk-button-primary cancel uk-width-1-2">
                {{'cancel' | translate}}
              </button>
             </div> 
           </div>
        </div>
      </div>
    </section>
    `;


export const copyHtml = `
<section class="uk-modal uk-open assessModel" id="jobdecline" tabindex="-1" role="dialog" aria-labelledby="label-jobdecline" aria-hidden="false" style="display: block; overflow-y: auto;">
      <div class="uk-modal-dialog copyModal" style="padding-bottom: 60px" *ngIf="currentUnit" >

        <a style="cursor:pointer; z-index: 50; margin-top:5px" class="uk-modal-close uk-close"  (click)="closeAssessModal($event) "  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
          <!-- icon -->
        </a>
        <div class="uk-modal-header" style="padding: 0!important;">
          <header id="label-jobdecline" style="box-shadow:none;height: 100%;">
            <h2 >
                <a  class="arrow uk-modal-close " (click)="closeAssessModal($event)"></a>
                {{'copy' | translate}}
             </h2>
          </header>
        </div>
        
          <span class="time">{{currentMonth}} {{'KW' | translate}} {{currentWeek}}</span>
        <div class="modal-content" style="padding: 0!important;">
          <div id="global-message" style="padding-top: 15px">
            <div *ngIf="emptyAlert" class="uk-alert uk-alert-warning  error warn-incomplete-profile" data-uk-alert="">
              <span>{{'empty_positions_for_copy' | translate }}</span>
            </div>
          </div>
          <form [formGroup]="copyForm" class="uk-form jobdecline" (submit)="sendCopyFilter($event)">
            <div  class="copyBlock">
               <div class="copySub">{{'copy_unit_order' | translate}}</div>
               <div class="copyContent" >
                  <label class="" for="copyUnit">
                    <input formControlName="unit" type="checkbox" (change)="changeUnitState($event.target.checked)" id="copyUnit">
                    <span class="checkBoxModal" >{{ currentUnit.nname }}</span>
                  </label>
                  <ul class="uk-list" *ngIf='currentUnit.positions' style="margin-top: 5px; margin-left:20px;">
                    <li *ngFor="let position of currentUnit.positions | mapToIterable | sortName:'value.pname'; let i = index" >
                      <label class="" attr.for='{{position.key}}'>
                        <input formControlName='{{position.key}}' type="checkbox" id='{{ position.key}}' [disabled]="unitSelected || disabledPosition(position.value.staff)">
                        <span class="checkBoxModal" [style.color]="unitSelected || disabledPosition(position.value.staff) ? 'rgba(0,0,0,0.45)': 'black' ">{{ position.value.pname }}</span>
                      </label>
                      <!--<p (click)="checkValue(position.value)">Google</p>-->
                    </li>
                  </ul>
               </div>
            </div>
            <hr class="modalCopy"/>
            <div  class="copyBlock">
               <div class="copySub">{{'shifts' | translate}}</div>
               <div class="copyContent">
                 <label class="" for="shift_0">
                  <input formControlName="shift_0" type="checkbox" id="shift_0">
                  <span class="checkBoxModal">{{ 'shift_0' | translate }}</span>
                </label>
                <label class="" for="shift_1">
                  <input formControlName="shift_1" type="checkbox" id="shift_1">
                  <span class="checkBoxModal">{{ 'shift_1' | translate }}</span>
                </label>
                <label class="" for="shift_2">
                  <input formControlName="shift_2" type="checkbox" id="shift_2">
                  <span class="checkBoxModal">{{ 'shift_2' | translate }}</span>
                </label>
                <label class="" for="shift_3">
                  <input formControlName='shift_3' type="checkbox" id="shift_3">
                  <span class="checkBoxModal">{{ 'shift_3' | translate }}</span>
                </label>
               </div>
            </div>
            <hr class="modalCopy"/>
            <div  class="copyBlock">
               <div class="copySub">{{'copy_need_or_assign' | translate}}</div>
               <div class="copyContent">
                 <label class="" for="copyModalNeed">
                  <input formControlName="need" type="checkbox" id="copyModalNeed">
                  <span class="checkBoxModal">{{ 'copy_need' | translate }}</span>
                </label>
                <label class="" for="copyModalAssignments">
                  <input formControlName='assignments' type="checkbox" id="copyModalAssignments">
                  <span class="checkBoxModal">{{ 'copy_assignments' | translate }}</span>
                </label>
               </div>
            </div>
            <hr class="modalCopy"/>
            <div class="copyBlock">
               <span class="copySub">{{'copy_numbers_of_week' | translate}}</span>
               <div class="copyContent">
                  <span style="display: block">{{'copy_to' | translate}}:</span>
                  <label class="nextWeek">
                    <input  value="1"
                            formControlName="nextWeek"
                            name="nextWeek"
                            type="radio"
                            id="nextWeek">
                    <span class="checkBoxModal">{{ 'copy_next_week' | translate }}</span>
                  </label>
                  <label class="nextWeek" >
                    <input value="2"
                           formControlName="nextWeek"
                           name="nextWeek"
                           type="radio"
                           id="next2Week">
                    <span class="checkBoxModal">{{ 'copy_next_2_week' | translate }}</span>
                  </label>
                  <label class="nextWeek" >
                    <input value="3"
                           formControlName="nextWeek"
                           name="nextWeek"
                           type="radio"
                           id="next3Week">
                    <span class="checkBoxModal">{{ 'copy_next_3_week' | translate }}</span>
                  </label>
                  <label class="nextWeek" >
                    <input value="4"
                           formControlName="nextWeek"
                           name="nextWeek"
                           type="radio"
                           id="next4Week">
                    <span class="checkBoxModal">{{ 'copy_next_4_week' | translate }}</span>
                  </label>
               </div>
            </div>
            <hr class="modalCopy"/>
            <div class="uk-width-1-1 uk-text-center" style="margin-bottom: 25px;">
              <button   type="submit" [disabled]="!copyForm.valid" class="uk-button uk-button-large uk-button-primary buttonOk">{{'OK'}}</button>
              <button  (click)="closeAssessModal($event)" class="uk-button uk-button-large uk-button-primary cancel buttonCancel">{{'Cancel'}}</button>
           </div>
          </form>
        </div>
      </div>
    </section>
    `;


import { Component, Input, Output, EventEmitter, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { htmlAssign } from './assign-up.html.ts';
import { select } from 'ng2-redux/lib/index';
import { AppStoreAction } from '../../../store/action';
import moment = require('moment/moment');
import { TargetJobService } from '../../../services/job.service';
import { JobService } from '../../../services/shared/JobService';
import { EmitService } from '../../../services/emit.service';
import { Observable } from 'rxjs/Rx';



@Component({
  selector: 'assign-modal',
  providers: [],
  directives: [],
  pipes: [ TranslatePipe ],
  styles: [` .spanCheckBox:before{
                font-size: 1.7em;
                position: relative;
                top: 5px;
              }
          `],
  template: htmlAssign
})
export class AssignModalController {
  @Output() closeEmitter = new EventEmitter();
  @Input() selectedAssign;
  @Input() updateInfoDayView;
  @select(store => store.selectedAssign) selectedAssign$: any;
  @select() currentUnit$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  @select() info$: any;

  @select(state => state.localisation) lang$: any;


  assignGroup = new FormGroup({
    fromHh: new FormControl(),
    fromMm: new FormControl(),
    toHh: new FormControl(),
    toMm: new FormControl(),
    jobchef: new FormControl(),
    jobinfo: new FormControl()

  });
  public info;
  public week;
  public hideScrollBar = this.jobService.hideScrollBar;
  private currentUnit: any;
  private pname;
  private uname;
  private date;
  private lang;
  private job;
  private logTime;
  private unsubscribe = [];
  private visibleCounter: number = 0;
/* tslint:disable:no-string-literal */
  // TODO: decide on passing ngModel data or ngControl data:
  constructor(private action: AppStoreAction,
              public targetService: TargetJobService,
              private emitter: EmitService,
              public jobService: JobService) {
    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname) {
        this.currentUnit = unit;
      }
    }));
    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
    }));
    this.unsubscribe.push(this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    }));
    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data;
    }));
    // this.emitter.addEvent('selectedAssign').subscribe(el => {
    //   this.selectedAssign = el;debugger;
    // });
  }



  populateFields(data: any): Observable<any> {
    return Observable.create((observer) => {
      Object.keys(data).forEach(key => {
        if (this.assignGroup.controls.hasOwnProperty(key)) {

          this.assignGroup.controls[key].patchValue(data[key]);
          observer.next({[key]: data[key]});
        }
      });
      observer.complete();
    });
  }
  updateJobAssign() {
    // this.assignGroup.controls.toHh.value
    let toString =
      this.nToSting( this.assignGroup.controls['toHh'].value) + ':' +
      this.nToSting(this.assignGroup.controls['toMm'].value);
    let fromString =
      this.nToSting(this.assignGroup.controls['fromHh'].value) + ':' +
      this.nToSting(this.assignGroup.controls['fromMm'].value);
    let tto = +moment(this.week, 'GGWWE')
      .add(this.selectedAssign.weekday - 1, 'd')
      .add(+toString.slice(0, 2), 'h')
      .add(+toString.slice(-2), 'm');
    let tfrom = +moment(this.week, 'GGWWE')
      .add(this.selectedAssign.weekday - 1, 'd')
      .add(+fromString.slice(0, 2), 'h')
      .add(+fromString.slice(-2), 'm');
    // this.action.deleteAssign(this.selectedAssign);
    let job = {
      position_id: this.selectedAssign.position_id,
      shift_idx: this.selectedAssign.shift_idx,
      week: this.week,
      weekday: this.selectedAssign.weekday,
      webuser_id: this.selectedAssign.webuser_id,
      tto, tfrom, to: toString, from: fromString,
      info: this.assignGroup.controls['jobinfo'].value,
      chef: this.assignGroup.controls['jobchef'].value, status: 'update'
    };
    this.targetService
      .putAssign(this.selectedAssign.weekday,
        this.selectedAssign.shift_idx,
        this.selectedAssign.position_id,
        this.selectedAssign.webuser_id,
        this.week, toString, fromString, tto, tfrom, this.assignGroup.controls['jobinfo'].value,
        this.assignGroup.controls['jobchef'].value)
      .subscribe(res => {
        this.action.fetchGetInfo();
        // this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      });

    this.emitter
      .emit('updteAssign' + this.selectedAssign.position_id + this.selectedAssign.shift_idx,
        job);

    // if (this.updateInfoDayView ) { this.action.fetchGetInfo(); }
    this.closeModalEditAssign();
  }

  deleteAssign() {
    let asi = this.selectedAssign;
    asi.status = 'delete';
    this.targetService
      .deleteAssign(
        this.selectedAssign.weekday,
        this.selectedAssign.shift_idx,
        this.selectedAssign.position_id,
        this.selectedAssign.webuser_id,
        this.week, 1)
      .subscribe(res => {
        this.action.fetchGetInfo();
        // this.action.getCurrentUnit(this.currentUnit);
         // this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      });

    this.emitter
      .emit('updteAssign' + this.selectedAssign.position_id + this.selectedAssign.shift_idx,
        this.selectedAssign);
    /*if (this.updateInfoDayView ) { this.action.fetchGetInfo(); }*/
    this.closeModalEditAssign();

  }
  ngOnInit() {
    this.hideScrollBar();
    let el = this.selectedAssign;
    if (el && el.position_id && this.currentUnit) {
      this.pname = this.currentUnit.positions[el.position_id].pname;
      this.uname = this.currentUnit.staff[el.webuser_id].uname;

      this.date = moment( this.week, 'GGWWE').lang(this.lang)
        .add(el.weekday - 1, 'd').format('ddd DD MMMM GGGG');

      let jobElement =  el;

      const assignModel = {
        jobinfo: jobElement.info ? jobElement.info.message : '',
        jobchef: jobElement.chef,
        fromHh: +jobElement.from.slice(0, 2),
        fromMm: +jobElement.from.slice(-2),
        toHh: +jobElement.to.slice(0, 2),
        toMm: +jobElement.to.slice(-2)
      };
      this.job = jobElement;
      this.populateFields(assignModel).subscribe(
        () => {},
        () => {},
        () => {}
      );
      if  (jobElement.log && Array.isArray(jobElement.log) && jobElement.log.length !== 0  ) {
        this.logTime = moment(jobElement.log.pop()[3]).format('M/DD/YYYY, H:mm:ss');
      }

    }

  }

  ngOnDestroy() {
    this.unsubscribe.map(el => el.unsubscribe());
    this.action.deleteAssign({});
    this.hideScrollBar();
  }

  nToSting(n) {
    return n > 9 ? '' + n : '0' + n;
  }
  closeModalEditAssign() {
    // this.emitter.emit('hideAssignModal', false);
    this.action.openModalAssign(false);
    // this.selectedAssign = false;
    // this.hideScrollBar();
  }
  isVisible( selectedAssign ) {
    if ( this.visibleCounter ) {
      this.hideScrollBar();
      this.visibleCounter = 0;
      return true;
    }
    this.visibleCounter++;
    return true;
  }
/* tslint:enable:no-string-literal */
}


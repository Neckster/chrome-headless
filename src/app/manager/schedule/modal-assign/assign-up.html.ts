export const htmlAssign = `
  <section class="uk-modal uk-open" id="jobedit" tabindex="-1" role="dialog"
           aria-labelledby="label-jobedit" aria-hidden="false"
           style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog">
      <a (click)="closeModalEditAssign()" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss"
         data-dismiss="modal">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-jobedit">
          <h2><a  (click)="closeModalEditAssign()" class="uk-modal-close" data-close="dismiss" data-dismiss="modal"><!--icon--></a>
            <span>{{'Edit_assignment' | translate}}</span></h2>
        </div>
      </div>
      <div class="unpub needci time-current modal-content job">
        <form class="uk-form uk-form-horizontal jobedit">
          <div class="uk-grid uk-grid-collapse uk-text-left" [formGroup]="assignGroup">
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <span class="uk-form-label">{{'Jobber' | translate}}</span>
                <span class="form-value uk-width-small-1-1 uk-width-medium-6-10 inactive">{{uname}}</span>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <span class="uk-form-label">{{'Date' | translate}}</span>
                <span
                  class="form-value uk-width-small-1-1 uk-width-medium-6-10">{{date}}</span>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <span class="uk-form-label">{{'Position' | translate}}</span>
                <span
                  class="form-value uk-width-small-1-1 uk-width-medium-6-10">{{pname}}</span>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <span class="uk-form-label">{{'Shift' | translate}}</span>
                <div class="uk-width-small-1-1 uk-width-medium-6-10">
                  <div id="jobtime" class="timerange uk-width-1-1">
                    <div class="timerange uk-grid uk-grid-collapse">
                      <div class="timefrom">
                        <div class="timedial">
                          <div class="uk-grid uk-grid uk-grid-collapse">
                            <div class="hh">
                              <div class="select-wrapper uk-width-1-1">
                                <select formControlName="fromHh" class="select timehh">
                                  <!-- filled by js -->
                                  <option [ngValue]="00">00</option>
                                  <option [ngValue]="01">01</option>
                                  <option [ngValue]="02">02</option>
                                  <option [ngValue]="03">03</option>
                                  <option [ngValue]="04">04</option>
                                  <option [ngValue]="05">05</option>
                                  <option [ngValue]="06">06</option>
                                  <option [ngValue]="07">07</option>
                                  <option [ngValue]="08">08</option>
                                  <option [ngValue]="09">09</option>
                                  <option [ngValue]="10">10</option>
                                  <option [ngValue]="11">11</option>
                                  <option [ngValue]="12">12</option>
                                  <option [ngValue]="13">13</option>
                                  <option [ngValue]="14">14</option>
                                  <option [ngValue]="15">15</option>
                                  <option [ngValue]="16">16</option>
                                  <option [ngValue]="17">17</option>
                                  <option [ngValue]="18">18</option>
                                  <option [ngValue]="19">19</option>
                                  <option [ngValue]="20">20</option>
                                  <option [ngValue]="21">21</option>
                                  <option [ngValue]="22">22</option>
                                  <option [ngValue]="23">23</option>
                                </select>
                              </div>
                            </div>
                            <div class="dial-divider uk-text-center">:</div>
                            <div class="mm">
                              <div class="select-wrapper uk-width-1-1">
                                <select formControlName="fromMm" class="select timemm">
                                  <!-- filled by js -->
                                  <option [ngValue]="00">00</option>
                                  <option [ngValue]="01">01</option>
                                  <option [ngValue]="02">02</option>
                                  <option [ngValue]="03">03</option>
                                  <option [ngValue]="04">04</option>
                                  <option [ngValue]="05">05</option>
                                  <option [ngValue]="06">06</option>
                                  <option [ngValue]="07">07</option>
                                  <option [ngValue]="08">08</option>
                                  <option [ngValue]="09">09</option>
                                  <option [ngValue]="10">10</option>
                                  <option [ngValue]="11">11</option>
                                  <option [ngValue]="12">12</option>
                                  <option [ngValue]="13">13</option>
                                  <option [ngValue]="14">14</option>
                                  <option [ngValue]="15">15</option>
                                  <option [ngValue]="16">16</option>
                                  <option [ngValue]="17">17</option>
                                  <option [ngValue]="18">18</option>
                                  <option [ngValue]="19">19</option>
                                  <option [ngValue]="20">20</option>
                                  <option [ngValue]="21">21</option>
                                  <option [ngValue]="22">22</option>
                                  <option [ngValue]="23">23</option>
                                  <option [ngValue]="24">24</option>
                                  <option [ngValue]="25">25</option>
                                  <option [ngValue]="26">26</option>
                                  <option [ngValue]="27">27</option>
                                  <option [ngValue]="28">28</option>
                                  <option [ngValue]="29">29</option>
                                  <option [ngValue]="30">30</option>
                                  <option [ngValue]="31">31</option>
                                  <option [ngValue]="32">32</option>
                                  <option [ngValue]="33">33</option>
                                  <option [ngValue]="34">34</option>
                                  <option [ngValue]="35">35</option>
                                  <option [ngValue]="36">36</option>
                                  <option [ngValue]="37">37</option>
                                  <option [ngValue]="38">38</option>
                                  <option [ngValue]="39">39</option>
                                  <option [ngValue]="40">40</option>
                                  <option [ngValue]="41">41</option>
                                  <option [ngValue]="42">42</option>
                                  <option [ngValue]="43">43</option>
                                  <option [ngValue]="44">44</option>
                                  <option [ngValue]="45">45</option>
                                  <option [ngValue]="46">46</option>
                                  <option [ngValue]="47">47</option>
                                  <option [ngValue]="48">48</option>
                                  <option [ngValue]="49">49</option>
                                  <option [ngValue]="50">50</option>
                                  <option [ngValue]="51">51</option>
                                  <option [ngValue]="52">52</option>
                                  <option [ngValue]="53">53</option>
                                  <option [ngValue]="54">54</option>
                                  <option [ngValue]="55">55</option>
                                  <option [ngValue]="56">56</option>
                                  <option [ngValue]="57">57</option>
                                  <option [ngValue]="58">58</option>
                                  <option [ngValue]="59">59</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="time-divider uk-text-center">‐</div>
                      <div class="timeto">
                        <div class="timedial">
                          <div class="uk-grid uk-grid uk-grid-collapse">
                            <div class="hh">
                              <div class="select-wrapper uk-width-1-1">
                                <select formControlName="toHh" class="select timehh">
                                  <!-- filled by js -->
                                  <option [ngValue]="00">00</option>
                                  <option [ngValue]="01">01</option>
                                  <option [ngValue]="02">02</option>
                                  <option [ngValue]="03">03</option>
                                  <option [ngValue]="04">04</option>
                                  <option [ngValue]="05">05</option>
                                  <option [ngValue]="06">06</option>
                                  <option [ngValue]="07">07</option>
                                  <option [ngValue]="08">08</option>
                                  <option [ngValue]="09">09</option>
                                  <option [ngValue]="10">10</option>
                                  <option [ngValue]="11">11</option>
                                  <option [ngValue]="12">12</option>
                                  <option [ngValue]="13">13</option>
                                  <option [ngValue]="14">14</option>
                                  <option [ngValue]="15">15</option>
                                  <option [ngValue]="16">16</option>
                                  <option [ngValue]="17">17</option>
                                  <option [ngValue]="18">18</option>
                                  <option [ngValue]="19">19</option>
                                  <option [ngValue]="20">20</option>
                                  <option [ngValue]="21">21</option>
                                  <option [ngValue]="22">22</option>
                                  <option [ngValue]="23">23</option>
                                </select>
                              </div>
                            </div>
                            <div class="dial-divider uk-text-center">:</div>
                            <div class="mm">
                              <div class="select-wrapper uk-width-1-1">
                                <select formControlName="toMm" class="select timemm">
                                  <!-- filled by js -->
                                  <option [ngValue]="00">00</option>
                                  <option [ngValue]="01">01</option>
                                  <option [ngValue]="02">02</option>
                                  <option [ngValue]="03">03</option>
                                  <option [ngValue]="04">04</option>
                                  <option [ngValue]="05">05</option>
                                  <option [ngValue]="06">06</option>
                                  <option [ngValue]="07">07</option>
                                  <option [ngValue]="08">08</option>
                                  <option [ngValue]="09">09</option>
                                  <option [ngValue]="10">10</option>
                                  <option [ngValue]="11">11</option>
                                  <option [ngValue]="12">12</option>
                                  <option [ngValue]="13">13</option>
                                  <option [ngValue]="14">14</option>
                                  <option [ngValue]="15">15</option>
                                  <option [ngValue]="16">16</option>
                                  <option [ngValue]="17">17</option>
                                  <option [ngValue]="18">18</option>
                                  <option [ngValue]="19">19</option>
                                  <option [ngValue]="20">20</option>
                                  <option [ngValue]="21">21</option>
                                  <option [ngValue]="22">22</option>
                                  <option [ngValue]="23">23</option>
                                  <option [ngValue]="24">24</option>
                                  <option [ngValue]="25">25</option>
                                  <option [ngValue]="26">26</option>
                                  <option [ngValue]="27">27</option>
                                  <option [ngValue]="28">28</option>
                                  <option [ngValue]="29">29</option>
                                  <option [ngValue]="30">30</option>
                                  <option [ngValue]="31">31</option>
                                  <option [ngValue]="32">32</option>
                                  <option [ngValue]="33">33</option>
                                  <option [ngValue]="34">34</option>
                                  <option [ngValue]="35">35</option>
                                  <option [ngValue]="36">36</option>
                                  <option [ngValue]="37">37</option>
                                  <option [ngValue]="38">38</option>
                                  <option [ngValue]="39">39</option>
                                  <option [ngValue]="40">40</option>
                                  <option [ngValue]="41">41</option>
                                  <option [ngValue]="42">42</option>
                                  <option [ngValue]="43">43</option>
                                  <option [ngValue]="44">44</option>
                                  <option [ngValue]="45">45</option>
                                  <option [ngValue]="46">46</option>
                                  <option [ngValue]="47">47</option>
                                  <option [ngValue]="48">48</option>
                                  <option [ngValue]="49">49</option>
                                  <option [ngValue]="50">50</option>
                                  <option [ngValue]="51">51</option>
                                  <option [ngValue]="52">52</option>
                                  <option [ngValue]="53">53</option>
                                  <option [ngValue]="54">54</option>
                                  <option [ngValue]="55">55</option>
                                  <option [ngValue]="56">56</option>
                                  <option [ngValue]="57">57</option>
                                  <option [ngValue]="58">58</option>
                                  <option [ngValue]="59">59</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div *ngIf="job.stat && job.stat.declined" class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <span class="uk-form-label">{{'Refused' | translate}}</span>
                <div class="uk-width-small-1-1 uk-width-medium-6-10">
                  <div *ngIf="logTime">on <span>{{logTime }}</span></div>
                  <div>
                    <span>{{job.stat.declined.reason | translate}}</span>
                    <!--<span class="decline-reason reason-unavailable">{{'Unavailable' | translate}}</span>-->
                    <!--<span class="decline-reason reason-unknown">{{'Other_reason' | translate}}</span>-->
                  </div>
                  <div *ngIf="job.stat.declined.info">{{job.stat.declined.info.message}}</div>
                </div>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label checkbox" for="jobchef">{{'Leader' | translate}}</label>
                <label class="uk-form-label checkbox">
                  <input formControlName="jobchef"  type="checkbox"><span class='spanCheckBox' >&nbsp;</span>
                </label>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="jobinfo">{{'Info' | translate}}</label>
                <textarea formControlName="jobinfo" class="uk-width-small-1-1 uk-width-medium-6-10" name="info"
                          cols="300" rows="4"></textarea>
              </div>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top uk-margin-bottom">
              <div class="uk-grid uk-grid-small">
                <div class="uk-width-1-2">
                  <button (click)="updateJobAssign();" type="button"
                          class="uk-button uk-button-large ok uk-width-1-1">{{'button_OK' | translate}}
                  </button>
                </div>
                <div class="uk-width-1-2">
                  <button (click)="deleteAssign()" type="button"
                          class="uk-button uk-button-large uk-button-danger uk-width-1-1">
                    {{'button_Delete' | translate}}
                  </button>
                </div>
              </div>
            </div>
          </div>
          <!--UK-GRID-->
         </form>
      </div>
    </div>
    <!--jobdlg-->
  </section>
`;

export const htmlSchedule = `

<add-jobber-modal> </add-jobber-modal>
<create-jobber *ngIf="currentUnit" [organisationStaff]="organisationStaff" [opened]="loginModalOpened" (closeEmitter)="loginModalOpened = false"></create-jobber>
<target-modal></target-modal>
<confirm-modal></confirm-modal>
<div *ngIf="selectedAssign">
  <assign-modal [selectedAssign]="selectedAssign" [updateInfoDayView]="dayView"></assign-modal>
</div>

<copy-modal *ngIf="openCopy && currentUnit" (closeEmitter)="openCopy = false" [currentUnit]='currentUnit' [week]="week"></copy-modal>
<alert-copy-modal *ngIf="openAlertCopyModal$ |async"></alert-copy-modal>


<div *ngIf="schedulePublic">
  <section class="uk-modal uk-open" id="jobpublish" tabindex="-1" role="dialog" aria-labelledby="label-jobpublish" aria-hidden="false" style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog">
      <a (click)="closePublicModal($event)" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-jobpublish">
          <h2><a (click)="closePublicModal($event)" class="uk-modal-close"><!--icon--></a>
            <span>{{'Publish_changes' | translate}} <span>{{counter}}</span> </span>
          </h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form jobpublish">
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-width-medium-1-1 uk-margin-bottom">
              <ul>
                <li *ngFor="let publish of unpublished">{{publish.weekDay | translate}} {{publish.shift  | translate}} {{publish.from}}-{{publish.to}}, {{publish.position}}: {{publish.webuser}}</li>
              </ul>
            </div>
            <div class="uk-width-1-1 uk-margin-bottom">
              <button (click)="sendPublish();" type="button" class="uk-button uk-button-large ok uk-width-1-1"> {{'Publish_changes' | translate}} <span> {{counter}} </span> </button>
            </div>
          </div>
          <!--UK-GRID-->
        </form>
      </div>
    </div>
  </section>
</div>

<div class="page schedule plannerview uk-clearfix" data-view="schedule">
  <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    <div class="uk-width-1-1 uk-container-center uk-margin-bottom ">
      <!-- 100% width -->
      <div *ngIf="!dayView && !weekPrintView && !dayPrintView" class="uk-grid uk-grid-collapse uk-flex uk-flex-bottom scheduleHeader " [ngClass]="{'dayViewChange': dayView}">
        <div class='uk-width-1-1'>
          <h1  class="uk-margin-bottom">{{'Schedule' | translate}}</h1>
          <!--<div *ngIf="dayView" class="uk-width-1-1 uk-margin-bottom dayViewTitle">
            <h2 class="uk-width-1-1 titleDate" style="margin:0!important;">{{'Sa 30.02.2016'}}</h2>
            <div class="uk-width-1-1 titleNote">{{'Note text'}}</div>
          </div>-->
				</div>
				<!--<div [ngClass]="{'uk-width-small-1-1 uk-width-medium-7-10 uk-width-large-8-10': !dayView,
				                 'uk-width-small-1-1 uk-width-medium-6-10 uk-width-large-7-10': dayView }">
					&lt;!&ndash;<div class="uk-grid uk-grid-small">
						&lt;!&ndash;<div class="uk-width-large-2-10 uk-margin-bottom dayToggleButtonWrap">
						  <div *ngIf="tempVar(currentUnit)" class="uk-button-group">
                  <button class="uk-button dayView" [ngClass]="{'activeView': dayView }" (click)="toggleDayView(true)">{{'Day' | translate}}</button>
                  <button class="uk-button dayView" [ngClass]="{'activeView': !dayView }" (click)="toggleDayView(false)">{{'Week' | translate}}</button>
              </div>
              <div *ngIf="!tempVar()" class="uk-button-group">
                  <button class="uk-button dayView" >{{'Day' | translate}}</button>
                  <button class="uk-button dayView" >{{'Week' | translate}}</button>
              </div>
              
						</div>&ndash;&gt;                                                         &lt;!&ndash;uk-width-small-1-3 uk-width-medium-3-10&ndash;&gt;
						&lt;!&ndash;<div class="uk-width-large-3-10 shiftToggleButtonWrap uk-margin-bottom" >
							<div class="uk-button-group" *ngIf="!dayView && tempVar(currentUnit)">
                  <button class="uk-button dayView" [ngClass]="{'activeView': switchView }" (click)="toggleView(true)">{{'Shift' | translate}}</button>
                  <button class="uk-button dayView" [ngClass]="{'activeView': !switchView }" (click)="toggleView(false)">{{'Position' | translate}}</button>
              </div>
              <div class="uk-button-group" *ngIf="!dayView && !tempVar()">
                  <button class="uk-button dayView" >{{'Shift' | translate}}</button>
                  <button class="uk-button dayView" >{{'Position' | translate}}</button>
              </div>
						</div>    &ndash;&gt;                                                   &lt;!&ndash;uk-width-small-1-3 uk-width-medium-3-10&ndash;&gt;
						&lt;!&ndash;<div class="uk-width-large-1-10 copyButtonWrap">
                  <div>
                     <button *ngIf="!dayView" class="copyButton uk-align-center" [disabled]="copyButtonDisabled()" type="button" (click)="openCopy = !openCopy; ">
                       <i  [svgTemplate]="'copyDoc'" ></i>
                     </button>
                  </div>
                  
            </div>&ndash;&gt;                                                                               &lt;!&ndash;uk-width-small-1-3 uk-width-medium-2-10&ndash;&gt;
            &lt;!&ndash;<div *ngIf='!dayView' class="uk-width-large-1-10 printButtonWrap">
                  <div>
                     <button *ngIf="true" class="copyButton uk-align-center" [disabled]="false" type="button">
                       <i  [svgTemplate]="'printIcon'" ></i>
                     </button>
                  </div>
            </div>    &ndash;&gt;                                                                          &lt;!&ndash;uk-width-small-1-3 uk-width-medium-1-10&ndash;&gt;
						&lt;!&ndash;<div *ngIf="!counter && !dayView" class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-3-10 uk-text-right uk-margin-bottom">
							<button type="button" class="uk-button uk-width-1-1 uk-text-truncate" disabled="">
								<span class="disabled">{{'changes_plural_zero' | translate}}</span>
							</button>
						</div>
						<div *ngIf="counter && !dayView" class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-3-10 uk-text-right uk-margin-bottom">
							<button (click)="publicButton = true; hideScrollBar() " type="button" class="uk-button uk-width-1-1 uk-text-truncate">
								<span class="enabled">{{'Publish_changes' | translate}}  <span> {{counter}} </span> </span>
								<span class="disabled">{{'changes_plural_zero' | translate}}</span>
							</button>
						</div>&ndash;&gt;
					</div>&ndash;&gt;
				</div>-->
			</div>


			<div *ngIf="!dayView && !weekPrintView && !dayPrintView" id="schedule-grid" class="uk-width-1-1 uk-grid uk-grid-collapse">
        <!-- removed: getWidthMainContent() -->
			  <div  id='scheduleStickyContainerId' class='uk-width-1-1' >
					<div  class="uk-width-small-10-10 uk-width-medium-3-10 uk-width-large-3-10 dayhead nosel " style="margin: 0px; float: left; height: 5rem;">
						<div class="kw-info uk-flex uk-flex-space-between uk-flex-middle" style="height: 5rem; line-height: 5rem;">
							<div class="kw-action uk-width-small-5-10 uk-width-large-4-10">
								<div class="uk-grid uk-grid-collapse">
									<span (click)="loadWeek(-1)" class="icon arrow-left uk-width-2-10 uk-visible-large"><span
                    class="label">{{'last_week' | translate}}</span></span>
									<span (click)="loadWeek(-1)" class="icon arrow-left uk-width-2-10 uk-hidden-large"><span
                    class="label">{{'yesterday' | translate}}</span></span>
									<span class="today uk-width-6-10">
                    <span (click)="loadWeek(0)" class="text">{{'today' | translate}}</span>
									<span class="date">Fr, 13. May 2016</span>
									</span>
									<span (click)="loadWeek(1)" class="icon arrow-right uk-width-2-10 uk-visible-large">
									  <span class="label">{{'next_week' | translate}}</span>
									</span>
									<span (click)="loadWeek(1)" class="icon arrow-right uk-width-2-10 uk-hidden-large"><span
                    class="label">{{'tomorrow' | translate}}</span></span>
								</div>
							</div>
							<div class="uk-width-small-2-10 uk-width-large-2-10 align-center">
                <div style="position:relative;padding-left: 50%;  max-width:30px ">
                   <i   (click)="showCalendar()" [svgTemplate]="'calendar'" style="cursor: pointer;"></i>
                  <div *ngIf="showCalendarVar">
                    <my-date-picker (selectedDate)="setDate($event)"
                                    [valueDate]="valueDate"
                                    [valueDay]="valueDay"
                                    [dayIn]="day"></my-date-picker>
                  </div>
                </div>
              </div>
							<div class="kw-date uk-width-small-5-10 uk-width-large-5-10" style="padding-left: 30px">
								<h2>{{showMonth}}</h2>
								<h3>{{'KW' | translate}} {{currentMonthISOWeek}}/{{currentMonthISOWeekYear}}</h3>
							</div>
						</div>
					</div>


					<div class="uk-hidden-small uk-width-medium-7-10 uk-width-large-7-10 dayhead nosel " style="margin: 0px; float: left">
						<div class="kw-days">
							<div class="uk-width-1-1">
								<div class="uk-grid uk-grid-collapse sevendays">
									<div class="uk-width-1-1" style="height: inherit">
										<div
										  *ngFor="let a of acc;let i = index"
										  [ngClass]="{today: isToday(i)}"
										  class="calday parentNote"
										  style='white-space: nowrap; height: inherit'
										  
										  (mouseover)="isDayHovering[i] = true"
										  (mouseout)="isDayHovering[i] = false">
										  <span (click)="checkItOnDayView(currentMonthISOWeek, i)">  
                        <span class="wday">{{weekDay[i] | translate}}</span>
                        <span class="mday">{{acc[i]}}.</span>
											</span>
										  <note
										    (noteUpdate)="handleNoteUpdate($event)"
										    [hovering]="isDayHovering[i]"
										    [idx]="i"
										    [week]="currentMonthISOWeek"
										    [year]="currentMonthISOWeekYear"
										    [isDayCurrent]="isToday(i)"
										    [noteBody]="notes[i]?.body"
										    [unit]="currentUnit">
                      </note>
										</div>
									</div>
								</div>
								<!-- sevendays uk-grid  -->
							</div>
						</div>
					</div>
        </div>
        <div class="uk-width-1-1 main" id='mainScheduleContent'  >
          <div id="cal">
            <div class="allshifts section" id="allshifts">
              <div class="uk-clearfix schedule-row heading" id="unit-150639">
                    <div class="uk-width-1-1 uk-clearfix">

                      <div class="uk-width-small-6-10 uk-width-medium-2-10 uk-float-left">
                        <div class="uk-grid uk-grid-collapse uk-position-relative callefthead">
                          <div *ngIf="currentUnit" class="text uk-width-small-5-6 uk-width-large-7-8">
                            {{ currentUnit.nname }}
                          </div>
                        </div>
                      </div>

                      <div class="uk-width-medium-1-10 uk-float-left uk-align-center" style="text-align: center; ">
                         <span class="uk-text-bold" >T: {{ this.overallTotalTiming | duration }}{{ 'abbr_hour_letter' | translate }}</span>
                      </div>

                      <div class=" uk-width-medium-7-10 uk-float-left">
                         <div *ngFor="let timing of totalWeekdayTimings" class="uk-float-left uk-text-center day-cell">
                           <span class="uk-text-bold">{{ timing | duration }}{{ 'abbr_hour_letter' | translate }}</span>
                         </div>
                      </div>
                    </div>
                  </div>
                <div *ngIf="tempVar() && !switchView && currentUnit" class="secbody">
                  <div *ngFor="let position of scheduleTree | sortName:'pname' " style='border-bottom: 0.1rem solid rgba(0, 0, 0, 0.18);'>
                   <position-controlling (changedPosition)="newPosition($event)"
                                         (recalculatedTotals)="updateTotals($event, 'POSITION')"
                                         [currentUnit]="currentUnit"
                                         [scheduled]="scheduled"
                                         [currentPosition]="position">
                    </position-controlling >
                  </div>
                </div>

                <div *ngIf=" switchView  && currentUnit" class="secbody">
                    <div *ngFor="let shifts of shiftTree; let i = index"
                         class="secbody">
                      <shift-controlling (changedShift)="newPosition($event)"
                                          (recalculatedTotals)="updateTotals($event, 'SHIFT')"
                                          [currentUnit]="currentUnit"
                                          [scheduled]="scheduled"
                                          [shiftIndex]="i"
                                          [shift]="shifts">
                      </shift-controlling>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      
      <div *ngIf="dayView && !weekPrintView && !dayPrintView">
          <days-controlling
              (changeWeekEvent)="changeWeekDayView($event)"
              (datepickerEvent)="changeDate"
              [valueDate]="valueDate"
              [unit]="currentUnit"
              [timeDayview]="timeDayview"
              [weeklyNotes]="notes"
              ></days-controlling>
      </div>
      
      
      <div *ngIf="weekPrintView">
        <print-view [positionsIn]="scheduleTree"
                    [unitId]="currentUnit.unit_id"
                    [week]='week'>
        </print-view>
      </div>
      <div *ngIf="dayPrintView">
        <print-view [positionsIn]="scheduleTree" 
                    [unitId]="currentUnit.unit_id"
                    [week]='week' 
                    [weekDay]=true>
        </print-view>
      </div>
    </div>
    <!-- column -->
  </div>
  <!-- grid -->
</div>

`;

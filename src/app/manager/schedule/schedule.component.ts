import {
  Component,
  HostListener }                    from '@angular/core';
import { FormControl }              from '@angular/forms';

import { select }                   from 'ng2-redux/lib/index';
import * as moment                  from 'moment';
import { allPass, path, range }     from 'ramda';
import { Observable }               from 'rxjs/Observable';

import * as R                       from 'ramda';

import { Cache }                    from '../../services/shared/cache.service';
import { WebuserService }           from '../../services/webuser.service';
import { ScheduleService }          from '../../services/shared/SchedulService';
import { CacheService }             from '../../services/shared/CacheService';
import { EmitService }              from '../../services/emit.service';
import { UnitService }              from '../../services/unit.service';
import { JobService }               from '../../services/shared/JobService';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';

import { MapToIterable }            from '../../lib/pipes/map-to-iterable';
import { SortNamePipe }             from '../../lib/pipes/sortName';
import { DurationPipe }             from '../../lib/pipes/duration';
import { IconState }                from '../../lib/enums/IconState';

import { Shift, ShiftInfo }         from '../../models/Shift';
import { Position }                 from '../../models/Position';
import {
  Assignment,
  AssignmentFactory
}                                   from '../../models/Assignment';
import { Job, JobFactory, JobPlan } from '../../models/Job';

import { htmlSchedule }             from './schedule.htm.ts';

import { AppStoreAction }           from '../../store/action';

import { PositionSchedule }         from './positions/position.component';
import { AddJobberModalController } from './modal-add-jobber/add-pop-up.component';
import { CreateJobberController }   from '../staff/create-jober/jobber-form.component';
import { TargetModalController }    from './modal-target/target-up.component';
import { AssignModalController }    from './modal-assign/assign.component';
import { ShiftController }          from './shift/shift.component';
import { CopyModalComponent }       from './modal-copy/modal-copy-component';
import { AlertCopyModalComponent }  from './modal-copy/alert-modal-copy-component';
import { ManagerDaysView }          from './days-view/days-view-component';
import { DatePikerComponent }       from '.././date-picker/date-picker';
import { Note }                     from './note/note.component';
import { PrintViewComponent }       from './printView/print-view-component';


const zipWithAdd = R.zipWith(R.add) as any;
const reduceSum = R.reduce(R.add as any, 0 as number);

const FIRST_RUN: number = 1;
const ONE_RUN: number = 1;
const WEEKDAYS: number = 7;

const SHIFT: string = 'SHIFT';
const POSITION: string = 'POSITION';

@Component({
  selector: 'schedule-controlling',
  directives: [
    PositionSchedule,
    AddJobberModalController,
    CreateJobberController,
    TargetModalController,
    AssignModalController,
    ShiftController,
    CopyModalComponent,
    ManagerDaysView,
    DatePikerComponent,
    AlertCopyModalComponent,
    Note,
    PrintViewComponent
  ],
  providers: [],
  pipes: [ MapToIterable, SortNamePipe, DurationPipe ],
  styles: [],
  template: htmlSchedule
})

export class ManagerSchedule {

  @select() info$: any;
  @select() currentUnit$: any;
  @select(state => state.showCreateJobberBool) modal$: any;
  @select(state => state.localisation) lang$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  @select(state => state.openModalAssign) openModalAssign$: any;
  @select() openAlertCopyModal$;
  @select(state => state.weekNotes) notes$: any;
  @select() scheduled$: any;
  @select() dayView$: any;
  @select() switchView$: any;
  @select() openCopy$: any;
  @select() schedulePublic$: any;
  @select() weekPrintView$: any;
  @select() dayPrintView$: any;

  // @select(state => state.notesError) notesError$: any;

  public checkUnit;


  public info;
  public store;
  public currentUnit;
  public week;
  public userId;
  public acc = [];
  public currentMonthISOWeek;
  public currentMonthISOWeekYear;
  public currentMonthStart;
  public organisationStaff;
  public loginModalOpened;
  public unsubscribe = [];
  public unpublished = [];
  public counter = 0;
  public switchView: boolean = true;
  public dayView: boolean;
  public publicButton = false;
  public lang;
  public hideScrollBar = this.jobService.hideScrollBar;
  public shiftParse;
  public weekDay;
  public scheduleTree;
  public shiftTree;
  public selectedAssign;
  public showCalendarVar;
  /**
   * Contain {jobberId: {'weekday:shift_idx1': Assignment, 'weekday-shift_idx': Assginment}}
   */
  public jobbersOnWork: any;

  private momentWeek: any = moment().startOf('isoWeek');
  private unitShift: Array<Shift>;
  private treeSubscriber;
  private width;
  private dateInput;
  private totalWeekdayTimings: number[];
  private overallTotalTiming: number;

  private selDate: string = 'DD-MM-YYYY';
  private minDate: string = '01-01-2016';
  private maxDate: string = '12-31-2045';
  private disableDays: Array<number> = [];
  private toContainPrevMonth: boolean = true;
  private toContainNextMonth: boolean = true;
  private valueDay;
  private valueDate;
  private notes: any[] = [];
  private scheduled: any = {};
  private isDayHovering = [];

  private updateRounds: number = 0;
  private finishedUpdateRound: boolean = false;

  private timeDayview: any =  {};
  private showMonth: string;
  private openCopy: any;
  private schedulePublic: any;

  private weekPrintView: boolean = false;
  private dayPrintView: boolean = false;

  constructor(public action: AppStoreAction, public cache: Cache,
              public publishService: UnitService, public jobService: JobService,
              private emitter: EmitService, private webuserService: WebuserService,
              private scheduleService: ScheduleService
  ) {

    this.jobbersOnWork = {};

    this.dateInput = new FormControl();

    this.shiftParse = this.webuserService.shiftParse;

    this.weekDay = this.webuserService.weekDay;

    this.week$.subscribe(storeWeek => {
      this.week = this.cache.get('schedule:week') || storeWeek || this.calculateWeek(undefined);
      this.momentWeek = moment(this.week, 'GGWW');
    });


    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      let self = this;

      if (info.manager) {
        this.userId = info.manager.selected.unit_id;
        if (this.userId !== 0 && !isNaN(this.userId)) {
          this.action.fetchCurrentUnit(this.userId, this.week);
        }
      }
    }));


    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
      this.changeWeekValue();
    }));

    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname) {
        this.userId = unit.unit_id;
        if (unit.job && unit.job.length) {
          this.prepareUnitJobs(unit);
        }
        this.currentUnit = unit;
        this.unitShift = [];
        for (let shiftData of this.currentUnit.shift) {
          this.unitShift.push(new Shift(
            '',
            shiftData.positions,
            shiftData.from,
            shiftData.to,
            shiftData.active,
            shiftData.hours,
            shiftData.bubbles,
            shiftData.shiftDayStatus,
            shiftData.shiftUnpublish,
            shiftData.shiftInfo
          ));
        }
        this.publishButton();
        this.parseSettingsStaff();

        this.action.fetchGetNotes(this.week, unit.unit_id);
        this.action.fetchJobberScheduledData(this.week, this.userId);

        if (unit.positions) {
          this.scheduleTree = this.initscheduleTree(unit);
          this.swapInitscheduleTree();
        }
      }
    }));

    this.unsubscribe.push(this.scheduled$.subscribe(val => {
      this.scheduled = val;
    }));

    this.unsubscribe.push(this.notes$.subscribe(val => {
      this.notes = val;
    }));
    this.unsubscribe.push(this.weekPrintView$.subscribe( weekPrintView => {
      this.weekPrintView = weekPrintView;
    }));
    this.unsubscribe.push(this.dayPrintView$.subscribe( dayPrintView => {
      this.dayPrintView = dayPrintView;
    }));

    /*this.unsubscribe.push(this.dayPrintView$.subscribe( dayPrintView => {
      this.dayPrintView = dayPrintView;
    }));*/

    if (this.cache.get('schedule:week')) {
      this.momentWeek = moment(this.cache.get('schedule:week'), 'GGWW').startOf('isoWeek');
      this.changeWeekValue();
      this.calculateWeek(
        undefined,
        this.cache.get('schedule:week').substr(2),
        this.cache.get('schedule:week').substr(0, 2)
      );
      // this.callActions(this.userId, this.week);
    }

    this.action.saveScheduleWeek(this.week);

    this.modal$.subscribe(el => {
      this.loginModalOpened = el;
    });

    this.treeSubscriber = this.emitter.addEvent('changeTree').subscribe(el => {
      this.newPositions(el);
    });

    // this.notesError$.subscribe(res => {
    // });

    this.openModalAssign$
        .subscribe(el => this.selectedAssign = el);
    // let keyDown$ = Observable.fromEvent(document, 'keydown');
    // let mouseMove$ = Observable.fromEvent(document, 'mousemove');
    this.dayView$.subscribe( dayView => {
      this.dayView = dayView;
    });
    this.switchView$.subscribe( switchView => {
      this.switchView = switchView;
      this.cache.set('schedule:view', this.switchView);
    });
    this.openCopy$.subscribe( openCopy => {
      this.openCopy = openCopy;
    });
    this.schedulePublic$.subscribe( schedulePublic => {
      if (schedulePublic) {
        this.sendPublish();
      }
      // this.schedulePublic = schedulePublic;
    });
  }

  prepareUnitJobs(unit) {

    unit.job = unit.job.map(el => JobFactory.createJob(el));

    unit.job.forEach((item: Job) => {
      unit.job.forEach(function (job: Job) {

        if (
          job.weekday === item.weekday
          && job.webuser_id === item.webuser_id
        ) {
          if (job.shift_idx + 1 === item.shift_idx) {
            item.prevJob = job;
          } else if (job.shift_idx - 1 === item.shift_idx) {
            item.nextJob = job;
          }
        }
      });
    });
  }

  @HostListener('window:scroll', ['$event'] )
  track (event) {
    let w = window;
      let fixEl = document.getElementById('scheduleStickyContainerId');
      if (fixEl) {
        if (w.scrollY > 120) {
          fixEl.classList.add('fixSticky');
        }else if (w.scrollY < 120) {
          fixEl.classList.remove('fixSticky');
        }
      }
  }




  getWidthMainContent() {
    let el = document.getElementById('mainScheduleContent');
    if (!this.width) {
      this.width = el.clientWidth;
      if (el) return el.clientWidth + 'px';
    }
  }

  ngOnInit() {
    this.changeWeekValue();
    this.switchView = JSON.parse(this.cache.get('schedule:view'));
    this.dateInput.valueChanges.subscribe((el) => {
      // console.log(el);
    });
  }

  ngOnDestroy() {
    this.unsubscribe.map(el => el.unsubscribe());
    this.emitter.unsubscribeAll('changeTree');
    this.treeSubscriber.unsubscribe();
    delete this.treeSubscriber;
    delete this.scheduleTree;
    delete this.shiftTree;
  }

/*  toggleView(i) {
    this.switchView = i;
    this.cache.setObject('schedule:view', this.switchView);
  }*/

  calculateWeek(date, week?, year?) {
    let currentMonthISOWeek = week || moment(date).format('WW');
    let currentMonthISOWeekYear = year || moment(date).format('GG');
    let calendarCurrentWeek = currentMonthISOWeekYear +
      currentMonthISOWeek;
    return calendarCurrentWeek;
  }

  loadWeek(weekDiff: number) {
    this.momentWeek = !weekDiff ?
      moment().startOf('isoWeek') :
      moment(this.momentWeek).add(weekDiff, 'week');

    this.week = !weekDiff ?
      this.calculateWeek(undefined) :
      this.calculateWeek(this.momentWeek);
    this.cache.set('schedule:week', this.week);
    this.callActions(this.userId, this.week);

    this.action.fetchGetNotes(this.week, this.currentUnit.unit_id);
  }

  isToday(day) {
    return +this.week === +this.calculateWeek(undefined) &&
      moment().format('DD') === this.acc[day];
  }

  callActions(userId, week) {
    if (userId) {
      this.action.fetchCurrentUnit(userId, week);
      this.changeWeekValue();
      this.action.saveScheduleWeek(week);
    }
  }

  changeWeekValue() {

    this.totalWeekdayTimings = [];
    this.overallTotalTiming = 0;
    CacheService.cleanCache();
    let currentMonthISOWeekYear = moment(this.momentWeek).format('GGGG');
    let currentMonthISOWeek: any = +moment(this.momentWeek).format('WW');
    let currentMonthStart = moment(this.momentWeek).lang(this.cache.get('lang')).format('MMMM');

    let endOfCurrentMonth = moment(this.momentWeek).format('WW'),
    startOfNextMonth = moment(this.momentWeek).add(1, 'month')
                          .startOf('month').lang(this.cache.get('lang')).format('WW');

    if (endOfCurrentMonth === startOfNextMonth) {
      this.showMonth = currentMonthStart + ' - ' +
            moment(this.momentWeek).add(1, 'month').lang(this.cache.get('lang')).format('MMMM');
    }else {
      this.showMonth = currentMonthStart;
    }

    let accArr = [];
    for (let i = 0; i < 7; i++) {
      accArr.push(moment(this.momentWeek).add(i, 'days').format('DD'));
    }
    this.acc = accArr;
    this.currentMonthISOWeekYear = currentMonthISOWeekYear;
    this.currentMonthISOWeek = currentMonthISOWeek;
    this.currentMonthStart = currentMonthStart;
    this.valueDate = moment().month(this.currentMonthStart).year(this.currentMonthISOWeekYear);

    // day view date
    this.timeDayview.currentMonthISOWeekYear = this.currentMonthISOWeekYear;
    this.timeDayview.currentMonthISOWeek = this.currentMonthISOWeek;
    this.timeDayview.currentMonthStart = this.currentMonthStart;
  }

  parseSettingsStaff() {


    ParseOrganisationService.treeStruct(this.info.manager.organisation);
    this.organisationStaff = {
      'unitOrganisation': ParseOrganisationService
        .unitPosition(ParseOrganisationService.treeData[0].child),
      'groupOrganisation': ParseOrganisationService.htmlOptionTree(
        ParseOrganisationService.treeData[0])
    };
  }
  /* tslint:disable */
  initscheduleTree(unit: any) {


    this.scheduleService.setWeek(this.week);
    if (!Object.keys(unit.positions).length) return;

    let positions = Object.keys(unit.positions).map((pos) => {
      let unpublishedAssign = 0,
          unpubArray = [0, 0, 0, 0, 0, 0, 0],
        position = new Position(
          unit.positions[pos].position_id,
          unit.positions[pos].company_id,
          unit.positions[pos].home_staff,
          unit.positions[pos].pname,
          [],
          unit.shift,
          unit.positions[pos].staff,
          unit.positions[pos].unit_id,
          0
        );

      position.shift = position.shift.map((shiftObj, shiftIdx) => {
        let shiftInfo = this.scheduleService
          .initScheduleAss(unit.positions[pos], shiftIdx, this.currentUnit, this.jobbersOnWork);
        shiftInfo = new ShiftInfo(
          shiftInfo.daysInfo,
          shiftInfo.jobberPerShift,
          shiftInfo.publisedShPosAss,
          shiftInfo.shiftUnpublishArr,
          shiftInfo.shiftUnpublished,
            shiftIdx
        );

        if (shiftInfo && shiftInfo.shiftUnpublished)
          unpublishedAssign += shiftInfo.shiftUnpublished;
          shiftInfo.shiftUnpublishArr.forEach( (el, i) => {
           if (!el) return false;

            unpubArray[i] += el;
          });
        return new Shift(
          shiftObj.name,
          shiftObj.positions,
          shiftObj.from,
          shiftObj.to,
          false,
          shiftObj.hours,
          shiftObj.bubbles,
          shiftObj.shiftDayStatus,
          shiftObj.shiftUnpublish,
          shiftInfo as any
        );
      });
      position.positionStatus = [];
      if (unit.job instanceof Array) {
        for (let day = 0; day < 7; day++) {
          position.positionStatus.push(this.scheduleService.cstat(day, position.shift));
        }
      }
      position.unpublish = unpublishedAssign;
      position['unpubArray'] = unpubArray;
      return position;
    });
    return positions;
  }
  /* tslint:disable */

  newPositions(shiftToPositionBool) {


    // let obj = shiftToPositionBool;
    this.counter = 0;
    this.unpublished = [];
    // this.swapInitscheduleTree();
    //
    // this.scheduleTree = this.shiftTree[0].positions;

    // this.scheduleTree.forEach((el, index) => {
    //   if (el.pname === obj.position.pname) {
    //     this.scheduleTree[index] = Object.assign({}, el, obj.position);
    //   }
    // });

    if( !this.scheduleTree) return;
    this.scheduleTree = this.scheduleTree.map(pEl => {

      pEl.unpublish = pEl.shift.reduce((a, c) => a += c.shiftInfo.shiftUnpublished, 0);
      return pEl;
    });
    this.scheduleTree.forEach(data => {
      data.shift.forEach(shift => {
        shift.shiftInfo.publisedShPosAss.forEach(assign => {
          // if (assign.weekday === 6)
          if (!assign.published && (!assign.stat || !assign.stat.declined)) {
            this.counter++;
            let publishInfo = {
              weekDay: this.weekDay[assign.weekday - 1],
              shift: this.shiftParse[assign.shift_idx],
              from: assign.from,
              to: assign.to,
              position: this.currentUnit.positions[assign.position_id].pname,
              webuser: this.currentUnit.staff[assign.webuser_id]
                ? this.currentUnit.staff[assign.webuser_id].uname
                : ''
            };
            // console.log(publishInfo);
            this.unpublished.push(publishInfo);

          }
        });
      });
    });
    this.action.schedulePublishCounter(this.counter);
    this.swapInitscheduleTree();
  }

  // publish button event
  publishButton() {
    this.counter = 0;
    this.unpublished = [];

    if (Array.isArray(this.currentUnit.job)) {

      this.currentUnit.job.forEach(data => {

       if(!data.published){
         // console.log(data);
         }



        if (!data.published && (!data.stat || !data.stat.declined) && !(data instanceof JobPlan)) {
          this.counter++;

          let publishInfo = {
            weekDay: this.weekDay[data.weekday - 1],
            shift: this.shiftParse[data.shift_idx],
            from: data.from,
            to: data.to,
            position: this.currentUnit.positions[data.position_id].pname,
            webuser: this.currentUnit.staff[data.webuser_id]
              ? this.currentUnit.staff[data.webuser_id].uname
              : ''
          };
          this.unpublished.push(publishInfo);
        }
      });
      this.action.schedulePublishCounter(this.counter);
    }

  }

  sendPublish() {
    this.publish();
    console.log(this.currentUnit.unit_id, this.week);
    this.publishService.publish(this.currentUnit.unit_id, this.week)
      .subscribe(() => {
        this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      });
    this.closePublicModal(event);
  }

  publish() {
    if (this.scheduleTree && this.scheduleTree.length) {
      this.scheduleTree = this.scheduleTree.map(data => {
        let pos = data;
        pos.shift = data.shift.map(shift => {
          let oneShift = shift;
          oneShift.shiftInfo.publisedShPosAss = oneShift.shiftInfo.publisedShPosAss.map(assign => {
            let oneAssign = assign;
            if (!oneAssign.published) {
              oneAssign.published = true;
              return oneAssign;
            }
            return oneAssign;
          });
          oneShift.shiftInfo.daysInfo.map(day => {
            let oneDay = day;
            oneDay.unpublishedDay = 0;
            for (let key in oneDay.currentDayAssign) {
              if (oneDay.currentDayAssign.hasOwnProperty(key) &&
                oneDay.currentDayAssign[key].curAsClass !== 'declined') {
                oneDay.currentDayAssign[key].curAsClass = 'empty sstatus_current';
              }
            }
            return oneDay;
          });
          oneShift.shiftInfo.shiftUnpublished = 0;
          return oneShift;
        });
        pos.unpublish = 0;
        return pos;


      });
      this.swapInitscheduleTree();
      this.action.schedulePublishCounter(0);
      this.unpublished = [];
    }
  }

  swapInitscheduleTree() {


    if (!this.unitShift ) return;
    let mainCounter = 0;

    this.shiftTree = this.unitShift.map((sEl, shIdx) => {
      let newShift = sEl;
      let shiftDayStatus = [];
      let shiftUnpublish = 0;
      let unpubArray = [0, 0, 0, 0, 0, 0, 0];
      // default icon state
      let iconState = IconState.Empty;
      // weekdays range
      const weekdays = range(0, 7);

      weekdays.forEach(day => {

        let dayStatuses = this.scheduleTree ? this.scheduleTree.map(stEntry => {

            let shiftInfo = stEntry.shift[shIdx].shiftInfo;
            if (shiftInfo.shiftUnpublished && shiftInfo.shiftUnpublishArr) {
              unpubArray[day] += shiftInfo.shiftUnpublishArr[day];
            }

            if (shiftInfo.daysInfo[day].targetInfo &&
              shiftInfo.daysInfo[day].dayAssigns < shiftInfo.daysInfo[day].targetInfo.min_job_count) {

              return IconState.Bad;

            } else if (shiftInfo.daysInfo[day].dayAssigns === 0) {

              return IconState.Empty;

            } else if (shiftInfo.publisedShPosAss.length) {

              let stat = shiftInfo.daysInfo[day].targetStatus ? 'Bad' :
                shiftInfo.daysInfo[day].targetInfo ? 'Good' : 'Need';

              if (!shiftInfo.daysInfo[day].targetStatus  &&
                shiftInfo.daysInfo[day].targetInfo &&
                shiftInfo.daysInfo[day].targetInfo.min_job_count === 0 &&
                shiftInfo.daysInfo[day].targetInfo.max_work_time === 0 ) {
                stat = 'Need';
              }

              return IconState[stat];
            }
          }) : [3];

        let checkPositionStat = this.scheduleService.assignIconClass(
          dayStatuses,
          day,
          moment(sEl.to, 'HH:mm').unix() - moment().startOf('day').unix());
        shiftDayStatus.push(checkPositionStat);
      });


      if (this.scheduleTree &&  this.scheduleTree.length) {
        shiftUnpublish = this.scheduleTree.reduce((acc, current) => {
          return acc += current.shift[shIdx].shiftInfo.shiftUnpublished;
        }, 0);
      }

      newShift.shiftUnpublish = shiftUnpublish;
      newShift.shiftDayStatus = shiftDayStatus;
      newShift.positions = this.scheduleTree || [];
      newShift['unpubArray'] = unpubArray;
      return newShift;
    });

  }

  updateTotals(event: number[], eventSource: string) {

    if (eventSource === SHIFT) {
      this.totalWeekdayTimings = event;
      this.overallTotalTiming = reduceSum(this.totalWeekdayTimings);
      return;
    }

    this.updateRounds += ONE_RUN;

    // clear total timings on first run
    if (R.or(!this.totalWeekdayTimings.length, this.updateRounds === FIRST_RUN))
      this.totalWeekdayTimings = R.repeat(0, WEEKDAYS);

    // calculating overall timings by merging each
    // position's duration into `this.totalWeekdayTimings`
    this.totalWeekdayTimings = zipWithAdd(this.totalWeekdayTimings, event);
    this.overallTotalTiming = reduceSum(this.totalWeekdayTimings);


    if (this.scheduleTree.length === this.updateRounds) {
      this.updateRounds = 0;
      this.finishedUpdateRound = true;
    } else {
      this.finishedUpdateRound = false;
    }
  }

  tempVar() {
    if (this.currentUnit) {
      return R.pluck('active', this.currentUnit.shift).length > 1;
    }
  }

  setDate($event) {
    if ($event.action) {
      this.valueDay = moment($event.action);
      this.momentWeek = moment($event.action).isoWeekday(1);
      this.week = this.calculateWeek(this.momentWeek);
      this.cache.set('schedule:week', this.week);
      this.cache.set('dayView:dateOnDayView', $event.action.format('DD MMMM YYYY'));
      this.changeWeekValue();
      this.callActions(this.userId, this.week);
    }
    this.showCalendarVar = false;
  }

  showCalendar() {
    this.showCalendarVar = true;
  }

  copyButtonDisabled() {
    if (this.week < parseInt(moment().format('GGWW'), 10)) {
      return true;
    }
    if (this.currentUnit && this.currentUnit.positions) {
      let pos = this.currentUnit.positions;
      if (Object.keys(pos).length) {
        return false;
      }
    }
    return true;
  }

  changeWeekDayView(e) {
    if ( typeof e.action === 'number') {
      this.loadWeek(e.action);
    }else {
      this.setDate(e);
    }
  }

  handleNoteUpdate(week) {
    this.action.fetchGetNotes(week, this.currentUnit.unit_id);
  }
  closePublicModal(e?) {
    this.action.schedulePublic(false);
    // this.hideScrollBar();
  }

  checkItOnDayView(m, d) {
    this.valueDay = moment(this.momentWeek).isoWeek(m).weekday(d + 1);
    this.cache.set('dayView:dateOnDayView',
      this.valueDay.format('DD MMMM YYYY'));
    this.action.dayViewAction(true);
  }

}


export const publishModalHtml = `
  <section class="uk-modal uk-open" id="jobpublish" tabindex="-1" role="dialog" aria-labelledby="label-jobpublish" aria-hidden="false" style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog" >
      <a (click)="closeAssessModal($event)" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-jobpublish">
          <h2><a (click)="closeAssessModal($event)" class="uk-modal-close"><!--icon--></a>
            <span>{{'Publish_changes' | translate}} <span>{{publishModalData.length}}</span> </span>
          </h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form jobpublish">
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-width-medium-1-1 uk-margin-bottom publishContent">
              <ul>
                <li *ngFor="let publish of publishModalData">{{publish.dayName | translate}} {{publish.shiftDay  | translate}} {{publish.from}}-{{publish.to}}, {{publish.pname}}: {{publish.uname}}</li>
              </ul>
              
            </div>
            <div class="uk-width-1-1 uk-margin-bottom">
              <button (click)="sendPulish()" type="button" class="uk-button uk-button-large ok uk-width-1-1"> {{'Publish_changes' | translate}} <span> {{publishModalData.length}} </span> </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
    `;


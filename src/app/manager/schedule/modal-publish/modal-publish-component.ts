import { Component, Input, Output, EventEmitter }  from '@angular/core';
import { AppStoreAction }                          from '../../../store/action';
import { publishModalHtml }                        from './modal-publish.html';
import { select }                                  from 'ng2-redux/lib/index';
import { JobService }                              from '../../../services/shared/JobService';
import { MapToIterable }                           from '../../../lib/pipes/map-to-iterable';
import { SortNamePipe }                            from '../../../lib/pipes/sortName';
import { ScheduledService }                        from '../../../services/scheduled.service';
import * as moment                                 from 'moment';
import { UnitService }                             from '../../../services/unit.service';



@Component({
  // The selector is what angular internally  uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'publish-modal',
  providers: [ScheduledService],
  directives: [],
  pipes: [MapToIterable, SortNamePipe],
  styles: [`
        .publishContent{
          font: 300 1.6rem 'Fira Sans Light', 'HelveticaNeue-Light', sans-serif;
          line-height: 3rem;
        }
    `],
  template: publishModalHtml

})

export class PublishModalComponent {

  @select() info$;
  @select() publishModalData$;
  @Input() unit;
  @Input() showDay;
  @Output() closeEmitter = new EventEmitter();

  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public publishModalData: any;
  public userId: any;
  public info;

  // TypeScript public modifiers
  constructor(public action: AppStoreAction,
              public publishService: UnitService,
              public jobService: JobService,
              private scheduledService: ScheduledService) {
    this.unsubscribe.push(this.publishModalData$.subscribe(data => {
      if (data) {
        this.publishModalData = data.map(j => {
          j.dayName = moment().day(j.weekday).format('dd');
          switch (j.shift_idx) {
            case 0:
              j.shiftDay = 'Morgen';
              break;
            case 1:
              j.shiftDay = 'Mittag / Tag';
              break;
            case 2:
              j.shiftDay = 'Abend';
              break;
            default:
              j.shiftDay = 'Nacht';
          }
          return j;
        });
      }
    }));

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;
      let self = this;
      if (info.manager) {
        this.userId = info.manager.selected.unit_id;
      }
    }));
  }
  sendPulish() {
    let dayInfo = this.showDay.format('GGWWE');
    let weekInfo = this.showDay.format('GGWW');

    this.scheduledService.publishJobs(
      this.unit.unit_id,
      this.showDay.format('GGWW'),
      this.showDay.format('E'))
      .subscribe(() => {
        this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, weekInfo);
        this.action.openPublishModal(false, []);
      });
  }
  ngOnInit() {
    this.hideScrollBar();
  }

  closeAssessModal(e) {
    e.preventDefault();
    // this.action.openPublishModal(false);
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => {
      el.unsubscribe();
    });
    this.hideScrollBar();
  }


}

export const htmlAddJobber = `
<div id="modal_manager" *ngIf="opened">
<section class="uk-modal uk-open"
         id="useradd"
         tabindex="-1"
         role="dialog"
         aria-labelledby="label-useradd"
         aria-hidden="false"
         style="display: block; overflow-y: scroll;">
  <div class="uk-modal-dialog">
    <a (click)="cleanStore();hideScrollBar()"  class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
      <!--icon-->
    </a>
    <div class="uk-modal-header">
      <div id="label-useradd">
        <h2><a (click)="cleanStore();hideScrollBar()"data-close="dismiss" data-dismiss="modal" class="uk-modal-close"><!--icon--></a>
        {{'Add_jobber' | translate}}</h2>
      </div>
    </div>
    <div class="modal-content">
      <div class="uk-grid uk-grid-collapse uk-text-left">
        <form class="uk-form useradd">
          <div
            class="uk-width-small-1-1 uk-width-medium-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
            <span>{{'Position' | translate}}</span>
            <div class="pname"></div>
          </div>
          <div class="uk-width-1-1 uk-container-center uk-grid-collapse uk-margin-bottom">
          
            <ul *ngIf="currentPositionStaff.length" class="uk-nav uk-nav-parent-icon uk-nav-side">
              <li [ngClass]="{'uk-open': showPositionList}" class="uk-parent jobber">
                <a (click)="showPositionList = !showPositionList;
                showUnitList = false; showCompanyList = false; showList = []">
                {{'from_this_position' | translate}}</a>
                   <ul *ngIf="showPositionList" class="uk-nav-sub">
                    <li *ngFor="let positionSatff of currentPositionStaff" class="jobber">
                      <a (click)="addExistingStaff(positionSatff.webuser_id); cleanStore(); hideScrollBar()">
                        <span class="jc-uname">{{positionSatff.uname}}</span>
                       <span class="icon plus circle inverse"></span>
                      </a>
                    </li>
                  </ul>
              </li>
            </ul>
            <ul  *ngFor="let unitCategory of separateCategory | mapToIterable; let i = index" class="uk-nav uk-nav-parent-icon uk-nav-side">
               <li *ngIf="unitCategory.key === unitStore.nname && unitCategory.value.length" [ngClass]="{'uk-open': showUnitList}" class="uk-parent jobber">
                <a (click)="showUnitList = !showUnitList; showPositionList = false; showCompanyList = false; showList = [];" class="">
                  {{'from_this_unit' | translate}}
                </a>
                  <ul *ngIf="showUnitList" class="uk-nav-sub">
                    <li *ngFor=" let staff of unitCategory.value" class="jobber">
                      <a (click)="addExistingStaff(staff.webuser_id); cleanStore(); hideScrollBar()">
                        <span class="jc-uname">{{staff.uname}}</span>
                        <span class="icon plus circle inverse"></span>
                      </a>
                    </li>
                  </ul>
              </li>
            </ul>
            <ul *ngFor="let unitCategory of separateCategory | mapToIterable; let i = index" class="uk-nav uk-nav-parent-icon uk-nav-side">
              <li *ngIf="unitCategory.key === unitStore.cname && unitCategory.value.length"
                  [ngClass]="{'uk-open': showCompanyList}" class="uk-parent jobber">
                <a (click)="showCompanyList = !showCompanyList; showPositionList = false; showUnitList = false; showList = []">
                 {{'job_hopper_for' | translate}} {{unitStore.cname}}
                </a>
                <ul *ngIf="showCompanyList" class="uk-nav-sub">
                  <li *ngFor=" let staff of unitCategory.value" class="jobber">
                    <a (click)="addExistingStaff(staff.webuser_id); cleanStore(); hideScrollBar()">
                      {{staff.uname}}
                      <span class="icon plus circle inverse"></span>
                    </a>
                  </li>
                </ul>
              </li>             
              <li *ngIf="unitCategory.key !== unitStore.nname && unitCategory.key !== unitStore.cname && unitCategory.value.length && unitCategory.key !== 'sys'"
                  [ngClass]="{'uk-open': showList[i]}" class="uk-parent jobber">
                <a (click)="handleClick(i); showCompanyList = false; showPositionList = false; showUnitList = false;">
                {{'job_hopper_for' | translate}} {{unitCategory.key}}</a>
                 <ul *ngIf="showList[i]" class="uk-nav-sub">
                  <li *ngFor="let positionSatff of unitCategory.value" class="jobber">
                    <a (click)="addExistingStaff(positionSatff.webuser_id); cleanStore(); hideScrollBar()">
                      {{positionSatff.uname}}
                      <span class="icon plus circle inverse"></span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </form>

        <div class="uk-width-1-1 uk-margin-large-top">
          <a (click)="showCreateJobber();" class="uk-margin-left table-link">
          {{'plussign_create_jobber' | translate}}
          </a>
        </div>
        <!--uk-width-1-1 uk-margin-large-bottom-->
      </div>
      <!--uk-grid -->
    </div>
  </div>
  <!--usreradd-->
</section>
</div>
`;

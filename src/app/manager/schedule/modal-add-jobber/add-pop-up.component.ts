import { Component, Output, EventEmitter } from '@angular/core';
import { htmlAddJobber } from './add-pop-up.html.ts';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { EmitService } from '../../../services/emit.service';
import { select } from 'ng2-redux/lib/index';
import { AppStoreAction } from '../../../store/action';
import { JobService } from '../../../services/shared/JobService';
import { MapToIterable } from '../../../lib/pipes/map-to-iterable';
import { SvgTemplateDirective } from '../../../lib/directives/svg-icon.directive';
import { PositionService } from '../../../services/position.service';
import * as _ from 'lodash';
@Component({
selector: 'add-jobber-modal',
providers: [],
directives: [SvgTemplateDirective],
pipes: [TranslatePipe, MapToIterable],
styles: [],
template: htmlAddJobber
})
export class AddJobberModalController {
  // TODO: decide on passing ngModel data or ngControl data:
  @Output() closeEmitter = new EventEmitter();

  @select(state => state.showAddModal) modal$: any;
  @select() currentUnit$: any;
  @select() info$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  public hideScrollBar = this.jobService.hideScrollBar;
  public info;
  public week;
  private opened;
  private selectedPosition;
  private unUseddStaff;
  private unitStore;
  private currentPositionStaff;
  private separateCategory;
  private showList = [];
  private showPositionList = false;

  constructor(public emit: EmitService,
              public action: AppStoreAction,
              public jobService: JobService,
              public positionService: PositionService) {
    this.info$.subscribe(info => {
      this.info = info;
    });
    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    });
    this.modal$.subscribe(val => {
      this.opened = val;
      this.filterStaff(val);
    });
  }

  filterStaff(val) {
    this.currentUnit$.subscribe(unit => {
      this.unitStore = unit;
      if (unit.cname) {
         this.selectedPosition = unit.positions[val.positionId];
         if (this.selectedPosition && this.selectedPosition.staff) {
           let arrOfUsedStaff = this.selectedPosition.staff[val.shiftIdx];
           this.unUseddStaff = Object.assign({}, unit.staff);
           if (arrOfUsedStaff) {
             for (let i = 0; i < arrOfUsedStaff.length; i++) {
               delete this.unUseddStaff[arrOfUsedStaff[i]];
             }
           }
         } else {
           this.unUseddStaff = Object.assign({}, unit.staff);
         }
      }
      this.currentPositionStaff = [];
      if (this.selectedPosition && this.selectedPosition.home_staff) {
        this.selectedPosition.home_staff.forEach(data => {
          if (this.unUseddStaff[data]) {
            this.currentPositionStaff.push(this.unUseddStaff[data]);
          }
        });
      }

      let arrOfCategoryName = [];
      if (_.values(this.unitStore.staff).length) {
        _.values(this.unitStore.staff).forEach((data: Itest) => {
          arrOfCategoryName.push(data.jumper_nname);
        });
        arrOfCategoryName = _.uniq(arrOfCategoryName);
      }

      this.separateCategory = {};
      this.showList = [];
      if (arrOfCategoryName.length) {
        arrOfCategoryName.forEach(el => {
          if (el) {
            this.separateCategory[el] = _.values(this.unUseddStaff).filter((data: Itest) => {
              return data.jumper_nname === el;
            });
            this.showList.push(false);
          }
        });
      }
    });
  }

  showCreateJobber() {
    this.action.showCreateJobberBool(true);
   }

  cleanStore() {
    this.action.showAddModal(false);
  }


  addExistingStaff(staffId) {
    this.selectedPosition.staff = Array.isArray(this.selectedPosition.staff) ?
      this.selectedPosition.staff : [undefined, undefined, undefined, undefined];
    let shiftStaff = this.selectedPosition.staff[this.opened.shiftIdx];
    shiftStaff = Array.isArray(shiftStaff) ? shiftStaff : [];
    shiftStaff.push(staffId);
    this.selectedPosition.staff[this.opened.shiftIdx] = shiftStaff;
    /*console.log(this.selectedPosition.pname, this.selectedPosition.position_id,
      this.selectedPosition.unit_id, this.selectedPosition.staff);*/
    this.positionService
      .putPoition(this.selectedPosition.pname, this.selectedPosition.position_id,
        this.selectedPosition.unit_id, this.selectedPosition.staff)
      .subscribe(res => {
        this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      });
  }

  handleClick(index) {
    this.showList = [];
    this.showList[index] = !this.showList[index];
  }
}

interface Itest {
  jumper_nname: any;
}

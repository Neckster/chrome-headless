import {
  Component,
  Input,
  Output,
  EventEmitter }                from '@angular/core';
import { FormControl }          from '@angular/forms';

import { NotesService }         from '../../../services/notes.service';

import { noteTemplate }         from './note.template';



const NEW: string = 'NEW';

const leftPad = (v) => v > 9 ? '' + v : '0' + v;

@Component({
  selector: 'note',
  styles: [ require('!raw!less!./styles.less') ],
  template: noteTemplate
})
export class Note {

  @Input() unit;
  @Input() idx;
  @Input() week;
  @Input() year;
  @Input() isDayCurrent;
  @Input() noteBody;
  @Input() hovering;

  @Output() noteUpdate = new EventEmitter();

  private unitId: number;
  private noteControl: FormControl = new FormControl();
  private noteState: 'NEW' | 'SUBMITTED' | 'RETRIEVED' | 'DELETED';
  private isHovering: boolean = false;
  private isEditing: boolean = false;

  constructor(private noteService: NotesService) {

    this.noteState = 'NEW';



    this.noteControl.valueChanges
      .debounceTime(2000)
      .subscribe((v: string) => {
        // if (!v.trim().length);
        this.noteService.saveNote({
          week: `${this.year.substring(2)}${leftPad(this.week)}`,
          day: `${this.idx}` as any,
          unit_id: this.unitId,
          body: v.trim()
        })
        .map(resp => resp.json())
        .subscribe((res: any) => {
          this.noteBody = res.body.trim();
          this.noteState = 'RETRIEVED';

          if (!this.noteBody.length) {
            this.noteState = 'NEW';
          } else {
            this.noteUpdate.emit(`${this.year.substring(2)}${leftPad(this.week)}`);
          }
        }, (err) => {
          console.error(err);
        });
      });
  }

  ngOnChanges(change) {

    if (this.unit) {
      this.unitId = this.unit.unit_id;
    }

    if (change.noteBody) {
      this.noteState = this.noteBody && this.noteBody.trim().length ? 'RETRIEVED' : 'NEW';
      this.noteControl.patchValue(this.noteBody, { emitEvent: false });
    }
    if (change.hovering && change.hovering.currentValue === true) {
      this.noteHoverIn(undefined);
    }

    if (change.hovering && change.hovering.currentValue === false) {
      this.noteHoverOut(undefined);
    }

  }

  private editStart(event?) {
    this.isEditing = true;
    this.noteHoverIn(event);
  }

  private editEnd(event) {
    this.isEditing = false;
    this.noteHoverOut(event);
  }

  private noteHoverIn(event?) {
    this.isHovering = true;
  }

  private noteHoverOut(event) {
    this.isHovering = false;
  }
  private changeFocus(el) {
    el.focus();
  }

}

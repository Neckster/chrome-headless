export const noteTemplate: string = `

 <div class="mNote" >
  <i [ngClass]="{ 'plus-sign': noteState === 'NEW'  && !isEditing }"
    *ngIf="noteState === 'NEW' && !isEditing "
    (click)="changeFocus(focusable)">+</i> 
  
  <input 
    #focusable
    type="text" 
    class="note-input" 
    (focus)="editStart($event)"
    (focusout)="editEnd($event)"
    [ngClass]="{ 'note-input--current':  isDayCurrent, 'note--main': noteState === 'RETRIEVED' }" 
    [formControl]="noteControl" />
  </div>
`;

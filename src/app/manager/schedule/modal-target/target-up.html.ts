export const htmlTarget = `
<div id="modal_manager" *ngIf="totalJobTarget">
  <section class="uk-modal uk-open" id="shiftedit" tabindex="-1" role="dialog"
           aria-labelledby="label-shiftedit" aria-hidden="false"
           style="display: block; overflow-y: scroll;">
    <div class="uk-modal-dialog">
      <a (click)="cleanStore();hideScrollBar()" class="uk-modal-close uk-close"
         title ='{{ "dismiss" | translate }}' data-close="dismiss"  data-dismiss="modal">
        <!--icon-->
      </a>
      <div class="uk-modal-header">
        <div id="label-shiftedit">
          <!-- FIXME: move this info out of header -->
          <h2><a (click)="cleanStore();hideScrollBar()" class="uk-modal-close"><!--icon--></a><span>{{'Need_for' | translate}}</span>
            <span id="shiftday">{{weekDay[totalJobTarget.weekday-1] | translate}}</span>
            <span id="shiftshift">{{selectedShift | translate}}</span>
            <span id="shiftslot">{{currentPosition}}</span>
          </h2>
        </div>
      </div>
      <div class="modal-content">
        <form class="uk-form uk-form-horizontal shiftedit" ngNoForm>
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">   
                <!--TODO: template id/for handling-->
                <label class="uk-form-label">{{'Target_Jobbers' | translate}}</label>
                <input [(ngModel)]="minJobCount"
                       class="uk-width-small-1-1 uk-width-medium-6-10" type="text" maxlength="2">
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <!--TODO: template id/for handling-->
                <label class="uk-form-label">{{'Maximum_hours' | translate}}</label>
                <input [(ngModel)]="maxWorkTime"
                       class="uk-width-small-1-1 uk-width-medium-6-10" type="text" maxlength="3">
              </div>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top">
              <button (click)="updateJobInfo(); cleanStore();" type="button" class="uk-button uk-button-large ok uk-width-1-1">{{'button_OK' | translate}}</button>
            </div>
          </div>
          <!--uk-grid--></form>


        <!--.shiftedit-->
      </div>
    </div>
  </section>
</div>
`;

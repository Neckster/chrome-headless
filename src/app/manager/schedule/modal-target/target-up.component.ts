import { Component, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { htmlTarget  } from './target-up.html.ts';
import { TargetJobService } from '../../../services/job.service';
import { JobService } from '../../../services/shared/JobService';
import {  select } from 'ng2-redux/lib/index';
import { AppStoreAction } from '../../../store/action';


@Component({
  selector: 'target-modal',
  template: htmlTarget
})
export class TargetModalController {

  // TODO: decide on passing ngModel data or ngControl data:
  @select() info$: any;
  @select() currentUnit$: any;
  @select(state => state.saveScheduleWeek) week$: any;
  @select(state => state.showModal) modal$: any;
  public info;
  public hideScrollBar = this.jobService.hideScrollBar;
  private totalJobTarget;
  private maxWorkTime;
  private minJobCount;
  private week;
  private shiftParse = ['shift_0', 'shift_1', 'shift_2', 'shift_3'];
  private weekDay = ['date_day_1_short_Monday',
    'date_day_2_short_Tuesday', 'date_day_3_short_Wednesday',
    'date_day_4_short_Thursday', 'date_day_5_short_Friday',
    'date_day_6_short_Saturday', 'date_day_7_short_Sunday'];
  private selectedShift;
  private currentPosition;


  constructor(public targetService: TargetJobService,
              public action: AppStoreAction,
              public jobService: JobService) {
    this.info$.subscribe(info => {
      this.info = info;
    });
    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    });

    this.modal$.subscribe(val => {
      this.totalJobTarget = val;
      if (this.totalJobTarget) {
        this.currentUnit$.subscribe(unit => {
          let posId = val.position_id || val.positionId;
          if (posId && unit.positions[posId]) {
            this.currentPosition = unit.positions[posId].pname;
          } else {
            console.error(`Can't find position on target unit: ${posId}`,
              unit.positions, unit, val);
          }
        });
        this.maxWorkTime = this.totalJobTarget.max_work_time;
        this.minJobCount = this.totalJobTarget.min_job_count;
        let shiftInd = this.totalJobTarget.shift_idx === undefined ?
          this.totalJobTarget.shiftIndex : this.totalJobTarget.shift_idx;
        this.selectedShift = this.shiftParse[shiftInd];
      } else {
        this.maxWorkTime = '';
        this.minJobCount = '';
      }
    });
  }

  cleanStore() {
    this.action.showModal('');
  }

  updateJobInfo() {

    /*if (+this.maxWorkTime === 0 && +this.minJobCount === 0) {

      this.cleanStore();
      this.hideScrollBar();

    } else {*/

      this.targetService.put(
        this.totalJobTarget.position_id || this.totalJobTarget.positionId,
        this.totalJobTarget.shift_idx === undefined ?
          this.totalJobTarget.shiftIndex : this.totalJobTarget.shift_idx,
        this.totalJobTarget.weekday, this.week, 0,
        +this.maxWorkTime,
        +this.minJobCount).subscribe(() => {
          this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
        });
      this.hideScrollBar();

    // }
  }
}

import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ComponentFactory,
  ComponentResolver }                     from '@angular/core';

import {
  FORM_DIRECTIVES,
  REACTIVE_FORM_DIRECTIVES,
  FormControl,
  FormBuilder,
  FormGroup, Validators }             from '@angular/forms';


import { htmlJobberForm }             from './jobber-form.html.ts';
import { UserService }                from '../../../services/user.service';
import { JubbrFormValidator }         from '../../../lib/validators/jubbr-validator';
import { AppStoreAction }             from '../../../store/action';
import { select }                     from 'ng2-redux/lib/index';
import { UserInvite }                 from '../../../lib/interfaces/user/invite';
import { ParseOrganisationService }   from '../../../services/shared/parseOrganisation.service';
import { PositionService }            from '../../../services/position.service';
import { JobService }                 from '../../../services/shared/JobService';
import { RecurrentSelect }            from '../../../lib/directives/recurrentSelect';
import { Cache }                      from '../../../services/shared/cache.service';


interface IJobber {
  firstname?: string;
  lastname?: string;
  email?: string;
  position?: number;
  shift?: any;
  hopper?: any;
  manager?: any;
  inviteMail?: string;
}

@Component({
  selector: 'create-jobber',
  providers: [],
  directives: [FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, RecurrentSelect],
  template: htmlJobberForm,
  styles: [`
          .modalBody {
            display: block;
            overflow-y: scroll;
          }
          .hideOutline:hover,
          .hideOutline:active,
          .hideOutline:focus{
            outline: none!important;
          }
          .tooltipInfoBlock{
            position:relative;
            top: 0;
          }
          .jobHopperInfo, .mailSendInfo{
            display: none;
            position: absolute;
            bottom: -15px;
            background: rgba(7, 160, 225, 1);
            color: white;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            padding: 3px 6px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.35);
          }
          
          .mailSendInfo{
            bottom: -50px;
          }
          
          div.jobHopperInfoTr{
            height: 0;
            border-left: 10px solid transparent; 
            border-right: 10px solid transparent; 
            border-bottom: 10px solid rgba(7, 160, 225, 1); 
            font-size: 0;
            line-height: 0;
            text-align: center;
            width: 0;
            position: absolute;
            top: -7px;
            left: 225px;
          }
          
      `]
})
export class CreateJobberController {

  @Input() opened;
  @Input() isResetting = false;
  // @Input() organisationStaff;
  @Output() closeEmitter = new EventEmitter();
  @Input() organisationStaff;

  @Input() thirdStep;
  @Input() submitForm;
  @Output() correctSubmit= new EventEmitter(true);

  @select() info$: any;
  @select(state => state.showAddModal) modal$: any;
  @select() currentUnit$: any;
  @select(state => state.saveScheduleWeek) week$: any;

  public info;
  public week;
  public hideScrollBar = this.jobService.hideScrollBar;

  private openedScedule;
  private unitStore;
  private homePositionId;
  // private organisationStaff;
  private jobber: IJobber = {
    firstname: undefined,
    lastname: undefined,
    email: undefined,
    position: undefined,
    shift: undefined,
    hopper: undefined,
    manager: undefined,
    inviteMail: undefined
  };
  private selectedPosition;
  private inviteForm: FormGroup;
  private unitOrganisation: any;
  private groupOrganisation: any;
  private alreadyAdd: any;

  constructor(public user: UserService,
              private fb: FormBuilder,
              public cache: Cache,
              public action: AppStoreAction,
              public jobService: JobService,
              public positionService: PositionService) {

    this.week$.subscribe(storeWeek => {
      this.week = storeWeek;
    });

    this.currentUnit$.subscribe(unit => {
      this.unitStore = unit;
    });

    this.info$.subscribe(info => {
      this.info = info;
       if (this.info.manager) {
        this.parseSettingsStaff();
      }
    });

    this.modal$.subscribe(val => {
      /* tslint:disable:no-string-literal */
      this.openedScedule = this.thirdStep ? true : val;
      if (this.openedScedule) {
        this.inviteForm.controls[`shift${this.openedScedule.shiftIdx}`].patchValue(true);
        this.homePositionId = 'p' + this.openedScedule.positionId;
        this.inviteForm.controls['home_position_id'].patchValue(this.homePositionId);

      }
    });
    this.groupOrganisation = {};
    /* tslint:enable:no-string-literal */
  }

  ngOnInit() {
    if (!this.inviteForm) {
      this._buildForm();
    }
    /*if (this.cache.get('alreadyAdd')) {
      this.alreadyAdd = true;
    }*/
  }

  ngOnChanges(changes: {[ propName: string]: any}) {
    /* tslint:disable:no-string-literal */
    if (changes['opened'] &&
       !changes['opened'].currentValue &&
        changes['opened'].previousValue) {
      this.onReset();
    }
    if (this.submitForm) {
      this.correctSubmit.emit({
        action: true
      });
    }
    /* tslint:enable:no-string-literal */
  }



  private onReset() {
    this._buildForm();
    this.isResetting = true;
    this.jobber = <IJobber>{};
    this.homePositionId = undefined;
    setTimeout(() => this.isResetting = false, 0);
    return false;
  }

  private _buildForm() {

    this.inviteForm = this.fb.group({
      firstname: ['', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])],
      lastname: ['', Validators.compose([
        Validators.required, Validators.minLength(2)
      ])],
      email: ['', Validators.compose([
        Validators.required, Validators.minLength(5),
        JubbrFormValidator.mail as any
      ])],
      home_position_id: ['none'],
      jumper_unit_id: [''],
      manager_unit_id: [''],
      sendmail: [false],
      mailmessage: ['', Validators.compose([
        Validators.minLength(4)
      ])],
      shift0: [false],
      shift1: [false],
      shift2: [false],
      shift3: [false]
    }, {validator: JubbrFormValidator
      .atLeastOne('home_position_id', 'jumper_unit_id', 'manager_unit_id')});
  }

  private formSubmit(e) {
    let stripFunction = this._compose(this._stripChar);
    let inviteFormPayload = <UserInvite>{};

    Object.keys(this.inviteForm.value).forEach(key => {
      if (this.inviteForm.value[key] !== '') {
        if (/shift/.test(key)) {
          inviteFormPayload['shift-' + key.slice(-1)] = this.inviteForm.value[key];
        } else {
          inviteFormPayload[key] = this.inviteForm.value[key];
        }
      }
    });

    inviteFormPayload = this._mapIterator(
      inviteFormPayload,
      [
        {
          fields: [
            'home_position_id',
            'jumper_unit_id',
            'manager_unit_id'
          ],
          transform: stripFunction
        },
        {
          fields: [
            'email'
          ],
          transform: this._toLower
        }
      ]
    );
    this.user.invite(inviteFormPayload).subscribe((res) => {
      this.action.jobberAlert(res);
      this.action.fetchGetInfo();
      this.action.fetchCurrentUnit(this.info.manager.selected.unit_id, this.week);
      if (!this.thirdStep) {
        this.hideScrollBar();
        this.closeEmitter.emit(undefined);
      }else {
        /*this.correctSubmit.emit({
          action: true
        });*/
       /* this.cache.set('alreadyAdd', true);
        this.alreadyAdd = true;*/
      }
      this.cleanStore();
      this.action.addJobberOpen(false);
    }, (err) => { });
  }

  private _mapIterator(obj: any, fields: any[]): any {
    let result = {};
    let propsCache = [];
    fields.forEach((field) => {
      for (let p in obj) {
        if (obj.hasOwnProperty(p) && obj[p] && field.fields.indexOf(p) !== -1) {
          /* tslint:disable:no-string-literal */
          result[p] = field['transform'](obj[p]);
          propsCache.push(p);
          /* tslint:enable:no-string-literal */
        } else if (propsCache.indexOf(p) === -1) {
          result[p] = obj[p];
        }
      }
    });

    return result;
  }

  private _stripChar(value: string): string {
    return value.slice(1);
  }

  private _toNumber(value: any): number {
    return parseInt(value, 10);
  }

  private _toLower(value: string): string {
    return value.toLowerCase();
  }

  private _compose(...funcs) {
    return funcs.reduce(function(f, g) {
      return function() {
        return f(g.apply(this, arguments));
      };
    });
  }

  private parseSettingsStaff() {
    if (!ParseOrganisationService.treeData) return;

    let treeData = ParseOrganisationService.treeData[0];
    ParseOrganisationService.treeStruct(this.info.manager.organisation);
    this.unitOrganisation = ParseOrganisationService
        .unitPosition(treeData.type === 'company' ? treeData.child : [treeData]);
    this.groupOrganisation = ParseOrganisationService.treeData;
  }

  private cleanStore() {
    this.action.showCreateJobberBool(false);
    this.action.showAddModal(false);
  }

  private sendInvite(e) {
    if (this.openedScedule)
      this.formSubmit(e);
  }

  private closeModal(e) {
    e.preventDefault();

    if (!this.thirdStep) {
      this.hideScrollBar();
      this.closeEmitter.emit();
    }else {
      Object.keys(this.inviteForm.value).forEach(el => {
        if (el === 'home_position_id') {
          this.inviteForm.patchValue({[el]: 'none'});
        }else {
          this.inviteForm.patchValue({[el]: undefined});
        }

      });
      /*this.correctSubmit.emit({
        action: true
      });*/
    }

    this.cleanStore();
    this.action.addJobberOpen(false);
  }

  private showInfoToolTip(el) {
    el.style.display = 'block';

  }
  private hideInfoToolTip(el) {
    el.style.display = 'none';
  }

}

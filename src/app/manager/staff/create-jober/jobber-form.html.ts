export const htmlJobberForm = `
<div  id="modal_manager" *ngIf="opened">

<section [ngClass]="{'uk-modal uk-open modalBody': !thirdStep}" class='hideOutline' id="userinvite" tabindex="-1" role="dialog" aria-labelledby="label-userinvite" aria-hidden="false">
  <div [ngClass]="{'uk-modal-dialog': !thirdStep}">
    <a *ngIf='!thirdStep' class="uk-modal-close uk-close" (click)="closeModal($event)"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
      <!--icon-->
    </a>
    <div *ngIf='!thirdStep' class="uk-modal-header">
      <div id="label-userinvite">
        <h2><a data-close="dismiss" data-dismiss="modal" 
                (click)="closeModal($event)" 
                class="uk-modal-close"><!--icon--></a>
          <span>{{ 'Invite_or_add_jobber' | translate }}</span>
        </h2>
      </div>
    </div>
    <div class="modal-content"> 
      <form class="uk-form uk-form-horizontal userinvite jc-userinviteform jc-template-replace-userinviteform" id="schedule-userinviteform"  [formGroup]="inviteForm" novalidate (submit)="formSubmit($event)" *ngIf="!isResetting">
        <div class="uk-grid uk-grid-collapse">
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label required" for="userinvitefirstname">{{ 'placeholder_Firstname' | translate }}</label>
              <input [ngClass]="{'uk-form-danger': inviteForm.controls.firstname.dirty && !inviteForm.controls.firstname.valid }" class="uk-width-small-1-1 uk-width-medium-6-10" type="text" id="userinvitefirstname"
              placeholder="{{'placeholder_Firstname' | translate}}" name="firstname" formControlName="firstname">
              <div *ngIf="inviteForm.controls.firstname.dirty && !inviteForm.controls.firstname.valid && inviteForm.controls.firstname.errors?.required" class="uk-alert uk-alert-warning" id="errorbox-1">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}</p>
                </div>
              </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label required" for="userinvitelastname">{{ 'Lastname' | translate }}</label>
              <input [ngClass]="{'uk-form-danger': inviteForm.controls.lastname.dirty && !inviteForm.controls.lastname.valid }" class="uk-width-small-1-1 uk-width-medium-6-10" type="text" id="userinvitelastname"
              placeholder="{{'placeholder_Lastname' | translate}}" name="lastname" formControlName="lastname">
              <div *ngIf="inviteForm.controls.lastname.dirty && !inviteForm.controls.lastname.valid && inviteForm.controls.lastname.errors?.required" class="uk-alert uk-alert-warning" id="errorbox-2">
                <p class="ws-errormessage">{{'Please_fill_out_this_field' | translate}}</p>
              </div>
              </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label required" for="userinviteemail">{{'Email' | translate}}</label>
              <input [ngClass]="{'uk-form-danger': inviteForm.controls.email.dirty && !inviteForm.controls.email.valid }" class="uk-width-small-1-1 uk-width-medium-6-10" id="userinviteemail" type="email"
              placeholder="{{'placeholder_Email' | translate}}" required="" name="email" formControlName="email">
              <div *ngIf="inviteForm.controls.email.dirty && !inviteForm.controls.email.valid && inviteForm.controls.email.errors?.required" class="uk-alert uk-alert-warning"  id="errorbox-3">
              <p class="ws-errormessage">Please enter a part following '@'.</p>
              </div>
              </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="userinvitehome_position_id">{{ 'Position' | translate }}</label>
              <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <select formControlName="home_position_id"
                        class="select"
                        [disabled]="!!openedScedule">
                  <option selected value="none">{{'None_female'|translate}}</option>
                  <optgroup *ngFor="let unit of unitOrganisation " label="{{unit.text}}">
                    <option *ngFor="let position of unit.child" [ngValue]="position.id">{{position.text}}</option>
                  </optgroup>
                </select>
              </div>
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label">{{ 'Shift' | translate }}</label>
              <div *ngIf='unitStore && unitStore.shift && unitStore.shift.length' class="uk-width-small-1-1 uk-width-medium-6-10 uk-grid">
                <!--uk-width-4-10-->
                <div *ngIf="unitStore &&(thirdStep || unitStore.shift[0].active)" class="uk-width-1-1 uk-container-center uk-margin-small-bottom shift-0">
                  <label>
                    <input type="checkbox" name="shift-0" class="" formControlName="shift0">
                    <span>
                      <span class="jc-from">{{unitStore.shift[0].from}}</span>–<span class="jc-to">{{unitStore.shift[0].to}}</span> (<span class="jc-name">{{ 'shift_0' | translate }}</span>)
                    </span>
                  </label>
                </div>
                <!--uk-width-4-10 -->
                <div *ngIf="unitStore && (thirdStep || unitStore.shift[1].active)" class="uk-width-1-1 uk-container-center uk-margin-small-bottom shift-1">
                  <label>
                    <input type="checkbox" name="shift-1" class="" formControlName="shift1">
                    <span>
                      <span class="jc-from">{{unitStore.shift[1].from}}</span>–<span class="jc-to">{{unitStore.shift[1].to}}</span> (<span class="jc-name">{{ 'shift_1' | translate }}</span>)
                    </span>
                  </label>
                </div>
                <div *ngIf="unitStore && (thirdStep || unitStore.shift[2].active)" class="uk-width-1-1 uk-container-center uk-margin-small-bottom shift-2">
                  <label>
                    <input type="checkbox" name="shift-2" class="" formControlName="shift2">
                    <span>
                      <span class="jc-from">{{unitStore.shift[2].from}}</span>–<span class="jc-to">{{unitStore.shift[2].to}}</span> (<span class="jc-name">{{ 'shift_2' | translate }}</span>)
                    </span>
                  </label>
                </div>
                <div *ngIf="unitStore && (thirdStep || unitStore.shift[3].active)" class="uk-width-1-1 uk-container-center uk-margin-small-bottom shift-3">
                  <label>
                    <input type="checkbox" name="shift-3" class="" formControlName="shift3">
                    <span>
                      <span class="jc-from">{{unitStore.shift[3].from}}</span>–<span class="jc-to">{{unitStore.shift[3].to}}</span> (<span class="jc-name">{{ 'shift_3' | translate }}</span>)
                    </span>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse tooltipInfoBlock">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 " for="userinvitejumper_unit_id" >{{ 'Job_hopper' | translate }} 
                <i class="informationButton uk-float-right uk-margin-right"
                   (mouseenter) ="showInfoToolTip(jobHopperInfo)"
                   (mouseleave) ="hideInfoToolTip(jobHopperInfo)"
                   [svgTemplate]="'informationButton'"></i>
              </label>
              <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <recurrent-select formControlName="jumper_unit_id">
                </recurrent-select>
              </div>
              <div class="uk-width-small-1-1 uk-width-medium-6-10 uk-push-4-10 uk-margin-top">
                <div class="uk-alert intro-hint hidden">
                  <span class="uk-icon uk-icon-info uk-icon-justify hidden"><!--icon--></span>{{ 'onboarding_Job_hopper' | translate }}</div>
              </div>
              
              <div class="jobHopperInfo" #jobHopperInfo>{{'onboarding_Job_hopper'|translate}}
                <div class="jobHopperInfoTr"></div>
              </div>
              
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="userinvitemanager_unit_id">{{ 'Manager' | translate }}</label>
              <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                <recurrent-select formControlName="manager_unit_id">
                </recurrent-select>
              </div>
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse tooltipInfoBlock">
              <label for="userinvitesendmail" class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10 ">
                <input class="uk-float-left" type="checkbox" id="userinvitesendmail" name="sendmail" formControlName="sendmail"><span  class="uk-float-left" >{{ 'Send_email' | translate }}</span>
                <i class="informationButton uk-float-right uk-margin-right"
                  (mouseenter) ="showInfoToolTip(senMailInfo)"
                  (mouseleave) ="hideInfoToolTip(senMailInfo)"
                 [svgTemplate]="'informationButton'"></i>
              </label>
              <div class="uk-alert intro-hint uk-width-1-1 hidden">{{'onboarding_Jobber_invitation' | translate }}</div>
              <div class="mailSendInfo " #senMailInfo>{{'onboarding_Jobber_invitation'|translate}}
                <div class="jobHopperInfoTr"></div>
              </div>
            </div>
          </div>
          <div class="uk-form-row uk-width-1-1">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label" for="userinvitemailmessage">{{ 'Message_to_new_jobber' | translate }}</label>
              <textarea class="uk-width-small-1-1 uk-width-medium-6-10" placeholder="{{ 'placeholder_Add_Message' | translate }}" id="userinvitemailmessage" maxlength="128" name="mailmessage" formControlName="mailmessage"></textarea>
            </div>
          </div>
          <div class="uk-alert uk-alert-success error invite-mail-new hidden" data-uk-alert="">New jobber <span class="jc-uname"><!--Jobber Name--></span> was invited to register him/herself.</div>
          <div class="uk-alert uk-alert-success error invite-mail-internal hidden" data-uk-alert="">Jobber <span class="jc-uname"><!--Jobber Name--></span> was invited to connect.</div>
          <div class="uk-alert uk-alert-success error invite-new hidden" data-uk-alert="">{{'msg_info_invite_new'|translate }}<span class="jc-uname"><!--Jobber Name--></span> {{'was_added'|translate}}.</div><div class="uk-alert uk-alert-success error invite-new hidden" data-uk-alert="">New jobber <span class="jc-uname"><!--Jobber Name--></span> was added.</div>
          <div class="uk-alert uk-alert-success error invite-internal hidden" data-uk-alert="">Jobber <span class="jc-uname"><!--Jobber Name--></span> {{'was_added'|translate}}.</div><div class="uk-alert uk-alert-success error invite-internal hidden" data-uk-alert="">Jobber <span class="jc-uname"><!--Jobber Name--></span> was added.</div>
          <div class="uk-width-1-1 uk-margin-top">
            <div  class="uk-grid uk-grid-small" >
              <div class="uk-width-1-2 uk-margin-bottom">
                <button type="submit" [disabled]="!inviteForm.valid" (click)="sendInvite($event); " class="uk-button uk-button-large uk-width-1-1 ok">{{ 'add' | translate }}</button>
              </div>
              <div class="uk-width-1-2 uk-margin-bottom">
                <button type="button" class="uk-button uk-button-large uk-modal-close uk-width-1-1 cancel" (click)="closeModal($event)">{{ 'button_Cancel' | translate }}</button>
              </div>
            </div>
          </div>
        </div></form>
    </div>
  </div>
</section>
</div>
`;

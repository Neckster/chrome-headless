import {
  Component,
  Input,
  DynamicComponentLoader,
  ElementRef }                    from '@angular/core';
import { FormGroup, FormControl,
      Validators }                from '@angular/forms';
import { JubbrFormValidator }     from '../../../lib/validators/jubbr-validator';
import { TranslatePipe }          from 'ng2-translate/ng2-translate';
import { WebuserService }         from '../../../services/webuser.service';
import { MapToIterable }          from '../../../lib/pipes/map-to-iterable';
import { UserService }            from '../../../services/user.service';
import { StaffService }           from '../../../services/staff.service';
import { select }                 from 'ng2-redux/lib/index';
import { htmlWebuser }            from './webuser.htm.ts';
import { AppStoreAction }         from '../../../store/action';
import { Cache }                  from '../../../services/shared/cache.service';
import { AddVacationComponent }   from '../../availability/addVacation/addVacationForm.component';
import * as moment                from 'moment';
import {
  filter,
  map,
  isEmpty as rempty,
  pipe,
  prop,
  range,
  pickBy, not }                   from 'ramda';
import { DayOffComponent }        from './dayoff/dayoff.component';
import { extend }                 from '../../schedule/days-view/lib/utils';
import { LimitPipe }              from '../../../lib/pipes/limit';
import { DayOff }                 from '../../../models/DayOff';




const ZERO_PLACEHOLDER = '0';
const DAY_OFF_LIMIT: number = 3;

@Component({
  selector: 'web-user',
  pipes: [TranslatePipe, MapToIterable, LimitPipe],
  directives: [DayOffComponent, AddVacationComponent],
  template: htmlWebuser,
  styles: [require('./style.webuser.less')]
})
export class WebuserComponent {

  @select(state => state.localisation) lang$: any;
  @select(state => state.vacationModal) vacationModal$: any;
  @Input() userData;
  @Input() organisationStaff;
  @Input() dayoffs;

  private currentTab: string = '';
  private isOn: boolean;
  private unitOrganisation: any;
  private groupOrganisation: any;
  private homePositionId: any;
  private jumperUnitId: any;
  private managerUnitId: any;
  private prevSelectOption;

  private orderBy: string = 'toDate';
  private orderByUp: boolean = false;

  private ts;

  private dayOffLimit: number = DAY_OFF_LIMIT;
  private showCaption: string;
  private dayOffAmount: number;
  private limitChanged: boolean = false;

  private optionsSelected = {};
  private addVacationOpened = {};

  private triggerDayOff: number = -1;
  private doRange: number[] = range(0, 3);

  private staffProfileForm = new FormGroup({
    firstname: new FormControl('',
      Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
        JubbrFormValidator.trimmed as any
      ])),
    lastname: new  FormControl('',
      Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32)
      ])),
    umail: new FormControl('',
      Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(128),
        JubbrFormValidator.mail as any,
        JubbrFormValidator.trimmed as any
      ])),
    cellphone: new FormControl('',
      Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(32),
        JubbrFormValidator.phoneNumber as any,
        JubbrFormValidator.trimmed as any
      ])),
    street: new FormControl('',
      Validators.compose([
        Validators.minLength(4),
        Validators.maxLength(64),
        JubbrFormValidator.trimmed as any
      ])),
    zip: new FormControl('',
      Validators.compose([
        Validators.minLength(4),
        Validators.maxLength(64),
        JubbrFormValidator.trimmed as any
      ])),
    location: new FormControl('',
      Validators.compose([
        Validators.maxLength(64),
        JubbrFormValidator.trimmed as any
      ])),
    country: new FormControl( '',
      Validators.compose([
        // JubbrFormValidator.notDefaultOption
      ])),
    birthdate: new FormControl( '',
      Validators.compose([
        JubbrFormValidator.birthDate as any,
        Validators.maxLength(10),
        JubbrFormValidator.trimmed as any,
        JubbrFormValidator.validDate as any
      ])),
    iban: new FormControl('',
        Validators.compose([
        Validators.minLength(0),
        Validators.maxLength(40),
        JubbrFormValidator.iBanValidator as any,
        JubbrFormValidator.trimmed as any
      ])),
    socialid: new FormControl( '',
      Validators.compose([
        Validators.maxLength(64),
        JubbrFormValidator.ssn as any,
        JubbrFormValidator.trimmed as any
      ])),
    maritalStatus: new FormControl( 'none',
      Validators.compose([
        // JubbrFormValidator.notDefaultOption
      ])),
    childrenAmount:  new FormControl('',
      Validators.compose([
        Validators.pattern('^[0-9]{1,2}$')
      ])),
    confession: new FormControl('', Validators.compose([
      Validators.minLength(2),
      Validators.maxLength(64),
      JubbrFormValidator.trimmed as any
    ]))
  });
  private editableValue: any;

  constructor(public user: UserService,
              public staffService: StaffService,
              private webuser: WebuserService,
              private cache: Cache, public action: AppStoreAction) {
    this.showCaption = 'Show_More';
  }

  ngOnInit() {
    this.currentTab = this.userData.webuser_id + '-settings';

    if (this.userData) {
      this.staffProfileForm.patchValue(this.userData);
    }

    if (this.cache.get('staff:userTab')) {
      if (this.cache.getObject('staff:userTab')
          .find((t) => t.indexOf(this.userData.webuser_id) !== -1)) {
        let [ id, tab ] = this.cache.getObject('staff:userTab')
          .find((t) => t.indexOf(this.userData.webuser_id) !== -1).split('-');
        if (+id === this.userData.webuser_id) {
          this.currentTab = `${id}-${tab}`;
        }
      }
    }

    this.homePositionId = this.userData.home_position_id === undefined ?
      '0' : `p${this.userData.home_position_id}`;
    this.jumperUnitId =  this.userData.jumper_unit_id === undefined ?
      '0' : `u${this.userData.jumper_unit_id}`;
    this.managerUnitId = this.userData.manager_unit_id === undefined ?
      '0' : `u${this.userData.manager_unit_id}`;
    this.parseSettings();

    this.isOn = this.cache.isInArray(
      'staff:usersOpened',
      `${this.userData.webuser_id}`
    );

    const stripZeroProps = map((group: any) => filter((skill: any) => skill.skill > 0, group));
    const stripEmptyProps = pickBy(p => not(rempty(p)));
    const parseSkills = pipe(stripZeroProps, stripEmptyProps);


    this.userData = Object.assign(
      {},
      this.userData,
      { skills: this.userData.skills ? parseSkills(this.userData.skills) : {} }
    );

    this.dayoffs = this.dayoffs && this.dayoffs
        .map(d => {
          return extend(d, { inPast: moment().isAfter(d.toDate) });
        });
        // .sort((a, b) => b.toDate - a.toDate);
    this.dayOffAmount = this.dayoffs && this.dayoffs.length;
  }

  ngOnChanges(changes: any) {
    this.groupOrganisation = changes.organisationStaff.currentValue.groupOrganisation;
  }

  checkCurrentTab(tab: string) {
    return this.currentTab === tab;
  }

  toggleUserCollapsible() {

    if (this.isOn) {
      this.cache.removeFromArray(
        'staff:usersOpened',
        `${this.userData.webuser_id}`
      );
    } else {
      this.cache.pushArray(
        'staff:usersOpened',
        `${this.userData.webuser_id}`
      );
    }

    this.isOn = !this.isOn;
  }

  private switchTab(selectedTab): void {

    let temp = this.cache.getObject('staff:userTab') || [];

    if (this.cache.getObject('staff:userTab') &&
        this.cache.getObject('staff:userTab')
          .some((tab) => tab.indexOf(this.userData.webuser_id) !== -1)) {

      temp = this.cache.getObject('staff:userTab')
        .filter((tab) => tab.indexOf(this.userData.webuser_id) === -1);
    }

    temp.push(selectedTab);
    this.cache.setObject('staff:userTab', temp);
    this.currentTab = selectedTab;
  };

  private parseSkillParent(skillParent: any, method) {
    if (method === 'lang') {
      return 'lang_' + this.webuser[method][skillParent];
    }else {
      return this.webuser[method][skillParent];
    }
  };

  private parseSkillChild(skillChild) {
    return this.webuser.skillLevel[skillChild];
  }

  private parseSettings() {
    this.groupOrganisation = this.organisationStaff.groupOrganisation;
    this.unitOrganisation = this.organisationStaff.unitOrganisation;
  }

  private focusSelect(data, eventSource?) {
    if (data !== ZERO_PLACEHOLDER) {
      this.prevSelectOption = data;
    } else if (eventSource) {
      this.optionsSelected[eventSource] = true;
    }
  }

  private userSubmitSkill(webuserId: number, selectedSettingId: string, optopnName: string) {

    this.focusSelect(selectedSettingId); // todo: here?

    // if (Object.keys(this.optionsSelected).length === 3) {
    //   return;
    // }

    let settingId = selectedSettingId.slice(1);
    if (selectedSettingId === ZERO_PLACEHOLDER) {
      settingId = '-' + this.prevSelectOption.slice(1);
    }
    let newSettings = {
      webuser_id: webuserId,
      [optopnName]: settingId
    };
    this.user.update(newSettings as any).subscribe();
    this.action.fetchGetInfo();
  }

  private sendInvite( e ) {
    e.preventDefault();
    let invite = {
      home_position_id: this.userData.home_position_id,
      jumper_unit_id: this.userData.jumper_unit_id,
      ['profilefirstname-' + this.userData.webuser_id]: this.userData.firstname,
      ['profilelastname-' + this.userData.webuser_id]: this.userData.lastname,
      ['profileumail' + this.userData.webuser_id]: this.userData.umail,
      ['profilewebuser_id-' + this.userData.webuser_id]: this.userData.webuser_id,
      sendmail: true,
      webuser_id: this.userData.webuser_id
    };
    this.user.invite(invite).subscribe(res => {
      this.action.fetchGetInfo();
    });
  }

  private onLimitChange(event?: MouseEvent) {

    if (event)
      event.preventDefault();

    this.limitChanged = true;
    if (this.dayOffLimit === DAY_OFF_LIMIT) {
      this.dayOffLimit = this.dayoffs && this.dayoffs.length;
      this.doRange = range(0, this.dayoffs.length);
      this.showCaption = 'Show_Less';
    } else {
      this.doRange = range(0, 4);
      this.showCaption = 'Show_More';
      this.dayOffLimit = DAY_OFF_LIMIT;
    }
  }

  private onDayOffEvent(event: MouseEvent | number, index, len) {
    if (typeof event === 'number') {
      this.onLimitChange();
      this.triggerDayOff = this._generateNextIndex(this.dayoffs, (x) => !x.inPast, index, event);
    }
  }

  private _generateNextIndex(
    list: any[],
    criteria: (x: any) => boolean,
    index: number,
    direction: number
  ): number {
    let nextIndex = (list.length + (index + direction)) % list.length;
    if (criteria(list[nextIndex])) {
      return nextIndex;
    } else {
      while (true) {
        nextIndex = (list.length + ((index) + direction)) % list.length;
        index = nextIndex;
        if (criteria(list[nextIndex])) {
          return nextIndex;
        }
      }
    }
  }

  private getDayOffPresentAmount(list: any[]): number {
    return list.filter(x => !x.inPast).length;
  }

  private changeToEditable(e, el) {
    e.preventDefault();
    if (this.userData.had_login || !el.readOnly ) return;

    this.editableValue = el.value;
    el.readOnly = false;
  }

  private finishEditable(e, el, name) {
    e.preventDefault();
    if (this.staffProfileForm.controls[name].valid) {
      this.submitForm();
    }else {
      this.staffProfileForm.patchValue({[name]: this.editableValue});
    }
    el.readOnly = true;
  }

  private submitForm() {
    this.staffService.updateStaffInfo(this.userData.webuser_id, this.staffProfileForm.value)
    .subscribe((res: any) => {
     }, (err) => {
      try {} catch (e) {}
    });
  }
}

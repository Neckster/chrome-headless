export const htmlWebuser = `
<!-- staff-header -->
<div class="uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item uk-text-left stafflistentry"
    id="webuser-id-{{userData.webuser_id}}"
    [ngClass]="{'uk-collapsed': !isOn }"
>
  <div (click)="toggleUserCollapsible()" class="stafflistentry-head uk-nestable-panel uk-clearfix uk-width-1-1">
    <div data-nestable-action="toggle" class="uk-nestable-toggle"></div>
    <!--<div class="webUserArrows" >
      <div class="secshow">
          <span *ngIf="isOn" class="icon large arrow-right grey" style='cursor:pointer' (click)="toggleUserCollapsible()">&nbsp;</span>
       </div>
      <div class="sechide">
        <span   *ngIf="!isOn" class="icon large arrow-down blue"  style='cursor:pointer' (click)="toggleUserCollapsible()">&nbsp;</span>
      </div>
    </div>-->
    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
      <div class="uk-width-2-10 uk-grid uk-grid-collapse uk-flex-middle">
        <div class="uk-width-small-1-1 uk-width-large-1-2 uk-width-xlarge-6-10 red-color"
            [ngClass]="{'red-color': userData.pending === true, 'orange-inactive': userData.had_login === false && !userData.pending }">
          <span>{{userData.uname}}</span>
        </div>
      </div>
      
      
      <div class="uk-width-4-10 uk-text-center">
        <div class="uk-grid uk-grid-collapse uk-flex-middle">
          <div class="uk-width-4-10"><span>{{ userData.home_nname || "&ndash;" }}</span></div>
          <div class="uk-width-4-10"><span>{{ userData.home_pname || "&ndash;" }}</span></div>
          <div class="uk-width-2-10">
            <span class="jc-realhours">{{userData.realhours}}</span><br>
            (<span class="jc-hours">{{userData.hours}}</span>)
          </div>
        </div>
      </div>
      <div class="uk-width-4-10 uk-flex uk-flex-center">
      
        <div class="uk-grid uk-width-1-1">
        
          <div class="uk-width-1-4 uk-text-center">
            
          </div>
          
          <div class="uk-width-1-4 uk-text-center">
            <!--<div *ngIf="dayoffs?.length" class="uk-badge uk-badge-notification">{{ dayoffs?.length }}</div>-->
            <button *ngIf="userData.count_day_off" class="uk-button badge-wide">{{ userData.count_day_off }}</button>
          </div>
          
          <div class="uk-width-1-4 uk-text-center">
            
          </div>
          
          <div class="uk-width-1-4 uk-text-center">
            <button *ngIf="userData.pending === true" (click)="sendInvite($event)" class="uk-button uk-button-primary pending uk-text-center badge-wide" style="font-size:1.35rem;">
              <span class="icon envelope"></span>
            </button>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <ul class="uk-nestable-list hr-center-list">
    <li>
      <!-- <div data-nestable-action="toggle" class="uk-nestable-handle"></div> -->
      <div class="uk-width-1-1">
      
      <template [ngIf]="checkCurrentTab(userData.webuser_id + '-daysoff')">
            <p class="uk-float-right uk-margin-remove">
              <a (click)="addVacationOpened[userData.webuser_id] = true" >{{'plus_Days_off'| translate}}</a> 
            </p>
            <div *ngIf="addVacationOpened[userData.webuser_id]">
               <add-vacation 
                  [opened]="addVacationOpened[userData.webuser_id]"
                  [userId] = "userData.webuser_id"
                  [userName] = "userData.uname"
                  [editByManager] = true
                (closeEmitter)="addVacationOpened[userData.webuser_id] = false"
                ></add-vacation>
            </div>
          </template>
      
        <ul class="uk-tab uk-tab-responsive jc-stafflist-subnav" id="staff-subnav-{{userData.webuser_id}}" data-uk-switcher>
         
          <li (click)="switchTab(userData.webuser_id + '-settings')" 
              [ngClass]="{'uk-active': checkCurrentTab(userData.webuser_id + '-settings')}"
              class="uk-active" id="staff-subnav-{{userData.webuser_id}}-settings" 
              aria-expanded="true">
            <a href="" class="current">{{ 'Settings' | translate }}</a>
          </li>
          
          <li (click)="switchTab(userData.webuser_id + '-profile')" 
           [ngClass]="{'uk-active': checkCurrentTab(userData.webuser_id + '-profile')}"
           id="staff-subnav-{{userData.webuser_id}}-profile" 
           aria-expanded="false">
            <a href="" class="current">{{ 'Profile' | translate }}</a>
          </li>
          
          <li (click)="switchTab(userData.webuser_id + '-skill')" 
           [ngClass]="{'uk-active': checkCurrentTab(userData.webuser_id + '-skill')}"
           id="staff-subnav-{{userData.webuser_id}}-skill" 
           aria-expanded="false">
            <a href="" class="current">{{ 'Skills' | translate }}</a>
          </li>
          
          <li (click)="switchTab(userData.webuser_id + '-daysoff')" 
            [ngClass]="{ 'uk-active': checkCurrentTab(userData.webuser_id + '-daysoff') }">
            <a href="" class="current"> {{ 'Days_off' | translate }} </a>
          </li>
         
          
           <li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true" aria-expanded="false">
            <a>{{'Settings'|translate}}</a>
            <div class="uk-dropdown uk-dropdown-small">
              <ul class="uk-nav uk-nav-dropdown"></ul>
              <div></div>
            </div>
          </li>
              
        </ul>
        
        <ul class="staff-content uk-switcher jc-stafflist-subtab" id="staff-subtab-{{userData.webuser_id}}">      
          <li class="staff-settings" [ngClass]="{'uk-active': currentTab === userData.webuser_id + '-settings' }">
            <div class="uk-width-small-1-2 uk-width-medium-2-3 uk-width-large-1-2">
              <form class="uk-form uk-form-horizontal staff jc-staff" ngNoForm>
                <div class="uk-form-row uk-width-1-1">
                  <div class="uk-grid uk-grid-collapse">
                    <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">
                    </label>
                   <!-- <div class="uk-width-small-1-1 uk-width-medium-6-10">
                      Unit name the position belongs to
                    </div>-->
                  </div>
                </div>
                <div class="uk-form-row uk-width-1-1 ">
                  <div class="uk-grid uk-grid-collapse">
                    <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                           for="staffhome_position_id">{{'Home_base'|translate}}</label>
                    <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                      <select [(ngModel)]=homePositionId
                              (click)="focusSelect(homePositionId, 'home')"
                              (ngModelChange)="userSubmitSkill(userData.webuser_id, homePositionId, 'home_position_id')"
                              class="select">
                        <option value="0">{{'None_female'|translate}}</option>
                        <optgroup *ngFor="let unit of unitOrganisation " label="{{unit.text}}">
                          <option *ngFor="let position of unit.child" [ngValue]="position.id">{{position.text}}</option>
                        </optgroup>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="uk-form-row uk-width-1-1 uk-margin-top">
                  <div class="uk-grid uk-grid-collapse">
                    <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                           for="staffjumper_unit_id">{{'Job_hopper'|translate}}</label>
                    <div class ="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                      <recurrent-select
                              [(ngModel)]="jumperUnitId"
                              [defaultValue]="jumperUnitId"
                              (click)="focusSelect(jumperUnitId, 'hopper')"
                              (change)="userSubmitSkill(userData.webuser_id, jumperUnitId, 'jumper_unit_id')">

                      </recurrent-select>
                    </div> 
                  </div>
                </div>
                <div class="uk-form-row uk-width-1-1 uk-margin-top">
                  <div class="uk-grid uk-grid-collapse">
                    <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10"
                           for="staffmanager_unit_id">{{'Manager'|translate}}</label>
                    <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                      <recurrent-select
                              [(ngModel)]="managerUnitId"
                              [defaultValue]="managerUnitId"
                              (click)="focusSelect(managerUnitId, 'manager')"
                              (change)="userSubmitSkill(userData.webuser_id, managerUnitId, 'manager_unit_id')">

                      </recurrent-select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </li>
          

          <!-- staff-settings end-->
          <li class="staff-profile" [ngClass]="{'uk-active':currentTab === userData.webuser_id + '-profile', 
            'redStatus': userData.pending === true, 'orangeStatus': userData.had_login === false && !userData.pending}">
            <div class="uk-width-small-1-2 uk-width-medium-2-3">
              <form [formGroup]="staffProfileForm" class="uk-form uk-form-horizontal profile jc-template-replace-user-profile"> <!--ngNoForm-->
                <div class="uk-form-row">
                  <label class="required uk-form-label">{{'Firstname' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profilefirstname"
                         formControlName="firstname"
                         placeholder="{{'placeholder_Firstname'|translate}}"
                         readonly="readonly">
                </div>
                <div class="uk-form-row">
                  <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Lastname' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profilelastname"
                         formControlName="lastname"
                         placeholder="{{'placeholder_Lastname'|translate}}"
                         readonly="readonly">
                </div>
                <div class="uk-form-row editableInput" >
                  <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Email' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profileumail"
                         formControlName="umail" #umail
                         (click)="changeToEditable($event, umail)"
                         (blur)="finishEditable($event, umail, 'umail')"
                         [ngClass]="{'uk-form-danger': !staffProfileForm.controls.umail.valid}"
                         placeholder="{{'placeholder_Email'|translate}}"
                         readonly="readonly">
                  <i class="editIcon" [svgTemplate]="'pencilEditIcon'"></i>
                </div>
                
                <div *ngIf="userData.cellphone" class="uk-form-row">
                  <label class="required uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Cellphone' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profilecellphone"
                         formControlName="cellphone"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.street"  class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Street'| translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profilestreet"
                         formControlName="street"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.zip" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'ZIP' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 zip profilestreet"
                         formControlName="zip"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.location" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Place' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                         formControlName="location"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.country" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10" for="profilecountry-132196">{{'Country' | translate}}</label>
                  <div class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                    <select formControlName="country" class="select profilecountry" disabled="disabled">
                      <option selected="">please select</option>
                      <option value="CH">Switzerland</option>
                      <option value="DE">Germany</option>
                      <option value="AT">Austria</option>
                    </select>
                  </div>
                </div>
                <div *ngIf="userData.birthdate" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Date_of_birth' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 profilebirthdate"
                         formControlName="birthdate"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.iban"class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'IBAN' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                         formControlName="iban"
                         readonly="readonly">
                </div>
                <div *ngIf="userData.socialid" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Social_Security_Number' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10 loc profilelocation"
                         formControlName="socialid"
                         readonly="readonly">
                </div>
                
                <div *ngIf="userData.maritalStatus" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Marital_status' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10" formControlName="maritalStatus" readonly="readonly">
                </div>
                
                 <div *ngIf="userData.childrenAmount" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Number_of_children' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10" formControlName="childrenAmount" readonly="readonly">
                </div>
                
                <div *ngIf="userData.confession" class="uk-form-row">
                  <label class="uk-form-label uk-width-small-1-1 uk-width-medium-3-10">{{'Confession' | translate}}</label>
                  <input class="uk-width-small-1-1 uk-width-medium-6-10" formControlName="confession" readonly="readonly">
                </div>
                
              </form>
            </div>
          </li>
          <!-- staff-profile end -->
            <!-- staff-contracts end-->
          <li class="staff-skills" [ngClass]="{'uk-active':currentTab === userData.webuser_id + '-skill' }" >
            <div *ngIf="!userData.skills">
             <div  class="uk-grid uk-margin-top-remove">
                <div class="uk-width-1-1">
                  <h2 class="uk-margin-remove">{{'Self_assessment'|translate}}</h2>
                </div>
                <div class="uk-width-1-1">
                    <h3 class="uk-margin-top">{{'Languages'|translate}}</h3>
                    <!-- ADD language on default -->
                    <dl class="uk-description-list-horizontal" >
                      <dt>{{'lang_German' | translate }}</dt>
                      <dd>-</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal" >
                      <dt>{{'lang_English' | translate }}</dt>
                      <dd>-</dd>
                    </dl>
                </div>
              </div>
            </div>
            <div *ngIf="userData.skills">
              <div *ngIf="userData.skills" class="uk-grid uk-margin-top-remove">
                <div class="uk-width-1-1">
                  <h2 class="uk-margin-remove">{{'Self_assessment'|translate}}</h2>
                </div>
                <div class="uk-width-1-1">
                  <div *ngIf="!userData.skills.lang">
                    {{'no_skills' | translate}}
                  </div>
                  <div *ngIf="userData.skills.lang">
                    <h3 class="uk-margin-top">{{'Languages'|translate}}</h3>
                    <dl class="uk-description-list-horizontal" *ngFor="let currentLang of userData.skills.lang | mapToIterable">
                      <dt>{{parseSkillParent(currentLang.key, 'lang')| translate}}</dt>
                      <dd>{{parseSkillChild(currentLang.value.skill)| translate }}</dd>
                    </dl>
                  </div>
                </div>
              </div>
              <hr class="uk-grid-divider">
              <div class="uk-grid staff-job-skills">
                <div class="uk-width-1-1">
                  <h3 class="uk-margin-top">{{'Skills'|translate}}</h3>
                </div>
                <div *ngFor="let currentSkill of userData.skills | mapToIterable">
                  <div *ngIf="currentSkill && currentSkill.key != 'lang'" class=" st-entertain">
                    <h4 class="uk-margin-remove">{{'skill_' + currentSkill.key |translate}}</h4>
                    <div class="uk-width-1-1">
                      <dl class="uk-description-list-horizontal" *ngFor="let currentEntertain of currentSkill.value | mapToIterable">
                        <dt>{{parseSkillParent(currentEntertain.key, currentSkill.key) |translate}}</dt>
                        <dd>{{parseSkillChild(currentEntertain.value.skill) |translate}}</dd>
                      </dl>
                    </div>
                  </div>      
                </div>
            </div>
            </div>
          </li>
          
          <li [ngClass]="{ 'uk-active': currentTab === userData.webuser_id + '-daysoff' }">
          
            <span *ngIf="!dayoffs?.length">
              {{'no_days_off'|translate}}.
            </span>
          
            <template [ngIf]="dayoffs?.length">
             <ul class="uk-width-1-1 uk-nestable-list uk-parent">
                <li *ngFor="let dayOffRecord of dayoffs|staffSort:orderBy:orderByUp; let i = index" class="uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item uk-text-left">
                   <dayoff *ngIf="true"
                      [data]="dayOffRecord"
                      [userId]="userData.webuser_id"
                      [index]="i"
                      [trigger]="triggerDayOff"
                      [userName]="userData.uname"
                      [dayOffAmount]="getDayOffPresentAmount(dayoffs)"
                      (dayOffOutput)="onDayOffEvent($event, i, dayoffs.length)">
                   </dayoff>
                </li>
              </ul>
             <!--<a *ngIf="(dayOffAmount > dayOffLimit) || limitChanged" style="padding: 1.5rem 3rem;" href="#" (click)="onLimitChange($event, dayoffs, dayOffLimit)">{{ showCaption | translate }}...</a>-->
            </template>
          </li>
          
        </ul>
        <!-- staff-content -->
        <!-- main-tabs -->
      </div>
    </li>
  </ul>
</div>
<!-- #stafflist -->
<div *ngIf="removeJobber">
  <section class="uk-modal uk-open" id="useredit" tabindex="-1" role="dialog" aria-labelledby="label-useredit" aria-hidden="false" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <a (click)="removeJobber = false" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal"></a>
      <div class="uk-modal-header">
        <header id="label-useredit">
          <h2><a href="#" class="uk-modal-close"><!--icon--></a>
            <span>Edit jobber</span>
          </h2>
        </header>
      </div>
      <div class="modal-content">
        <form class="uk-form useredit" ngNoForm>
          <div class="uk-grid uk-grid-collapse uk-text-left">
            <div class="uk-width-1-1">
              <div class="jc-uname">
              </div>
            </div>
            <div id="useredit_ok" class="uk-width-1-1 uk-margin-bottom">
              <button (click)="removeJobber = false" type="button" class="uk-width-1-1 uk-button uk-button-large uk-modal-close remove delete jc-click-deletePosJobber">{{'Remove_from_position' | translate}}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
`;


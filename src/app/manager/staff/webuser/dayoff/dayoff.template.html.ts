export const dayoffTemplate: string = `
    
  <div (click)="toggle = !toggle" class="stafflistentry-head uk-nestable-panel uk-width-1-1 uk-margin-small-top defaultStyles " [ngClass]="{'uk-collapsed': !toggle, 'past-item': data.inPast }" [style.background]="!toggle ? 'white' : ''">
    <div data-nestable-action="toggle" class="uk-nestable-toggle" ></div>
    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle " [style.background]="!toggle ? 'white' : ''">
      <div class="uk-width-1-1 ">
         <div class="uk-width-1-4 uk-display-inline-block uk-float-left uk-margin-small-top"> {{data.fromDate.format('DD.MM.YYYY') }} - {{data.toDate.format('DD.MM.YYYY')}}</div>
         <div class="uk-width-1-4 uk-display-inline-block uk-float-left uk-margin-small-top"> {{reasonsJobber[data.reason]| translate}}</div>
         <div class="uk-width-1-4 uk-display-inline-block uk-float-left ">
            <div class="uk-width-1-1 statusTagWrapper" style="text-align: right; padding-right: 10px">
             <span [style.visibility]="data.totalDays ? '' : 'hidden'"> {{data.totalDays}} {{ 'days'| translate}}</span>
           </div>
         </div>
         <div class="uk-width-1-4 uk-display-inline-block uk-float-right">

           
            <div class="uk-width-1-4 uk-float-right">
               <i class="pencilEditButton" [ngClass] = "{ 'label-icon-past': data.inPast}"
                            [svgTemplate]="'pencilEditIcon'" (click)="clickOnPencil($event, data.inPast)"></i>      
            </div>
            <div class="uk-width-2-4 uk-float-right statusTagWrapper">
              <span [ngClass]="{ 'label-past': data.inPast }" class="statusTag {{data.statusLabel}} uk-margin-left">{{data.statusLabel | translate}}</span>
            </div>
          </div>
      </div>
      <div *ngIf="addVacationOpened">
         <add-vacation 
            [opened] = "addVacationOpened"
            [editByManager] = true
            [userId] = 'userId'
            [userName] ='userName'
            [vacation]="data"
            [showNavigation]="dayOffAmount > 1"
            [amount] = "dayOffAmount - 1"
            [index] = index
            (closeEmitter)="onVacationClose($event)">    
          </add-vacation>
      </div>
     
    </div>
    
    </div>
    
     <ul *ngIf='toggle' class=" hr-center-list uk-nestable-list defaultStyles" [ngClass]="{ 'past-item': data.inPast }" style="background: white">
      <li>
          <!-- <div class="uk-width-1-1 jc-template-replace-jobs-details"></div> -->
          <div class="jc-template-jobs-details">
            <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
              <div class="uk-width-1-1">
                <!--<div class="uk-grid uk-grid-collapse uk-text-left">
                  <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">{{'Company' | translate}}
                  </div>
                  <div class="uk-width-small-3-4 uk-width-medium-3-4">
                    <p class="uk-width-1-1 uk-margin-remove"><span
                      class="jc-title">{{companyName}}</span></p>
                  </div>
                </div>-->
                <div class="uk-width-1-1 uk-margin-top">
                  <div class="uk-grid uk-grid-collapse uk-text-left">
                    <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                      {{'Status' | translate}}
                    </div>
                    <div class="uk-width-small-3-4 uk-width-medium-3-4">
                      <div class="uk-width-1-1 uk-margin-remove uk-margin-small-top" *ngIf="data.send">
                          <div class="jc-diffdays uk-width-6-10 uk-float-left">
                            <div class="uk-width-5-10 uk-float-left" *ngIf="data.webuserId !== data.send.webuser_id">{{ 'set_by' | translate}}</div>
                            <div class="uk-width-5-10 uk-float-left" *ngIf="data.webuserId == data.send.webuser_id">{{ 'Inquiry_sent' | translate}}</div>
                             <div *ngIf="data.webuserId !== data.send.webuser_id" 
                                  class="uk-width-5-10 uk-float-right"> 
                                {{data.send['webuser_name']}}
                            </div>
                          </div>
                          <div class="uk-width-4-10 uk-float-left ">
                            {{ data.send.dateMoment.format('DD.MM.YYYY') }}
                          </div>
                      </div>
                      <div class="uk-width-1-1 uk-margin-remove" *ngIf="data.change">
                          <div class="jc-diffdays uk-width-6-10 uk-float-left">{{ 'Changed' | translate}} </div>
                          <div class="uk-width-4-10 uk-float-left">
                            <div class="uk-width-5-10 uk-float-left">{{ data.change.dateMoment.format('DD.MM.YYYY') }}</div>
                            <div class="uk-width-5-10 uk-float-right"></div>
                          </div>
                      </div>
                      <div class="uk-width-1-1 uk-margin-remove" *ngIf="data.approve">
                          <div class="jc-diffdays uk-width-6-10 uk-float-left">
                              <div class="uk-width-5-10 uk-float-left">{{ 'Approved' | translate}}</div>
                              <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{data.approve['webuser_name']}} </div>
                          </div>
                          <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{ data.approve.dateMoment.format('DD.MM.YYYY')  }}</div>
                      </div>
                      <div class="uk-width-1-1 uk-margin-remove" *ngIf="data.update">
                          <div class="jc-diffdays uk-width-6-10 uk-float-left">
                              <div class="uk-width-5-10 uk-float-left"> {{ 'Updated' | translate}}</div>
                              <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{data.update['webuser_name']}}</div>
                          </div>
                          <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{ data.update.dateMoment.format('DD.MM.YYYY') }}</div>
                      </div>
                      <div class="uk-width-1-1 uk-margin-remove" *ngIf="data.decline">
                          <div class="jc-diffdays uk-width-6-10 uk-float-left">
                              <div class="uk-width-5-10 uk-float-left"> {{ 'Declined' | translate}}</div>
                              <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{data.decline['webuser_name']}}</div>
                          </div>
                          <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{ data.decline.dateMoment.format('DD.MM.YYYY') }}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div *ngIf="data.message" class="uk-width-1-1 uk-margin-top">
                  <div class="uk-grid uk-grid-collapse uk-text-left">
                    <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                      {{'Info' | translate}}
                    </div>
                    <div class="uk-width-small-3-4 uk-width-medium-3-4">
                      <p class="uk-width-1-1 uk-margin-remove"><span class="jc-comments">{{data.message}}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div *ngIf="data.managerMessage" class="uk-width-1-1 uk-margin-top">
                  <div class="uk-grid uk-grid-collapse uk-text-left">
                    <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                      {{'Message' | translate}}
                    </div>
                    <div class="uk-width-small-3-4 uk-width-medium-3-4">
                      <p class="uk-width-1-1 uk-margin-remove"><span class="jc-comments">{{data.managerMessage}}</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
     </ul>
  

`;

import { Component, Input, EventEmitter }       from '@angular/core';
import { select }                 from 'ng2-redux/lib/index';
import { dayoffTemplate }         from './dayoff.template.html';
import { CapitalizePipe }         from '../../../../lib/pipes/capitalize';
import { ModalApiService }        from '../../../../services/shared/modal';
import * as moment                from 'moment';
import { AddVacationComponent }
          from '../../../availability/addVacation/addVacationForm.component';
import { Output } from '@angular/core';

@Component({
  selector: 'dayoff',
  directives: [AddVacationComponent],
  pipes: [CapitalizePipe],
  template: dayoffTemplate,
  styles: [`
        .defaultStyles{
          box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 
                      0 1px 3px 0 rgba(0, 0, 0, 0.12)!important;
          background: white;
        }
       
        .statusTagWrapper{
          padding-top: 5px;
        }
        .statusTag{
          color: white;
          background: #ff8f00;
          padding: 2px 4px;
          -webkit-border-radius: 5px;
          -moz-border-radius: 5px;
          border-radius: 5px;
        }
        .statusTag.Updated, .statusTag.Approved{
          background: #18a018;
        }
        .statusTag.Declined{
          background: #E7403D;
        }
        .past-item a, .past-item div, .past-item p, .past-item li * {
           color: lightgrey!important;
        }
        .label-past {
          color: #fff!important;
          background: darkgray!important;
        }
  `]
})
export class DayOffComponent {

  @Input() data;
  @Input() userName;
  @Input() userId;
  @Output() dayOffOutput = new EventEmitter();
  @Input() trigger;
  @Input() index;
  @Input() dayOffAmount;

  @select() info$;

  private info;
  private toggle: boolean = false;
  private reasonsJobber = {
    'holidays': 'Holidays_reason',
    'education': 'Education_reason',
    'services': 'Public_and_social_reason',
    'sickness': 'Sickness_reason',
    'overtime': 'overtime_compensation',
    'other': 'Other_reason'
};
  private companyName: string;
  private addVacationOpened: boolean = false;
  private tagStatus: string;

  constructor(private modalApi: ModalApiService) {
    this.info$.subscribe(info => {
      this.info = info;
    });
  }

  ngOnInit() {
    this.checkCompanyList();
    this.checkTagStatus();
  }

  ngOnChanges(change) {
    if (change.trigger) {
      if (this.index === change.trigger.currentValue) {
        this.addVacationOpened = true;
      }
    }
  }

  changeVacationHandler(event, data) {
    event.stopPropagation();
    this.modalApi.open('ADD_VACATION');
  }

  changeDate(st, week, day?) {
    if ( st === 'week') {
      let date = moment(`${week}${day}`, 'GGWWE');
      return date.format('DD.MM.YYYY');
    }else if (st === 'mn') {
      let date = moment(week, 'mm');
      return date.format('DD.MM.YYYY');
    }
  }

  checkCompanyList() {
    let id = this.data.companyId,
      res = this.info.companies.hasOwnProperty(id) ? this.info.companies[id] : undefined;
    this.companyName = res ? res.cname : 'Invalid company';
  }
  /* tslint:disable:no-string-literal */
  checkTagStatus() {
    let sortObj = {
      Approved : this.data.approve ? this.data.approve.dateMoment : undefined,
      Updated : this.data.update ? this.data.update.dateMoment : undefined,
      Declined : this.data.decline ? this.data.decline.dateMoment : undefined
    };

    let arr: any = Object.keys(sortObj).sort( (a: any, b: any): any => {
      if (!sortObj[a]) return true;

      if (!sortObj[b]) return false;

        return sortObj[b].isAfter(sortObj[a], 'minutes');
    }),
      res = arr[0],
      noStatus = !sortObj['Approved'] &&
                 !sortObj['Updated'] &&
                 !sortObj['Declined'];
    this.tagStatus = noStatus ? 'Open' : res;

  }
  clickOnPencil(e, past) {
    e.preventDefault();
    e.stopPropagation();
    if (!past) {
      this.addVacationOpened = true;
    }
  }
  onVacationClose(event) {
    this.dayOffOutput.emit(event);
    this.addVacationOpened = false;
  }
  /* tslint:disable:no-string-literal */
}

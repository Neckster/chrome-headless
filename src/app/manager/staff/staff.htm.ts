export const htmlStaff = `

<create-jobber *ngIf="info.manager"
               [opened]="addJobberOpen"
               (closeEmitter)="loginModalOpened = false">
               
</create-jobber>

<!--<add-vacation [opened]="false" [vacation]="{}"></add-vacation>-->

<div class="uk-grid uk-grid-collapse uk-text-left" style='padding:0 1rem'>
  <div class="uk-width-1-1 uk-container-center">
    <h1>{{'Staff' | translate}}</h1>
    <div class="uk-width-small-1-1 uk-width-large-1-2">
      <!--<div class="uk-grid uk-grid-small">
        &lt;!&ndash;<div class="uk-width-4-10 uk-margin-bottom">
          <button type="button" class="uk-button uk-button-large uk-width-1-1 jc-click-userinvite"
                  (click)="loginModalOpened = !loginModalOpened; hideScrollBar()" [disabled]="!selectedUnit">{{'plussign_jobber' | translate}}
          </button>
        </div>&ndash;&gt;
        &lt;!&ndash;<div class="uk-width-6-10 uk-margin-bottom">
          <form class="uk-form uk-autocomplete uk-width-1-1 jc-search-staff" ngNoForm>
            <div class="search-box uk-form-icon-flip uk-width-1-1">
              <input class="uk-search-field uk-width-1-1 jc-search-staff" type="search"
                     autocomplete="off"
                     [(ngModel)]="searchStr"
                     (keyup)="onKey($event)" id="staffsearchtext" placeholder="{{'placeholder_Search_staff' | translate}}"/>
              <i class="search-icon" [svgTemplate]="'magnifyingGlass'"></i>
            </div>

            &lt;!&ndash;<div class="uk-dropdown" aria-expanded="false"></div>&ndash;&gt;
          </form>
        </div>&ndash;&gt;
      </div>-->
    </div>
    <div class="uk-width-1-1">
      <!-- FIXME: create left/right icons for this info box in CSS -->
      <span
        class="uk-width-1-1 tip uk-hidden-large">{{'swipe_left_right' | translate}}</span>
      <div class="uk-grid uk-grid-collapse">
        <div class="table-container-outer">
          <div class="table-fade uk-hidden-large uk-hidden-xlarge"></div>
          <div class="table-container">
            <div id="css-table-3" class="css-table staff-table">
              <div class="uk-sticky-placeholder sraff-bar" data-uk-sticky="{top:120}"  style="height: 40px; margin: 0; margin-bottom: 20px">
                <div class="staff-header table-header uk-width-1-1 uk-text-left uk-text-bold"
                     style="margin: 0">
                  <div class="uk-grid uk-grid-collapse uk-width-1-1">
                    <div class="uk-width-2-10">
                      <span class="jc-uname">
                        <a class="staff-sort uname"
                           data-order-by="uname"
                           data-order-dir="asc"
                           [ngClass]="{asc: cols.unameOrderBy, desc: !cols.unameOrderBy, current: currentOrderBy === 'uname' }"
                           (click)="cols.unameOrderBy = !cols.unameOrderBy; onFilterChange('uname', cols.unameOrderBy)"
                        >{{'firstname_lastname' | translate}}</a>
                      </span>
                    </div>
                    <div class="uk-width-4-10 uk-text-center">
                      <div class="uk-grid uk-grid-collapse">
                        <!-- TODO
                        <div class="uk-width-3-10">
                          <span class="jc-rating"><a class="staff-sort rating" data-order-by="rating" data-order-dir="asc">{{i18n Rating}}</a></span>
                        </div>
                        -->
                        <div class="uk-width-4-10">
                          <span class="jc-nname">
                            <a class="staff-sort nname current"
                               data-order-by="nname"
                               data-order-dir="asc"
                               [ngClass]="{'asc': cols.nnameOrderBy, 'desc': !cols.nnameOrderBy, current: currentOrderBy === 'nname' }"
                               (click)="cols.nnameOrderBy = !cols.nnameOrderBy; onFilterChange('nname', cols.nnameOrderBy)"
                            >Unit</a>
                          </span>
                        </div>
                        <div class="uk-width-4-10">
                          <span class="jc-pname">
                            <a class="staff-sort pname current"
                               data-order-by="pname"
                               data-order-dir="asc"
                               [ngClass]="{'asc': cols.pnameOrderBy, 'desc': !cols.pnameOrderBy, current: currentOrderBy === 'pname' }"
                               (click)="cols.pnameOrderBy = !cols.pnameOrderBy; onFilterChange('pname', cols.pnameOrderBy)">
                              Position
                            </a>
                          </span>
                        </div>
                        <div class="uk-width-2-10">
                          <span class="jc-hours">
                            <a class="staff-sort hours current"
                               data-order-by="hours"
                               data-order-dir="asc"
                               [ngClass]="{'asc': cols.hoursOrderBy, 'desc': !cols.hoursOrderBy, current: currentOrderBy === 'hours' }"
                               (click)="cols.hoursOrderBy = !cols.hoursOrderBy; onFilterChange('hours', cols.hoursOrderBy)">
                              {{'abbr_sum_hours'|translate}} ({{'planned' | translate}})
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                    
                    <div class="uk-width-4-10 uk-text-center">
                      <!-- TODO
                      <span class="jc-issues">
                        <a class="staff-sort issues asc" data-order-by="issues" data-order-dir="asc">{{'Open_issues'| translate}}</a>
                      </span>-->
                      <p class="uk-margin-bottom-remove"> {{ 'open_issues' | translate }} </p>
                      
                      <div class="uk-grid uk-width-1-1">
                        <div class="uk-width-1-4 uk-text-center">
                          <span class="header-caption-s uk-link"> {{ 'Check-in' | translate }} </span>
                        </div>
                        <div class="uk-width-1-4 uk-text-center">
                          <span class="header-caption-s uk-link">
                            <a class="staff-sort day_off current"
                               data-order-by="day_off"
                               data-order-dir="asc"
                               [ngClass]="{'asc': cols.daysOffOrderBy, 'desc': !cols.daysOffOrderBy, current: currentOrderBy === 'day_off' }"
                               (click)="cols.daysOffOrderBy = !cols.daysOffOrderBy; onFilterChange('day_off', cols.daysOffOrderBy)">
                                {{'day_off' | translate}} 
                            </a>
                           </span>
                        </div>
                        <div class="uk-width-1-4 uk-text-center">
                          <span class="header-caption-s uk-link"> {{'evaluation' | translate}} </span>
                        </div>
                        <div class="uk-width-1-4 uk-text-center">
                          <span class="header-caption-s uk-link"> 
                            <a class="staff-sort onboarding current"
                               data-order-by="onboarding"
                               data-order-dir="asc"
                               [ngClass]="{'asc': cols.onboardingOrderBy, 'desc': !cols.onboardingOrderBy, current: currentOrderBy === 'onboarding' }"
                               (click)="cols.onboardingOrderBy = !cols.onboardingOrderBy; onFilterChange('onboarding', cols.onboardingOrderBy)">
                              {{'onboarding' | translate}}
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- staff-header -->

              <ul *ngIf="staffList[0]" id="stafflist" class="uk-width-1-1 uk-nestable-list uk-parent">
                <li 
                  class="uk-parent uk-nestable-item uk-nestable-nodrag uk-nestable-list-item uk-text-left stafflistentry"
                  *ngFor="let user of staffList | paginate: { id: 'server', itemsPerPage: 10, currentPage: selectedPage, totalItems: total }">
                  <web-user *ngIf="info.manager" [organisationStaff]="organisationStaff"
                            [userData]="user" [dayoffs]="userDayOffsMap[user.webuser_id]"></web-user>
                </li>
              </ul>
              <!-- #stafflist -->
            </div>
            <!-- css-table -->
          </div>
          <!-- table-container -->
        </div>
        <!-- table-container-outer -->
      </div>
      <!-- uk-grid -->
    </div>
    <!-- uk-width-1-1 -->

    <div *ngIf="staffList[0]" class="uk-width-1-1 uk-container-center uk-margin uk-grid">
      <div class="spinner" [ngClass]="{ 'hidden': !loading }"></div>
      <pagination-controls class="uk-container-center uk-pagination" (pageChange)="onPageChange($event)"
                           id="server" #p>                  
       <div class="custom-pagination">
                  <div *ngFor="let page of p.pages"  class='inlineDivs' [class.current]="p.getCurrent() === page.value">
                      <a (click)="p.setCurrent(page.value)" *ngIf="p.getCurrent() !== page.value" class='paginationElement'>
                          <span>{{ page.label }}</span>
                      </a>
                      <div *ngIf="p.getCurrent() === page.value" class='paginationElement'>
                          <span>{{ page.label }}</span>
                      </div>
                  </div>
                  
              </div>
         </pagination-controls>
    </div>
    <!-- pagination -->
  </div>
  <!-- uk-container-center -->
</div>
`;

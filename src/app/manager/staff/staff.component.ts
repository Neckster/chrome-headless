import {
    Component,
    NgZone,
    ApplicationRef,
    HostListener }                  from '@angular/core';
import { select }                   from 'ng2-redux/lib/index';
import { TranslatePipe }            from 'ng2-translate/ng2-translate';
import {
  PaginationControlsCmp,
  PaginatePipe,
  PaginationService }               from 'ng2-pagination/index';
import { Observable }               from 'rxjs/Rx';
import * as R                       from 'ramda';

import { SvgTemplateDirective }     from '../../lib/directives/svg-icon.directive';
import { DayOffFactory, DayOff } from '../../models/DayOff';

import { AppStoreAction }           from '../../store/action';

import { StaffService }             from '../../services/staff.service';
import { SkillsService }            from '../../services/skills.service';
import { ModalApiService }          from '../../services/shared/modal';
import { JobService }               from '../../services/shared/JobService';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { Cache }                    from '../../services/shared/cache.service';

import { WebuserComponent }         from './webuser/webuser.component';
import { CreateJobberController }   from './create-jober/jobber-form.component';

import { htmlStaff }                from './staff.htm.ts';


const dayOffsPath = R.path(['manager', 'day_offs']);

const FIRST_PAGE: number = 1;
const ITEMS_PER_PAGE: number = 10;

const ORDER_ASCEND: string = 'asc';
const ORDER_DESCEND: string = 'desc';

const REMOTE_RECORD: string = 'remote';
const CACHED_RECORD: string = 'cached';


interface IRecordMetadata {
  kind: string;
  cached: boolean;
}

interface IRecord extends IRecordMetadata {
  pagination: any;
  staff: any[];
  page: number;
}

interface ICachedRecord extends IRecord { }

interface IServerRecord extends IRecord { }

type PageAction = ICachedRecord | IServerRecord;

const boolToOrder = (dir: boolean): string => dir ? ORDER_ASCEND : ORDER_DESCEND;

const remoteRecordMetadata: IRecordMetadata = {
  kind: 'remote',
  cached: false
};

const cachedRecordMetadata: IRecordMetadata = {
  kind: 'cached',
  cached: true
};

@Component({
  selector: 'manager-controlling',
  providers: [PaginationService, DayOffFactory],
  directives: [PaginationControlsCmp, WebuserComponent,
    CreateJobberController, SvgTemplateDirective],
  pipes: [TranslatePipe, PaginatePipe],
  styles: [require('./style.less')],
  template: htmlStaff
})
export class ManagerStaff {

  @select() info$: any;
  @select(state => state.localisation) lang$: any;
  @select() currentUnit$: any;
  @select() addJobberOpen$: any;
  @select() staffFilter$: any;


  public info;
  public organisationStaff;
  public loginModalOpened = false;
  public unsubscribe: any;
  public selectedPage: number = FIRST_PAGE;
  public total: number;
  public currentOrderBy: string;
  public orderDirection: string;
  public test: any;
  public itemPer = ITEMS_PER_PAGE;
  public staffCache = {};
  public searchStr: string;
  public staffList = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  private unitStore;
  private selectedUnit;
  private addJobberOpen: boolean;

  private userDayOffsMap;

  private cols = {
    unameOrderBy: false,
    nnameOrderBy: false,
    pnameOrderBy: false,
    hoursOrderBy: false,
    daysOffOrderBy: false,
    onboardingOrderBy: false
  };

  constructor(public staff: StaffService,
              public skills: SkillsService,
              public cache: Cache,
              public jobService: JobService,
              public action: AppStoreAction,
              private modalApi: ModalApiService,
              private dayOffFactory: DayOffFactory
  ) {

    this.orderDirection = this.cache.get('staff:direction') || ORDER_ASCEND;
    this.currentOrderBy = this.cache.get('staff:orderby') || 'uname';
    this.searchStr = this.cache.get('staff:search') || '';

    for (let v in this.cols) {
      if (this.cols.hasOwnProperty(v))
        this.cols[v] = this.orderDirection === 'asc';
    }

    this.currentUnit$.subscribe(unit => {
      this.unitStore = unit;
      this.selectedUnit = R.compose(R.not, R.isEmpty)(unit);
    });

    this.addJobberOpen$.subscribe( addJobberOpen => {
      this.addJobberOpen = addJobberOpen;
    });

    this.test = this.info$.subscribe(info => {
      this.info = info;
      this.staffCache[1] = undefined;
      this.staffCache = {};

      if (info.manager && info.manager.selected.company_id) {
        if (dayOffsPath(info)) {
          this.userDayOffsMap = dayOffsPath(info)[info.manager.selected.company_id];
        }
      }

      if (this.userDayOffsMap) {
        this.userDayOffsMap = R.map(R.map(v =>
          this.dayOffFactory.createDayOff(v)))(this.userDayOffsMap);
      }else {
        this.userDayOffsMap = {};
      }

      if (this.info.manager) {
        let unitId = info.manager.selected.unit_id;
        if (!this.unitStore.unit_id && !isNaN(unitId) && unitId !== 0) {
          this.action.fetchCurrentUnit(info.manager.selected.unit_id);
        }
        this.lang$.subscribe(data => {
          if (this.info.manager) {
            ParseOrganisationService.lang = data;
            this.parseSettingsStaff();
          }
        });

      let pageToGet = this.cache.getObject('staff:pageNum') || FIRST_PAGE;

      this.getPage(pageToGet, this.currentOrderBy, this.orderDirection)
        .subscribe(this.processRetrievedPage.bind(this));



      }
    });
    this.staffFilter$.subscribe(staffFilter => {
      this.searchStr = staffFilter;
      this.onKey(event);

    });

  }


@HostListener('window:scroll', ['$event'] )
track (event) {
  let w = window;
  let fixEl = document.getElementById('staffStickyContainerId');
  if (fixEl) {
    if (w.scrollY > 120) {
      fixEl.classList.add('st');
    }else if (w.scrollY < 120) {
      fixEl.classList.remove('st');
    }
  }
}



ngOnInit() {
    this.staffCache[this.selectedPage] = this.staffList;
  }

  onKey(event) {
    this.staffCache = {};
    this.cache.set('staff:search', this.searchStr);
    this.getPage(FIRST_PAGE, this.currentOrderBy, this.orderDirection)
      .subscribe(this.processRetrievedPage.bind(this));
  }

  getPage(
    page: number = 1,
    order: string = 'uname',
    direction: string = 'desc',
    search?
  ): Observable<PageAction> {

    this.cache.set('staff:pageNum', page);
    this.selectedPage = page;

    this.currentOrderBy = order;
    this.cache.set('staff:direction', this.orderDirection);
    this.cache.set('staff:orderby', this.currentOrderBy);

    if (this.staffCache[page]) {

      this.staffList = this.staffCache[page];

      return Observable.create(observer => {
        observer.next(R.merge(cachedRecordMetadata, {
          page,
          staff: this.staffCache[page]
        }));
      });

    } else {

      const complementaryObservable = Observable.create(observer => {
        observer.next(R.merge(remoteRecordMetadata, {
          page
        }));
      });

      const getStaffObservable = this.staff
        .get(page, this.itemPer, this.currentOrderBy, this.orderDirection, this.searchStr);

      /* tslint:disable */
      return Observable.combineLatest(
        getStaffObservable,
        complementaryObservable
      ).map((xs: any) => R.merge.apply(null, xs)); //  R.merge(...xs)
      /* tslint:enable */
    }
  }

  onFilterChange(currentOrder: string, directionBy: boolean) {
    this.staffCache = {};
    this.orderDirection = boolToOrder(directionBy);

    this.getPage(this.selectedPage, currentOrder, this.orderDirection, this.searchStr)
      .subscribe(this.processRetrievedPage.bind(this));
  }

  onPageChange(event) {
    this.getPage(event, this.currentOrderBy, this.orderDirection, this.searchStr)
      .subscribe(this.processRetrievedPage.bind(this));
  }

  parseSettingsStaff() {
    ParseOrganisationService.treeStruct(this.info.manager.organisation);
    this.organisationStaff = {
      'unitOrganisation': ParseOrganisationService
          .unitPosition(ParseOrganisationService.treeData[0].child),
      'groupOrganisation': ParseOrganisationService
          .htmlOptionTree(ParseOrganisationService.treeData[0])
    };
  }

  processRetrievedPage(response: PageAction) {
    this.staffList = response.staff;
    switch (response.kind) {
      case REMOTE_RECORD:
        this.total = response.pagination.total;
        this.staffCache[response.pagination.page] = response.staff;
        break;
      case CACHED_RECORD:
        break;
      default : // empty

    }

  }

  processRetrievingError(error: any) {
    this.staffList = [];
    this.staffCache = {};
    this.total = 0;
  }

  ngOnDestroy() {
    this.test.unsubscribe();
    ParseOrganisationService.isEditCompany = false;
  }
}

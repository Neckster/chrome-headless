export const reportTabHTML = `
<div class="container" style="padding:0!important; margin:0!important">
  <div *ngIf="reportsMenu"> 
    <div class="uk-form uk-width-1-1 reportFilters" [formGroup]="reportsControllers" >
      <div class="uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-2-10 
                  uk-float-left uk-margin-bottom uk-margin-right">
        <span class="uk-width-1-1">{{'From'|translate}}</span>
        <input type="text"
              (click)="openFromCalendar = true"
               formControlName="fromControl"
               class="uk-width-1-1"
               placeholder="{{'birthday_placeholder' | translate}}"
               readonly="true">
        <div *ngIf="fromDateAlert" class="uk-width-1-1">
           <span  class="error uk-text-danger ">{{'msg_err_date_is_past'|translate}</span>
        </div>
        <div *ngIf="openFromCalendar" class="uk-float-right">
           <my-date-picker
              (selectedDate)="setFromDate($event)"
              [valueDate]="fromDate"
              [valueDay]="fromDay">
           </my-date-picker>
         </div>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-2-10 
                  uk-float-left uk-margin-bottom uk-margin-right">
        <span class="uk-width-1-1 ">{{'To'|translate}}</span>
        <input type="text"
               (click)="openToCalendar = true"
               formControlName="toControl"
               class="uk-width-1-1"
               placeholder="{{'birthday_placeholder' | translate}}"
               readonly="true">
        <div *ngIf="toDateAlert" class="uk-width-1-1">
           <span  class="error uk-text-danger ">{{'msg_err_date_is_past' | translate}}</span>
        </div>
        <div *ngIf="openToCalendar">
           <my-date-picker
            (selectedDate)="setToDate($event)"
              [valueDate]="toDate"
              [valueDay]="toDay"
            ></my-date-picker>
        </div>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-3-10 uk-width-large-2-10 
                  uk-float-left uk-margin-bottom ">
        <span class="uk-width-1-1 uk-invisible">{{'Unit'|translate}}</span>
        <div class="uk-width-1-1">
          <recurrent-select (selected)="onChangeSelect($event)" [noCompany]="true" [defaultValue]="defaultValue">
           </recurrent-select>
       </div>
        
      </div>
    </div>
    <div class="uk-width-1-1 reportWrap">
      <div [ngClass]="{'sortTagUp': orderByUp,'sortTagDown': !orderByUp}"
        class="uk-width-1-1 reportTitle " >
        <div class="reportCol" style="width:5%;text-align: center">{{'Nb'|translate}}.</div>
        <div class="reportCol webuser_name" style="width:15%" (click)="sortByTag('webuser_name')">
          {{'Firstname'|translate}} 
          <i [ngClass]="{sorted: orderBy === 'webuser_name'}"></i>
          </div>
        <div class="reportCol" style="width:15%" (click)="sortByTag('webuser_surname')">
          {{'Surname'|translate}}
          <i [ngClass]="{sorted: orderBy === 'webuser_surname'}"></i>
        </div>
        <div class="reportCol" style="width:15%" (click)="sortByTag('unit_name')">
          {{'Unit'|translate}}
          <i [ngClass]="{sorted: orderBy === 'unit_name'}"></i>
        </div>
        <div class="reportCol" style="width:15%" (click)="sortByTag('position_name')">
          {{'Position'|translate}}
          <i [ngClass]="{sorted: orderBy === 'position_name'}"></i>
        </div>
        <div class="reportCol" (click)="sortByTag('jobbhome_hours')" style="text-align: center;width:16%;">
          {{'homebase_hours'|translate}}
         <i [ngClass]="{sorted: orderBy === 'jobbhome_hours'}"></i>
        </div>
        <div class="reportCol" (click)="sortByTag('jobbhoper_hours')" style="text-align: right;width:8%;">
          {{'Job_hopper'|translate}}
          <i [ngClass]="{sorted: orderBy === 'jobbhoper_hours'}"></i>
        </div>
        <div class="reportCol" (click)="sortByTag('total')" style=" width:6%; text-align: center; float:right">
          {{'Total'|translate}}
          <i [ngClass]="{sorted: orderBy === 'total'}"></i>
        </div>
      </div>
      <div class="uk-width-1-1 reportContent">
        <ul *ngIf="jobbers" class="uk-list">
          <li *ngFor="let jobber of jobbers|staffSort:orderBy:orderByUp; let i = index;" (click)="openTimesheet(true, jobber)">
            <div class="reportCol" style="width:5%; text-align: center">{{i + 1}}.</div>
            <div class="reportCol" style="width:15%">{{jobber.webuser_name || ' '}}</div>
            <div class="reportCol" style="width:15%">{{jobber.webuser_surname || ' '}}</div>
            <div class="reportCol" style="width:15%">{{jobber.unit_name || ' '}}</div>
            <div class="reportCol" style="width:15%">{{jobber.position_name || ' '}}</div>
            <div class="reportCol" style="width:16%; text-align: center">{{jobber.home_worked_time || '0'}}</div>
            <div class="reportCol" style="width:8%; text-align: center;">{{jobber.hoper_worked_time || '0'}}</div>
            <div class="reportCol" style=" width:6%; text-align: center; float:right">{{jobber.total_hours || '0'}}</div>
          </li>
        </ul>
      </div>
    </div>
  </div>

 
</div>
`;

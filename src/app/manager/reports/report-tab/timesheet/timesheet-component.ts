import { Component, Input,
      Output, HostListener }        from '@angular/core';
import { select }                   from 'ng2-redux/lib/index';
import { TranslateService }         from 'ng2-translate/ng2-translate';
import { Observable }               from 'rxjs/Rx';
import { timesheetHTML }            from './timesheet.html';
import { AppStoreAction }           from '../../../../store/action';
import { Cache }                    from '../../../../services/shared/cache.service';
import { FormGroup, FormControl }   from '@angular/forms';
import * as moment                  from 'moment';
import { range }                    from 'ramda';
import { StatisticService,
        ShortUserStatistic }        from '../../../../services/statistic.service';
import { PrintService }             from '../../../../services/print.service';
import { TimestampPipe } from '../../../../lib/pipes/timestamp';

declare namespace pdfMake {
  export function createPdf(doc: any): any;
}

@Component({
  selector: 'timesheet',
  styles: [require('./style.less')],
  template: timesheetHTML,
  pipes: [TimestampPipe]
})
export class TimesheetComponent {

  @select() info$: any;
  @select(state => state.localisation) lang$: any;
  @select() currentUnit$: any;
  @select() reportMonth$;

  @Input() jobbers;
  @Input() selectedJobber;

  private info;
  private timesheet: boolean = true;
  private jobbersList: any;
  private daysList: any;
  private selectedUser: any;
  private userStatistic: any;
  private company: any;
  private timeList = [
    {
      name: 'Previous Month',
      value: 'previous_month'
    },
    {
      name: 'Current Month',
      value: 'current_month'
    },
    {
      name: 'Current Year',
      value: 'current_year'
    }
  ];
  private defaultMonth = moment().format('MMMM');
  private defaultYear = moment().format('YYYY');
  private timesheetControllers = new FormGroup({
    jobberControl: new FormControl(),
    monthControl: new FormControl(this.defaultMonth),
    yearControl: new FormControl(this.defaultYear)
  });
  private detailInfo: any;
  private totalInfo: any;
  private monthCounter = range(0, 12);
  private monthList = this.monthCounter.map(m => {
    return moment().month(m).format('MMMM');
  });
  private lastYear: any = moment().add(10, 'year').year();
  private lang: string;
  private yearsList = range(2015, this.lastYear);
  private moveHeaderleft: any;

  constructor (public cache: Cache,
              public statisticService: StatisticService,
              private tp: TranslateService,
              public action: AppStoreAction,
              private print: PrintService) {

    this.info$.subscribe(info => {
      this.info = info;

      if (this.info && this.info.manager && this.info.manager.selected
        && this.info.companies && this.info.companies[this.info.manager.selected.company_id]) {
        this.company = this.info.companies[this.info.manager.selected.company_id];
      }
    });

    this.reportMonth$.subscribe(data => {
      let setMonth = data ? data.format('MMMM') : moment().format('MMMM');
      this.timesheetControllers.patchValue({monthControl: setMonth});
    });

     this.lang$.subscribe(data => {
       this.lang = data;
    });

  }

  @HostListener('window:scroll', ['$event']) track(event) {
    let w = window;
    let fixEl = document.getElementById('timesheetStickyContainerId');
    if (fixEl) {
      if (w.scrollY >= 210) {
        fixEl.classList.add('st');
      } else if (w.scrollY < 210) {
        fixEl.classList.remove('st');
      }
    }
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    let el = document.getElementById('tableBody').getBoundingClientRect();
    this.moveHeaderleft = el.left + 'px';

  }

  onScroll(e, content) {
    let dis = content.getBoundingClientRect();
    this.moveHeaderleft =  dis.left + 'px';
  }

  /* tslint:disable */
  ngOnInit() {
    if (this.jobbers) {
      this.jobbersList = this.jobbers;
      this.timesheetControllers.patchValue({jobberControl: this.selectedJobber['webuser_id']});
    }
    this.emptyTable();
    this.submit();

    this.timesheetControllers.valueChanges.subscribe(el => {
      this.emptyTable();
      this.submit( el.monthControl, el.yearControl,  el.jobberControl);
    });
  }
  /* tslint:disable */


  submit(month?, year?, uId?) {
    month = month || this.timesheetControllers.value.monthControl;
    year = year || this.timesheetControllers.value.yearControl;
    uId = uId || this.timesheetControllers.value.jobberControl;
    this.statisticService
      .getUserStatistic(month, +year, uId)
      .subscribe((info: ShortUserStatistic[]) => {
        this.userStatistic = info;
        if (this.userStatistic) {
          this.detailInfo = this.userStatistic.detail_info;
          this.totalInfo = this.userStatistic.total_info;
          this.convertData();
        }
      });
  }

  emptyTable() {
    let daysCounter = moment()
        .month(this.timesheetControllers.value.monthControl)
        .year(this.timesheetControllers.value.yearControl)
        .daysInMonth();
    let daysRage = range(1, daysCounter);
    this.detailInfo = daysRage.map(el => {
      let d = moment(`${+el}`, 'DD');
      return {
        dateFormat: d.format('dddd, MMM DD'),
        dateFormatDay: d.format('dddd'),
        dateFormatMonth: d.format('MMM'),
        dateFormatDate: d.format('DD')
      };
    });
  }

  /* tslint:disable */
  convertData() {
    if (this.detailInfo && this.detailInfo.length) {
      // console.log(this.detailInfo);
      let totalStatus;
      this.detailInfo = this.detailInfo.map(el => {
        el['dateFormat'] = moment(`${+el.date}`, 'X').format('dddd, MMM DD');
        el['dateFormatDay'] = moment(`${+el.date}`, 'X').format('dddd');
        el['dateFormatMonth'] = moment(`${+el.date}`, 'X').format('MMM');
        el['dateFormatDate'] = moment(`${+el.date}`, 'X').format('DD');

        el['totalStatus'] = 'checked';
        if (el.rows.length) {
          el.rows = el.rows.map(sh => {
            if (sh.day_off_data) {
            sh.day_off_data['dateFrom'] =
              moment(`${+sh.day_off_data.from_week}${+sh.day_off_data.from_weekday}`, 'GGWWE')
                .format('DD.MM.YYYY');
            sh.day_off_data['dateTo'] =
              moment(`${+sh.day_off_data.to_week}${+sh.day_off_data.to_weekday}`, 'GGWWE')
                .format('DD.MM.YYYY');
            }
            if ( !sh.day_off_data &&  sh.status !== 'checked')  el['totalStatus'] = 'unChecked';
            return sh;
          });
        }
        return el;
      });
    }
    if (this.totalInfo.day_off &&
      Object.keys(this.totalInfo.day_off).length) {
      let dayOff = this.totalInfo.day_off,
          tempDayOff = 0, tempHoliday = 0;
      tempDayOff += dayOff.holidays ? dayOff.holidays : 0;
      tempDayOff += dayOff.services ? dayOff.services : 0;
      tempDayOff += dayOff.overtime ? dayOff.overtime : 0;
      this.totalInfo.day_off['dayOffTotal'] = tempDayOff;

      tempHoliday += dayOff.sickness ? dayOff.sickness : 0;
      tempHoliday += dayOff.education ? dayOff.education : 0;
      tempHoliday += dayOff.other ? dayOff.other : 0;
      this.totalInfo.day_off['holidayTotal'] = tempHoliday;
    }
  }
  /* tslint:disable */

  getXLSUrl() {
      let month = this.timesheetControllers.value.monthControl;
      let year = this.timesheetControllers.value.yearControl;
      let uId = this.timesheetControllers.value.jobberControl;
      return this.statisticService.getUserStatisticXLSUrl(month, +year, uId);
  }

  onPrintClick(event) {

    event.preventDefault();
    let monthTable = this.print.buildTable(
      [100, 40, 40, 40, 210, 30],
      this.print.buildTableRow([
        {text: `${this.tp.instant('Day').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]},
        {text: `${this.tp.instant('In').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]},
        {text: `${this.tp.instant('Out').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]},
        {text: `${this.tp.instant('Break').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]},
        {text: `${this.tp.instant('Details').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]},
        {text: `${this.tp.instant('Total').toUpperCase()}`, fontSize: 9, margin: [0,5,0,5]}
      ]),
      this.print.buildTableBody(this.detailInfo, this.tp.currentLang)
        .reduce((acc: any[], c) => acc.concat(c), []),
      this.print.buildTableRow([
        {text: `${this.tp.instant('worked_hours')}: ${this.totalInfo.jobbhome_name} - ${this.totalInfo.jobbhome_hours}${this.tp.instant('abbr_hour_letter')}, ${this.tp.instant('Job_hopper')} - ${this.totalInfo.jobbhoper_hours}h`, fontSize: 10, colSpan: 5, bold: false, margin: [0,5,0,5]}, // tslint:disable
        {text: '', margin: [0,5,0,5]},
        {text: '', margin: [0,5,0,5]},
        {text: '', margin: [0,5,0,5]},
        {text: '', margin: [0,5,0,5]},
        {text: `${this.totalInfo.total_work_hours.replace(':', '.')}`, fontSize: 9, bold: true, margin: [0,5,0,5]}
      ])
    );

    let sumsTable = this.print.buildTable(
      ['*', '*', '*', '*', '*', '*'],
      this.print.buildTableRow([
        {
          text: `${this.tp.instant('day_off')}:`, colSpan: 3, alignment: 'left'
        },
        {
          text: ''
        },
        {
          text: ''
        },
        {
          text: `${this.tp.instant('Controlled_agreed')}`, colSpan: 3, alignment: 'right'
        },
        {
          text: ''
        },
        {
          text: ''
        }
      ]),
      [].concat(
        this.print.buildTableRow([
          {text: `${this.tp.instant('Holidays_reason')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.holidays ? `${this.totalInfo.day_off.holidays}  ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: `${this.tp.instant('Sickness_reason')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.sickness ? `${this.totalInfo.day_off.sickness}  ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: '', margin: [0,0,0,0]},
          {text: '', margin: [0,0,0,0]},
        ]),
        this.print.buildTableRow([
          {text: `${this.tp.instant('Public_holidays')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.services ? `${this.totalInfo.day_off.services} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: `${this.tp.instant('Education_reason')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.education ? `${this.totalInfo.day_off.education} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: '', margin: [0,0,0,0]},
          {text: '', margin: [0,0,0,0]}
        ]),
        this.print.buildTableRow([
          {text: `${this.tp.instant('Overtime')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.overtime ? `${this.totalInfo.day_off.overtime} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: `${this.tp.instant('Other_reason')}:`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: this.totalInfo.day_off.other ? `${this.totalInfo.day_off.other} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,0]}, // tslint:disable
          {text: '', margin: [0,0,0,0]},
          {text: '', margin: [0,0,0,0]}
        ]),
        this.print.buildTableRow([
          {text: `${this.tp.instant('Total')}:`, bold: false, alignment: 'left', margin: [0,0,0,10]}, // tslint:disable
          {text: this.totalInfo.day_off.dayOffTotal ? `${this.totalInfo.day_off.dayOffTotal} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,10]}, // tslint:disable
          {text: `${this.tp.instant('Total')}:`, bold: false, alignment: 'left', margin: [0,0,0,10]}, // tslint:disable
          {text: this.totalInfo.day_off.holidayTotal ? `${this.totalInfo.day_off.holidayTotal} ${this.tp.instant('days')}` : `0 ${this.tp.instant('days')}`, bold: false, alignment: 'left', margin: [0,0,0,10]}, // tslint:disable
          {text: '', margin: [0,0,0,10]},
          {text: '', margin: [0,0,0,10]}
        ]),
        this.print.buildTableRow([
          {text: `${this.tp.instant('Date')}`, bold: false, alignment: 'left', margin: [0,0,0,0], pagebreak: 'after'},
          {text: '......................', bold: false, alignment: 'left', margin: [0,0,0,0], pagebreak: 'after'},
          {text: '', margin: [0,0,0,0], pagebreak: 'after'},
          {text: '', margin: [0,0,0,0], pagebreak: 'after'},
          {text: '', margin: [0,0,0,0], pagebreak: 'after'},
          {text: '.......................', bold: false, alignment: 'right', margin: [0,0,0,0], pagebreak: 'after'}
        ])
      )
    );

    let headerTable = this.print.buildTable(
      ['*', '*', '*', '*'],
      [],
      [].concat(
        this.print.buildTableRow([
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: this.company.cname || '',
            alignment: 'right',
            margin: [0,0,0,0],
            bold: false,
            fontSize: 12
          }
        ]),
        this.print.buildTableRow([
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: `${this.company.street}` || '',
            alignment: 'right',
            margin: [0,0,0,0],
            bold: false,
            fontSize: 12
          }
        ]),
        this.print.buildTableRow([
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: '', margin: [0,0,0,0]
          },
          {
            text: `${this.company.location}` || '',
            alignment: 'right',
            margin: [0,0,0,0],
            bold: false,
            fontSize: 12
          }
        ]),
        this.print.buildTableRow([
          {
            text: `${this.tp.instant('Timesheet')}`,
            alignment: 'left',
            fontSize: 12
          },
          {
            text: this.selectedJobber.full_data.uname,
            alignment: 'left',
            fontSize: 12
          },
          {
            text: `${this.tp.instant(this.timesheetControllers.value.monthControl)} ${this.timesheetControllers.value.yearControl}`, // tslint:disable
            alignment: 'left',
            fontSize: 12
          },
          {
            text: ''
          }
        ])
      )
    );


    let printObject = {
      content: [
        // { text: `${this.tp.instant('Timesheet')}` },
        Object.assign({}, headerTable, {layout: 'noBorders'}),
        monthTable,
        { text: `* ${this.tp.instant('report_time_note')}`, margin: [0, 10, 0, 10], fontSize: 10, bold: true },// tslint:disable
        Object.assign({}, sumsTable, {layout: 'noBorders'})],

      defaultStyle: {
        // alignment: 'justify'
      }
    };

    pdfMake.createPdf(printObject).open();
  }

}

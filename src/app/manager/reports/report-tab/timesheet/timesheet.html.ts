export const timesheetHTML = `
<div *ngIf="timesheet" >
  <div class="uk-width-1-1 uk-form uk-margin-large-top" >
      <div class="uk-width-1-1 timeSheetFilters"  [formGroup]="timesheetControllers">
        <div class="uk-width-8-10 uk-float-left">
          <div class="uk-width-4-10 uk-float-left uk-margin-large-right">
            <span class="timesheetTitle">{{'Timesheet'|translate}}:</span>
            <select formControlName="jobberControl"
                    id="timesheetJobber"
                    class="select" >
              <option *ngFor="let j of jobbersList" value="{{j.webuser_id}}">{{ j.webuser_name + ' ' + j.webuser_surname}}</option>
            </select>
          </div>
          <!--<div class="uk-width-2-10 uk-float-left">
            <select formControlName="timeControl">
              <option *ngFor="let t of timeList" value="{{t.value}}">{{t.name}}</option>
            </select>
          </div>-->
          <div class="uk-width-2-10 uk-float-left">
            <select formControlName="monthControl">
              <option *ngFor="let month of monthList" value="{{month}}">{{month | translate}}</option>
            </select>
          </div>
          <div class="uk-width-1-10 uk-float-left">
            <select formControlName="yearControl">
              <option *ngFor="let year of yearsList" value="{{year}}">{{year}}</option>
            </select>
          </div>
          <div class="uk-width-medium-1-1 uk-width-large-2-10 uk-float-right" style="text-align: center">
             <a [attr.href]="getXLSUrl()" target="_blank"><button class="uk-button uk-display-inline">{{'EXPORT'}}</button></a>
             <button (click)="onPrintClick($event)" class="uk-button printButton uk-display-inline">
               <i [svgTemplate]="'printIcon'"></i>
             </button>
          </div>
        </div>
        <div class="uk-width-2-10 uk-float-right" style="text-align: right">
          <span>{{company.cname}}</span><br>
          <span>{{company.street}}</span><br>
          <span>{{company.location}}</span>
        </div>
      </div>
      <div class="TableMainWrap" (scroll)="onScroll($event, tableBodyVal)">
        <table class="timesheetTable uk-margin-top" >
          <thead id="timesheetStickyContainerId" [style.left]="moveHeaderleft" class="timesheetTitle" >
            <tr>
              <th class="uk-width-1-10" style="min-width: 150px">{{'Day'|translate}}</th>
              <th class="uk-width-8-10 tableWrap">
                <table class="uk-width-1-1" >
                  <tr>
                    <th class="uk-width-1-10 timeStatus" style="width: 117px">{{'In' |translate}}</th>
                    <th class="uk-width-1-10 timeStatus" style="min-width: 103px">{{'Out' |translate}}</th>
                    <th class="uk-width-1-10 timeStatus" style="min-width: 103px">{{'Break' |translate}}</th>
                    <th class="uk-width-5-10 inform" >{{'Details'}}</th>   
                  </tr>        
                </table>
              </th>
              <th class="uk-width-1-10 totalDayHours checked">{{'total_hours' | translate }}</th>
            </tr>
          </thead>
          <tbody  class="timesheetBody" id='tableBody' #tableBodyVal>
            <tr *ngFor="let day of detailInfo; let i = index;">
               <td class="uk-width-1-10 dateInfo" style="min-width: 150px">
                {{day.dateFormatDay | translate }}, {{day.dateFormatMonth | translate }} {{day.dateFormatDate}} 
               </td>
               <td class="tableWrap">
                 <table class="uk-width-1-1" >
                    <template [ngIf]='day.rows && day.rows.length'>
                      <tr *ngFor ='let shift of day.rows' class="shiftLine fake" 
                          [ngClass]="{'checked': shift.day_off_data  || (shift.status && shift.status === 'checked')}">
                        <template [ngIf]="!shift.is_day_off" >
                          <td class="uk-width-1-10 timeStatus" style="width: 117px">{{ shift.fromTime | timestamp : 'en' : 'HH:mm' }}</td>
                          <td class="uk-width-1-10 timeStatus" >{{ shift.toTime | timestamp : 'en' : 'HH:mm' }}</td>
                          <td class="uk-width-1-10 timeStatus">{{shift.break || '00:00'}}</td>
                          <td class="uk-width-5-10 inform">
                            {{shift.position_name}}, {{shift.unit_name }}</td>
                        </template>
                        <template [ngIf]="shift.is_day_off">
                          <td class="uk-width-1-10 timeStatus" style="width: 117px">{{'Days_off'|translate}}</td>
                          <td class="uk-width-1-10 timeStatus" ><span>-</span></td>
                          <td class="uk-width-1-10 timeStatus" ><span>-</span></td>
                          <td class="uk-width-5-10 inform" >
                          {{shift.day_off_data.reason |translate}} ({{shift.day_off_data.dateFrom}} - {{shift.day_off_data.dateTo}}
                          | {{shift.day_off_data.total_days}} {{'days'}})
                          </td>
                                   
                        </template>
                      </tr>
                   </template>
                   <template [ngIf]='!day.rows || !day.rows.length'>
                      <tr class="shiftLine fake">
                        <td class="uk-width-1-10 timeStatus" style="width: 117px"><span>-</span></td>
                        <td class="uk-width-1-10 timeStatus" ><span>-</span></td>
                        <td class="uk-width-1-10 timeStatus" ><span>-</span></td>
                        <td class="uk-width-5-10 inform" ><span>-</span></td>
                      </tr>
                   </template>
                  </table>        
               </td>
               <td class="uk-width-1-10 totalDayHours"
                  [ngClass]="{'checked': day.totalStatus && day.totalStatus === 'checked'}">
                  {{day.total_work_hours || ' '}}
               </td>
            </tr>
          </tbody>
          <tfoot *ngIf="totalInfo" class="total-footer">
               <tr>
                 <td class="uk-width-1-10" style="min-width:150px">
                 {{'TOTAL'}}              
                 </td>  
                 <td class="uk-width-8-10 tableWrap" >
                    <table class="uk-width-1-1" >
                      <tr>
                        <td class="uk-width-1-10 timeStatus" style="width: 117px"></td>
                        <td class="uk-width-1-10 timeStatus" ></td>
                        <td class="uk-width-1-10 timeStatus" ></td>
                        <td class="uk-width-5-10 inform" >{{'worked_hours' | translate}}
                        ({{totalInfo.jobbhome_name}}: {{totalInfo.jobbhome_hours}}h | {{'Job hopper'|translate}}:
                        {{totalInfo.jobbhoper_hours}}h)</td>
                      </tr>        
                    </table>
                 </td>
                 <td class="uk-width-1-10 totalDayHours">
                  {{totalInfo.total_work_hours || ''}}
                 </td>
              </tr>     
          </tfoot>
        </table>
        <div *ngIf='totalInfo && totalInfo.day_off' class="totalInfo uk-width-1-1">
          <div class="uk-width-3-5 uk-float-left uk-margin-large-top" style="padding: 0 15px 0 0">
            <p class="title-total">{{'Days_off' | translate}}:</p>
            <div class="uk-width-2-5 uk-float-left">
                <ul class="uk-list uk-width-1-1">
                  <li>
                    <span class="uk-float-left">{{'Holidays' | translate}}:</span>
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.holidays">
                      {{totalInfo.day_off.holidays}} {{'days'|translate}}
                    </span>
                    <span class="uk-float-right" *ngIf="!totalInfo.day_off.holidays">
                      {{ '-'}} 
                    </span>
                  </li>
                  <li>
                    <span class="uk-float-left" >{{'Public_holidays' | translate}}:</span>
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.services">
                      {{totalInfo.day_off.services}} {{'days'|translate}}
                    </span>
                     <span class="uk-float-right" *ngIf="!totalInfo.day_off.services">
                      {{ '-'}} 
                    </span>
                  </li>
                  <li>
                    <span class="uk-float-left">{{'Overtime'|translate}}:</span>
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.overtime">
                      {{totalInfo.day_off.overtime}} {{'days'|translate}}
                    </span>
                    <span class="uk-float-right" *ngIf="!totalInfo.day_off.overtime">
                      {{ '-'}} 
                    </span>
                  </li>
                  <!--<li>-->
                    <!--<span class="uk-float-left">{{'Total'}}:</span>-->
                    <!--<span class="uk-float-right" *ngIf="totalInfo.day_off.dayOffTotal">-->
                       <!--{{totalInfo.day_off.dayOffTotal}} {{'days'|translate}}-->
                    <!--</span>-->
                     <!--<span class="uk-float-right" *ngIf="!totalInfo.day_off.dayOffTotal">-->
                      <!--{{ '-'}} -->
                    <!--</span>-->
                  <!--</li>-->
                  <li class="uk-margin-top">
                    <span class="uk-float-left">{{'Date'|translate}}</span>
                    <span class="uk-float-right">...................................................</span>
                  </li>
                </ul>
                
            </div>
            <div class="uk-width-2-5 uk-margin-large-left uk-float-left" style="padding: 0 15px">
                <ul class="uk-list uk-width-1-1">
                  <li>
                    <span class="uk-float-left">{{'Sickness' |translate}}:</span>
                    
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.sickness">
                      {{totalInfo.day_off.sickness}} {{'days'|translate}}
                    </span>

                    <span class="uk-float-right" *ngIf="!totalInfo.day_off.sickness">
                      {{ '-'}} 
                    </span>

                  </li>
                  <li>
                    <span class="uk-float-left">{{'Education_reason' |translate}}:</span>
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.education">
                      {{totalInfo.day_off.education }}  {{'days'|translate}}
                    </span>

                    <span class="uk-float-right" *ngIf="!totalInfo.day_off.education">
                      {{ '-'}} 
                    </span>

                  </li>
                  <li><span class="uk-float-left">{{'Other_reason' |translate}}:</span>
                    <span class="uk-float-right" *ngIf="totalInfo.day_off.other">
                      {{totalInfo.day_off.other }} {{'days'|translate}}
                    </span>

                    <span class="uk-float-right" *ngIf="!totalInfo.day_off.other">
                      {{ '-'}} 
                    </span>

                  </li>
                  <!--<li>-->
                    <!--<span class="uk-float-left">{{'Total'}}:</span>-->
                    <!--<span class="uk-float-right" *ngIf="totalInfo.day_off.holidayTotal">-->
                      <!--{{totalInfo.day_off.holidayTotal}} {{'days'|translate}}-->
                    <!--</span>-->
                       <!--<span class="uk-float-right" *ngIf="!totalInfo.day_off.holidayTotal">-->
                      <!--{{ '-'}} -->
                    <!--</span>-->
                  <!--</li>-->
                </ul> 
            </div>
          </div>
          <div style='position:relative; top:0'
                class="uk-width-2-5 uk-float-right uk-align-center uk-margin-large-top" 
                style="padding-left: 20px; min-height: 17.2rem; position: relative; top:0">
                <p class="title-total" style="position:absolute; top:0">{{'Controlled_agreed' | translate}}:</p>  
                <p style="position:absolute; bottom:0; margin: 0">
                ....................................................................
                </p>
          </div>
        </div>
      </div>
  </div>
</div>
`;

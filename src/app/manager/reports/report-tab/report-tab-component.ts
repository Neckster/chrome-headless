import {
  Component,
  NgZone,
  ViewChild,
  Input, Output,
  ElementRef,
  ApplicationRef }                  from '@angular/core';
import { select }                   from 'ng2-redux/lib/index';
import { TranslatePipe }            from 'ng2-translate/ng2-translate';
import { Observable }               from 'rxjs/Rx';
import { SvgTemplateDirective }     from '../../../lib/directives/svg-icon.directive';
import { reportTabHTML }            from './report-tab-component.html';
import { AppStoreAction }           from '../../../store/action';
import { StaffService }             from '../../../services/staff.service';
import { JobService }               from '../../../services/shared/JobService';
import { ParseOrganisationService } from '../../../services/shared/parseOrganisation.service';
import { Cache }                    from '../../../services/shared/cache.service';
import { StaffSortPipe }            from '../../../../app/lib/pipes/staffSortPipes';
import { FormGroup, FormControl }   from '@angular/forms';
import * as moment                  from 'moment';
import { RecurrentSelect }          from '../../../lib/directives/recurrentSelect';
import { TimesheetComponent }       from './timesheet/timesheet-component';
import { StatisticService,
          ShortUserStatistic }      from '../../../services/statistic.service';
import { EventEmitter }             from '@angular/router-deprecated/src/facade/async';


@Component({
  selector: 'report-tab',
  providers: [ StatisticService ],
  directives: [RecurrentSelect, TimesheetComponent],
  styles: [require('../style.less')],
  template: reportTabHTML
})
export class ReportComponent {

  @ViewChild('select') container: ElementRef;

  @select() info$: any;
  @select(state => state.localisation) lang$: any;
  @select() currentUnit$: any;
  @select() reportCompany$: any;
  @Output() outSelectedJobber = new EventEmitter();

  private info;
  private tree;
  private organizations;

  private innerValue;
  private reportsMenu: boolean = true;
  private orderBy: string = 'webuser_name';
  private orderByUp: boolean = true;

  private tabs: any;
  private fromDate = moment();
  private fromDay = moment().startOf('month');
  private openFromCalendar: boolean = false;
  private fromDateAlert: boolean = false;
  private jobbers: ShortUserStatistic[] = [];

  private userStatistic: any = [];
  private toDate = moment();
  private toDay = moment().endOf('month');
  private openToCalendar: boolean = false;

  private toDateAlert: boolean = false;

  private selectedJobber: any;

  private reportsControllers = new FormGroup({
    fromControl: new FormControl(),
    toControl: new FormControl(),
    companyControl: new FormControl()
  });

  private timesheetControllers = new FormGroup({
    jobberControl: new FormControl(),
    timeControl: new FormControl('currentMonth')
  });
  private selectedUser: any;
  private defaultValue: any;

  constructor(public staff: StaffService,
              public cache: Cache,
              public jobService: JobService,
              public action: AppStoreAction,
              public statisticService: StatisticService) {

    this.reportCompany$.subscribe(reportCompany => {
      this.defaultValue = reportCompany;
    });
  }

  /* tslint:disable */
  ngOnInit() {
    this.reportsControllers.valueChanges.subscribe(str => {
      if ( str.fromControl && str.toControl && str.companyControl
        && !this.fromDateAlert && !this.toDateAlert) {
        this.statisticService.filterJobbers(
            +this.fromDay.format('GGWW'),
            +this.toDay.format('GGWW'),
            +this.fromDay.format('E'),
            +this.toDay.format('E'),
            str.companyControl
        ).subscribe((info: ShortUserStatistic[]) => {
          this.jobbers = info;
          if (this.jobbers.length) {
            this.openTimesheet(false, this.jobbers.sort(this.sortJobber)[0]);
          }else {
            this.openTimesheet(false, []);
          }
        });
      }
    });

    this.initReportControllers(this.reportsControllers, this.getReportTimerange(this.cache))

  }


  getReportTimerange(storageInterface: Cache) {

    const fromStorage = storageInterface.getObject('timesave');
    const toStorage = storageInterface.getObject('timesavetoday');

    return (fromStorage && toStorage)
      ? { from: moment(fromStorage).startOf('month').format('DD.MM.YYYY'), to: moment(toStorage).format('DD.MM.YYYY') }
      : { from: moment().startOf('month').format('DD.MM.YYYY'), to: moment().format('DD.MM.YYYY') };

  }

  initReportControllers(form: FormGroup, initialValues): void {

    form.get('fromControl').patchValue(initialValues.from, { emitEvent: false });
    form.get('toControl').patchValue(initialValues.to, { emitEvent: false });

  }

  sortJobber(a, b) {
    return a['webuser_name'].toLowerCase() === b['webuser_name'].toLowerCase() ? 0 :
      a['webuser_name'].toLowerCase() > b['webuser_name'].toLowerCase() ?
      1  : -1 ;

  }
  /* tslint:disable */

  checkTabStatus(tabName: string) {
    return this.tabs.find(t => t.name === tabName).active;
  }

  switchTab(tabName: string) {
    this.resetTabs();
    this.tabs.find(t => t.name === tabName).active = true;
    this.cache.set('reports:tab', tabName);

  }

  resetTabs() {
    this.tabs = this.tabs.map(t => Object.assign({}, t, {active: false}));
  }

  sortByTag(name: any) {
    this.orderBy = name;
    this.orderByUp = !this.orderByUp;
  }

  openTimesheet(open: boolean, jobber?: any) {
    this.selectedJobber = jobber;
    if (!this.jobbers.length) {
      this.selectedJobber = [];
    }
    this.outSelectedJobber.emit({
      jobbers:  this.jobbers,
      selected: this.selectedJobber,
      open: open
    });


  }
  /* tslint:disable */

  setFromDate(e) {
    if (e.action) {
      this.fromDay = e.action;
      this.action.selectedMonthOnReport(e.action);
      this.reportsControllers.patchValue({fromControl: e.action.format('DD.MM.YYYY')});
      this.checkAlert();
    }
    this.openFromCalendar = false;
  }

  setToDate(e) {
    if (e.action) {
      this.toDay = e.action;
      this.reportsControllers.patchValue({toControl: e.action.format('DD.MM.YYYY')});
      this.checkAlert();
    }

    this.openToCalendar = false;
  }

  checkAlert() {
    if (this.reportsControllers.value['toControl'] &&
      this.reportsControllers.value['fromControl']) {
      this.toDateAlert  = this.fromDay.isSame(this.toDay, 'day') ||
                          this.fromDay.isAfter(this.toDay) ;
    }
  }

  onChangeSelect(e) {
    if (e.action && e.edit) {
      this.defaultValue = e.action;
      this.reportsControllers.patchValue({companyControl: +e.action.slice(1)});
    }else if(e.action && !e.edit && this.defaultValue) {
      this.reportsControllers.patchValue({companyControl: +this.defaultValue.slice(1)});
    }else{
      this.defaultValue = e.action;
      this.reportsControllers.patchValue({companyControl: +e.action.slice(1)});
    }
  }


  saveToLocalStorage(fromDay, toDay) {
    localStorage.setItem("timesave", JSON.stringify(fromDay));
    localStorage.setItem("timesavetoday", JSON.stringify(toDay));
  }

  round(value) {
    return Math.round(value * 100) / 100;
  }

  ngOnDestroy() {
    let fromDay = this.fromDay;
    let toDay = this.toDay;

    this.saveToLocalStorage(fromDay, toDay);

    if  (this.defaultValue) {
      this.action.selectedCompanyOnReport(this.defaultValue);
    }
  }
}

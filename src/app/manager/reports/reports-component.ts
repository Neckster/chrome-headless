import { Component }                from '@angular/core';
import { select }                   from 'ng2-redux/lib/index';
import { TranslatePipe }            from 'ng2-translate/ng2-translate';
import { SvgTemplateDirective }     from '../../lib/directives/svg-icon.directive';
import { reportsHTML }              from './reports.html';
import { StatisticService }         from '../../services/statistic.service';
import { AppStoreAction }           from '../../store/action';
import { StaffService }             from '../../services/staff.service';
import { JobService }               from '../../services/shared/JobService';
import { Cache }                    from '../../services/shared/cache.service';
import { StaffSortPipe }            from '../../../app/lib/pipes/staffSortPipes';
import { IUITab }                   from '../../lib/interfaces/shared/ui';
import { ReportComponent }          from './report-tab/report-tab-component';
import { TimesheetComponent }       from './report-tab/timesheet/timesheet-component';

const tabList: IUITab[] = [
  {
    name: 'report',
    caption: 'Jobber_hours',
    active: true
  },
  {
    name: 'timesheet',
    caption: 'Timesheet',
    active: false
  }

];

@Component({
  selector: 'report-controlling',
  providers: [StatisticService],
  directives: [SvgTemplateDirective,
    ReportComponent, TimesheetComponent],
  pipes: [TranslatePipe, StaffSortPipe],
  styles: [require('./style.less')],
  template: reportsHTML
})
export class ManagerReports {

  @select() info$: any;
  @select(state => state.localisation) lang$: any;
  @select() currentUnit$: any;

  private tabs: any;
  private timesheetJobbers: any;
  private selectedJobber: any;
  private showTimeSheet: boolean = false;

  constructor(public staff: StaffService,
              public cache: Cache,
              public jobService: JobService,
              public action: AppStoreAction) {
    this.tabs = tabList;
  }

  ngOnInit() {}

  checkTabStatus(tabName: string) {
    return this.tabs.find(t => t.name === tabName).active;
  }

  switchTab(tabName: string) {
    this.resetTabs();
    this.tabs.find(t => t.name === tabName).active = true;
    this.cache.set('reports:tab', tabName);
  }

  resetTabs() {
    this.tabs = this.tabs.map(t => Object.assign({}, t, {active: false}));
  }

  setSelectedJobbers(e) {
    if (e.selected && e.jobbers && e.jobbers.length) {
      this.selectedJobber = e.selected;
      this.timesheetJobbers = e.jobbers;
      if (e.open && this.timesheetJobbers.length) {
        this.showTimeSheet = true;
        this.switchTab('timesheet');
      }
    }else {
      this.selectedJobber = [];
      this.timesheetJobbers = [];
      this.showTimeSheet = false;
    }
  }
  timesheetJoober(e) {

  }

}

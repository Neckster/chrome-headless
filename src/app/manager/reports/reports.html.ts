export const reportsHTML = `
<div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    <div class=" uk-width-1-1 uk-container-center">
      <h1>{{'Reporting' | translate}}</h1>
      <ul id="availability-subnav" class="uk-tab uk-tab-responsive">

       <li (click)="switchTab('report')" 
           id="availability-subnav-availability"
           [ngClass]="{'uk-active': checkTabStatus('report')}">
        <a class="current">{{'Mitarbeiterstunden' | translate}}</a>
       </li>
     
        <li (click)="timesheetJobbers?.length && switchTab('timesheet')" 
            id="availability-subnav-vacation"
            [ngClass]="{'uk-active': checkTabStatus('timesheet')}">
          <a class="current" [ngClass]="{'unSelectTub': !timesheetJobbers?.length}">
            {{'Timesheet' | translate}}
          </a>
        </li>
        
      </ul>
    </div>

    <div *ngIf="checkTabStatus('report')" style="width: 100%">
        <report-tab (outSelectedJobber)="setSelectedJobbers($event)" >
        </report-tab>
    </div>

    <div *ngIf="checkTabStatus('timesheet')" style="width: 100%">
      <timesheet [jobbers]="timesheetJobbers"  [selectedJobber]="selectedJobber" (timesheetJoober)="timesheetJoober($event)">
      </timesheet>
  </div>
`;

export const htmlAddVacationForm = `
<div id="modal_jobber" *ngIf="opened">
  <section class="uk-modal uk-open" id="availholiday" tabindex="-1" role="dialog"
           aria-labelledby="label-availholiday" aria-hidden="false" 
           style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog" (click)="stopProp($event)">
      <a (click)="closeAddVacationModal($event)" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
        <!-- icon -->
      </a>
      <div class="uk-modal-header">
        <div id="label-availholiday">
          <h2>
            <a (click)="closeAddVacationModal($event)" class="uk-modal-close"></a>
            <span *ngIf="!vacation">{{'Days_off_inquiry'| translate}}</span> <span *ngIf="vacation">{{'Correct_Days_off'|translate}}</span> </h2>
        </div>
      </div>
      <div class="modal-content">
        <div id="vacation-alert-empty" class="uk-alert uk-alert-danger error hidden">Please enter a
          date.
        </div>
        <div id="vacation-alert-title" class="uk-alert uk-alert-danger error hidden">Please enter a
          title.
        </div>
        <div id="vacation-alert-dates" class="uk-alert uk-alert-danger error hidden">End date cannot
          be before start date.
        </div>
        <form *ngIf='!editByManager && !editByUser' [formGroup]="companyForm" class="uk-form uk-form-horizontal availholiday">
          <div class="uk-grid uk-grid-small uk-text-left">
             <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label reqField" for="sel-availholiday-comment">{{'Company'| translate}} </label>
                <div class="uk-width-small-4-10 uk-width-medium-6-10">
                  <div *ngFor="let comp of companies | mapToIterable; let i = index" >
                   <label  attr.for='{{comp.key}}'>
                      <input  formControlName='{{comp.value.company_id}}' type="checkbox" id='{{comp.key}}'>
                      <span class="checkBoxModal">{{comp.value.cname}}</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div *ngIf='editByManager' class="uk-form uk-form-horizontal availholiday">
          <div  class="uk-form-row uk-width-1-1  blockTitle">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label" for="sel-availholiday-company">{{'Name' | translate}}</label>
              <div  class=" uk-width-small-1-1 uk-width-medium-6-10  future">
                {{setName}}
              </div>
            </div>
          </div>
        </div>
        <div *ngIf='editByUser' class="uk-form uk-form-horizontal availholiday">
          <div  class="uk-form-row uk-width-1-1  blockTitle">
            <div class="uk-grid uk-grid-collapse">
              <label class="uk-form-label  reqField" for="sel-availholiday-comment">{{'Company'| translate}} </label>
              <div  class=" uk-width-small-1-1 uk-width-medium-6-10  future uk-margin-small-top">
                {{companyName}}
              </div>
            </div>
          </div>
        </div>
        
        <form [formGroup]="vacationForm" class="uk-form uk-form-horizontal availholiday" (submit)="submitVacation($event)" >
          <div class="uk-grid uk-grid-small uk-text-left">
            <!--Reason section start-->
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label reqField" for="sel-availholiday-company">{{'Reason'|translate}}</label>
                <div id="sel-availholiday-company-wrapper" class=" uk-width-small-1-1 uk-width-medium-6-10">
                  <select  formControlName="reasonControl">
                    <option value="false" disabled selected>{{'please_select' | translate}}</option>
                    <option *ngFor="let reasonItem of reasonsList" value="{{reasonItem.name}}">{{reasonItem.caption | translate}}</option>
                  </select>
                </div>
              </div>
            </div>
            <!--Reason section end-->
            
            <!--Days section start-->
            <div  class="uk-width-1-1">
              <div class="uk-width-1-1 availholiday-unit uk-margin-top">
                <label class="uk-form-label uk-width-small-4-10 reqField" for="sel-availholiday-date-start">{{'time_From' | translate}}</label>
                <input type="text" id="sel-availholiday-date-start"
                       formControlName="startDateControl"
                       [readonly]="true"
                       [ngClass]="{'uk-form-danger': showAlertFromDate }"
                       (click)="showCalendar('from')"
                       placeholder="{{'birthday_placeholder' | translate}}"
                       class="uk-width-small-4-10 uk-width-medium-6-10 selectDate future">
                <div *ngIf="calendarFromOpen" class="uk-float-right">
                    <my-date-picker
                        (selectedDate)="setFromDate($event)"
                        [valueDate]="fromDate"
                        [valueDay]="fromDay"
                        ></my-date-picker>
                </div>
                <br>
                <div *ngIf="showAlertFromDate" class="uk-width-1-1">
                   <span  class="error uk-text-danger uk-margin-large-left uk-width-1-1 uk-float-left" style="white-space: nowrap;">
                   {{'msg_err_date_is_past'|translate}}</span>
                </div>
              </div>
              <div class="uk-form-row uk-width-1-1 uk-margin-top">
                  <label class="uk-form-label reqField" for="sel-availholiday-company">{{'Shift' | translate}}</label>
                  <select   formControlName="startShiftControl"  class="uk-width-small-1-1 selectShift "
                          [ngClass]="{'uk-form-danger': !correctShifts }"> <!--(change)="checkRangeIsEmpty()" -->
                    <option value="false" disabled selected>{{'please_select' | translate}}</option>
                    <option *ngFor="let shift of shiftList" value="{{shift.name}}">{{shift.caption | translate}}</option>
                  </select>
                   <div *ngIf="alertFromShift" class="uk-width-1-1">
                     <span  class="error uk-text-danger uk-margin-large-left uk-width-1-1 uk-float-left" style="white-space: nowrap;">
                     {{'msg_err_date_is_past'|translate}}.</span>
                  </div>
              </div>
              <div class=" uk-width-1-1 availholiday-unit uk-margin-top">
                <label class="uk-form-label reqField" for="sel-availholiday-date-end">{{'time_Until' | translate}}</label>
                    <input type="text" id="sel-availholiday-date-end"
                       formControlName="endDateControl"
                       [readonly]="true"
                       [ngClass]="{'uk-form-danger': showAlertToDate }"
                       (click)="showCalendar('to')"
                       placeholder="{{'birthday_placeholder' | translate}}"
                       class="uk-width-small-4-10 uk-width-medium-6-10 selectDate ">
                <div *ngIf="calendarToOpen" class="uk-float-right">
                    <my-date-picker
                        (selectedDate)="setToDate($event)"
                        [valueDate]="toDate"
                        [valueDay]="toDay"></my-date-picker>
                </div>
                <br>
                <div *ngIf="showAlertToDate" class="uk-width-1-1">
                   <span  class="error uk-text-danger uk-margin-large-left uk-width-1-1 uk-float-left" style="white-space: nowrap;">
                   {{'msg_err_date_is_past'|translate}}</span>
                </div>
                
              </div>
              <div class="uk-form-row uk-width-1-1 uk-margin-top">
                <div class="uk-width-1-1 uk-grid uk-grid-collapse">
                  <label class="uk-form-label reqField" for="sel-availholiday-company">{{'Shift' | translate}}</label>
                  <select class="selectShift"  formControlName="endShiftControl"
                           [ngClass]="{'uk-form-danger': !correctShifts || shiftsAreSame}"> <!--(change)="checkRangeIsEmpty()"-->
                    <option value="false" disabled selected>{{'please_select' | translate}}</option>
                    <option *ngFor="let shift of shiftList" value="{{shift.name}}">{{shift.caption | translate}}</option>
                  </select>
                  <div *ngIf="!correctShifts" class="uk-width-1-1">
                     <span  class="error uk-text-danger uk-width-1-1 uk-margin-large-left uk-float-left" style="white-space: nowrap;">
                     {{'msg_err_invalid_end_before_start'|translate}}</span>
                  </div>
                  <div *ngIf="shiftsAreSame">
                    <span  class="error uk-text-danger uk-width-1-1 uk-margin-large-left uk-float-left" style="white-space: nowrap;">
                     {{'msg_err_invalid_end_same_start'|translate}}</span>      
                  </div>
                </div>
              </div>
              <div class=" uk-width-1-1 availholiday-unit uk-margin-top">
                  <label class="uk-form-label " for="sel-availholiday-date-end">{{'total_days' | translate}}</label>
                  <input formControlName="totalDaysControl"
                      [ngClass]="{'uk-form-danger': !vacationForm.controls['totalDaysControl'].valid }" type="text" class="uk-width-3-10" >
              </div>
            </div>
            
            <!--Days section end-->
            
            
            <!--Info section start-->
            <div class="uk-form-row uk-width-1-1 uk-margin-top">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" 
                  for="sel-availholiday-comment">{{'Info'| translate}}</label>
                <textarea formControlName="infoControl"
                          [readonly]='editByManager && vacation'
                          [ngClass]="{'uk-form-danger': infoInValid }"
                          class="uk-width-small-1-1 uk-width-medium-6-10" name="comments"
                          id="sel-availholiday-comment" cols="300" rows="2"
                          placeholder="{{'Info'| translate}}"></textarea>

              </div>
            </div>
            <!--Info section end-->
            
            <!--Manager message section start-->
            <div *ngIf='editByManager' class="uk-form-row uk-width-1-1 uk-margin-top">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="sel-availholiday-comment">{{'Message'| translate}}</label>
                <textarea formControlName="managerMesControl"
                          class="uk-width-small-1-1 uk-width-medium-6-10" name="comments"
                          id="sel-availholiday-comment" cols="300" rows="2"
                          placeholder="{{'Message'| translate}}"></textarea>
              </div>
            </div>
            <!--Manager message end-->

            <div *ngIf='!editByManager'  class="uk-width-1-1 uk-margin-large-top">
              <div class="uk-grid uk-grid-small">
                <div id="availholiday-ok"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                  <button [disabled]="(!(companyForm.valid && vacationForm.valid) ||
                                      (showAlertFromDate || showAlertToDate || 
                                      alertToShift || alertFromShift))"
                          id="availholiday-save"
                          (click)="submitVacation($event, 'Sent')"
                          class="uk-button uk-button-large uk-width-1-1 ok"> 
                    {{'Save' | translate}}
                  </button>
                </div>
                <div id="availholiday-delete"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                  <button *ngIf="!vacation" type="button"
                          (click)="closeAddVacationModal($event)"
                          class="uk-button uk-button-large uk-button-primary cancel buttonCancel uk-width-1-1">
                          {{'button_Cancel' | translate}}
                  </button>
                  <button *ngIf="vacation" type="button"
                          (click)="deleteVacation($event)"
                          class="uk-button uk-button-large uk-button-danger uk-width-1-1">
                          {{'button_Delete' | translate}}
                  </button>
                </div>
              </div>
            </div>
            <div *ngIf="!rangeEmpty && editByManager && vacation && vacation.statusLabel === 'Open'" 
                  class="uk-alert uk-alert-warning error invite-internal" data-uk-alert="">
              <span>{{setDatesToMessage('msg_warn_range_not_empty' | translate) }}</span>
            </div>
            <!---->
            <div *ngIf='editByManager && !vacation'  class="uk-width-1-1 uk-margin-large-top">
              <div class="uk-grid uk-grid-small">
                <div id="availholiday-ok"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                     
                  <button [disabled]="(!(vacationForm.valid) ||
                                      (showAlertFromDate || showAlertToDate 
                                      || alertToShift || alertFromShift)) || rangeChecking"
                          id="availholiday-save" 
                          (click)="submitVacation($event, 'Sent')"
                          class="uk-button uk-button-large uk-width-1-1 "> 
                    {{'Send' | translate}}
                  </button>
                </div>
                <div id="availholiday-delete"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                  <button  type="button"
                          (click)="closeAddVacationModal($event)"
                          class="uk-button uk-button-large uk-button-primary cancel buttonCancel uk-width-1-1">
                          {{'button_Cancel' | translate}}
                  </button>
                </div>
              </div>
            </div>
          <!---->

            <div *ngIf='editByManager && vacation' class="uk-width-1-1 uk-margin-large-top">
              <div class="uk-grid uk-grid-small">
                <div id="availholiday-ok"
                     class=" {{ lang == 'de'? 
                     'uk-width-1-1 uk-margin-small-bottom' : 'uk-width-1-3 uk-margin-large-bottom'}} uk-text-center ">
                  <button [disabled]="disabledAprove() && !rangeChecking"
                          (click)="submitVacation($event, 'Approve')"
                          id="availholiday-save" 
                          class="uk-button uk-button-large uk-width-1-1 ">
                    {{'Approve' | translate}}
                  </button>
                </div>

                <div id="availholiday-ok"
                     class=" uk-text-center uk-margin-large-bottom {{ lang == 'de'? 
                     'uk-width-2-4' : 'uk-width-1-3 '}} uk-text-center ">
                  <button [disabled]="(!vacationForm.valid ||
                                      (showAlertFromDate || showAlertToDate 
                                      || alertToShift || alertFromShift)) || rangeChecking"
                          (click)="submitVacation($event, 'Save')"
                          id="availholiday-save"
                          class="uk-button uk-button-large uk-width-1-1 cancel">
                    {{'Save_changes'| translate}}
                  </button>
                </div>

                <div id="availholiday-ok"
                     class=" uk-text-center uk-margin-large-bottom {{ lang == 'de'? 
                     'uk-width-2-4' : 'uk-width-1-3 '}} uk-text-center uk-margin-large-bottom">
                  <button [disabled]="(!vacationForm.valid ||
                                      (showAlertFromDate || showAlertToDate 
                                      || alertToShift || alertFromShift)) || rangeChecking" 
                          (click)="submitVacation($event, 'Decline')"
                          id="availholiday-save" 
                          class="uk-button uk-button-large uk-button-danger uk-width-1-1 ">
                    {{'Decline' | translate}}
                  </button>
                </div>

              </div>
            </div>
            
          </div>
          <!-- UK-GRID -->
          </form>
          
          
          <div *ngIf="editByManager && showNavigation" class="uk-grid">
            <div class="uk-width-1-2">
              <a *ngIf='index > 0' 
                 class="uk-width-1-1 cancel uk-text-left" 
                 (click)="onModalNavigationClick($event, -1)">
                <i class="back"></i> <span>{{'Previuos'|translate}}</span> 
              </a>
            </div>
            <div class="uk-width-1-2">
              <a *ngIf='index < amount' 
                 class="uk-width-1-1 cancel uk-text-right uk-float-right" 
                 (click)="onModalNavigationClick($event, 1)">
                <span>{{'Next'|translate}}</span> <i class="next"></i>
              </a>
            </div>
          </div>
         

      </div>
      <!-- ws-validate -->
    </div>
    <!-- modal-content -->
    <!-- modal-inner -->
  </section>
</div>
`;

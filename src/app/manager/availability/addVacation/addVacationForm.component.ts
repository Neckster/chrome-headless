import { Component, Input, Output, EventEmitter  }  from '@angular/core';
import { htmlAddVacationForm }                      from './addVacationForm.htm.ts';

import { AppStoreAction }                           from '../../../store/action';
import { select }                                   from 'ng2-redux/lib/index';
import { FormBuilder, FormControl,
  FormGroup, Validators }                     from '@angular/forms';
import { Cache }                                    from '../../../services/shared/cache.service';
import { IUICaption }                               from '../../../lib/interfaces/shared/ui';
import { JobService }                               from '../../../services/shared/JobService';
import { DatePikerComponent }                       from '../../date-picker/date-picker';
import * as moment                                  from 'moment';
import * as R                                       from 'ramda';
import {
    DayOffFactory,
    DayOff,
  DAY_OFF_STATUS_APPROVE,
  DAY_OFF_STATUS_DECLINE,
  DAY_OFF_STATUS_UPDATE,
    DAY_OFF_STATUS_SEND
}                                                   from '../../../models/DayOff';
import { DayOffService }                            from '../../../services/day_off.service';
import { Observable }                               from 'rxjs';
import { ScheduledService }                         from '../../../services/scheduled.service';


const shiftList: IUICaption[] = [
  {name: '0', caption: 'shift_0'},
  {name: '1', caption: 'shift_1'},
  {name: '2', caption: 'shift_2'},
  {name: '3', caption: 'shift_3'}
];
const reasonsJobber: IUICaption[] = [
  {name: 'holidays', caption: 'Holidays_reason'},
  {name: 'services', caption: 'Public_and_social_reason'},
  {name: 'overtime', caption: 'overtime_compensation'},
  {name: 'sickness', caption: 'Sickness_reason'},
  {name: 'education', caption: 'Education_reason'},
  {name: 'other', caption: 'Other_reason'}
];

const  infoRequired: boolean = false;

const sendObjProfile = {
  'company': [],
  'reason': '',
  'from_week': 0,
  'from_weekday': 0,
  'from_shift': 0,
  'to_week': 0,
  'to_weekday': 0,
  'to_shift': 0,
  'message': '',
  'total_days': '',
  'status': DAY_OFF_STATUS_SEND
};
const sendObjStaff = {
  'webuser_id': '',
  'company_id' : '',
  'day_off_id': undefined,
  'reason': '',
  'from_week': 0,
  'from_weekday': 0,
  'from_shift': 0,
  'to_week': 0,
  'to_weekday': 0,
  'to_shift': 0,
  'message': '',
  'manager_message': '',
  'total_days': '',
  'status': DAY_OFF_STATUS_SEND
};


@Component({
  selector: 'add-vacation',
  providers: [ DayOffService, DayOffFactory],
  directives: [DatePikerComponent],
  pipes: [ ],
  styles: [ `
     .selectDate { width: calc(30% - 4px) }
     .selectShift { width: calc(35% - 4px) }
     @media screen and (max-width: 480px){
      .selectDate, .selectShift { margin-top: 1.5rem; width: 100%; }
     }
    .checkBoxModal{
     font-size: 1.6rem;
     font-weight: 500;
     font-family: 'jclight', 'HelveticaNeue-Light', sans-serif;
       }
     .checkBoxModal:before{
       font-size: 25px;
       position:relative;
       top: 3px;
       font-weight: 500;
       margin-right: 20px;
     }
     .future{
       line-height: 40px;
     }
    
   ` ],
  template: htmlAddVacationForm
})

export class AddVacationComponent {

  @Input() opened;
  @Input() vacation: DayOff;

  @Input() editByManager: boolean;
  @Input() editByUser: boolean;
  @Input() createByUser: boolean;

  @Input() userId;
  @Input() userName;

  @Input() stop;

  @Input() showNavigation: boolean;
  @Input() amount: any;
  @Input() index: any;

  @Output() closeEmitter = new EventEmitter();
  @select() info$: any;
  @select() lang$: any;
  @select() currentUnit$: any;

  private info: any;
  private reasonsList: any;
  private shiftList: any;
  private hideScrollBar = this.jobService.hideScrollBar;
  private showDates: boolean = true;
  private showHalfDates: boolean = false;
  private fromDate = moment();
  private fromDay: any = '';
  private toDate = moment();
  private toDay: any = '';

  private fromShiftDisabled: boolean = true;
  private toDateDisabled: boolean = true;
  private toShiftDisabled: boolean = true;

  private showAlertFromDate: boolean = false;
  private showAlertToDate: boolean = false;
  private showPlusHalfDay: boolean = false;
  private calendarFromOpen: boolean = false;
  private calendarToOpen: boolean = false;
  private halfDayCounter: number = 1;
  private halfDayInfo: any = [];
  private unsubscribe = [];
  private sendObj: any;
  private companyForm: any;
  private vacationForm: any;
  private odlVacation: any;
  private companies: any; // information about company
  private approveActive: boolean = true;
  private infoValid: boolean = false;
  private alertFromShift: boolean = false;
  private alertToShift: boolean = false;
  private companyName: string;
  private unitStaff: any;
  private setName: string;
  private lang: string;
  private rangeEmpty: boolean;
  private rangeChecking: boolean;
  private correctShifts: boolean = true;
  private infoInValid: boolean = false;
  private shiftsAreSame: boolean = false;

  constructor(
    public jobService: JobService,
    public fb: FormBuilder,
    public cache: Cache,
    public action: AppStoreAction,
    public dayOffService: DayOffService,
    public dayOffFactory: DayOffFactory,
    public scheduledService: ScheduledService
  ) {

    this.reasonsList = reasonsJobber;



    this.shiftList = shiftList;

    this.info$.subscribe(info => {
      this.info = info;
      if (this.info  && this.info.companies) {
        this.companies = this.info.companies;
      }
    });
    this.unsubscribe.push(this.currentUnit$.subscribe(unit => {
      if (unit.cname && unit.staff) {
        this.unitStaff = unit.staff;
      }
    }));
    this.unsubscribe.push(this.lang$.subscribe(data => {
      this.lang = data || this.cache.get('lang') || 'en';
    }));

  }

  /* tslint:disable:no-string-literal */

  ngOnInit() {
    if (this.unitStaff) {
      this.setName = this.unitStaff[this.userId] ?
        this.unitStaff[this.userId].uname : this.userName;
    }

    if (this.editByManager) {
      this.sendObj = Object.assign({}, sendObjStaff);
    }else if (this.editByUser && this.vacation
          && this.companies) {
      Object.keys(this.companies).map( name => {
        if (+name === +this.vacation.companyId ) {
          this.companyName = this.companies[name].cname;
        }
      });
      this.sendObj = Object.assign({}, sendObjProfile);
    }else {
      this.sendObj = Object.assign({}, sendObjProfile);
    }
    if (this.companies) {
      this.companyForm = this.buildCompForm(this.fb, this.companies);
      this.vacationForm = this.builVacFrom(this.fb);
    }
    this.odlVacation = this.vacationForm.value;
    this.hideScrollBar();
    this.vacationForm.valueChanges.subscribe(el => {
      this.approveActive = R.equals(this.odlVacation, this.vacationForm.value);
    });

    this.rangeChecking = false;
    this.rangeEmpty = true;

    if (this.vacation) {
      this.fromDate = this.vacation.fromDate;
      this.fromDay = moment(this.vacation.fromDate);
      this.toDate = this.vacation.toDate;
      this.toDay = moment(this.vacation.toDate);
      this.checkRangeIsEmpty();
    }
  }

  buildCompForm(fb, props): FormGroup {
    let group: any = {};

    for (let key in props) {
      if (props.hasOwnProperty(key)) {
        group[props[key].company_id.toString()] = [false];
      }
    }

    if (this.vacation && !this.editByManager) {
        if (typeof this.vacation.companyId === 'number') {
            group[this.vacation.companyId] = [true];
        }else {
          /*this.vacation.companyId.forEach( el => {
            group[el] = [true];
          });*/
        }
    }

    return fb.group(group, {
      validator: (formGroup) => {
        let res: any;
        R.map( el => {
          if (el) res =  el;
        }, formGroup.value );
        return res ? undefined : formGroup;
      }
    });
  }
  hasExclamationMark(input: FormControl) {

  }

  builVacFrom(fb): FormGroup {

    let checkedSelect = (input: FormControl) => {
      const selectIs = input.value !== 'false';
      return selectIs ? undefined : { needSelect: true };
    };

    let prop = {
      reasonControl: [this.vacation && this.vacation.reason ?
                      this.vacation.reason.toString() : 'false',
                      checkedSelect],
      startDateControl: [this.vacation && this.vacation.fromDate ?
                      this.vacation.fromDate.format('DD.MM.YYYY') : '',
                      Validators.required],
      startShiftControl: [this.vacation && this.vacation.fromShift >= 0 ?
                      this.vacation.fromShift.toString() : 'false',
                      checkedSelect ],
      endDateControl: [this.vacation && this.vacation.toDate ?
                      this.vacation.toDate.format('DD.MM.YYYY') : '',
                      Validators.required],
      endShiftControl: [this.vacation && this.vacation.toShift >= 0 ?
                      this.vacation.toShift.toString() : 'false',
                      checkedSelect],
      totalDaysControl: [this.vacation && this.vacation['totalDays'] ?
                      this.vacation['totalDays'] : '',
                      Validators.pattern('^[0-9]{1,3}([.][0-9]{1,2})?$')],
      infoControl: [this.vacation && this.vacation.message ?
                    this.vacation.message : '', Validators.minLength(2)],
      managerMesControl: [this.vacation && this.vacation.managerMessage
          ? this.vacation.managerMessage : '']
    };

    return fb.group(prop, {
      validator: (formGroup) => {
        if (this.rangeChecking && !this.editByUser) {
          return undefined;
        }
        let value = formGroup.value ,
            ctr  = formGroup.controls,
            infoInValid = true,
            startDate = ctr.startDateControl.valid,
            startDateValue = value.startDateControl,
            endDate = ctr.endDateControl.valid,
            endDateValue = value.endDateControl,
            startSh = ctr.startShiftControl.valid,
            startShValue = value.startShiftControl,
            endSh = ctr.endShiftControl.valid,
            endShValue = value.endShiftControl;

        if (startDate && endDate && startSh && endSh
            && startDateValue === endDateValue) {
          this.correctShifts = +startShValue < +endShValue ?
              true : +startShValue === +endShValue ? true : false;
          this.shiftsAreSame = +startShValue === +endShValue;
          this.checkRangeIsEmpty();
        }else {
          this.correctShifts = true;
        }

        if (value['reasonControl'] === 'other') {
            this.infoInValid = value['infoControl'].length ?
              false : true;
          if (this.editByManager && this.vacation) {
            this.infoInValid = false;
          }
        }else {
          this.infoInValid = false;
        }
        return !this.infoInValid && this.correctShifts && !this.shiftsAreSame ?
          undefined : formGroup;
      }
    });
  }

  // Choose days block
  // set vacation between two dates
  showCalendar(type) {
    if (type === 'from') {
      this.calendarFromOpen = true;
    }else {
      this.calendarToOpen = true;
    }
  }

  setFromDate(e) {
    if (e.action) {
      this.fromDay = e.action;
      let res = e.action.format('DD.MM.YYYY');
      this.vacationForm.patchValue({'startDateControl':  res});
      // valid on past
      if (!this.editByManager) {
        let now = moment(), checkDate = moment(this.fromDay);
        this.showAlertFromDate = now.isAfter(checkDate);
      }
      if (this.toDay) {
        let now = moment(this.fromDay), checkDate = moment(this.toDay);
        this.showAlertToDate =  now.isAfter(checkDate) ? true : false;
      }
      if (this.showAlertToDate) {
        this.shiftsAreSame = false;
        this.correctShifts = true;
      }
      this.checkRangeIsEmpty();
    }
    this.calendarFromOpen = false;
    // calc total hours
    // this.calcTotalDays();
  }

  setToDate(e) {
    if (e.action) {
      this.toDay = e.action;
      let res = e.action.format('DD.MM.YYYY');
      this.vacationForm.patchValue({'endDateControl':  res});
      // valid on past
      if (!this.editByManager) {
        let now = moment(), checkDate = moment(this.toDay);
        this.showAlertToDate = now.isAfter(checkDate);
      }
      if (this.fromDay) {
        let now = moment(this.fromDay), checkDate = moment(this.toDay);
        this.showAlertToDate =  now.isAfter(checkDate);
      }
      if (this.showAlertToDate) {
        this.shiftsAreSame = false;
        this.correctShifts = true;
      }
      this.checkRangeIsEmpty();
    }
    this.calendarToOpen = false;
    // this.calcTotalDays();
  }

  checkRangeIsEmpty() {
    this.rangeChecking = true;
    if (this.fromDay && this.toDay) {
      let toShift = +this.vacationForm.value['endShiftControl'] || 3;
      let fromShift = +this.vacationForm.value['startShiftControl'] || 0;

      this.scheduledService.checkRange(
        this.userId,
        this.fromDay.format('GGWW'),
        this.toDay.format('GGWW'),
        this.fromDay.format('E'),
        this.toDay.format('E'),
        fromShift,
        toShift,
        this.info.manager ? this.info.manager.selected.company_id : undefined
      ).subscribe((status) => {
        this.rangeEmpty = status.empty;
        this.rangeChecking = false;
      }, () => {
        this.rangeChecking = false;
      });
    }
  }

  /*calcTotalDays() {
   let from = this.vacationForm.value['from_week'],
   to = this.vacationForm.value['endDateControl'];

   if (!this.showAlertToDate && !this.showAlertFromDate &&
   from && to && from.length && to.length) {
   let start = this.fromDay, end = this.toDay,
   res = end.diff(start, 'days');
   this.vacationForm.patchValue({'totalDaysControl': res});
   }else {
   this.vacationForm.patchValue({'totalDaysControl': ''});
   }
   }*/

  submitVacation(e, status?) {
    e.preventDefault();
    e.stopPropagation();

    if (this.rangeChecking && !this.editByUser
        && !this.createByUser) {
      return false;
    }

    let sendObj = this.sendObj;
    sendObj.from_weekday = this.fromDay ?
      +this.fromDay.format('E') : this.vacation.fromWeekday;
    sendObj.from_week = this.fromDay ?
        +this.fromDay.format('GGWW') : this.vacation.fromWeek;

    sendObj.to_weekday = this.toDay ?
      +moment(this.toDay).format('E') : +this.vacation.toWeekday;
    sendObj.to_week = this.toDay ?
                  +this.toDay.format('GGWW') : +this.vacation.toWeek;

    sendObj.reason = this.vacationForm.value['reasonControl'];
    sendObj.from_shift = +this.vacationForm.value['startShiftControl'];
    sendObj.to_shift = +this.vacationForm.value['endShiftControl'];
    sendObj['total_days'] = +this.vacationForm.value['totalDaysControl'];
    sendObj.message = this.vacationForm.value['infoControl'];

    if (this.vacation) {
      sendObj.day_off_id = this.vacation.dayOffId;
    }else if (!this.editByManager) {
      sendObj.day_off_id = undefined;
    }

    if (this.editByManager) {
      sendObj.webuser_id = this.userId;
      sendObj.company_id = this.vacation ?
              this.vacation.companyId : this.info.manager.selected.company_id;
      sendObj['manager_message'] = this.vacationForm.value['managerMesControl'];
      switch (status) {
        case 'Decline':
          sendObj.status =  DAY_OFF_STATUS_DECLINE;
              break;
        case 'Approve':
          sendObj.status =  DAY_OFF_STATUS_APPROVE;
          break;
        case 'Save':
          sendObj.status =  DAY_OFF_STATUS_UPDATE;
              break;
        default:
          sendObj.status =  DAY_OFF_STATUS_SEND;
      }
    }else if (!this.editByManager) {
      sendObj.company_id = [].concat(this.checkedCompany());
    }
    let dayOffs = this.dayOffFactory.createDayOff(sendObj);
    if (dayOffs.length) {
      let subscriptions = R.map(dayOff => this.dayOffService.saveDayOff(dayOff), dayOffs);
      Observable.forkJoin(subscriptions).subscribe(v => {

        this.action.fetchGetInfo();
      });
    } else {
      this.dayOffService.saveDayOff(dayOffs as DayOff).subscribe(v => {

        this.action.fetchGetInfo();
      });
    }
    this.closeAddVacationModal(e);

    // this must be api to create or save vacations !

  }

  setDatesToMessage(message) {
    if (this.fromDay && this.toDay) {
      return message.replace('${dates}',
        this.fromDay.format('DD.MM') + ' - ' + this.toDay.format('DD.MM'));
    }
    return message.replace('${dates}', '');
  }

  checkedCompany() {
    // TODO: refactoring this iteration
    let comp = this.companyForm.value,
      res = [];
    for (let key in comp) {
      if ( comp.hasOwnProperty(key)) {
        if (comp[key] && this.companies[key]) {
          res.push(key);
        }
      }
    }
    return res;
  }


  // End
  closeAddVacationModal(e) {
    // console.log(e);
    this.closeEmitter.emit(e);
    this.deleteAllControls();
    this.stopProp(e);
  }

  deleteAllControls() {
    let res  = {};
    Object.keys(this.vacationForm.value).map( name => {
      name === 'reasonControl' ?
        res[name] = 'false' :
        res[name] = '';
    });
    this.vacationForm.patchValue(res);
  }

  deleteVacation(e) {
    this.dayOffService.deleteDayOff(this.vacation.dayOffId).subscribe(v => {
      this.closeAddVacationModal(e);
      this.action.fetchGetInfo();
    });
  }

  disabledAprove() {
    return !(this.approveActive && !this.vacation.update && !this.vacation.approve
        && !this.vacation.decline && this.vacationForm.valid) ||
      (this.showAlertFromDate || this.showAlertToDate);
  }

  stopProp(e) {
   if (!this.stop) {
     if (e.stopPropagation) {
       e.stopPropagation();
     }
     if (e.preventDefault) {
       e.preventDefault();
     }
   }
  }


  ngOnDestroy() {
    this.hideScrollBar();
  }

  onModalNavigationClick(event, diff: number) {
    this.stopProp(event);
    // this.action.openVacationModal({diff, userId: this.userId});
    this.closeAddVacationModal(diff);
  }
  /* tslint:disable:no-string-literal */
}



import { Component, Input, Output, EventEmitter  }  from '@angular/core';

import { AppStoreAction }                           from '../../../../store/action';
import { select }                                   from 'ng2-redux/lib/index';
import { FormBuilder, Validators }                  from '@angular/common';
import { JobService }                               from '../../../../services/shared/JobService';
import { FormGroup, FormControl }                   from '@angular/forms';
import { DatePikerComponent }                       from '../../../date-picker/date-picker';
import * as moment                                  from 'moment';



@Component({
  selector: 'half-day',
  providers: [ ],
  directives: [DatePikerComponent],
  pipes: [ ],
  styles: [`
          .clearFieldCroos:after{
            position: relative;
            top: 10px;
            color: black;
          }
          .deleteLink{
            position: relative;
            top: 8px;
          }
              `],
  template: `
          <form [formGroup]="halfDayForm">
                 
            <div class=" uk-width-1-1 availholiday-unit uk-margin-small-top ">
                <label class="uk-form-label" for="sel-availholiday-date-end">
                  {{'Half day on '}}
                </label>

                <input class="uk-width-small-1-1 uk-width-medium-3-10"
                         formControlName="dateControl"
                         (click)="showCalendar()"
                         placeholder="{{'birthday_placeholder' | translate}}"
                         type="text" [readonly]="true">
                <i *ngIf='!mainIndex' 
                   class="uk-close clearFieldCroos"
                   (click)="clearDate($event)" ></i>
                <span *ngIf='mainIndex' class="deleteLink" (click)="deleteDate()">Delete</span>
                <div *ngIf="calendarOpen" class="uk-float-right">
                      <my-date-picker
                          (selectedDate)="setFromDate($event)"
                          [valueDate]="fromDate"
                          [valueDay]="fromDay"></my-date-picker>
                </div>
            </div>
            <div *ngIf='showCheckBox' class=" uk-width-1-1 uk-margin-top" >
                 <label class="uk-form-label" for="sel-availholiday-date-end">
                  {{'choose_half_day'| translate}}
                </label>
                <div class="uk-width-small-1-1 uk-width-medium-8-10 ">
                  <label class="halfRadio uk-margin-small-top">
                    <input  value="0"
                            formControlName="halfRadio"
                            name="halfRadio"
                            type="radio"
                            id="halfRadioFirst" />
                    <span class="checkBoxModal">{{ 'First part of day' }}</span>
                  </label>
                  <label class="halfRadio uk-margin-small-top">
                    <input  value="1"
                            name="halfRadio"
                            formControlName="halfRadio"
                            type="radio"
                            id="halfRadioSecond" />
                    <span class="checkBoxModal">{{ 'Second part of day' }}</span>
                  </label>
                </div>
            </div>
            
          </form>
    `
})

export class HalfDayComponent {

  @select() info$: any;

  @Input() mainIndex;
  @Input() isManager;
  @Input() setDayIn;

  @Input() latestDate;
  @Output() deleteThis  = new EventEmitter();
  @Output() setDate  = new EventEmitter();

  private fromDate = moment();
  private fromDay = moment();
  private showAlertFromDate: boolean = false;
  private showCheckBox: boolean = false;

  // UI controls
  private halfDayForm = new FormGroup({
    dateControl: new FormControl(),
    halfRadio: new FormControl('0')
  });

  private calendarOpen: boolean = false;
  /* tslint:disable:no-string-literal */
  constructor(
    public jobService: JobService,
    private fb: FormBuilder,
    public action: AppStoreAction) {

    this.info$.subscribe(info => {
    });
    this.halfDayForm.valueChanges.subscribe(val => {
      this.setDate.emit({
        action: val
      });
      this.showCheckBox =
        val['dateControl'] ? true : false;
    });
  }
  /* tslint:disable:no-string-literal */


  ngOnInit() {
/*    if (Object.keys(this.setDayIn).length) {
      debugger;
      this.halfDayForm.patchValue(this.setDayIn);
    }*/
  }

  ngOnDestroy() {
  }


  showCalendar() {
    // debugger;
    this.calendarOpen = true;
  }
  // From days
  setFromDate(e) {
    // debugger;
    if (e.action) {
      this.fromDay = e.action;
      let res = e.action.format('DD.MM.YYYY');
      this.halfDayForm.patchValue({'dateControl':  res});
    }
    // debugger;
    this.calendarOpen = false;
  }

  clearDate(e) {
    e.stopPropagation();
    this.halfDayForm.patchValue({'dateControl':  ''});
  }

  deleteDate() {
    this.deleteThis.emit({
      action: this.mainIndex
    });
  }

}



/**
 * Created by user on 13.05.16.
 */
import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { htmlEditVacationForm } from './editVacationForm.htm.ts';
import { JobService } from '../../../services/shared/JobService';


@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'edit-vacation',  // <controlling></controlling>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [ ],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [ ],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [ ],
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: htmlEditVacationForm
})

export class EditVacationComponent {
  // Set our default values
  @Input() opened;
  @Output() closeEmitter = new EventEmitter();
  public hideScrollBar = this.jobService.hideScrollBar;
  // TypeScript public modifiers
  constructor( public jobService: JobService) {
  }

  saveVacation( e ) {
    e.preventDefault();
  }

  deleteVacation( e ) {
    e.preventDefault();
  }

  closeEditVacationModal( e ) {
    e.preventDefault();
    this.closeEmitter.emit();
    this.hideScrollBar();
  }
}

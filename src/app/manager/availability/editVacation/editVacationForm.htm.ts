export const htmlEditVacationForm = `
<div id="modal_jobber" *ngIf="opened">
  <section class="uk-modal uk-open" id="availholiday" tabindex="-1" role="dialog"
           aria-labelledby="label-availholiday" aria-hidden="false"
           style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <a (click)="closeEditVacationModal( $event )" class="uk-modal-close uk-close"  title ='{{ "dismiss" | translate }}' data-close="dismiss" data-dismiss="modal">
        <!-- icon -->
      </a>
      <div class="uk-modal-header">
        <div id="label-availholiday">
          <h2>
            <a (click)="closeEditVacationModal( $event )" class="uk-modal-close">
              <!-- mobile-back-button -->
            </a>Edit holidays</h2>
        </div>
      </div>
      <div class="modal-content">
        <div id="vacation-alert-empty" class="uk-alert uk-alert-danger error hidden">Please enter a
          date.
        </div>
        <div id="vacation-alert-title" class="uk-alert uk-alert-danger error hidden">Please enter a
          title.
        </div>
        <div id="vacation-alert-dates" class="uk-alert uk-alert-danger error hidden">End date cannot
          be before start date.
        </div>
        <form class="uk-form uk-form-horizontal availholiday">
          <div class="uk-grid uk-grid-small uk-text-left">
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="sel-availholiday-company">Company</label>
                <div id="sel-availholiday-company-wrapper"
                     class="select-wrapper uk-width-small-1-1 uk-width-medium-6-10">
                  <select id="sel-availholiday-company" class="jc-nname-list select">
                    <option id="option-choose" disabled="">please select</option>
                    <option id="option-all-f" value="0">All</option>
                    <option id="option-all-f" value="150024">Bar und Lounge Kette AG</option>
                    <option id="option-all-f" value="150023">Club AG</option>
                    <option id="option-all-f" value="150049">Company</option>
                    <option id="option-all-f" value="150067">Company</option>
                    <option id="option-all-f" value="150066">Company</option>
                    <option id="option-all-f" value="150065">Company</option>
                    <option id="option-all-f" value="150064">Company</option>
                    <option id="option-all-f" value="150063">Company</option>
                    <option id="option-all-f" value="150062">Company</option>
                    <option id="option-all-f" value="150060">Company</option>
                    <option id="option-all-f" value="150059">Company</option>
                    <option id="option-all-f" value="150070">Company</option>
                    <option id="option-all-f" value="150027">Company</option>
                    <option id="option-all-f" value="150071">Company</option>
                    <option id="option-all-f" value="150068">Company</option>
                    <option id="option-all-f" value="150028">Company</option>
                    <option id="option-all-f" value="150029">Company</option>
                    <option id="option-all-f" value="150030">Company</option>
                    <option id="option-all-f" value="150058">Company</option>
                    <option id="option-all-f" value="150057">Company</option>
                    <option id="option-all-f" value="150056">Company</option>
                    <option id="option-all-f" value="150054">Company</option>
                    <option id="option-all-f" value="150053">Company</option>
                    <option id="option-all-f" value="150052">Company</option>
                    <option id="option-all-f" value="150051">Company</option>
                    <option id="option-all-f" value="150050">Company</option>
                    <option id="option-all-f" value="150069">Company</option>
                    <option id="option-all-f" value="150048">Company</option>
                    <option id="option-all-f" value="150047">Company</option>
                    <option id="option-all-f" value="150031">Company</option>
                    <option id="option-all-f" value="150045">Company</option>
                    <option id="option-all-f" value="150044">Company</option>
                    <option id="option-all-f" value="150043">Company</option>
                    <option id="option-all-f" value="150042">Company</option>
                    <option id="option-all-f" value="150041">Company</option>
                    <option id="option-all-f" value="150040">Company</option>
                    <option id="option-all-f" value="150038">Company</option>
                    <option id="option-all-f" value="150037">Company</option>
                    <option id="option-all-f" value="150036">Company</option>
                    <option id="option-all-f" value="150035">Company</option>
                    <option id="option-all-f" value="150034">Company</option>
                    <option id="option-all-f" value="150033">Company</option>
                    <option id="option-all-f" value="150032">Company</option>
                    <option id="option-all-f" value="131075">Event / Festival</option>
                    <option id="option-all-f" value="131072">Gastronomie Gruppe AG</option>
                    <option id="option-all-f" value="150026">Hotel AG</option>
                    <option id="option-all-f" value="150046">Jubbr</option>
                    <option id="option-all-f" value="150039">Jubbr AG</option>
                    <option id="option-all-f" value="150021">Jubbr AG</option>
                    <option id="option-all-f" value="131074">Openair Frauenfeld 2015</option>
                    <option id="option-all-f" value="150015">Restaurant AG</option>
                    <option id="option-all-f" value="150055">Test</option>
                    <option id="option-all-f" value="150061">Test Firma</option>
                  </select>
                  <input id="holiday-id" type="hidden" value="vacation-131072-150023-31.05.2016">
                </div>
              </div>
            </div>
            <div class="uk-width-1-1 availholiday-unit uk-margin-top">
              <label class="uk-form-label" for="sel-availholiday-date-start">from</label>
              <!-- FIXME: use type=date replacement -->
              <input type="text" id="sel-availholiday-date-start"
                     class="uk-width-small-1-1 uk-width-medium-6-10 selectDate future"
                     data-uk-datepicker="">
              <span class="error hidden">{{'msg_err_date_is_past'|translate}}.</span>
            </div>
            <div class=" uk-width-1-1 availholiday-unit uk-margin-small-top">
              <label class="uk-form-label" for="sel-availholiday-date-end">until</label>
              <!-- FIXME: use type=date replacement -->
              <input type="text" id="sel-availholiday-date-end"
                     class="uk-width-small-1-1 uk-width-medium-6-10 selectDate future"
                     data-uk-datepicker="">
              <span class="error hidden">{{'msg_err_date_is_past'|translate}}</span>
            </div>
            <div class="uk-width-1-1 uk-form uk-form-stacked uk-margin-top">
              <div class=" uk-width-1-1">
                <label class="uk-form-label required" for="sel-availholiday-name">Title</label>
                <input class="uk-width-small-1-1 uk-width-medium-6-10" type="text"
                       id="sel-availholiday-name">
                <span class="error hidden">{{'msg_err_date_is_past'|translate}}</span>
              </div>
            </div>
            <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid uk-grid-collapse">
                <label class="uk-form-label" for="sel-availholiday-comment">Comments</label>
                <textarea class="uk-width-small-1-1 uk-width-medium-6-10" name="comments"
                          id="sel-availholiday-comment" cols="300" rows="2"
                          placeholder="Info"></textarea>
              </div>
            </div>
            <div class="uk-width-1-1 uk-margin-large-top">
              <div class="uk-grid uk-grid-small">
                <div id="availholiday-ok"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                  <button (click)=' saveVacation( $event )'
                    id="availholiday-save" class="uk-button uk-button-large uk-width-1-1 ok">
                    {{'Save' | translate}}
                  </button>
                </div>
                <div id="availholiday-delete"
                     class="uk-width-1-2 uk-text-center uk-margin-large-bottom">
                  <button type="button"
                          (click)=' deleteVacation( $event )'
                          class="uk-button uk-button-large uk-button-danger uk-width-1-1">
                          {{'button_Delete' | translate}}
                  </button>
                </div>
              </div>
            </div>
          </div>
          <!-- UK-GRID --></form>


      </div>
      <!-- ws-validate -->
    </div>
    <!-- modal-content -->
    <!-- modal-inner -->
  </section>
</div>
`;

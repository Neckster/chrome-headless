export const availabilityTemplate: string = `

<!--<edit-vacation [opened]="editVacationOpened"
               (closeEmitter)="editVacationOpened = false"></edit-vacation>-->



<div class="page availability jobberview uk-clearfix">

  <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
    <div class="uk-width-small-1-1 uk-width-medium-9-10 uk-width-xlarge-7-10 uk-container-center">
      <h1 *ngIf="!secondStep">{{'Availability' | translate}}</h1>

             <div *ngIf="!secondStep && addVacationOpened">
               <add-vacation [opened]="addVacationOpened" [stop]="true" [createByUser]='true'
                (closeEmitter)="addVacationOpened = false"></add-vacation>
             </div>

      <ul *ngIf='!secondStep' id="availability-subnav" class="uk-tab uk-tab-responsive">
     
        <li (click)="switchTab('availability')" id="availability-subnav-availability" [ngClass]="{'uk-active': checkTabStatus('availability')}">
          <a class="current">{{'Availability' | translate}}</a>
        </li>
     
        <li (click)="switchTab('vacation')" id="availability-subnav-vacation" [ngClass]="{'uk-active': checkTabStatus('vacation')}">
          <a class="current">{{'Days_off' | translate}}</a>
        </li>
        <a (click)="addVacationOpened = !addVacationOpened" id="add-vacation" *ngIf="checkTabStatus('vacation')"
             class="jc-click-availholiday uk-text-right addvacation">+ {{'new_request'|translate}}</a>
        <li class="uk-tab-responsive uk-active uk-hidden" aria-haspopup="true"
            aria-expanded="false"><a>{{'Availability' | translate}}</a>
          <div class="uk-dropdown uk-dropdown-small">
            <ul class="uk-nav uk-nav-dropdown"></ul>
          </div>
        </li>
        
      </ul>
      
      <ul id="availability-subtab" class="uk-switcher uk-margin">
        <li id="availability-sub-availability"
            *ngIf="checkTabStatus('availability')"
            [ngClass]="{'uk-active': checkTabStatus('availability')}">
          <div
               class="uk-grid uk-grid-collapse jc-template-replace-switch-table">
            <div class="uk-width-1-10 uk-grid-collapse uk-margin-bottom"></div>
            <div class="uk-width-9-10 uk-grid-collapse uk-margin-bottom">
              <div *ngIf="info.sys" class="uk-grid uk-grid-collapse uk-margin-bottom shifts">

                <div class="uk-width-2-10 uk-grid-collapse uk-container-center shift">
                  {{'shift_0' | translate}}<br>{{info.sys.shift[0].from}}&nbsp;– {{info.sys.shift[0].to}}
                </div>
                <div class="uk-width-2-10 uk-grid-collapse uk-container-center shift">
                  {{'shift_1' | translate}}<br>{{info.sys.shift[1].from}}&nbsp;– {{info.sys.shift[1].to}}
                </div>
                <div class="uk-width-2-10 uk-grid-collapse uk-container-center shift">
                  {{'shift_2' | translate}}<br>{{info.sys.shift[2].from}}&nbsp;– {{info.sys.shift[2].to}}
                </div>
                <div class="uk-width-2-10 uk-grid-collapse uk-container-center shift">
                  {{'shift_3' | translate}}<br>{{info.sys.shift[3].from}}&nbsp;– {{info.sys.shift[3].to}}
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_1_short_Monday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-10"
                         (change)="eventSwitch($event, 1, 0)" [checked]="checkSwitcher(0)">
                  <label for="avail-10">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch user-success" type="checkbox" id="avail-11"
                         (change)="eventSwitch($event, 1, 1)" [checked]="checkSwitcher(7)">
                  <label for="avail-11">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch user-success" type="checkbox" id="avail-12"
                         (change)="eventSwitch($event, 1, 2)" [checked]="checkSwitcher(14)">
                  <label for="avail-12">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-13"
                         (change)="eventSwitch($event, 1, 3)" [checked]="checkSwitcher(21)">
                  <label for="avail-13">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_2_short_Tuesday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-20"
                         (change)="eventSwitch($event, 2, 0)" [checked]="checkSwitcher(1)">
                  <label for="avail-20">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-21"
                         (change)="eventSwitch($event, 2, 1)" [checked]="checkSwitcher(8)">
                  <label for="avail-21">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-22"
                         (change)="eventSwitch($event, 2, 2)" [checked]="checkSwitcher(15)">
                  <label for="avail-22">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-23"
                         (change)="eventSwitch($event, 2, 3)" [checked]="checkSwitcher(22)">
                  <label for="avail-23">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_3_short_Wednesday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-30"
                         (change)="eventSwitch($event, 3, 0)" [checked]="checkSwitcher(2)">
                  <label for="avail-30">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-31"
                         (change)="eventSwitch($event, 3, 1)" [checked]="checkSwitcher(9)">
                  <label for="avail-31">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-32"
                         (change)="eventSwitch($event, 3, 2)" [checked]="checkSwitcher(16)">
                  <label for="avail-32">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-33"
                         (change)="eventSwitch($event, 3, 3)" [checked]="checkSwitcher(23)">
                  <label for="avail-33">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_4_short_Thursday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-40"
                         (change)="eventSwitch($event, 4, 0)" [checked]="checkSwitcher(3)">
                  <label for="avail-40">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-41"
                         (change)="eventSwitch($event, 4, 1)" [checked]="checkSwitcher(10)">
                  <label for="avail-41">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-42"
                         (change)="eventSwitch($event, 4, 2)" [checked]="checkSwitcher(17)">
                  <label for="avail-42">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-43"
                         (change)="eventSwitch($event, 4, 3)" [checked]="checkSwitcher(24)">
                  <label for="avail-43">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_5_short_Friday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-50"
                         (change)="eventSwitch($event, 5, 0)" [checked]="checkSwitcher(4)">
                  <label for="avail-50">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-51"
                         (change)="eventSwitch($event, 5, 1)" [checked]="checkSwitcher(11)">
                  <label for="avail-51">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-52"
                         (change)="eventSwitch($event, 5, 2)" [checked]="checkSwitcher(18)">
                  <label for="avail-52">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-53"
                         (change)="eventSwitch($event, 5, 3)" [checked]="checkSwitcher(25)">
                  <label for="avail-53">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_6_short_Saturday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-60"
                         (change)="eventSwitch($event, 6, 0)" [checked]="checkSwitcher(5)">
                  <label for="avail-60">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-61"
                         (change)="eventSwitch($event, 6, 1)" [checked]="checkSwitcher(12)">
                  <label for="avail-61">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-62"
                         (change)="eventSwitch($event, 6, 2)" [checked]="checkSwitcher(19)">
                  <label for="avail-62">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-63"
                         (change)="eventSwitch($event, 6, 3)" [checked]="checkSwitcher(26)">
                  <label for="avail-63">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
            <div class="uk-width-1-10 uk-grid-collapse day">
              <p>{{'date_day_7_short_Sunday' | translate}}</p>
            </div>
            <div class="uk-width-9-10 uk-grid-collapse switches">
              <div class="uk-grid uk-grid-collapse">

                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-70"
                         (change)="eventSwitch($event, 7, 0)" [checked]="checkSwitcher(6)">
                  <label for="avail-70">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-71"
                         (change)="eventSwitch($event, 7, 1)" [checked]="checkSwitcher(13)">
                  <label for="avail-71">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-72"
                         (change)="eventSwitch($event, 7, 2)" [checked]="checkSwitcher(20)">
                  <label for="avail-72">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
                <div class="uk-width-2-10 uk-container-center uk-grid-collapse switch">
                  <input class="switch" type="checkbox" id="avail-73"
                         (change)="eventSwitch($event, 7, 3)" [checked]="checkSwitcher(27)">
                  <label for="avail-73">
                    <!-- empty -->
                  </label>
                  <!-- TODO a class="jc-click-availdetail">Details…</a-->
                </div>
              </div>
            </div>
          </div>
          <!--  switch-table  MAIN uk-grid -->
        </li>
        <li id="availability-sub-vacation" *ngIf="!secondStep && checkTabStatus('vacation')"
        [ngClass]="{'uk-active': checkTabStatus('vacation')}">
          <p *ngIf="!dayOff || !dayOff.length">{{'daysOffMock' | translate}}</p>
          <ul *ngIf="dayOff && dayOff.length" id="holiday-table" class="uk-list uk-padding-remove uk-nestable-empty" >
            <li *ngFor="let day of dayOff; let i = index;"
              class="uk-nestable-item uk-nestable-nodrag uk-parent jc-template-holiday"
              id="vacation-131961-please select-17.05.2016"
              [ngClass]="{'uk-collapsed': !collapsed[day.dayOffId], 'past-item': day.inPast}">
              <div class="uk-nestable-panel uk-clearfix">
                <div class="uk-nestable-toggle"
                     data-nestable-action="toggle"
                     (click)="openDaysOff(day.dayOffId)"></div>
                <div
                  class=" uk-grid uk-grid-collapse uk-flex uk-flex-middle uk-width-1-1" style="padding-right: 5px">
                  <div class=" uk-width-1-4">
                    <ul class="uk-padding-remove uk-text-center uk-clearfix">
                      <li class="uk-float-left"><span class="jc-start-date">{{day.fromDate.format('DD.MM.YYYY')}}</span></li>
                      <li class="uk-float-left">&nbsp;-&nbsp;</li>
                      <li class="uk-float-left"><span class="jc-end-date">{{day.toDate.format('DD.MM.YYYY')}}</span></li>
                    </ul>
                  </div>
                  <div class="uk-width-1-4 uk-clearfix">
                    <span class="jc-company">{{reasonsJobber[day.reason]| translate}}</span>
                  </div>
                   <div class=" uk-width-1-4 uk-clearfix"
                        [style.visibility]=" !day.totalDays  ? 'hidden' : ''"
                      style="text-align: right;">{{day.totalDays}} {{'days'| translate}}</div>
                  <div class=" uk-width-1-4 uk-clearfix">
                    <div class="uk-width-6-10 uk-float-left statusTagWrapper" style="text-align: right;">
                       <span class="statusTag {{ day.statusLabel }} uk-margin-small-left" [ngClass]="{'label-past': day.inPast }">{{day.statusLabel | translate}}</span>
                    </div>
                    <div class="uk-width-4-10 uk-float-right" style="float:right">
                      
                      <div class="action-wrapper" style="float: right">
                        <!--<button type="button"
                                 [style.visibility]=" tagStatus !== 'Open' ? 'hidden' : ''"
                                class="uk-button edit-vacation"
                                id="vacation-131961-please select-17.05.2016"
                                (click)="editVacationOpened = day.dayOffId">change
                        </button> -->
                        <i class=' pencilEditButton' [ngClass] = "{ 'label-icon-past': day.inPast}"
                           [style.visibility]=" !day.isOpen() ? 'hidden' : ''"
                            [svgTemplate]="'pencilEditIcon'" (click)="editVacationOpened = (!day.inPast && day.dayOffId)"></i>
                        <div *ngIf="editVacationOpened === day.dayOffId">
                           <add-vacation 
                              [opened]="editVacationOpened"
                              [vacation]="day"
                              [editByUser]="true"
                            (closeEmitter)="editVacationOpened = false"></add-vacation>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ul class="uk-nestable-list">
                <li>
                  <!-- <div class="uk-width-1-1 jc-template-replace-jobs-details"></div> -->
                  <div class="jc-template-jobs-details">
                    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-middle">
                     
                      <div class="uk-width-1-1">
                          <div class="uk-grid uk-grid-collapse uk-text-left">
                            <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">{{'Company' | translate}}
                            </div>
                            <div class="uk-width-small-3-4 uk-width-medium-3-4">
                              <p class="uk-width-1-1 uk-margin-remove"><span
                                class="jc-title">{{checkCompanyList(day)}}</span></p>
                            </div>
                          </div>
                          <div class="uk-width-1-1 uk-margin-top">
                            <div class="uk-grid uk-grid-collapse uk-text-left">
                              <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                                {{'Status' | translate}}
                              </div>
                              <div class="uk-width-small-3-4 uk-width-medium-3-4">
                                <div class="uk-width-1-1 uk-margin-remove uk-margin-small-top" *ngIf="day.send">
                                    <div class="jc-diffdays uk-width-6-10 uk-float-left">
                                      <div class="uk-width-5-10 uk-float-left" *ngIf="day.webuserId !== day.send.webuser_id">{{ 'set_by' | translate}}</div>
                                      <div class="uk-width-5-10 uk-float-left" *ngIf="day.webuserId == day.send.webuser_id">{{ 'Inquiry_sent' | translate}}</div>
                                       <div *ngIf="day.webuserId !== day.send.webuser_id" 
                                            class="uk-width-5-10 uk-float-right"> 
                                          {{day.send['webuser_name']}}
                                      </div>
                                    </div>
                                    <div class="uk-width-4-10 uk-float-left ">
                                      {{ day.send.dateMoment.format('DD.MM.YYYY') }}
                                    </div>
                                </div>
                                <div class="uk-width-1-1 uk-margin-remove" *ngIf="day.change">
                                    <div class="jc-diffdays uk-width-6-10 uk-float-left">{{ 'Changed' | translate}} </div>
                                    <div class="uk-width-4-10 uk-float-left">
                                      <div class="uk-width-5-10 uk-float-left">{{ day.change.dateMoment.format('DD.MM.YYYY') }}</div>
                                      <div class="uk-width-5-10 uk-float-right"></div>
                                    </div>
                                </div>
                                <div class="uk-width-1-1 uk-margin-remove" *ngIf="day.approve">
                                    <div class="jc-diffdays uk-width-6-10 uk-float-left">
                                        <div class="uk-width-5-10 uk-float-left">{{ 'Approved' | translate}}</div>
                                        <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{day.approve['webuser_name']}} </div>
                                    </div>
                                    <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{ day.approve.dateMoment.format('DD.MM.YYYY')  }}</div>
                                </div>
                                <div class="uk-width-1-1 uk-margin-remove" *ngIf="day.update">
                                    <div class="jc-diffdays uk-width-6-10 uk-float-left">
                                        <div class="uk-width-5-10 uk-float-left"> {{ 'Updated' | translate}}</div>
                                        <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{day.update['webuser_name']}}</div>
                                    </div>
                                    <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{ day.update.dateMoment.format('DD.MM.YYYY') }}</div>
                                </div>
                                <div class="uk-width-1-1 uk-margin-remove" *ngIf="day.decline">
                                    <div class="jc-diffdays uk-width-6-10 uk-float-left">
                                        <div class="uk-width-5-10 uk-float-left"> {{ 'Declined' | translate}}</div>
                                        <div class="uk-width-5-10 uk-float-right"> {{'by' |translate}} {{day.decline['webuser_name']}}</div>
                                    </div>
                                    <div class="uk-width-4-10 uk-float-left uk-text-align-right">{{  day.decline.dateMoment.format('DD.MM.YYYY') }}</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div *ngIf="day.message" class="uk-width-1-1 uk-margin-top">
                            <div class="uk-grid uk-grid-collapse uk-text-left">
                              <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                                {{'Info' | translate}}
                              </div>
                              <div class="uk-width-small-3-4 uk-width-medium-3-4">
                                <p class="uk-width-1-1 uk-margin-remove"><span class="jc-comments">{{day.message}}</span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div *ngIf="day.managerMessage" class="uk-width-1-1 uk-margin-top">
                            <div class="uk-grid uk-grid-collapse uk-text-left">
                              <div class="uk-width-small-1-4 uk-width-medium-1-4 uk-text-bold">
                                {{'Message' | translate}}
                              </div>
                              <div class="uk-width-small-3-4 uk-width-medium-3-4">
                                <p class="uk-width-1-1 uk-margin-remove"><span class="jc-comments">{{day.managerMessage}}</span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
   
    </div>
    <!-- column -->
  </div>
  <!-- uk-grid -->
</div>

`;

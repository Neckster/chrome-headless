import { Component,
        Input, Output }           from '@angular/core';
import { select }                 from 'ng2-redux/lib/index';

import { EditVacationComponent }  from './editVacation/editVacationForm.component';
import { AddVacationComponent }   from './addVacation/addVacationForm.component';
import { AvailabilityService }    from '../../services/availability.service';
import { JobService }             from '../../services/shared/JobService';

import { availabilityTemplate }   from './availability.htm.ts';
import { AppStoreAction }         from '../../store/action';

import { prop, is }               from '../../lib/curried';
import { Cache }                  from '../../services/shared/cache.service';
import { IUITab }                 from '../../lib/interfaces/shared/ui';
import * as moment                from 'moment';
import { DayOffComponent }        from '../staff/webuser/dayoff/dayoff.component';
import { extend }                 from '../schedule/days-view/lib/utils';
import { EventEmitter }           from '@angular/router-deprecated/src/facade/async';
const tabList: IUITab[] = [
  {
    name: 'availability',
    caption: 'Availability',
    active: true
  },
  {
    name: 'vacation',
    caption: 'Vacation',
    active: false
  }
];


@Component({
  selector: 'availability-controlling',
  directives: [EditVacationComponent,
    AddVacationComponent, DayOffComponent],
  template: availabilityTemplate
})

export class JobberAvailability {

  @select() info$: any;
  @Input() secondStep;
  @Input() submitForm;
  @Output() correctSubmit = new EventEmitter(true);


  public info;
  public avalibleItem;
  public unsubscribe = [];
  public hideScrollBar = this.jobService.hideScrollBar;
  public editVacationOpened;
  private switchTabs: boolean = true;
  private availableCount = true;
  private tabs;
  private vacation: any = true;
  private dayOff = [];
  private collapsed: any = {};
  private tagStatus: string;

  private reasonsJobber = {
    'holidays': 'Holidays_reason',
    'education': 'Education_reason',
    'services': 'Public_and_social_reason',
    'sickness': 'Sickness_reason',
    'overtime': 'overtime_compensation',
    'other': 'Other_reason'
  };

  constructor(public avalibility: AvailabilityService, public action: AppStoreAction,
              private cache: Cache, private jobService: JobService) {

    this.tabs = tabList;

    if (this.cache.get('availability:tab')) {
      this.tabs = this.tabs.map((t) => Object.assign(
        {}, t, {active: t.name === this.cache.get('availability:tab')}
      ));
    }

    this.unsubscribe.push(this.info$.subscribe(info => {
      this.info = info;

      if (this.info && Array.isArray(this.info.available) &&
        this.info.available.length) this.getAvalibility();

      if (this.info && this.info.day_offs) {
        this.dayOff = this.info.day_offs.map(d => {
              return extend(d, { inPast: moment().isAfter(d.toDate) });
            })
            .sort((a, b) => b.toDate - a.toDate);

        if (this.cache.get('daysOff:tab')) {
          let t =  JSON.parse(this.cache.get('daysOff:tab'));
          this.collapsed = Object.assign({}, this.collapsed, t);
        }

      }
    }));
  }

  eventSwitch(event, weekday, shift) {
    let isChecked = event.currentTarget.checked;
    let selectedValue = isChecked ? 1 : 0;
    this.avalibility.put(0, selectedValue, shift, '0000', weekday)
      .subscribe(res => {
        this.action.fetchGetInfo();
      });
  }

  public getAvalibility() {
    this.avalibleItem = this.info.available
      .filter((a) => is(0)(prop('company_id')(a)))
      .shift();
    this.availableCount = this.avalibleItem.available.every( el => el !== 1 );
    this.action.showAlertAvailability(this.availableCount);
  }

  public checkSwitcher(id) {
    if (Array.isArray(this.info.available) &&
      this.info.available.length)
      return this.avalibleItem ? this.avalibleItem.available[id] === 1 : false;
  }

  public switchTab(tabName: string) {

    this.resetTabs();
    this.tabs.find(t => t.name === tabName).active = true;
    this.cache.set('availability:tab', tabName);

  }

  public checkTabStatus(tabName: string) {
    return this.tabs.find(t => t.name === tabName).active;
  }

  public resetTabs() {
    this.tabs = this.tabs.map(t => Object.assign({}, t, {active: false}));
  }

  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe());
  }
  ngOnChanges(change) {
    if (this.submitForm) {
      // let checked = this.avalibleItem ?
      //       this.avalibleItem.available.some( el => el === 1) : false;
      this.correctSubmit.emit({
        // action: checked
        action: true
      });
    }
  }

  changeDate(st, week, day?) {
    if ( st === 'week') {
      let date = moment(`${week}${day}`, 'GGWWE');
      return date.format('DD.MM.YYYY');
    }else if (st === 'mn') {
      let date = moment(week, 'mm');
      return date.format('DD.MM.YYYY');
    }
  }

  checkCompanyList(day) {
    let id = day.companyId,
        res = this.info.companies.hasOwnProperty(id) ? this.info.companies[id] : undefined;
    return res ? res.cname : 'Invalid company';
  }
  openDaysOff(id) {
    this.collapsed[id] = !this.collapsed[id];
    this.cache.set('daysOff:tab', JSON.stringify(this.collapsed) );
  }
  /* tslint:disable:no-string-literal */
  checkTagStatus(day) {
    let sortObj = {
      Approved : day.approve ? day.approve.dateMoment : undefined,
      Updated : day.update ? day.update.dateMoment : undefined,
      Declined : day.decline ? day.decline.dateMoment : undefined
    };
    let arr: any = Object.keys(sortObj).sort( (a: any, b: any): any => {
        if (!sortObj[a]) return true;

        if (!sortObj[b]) return false;

        return sortObj[b].isAfter(sortObj[a], 'minutes');
      }),
      res = arr[0],
      noStatus = !sortObj['Approved'] &&
        !sortObj['Updated'] &&
        !sortObj['Declined'];

    this.tagStatus = noStatus ? 'Open' : res;
    return this.tagStatus;
  }
  /* tslint:disable:no-string-literal */

}

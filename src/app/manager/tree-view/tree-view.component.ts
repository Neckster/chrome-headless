import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { UnitService } from '../../services/unit.service';
import { AppStoreAction } from '../../store/action';
import { treeViewHtml } from './tree-view.html';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { SvgTemplateDirective } from '../../lib/directives/svg-icon.directive';
import { JobService } from '../../services/shared/JobService';

@Component({
  selector: 'tree-view',
  providers: [],
  directives: [TreeViewComponent, SvgTemplateDirective],
  pipes: [],
  styles: [`
        .hideArrow:after{
          display: none;
        }    
        
      `],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: treeViewHtml
})

export class TreeViewComponent {
  // Set our default values
  @Input() company;
  @Input() close;
  @Input() child;
  @Input() lvl;
  @Input() info;
  @Input() directories;
  @Input() options;
  @Input() parentShift;
  public show: any;
  // public child: any;
  public unitService: any;
  public open = false;
  public hideScrollBar = this.jobService.hideScrollBar;

  // TypeScript public modifiers
  constructor(unitService: UnitService,
              private action: AppStoreAction,
              private  jobService: JobService) {
    this.unitService = unitService;
    this.show = true;
  }

  ngOnInit() {
  }

  clearEvent(event) {
    event.preventDefault();
  }

  slectUnit(unit, event) {
    event.preventDefault();
    this.action.fetchCurrentUnit(+unit.id.slice(1),
      ParseOrganisationService.calculateWeek());
      this.action.setUname(unit.text ? unit.text : unit.name);
      this.action.fetchGetInfo();
      if (this.close) {
        this.hideScrollBar();
        this.close.emit(undefined);
      }
  }
}

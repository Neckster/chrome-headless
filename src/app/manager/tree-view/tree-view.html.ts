//noinspection TsLint
export const treeViewHtml = `
<ul  class="uk-nav-parent-icon" [ngClass]="{'uk-nav-sub': child, 'uk-nav': !child}"  >
    <li *ngFor="let dir of company" class="uk-parent" >
     
        <a   href="#" class='hideArrow' *ngIf="dir.type === 'unit'" (click)="  slectUnit(dir, $event)">{{ dir.name }}</a>
        <a   class='hideArrow' *ngIf="dir.type !== 'unit'" (click)="dir.open = !dir.open">
          <div class="icon blue uk-margin-right uk-margin-small-left" [ngClass]='{"arrow-down":dir.open , "arrow-right":!dir.open}'></div>
          {{ dir.name }}
        </a>
        
        <ul class="uk-nav-sub uk-nav-parent-icon " *ngIf="dir.open">

            <li class="uk-parent" *ngIf="dir.directories.length > 0">
             <tree-view [close]="close" [child]="true" [company]="dir.directories" ></tree-view>
            </li>

            <li *ngFor="let file of dir.files" style="margin-bottom: 1%">
              <a *ngIf="file"  href="#" style="margin-bottom: 1%" (click)="slectUnit(file, $event)">{{ file.text || file.name }}</a>
            </li>
        </ul>
    </li>
</ul>`;

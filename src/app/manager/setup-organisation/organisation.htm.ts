//noinspection TsLint
export const htmlOrganisation: string = `
<div *ngIf="info && info.sys" class="page organisation plannerview uk-clearfix">
      <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
        <div class="uk-width-1-1 uk-container-center uk-grid-collapse">
          <h1 *ngIf="!secondStep">{{'organisation_structure' | translate}}</h1>
          <p class="uk-alert uk-alert-success OrgStAlert" *ngIf="!secondStep && showOrgAlert">
            <a (click)="hideAlert()" class="uk-alert-close uk-close"></a>
            <span>{{'hint_configure_shifts' | translate}}</span><br>
            <span>{{'hint_system_shift_availability' | translate}}</span><br>
            <span>{{'hint_configure_shifts_time' | translate}}</span>
          </p>
          <div *ngIf="errorMesage" class="alertbox">
            <div id="error-organisation-position-active" class="uk-alert uk-alert-danger error" data-uk-alert="">
              <a (click)="action.treeViewError(false)" class="uk-alert-close uk-close"></a>{{'warn_position_has_jobbers' | translate}}</div>
          </div>
          <div *ngIf="errorMesageU" class="alertbox">
            <div id="error-organisation-position-active" class="uk-alert uk-alert-danger error" data-uk-alert="">
              <a (click)="action.treeViewErrorU(false)" class="uk-alert-close uk-close"></a>{{'Unit_or_Group_still_contains_elements' | translate}}</div>
          </div>
          <form class="uk-form account">
            <div class="table-heading uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
              <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-4-10 uk-container-center uk-grid-collapse uk-hidden-small"></div>
              <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-6-10 uk-container-center uk-grid-collapse">
                <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
                  <div class="toggle uk-width-1-5 uk-container-center uk-grid-collapse"><br/>{{'inherit' | translate}}</div>
                  <!-- FIXME: lots of duplicate IDs -->
                  
                  <div *ngFor="let shift of info.sys.shift; let i=index" class="uk-width-1-5 uk-container-center uk-grid-collapse">
                    <span class="jc-shift-0-name" id="account-unit-0-shift-0-name">{{shiftParse[i] | translate}}</span>
                    <br>
                    <span class="jc-shift-0-from" id="account-unit-0-shift-0-from">{{shift.from}}</span>–
                    <span class="jc-shift-0-to" id="account-unit-0-shift-0-to">{{shift.to}}</span>
                  </div>
                
                </div>
              </div>
            </div>
            <div class="uk-grid uk-grid-collapse uk-text-left uk-padding-remove">
              <div class="uk-width-1-1 uk-container-center uk-grid-collapse">
                <div class="account-tree jstree jstree-1 jstree-default" >
                  <my-tree-view [secondStep]="secondStep" [info]="info" [company]="organisation" [options]="options" [parentShift]="parentShift"></my-tree-view>
                </div>
              </div>
            </div></form>
            
          

        </div>
        <!-- column -->
      </div>
      <!-- grid -->
    </div>
`;

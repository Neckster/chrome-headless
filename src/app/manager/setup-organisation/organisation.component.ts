import { Component, Input, Output } from '@angular/core';
import { select }                   from 'ng2-redux';
import { htmlOrganisation }         from './organisation.htm.ts';
import { TreeViewBase }             from '../organisation-tree/tree-view';
import { Directory }                from '../organisation-tree/tree-view/directory';
import { ParseOrganisationService } from '../../services/shared/parseOrganisation.service';
import { AppStoreAction }           from '../../store/action';
import { OutputEmitter }            from '@angular/compiler/src/output/abstract_emitter';
import { EventEmitter }             from '@angular/router-deprecated/src/facade/async';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'auth'
  selector: 'setup-organisation',  // <controlling></controlling>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [
    TreeViewBase
  ],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [],
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: htmlOrganisation
})

export class SetupOrganisation {
  // Set our default values
  @select() info$;
  @select(state => state.treeViewError) treeErrorMessage$;
  @select(state => state.treeViewErrorU) treeErrorMessageU$;
  @Input() secondStep;
  @Input() submitForm;
  @Output() correctSubmit = new EventEmitter(true);

  public info: any;
  public organisation: Array<Directory>;
  public options: any;
  public parentShift: any;
  public unsubscribe = [];
  public errorMesageU: any;
  public shiftParse = ['shift_0', 'shift_1', 'shift_2', 'shift_3'];
  // TypeScript public modifiers
  private errorMesage;
  private showOrgAlert = true;


  constructor(public action: AppStoreAction) {
    ParseOrganisationService.isEditCompany = true;

    this.unsubscribe.push(this.info$.subscribe(info => {
      if (info.manager) {
        this.info = Object.assign({}, info);
        ParseOrganisationService.sys = Object.assign({}, info.sys);
        if (!ParseOrganisationService.treeViewOpen) {
          let obj = {};
          info.manager.organisation.forEach(org => { obj[org.id] = false; });
          ParseOrganisationService.treeViewOpen = obj;
        }
        this.init();
      }
    }));
    this.treeErrorMessage$.subscribe(data => {
      this.errorMesage = data;
    });
    this.treeErrorMessageU$.subscribe(data => {
      this.errorMesageU = data;
    });
  }
  ngOnChanges(change) {
    if (this.submitForm) {
      this.correctSubmit.emit({
        action: true
      });
    }
  }
  ngOnDestroy() {
    this.unsubscribe.forEach(el => el.unsubscribe());
    ParseOrganisationService.isEditCompany = false;
  }
  init() {
    ParseOrganisationService.treeStruct(this.info.manager.organisation);
    let treeData = ParseOrganisationService.treeData;
    this.organisation = [new Directory(
      treeData[0].id,
      treeData[0].text,
      this.getDir(treeData[0].child),
      this.getFiles(treeData[0].child),
      treeData[0].type, treeData[0].data)];
    this.parentShift = Object.assign({}, this.info.sys.shift);
    // ParseOrganisationService.activeId = treeData[0].id;

    let alertInStorage = localStorage.getItem('organisationAlert');
    if ( alertInStorage ) {
      this.showOrgAlert = JSON.parse(alertInStorage);
    }

  }

  getFiles(data) {
    if (!data) return  [];
    return data.filter(el => {
      return  !el.child;
    }).map(el => {
      return el;
    });
  }
  getDir(data) {
    if (!data) return [];
    return data.filter(el => {
      return !!(el.child && el.child.length > 0);
    }).map(el => {
      let children = el.child && el.child.length > 0 ? el.child : [];
      return new Directory(
        el.id,
        el.text,
        this.getDir(children),
        this.getFiles(children),
        el.type, el.data);
    });
  }
  getFilesByType(data) {
    if (!data) return  [];
    return data.filter(el => {
      return  el.type === 'position'; // !el.child;
    }).map(el => {
      return el;
    });
  }
  getDirByType(data) {
    if (!data) return [];
    return data.filter(el => {
      return el.type !== 'position'; // !!(el.child && el.child.length > 0);
    }).map(el => {
      let children = el.child && el.child.length > 0 ? el.child : [];
      return new Directory(
          el.id,
          el.text,
          this.getDir(children),
          this.getFiles(children),
          el.type, el.data);
    });
  }
  hideAlert() {
    localStorage.setItem('organisationAlert', 'false' );
    this.showOrgAlert = false;
  }
}



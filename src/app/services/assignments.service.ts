import { Injectable } from '@angular/core';
import { Assignment } from '../models/Assignment';

import {
  identity as Identity,
  isNil,
  allPass,
  anyPass,
  not,
  has,
  equals,
  where,
  pipe,
  sort,
  filter,
  flip,
  ifElse,
  both,
  length,
  compose,
  adjust,
  head,
  is,
  curryN,
  map,
  T } from 'ramda';

import { JobService } from './shared/JobService';
import { InfoResponse } from './company.service';

@Injectable()
export class AssignmentsService {

  constructor(private jobService: JobService) {

  }

  public getCurrentAssignments(
    {webuser_id: userId, week, timeGGWW}: any,
    assignments: Assignment[],
    info: InfoResponse): Assignment[] {

    // const userId = inputCriteria.webuser_id;
    // const week =  inputCriteria.week;
    // const timeGGWW = inputCriteria.timeGGWW;

    // Higher-order predicate function
    // filterPred: Object -> Boolean
    const filterPred = where({
      // citime     : Identity,
      cotime     : isNil,
      webuser_id : equals(userId),
      weekday    : equals(week),
      week       : equals(timeGGWW),
      published  : equals(T()),
      stat       : x => is(Object, x) ? (x.deleted || x.declined) ? false : true : true, // todo fix
      status     : x => not(equals('past', x))
    });

    const comparatorFn = (a: Assignment, b: Assignment): number => {
      return a.tFrom - b.tFrom;
    };

    // const isInstanceOf = (type, i) => i instanceof type; // depr. in favour of `R.is`
    const decorateWithStaff = (x: Assignment | any): Assignment | any => {
      switch (is(Assignment, x)) { // todo: still unsure if there's need in this check
        case true:
          x.staff = <Assignment>x.from ?
            this.jobService.getStaff(x, info) : [];
          return x;
        case false:
          return Object.assign({}, x, {
            staff: <Assignment>x.from ?
              this.jobService.getStaff(x, info) : []
          });
        default: // empty
      }
    };

    // Wrapping `R.adjust` with flipped arg. order function-wrapper
    // const adjustFlipped = (index) => (fn) => adjust(fn, index);
    // adjustFlipped: Number -> Function -> List
    const adjustFlipped = curryN(2, ((index, fn) => adjust(fn, index)));

    // Partially applying `adjustFlipped` to desired indices
    // and keeping returned func. for further transformation
    // adjustFirst : Function -> List
    const adjustFirst = adjustFlipped(0);
    const adjustSecond = adjustFlipped(1);

    // `combinedAdjust` - `R.adjust` composed of 2 separate `adjusts`
    const combinedAdjust = compose(adjustFirst(decorateWithStaff), adjustSecond(decorateWithStaff));

    const decorateWithJobServiceData = ifElse(
      // input arg is non-empty array
      both(Identity, length as any),
      // bind context, as there are internal `this` ref's
      // flip function to receive second arg before first
      flip(this.jobService.calcAssignments.bind(this.jobService))(info.meta.time) as any,
      Identity
    );

    // assignmentPipeline: Assignment[] -> Assignment[]
    const assignmentPipeline: (a: Assignment[]) => Assignment[] = pipe(
      decorateWithJobServiceData,
      // x => console.log(x) || x,
      filter(filterPred),
      // x => console.log(x) || x,
      sort(comparatorFn),
      combinedAdjust
    ) as (a: Assignment[]) => Assignment[];

    return assignmentPipeline(assignments as Assignment[]) as Assignment[];
  }

}

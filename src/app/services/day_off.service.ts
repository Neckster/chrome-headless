import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { Http } from '@angular/http';
import { BaseService } from './shared/base.service';
import { DayOff, DayOffFactory } from '../models/DayOff';
import { map } from 'ramda';

@Injectable()
export class DayOffService extends BaseService {
  constructor(private http: Http,
              private apiHttp: NodeApiHttp,
              private dayOffFactory: DayOffFactory) {
    super();
  }

  saveDayOff(dayOff: any) { // : DayOff
    return this.apiHttp.put('day_off', JSON.stringify(dayOff)).map(v => v.json());
  }

  deleteDayOff(dayOffId: number) {
    return this.apiHttp.delete(`day_off/${dayOffId}`).map(v => v.json());
  }

  prepareInfo(infoData) {
    if (infoData.day_offs) {
      infoData.day_offs = map(dayOff => {
        return this.dayOffFactory.createDayOff(dayOff);
      }, infoData.day_offs);
    }
  }
}

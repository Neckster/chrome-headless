import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

interface IServerResponse {
  pagination: any;
  staff: any;
}

@Injectable()
export class StaffService extends BaseService {

  constructor(private http: Http, private apiHttp: NodeApiHttp) {
    super();
  }

  public get(page: number, step: number, orderBy: string,
             orderDirection: string, q?: string): Observable<IServerResponse> {
    let url = 'staff';
    const args = Array.prototype.slice.call(arguments, 0);
    while (args.length > 1) {
      url = url + '/' + args.shift();
    }
    if (q) {
      url += `?q=${q}`;
    }
    return this.apiHttp.get(url).map((res) => res.json());
  }

  public getShortInfo(q?: string): Observable<IServerResponse> {
    let url = 'staff/short/all';
    if (q) {
      url += `?q=${q}`;
    }
    return this.apiHttp.get(url).map((res) => res.json());
  }

  public updateStaffInfo(userId, info): Observable<Response> {
    return this.apiHttp.put('staff/user/' + userId,
      JSON.stringify({profile: info})).map(res => res.json());
  }

}

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';

export interface InfoResponse {
  available: any;
  company: any;
  manager: any;
  meta: any;
  news: any;
  rating: any;
  sys: any;
  webuser: any;
  assignment?: any;
}

@Injectable()
export class CompanyService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }

  /*fetch the managed organisation structure of the given company.*/
  public putCompany(param): Observable<InfoResponse> {
    let url = 'company';
    return this.apiHttp.put(url, JSON.stringify(param)).map((res) => res.json());
  }
  /*fetch the managed organisation structure of the company that was selected before*/
  // public getOrganisation(): Observable<Response> {
  //   return this.apiHttp.getUnit(`organisation`).map((res) => res.json());
  // }
}

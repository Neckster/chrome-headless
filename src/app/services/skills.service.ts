import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SkillsService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }
/*
  public getUnit(): Observable<Response> {
    return this.apiHttp.getUnit('user/skill').map((res) => res.json());
  }
*/
  public get(id?: number): Observable<Response> {
    return this.apiHttp.get(`user/skill/${id}`).map((res) => res.json());
  }

  // TODO: implement
  public put(id: number, skills: Object) {

  }
}

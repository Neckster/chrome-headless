import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { BaseService } from './shared/base.service';

@Injectable()
export class EmitService extends BaseService {

  private observe;

  constructor() {
    super();
    this.observe = [];
  }

  addEvent(eName) {
    let event = new EventEmitter();
    this.observe.push({name: eName, event: event});
    return event;
  }

  emit(eName: string, value?: any) {
    let event = this.getEvents(eName);
    if (event[0] !== undefined) {
      event[0].event.emit(value ? value : value);
    }
  }

  emitAll(eName: string, value?: any) {
    let events = this.getEvents(eName);
    for (let event of events) {
      if (event.event) {
        event.event.emit(value);
      }
    }
  }

  unsubscribeAll(eName: string) {
    this.observe = this.observe.filter(eventData => {
      return eventData.name !== eName;
    });
  }

  getEvent(eName: string) {
    let event = this.observe.filter(eventData => { return eventData.name === eName; });
    return event ? event : undefined;
  }

  private getEvents(eName: string) {
    return this.observe.filter(eventData => {
      return eventData.name === eName;
    });
  }
}

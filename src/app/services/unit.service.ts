import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { NodeApiHttp } from './shared/node-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestOptionsArgs, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ParseOrganisationService } from './shared/parseOrganisation.service';
import { merge } from 'ramda';


@Injectable()
export class UnitService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp, private nodeApiHttp: NodeApiHttp) {
    super();
  }

  public list(): Observable<Response> {
    return this.apiHttp.get('unit').map((res) => res.json());
  }

  // public getUnit(unitId: number): Observable<Response> {
  //   return this.apiHttp.get(`unit/${unitId}`).map((res) => res.json());
  // }
  // public get(unitId: number): Observable<Response> {
  //   return this.apiHttp.get(`unit/${unitId}`).map((res) => res.json());
  // }

  public getUnit(unitId: number, week?: string) {
    let url = `unit/${unitId}/${week ? week : ParseOrganisationService.calculateWeek()}`;

    return Observable.forkJoin(
        this.apiHttp.get(url).map((res) => res.json()),
        this.nodeApiHttp.get(url).map((res) => res.json())
    ).map(unitData => {
      return merge(unitData[0], unitData[1]);
    });
  }

  public put(unit: any): Observable<Response> {
    return this.apiHttp.put('unit', JSON.stringify(unit)).map((res) => res.json());
  }

  public del(unitId: any): Observable<Response> {
    // let basicOptions: RequestOptionsArgs = { body: JSON.stringify({ validTo }) };
    // let opts = new RequestOptions(basicOptions);
    return this.apiHttp.delete(`unit/${unitId}`).map((res) => res.json());
  }

  public publish(unitId: number, week: string): Observable<Response> {
    return this.apiHttp.post('unit/publish', JSON.stringify({
      unit_id: unitId, week
    })).map((res) => res.json());
  }


}

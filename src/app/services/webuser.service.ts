import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class WebuserService extends BaseService {

  public lang = {
    en: 'English',
    de: 'German',
    fr: 'French',
    es: 'Spanish',
    it: 'Italian',
    pt: 'Portuguese',
    ru: 'Russian',
    hi: 'Hindi',
    ar: 'Arabic',
    zh: 'Chinese',
    ja: 'Japanese',
    ko: 'Korean',
    bn: 'Bengali',
    th: 'Tai Kadai'
  };

  public obj = {
    admin: 'Admin'
  };

  public hotel = {
    'reception': 'skill_hotel_reception',
    'head-porter': 'skill_hotel_head_porter',
    'concierge': 'skill_hotel_concierge',
    'porter': 'skill_hotel_porter',
    'driver': 'skill_hotel_driver',
    'housekeeping-principal': 'skill_hotel_housekeeping_principal',
    'housekeeping': 'skill_hotel_housekeeping',
    'room-service': 'skill_hotel_room_service',
    'fitness': 'skill_hotel_fitness',
    'shop': 'skill_hotel_shop',
    'f-b-manager': 'skill_hotel_f_b_manager',
    'division-manager': 'skill_hotel_division_manager',
    'c-b-manager': 'skill_hotel_c_b_manager'
  };

  public admin = {
    'allround': 'skill_admin_allround',
    'secretary': 'skill_admin_secretary',
    'finance': 'skill_admin_finance',
    'marketing': 'skill_admin_marketing',
    'hr': 'skill_admin_hr',
    'event': 'skill_admin_event',
    'promotion': 'skill_admin_promotion',
    'production': 'skill_admin_production',
    'sales': 'skill_admin_sales',
    'buying': 'skill_admin_buying',
    'warehouse': 'skill_admin_warehouse',
    'cleaning': 'skill_admin_cleaning',
    'building-services': 'skill_admin_building_services',
    'it': 'skill_admin_it'
  };
  public entertain = {
    'cashier': 'skill_entertain_cashier',
    'cloakroom': 'skill_entertain_cloakroom',
    'host': 'skill_entertain_host',
    'security': 'skill_entertain_security',
    'steward': 'skill_entertain_steward',
    'usher': 'skill_entertain_usher',
    'driver': 'skill_entertain_driver',
    'flagman': 'skill_entertain_flagman',
    'logistics': 'skill_entertain_logistics',
    'roadie': 'skill_entertain_roadie',
    'stagehand': 'skill_entertain_stagehand',
    'rigger': 'skill_entertain_rigger',
    'event-technician': 'skill_entertain_event_technician',
    'forklift': 'skill_entertain_forklift'
  };

  public gastro = {
    'principal': 'skill_gastro_principal',
    'service': 'skill_gastro_service',
    'sommelier': 'skill_gastro_sommelier',
    'buffet': 'skill_gastro_buffet',
    'bar-principal': 'skill_gastro_bar_principal',
    'bar': 'skill_gastro_bar',
    'runner': 'skill_gastro_runner',
    'head-chef': 'skill_gastro_head_chef',
    'cook': 'skill_gastro_cook',
    'kitchen-help': 'skill_gastro_kitchen_help',
    'scullery': 'skill_gastro_scullery',
    'reception': 'skill_gastro_reception',
    'allround': 'skill_gastro_allround',
    'restaurant-manager': 'skill_gastro_restaurant_manager'
  };

  public tourism = {
    'tour-guide': 'skill_tourism_tour_guide',
    'cashier': 'skill_tourism_cashier',
    'technician': 'skill_tourism_technician',
    'letting': 'skill_tourism_letting',
    'instructor-skiing': 'skill_tourism_instructor_skiing',
    'instructor-water': 'skill_tourism_instructor_water',
    'bus-driver': 'skill_tourism_bus_driver'
  };

  public shiftParse = ['shift_0', 'shift_1', 'shift_2', 'shift_3'];
  public weekDay = ['date_day_1_short_Monday',
    'date_day_2_short_Tuesday', 'date_day_3_short_Wednesday',
    'date_day_4_short_Thursday', 'date_day_5_short_Friday',
    'date_day_6_short_Saturday', 'date_day_7_short_Sunday'];

  public skillLevel = ['-', 'Basics', 'Good', 'Expert' ];

  constructor(public apiHttp: LuaApiHttp) {
    super();
  }


}

import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { Http } from '@angular/http';
import { BaseService } from './shared/base.service';
import { map } from 'ramda';
import * as moment from 'moment';

export interface ShortUserStatistic {
    webuser_id: number;
    webuser_name: string;
    webuser_surname: string;
    unit_name: string;
    position_name: string;
    jobbhome_hours: number;
    jobbhoper_hours: number;
}

export interface StatisticItem {
    date: number;
    total_work_hours: number;
    total_planned_hours: number;
    rows: DayOffStatisticRow[]|WorkStatisticRow[];
}

export interface WorkStatisticRow {
    shift: number;
    from: string;
    to: string;
    position_id: number;
    position_name: string;
    unit_id: number;
    unit_name: string;
    worked_time: number;
    planned_time: number;
    is_day_off: boolean;
}

export interface DayOffStatisticRow {
    is_day_off: boolean;
    shift: number;
    day_off_data: {
        reason: string;
        total_days: number;
        message: string;
    };
}

export interface FullStatistic {
    detail_info: StatisticItem[];
    total_info: {
        total_work_hours: number;
        total_planned_hours: number;
        day_off: any;
    };
}

@Injectable()
export class StatisticService extends BaseService {
    constructor(private http: Http, private apiHttp: NodeApiHttp) {
        super();
    }

    filterJobbers(fromWeek, toWeek, fromWeekday, toWeekday, unitId) {
        return this.apiHttp.post('statistic/short', JSON.stringify({
            range: {
                from_week: fromWeek,
                to_week: toWeek,
                from_weekday: fromWeekday,
                to_weekday: toWeekday
            },
            unit_id: unitId
        })).map(v => v.json());
    }

    getUserStatisticByRange(fromWeek, toWeek, fromWeekday, toWeekday, webuserId) {
        return this.apiHttp.post('statistic', JSON.stringify({
            range: {
                from_week: fromWeek,
                to_week: toWeek,
                from_weekday: fromWeekday,
                to_weekday: toWeekday
            },
            webuser_id: webuserId
        })).map(v => v.json());
    }

    getUserStatistic(monthName: string, yearName: number,  webuserId) {
        let from = moment().month(monthName).year(yearName).startOf('month');
        let to = moment(from).endOf('month');
        return this.getUserStatisticByRange(
            from.format('GGWW'),
            to.format('GGWW'),
            from.format('E'),
            to.format('E'),
            webuserId);

    }

    getUserStatisticXLSUrl(monthName: string, yearName: number, webuserId) {
        let from = moment().month(monthName).year(yearName).startOf('month');
        let to = moment(from).endOf('month');
        return this.apiHttp.baseUrl + '/statistic/xls' + this.apiHttp.jsonToQueryString({
            range: {
                from_week: from.format('GGWW'),
                to_week: to.format('GGWW'),
                from_weekday: from.format('E'),
                to_weekday: to.format('E')
            },
            webuser_id: webuserId
        });
    }
}

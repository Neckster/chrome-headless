import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { Http } from '@angular/http';
import { BaseService } from './shared/base.service';

@Injectable()
export class ScheduledService extends BaseService {
  constructor(private http: Http, private apiHttp: NodeApiHttp) {
    super();
  }

  getUnitUsersWeekJobs(week, unit) {
    return this.apiHttp.get(`schedule/${unit}/${week}`);
  }

  checkForFutureAssignments(week, positions, countWeeks) {
    return this.apiHttp.post(`schedule/week/check`, JSON.stringify({
      'position_ids': positions,
      'week': week,
      'count_weeks': countWeeks
    })).map(v => v.json());
  }

  copyWeekFor(week, positions, countWeeks, needOnly) {
    return this.apiHttp.post(`schedule/week/copy`, JSON.stringify({
      'position_ids': positions,
      'week': week,
      'count_weeks': countWeeks,
      'need_only': needOnly
    })).map(v => v.json());
  }

  clearWeeks(week, positions) {
    return this.apiHttp.post(`schedule/week/check`, JSON.stringify({
      'position_ids': positions,
      'week': week
    })).map(v => v.json());
  }

  publishJobs(unitId, week, weekday?) {
    return this.apiHttp.post('unit/publish', JSON.stringify({
      'unit_id': unitId,
      'week': week,
      'weekday': weekday
    })).map(v => v.json());
  }

  checkRange(webuserId, fromWeek, toWeek, fromWeekday, toWeekday, fromShift, toShift, companyId) {
    return this.apiHttp.post('schedule/range/check', JSON.stringify({
      'webuser_id': webuserId,
      'from_week': fromWeek,
      'from_weekday': fromWeekday,
      'from_shift': fromShift,
      'to_week': toWeek,
      'to_weekday': toWeekday,
      'to_shift': toShift,
      'company_id': companyId
    })).map(v => v.json());
  }
}

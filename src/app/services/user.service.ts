import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { UserProfile } from '../lib/interfaces/user/profile';
import { UserInvite } from '../lib/interfaces/user/invite';

@Injectable()
export class UserService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }

  public get(): Observable<Response> {
    return this.apiHttp.get('user').map(res => res.json());
  }

  public optin(): Observable<Response> {
    return this.apiHttp.post('optin', '').map(res => res.json());
  }

  // TODO check if its the same route as option
  public optout(): Observable<Response> {
    return this.apiHttp.post('optin', '').map(res => res.json());
  }

  public update(user: UserProfile): Observable<Response> {
    return this.apiHttp.put('user',
      JSON.stringify(_.omitBy(user, _.isUndefined))).map(res => res.json());
  }

  public updateSkills(userId: number, skills): Observable<Response> {
    return this.apiHttp.put('user/skill',
      JSON.stringify({ webuser_id: userId, skills })).map(res => res.json());
  }

  // TODO check staff-grid
  public invite(user): Observable<Response> {
    return this.apiHttp.post('user/invite',
      JSON.stringify(_.omitBy(user, _.isUndefined))).map((res) => res.json());
  }

  // import
  public convey(columns, data): Observable<Response> {
    return this.apiHttp.post('user/import',
      JSON.stringify({ columns, data })).map((res) => res.json());
  }

  public find(query): Observable<Response> {
    return this.apiHttp.get(`/user/find/${query}`).map((res) => res.json());
  }

  public rate(userId, rating): Observable<Response> {
    return this.apiHttp.put(`user/rate/${userId}/${rating}`, '').map((res) => res.json());
  }

 /* public updateStaffInfo(userId, info) : Observable<Response> {
    debugger;
    return this.apiHttp.put('staff/user',
      JSON.stringify({ webuser_id: userId, info })).map(res => res.json());
  }*/

}

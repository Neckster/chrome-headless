import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
interface InfoResponse {
  available: any;
  company: any;
  manager: any;
  meta: any;
  news: any;
  rating: any;
  sys: any;
  webuser: any;
}

@Injectable()
export class OrganizationService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }

  /*fetch the managed organisation structure of the given company.*/
  public getOrganisation(companyId?: number): Observable<InfoResponse> {
    let url = companyId ? `organisation/${companyId}` : 'organisation';
    return this.apiHttp.get(url).map((res) => res.json());
  }
  /*fetch the managed organisation structure of the company that was selected before*/
  // public getOrganisation(): Observable<Response> {
  //   return this.apiHttp.getUnit(`organisation`).map((res) => res.json());
  // }
}

import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { Http } from '@angular/http';
import { BaseService } from './shared/base.service';

@Injectable()
export class OnBoardingService extends BaseService {
  constructor(private http: Http, private apiHttp: NodeApiHttp) {
    super();
  }

  changeDefaultCompanyData(company, unit, positions) {
    let positionsData = positions.map(item => {
      return {
        position_name: item.position_name,
        position_id: item.position_id
      };
    });

    return this.apiHttp.put(`user/company/onboarding`, JSON.stringify({
      company_data: {
        company_id: company.company_id,
        company_name: company.company_name
      },
      unit_data: {
        unit_id: unit.unit_id,
        unit_name: unit.unit_name
      },
      positions_data: positionsData
    })).map(v => v.json());
  }

  changeOnBoardingStep(step, webuserId) {
    console.log('service', step);
    return this.apiHttp.put(`user/onboarding/step`, JSON.stringify({
      step: step,
      webuser_id: webuserId
    })).map(v => v.json());
  }

  checkUserStatus() {
    return this.apiHttp.get('auth/user/status').map(v => v.json()).share();
  }
}

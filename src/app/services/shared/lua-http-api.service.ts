import { ApiHttp }    from './http-api.service';
import { Injectable, provide } from '@angular/core';
import {
  Http,
  Headers,
  RequestOptionsArgs,
  ConnectionBackend,
  RequestOptions,
  Request,
  Response,
  XHRBackend,
  BrowserXhr,
  BaseRequestOptions,
  BaseResponseOptions,
  ResponseOptions
} from '@angular/http';

@Injectable()
export class LuaApiHttp extends ApiHttp {
  public baseUrl: string = 'http://www.jubbr.com/luaapi';
}

export const LUA_API_HTTP_PROVIDERS: any[] = [
  provide(LuaApiHttp,
    {
      useFactory: (xhrBackend, requestOptions) => new LuaApiHttp(xhrBackend, requestOptions),
      deps: [XHRBackend, RequestOptions]
    }),
  BrowserXhr,
  provide(RequestOptions, {useClass: BaseRequestOptions}),
  provide(ResponseOptions, {useClass: BaseResponseOptions}),
  XHRBackend
];

/**
 * Created by user on 22.08.16.
 */
/**
 * Created by user on 26.05.16.
 */
import { Injectable, provide } from '@angular/core';
import { Directory } from '../../manager/organisation-tree/tree-view/directory';
import * as moment  from 'moment/moment';
import * as _ from 'lodash';


@Injectable()
export class CacheService {
  public static day1 = {};
  public static day2 = {};
  public static day3 = {};
  public static day4 = {};
  public static day5 = {};
  public static day6 = {};
  public static day7 = {};

  public static shift0 = {};
  public static shift1 = {};
  public static shift2 = {};
  public static shift3 = {};

  static get(day) {
    return this[`day${day + 1}`];
  }
  static set(dayObject, day) {
    this[`day${day + 1}`] = dayObject;
  }
  static setUpdate(day) {
    this[`day${day}`].update = true;
  }
  static cleanCache() {
    for (let i = 1; i < 8; i++ ) {
      this[`day` + i] = {};
    }
  }
  static iterationShift (shiftIdx, pos) {
    this[`shift${shiftIdx}`] = {
      [pos]: {}
    };
  }
  static getShift(shiftIdx, pos) {
    if (!Object.keys(this[`shift${shiftIdx}`]).length)
      return false;
    return this[`shift${shiftIdx}`][pos];
  }
  static setShift(shiftObject, shiftIdx, pos) {
    this[`shift${shiftIdx}`] = {
      [pos] : shiftObject
    };
  }
  static setUpdateShift(shiftIdx, pos) {

    this[`shift${shiftIdx}`] = {
      [pos]: {
        update:  true
      }
    };
  }
  static cleanCacheShift() {
    for (let shiftIdx = 0; shiftIdx < 4; shiftIdx++ ) {
      this[`shift${shiftIdx}`] = {};
    }
  }
}

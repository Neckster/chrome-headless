import { Injectable } from '@angular/core';

@Injectable()
export class Cache {

  private interface: string = undefined;
  private defaultInterface = 'localStorage';
  private allowedInterfaces = ['localStorage', 'sessionStorage'];
  private internalCache = {};

  public setInterface(value: string) {
    this.interface = this.allowedInterfaces.indexOf(value) !== -1 ?
      value : this.defaultInterface;
  }

  constructor() {
    this.interface = this.defaultInterface;
  }

  public get(key: string) {
    return this.internalCache[key] ?
      this.internalCache[key] : window[this.interface].getItem(key);
  }

  public set(key: string, val: any) {
    this.internalCache[key] = val;
    window[this.interface].setItem(key, val);
  }

  public getObject(key: string) {
    return JSON.parse(this.get(key));
  }

  public setObject(key: string, valObj: any) {
    this.set(key, JSON.stringify(valObj));
  }

  public pushArray(arrName: string, el: any) {
    if (!this.getObject(arrName)) {
      this.setObject(arrName, [el]);
    } else if (!this.isInArray(arrName, el)) {
      this.setObject(
        arrName,
        this.getObject(arrName).concat(el)
      );
    }
  }

  public removeFromArray(arrName: string, el: any) {
    if (this.isInArray(arrName, el)) {
      let temp = this.getObject(arrName);
      temp.splice(
        this.getObject(arrName).indexOf(el), 1
      );
      this.setObject(
        arrName,
        temp
      );
    }
  }

  public isInArray(arrName: string, el) {
    return this.getObject(arrName) ? this.getObject(arrName).includes(el) : false;
  }

  public removeKey(key: string) {
    window[this.interface].removeItem(key);
  }

  public cleanAppCache(remain: string[]) {
    Object.keys(window[this.interface]).forEach((el) => {
      if (!remain.includes(el)) {
        this.removeKey(el);
        delete this.internalCache[el];
      }
    });
  }

}

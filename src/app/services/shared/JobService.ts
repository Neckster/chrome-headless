import { Injectable }                     from '@angular/core';
import { BaseService }                    from './base.service';
import * as moment                        from 'moment';
import * as _                             from 'lodash';
import { range }                          from 'ramda';
import { and, or, prop as get, is, not }  from '../../lib/curried';
import { Maybe }                          from 'monet';


@Injectable()
export class JobService extends BaseService {

  public week;

  constructor() {
    super();
    this.week = 0;
  }

  public setWeek(week) {
    this.week = week;
  }

  public getWeek() { return this.week; }

  // Parse Time
  public getRealTime(
    dmy,
    time: string,
    isoWeek: string,
    day: number,
    year: number = moment().year()
  ) {
    const hour = parseInt(`${time[0]}${time[1]}`, 10);
    const minutes = parseInt(`${time[3]}${time[4]}`, 10);
    return moment(dmy)
      .year(year)
      .hour(hour)
      .minute(minutes)
      .second(0)
      .millisecond(0)
      .isoWeek(+isoWeek)
      .isoWeekday(day)
      .valueOf();

  }

  // check isLate
  public isLate(job, now) {
    // return f.isAfter(t);
    if (now < job.tFrom) return false;
    if (job.checkIn && job.checkIn < job.tFrom) return false;
    return !!((job.tFrom - job.checkIn) < -60000  || !job.checkIn);
  }

  // check is not come to work
  public isAbandon(job, now) {
    return !!(now > job.tTo && !job.checkIn && !job.checkOut);
  }

  // calc late time
  public calcLate(job, now) {
    let lateDiff = moment.utc(moment(job.checkIn)
      .diff(moment(job.tFrom)))
      .format('HH:mm');
    job.hours = {lateDiff: lateDiff};
    let lateH = lateDiff[0] === '0' ? lateDiff[1] : `${lateDiff[0]}${lateDiff[1]}`;
    let lateM = lateDiff[3] === '0' ? lateDiff[4] : `${lateDiff[3]}${lateDiff[4]}`;
    // job.tTo = moment(job.tTo).add(+lateH, 'h').add(+lateM, 'm').valueOf();
    return job;
  }

  getTimeFromDiff(msec) {
    let neg = 0 > msec;
    if (neg) {
      msec = -msec;
    }

    let min = Math.round(msec / 60000);
    let hrs = Math.floor(min / 60);
    min -= hrs * 60;
    return (neg ? '-' : '') + this.twoDigits(hrs) + ':' + this.twoDigits(
        min);
   }

  twoDigits = function(num) { // 2 digits from positive number
    return (0 > num || 9 < num ? '' : '0') + num;
  };

  // calc expected actual and diff hours
  public calcHours(job, now) {
    if (!job.hours) job.hours = {};
    let fromMoment = moment(job.tFrom);
    let toMoment = moment(job.tTo);

    // Check nextday
    if (fromMoment.unix() > toMoment.unix()) {
      toMoment.add('day', 1);
    }

    job.hours.planned = this.getTimeFromDiff(toMoment.diff(fromMoment));
    job.hours.plannedU = toMoment.diff(fromMoment);
    let idle = job.idlemanager || job.idleuser;
    idle = idle ? idle : '00:00';
    let idleU = this.calculateStringHours([idle]).milisecondTotal;
    // no chekin chekout
    if (!job.checkIn && !job.checkOut) {
      job.hours.actual = '00:00';
      job.hours.actualU = 0;
      job.hours.expected = job.hours.planned;
      job.hours.expectedU = job.hours.plannedU;
      job.hours.diff = '00:00';
      job.hours.diffU = 0;
      return job;
    }
    if (job.checkOut) {

      job.hours.actualU = moment(job.checkOut).diff(moment(job.checkIn)) - idleU;
      job.hours.actual = this.getTimeFromDiff(job.hours.actualU);
      job.hours.expected = job.hours.actual;
      job.hours.expectedU = job.hours.actualU;

      if (job.hours.plannedU === job.hours.actualU) {
        job.hours.diffU = 0;
        job.hours.diff = '00:00';
      }
        // if (job.hours.plannedU < job.hours.expectedU) {
        job.hours.diffU = job.hours.expectedU - job.hours.plannedU;
        job.hours.diff = this.getTimeFromDiff(job.hours.diffU);
      // } else {
      //   job.hours.diffU = (job.hours.plannedU - job.hours.expectedU) -
      //     2 * (job.hours.plannedU - job.hours.expectedU);
      //   job.hours.diff = this.getTimeFromDiff(job.hours.diffU);
      // }
    } else {
      job.hours.actualU = moment(now).diff(moment(job.checkIn));
      job.hours.actual = this.getTimeFromDiff(job.hours.actualU);
      job.hours.expected = job.hours.planned;
      job.hours.expectedU = job.hours.plannedU;
      job.hours.diff = '00:00';
      job.hours.diffU = 0;
    }
    if (job.hours.actual === '00:00') {
      job.hours.diff = '00:00';
      job.hours.diffU = 0;
    }
    return job;
  }

  // main method
  isTommorow(from, to) {
    let f = moment(from, 'HH:mm'),
        t = moment(to, 'HH:mm');
     return f.isAfter(t);

    /*let a = +`${from[0]}${from[1]}`;
    let b = +`${to[0]}${to[1]}`;
    return b <= a;*/
  }

  public calcJobStatuses(job, now, isoWeek) {
    // if (job.webuser_id === 132469 && job.from === '15:00' && job.to === '05:00') debugger;

    let nowC = moment(now).isoWeek(isoWeek).valueOf();
    // isoWeek = isoWeek - 1;
    if (this.isTommorow(job.from, job.to)) {
      let days = job.weekday + 1;
      /*let ff = moment(now).hours(+`${job.from[0]}${job.from[1]}`)
        .minute(+`${job.from[3]}${job.from[4]}`).valueOf();
      if (ff <= moment(now).hours(23).minutes(59).valueOf() &&
        ff >= moment('20:00', 'HH:mm').valueOf()) {
        job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday);
      } else {
        job.tFrom  = this.getRealTime(now, job.from, isoWeek, days);
      }*/
      // debugger;
      job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday);
      job.tTo = this.getRealTime(now, job.to, isoWeek, days);
    } else {
      job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday);
      job.tTo = this.getRealTime(now, job.to, isoWeek, job.weekday);
    }
    // debugger;
    if (!job.tcimanager && job.tciuser && job.citime) {
      if (job.tciuser < job.citime) {
        job.isClarify = true;
        job.chekInC = true;
      }else if (job.citime && !job.cotime) {
        job.isWorking = this.checkIfJobInPast(job);

      }
    }
    let a = this.isManagerChanged(job);

    if (!job.tcomanager && job.tcouser && job.cotime) {
      if (job.tcouser < job.cotime) {
        job.isClarify = true;
        job.checkOutC = true;
      }
    }
    // job.status = this.getStatus(now, job);
    job.idle = job.idlemanager ? job.idlemanager : job.idleuser;
    job.checkOut = job.tcomanager ? job.tcomanager : job.tcouser;
    job.checkIn = job.tcimanager ? job.tcimanager : job.tciuser;
// AutoCheckOut
    if (job.checkIn && !job.checkOut && now > moment(job.tTo).add(3, 'hours').valueOf()) {
      job.checkOut = moment(job.tTo).valueOf();
      job.checkOutC = true;
      job.isClarify = true;
      job.auto = true;
    }
    // if (job.to === '17:00') debugger
    if (job.stat && job.stat.declined) {
      return Object.assign({}, this.calcHours(job, now), {isClarify: true, status: 'declined'});
    }

    if (job.tcimanager && job.tcomanager) {
      job = this.calcHours(job, now);
      job.status = 'approve';
      return job;
    }
    if (now < job.tFrom) {
      return Object.assign({}, this.calcHours(job, now));
    }

    if (now > job.tTo) {
      if (this.isAbandon(job, now)) {
        // Is Abandon
        if (moment().isoWeek() >= +`${this.week[2]}${this.week[3]}`) {
          job.status = 'abandon';
          job.isClarify = !this.isManagerChanged(job);
        }
        job = this.calcHours(job, now);
        return Object.assign({}, job);
      }
      // Check if late
      if (this.isLate(job, now)) {
        // Is Late
        job.isLate = true;
        job = this.calcLate(job, now);
        job = this.calcHours(job, now);
          // Is Working but too late
          if (job.tTo === job.checkOut || job.tTo < job.checkOut) {
            // Spend all hours
            // job.isClarify = undefined;
            job.status = 'OK';
            job = this.calcHours(job, now);
            return Object.assign({}, job);
          } else if (job.checkOut < job.tTo) {
            // Not enough hours
            job = this.calcHours(job, now);
            job.isClarify = true;
            job.status = 'notEhour';
            return Object.assign({}, job);
          } else if (!job.checkOut) {
            job.status = 'OK';
            job.isWorking = true;
            return Object.assign({}, job);
          }
      } else {
        job.isWorking = !this.inPast(job);
        job = this.calcHours(job, now);
        if ((job.checkOut - job.tTo) < -60000) {
          // Not enogh hours
          job.isClarify = true;
          job.status = 'notEhour';
        } else {
          if (job.checkOut) {
            job.status = 'OK';
          } else {
            job.status = 'curentlyWorking';
            job.isWorking = true /*!this.inPast(job)*/;
          }
        }
        return Object.assign({}, job);
      }
    } else {
      // Currently must working
      if (this.isLate(job, now)) {
          job = this.calcLate(job, now);
          job.isLate = this.checkIfJobInPast(job);
        if (job.checkIn === undefined) {
          job.status = 'notInWork';
        } else {
          if (!job.checkOut) {
            job.status = 'curentlyWorking';
            // job.isClarify = undefined;
            job.isWorking = !this.isManagerChanged(job);
          } else {
            if ((job.checkOut - job.tTo) < -60000) {
              job.status = 'notEhour';
              job.isClarify = true;
            } else {
              job.status = 'OK';
            }
          }
        }
        job = this.calcHours(job, now);
        return Object.assign({}, job);
      } else {
        if (!job.checkOut) {
          job.isWorking = true;
          job.status = 'OK';
          job = this.calcHours(job, now);
        } else {
          job = this.calcHours(job, now);
          job.status = job.checkOut < job.tTo ? 'notEhour' : 'OK';
          job.isClarify = !this.isManagerChanged(job);
        }
        return Object.assign({}, job);
      }
    }
    return Object.assign({}, job);
  }

  checkIfJobInPast(job) {
    let to = moment(job.tTo);
    return moment().isBefore(to);
  }


  isManagerChanged(job: any) {
    if (!job) return;
    let lastLog = job.log ?
        job.log[job.log.length - 1] : undefined;
    if (lastLog) {
      return lastLog[0] === 'accept' ||
      lastLog[0] === 'accepted' ||
      lastLog[0] === 'amend' ||
      lastLog[0] === 'cancel' ?
        true : false;
    }else {
      return false;
    }

  }

  inPast(job) {
    let to = job.to.split(':'),
        toTime = moment(`${this.week}${job.weekday}`, 'GGWWE')
            .add(to[0], 'h')
            .add(to[1], 'm')
            .add(3, 'h');
    return moment().isAfter(toTime);
  }

  calcDiff(hours, now) {
    if (hours.lateDiff) {
      let dH = +`${hours.lateDiff[0]}${hours.lateDiff[1]}`;
      let dM = +`${hours.lateDiff[3]}${hours.lateDiff[4]}`;
      let h = +`${hours.planned[0]}${hours.planned[1]}`;
      let ddH = +`${hours.diff[0]}${hours.diff[1]}`;
      let ddM = +`${hours.diff[3]}${hours.diff[1]}`;
      let m = +`${hours.planned[3]}${hours.planned[4]}`;
      let planned = moment(now).hours(h).minutes(m);
      hours.planned = planned.subtract(dH, 'h').subtract(dM, 'm').format('HH:mm');
      let diff = moment(now).hours(ddH).minutes(ddM);
      hours.diff = diff.subtract(dH, 'h').subtract(dM, 'm').format('HH:mm');
      if (hours.flag)
        hours.diff = '00:00';
    }
    return hours;
  }

  emptyBubblesArraForWeek() {
    return range(0, 7).map(el => {
      return {'clarify': [], 'late': [], 'working': []};
    });
  }
  calcBubblesForPosition(position) {
    let aq = position.jobbers.map(jobber => {
      // debugger;
      let weekday =  jobber.job.weekday ?
              jobber.job.weekday : jobber.job._weekday,
          bubblesOnDayArr = this.emptyBubblesArraForWeek();
      if (jobber.job.isClarify) {
        bubblesOnDayArr[weekday - 1].clarify.push(jobber);
      }
      if (jobber.job.isLate && !jobber.job.checkIn) {
        bubblesOnDayArr[weekday - 1].late.push(jobber);
      }

      if (jobber.job.isWorking) {
        bubblesOnDayArr[weekday - 1].working.push(jobber);
      }
      return {
        clarify: jobber.job.isClarify ? 1 : 0,
        late: (jobber.job.isLate && !jobber.job.checkIn) ? 1 : 0,
        working: jobber.job.isWorking ? 1 : 0,
        bubblesOnDayArr: bubblesOnDayArr
      };
    });
     return aq.reduce((a, b) => {
      let bubblesOnDayArr = this.emptyBubblesArraForWeek(),
          aa = {},
          arr = [],
          aArr = a.bubblesOnDayArr || bubblesOnDayArr,
          bArr = b.bubblesOnDayArr || bubblesOnDayArr;
      arr = aArr.map((el, i) => {
          el.clarify = el.clarify.concat(bArr[i].clarify);
          el.late = el.late.concat(bArr[i].late);
          el.working = el.working.concat(bArr[i].working);
        return el;
        });
      return {
         clarify: (a.clarify ? a.clarify : 0 ) + (b.clarify ? b.clarify : 0 ),
         late: (a.late ? a.late : 0 ) + (b.late ? b.late : 0 ),
         working: (a.working ? a.working : 0 ) + (b.working ? b.working : 0 ),
         bubblesOnDayArr: arr
       };
    }, 0);
  }



  getHoursMinutes(hours) {
    // let hours = {planned: undefined, actual: undefined, diff: undefined};
    if (hours.planned.length < 5) hours.planned = '0'.concat(hours.planned);
    if (hours.actual.length < 5) hours.actual = '0'.concat(hours.actual);
    if (hours.diff.length < 5) hours.diff = '0'.concat(hours.diff);
    let pH = hours.planned[0] === '0' ?
      +`${hours.planned[1]}` :
      +`${hours.planned[0]}${hours.planned[1]}`;
    let pM = hours.planned[3] === '0' ?
      +`${hours.planned[4]}` :
      +`${hours.planned[3]}${hours.planned[4]}`;
    let pMinutes = pH * 60 + pM;

    let aH = hours.actual[0] === '0' ?
      +`${hours.actual[1]}` :
      +`${hours.actual[0]}${hours.actual[1]}`;
    let aM = hours.actual[3] === '0' ?
      +`${hours.actual[4]}` :
      +`${hours.actual[3]}${hours.actual[4]}`;
    let aMinutes = aH * 60 + aM;

    let dH = hours.diff[0] === '0' ?
      +`${hours.diff[1]}` :
      +`${hours.diff[0]}${hours.diff[1]}`;
    let dM = hours.diff[3] === '0' ?
      +`${hours.diff[4]}` :
      +`${hours.diff[3]}${hours.diff[4]}`;
    let dMinutes = dH * 60 + dM;
    return {actual : aMinutes, diff: dMinutes, planned: pMinutes};
  }

  calculateStringHours (data) {
    let breakPerPosition = moment(0);
    let stringTotal: any;
    data.forEach(stringTime => {
      breakPerPosition.add(stringTime.split(':')[0], 'h')
          .add(stringTime.split(':')[1], 'm');
    });
    let dif = breakPerPosition.diff(moment(0));
    let duration = moment.duration(dif);
    stringTotal = Math.floor(duration.asHours()) +
      moment.utc(duration.asMilliseconds()).format(':mm');
    return { stringTotal, milisecondTotal: duration.asMilliseconds() };
  }

  calcHoursForPosiiton(position) {
    let minutes = undefined;
    let hours = {
      planned: undefined, plannedU: undefined,
      actual: undefined, actualU: undefined,
      diff: undefined, diffU: undefined,
      expected: undefined, expectedU: undefined,
      breakh: undefined, breakU: undefined
    };
    // breakh
    let breakU = position.jobbers
      .map(jobber => {return jobber.job.idlemanager || jobber.job.idleuser; })
      .filter(el => !!el);
    hours.breakh = this.calculateStringHours(breakU).stringTotal;
    hours.breakU = this.calculateStringHours(breakU).milisecondTotal;
    // planned
    let plannedU = position.jobbers
      .map(jobber => {return jobber.job.hours.plannedU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.planned = this.getTimeFromDiff(plannedU);
    hours.plannedU = plannedU;
    // actual
    let actualU = position.jobbers
      .map(jobber => {return jobber.job.hours.actualU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.actual = this.getTimeFromDiff(actualU);
    hours.actualU = actualU;
    // diff
    let diffU = position.jobbers
      .map(jobber => {return jobber.job.hours.diffU; })
      .reduce((a, b) => {return a + b; }, 0);

    hours.diff =  this.getTimeFromDiff(diffU);
    hours.diffU = diffU;
    // expected
    let expectedU = position.jobbers
      .map(jobber => {return jobber.job.hours.expectedU; })
      .reduce((a, b) => {return a + b; }, 0);
    hours.expected = this.getTimeFromDiff(expectedU);
    hours.expectedU = expectedU;
    return hours;
  }

  calcShift(shift) {
    if (shift.positions.length < 1 || !shift.active) return shift;

    let positions = shift.positions.map(position => {
      return Object.assign({}, position, {
        hours: this.calcHoursForPosiiton(position),
        bubbles: this.calcBubblesForPosition(position)
      });
    });
    let bubbles = positions.map(pos => {return pos.bubbles; }).reduce((a, b) => {
      let res = {};
      let aArr = a.bubblesOnDayArr || this.emptyBubblesArraForWeek(),
          bArr = b.bubblesOnDayArr || this.emptyBubblesArraForWeek(),
          arr = aArr.map((el, i) => {
            el.clarify = el.clarify.concat(bArr[i].clarify);
            el.late = el.late.concat(bArr[i].late);
            el.working = el.working.concat(bArr[i].working);
            return el;
          });

      if (a === 0) {
        res = {
          clarify: a + b.clarify,
          late: a + b.late,
          working: a + b.working,
          bubblesOnDayArr: arr
        };
      } else {
        res = {
          clarify: a.clarify + b.clarify,
          late: a.late + b.late,
          working: a.working + b.working,
          bubblesOnDayArr: arr
        };
      }
      return res;
    });

    let hours = positions.map(pos => {return pos.hours; }).reduce((a, b) => {
        let res = {
          plannedU: undefined,
          actualU: undefined,
          diffU: undefined,
          expectedU: undefined
        };
        if (a === 0) {
          res = {
            actualU: a + b.actualU,
            diffU: a + b.diffU,
            plannedU: a + b.plannedU,
            expectedU: a + b.expectedU
          };
        } else {
          res = {
            actualU: a.actualU + b.actualU,
            diffU: a.diffU + b.diffU,
            plannedU: a.plannedU + b.plannedU,
            expectedU: a.expectedU + b.expectedU
          };
        }
        return res;
      });

    let resHours = {
      plannedU: hours.plannedU,
      actualU: hours.actualU,
      diffU: hours.diffU,
      expectedU: hours.expectedU,
      planned: this.getTimeFromDiff(hours.plannedU),
      actual: this.getTimeFromDiff(hours.actualU),
      diff: this.getTimeFromDiff(hours.diffU),
      expected: this.getTimeFromDiff(hours.expectedU),
      breakh: this.calculateStringHours(positions.map(pos => pos.hours.breakh)).stringTotal
    };
    return Object.assign({}, shift, {positions, bubbles, hours: resHours});
  }

  calcAssignments(assigns, meta, flag = true) {
    let now = moment(meta);
    let res: JobI[] = [{webuser_id: undefined, status: undefined}];
    let result = assigns
      .map((job, index) => {
        let isoWeek = ('' + job.week).slice(-2);
        let iso = +isoWeek;
        let year = parseInt('20' + ('' + job.week).substr(0, 2), 10);
        // isoWeek = `${iso - 1}`;
        let checkIn = job.tcimanager ? job.tcimanager : job.tciuser;
        let checkOut = job.tcomanager ? job.tcomanager : job.tcouser;
        job.tFrom = this.getRealTime(meta, job.from, isoWeek, job.weekday, year);
        if (this.isTommorow(job.from, job.to)) {
          let days = job.weekday + 1;

          /*if (!this.checkFromIsOnNextDay(job)) {
            job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday, year);
          } else {
            job.tFrom  = this.getRealTime(now, job.from, isoWeek, days, year);
          }*/
          job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday, year);
          job.tTo = this.getRealTime(now, job.to, isoWeek, days, year);
        } else {
          job.tFrom = this.getRealTime(now, job.from, isoWeek, job.weekday, year);
          job.tTo = this.getRealTime(now, job.to, isoWeek, job.weekday, year);
        }
        // AutoCheckOut
        if (checkIn && !checkOut && meta > moment(job.tTo).add(3, 'hours').valueOf()) {
          job.autoCheckOut = true;
          checkOut = job.tTo;
        }
        job.checkIn = checkIn;
        job.checkOut = checkOut;
        if (now > job.tTo) {
          if ((checkIn && checkOut) || (!checkIn && !checkOut))
            job.status = 'past';
          if (checkIn && !checkOut) {
            job.status = 'needToCheckOut';
            res[0] = job;
          }
          // if (!checkIn && !checkOut) {
          //   job.status = 'past';
          //   res.push(job);
          // }
        } else if (now < job.tFrom) {
          if (checkIn && !checkOut) {
            job.status = 'inWork';
            res[0] = job;
          } else {
            if (meta >= moment(job.tFrom).subtract(15, 'minutes').valueOf()) {
              job.status = 'canCheckIn';
              res.push(job);
            } else {
              job.status = 'cantCheckIn';
              res.push(job);
            }
          }
        } else {
            if (!checkIn) {
              job.status = 'canCheckIn';
              res.push(job);
            } else  {
              if (checkOut) {
                job.status = 'past';
              } else {
                job.status = 'canCheckOut';
                res[0] = job;
              }
            }
        }
        if (job.stat && job.stat.checkedout && job.stat.checkedout === 1 && job.checkOut)
          job.status = 'past';
        if (job.checkOut && job.checkIn) job.status = 'past';
        return job;
    });
    if (!res[0].status) {
      res = res.filter((el: JobI) => {return el.hasOwnProperty('status'); });
    }
    if (flag) return result;
    return res;
  }



  structPositions(shiftIndex, unit, day, info, currentMonthISOWeek) {
     return _.values(unit.positions).filter((position: PositionI) => {
      return position.staff;
    }).map((position: PositionI, index) => {
      if (position.staff[shiftIndex] instanceof Array || unit.job instanceof Array) {
        let arrOfAssign = [];
        if (unit.job instanceof Array) {
          arrOfAssign = unit.job.filter(el => {
            return el.position_id === position.position_id && el.shift_idx === shiftIndex;
          });
        }
        let usersWithAssignment = _.uniqBy(arrOfAssign, 'webuser_id')
          .filter((user: JobI) => user.webuser_id !== 0)
          .map((user: JobI) => user.webuser_id);
        let staffId = [];
       if (position.staff[shiftIndex] instanceof Array)
          staffId = position.staff[shiftIndex];

        let allStaffPerPosition: any = _.union(usersWithAssignment, staffId);
        let jobbers = allStaffPerPosition
          .map(el => {
            return unit.staff[el];
          })
          .filter(el => {
            return el;
          });
        let jobs = unit.job.filter(job => {
          if (job.position_id === position.position_id
            && job.shift_idx === shiftIndex
            && job.weekday === day
            && (!job.stat || (job.stat && !job.stat.declined))) {
            return jobbers.filter((el: JobI) => {
              return el.webuser_id === job.webuser_id;
            }).length > 0 ? true : false;
          }
          return false;
        }).filter(job => {
          return job.position_id === position.position_id
            && job.shift_idx === shiftIndex
            && job.published;
        });
        let jobbersWithWork = jobbers.map(el => {
          let res = jobs.filter((job: JobI) => {
            return job.webuser_id === el.webuser_id;
          });
          if (res.length > 0) {
            return Object.assign({}, el, {job: res[0]});
          }
          return el;
        }).filter(el => {
          return el.hasOwnProperty('job');
        }).filter(jobber => {
          let t = jobber.job.position_id === position.position_id;
          let s = jobber.job.shift_idx === shiftIndex;
          return s && t && jobber.job.weekday === day;
        }).map(jobber => {
            let job = this.calcJobStatuses(jobber.job,
              info.meta.time,
              currentMonthISOWeek);

            return Object.assign({}, jobber, {job: job });
          }).filter(jobber => {
            let job = jobber.job;
            return true;
          // return !job.stat ||
            //   (!job.stat.hasOwnProperty('deleted') &&
            //   !job.stat.hasOwnProperty('declined'));
          });

        return {position: position, jobbers: jobbersWithWork};
      } else return {position: position, jobbers: []};
    }).filter(el =>  el.jobbers.length);
  }

  getStaff(ass, info) {
    return Maybe.fromNull(info)
      .filter(and([
        (c) => get('colleague')(c),
        (c) => Array.isArray(get('colleague')(c))
        ]))
      .bind((col) =>
        col.colleague.filter(and([
          (c) => is(ass.shift_idx)(get('shift_idx')(c)),
          (c) => is(ass.weekday)(get('weekday')(c)),
          (c) => is(ass.week)(get('week')(c)),
          (c) => is(ass.position_id)(get('position_id')(c)),
          (c) => not(is(ass.webuser_id)(get('webuser_id')(c)))
        ])));
  }

  hideScrollBar() {
    let htmlTag = document.getElementsByTagName('html'),
      htmlClass = htmlTag[0].classList.toggle('uk-modal-page');
/*    let modal  = document.getElementsByClassName('uk-modal'),
      htmlTag = document.getElementsByTagName('html');
    if (modal.length) {
      let htmlClass = htmlTag[0].classList.add('uk-modal-page');
    }else if(!modal.length) {
      let htmlClass = htmlTag[0].classList.remove('uk-modal-page');*/
  }

  isNextAssignment(first: any, second: any) {
    if (first && second) {
      return first.shift_idx === second.shift_idx || first.shift_idx + 1 === second.shift_idx;
    }
    return false;
  }

  checkFromIsOnNextDay(job) {
    return false; // +moment(job.tFrom).format('HH') < 5 && job.shift_idx === 3;
  }
}

interface PositionI {
  staff: any;
  position_id: any;
}

interface JobI {
  webuser_id: any;
  status: any;
}

import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LuaApiHttp } from './lua-http-api.service';
import { UserEnroll } from '../../lib/interfaces/user/enroll';
import { NodeApiHttp } from './node-http-api.service';


interface IServerResponse {
  user: string;
  message: string;
  webuser: any;
}

@Injectable()
export class AuthService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp, private nodeApiHttp: NodeApiHttp) {
    super();
  }

  public login(user: string, password: string): Observable<IServerResponse> {
    return this.apiHttp.post('auth/login', JSON.stringify({
      user, password
    })).map((res) => res.json());
  }

  public activateNodeSession(webuserId) {
    return this.nodeApiHttp.post('auth/session', JSON.stringify({webuser_id: webuserId}))
      .map(res => res.json());
  }

  public dropNodeSession() {
    return this.nodeApiHttp.post('auth/session/drop', JSON.stringify({}))
      .map(res => res.json());
  }

  public logout(): Observable<Response> {
    return this.apiHttp.post(`auth/logout`, '').map((res) => res.json());
  }

  public updatePassword(token: string, password: string, password2: string): Observable<Response> {
    return this.apiHttp.post(`auth/update`, JSON.stringify({
      token, password, password2
    })).map((res) => res.json());
  }

  public reactivateEmail(email: string): Observable<Response> {
    return this.apiHttp.post(`auth/reactivate`, JSON.stringify({
      email
    })).map((res) => res.json());
  }

  public enroll(user: UserEnroll): Observable<Response> {
    return this.apiHttp.post(`auth/enroll`, JSON.stringify(user)).map((res) => res.json());
  }

}

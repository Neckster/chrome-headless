import { Injectable }   from '@angular/core';
import { Observable }   from 'rxjs/Observable';


interface Callback { (data: any): void; }

declare class EventSource {
  onmessage: Callback;
  onerror: Callback;
  close(): void;
  addEventListener(event: string, cb: Callback): void;
  constructor(name: string, options: any);
}


@Injectable()
export class EventStream {

  private allowStream: boolean = false;

  constructor() {
    if ((<any>window).EventSource) {
      this.allowStream = true;
    }
  }

  get allow() {
    return this.allowStream;
  }

  createStream(hash: string = ''): Observable<any> {
    return Observable.create(observer => {
      const eventSource = new EventSource(
        `https://stage.jubbr.com/api/push/subscribe?events=${hash + '+'}broadcast`,
        { withCredentials: true }
      );
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);
      return () => {
        eventSource.close();
      };
    });
  }

}

import { Injectable }   from '@angular/core';

// Custom typing for the browser's Notification interface
declare class Notification {
  public static permission: SNotificationStatus;
  public static requestPermission(): Promise<any>;
  constructor(title: string, params);
  public close(): void;
}

// String-based enum
// https://basarat.gitbooks.io/typescript/content/docs/types/stringLiteralType.html
type SNotificationStatus = 'default' | 'granted' | 'denied';

// SNotificationStatus-based string literals
const DEFAULT: string = 'default';
const GRANTED: string = 'granted';
const DENIED: string = 'denied';

@Injectable()
export class NotificationHandler {

  private permissionStatus: SNotificationStatus;
  private allowShowing: boolean;

  constructor() {

    const getNotificationsStatus = (notification): Promise<SNotificationStatus> => {
      const notificationsStatus: SNotificationStatus = notification.permission;
      if (notificationsStatus === DEFAULT) {
        return notification.requestPermission();
      } else {
        return Promise.resolve(notificationsStatus);
      }
    };

    if (!(<any>window).Notification) {
      this.allowShowing = false;
      return; }


    getNotificationsStatus(Notification).then((status: SNotificationStatus) => {
      this.permissionStatus = status;
      this.allowShowing = status === GRANTED;
    }).catch((notificationError) => {
      this.allowShowing = false;
      console.log(notificationError);
    });

  }

  show(title: string, options) {
    if (this.allowShowing) {
      let note = new Notification(title, options);
      // automatically close notification in Chrome, etc.
      setTimeout(note.close.bind(note), 5000);
    }
  }

}

import { Injectable } from '@angular/core';
import { select } from 'ng2-redux/lib/index';
import { AppStoreAction } from '../../store/action';

@Injectable()
export class ModalApiService {

  // @select(state => state.openedModals) modals$: any;

  constructor(private action: AppStoreAction) {
    // this.modals$.subscribe(console.log);
  }

  open(id: string) {
    this.action.openModal(id);
  }

  close(id: string = undefined) {
    this.action.closeModal(id);
  }

}

import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { TargetJobService } from '../job.service';
import { CacheService } from '../shared/CacheService';
import { AppStoreAction } from '../../store/action';
import { IconState } from '../../lib/enums/IconState';


@Injectable()
export class ScheduleService extends BaseService {
  public static unavailablePositionClass = 'ostatus_off empty sstatus_current';
  public week;
  constructor(public targetService: TargetJobService,
              public action: AppStoreAction) {
    super();
    this.week = 0;
  }
  public setWeek(week) {
    this.week = week;
  }

  iterateDayAvalible(shiftIndex) {
    let shiftAvalible;
    switch (shiftIndex) {
      case 1: shiftAvalible = 7;
        break;
      case 2: shiftAvalible = 14;
        break;
      case 3: shiftAvalible = 21;
        break;
      default: shiftAvalible = 0;
    }
    return shiftAvalible;
  }


  // new controller
  initScheduleAss(currentPosition, shiftIndex, currentUnit, jobsOnWork?) {
    let staff = currentPosition.staff ? currentPosition.staff[shiftIndex] : [];
    let usersWithAssignment = [];
    let publisedShPosAss = [];
    let jobs = currentUnit.job;

    let emptyAssignment = [{}, {}, {}, {}, {}, {}, {}];

    let daysInfo = emptyAssignment.map((a, day) => {
      return Object.assign({},
        { timeStatus: this.checkDateStatus(day + 1, currentUnit.shift[shiftIndex])});
    });
    // get only staff-grid job
    if (jobs instanceof Array) {
      publisedShPosAss = jobs.filter(assign => {
        // delete  past assignment which is not published
        if (assign.shift_idx === shiftIndex &&
          assign.published === false &&
          daysInfo[assign.weekday - 1].timeStatus === 'time-past' &&
          assign.position_id === currentPosition.position_id &&
          assign.webuser_id !== 0) {
          this.deleteAssign(assign);
        } else {
          return assign.shift_idx === shiftIndex &&
            assign.position_id === currentPosition.position_id;
        }
      });
      usersWithAssignment = _.uniqBy(publisedShPosAss, 'webuser_id')
        .filter((user: JobI) => user.webuser_id !== 0)
        .map((user: JobI) => user.webuser_id);
    }
    daysInfo = daysInfo.map((dEl, i) => {
      return this.init(i, publisedShPosAss, dEl);
    });
    // calculate all information about staff-grid to show on top

    staff = _.union(staff, usersWithAssignment);
    let shiftUnpublished = 0;
    let shiftUnpublishArr = [];
    let jobberPerShift = [];
    // create jobber information
    if (staff.length && currentUnit.staff) {
      jobberPerShift = staff.map(staffId => {
        if (currentUnit.staff[staffId]) {
          let jobber = currentUnit.staff[staffId];
          let emptyassignment = [{}, {}, {}, {}, {}, {}, {}];
          jobber.assign = emptyassignment.map((assign, indexDay) => {
            let shiftAvailable = this.iterateDayAvalible(shiftIndex);
            let binary = currentUnit.staff[staffId].available ?
              currentUnit.staff[staffId].available[shiftAvailable + indexDay] : 0;
            return Object.assign({}, assign, {
              classAssignment: this.checkAvailability(binary),
              daysInfo: daysInfo[indexDay].timeStatus,
              dayAssignments: jobsOnWork && jobsOnWork[jobber.webuser_id]
              && jobsOnWork[jobber.webuser_id][indexDay]
            });
          }).sort((a: any, b: any) => {
            return (a.uname > b.uname) ? 1 : ((b.uname > a.uname) ? -1 : 0);
          });
          return Object.assign({}, jobber);
        }
      }).filter(el => el).sort(function (a, b) {
        return a.uname.localeCompare(b.uname);
      });
      if (jobs instanceof Array) {
        let unpub  = this.checkUnpublished(daysInfo);
        shiftUnpublished = unpub.counter;
        shiftUnpublishArr = unpub.info;
      }
    }
    return Object.assign({}, {}, {jobberPerShift,
      shiftUnpublished, daysInfo, publisedShPosAss, shiftUnpublishArr});

  }

  checkUnpublished(daysInfo) {
    let counterUnpub = 0,
        infoArray = [];
    for (let i = 0; i < 7; i++) {
      if (daysInfo[i].unpublishedDay)
        counterUnpub += daysInfo[i].unpublishedDay;
        infoArray.push(daysInfo[i].unpublishedDay);
    }
    return {counter: counterUnpub,
            info: infoArray };
  }


  init(i, publisedShPosAss, dEl?, shift?) {
    let timeStatus;
    if (!dEl)
      timeStatus = {timeStatus: this.checkDateStatus(i + 1, shift)};
    let pendingCount = 0;
    let totalMm = 0;
    let currentDayAssign = {};
    // created object with days
    publisedShPosAss.forEach(pEl => {
      if (pEl.webuser_id !== 0 && pEl.weekday === i + 1) {
        currentDayAssign[pEl.webuser_id] = pEl;
      }
    });
    let unpublishedDay = 0;
    let dayAssigns = [];
    _.values(currentDayAssign).forEach((el: any) => {
      el.curAsClass = this.assigmnetClass(el);
      if (!el.stat || !el.stat.declined) {
        unpublishedDay += el.published ? 0 : 1;
        if (el.pending && el.pending === 'true') {
          pendingCount++;
        } else {
          totalMm += this.parseStrToMm(el.to, el.from);
          dayAssigns.push(el);
        }
      }
    });
    let totalDayTime = this.parseMsToStr(totalMm);
    let targetInfo = publisedShPosAss
      .filter(el => el.weekday === i + 1 && el.webuser_id === 0)[0];
    let targetStatus = !!(pendingCount || targetInfo &&
    this.checkStat(targetInfo.min_job_count, dayAssigns.length,
      totalDayTime, targetInfo.max_work_time));
    return Object.assign({}, dEl, timeStatus, {dayAssigns: dayAssigns.length, pendingCount,
      totalDayTime, targetInfo, targetStatus, currentDayAssign, unpublishedDay});
  }

  checkAvailability(availableStatus) {
    if (availableStatus !== 1)
      return ScheduleService.unavailablePositionClass;
    return 'empty sstatus_current';
  }
  //  heck what assigmant status and return class for it
  assigmnetClass (assignment) {
      if (!assignment.published && !( assignment.stat && assignment.stat.declined ) ) {
        return 'unpub needci';
      } else if (assignment.stat && assignment.stat.declined ) {
        return `declined`;
      } else if (assignment.chef && assignment.from) {
        return `chef`;
      }
    return 'empty sstatus_current';
  }

  // delete unpublished assign
  deleteAssign(assign) {
    this.targetService
      .deleteAssign(assign.weekday,
        assign.shift_idx, assign.position_id,
        assign.webuser_id, this.week, 1)
      .subscribe();
  }

  // response for active staff-grid assignment and target status
  checkDateStatus(weekday, shift) {
    let currentTime = +moment();
    let activeLine: number;
    let shiftLine = shift.to.split(':');

    let correction = -1;

    if (shift.name === 'Nacht' &&
      +shift.to.slice(0, 1) < 24) {
      correction = 0;
    }

    activeLine = +moment(this.week, 'GGWWE')
      .add(weekday + correction, 'd')
      .add(shiftLine[0], 'h')
      .add(shiftLine[1], 'm');

    return currentTime <= activeLine ? 'time-future' : 'time-past';
  }

  // calculate for each day assigned hours
  parseStrToMm(thhmm, fhhmm) {
    let to: any;
    if (thhmm.split(':')[0] < fhhmm.split(':')[0]) {
      to = moment(0)
        .add(1, 'd')
        .add(thhmm.split(':')[0], 'h')
        .add(thhmm.split(':')[1], 'm');
    } else {
      to = moment(0)
        .add(thhmm.split(':')[0], 'h')
        .add(thhmm.split(':')[1], 'm');
    }
    let from: any = moment(0)
      .add(fhhmm.split(':')[0], 'h')
      .add(fhhmm.split(':')[1], 'm');
    let diff = to.diff(from);
    return diff;
  }

  parseMsToStr(mm) {
    let duration = moment.duration(mm);
    return Math.floor(duration.asHours()) +
      moment.utc(duration.asMilliseconds()).format(':mm');
  }

  // show red black status indicator for target
  checkStat(minJobCount, totalJobCount, totalWorkTime, maxWorkTime) {
    let count = !!minJobCount ? totalJobCount < minJobCount ? true : false : false;
    let hour = !!maxWorkTime ?
      +totalWorkTime.replace(':', '') > maxWorkTime * 100 ? true : false : false;
    return count || hour;
  }

  isEmpty(arrEmpty) {
    let count = 0;
    for (let key in arrEmpty) {
      if (arrEmpty[key] !== '0:00') {
        count++;
      }
    }
    return count === 0;
  }

  cstat(weekday, shiftObj) {
    let boolStateArr = [];
    let objEmpty = {};
    for (let i = 0; i < 4; i++) {
      boolStateArr.push(shiftObj[i].shiftInfo.daysInfo[weekday].targetStatus);
      objEmpty[i] = shiftObj[i].shiftInfo.daysInfo[weekday].totalDayTime;
    }
    return this.checkCstat(weekday, boolStateArr );
  }

  // deprecate this in favour of assignIconClass()
  checkCstat(weekday, boolStateArr, posEmpty?) {
    if (boolStateArr.includes(true)) {
      return 'cstat_bad ' + this.checkChefFromTo(weekday);
    } else if ( boolStateArr.includes(false) ) {
      return 'cstat_good ' + this.checkChefFromTo(weekday);
    } else {
      return 'cstat_empty ' + this.checkChefFromTo(weekday);
    }
  }

  assignIconClass(states: IconState[], weekday?: number, tFrom?: number) {
    let className = states.includes(IconState.Bad) ?
      'cstat_bad' : states.includes(IconState.Need) ?
      'cstat_need' :  states.includes(IconState.Good) ?
      'cstat_good' : 'cstat_empty';

    if (Number.isInteger(weekday))
      className += ' ' + this.checkChefFromTo(weekday, tFrom);
    return className;

  }

  checkChefFromTo(day: number, tFrom?: number) {
    let tto = moment(this.week, 'GGWWE');
    tto.add(day, 'd');
    if (tFrom) {
      tto.add(tFrom, 's');
    }
    return moment().startOf('day').unix() <= tto.startOf('day').unix()
        ? 'time-future'
        : 'time-past';
  }

}

interface JobI {
  webuser_id: any;
}


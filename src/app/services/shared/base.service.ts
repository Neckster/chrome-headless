interface IDictionary<T> {
  [K: string]: T;
}

export class BaseService {

  public apiRoutes: IDictionary<string> = {
    'logout': 'auth/logout',
    'updatePassword': 'auth/update',
    'reactivateEmail': 'auth/reactivate',
    'enroll': 'auth/enroll'
  };

  constructor() {}
  }

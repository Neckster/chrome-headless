import { ApiHttp }    from './http-api.service';
import { Injectable, provide } from '@angular/core';
import {
  Http,
  Headers,
  RequestOptionsArgs,
  ConnectionBackend,
  RequestOptions,
  Request,
  Response,
  XHRBackend,
  BrowserXhr,
  BaseRequestOptions,
  BaseResponseOptions,
  ResponseOptions
} from '@angular/http';

declare let __NODE_API__: any;

@Injectable()
export class NodeApiHttp extends ApiHttp {
  public baseUrl: string;


  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
    this.baseUrl = __NODE_API__ ? __NODE_API__() : 'http://www.jubbr.com/nodeapi/api';
  }

  jsonToQueryString(json) {
        function objectToQuery(data, prefix) {
            return Object.keys(data).map(function (key) {
                let value = data[key];
                key = encodeURIComponent(key);
                if (prefix) {
                    key = prefix + '[' + key + ']';
                }
                if (typeof value !== 'object') {
                    value = encodeURIComponent(value);

                    return key + '=' + value;
                } else {
                    return objectToQuery(value, key);
                }
            }).join('&');
        }
        return '?' + objectToQuery(json, '');
    }
}

export const NODE_API_HTTP_PROVIDERS: any[] = [
  provide(NodeApiHttp,
    {
      useFactory: (xhrBackend, requestOptions) => new NodeApiHttp(xhrBackend, requestOptions),
      deps: [XHRBackend, RequestOptions]
    }),
  BrowserXhr,
  provide(RequestOptions, {useClass: BaseRequestOptions}),
  provide(RequestOptions, {useClass: BaseRequestOptions}),
  provide(ResponseOptions, {useClass: BaseResponseOptions}),
  XHRBackend
];

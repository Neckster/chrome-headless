import {
  Http,
  Headers,
  RequestOptionsArgs,
  ConnectionBackend,
  RequestOptions,
  Request,
  Response
} from '@angular/http';

import { Observable } from 'rxjs/Observable';


export abstract class ApiHttp extends Http {
  public baseUrl: string;

  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  public send(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, options);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(options);

    return super.request(`${this.baseUrl}/${url}`, options);
  }

  /**
   * Performs a request with `getUnit` http method.
   */
  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(Object.assign({}, options, { method: 'GET' }));

    return super.get(`${this.baseUrl}/${url}`, options);
  }

  /**
   * Performs a request with `post` http method.
   */
  public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(Object.assign({}, options, { method: 'POST' }));

    return super.post(`${this.baseUrl}/${url}`, body, options);
  }

  /**
   * Performs a request with `put` http method.
   */
  public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(Object.assign({}, options, { method: 'PUT' }));

    return super.put(`${this.baseUrl}/${url}`, body, options);
  }

  /**
   * Performs a request with `delete` http method.
   */
  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(Object.assign({}, options, { body: '', method: 'DELETE' }));

    return super.delete(`${this.baseUrl}/${url}`, options);
  }

  /**
   * Performs a request with `patch` http method.
   */
  public patch(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(options);

    return super.patch(`${this.baseUrl}/${url}`, body, options);
  }

  /**
   * Performs a request with `head` http method.
   */
  public head(url: string, options?: RequestOptionsArgs): Observable<Response> {

    options = this.prepareOptions(options);

    return super.head(`${this.baseUrl}/${url}`, options);
  }

  /**
   * Adding authorization headers
   *
   * @param options
   * @returns {RequestOptionsArgs}
   */
  private prepareOptions(options?: RequestOptionsArgs) {

    let basicOptions: RequestOptionsArgs = {
      headers: new Headers({
        'Content-type': 'application/json',
        'Authorization': 'Basic c3RhZmY6cHJldmlldw==',
        'Accept': `application/vnd.jubbr.v1+json`
      })
    };

    let reqOptions = new RequestOptions(basicOptions);
    if (options) reqOptions = Object.assign({}, reqOptions, options);

    if (options.method === 'GET') // https://github.com/angular/angular/issues/10612
      reqOptions = Object.assign({}, reqOptions, { body: '' });
    return reqOptions;
  }
}

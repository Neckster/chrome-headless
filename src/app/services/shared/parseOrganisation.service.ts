/**
 * Created by user on 26.05.16.
 */
import { Injectable, provide } from '@angular/core';
import { Directory } from '../../manager/organisation-tree/tree-view/directory';
import * as moment  from 'moment/moment';
import * as _ from 'lodash';

const TAB: string = '&nbsp;&nbsp;&nbsp;';

@Injectable()
export class ParseOrganisationService {
  static currweek: any;
  static addModalData;
  static nonSelect: boolean;
  static currCompany: any;
  static data: any;
  static treeData;
  static isEditCompany: boolean;
  static sys: any;
  static treeViewOpen = undefined;
  static currUnitName: any;
  static unitId: any;
  static instance: ParseOrganisationService;
  static isCreated: boolean = false;
  static info;
  static lang;
  static activeId;
  static recChildDef(data, organisation) {
    let childs = organisation
      .filter(el => {return el.parent === data.id; } )
      .map(el => {
        return this.recChildDef(el, organisation);
      });
    if (childs.length > 0) data.child = childs;
    return data;
  }

  static treeStruct(data): any {
    this.treeData = data.filter((el) => {
        return el.type === 'company' || el.parent === '#';
      })
      .map(company => {
        return this.recChildDef(company, data);
      });
  }

  static unitPosition(data) {
    if (data) {
      let units = data.map(el => {
        if (el.child) {
          return el.type === 'unit' ? [el] : this.unitRec(el.child);
        } else {
          return el;
        }
      });
      return _.without(_.flattenDeep(units), undefined);
    }
    return [];
  }

  static unitRec(data) {
    let units = [];
    let unitsParent = data.filter(el => {
      if (el.child) return el.type === 'unit';
    });
    let unitsChild = data.filter(el => {
      if (el.child) return el.type === 'group';
    }).map(el => {
        if (el.child) return this.unitRec(el.child);
    });
    if (unitsParent.length) units.push(unitsParent);
    if (unitsChild.length) units.push(unitsChild);
    return units;
  }



  static htmlOptionTree(data, noCompany?, noLocal?) {
    let indent = TAB;
   let lang = localStorage.getItem('lang');
    let htmOptions = noCompany ? `` : noLocal ?
      `<option  value="${data.id}" > ${data.text} </option>` :
      `<option  value="0"> ${lang === 'de' ? 'Nein' : 'No'}</option>
      <option  value="${data.id}" >${data.text}</option>`;

    if (data.child) {
    for (let i = 0; i < data.child.length; i++) {
      if (data.child[i].type !== 'position') {
        htmOptions +=
          `<option style='font-weight: bold' selected="${noCompany && !i}" 
            value="${data.child[i].id}" >` +
          `${indent + data.child[i].text}</option>`;
        if (data.child[i].child)
          htmOptions += this.htmlBuildRec(data.child[i], indent);
      }
    }
  }
    return htmOptions;
  }

  static htmlBuildRec(data, indent) {
    indent += TAB;
    let stringOption = '';
    for (let i = 0; i < data.child.length; i++) {
      if (data.child[i].type !== 'position') {
        stringOption += `<option value="${data.child[i].id}" >
                          ${indent + data.child[i].text}</option>`;
        if (data.child[i].child && data.child[i].type !== 'position')
          stringOption += this.htmlBuildRec(data.child[i], indent);
      }
    }
    return stringOption;
  }

  static getTreeData() {
    return this.treeData[0];
  }

  static setTreeData(data: any) {
    this.info = data;
    this.data = data.manager.organisation;
  }

  static getInfoData() {
    return this.info;
  }

  static getFiles(data) {
    if (data) {
      return data.filter(el => {
        return el.type !== 'company' && el.type !== 'group';
      }).map(el => {
        return el;
      });
    }
    return [];
  }

  static getDir(data) {
    if (data) {
      return data.filter(el => {
        return !!(el.child && el.child.length > 0) && el.type !== 'unit';
      }).map(el => {
        return new Directory(
          el.id,
          el.text,
          this.getDir(el.child),
          this.getFiles(el.child),
          el.type, el.data);
      });
    }
    return [];
  }
  static calculateWeek() {

    let currentMonthISOWeek = moment().isoWeek();
    let currentMonthISOWeekYear = moment().isoWeekYear();
    let calendarWeek = ('' + currentMonthISOWeekYear).substring(2, 4) +
      currentMonthISOWeek;
    return calendarWeek;
  }

  constructor() {
  }
}

import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AvailabilityService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }

  public put(companyId: number, on, shift, week, weekday) {
    return this.apiHttp.put(`available`, JSON.stringify({
      company_id: companyId, on, shift, week, weekday
    })).map((res) => res.json());
  }

}

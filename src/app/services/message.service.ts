import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IMessage } from '../lib/interfaces/message';


@Injectable()
export class MessageService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
  }

  // no-op?
  public read(id: number): Observable<Response> {
    return this.apiHttp.put(`message/${id}/flag/read`, '').map((res) => res.json());
  }

  public unread(id: number): Observable<Response> {
    return this.apiHttp.put(`message/${id}/unflag/read`, '').map((res) => res.json());
  }

  public remove(id: number): Observable<Response> {
    return this.apiHttp.put(`message/${id}/flag/deleted`, '').map((res) => res.json());
  }

  public undoRemove(id: number): Observable<Response> {
    return this.apiHttp.put(`message/${id}/unflag/deleted`, '').map((res) => res.json());
  }

  public create(messageObj: IMessage): Observable<Response> {
    return this.apiHttp.post('message', JSON.stringify(messageObj)).map((res) => res.json());
  }

}

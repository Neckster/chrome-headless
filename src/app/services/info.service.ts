import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { NodeApiHttp } from './shared/node-http-api.service';
import { BaseService } from './shared/base.service';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ParseOrganisationService } from './shared/parseOrganisation.service';
import { merge, forEach } from 'ramda';
import { DayOffService } from './day_off.service';

interface InfoInterface {
  news: any;
}
@Injectable()
export class InfoService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp, private nodeApiHttp: NodeApiHttp,
  private dayOffService: DayOffService) {
    super();
  }

  public get(week?: string): Observable<Response> {
    let url = week ? `info/${week}` : 'info/' + ParseOrganisationService.calculateWeek();
    return Observable.forkJoin(
        this.apiHttp.get(url).map((res) => res.json()),
        this.nodeApiHttp.get(url).map((res) => res.json())
    ).map(totalInfo => {
      let info = totalInfo[0];

      // Merge first-level fields
      for (let key in totalInfo[1]) {
        if (totalInfo[1].hasOwnProperty(key)) {
          let val = totalInfo[1][key];
          if (info.hasOwnProperty(key)) {
            info[key] = merge(info[key], val);
          } else {
            info[key] = val;
          }
        }
      }

      this.dayOffService.prepareInfo(info);

      return info;
    });
  }

}

import { Injectable } from '@angular/core';
import * as moment    from 'moment';
import { TranslateService }         from 'ng2-translate/ng2-translate';
import { TimestampPipe } from '../lib/pipes/timestamp';

const defaultMargin: number[] = [0, 10, 0, 10];
const defaultWidth = ['*', 55, 55, 55, 200, 20];

@Injectable()
export class PrintService {

  constructor(private ts: TranslateService) {
  }

  buildHeader(headerString = 'Timesheet', style = ''): any {
    return {
      text: headerString,
      style
    };
  }

  buildPersonalHeader(uname = '', fromPeriod = '', toPeriod = ''): any {
    return {
      text: `${uname}\nFrom - ${fromPeriod}, To - ${toPeriod}`,
      bold: true,
      margin: defaultMargin
    };
  }

  buildTable(width: any[], caps = [], body: any, rows = []) {
    return {
      table: {
        widths: width,
        body: caps.concat(body).concat(rows)
      }
    };
  }

  buildTableRow(captions: any[] = []): any[] {
    return [captions.map(c => {
      return Object.assign({}, {
        fontSize: 10,
        text: ' ',
        margin: defaultMargin,
        alignment: 'center',
        bold: true,
        colSpan: 1
      }, c);
    })];
  }

  buildRowSpanned(cellData: any[], span = 1) {
    return cellData.map((c, i) => {
      return [
        {
          text: c.text || '',
          rowSpan: i === 0 ? span : 1
        }
      ];
    });
  }

  buildTableBody(daysData: any[], loc = 'en'): any {

    return daysData.map(day => {
      if (!day.rows || !day.rows.length) {
        return [[
          {
            text: `${this.ts.instant(day.dateFormatDay)}, ${this.ts.instant(day.dateFormatMonth)} ${this.ts.instant(day.dateFormatDate)}`, // tslint:disable
            fontSize: 10
          },
          {
            text: ''
          },
          {
            text: ''
          },
          {
            text: ''
          },
          {
            text: ''
          },
          {
            text: ''
          }
        ]];
      } else {
        return day.rows.map((row: any, i: number, rows: any[]): any => {
          if (row.is_day_off) {
            return [
              {
                rowSpan: i === 0 ? rows.length : 1,
                text: `${this.ts.instant(day.dateFormatDay)}, ${this.ts.instant(day.dateFormatMonth)} ${this.ts.instant(day.dateFormatDate)}`, // tslint:disable
                fontSize: 10
              },
              {
                text: loc === 'en' ? 'DAY OFF' : 'ABWES',
                alignment: 'center',
                fontSize: 9
              },
              {
                text: ' ',
                alignment: 'center',
                fontSize: 10
              },
              {
                text: ' ',
                alignment: 'center',
                fontSize: 10
              },
              {
                text: `${this.ts.instant(row.day_off_data.reason)} (${row.day_off_data.dateFrom} - ${row.day_off_data.dateTo} | ${row.day_off_data.total_days} ${this.ts.instant('days')})`, // tslint:disable
                fontSize: 10
              },
              {
                text: '',
                rowSpan: 1,
                alignment: 'center',
                fontSize: 10
              }
            ];
          } else {
            let append = (row.status === 'unchecked') ? '*' : '';
            let jobBreak = row.break === 0 ? '00:00' : row.break;

            return [
              {
                rowSpan: i === 0 ? rows.length : 1,
                text: `${this.ts.instant(day.dateFormatDay)}, ${this.ts.instant(day.dateFormatMonth)} ${this.ts.instant(day.dateFormatDate)}`, // tslint:disable
                margin: defaultMargin,
                fontSize: 10
              },
              {
                text: `${new TimestampPipe().transform(row.from, this.ts.currentLang, 'HH:mm')} ${append}`, //`${row.from} ${append}`,
                alignment: 'center',
                fontSize: 10,
                bold: true
              },
              {
                text: `${new TimestampPipe().transform(row.to, this.ts.currentLang, 'HH:mm')} ${append}`,
                alignment: 'center',
                fontSize: 10,
                bold: true
              },
              {
                text: `${jobBreak}`,
                alignment: 'center',
                fontSize: 10,
                bold: true
              },
              {
                text: `${row.unit_name} - ${row.position_name}`,
                fontSize: 10
              },
              {
                rowSpan: 1,
                text: `${row.worked_time.replace(':', '.')}` || '',
                alignment: 'center',
                fontSize: 10,
                bold: true
              }
            ];
          }
        });
      }
    });
  }
}

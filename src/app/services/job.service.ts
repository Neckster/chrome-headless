/**
 * Created by user on 21.06.16.
 */
import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import * as _ from 'lodash';
import { Http, RequestOptions, RequestMethod, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { NodeApiHttp } from './shared/node-http-api.service';
interface JobResponse {
  max_work_time: number;
  min_job_count: number;
  position_id: number;
  shift_idx: number;
  week: string;
  weekday: number;
}
@Injectable()
export class TargetJobService extends BaseService {

  constructor(private http: Http, private apiHttp: LuaApiHttp, private nodeApiHttp: NodeApiHttp) {
    super();
  }

  public getPastAssignments(year, month) {
    return this.nodeApiHttp.get('assignments/past/' + year + '/' + month).map(v => v.json());
  }

  public put(positionId, shiftIdx, weekday, week, webuserId, maxWorkTime?, minJobCount?) {
    let job = {
      max_work_time: maxWorkTime || undefined,
      min_job_count: minJobCount || undefined,
      position_id: positionId,
      shift_idx: shiftIdx,
      week, weekday,
      webuser_id: webuserId
    };
    return this.apiHttp.put(`job`, JSON.stringify(_.omitBy(job, _.isUndefined)))
      .map((res) => {
        res.json();
      });
  }

  public putAssign(weekday, shiftIdx, positionId, webuserId, week,
                   to, from, tto, tfrom, info?, chef?) {
    let job = {
      position_id: positionId,
      shift_idx: shiftIdx,
      week, weekday,
      webuser_id: webuserId,
      to, from, tto, tfrom, info, chef
    };
    return this.apiHttp.put(`job`, JSON.stringify(_.omitBy(job, _.isUndefined)))
      .map((res) => {
        res.json();
      });
  }

  public jobStatus(param, status, time, week, value, idle?) {
    let job = {
      position_id: param.position_id,
      shift_idx: param.shift_idx,
      webuser_id: param.webuser_id,
      weekday: param.weekday,
      week: +week, status, time,
      value, idle
    };
    return this.apiHttp.post(`job/status`, JSON.stringify(_.omitBy(job, _.isUndefined)))
      .map((res) => {
        res.json();
      });
  }
  public jobRefuse(value, param, week) {
    let job = {
      position_id: param.position_id,
      shift_idx: param.shift_idx,
      webuser_id: param.webuser_id,
      weekday: param.weekday,
      week: +week,
      status: 'declined',
      value: value
    };
    return this.apiHttp.post(`job/status`, JSON.stringify(_.omitBy(job, _.isUndefined)))
      .map((res) => {
        res.json();
      });
  }
  public jobAssess(userId, rating) {
    return this.apiHttp.put(`user/rate/${userId}/${rating}`, '').map((res) => res.json());
  }
  public avaible(value, param, week) {
    let job = {
      position_id: param.position_id,
      shift_idx: param.shift_idx,
      webuser_id: param.webuser_id,
      weekday: param.weekday,
      week: param.week,
      status: 'accepted',
      value: 1
    };
    return this.apiHttp.post(`job/status`, JSON.stringify(_.omitBy(job, _.isUndefined)))
      .map((res) => {
        res.json();
      });
  }

  public deleteAssign(weekday, shiftIdx, positionId, webuserId, week, value) {
    let job = {
      position_id: positionId,
      shift_idx: shiftIdx,
      webuser_id: webuserId,
      weekday, week, value,
      status: 'deleted'
    };
    return this.apiHttp.post(`job/status`, JSON.stringify(job))
      .map((res) => {
        res.json();
      });
  }

  public adjust(param) {
    return this.apiHttp.put('job/adjust', JSON.stringify(param))
      .map((res) => {
        res.json();
      });
  }
}

// #1567

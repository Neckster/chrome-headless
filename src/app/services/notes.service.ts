import { Injectable } from '@angular/core';
import { NodeApiHttp } from './shared/node-http-api.service';
import { Http } from '@angular/http';
import { BaseService } from './shared/base.service';

interface Note {
  week: string;
  day: '0'|'1'|'2'|'3'|'4'|'5'|'6';
  unit_id: number;
  body: string;
}

@Injectable()
export class NotesService extends BaseService {
  constructor(private http: Http, private apiHttp: NodeApiHttp) {
    super();
  }

  getNotes(week, unit) {
    return this.apiHttp.get(`note/${unit}/${week}`);
  }

  /**
   * *request{
   *    week: "YYWW",
   *    day: [0..6]
   *    unit_id: number,
   *    body: string
   * }
   * @param note Note
   * @returns {Observable<Response>}
   */
  saveNote(note: Note) {
    return this.apiHttp.post('note', JSON.stringify(note));
  }
}

import { Injectable } from '@angular/core';
import { LuaApiHttp } from './shared/lua-http-api.service';
import { BaseService } from './shared/base.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

interface InfoResponse {
  company_id: any;
  pname: any;
  unit_id: any;
  position_id: any;
}

@Injectable()
export class PositionService extends BaseService {
  public url;
  constructor(private http: Http, private apiHttp: LuaApiHttp) {
    super();
    this.url = 'position';
  }

  /*fetch the managed organisation structure of the given company.*/
  public putPoition(name, companyId, unitId, staff?): Observable<InfoResponse> {
    let params = {
      company_id: `${companyId}`,
      pname: name,
      unit_id: staff ? unitId : `${unitId}`.slice(1),
      staff
    };
    return this.apiHttp
      .put(this.url, JSON.stringify(_.omitBy(params, _.isUndefined))).map((res) => res.json());
  }
  public rename(params) {
    return this.apiHttp.put(this.url, JSON.stringify(params)).map(res => res.json());
  }

  public delPosition(positioId): Observable<InfoResponse> {
    return this.apiHttp.delete(this.url + `/${positioId}`).map(res => res.json());
  }

  public putTreePosition( positionId: string, unitId: string ) {
    let params = {
      position_id: positionId,
      unit_id: unitId
    };
    return this.apiHttp
      .put( 'position', JSON.stringify(_.omitBy(params, _.isUndefined))).map((res) => res.json());
  }

  public putTreeUnit( parentId: string, currentId: string ) {
    let params = {
      parent: parentId,
      unit_id: currentId
    };
    return this.apiHttp
      .put( 'unit', JSON.stringify(_.omitBy(params, _.isUndefined))).map((res) => res.json());
  }

  // public delJobber(deletejobber): Observable<InfoResponse> {
  //   console.log(deletejobber)
  //   return this.apiHttp.put(this.url, JSON.stringify(deletejobber)).map(res => res.json());
  // }
  /*fetch the managed organisation structure of the company that was selected before*/
  // public getOrganisation(): Observable<Response> {
  //   return this.apiHttp.getUnit(`organisation`).map((res) => res.json());
  // }
}

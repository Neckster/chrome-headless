import { InfoService } from '../services/info.service';
export const GET_INFO = 'GET_INFO';
export const SET_CURRENT_UNIT = 'SET_CURRENT_UNIT';
export const SET_WEEK = 'SET_WEEK';
export const GET_CURRENT_UNIT = 'GET_CURRENT_UNIT';
export const CHANGE_COMPANY_STAFF = 'CHANGE_COMPANY_STAFF';
export const SET_CURRENT_ORGANISATION = 'SET_CURRENT_ORGANISATION';
export const ASSIGN_INFO = 'ASSIGN_INFO';
export const MODAL_DATA = 'MODAL_DATA';
export const ADD_MODAL = 'ADD_MODAL';
export const CREATE_MODAL = 'CREATE_MODAL';
export const SET_MODAL_TIMING_DATA = 'SET_MODAL_TIMING_DATA';
export const CLOSE_MODAL_TIMING = 'CLOSE_MODAL_TIMING';
export const PER_SHIFT = 'PER_SHIFT';
export const LANGUAGE = 'LANGUAGE';
export const JOBBER_CREATED = 'JOBBER_CREATED';
export const TREE_VIEW_MESSAGE = 'TREE_VIEW_MESSAGE';
export const OPEN_ASSIGN_MODAL = 'OPEN_ASSIGN_MODAL';
export const STREAM_UPDATE = 'STREAM_UPDATE';
export const AUTH_STATE_CHANGE = 'CHANGE_AUTH_STATE';

import { Injectable } from '@angular/core';
import { UnitService } from '../services/unit.service';
import { NgRedux } from 'ng2-redux/lib/index';
import { RootState } from './reducer';
import { ParseOrganisationService } from '../services/shared/parseOrganisation.service';
import { Assignment } from '../models/Assignment';
import { NotesService } from '../services/notes.service';
import { ScheduledService } from '../services/scheduled.service';
import { OnBoardingService } from '../services/onboardin.service';
import { StaffService } from '../services/staff.service';

@Injectable()
export class AppStoreAction {
  constructor(public infoService: InfoService, public ngRedux: NgRedux<RootState>,
              public unitService: UnitService, private notes: NotesService,
              public scheduled: ScheduledService, public onBoardingService: OnBoardingService,
              public staffService: StaffService
  ) {

  }
  setUname(name) {
    this.ngRedux.dispatch({
      type: 'setUname',
      name
    });
  }
  setCname(name) {
    this.ngRedux.dispatch({
      type: 'setCname',
      name
    });
  }
  cleatParseService() {
    ParseOrganisationService.currCompany = undefined;
    ParseOrganisationService.isEditCompany = undefined;
    ParseOrganisationService.currUnitName = undefined;
    ParseOrganisationService.info = undefined;
  }
  setCurrentOrganisation(org) {
    this.ngRedux.dispatch({
      type: SET_CURRENT_ORGANISATION,
      org: org
    });
  }
  setCloseUnitModal(v) {
    this.ngRedux.dispatch({
      type: 'unitModal',
      v: v
    });
  }
  setCurrentUnit(unit) {
    this.ngRedux.dispatch({
      type: SET_CURRENT_UNIT,
      unit: unit
    });
  }

  getInfo(json) {
    this.ngRedux.dispatch({
      type: GET_INFO,
      info: json
    });
  };

  getNotes(json) {
    this.ngRedux.dispatch({
      type: 'GET_NOTES',
      value: json
    });
  }

  handleNotesError(error) {
    this.ngRedux.dispatch({
      type: 'NOTES_ERROR',
      error: error
    });
  }

  getScheduled(json) {
    this.ngRedux.dispatch({
      type: 'GET_SCHEDULED',
      value: json
    });
  }

  handleScheduledError(error) {
    this.ngRedux.dispatch({
      type: 'SCHEDULED_ERROR',
      value: error
    });
  }

  getCurrentUnit(json) {
    this.ngRedux.dispatch({
      type: GET_CURRENT_UNIT,
      unit: json
    });
  }
  setCurrentCompanyStaff(staff) {
    this.ngRedux.dispatch({
      type: CHANGE_COMPANY_STAFF,
      staff: staff
    });
  }
  handleUnitError(err) {
    this.ngRedux.dispatch({
      type: 'unit_error',
      err
    });
  }
  clearStore() {
    this.cleatParseService();
    this.ngRedux.dispatch({
      type: 'clear'
    });
  }
  fetchCurrentUnit(id, week?): any {
      this.unitService.getUnit(+id, week)
        .subscribe(json => this.getCurrentUnit(json),
          (err) => { this.handleUnitError({err: true}); });
  }

  fetchGetInfo(week?): any {
    this.fetchCurrentCompanyStaff();
      this.infoService.get(week)
        .subscribe(json => this.getInfo(json));
  }

  fetchGetNotes(week: number, unit: number): any {
    this.notes.getNotes(week, unit)
      .map(v => v.json())
      .subscribe((res) => this.getNotes(res), (err) => this.handleNotesError(err));
  }

  fetchJobberScheduledData(week: number, unit: number): any {
    return this.scheduled.getUnitUsersWeekJobs(week, unit)
      .map(v => {
        return v.json();
      })
      .subscribe(this.getScheduled.bind(this), this.handleScheduledError.bind(this));
  }

  /**
   * Load staff of current company on unit change
   */
  fetchCurrentCompanyStaff() {
    return this.staffService.getShortInfo().subscribe((v) => {
      this.setCurrentCompanyStaff(v.staff);
    });
  }

  /**
   *
   * @returns {Observable<R>}
   */
  fetchAuthStatus() {
    let checkUserStatus = this.onBoardingService.checkUserStatus();
    checkUserStatus.subscribe((state) => {
      this.checkAuthState(state);
      return state;
    }, (err) => {
      this.checkAuthState({
        status: false
      });
    });
    return checkUserStatus;
  }

  saveScheduleWeek(week) {
    this.ngRedux.dispatch({
      type: SET_WEEK,
      week
    });
  }
  selectedAssign(assign, shiftAssign?) {
    this.ngRedux.dispatch({
      type: ASSIGN_INFO,
      assignInfo: {assign, shiftAssign}
    });
  }

  showModal(data) {
    this.ngRedux.dispatch({
      type: MODAL_DATA,
      data
    });
  }

  showAddModal(data) {
    this.ngRedux.dispatch({
      type: ADD_MODAL,
      data
    });
  }
  showCreateJobberBool(data) {
    this.ngRedux.dispatch({
      type: CREATE_MODAL,
      data
    });
  }
  setModalTimingData(data) {
    this.ngRedux.dispatch({
      type: SET_MODAL_TIMING_DATA,
      data
    });
  }
  closeModalTiming(updated) {
    this.ngRedux.dispatch({
      type: CLOSE_MODAL_TIMING,
      data: updated
    });
  }
  totalPerShift(data) {
    this.ngRedux.dispatch({
      type: PER_SHIFT,
      data
    });
  }
  localisation(data) {
    this.ngRedux.dispatch({
      type: LANGUAGE,
      data
    });
  }
  jobberAlert(data) {
    this.ngRedux.dispatch({
      type: JOBBER_CREATED,
      data
    });
  }
  treeViewError(data) {
    this.ngRedux.dispatch({
      type: TREE_VIEW_MESSAGE,
      data
    });
  }
  treeViewErrorU(data) {
    this.ngRedux.dispatch({
      type: 'setUErorr',
      value: data
    });
  }
  openDelModal(open, value) {
    let data = {show: open, data: value};
    this.ngRedux.dispatch({
      type: 'setDelData',
      value: data
    });
  }

  broadcastEvent(data) {
    this.ngRedux.dispatch({
      type: STREAM_UPDATE,
      value: data
    });
  }

  // satCalculateUnit(calcUnit) {
  //   this.ngRedux.dispatch({
  //     type: 'setCalcUnit',
  //     value: calcUnit
  //   });
  // }
  // calculateDay(object, weekDay) {
  //   this.ngRedux.dispatch({
  //     type: 'setDay',
  //     value: object,
  //     day: weekDay
  //   });
  // }
  deleteAssign(assignObj) {
    this.ngRedux.dispatch({
      type: 'deleteAssigning',
      value: assignObj
    });
  }
  updatePosition(position?, shift?, shiftIdx?, act?, weekday?) {
    this.ngRedux.dispatch({
      type: 'updatePosition',
      value: {position, shift, shiftIdx, act, weekday}
    });
  }
  showAlertAvailability (correct: boolean) {
    this.ngRedux.dispatch({
      type: 'SHOW_ALERT_AVAILABILITY',
      value: correct
    });
  }
  showAlertScheduledJobber (assignment: Assignment) {
    this.ngRedux.dispatch({
      type: 'SHOW_SCHEDULE_ALERT',
      value: assignment
    });
  }
  hideAlertScheduledJobber () {
    this.ngRedux.dispatch({
      type: 'SHOW_SCHEDULE_ALERT',
      value: false
    });
  }
  openModalAssign(getObj) {
    this.ngRedux.dispatch({
      type: 'OPEN_ASSIGN_MODAL',
      value: getObj
    });
  }

  openModal(id: string) {
    this.ngRedux.dispatch({
      type: 'MODAL_OPEN',
      value: id
    });
  }

  closeModal(id: string) {
    this.ngRedux.dispatch({
      type: 'MODAL_CLOSE',
      value: id
    });
  }

  openAlertCopyModal(open: boolean, data: any) {
    this.ngRedux.dispatch({
      type: 'OPEN_ALERT_COPY_MODAL',
      value: open,
      copyModalData: data
    });
  }

  openPublishModal(open: boolean, data?: any) {
    this.ngRedux.dispatch({
      type: 'DAY_VIEW_MAIN_PUBLISH',
      value: open,
      publishModalData: data
    });
  }
  changeDayViewToggle(open) {
    this.ngRedux.dispatch({
      type: 'CHANGE_DAY_VIEW_TOGGLE',
      value: open
    });
  }
  changeShiftViewToggle(open) {
    this.ngRedux.dispatch({
      type: 'CHANGE_SHIFT_VIEW_TOGGLE',
      value: open
    });
  }

  dayViewAction(open: boolean) {
    this.ngRedux.dispatch({
      type: 'DAY_VIEW_ACTION',
      value: open
    });
  }

  switchViewAction(open: boolean) {
    this.ngRedux.dispatch({
      type: 'SWITCH_VIEW_ACTION',
      value: open
    });
  }
  openCopyAnction( open: boolean) {
    this.ngRedux.dispatch({
      type: 'OPEN_COPY_ACTION',
      value: open
    });
  }
  schedulePublishCounter( num: number) {
    this.ngRedux.dispatch({
      type: 'SCHEDULE_PUBLISH_COUNTER',
      value: num
    });
  }
  schedulePublic( open: boolean) {
    this.ngRedux.dispatch({
      type: 'SCHEDULE_PUBLIC',
      value: open
    });
  }
  dayViewUnpubStaff(staff: any) {
    this.ngRedux.dispatch({
      type: 'DAY_VIEW_UNPUB_STAFF',
      value: staff
    });
  }
  addJobberOpen(open: any) {
    this.ngRedux.dispatch({
      type: 'ADD_JOBBER_OPEN',
      value: open
    });
  }
  staffFilterStr(str: string) {
    this.ngRedux.dispatch({
      type: 'STAFF_FILTER_STR',
      value: str
    });
  }
  openWeekPrintView(open: boolean) {
    this.ngRedux.dispatch({
      type: 'OPEN_WEEK_PRINT_VIEW',
      value: open
    });
  }
  openDayPrintView(open: boolean) {
    this.ngRedux.dispatch({
      type: 'OPEN_DAY_PRINT_VIEW',
      value: open
    });
  }
  selectedCompanyOnReport(id: any) {
    this.ngRedux.dispatch({
      type: 'SELECTED_COMPANY_ON_REPORT',
      value: id
    });
  }
 selectedMonthOnReport(month) {
    this.ngRedux.dispatch({
      type: 'SELECTED_MONTH_ON_REPORT',
      value: month
    });
  }

  userIsManager(isManager: boolean) {
    this.ngRedux.dispatch({
      type: 'USER_IS_MANAGER',
      value: isManager
    });
  }

  profileSet(stepsFinished: boolean) {
    this.ngRedux.dispatch({
      type: 'PROFILE_SET',
      value: stepsFinished
    });
  }

  checkAuthState(status) {
    this.ngRedux.dispatch({
      type: AUTH_STATE_CHANGE,
      state: status
    });
  }
}

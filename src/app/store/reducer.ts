import * as Actions from './action';
import { combineReducers } from 'redux';

export const initialState = {
  info: {},
  currentUnit: {},
  currentOrganisation: {},
  saveScheduleWeek: 0,
  selectedAssign: {},
  unitName: undefined,
  companyName: undefined,
  closeUnitModal: false,
  modalTiming: {
    open: false,
    param: undefined,
    updated: false
  },
  showModal: false,
  showAddModal: false,
  showCreateJobberBool: false,
  totalPerShift: [],
  localisation: 'en',
  jobberAlert: false,
  treeViewError: false,
  treeViewErrorU: false,
  delData: {show: false, data: {}},
  satCalculateUnit: {},
  calculateDay: {},
  deleteAssign: {},
  updatePosition: {},
  availabilityAlert: false,
  scheduledJobberAlert: false,
  openModalAssign: false,
  broadcastEvent: {},
  openedModals: [],
  openAlertCopyModal: false,
  copyModalData: undefined,
  weekNotes: [],
  notesError: {},
  scheduled: {},
  scheduledError: {},
  openPublishModal: false,
  publishModalData: undefined,
  dayView: false,
  switchView: true,
  openCopy: false,
  schedulePubCounter: 0,
  schedulePublic: false,
  dayUnpubStaff: undefined,
  addJobberOpen: false,
  staffFilter: '',
  weekPrintView: false,
  dayPrintView: false,
  reportCompany: '',
  reportMonth: '',
  userIsManager: false,
  profileSet: false,
  authStatus: {},
  authStateChecked: false,
  currentCompanyStaff: []
};

export interface RootState {
  info: any;
  currentUnit: any;
  closeUnitModal: boolean;
  currentOrganisation: any;
  saveScheduleWeek: any;
  selectedAssign: any;
  unitName: any;
  companyName: any;
  modalTiming: {
    open: boolean,
    param: Object,
    updated: boolean
  };
  showModal: any;
  showAddModal: any;
  showCreateJobberBool: boolean;
  totalPerShift: any;
  localisation: string;
  jobberAlert: any;
  treeViewError: boolean;
  treeViewErrorU: boolean;
  delData: {
    show: boolean;
    data: any;
  };
  satCalculateUnit: any;
  calculateDay: any;
  deleteAssign: any;
  updatePosition: any;
  openModalAssign: any;
  broadcastEvent: any;
  openedModals: string[];
  openAlertCopyModal: boolean;
  copyModalData: any;
  weekNotes: any[];
  notesError: any;
  scheduled: any;
  scheduledError: any;
  openPublishModal: boolean;
  publishModalData: any;
  dayView: boolean;
  switchView: boolean;
  openCopy: boolean;
  schedulePubCounter: number;
  schedulePublic: boolean;
  dayUnpubStaff: any;
  addJobberOpen: boolean;
  staffFilter: string;
  weekPrintView: boolean;
  dayPrintView: boolean;
  reportCompany: string;
  reportMonth: any;
  userIsManager: boolean;
  profileSet: boolean;
  authStatus: any;
  authStateChecked: boolean;
  currentCompanyStaff: any;
}

export function rootReducer(state: RootState = initialState, action) {
  switch (action.type) {
    case 'unit_error':
      return Object.assign({}, state, { currentUnit: action.err });

    case 'unitModal':
      return Object.assign({}, state, { closeUnitModal: action.v });

    case 'clear':
      return Object.assign({}, state, { info: {}, currentUnit: {}, currentOrganisation: {} });

    case 'setUname':
      return Object.assign({}, state, { unitName: action.name });

    case 'setCname':
      return Object.assign({}, state, { companyName: action.name });

    case 'setUErorr':
      return Object.assign({}, state, { treeViewErrorU: action.value });

    case 'setDelData':
      return Object.assign({}, state, { delData: action.value });

    case Actions.GET_INFO:
      return Object.assign({}, state, { info: action.info });

    case 'GET_NOTES':
      return Object.assign({}, state, { weekNotes: action.value });

    case 'NOTES_ERROR':
      return Object.assign({}, state, { notesError: action.error });

    case 'GET_SCHEDULED':
      return Object.assign({}, state, { scheduled: action.value });

    case 'SCHEDULED_ERROR':
      return Object.assign({}, state, { scheduledError: action.value });

    case Actions.SET_CURRENT_UNIT:
      return Object.assign({}, state, { currentUnit: action.unit });

    case Actions.GET_CURRENT_UNIT:
      return Object.assign({}, state, { currentUnit: action.unit });

    case Actions.CHANGE_COMPANY_STAFF:
      return Object.assign({}, state, { currentCompanyStaff: action.staff });

    case Actions.SET_CURRENT_ORGANISATION:
      return Object.assign({}, state, { currentOrganisation: action.org });

    case Actions.SET_WEEK:
      return Object.assign({}, state, { saveScheduleWeek: action.week });

    case Actions.ASSIGN_INFO:
      return Object.assign({}, state, { selectedAssign: action.assignInfo });

    case Actions.MODAL_DATA:
      return Object.assign({}, state, { showModal: action.data });

    case Actions.ADD_MODAL:
      return Object.assign({}, state, { showAddModal: action.data });

    case Actions.CREATE_MODAL:
      return Object.assign({}, state, { showCreateJobberBool: action.data });

    case Actions.SET_MODAL_TIMING_DATA:
      return Object.assign({}, state, {
        modalTiming: {
          open: true,
          param: action.data,
          updated: false
        }
      });

    case Actions.CLOSE_MODAL_TIMING:
      return Object.assign({}, state, {
        modalTiming: {
          open: false,
          param: void 0,
          updated: action.data
        }
      });

    case Actions.PER_SHIFT:
      return Object.assign({}, state, { totalPerShift: action.data });

    case Actions.LANGUAGE:
      return Object.assign({}, state, { localisation: action.data });

    case Actions.JOBBER_CREATED:
      return Object.assign({}, state, { jobberAlert: action.data });

    case Actions.TREE_VIEW_MESSAGE:
      return Object.assign({}, state, { treeViewError: action.data });

    case 'setCalcUnit':
      return Object.assign({}, state, { satCalculateUnit: action.value });

    case 'deleteAssigning':
      return Object.assign({}, state, { deleteAssign: action.value });

    case 'updatePosition':
      return Object.assign({}, state, { updatePosition: action.value });

    case 'SHOW_ALERT_AVAILABILITY':
      return Object.assign({}, state, { availabilityAlert: action.value });

    case 'SHOW_SCHEDULE_ALERT':
      return Object.assign({}, state, { prevScheduledAssignAlert: action.value });

    case 'OPEN_ASSIGN_MODAL':
      return Object.assign({}, state, { openModalAssign: action.value });

    case Actions.STREAM_UPDATE:
      return Object.assign({}, state, { broadcastEvent: action.value });

    case 'MODAL_OPEN':
      return Object.assign({}, state, { openedModals: [action.value, ...state.openedModals] });

    case 'MODAL_CLOSE':
      return Object.assign({}, state,
        { openedModals: action.value ?
          state.openedModals.filter(m => m !== action.value) : [] });

    case Actions.AUTH_STATE_CHANGE:
      return Object.assign({}, state, {
        authState: action.state,
        authStateChecked: true
      });

    case 'OPEN_ALERT_COPY_MODAL':
      return Object.assign({}, state, {
        openAlertCopyModal: action.value,
        copyModalData: action.copyModalData
      });

    case 'DAY_VIEW_MAIN_PUBLISH':
      return Object.assign({}, state, {
        openPublishModal: action.value,
        publishModalData: action.publishModalData,
        dayUnpubStaff: action.publishModalData
      });
    case 'DAY_VIEW_ACTION':
      return Object.assign({}, state, {
        dayView: action.value
      });

    case 'SWITCH_VIEW_ACTION':
      return Object.assign({}, state, {
        switchView: action.value
      });
    case 'OPEN_COPY_ACTION':
      return Object.assign({}, state, {
        openCopy: action.value
      });
    case 'SCHEDULE_PUBLISH_COUNTER':
      return Object.assign({}, state, {
        schedulePubCounter: action.value
      });
    case 'SCHEDULE_PUBLIC':
      return Object.assign({}, state, {
        schedulePublic: action.value
      });
    case 'DAY_VIEW_UNPUB_STAFF':
      return Object.assign({}, state, {
        dayUnpubStaff: action.value
      });
    case 'ADD_JOBBER_OPEN':
      return Object.assign({}, state, {
        addJobberOpen: action.value
      });
    case 'STAFF_FILTER_STR':
      return Object.assign({}, state, {
        staffFilter: action.value
      });
    case 'OPEN_WEEK_PRINT_VIEW':
      return Object.assign({}, state, {
        weekPrintView: action.value,
        dayPrintView: false
      });
    case 'OPEN_DAY_PRINT_VIEW':
      return Object.assign({}, state, {
        dayPrintView: action.value,
        weekPrintView: false
      });
    case 'SELECTED_COMPANY_ON_REPORT':
      return Object.assign({}, state, {
        reportCompany: action.value
      });
    case 'SELECTED_COMPANY_ON_REPORT':
      return Object.assign({}, state, {
        reportCompany: action.value
      });
    case 'SELECTED_MONTH_ON_REPORT':
      return Object.assign({}, state, {
        reportMonth: action.value
      });

    case 'USER_IS_MANAGER':
      return Object.assign({}, state, {
        userIsManager: action.value
      });

    case 'PROFILE_SET':
      return Object.assign({}, state, {
        profileSet: action.value
      });

    default:
      return state;
  }
}

// TODO: combine reducers
// export const rootReducer = combineReducers({
//   mainReducer
// });
//
// console.log(rootReducer);



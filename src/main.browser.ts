// jit compiler init
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { JubbrAppModule } from 'jubbr.module';

document.addEventListener('DOMContentLoaded',
  () => platformBrowserDynamic().bootstrapModule(JubbrAppModule));

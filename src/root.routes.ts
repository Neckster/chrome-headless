import { Routes }               from '@angular/router';
import { UnauthResolver }       from 'app/lib/routerActivators/route.resolver';
import { LoggedResolver }       from 'app/lib/routerActivators/logged.resolver';

import {
  JobberNews,
  AuthComponent,
  Manager,
  ManagerControlling,
  ManagerReports,
  ManagerSchedule,
  ManagerStaff,
  SetupCompany,
  SetupOrganisation,
  JobberProfile,
  FirstStepComponent,
  JobberAvailability,
  AssignmentsComponent          } from './platform/component-directives';

 import { NewsResolver }          from 'app/lib/resolvers/news.resolver';
 import { OnboardingCheck }       from 'app/lib/resolvers/onboardingCheck';

export const rootRoutes: Routes = [
  {
    path: 'auth',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/auth/login'
      },
      {
        path: ':authStrategy',
        component: AuthComponent,
        canActivate: [LoggedResolver]
      },
      {
        path: '**',
        redirectTo: '/auth/login'
      }
    ]
  },

  {
    path: '',
    component: Manager,
    canActivate: [UnauthResolver],
    children: [
      {
        path: 'onboarding',
        component: FirstStepComponent,
        canDeactivate: [OnboardingCheck]
      },
      {
        path: 'news',
        component: JobberNews
        // canActivate: [OnboardingCheck]
      },
      {
        path: 'controlling',
        // canActivate: [OnboardingCheck],
        component: ManagerControlling
      },
      {
        path: 'schedule',
        // canActivate: [OnboardingCheck],
        component: ManagerSchedule
      },
      {
        path: 'staff',
        // canActivate: [OnboardingCheck],
        component: ManagerStaff
      },
      {
        path: 'reports',
        // canActivate: [OnboardingCheck],
        component: ManagerReports
      },
      {
        path: 'setup-company',
        // canActivate: [OnboardingCheck],
        component: SetupCompany
      },
      {
        path: 'setup-organisation',
        // canActivate: [OnboardingCheck],
        component: SetupOrganisation
      },
      {
        path: 'profile',
        // canActivate: [OnboardingCheck],
        component: JobberProfile
      },
      {
        path: 'availability',
        // canActivate: [OnboardingCheck],
        component: JobberAvailability
      },
      {
        path: 'assignment/:tab',
        // canActivate: [OnboardingCheck],
        component: AssignmentsComponent
      },
      {
        path: 'assignment',
        // canActivate: [OnboardingCheck],
        component: AssignmentsComponent
      },
      {
        path: '**', redirectTo: '/news'
      }
    ]
  }
];

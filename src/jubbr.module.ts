import {
  NgModule,
  NgModuleMetadataType }                    from '@angular/core';
import { BrowserModule }                    from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }                     from '@angular/router';
import { HttpModule }                       from '@angular/http';

import { PLATFORM_PROVIDERS }               from './platform/browser';
import { APPLICATION_COMPONENTS }           from './platform/component-directives';
import { App }                              from './app/app.component';
import { rootRoutes }                       from './root.routes';

import { Ng2PaginationModule }              from 'ng2-pagination';


@NgModule({
  declarations: [
    ...APPLICATION_COMPONENTS
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rootRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    Ng2PaginationModule
  ],
  providers: [
    ...PLATFORM_PROVIDERS
  ],
  bootstrap: [App]
} as NgModuleMetadataType)

export class JubbrAppModule {}

import { App }                    from '../app/app.component';
import { Manager }                from '../app/manager/manager.component';
import { AuthComponent }          from '../app/auth/auth.component';
import { AssignmentsComponent }   from '../app/manager/assignments/assignments.component';
import { JobberAvailability }     from '../app/manager/availability/availability.component';
import { JobberNews }             from '../app/manager/news/news.component';
import { JobberProfile }          from '../app/manager/profile/profile.component';
import { ManagerControlling }     from '../app/manager/controlling/controlling.component';
import { ManagerReports }         from '../app/manager/reports/reports-component';
import { ManagerSchedule }        from '../app/manager/schedule/schedule.component';
import { ManagerStaff }           from '../app/manager/staff/staff.component';
import { SetupCompany }           from '../app/manager/setup-company/company.component';
import { SetupOrganisation }      from '../app/manager/setup-organisation/organisation.component';
import { AddComplexTree }
                  from '../app/manager/organisation-tree/addComplexTree/addComplexTree.component';
import { TreeViewComponent }      from '../app/manager/tree-view/tree-view.component';
import { UnitComponent }          from '../app/manager/unit-select/unit.component';
import {
  AddLanguage }            from '../app/manager/profile/add-language/add-language.component';
import { LoginComponent }         from '../app/auth/log-in/log-in.components';
import { NewPasswordComponent }   from '../app/auth/new-password/new-password.component';
import { SignupComponent }        from '../app/auth/sign-up/sign-up.component';
import {
  ConfirmRegComponent }    from '../app/auth/confirm-registration/confirm-registration.component';
import { PasswordInfoComponent }  from '../app/auth/password-info/password-info.component';
import { FirstStepComponent }     from '../app/manager/firstStep/firstStepComponent';
import { UnavailableModalComponent }
        from '../app/manager/schedule/staff-grid/unavailable-modal/unavailable-mdal.component';
import { DatePikerComponent }     from '../app/manager/date-picker/date-picker';


export const APPLICATION_COMPONENTS = [
  App, Manager, AuthComponent, AssignmentsComponent,
  JobberAvailability, JobberNews, JobberProfile, ManagerControlling,
  ManagerSchedule, ManagerStaff, SetupCompany, SetupOrganisation,
  TreeViewComponent, UnitComponent, AddLanguage, ManagerReports,
  LoginComponent, SignupComponent, NewPasswordComponent,
  ConfirmRegComponent, PasswordInfoComponent, AddComplexTree,
  FirstStepComponent, UnavailableModalComponent, DatePikerComponent
];

export { JobberNews, Manager, AuthComponent, ManagerControlling, ManagerSchedule, ManagerStaff,
  SetupCompany, SetupOrganisation, JobberProfile, JobberAvailability, AssignmentsComponent,
  ManagerReports, AddComplexTree, FirstStepComponent, UnavailableModalComponent,
  DatePikerComponent};

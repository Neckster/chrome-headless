import {
  FORM_PROVIDERS,
  LocationStrategy,
  HashLocationStrategy,
  FormBuilder } from '@angular/common';
import { provide,  PLATFORM_PIPES } from '@angular/core';
import { NgRedux } from 'ng2-redux';
// Angular 2 Http
import { HTTP_PROVIDERS, BrowserXhr } from '@angular/http';
import { Http } from '@angular/http';

import { ROUTER_PROVIDERS } from '@angular/router-deprecated';

import {
  TranslateService, TranslateLoader,
  TranslateStaticLoader, TranslatePipe
} from 'ng2-translate/ng2-translate';

import {
  LUA_API_HTTP_PROVIDERS,
  LuaApiHttp
} from '../app/services/shared/lua-http-api.service.ts';
import {
  NODE_API_HTTP_PROVIDERS,
  NodeApiHttp
} from '../app/services/shared/node-http-api.service.ts';
import { AuthService }          from '../app/services/shared/auth.service';
import { AvailabilityService }  from '../app/services/availability.service';
import { MessageService }       from '../app/services/message.service';
import { StaffService }         from '../app/services/staff.service';
import { CustomBrowserXhr }     from '../app/services/shared/custom-xhr';
import { SkillsService }        from '../app/services/skills.service';
import { WebuserService }       from '../app/services/webuser.service';
import { UserService }          from '../app/services/user.service';
import { InfoService }          from '../app/services/info.service';
import { OrganizationService }  from '../app/services/organisation.service';
import { UnitService }          from '../app/services/unit.service';
import { EmitService }          from '../app/services/emit.service';
import { ParseOrganisationService } from '../app/services/shared/parseOrganisation.service';
import { AppStoreAction }       from '../app/store/action';
import { PositionService }      from '../app/services/position.service';
import { CompanyService }       from '../app/services/company.service';
import { TargetJobService }     from '../app/services/job.service';
import { JobService }           from '../app/services/shared/JobService';
import { Cache }                from '../app/services/shared/cache.service';
import { ScheduleService }      from '../app/services/shared/SchedulService';
import { ModalApiService }      from '../app/services/shared/modal';
import { NewsResolver }         from '../app/lib/resolvers/news.resolver';
import { NotificationHandler }  from '../app/services/shared/notifications.service';
import { EventStream }          from '../app/services/shared/sse-client';
import { UnauthResolver }       from '../app/lib/routerActivators/route.resolver';
import { LoggedResolver }       from '../app/lib/routerActivators/logged.resolver';
import { NotesService }         from '../app/services/notes.service';
import { ScheduledService }     from '../app/services/scheduled.service';
import { DayOffService }        from '../app/services/day_off.service';
import { PrintService }         from '../app/services/print.service';
import { DAY_OFF_FACTORY_PROVIDER } from '../app/models/DayOff';
import { OnboardingCheck }      from '../app/lib/resolvers/onboardingCheck';
import { OnBoardingService } from '../app/services/onboardin.service';
import { AssignmentsService } from '../app/services/assignments.service';
/*
* Application Providers/Directives/Pipes
* providers/directives/pipes that only live in our browser environment
*/
export const APPLICATION_PROVIDERS = [
  ...FORM_PROVIDERS,
  ...HTTP_PROVIDERS,
  ...ROUTER_PROVIDERS,
  LUA_API_HTTP_PROVIDERS,
  NODE_API_HTTP_PROVIDERS,
  provide(TranslateLoader, {
    useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
    deps: [Http]
  }),
  provide(BrowserXhr, { useClass: <any>CustomBrowserXhr }),
  provide(PLATFORM_PIPES, {
    useValue: [
      TranslatePipe
    ],
    multi: true
  }),
  NgRedux,
  AppStoreAction,
  TranslateService,
  UserService,
  AuthService,
  AvailabilityService,
  MessageService,
  StaffService,
  SkillsService,
  WebuserService,
  InfoService,
  OrganizationService,
  UnitService,
  EmitService,
  ParseOrganisationService,
  PositionService,
  CompanyService,
  TargetJobService,
  JobService,
  Cache,
  ScheduleService,
  ModalApiService,
  FormBuilder,
  NewsResolver,
  NotificationHandler,
  EventStream,
  UnauthResolver,
  LoggedResolver,
  OnboardingCheck,
  NotesService,
  ScheduledService,
  DayOffService,
  PrintService,
  DAY_OFF_FACTORY_PROVIDER,
  OnBoardingService,
  AssignmentsService
];

export const PROVIDERS = [
  ...APPLICATION_PROVIDERS
];
